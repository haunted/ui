# Redeam Console

The internal tool exclusively for Redeam stuff members to manage Suppliers, Resellers, Devices etc.

This project is built on top of [create-react-app](https://github.com/facebook/create-react-app).

## Table of Contents

* Installation
* Configuration
* Running/Building/Testing
* Adding a feature
* Adding actions/reducers
* Styling components
* Writing tests
* Other notes

## Installation

To start working on `console` clone it from GitLab

```
git clone git@gitlab.com:redeam/console.git
```

#### Installing dependencies

```
npm i
```

## Configuration

To avoid different CORS and Cookies issues (e.g. [this one](https://goo.gl/UUZkpx)) you need to configure a host to run the project on. First edit your `hosts`

```
sudo vim /etc/hosts
```

and add there the following line

```
127.0.0.1 console.redeam.dev
```

Then create a `.env.dev` file in the project's root with the following content:

```
HOST=console.redeam.dev
HTTPS=true
REACT_APP_API_URL=https://4f4c2e9e-72f2-4478-8bb6-b461cde1094f.mock.pstmn.io
REACT_APP_API_KEY=f56ffe9425014c54a64ff0a64a8d8f25
```

This will tell `create-react-app` to run the app on `console.redeam.dev:3000` and use HTTPS with auto-generated certificates.
Other two variables stand for API configuration (currently we are using Postman Mock Service).

> You likely will also need to disable Chrome HSTS security check. 
To do that, change [this setting](chrome://flags/#allow-insecure-localhost) to `Enabled`  

## Running/Building/Testing

#### To run the project in development mode

```
npm start
```

#### To build the source code for production

Set environment variables
```
REACT_APP_API_URL=https://4f4c2e9e-72f2-4478-8bb6-b461cde1094f.mock.pstmn.io
REACT_APP_API_KEY=f56ffe9425014c54a64ff0a64a8d8f25
```

and then run the build

```
npm run build
```

To run the tests

```
npm run test
```

## Adding a feature

Feature is the top level concept of a project. A feature is actually a natural way to describe some capability of an application. For example we have a `supplier` feature which contains all views, components, redux stuff, styles and tests which belong to suppliers. All features stored in `src/features`.

To add a new `testFeature` you need to create the folowing structure:

```
src/
  features/
    testFeature/
      forms/
      redux/
      <TestFeatureComponent1>/
        index.js
        TestFeatureComponent1.js
        TestFeatureComponent1.jss.js
        TestFeatureComponent1.spec.js
      <TestFeatureComponent2>/
        ...
      types.js
```

Where `TestFeatureComponent1` and `TestFeatureComponent2` are components name.

`index.js` should always export the main component as default. For example

```
export { default } from './TestFeatureComponent1';
```

If your feature doesn't require any `redux` stuff or doesn't have any `forms` you don't need to create folders for them.
