// @flow

import 'react-notifications/lib/notifications.css';
import 'react-select/dist/react-select.css';

import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import injectSheet, { jss, ThemeProvider } from 'react-jss';
import { NotificationContainer } from 'react-notifications';
import normalize from 'normalize-jss';
import theme from 'redeam-styleguide/lib/theme';
import store from './common/store';
import LoginView from './features/auth/LoginView';
import DashboardView from './features/dashboard/DashboardView';
import ConfirmContainer from './features/common/ConfirmContainer';
import history from './common/history';
import styles from './App.jss';

jss.createStyleSheet(normalize).attach();

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <div>
          <Router history={history}>
            <Switch>
              <Route path="/login" exact component={LoginView} />
              <Route path="/" component={DashboardView} />
            </Switch>
          </Router>

          <ConfirmContainer />
          <NotificationContainer />
        </div>
      </Provider>
    </ThemeProvider>
  );
}

export default injectSheet(styles)(App);
