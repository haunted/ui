// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import type { Dispatch } from 'redux';
import DefaultModal from 'redeam-styleguide/lib/Modal';
import * as commonActions from '../../common/redux/actions';
import type { State, ModalId } from '../../../common/types';

type Props = {
  /**
   * The id of the current modal
   */
  id: ModalId,

  /**
   * The id of the modal which is currently open (taken from redux)
   */
  openModalId: ModalId,

  /**
   * A bound action creator which closes the Modal
   */
  closeModal: () => any,
};

class Modal extends Component<Props> {
  render() {
    const {
      id,
      openModalId,
      closeModal,
      ...otherProps
    } = this.props;

    return (
      <DefaultModal
        open={id === openModalId}
        onRequestClose={closeModal}
        portalSelector="#modal-root"
        {...otherProps}
      />
    );
  }
}

const mapStateToProps = (state: State) => {
  const openModalId = state.common.ui.modal;

  return {
    openModalId,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...commonActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Modal);
