import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';

class Menu extends Component {
  render() {
    const {
      classes,
    } = this.props;

    return (
      <nav className={classes.wrapper}>
        <Link to="/suppliers">Suppliers</Link>
        <Link to="/resellers">Resellers</Link>
        <Link to="/pass-issuers">Pass Issuers</Link>
        <Link to="/devices">Devices</Link>
        <Link to="/users">Staff Members</Link>
        <Link to="/profile">My Profile</Link>
      </nav>
    );
  }
}

const styles = theme => ({
  wrapper: {
    width: 200,
    padding: [12, 24],
    background: theme.colors.primary,

    '& a': {
      display: 'block',
      margin: [0, 24],
      padding: [12, 0],
      fontSize: 14,
      color: 'rgba(255, 255, 255, 0.5)',
      textDecoration: 'none',

      '&:hover': {
        color: theme.colors.secondary,
      },
    },
  },
});

export default injectSheet(styles)(Menu);
