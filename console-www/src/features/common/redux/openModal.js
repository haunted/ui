// @flow

import type { Action, State } from '../types';
import type { ModalId } from '../../../common/types';

export const openModal = (modalId: ModalId): Action => ({
  type: 'common/openModal',
  modalId,
});

export type OpenModalT = typeof openModal;

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'common/openModal':
      return {
        ...state,
        ui: {
          ...state.ui,
          modal: action.modalId,
        },
      };

    default:
      return state;
  }
}
