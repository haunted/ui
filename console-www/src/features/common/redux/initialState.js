// @flow

import type { State } from '../types';

const state: State = {
  ui: {
    modal: null,
  },
};

export default state;
