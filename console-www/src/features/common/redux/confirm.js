// @flow

import { call, take } from 'redux-saga/effects';
import type { Saga } from 'redux-saga';
import type { Action, ActionType } from '../types';
import { ConfirmManager } from '../ConfirmContainer';

export function confirmAccept(): Action {
  return {
    type: 'common/confirmAccept',
  };
}

export type ConfirmAcceptT = typeof confirmAccept;


export function confirmCancel(): Action {
  return {
    type: 'common/confirmCancel',
  };
}

export type ConfirmCancelT = typeof confirmCancel;


export function* confirm(message: string, options: {}): Saga<boolean> {
  yield call([ConfirmManager, ConfirmManager.show], message, options);

  const action = yield take((['common/confirmAccept', 'common/confirmCancel']: Array<ActionType>));

  yield call([ConfirmManager, ConfirmManager.hide]);

  return action.type === ('common/confirmAccept': ActionType);
}
