// @flow

import type { Action, State } from '../types';

export const closeModal = (): Action => ({
  type: 'common/closeModal',
});

export type CloseModalT = typeof closeModal;

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'common/closeModal':
      return {
        ...state,
        ui: {
          ...state.ui,
          modal: null,
        },
      };

    default:
      return state;
  }
}
