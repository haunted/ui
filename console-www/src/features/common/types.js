// @flow

import type { ModalId } from '../../common/types';


/**
 * State
 */
export type State = {
  ui: {
    modal: ?ModalId,
  },
};


/**
 * Action types
 */
type OpenModal = 'common/openModal';
type CloseModal = 'common/closeModal';
type ConfirmAcceptT = 'common/confirmAccept';
type ConfirmCancelT = 'common/confirmCancel';

export type ActionType =
  | OpenModal
  | CloseModal
  | ConfirmAcceptT
  | ConfirmCancelT;

export type Action =
  | {
      type: OpenModal,
      modalId: ModalId,
    }
  | { type: CloseModal }
  | { type: ConfirmAcceptT }
  | { type: ConfirmCancelT };


export { ConfirmAcceptT, ConfirmCancelT } from './redux/confirm';


/**
 * Action creator types
 */
export { OpenModalT } from './redux/openModal';
export { CloseModalT } from './redux/closeModal';
