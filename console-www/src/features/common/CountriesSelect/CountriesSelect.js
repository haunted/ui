// @flow

import React, { Component } from 'react';
import Select from 'react-select';
import { type FieldProps } from 'redux-form';
import { withStyles } from 'redeam-styleguide/lib/utils';

import countries from '../../../common/resources/countries.json';

import styles, { type Styles } from './CountriesSelect.jss';

type Props = {
  classes: Styles,
} & FieldProps;

export class CountriesSelect extends Component<Props> {
  render() {
    const { input: { value: inputValue, onChange }, classes } = this.props;

    return (
      <div>
        <span className={classes.label}>Country</span>
        <div className={classes.selectWrapper}>
          <Select
            name="country"
            options={countries.map(country => ({ label: country, value: country }))}
            value={inputValue}
            onChange={item => onChange(item && item.value)}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(CountriesSelect);
