// @flow

const styles = {
  label: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#29337d',
  },
  selectWrapper: {
    padding: '10px 0',
  },
};

export type Styles = { [$Keys<typeof styles>]: string };
export default styles;
