import { EventEmitter } from 'events';

const events = {
  CHANGE: 'CHANGE',
  SHOW: 'SHOW',
  HIDE: 'HIDE',
};

class ConfirmManager extends EventEmitter {
  constructor() {
    super();

    this.confirm = null;
  }

  show(message, options = {}) {
    this.confirm = {
      message,
      ...options,
    };

    this.emitChange();
  }

  hide() {
    this.confirm = null;

    this.emitChange();
  }

  emitChange() {
    this.emit(events.CHANGE, this.confirm);
  }

  addChangeListener(callback) {
    this.addListener(events.CHANGE, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(events.CHANGE, callback);
  }
}

export default new ConfirmManager();
