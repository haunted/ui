// @flow

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Confirm from 'redeam-styleguide/lib/Prompt';
import ConfirmManager from './ConfirmManager';
import store from '../../../common/store';
import * as commonActions from '../redux/actions';

type ConfirmT = ?{
  message: string,
  acceptLabel?: string,
  cancelLabel?: string,
};

type LocalState = {
  confirm: ConfirmT,
};

class ConfirmContainer extends Component<{}, LocalState> {
  container: ?HTMLElement;

  constructor() {
    super();

    (this: any).handleChange = this.handleChange.bind(this);

    this.state = {
      confirm: ConfirmManager.confirm,
    };
  }

  componentWillMount() {
    this.container = document.getElementById('modal-root');
    ConfirmManager.addChangeListener(this.handleChange);
  }

  componentWillUnmount() {
    ConfirmManager.removeChangeListener(this.handleChange);
  }

  handleChange(confirm: ConfirmT) {
    this.setState({ confirm });
  }

  render() {
    const { confirm } = this.state;

    if (confirm && this.container) {
      const { message, ...confirmProps } = confirm;

      return ReactDOM.createPortal(
        <Confirm
          onAccept={() => store.dispatch(commonActions.confirmAccept())}
          onCancel={() => store.dispatch(commonActions.confirmCancel())}
          {...confirmProps}
        >
          {message}
        </Confirm>,
        this.container,
      );
    }

    return '';
  }
}

export default ConfirmContainer;
