import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import injectSheet from 'react-jss';
import * as authActions from '../../auth/redux/actions';

import logoImage from '../../../images/logo.svg';

function Header(props) {
  const {
    classes,
    logout,
  } = props;

  return (
    <header className={classes.wrapper}>
      <img src={logoImage} alt="Redeam" className={classes.logo} />

      <button onClick={logout}>Logout</button>
    </header>
  );
}

const styles = {
  wrapper: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 60,
    padding: [0, 24],
    background: 'white',
    boxShadow: [0, 0, 2, 0, 'rgba(0, 0, 0, 0.25)'],
    zIndex: 1000,
  },
  logo: {
    width: 140,
  },
};

const mapDispatchToProps = dispatch => bindActionCreators({
  ...authActions,
}, dispatch);

export default connect(
  null,
  mapDispatchToProps,
)(injectSheet(styles)(Header));
