// @flow

import React, { Component } from 'react';
import { reduxForm, Field, type FormProps } from 'redux-form';
import { withStyles } from 'redeam-styleguide/lib/utils';
import { TextField } from 'redeam-styleguide/lib/formElements';
import Button from 'redeam-styleguide/lib/Button';
import history from '../../../../common/history';
import styles, { type Styles } from './SupplierForm.jss';
import type { SupplierFormData } from '../../types';
import CountriesSelect from '../../../common/CountriesSelect';

type Props = FormProps & {
  edit: boolean,
  classes: Styles,
}

export class SupplierForm extends Component<Props> {
  static defaultProps = {
    edit: false,
  };

  render() {
    const {
      edit,
      initialValues,
      classes,
      handleSubmit,
      formLoading,
    } = this.props;

    return (
      <form onSubmit={handleSubmit} className={classes.form}>
        <div className={classes.formField}>
          <Field
            label="Name"
            name="name"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formField}>
          {edit ? (
            <span>Code: {initialValues.code}</span>
          ) : (
            <Field
              label="Code"
              name="code"
              component={TextField}
              disabled={formLoading}
            />
          )}
        </div>

        <div className={classes.formField}>
          <Field
            label="City"
            name="city"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formField}>
          <Field
            label="State"
            name="state"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formField}>
          <Field
            label="Country"
            name="country"
            component={CountriesSelect}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formField}>
          {!edit && (
            <Field
              label="Email"
              name="email"
              component={TextField}
              disabled={formLoading}
            />
          )}
        </div>

        <div className={classes.formField}>
          {!edit && (
            <Field
              label="Password"
              name="password"
              type="password"
              component={TextField}
              disabled={formLoading}
            />
          )}
        </div>

        <div className={classes.formField}>
          {!edit && (
            <Field
              label="Confirm Password"
              name="passwordConfirmation"
              type="password"
              component={TextField}
              disabled={formLoading}
            />
          )}
        </div>

        <div className={classes.formField}>
          {!edit && (
            <Field
              label="First Name"
              name="firstName"
              component={TextField}
              disabled={formLoading}
            />
          )}
        </div>

        <div className={classes.formField}>
          {!edit && (
            <Field
              label="Last Name"
              name="lastName"
              component={TextField}
              disabled={formLoading}
            />
          )}
        </div>

        <div className={classes.formNavigation}>
          <Button
            tertiary
            onClick={() => history.push('/suppliers')}
            className={classes.cancelButton}
          >
            Cancel
          </Button>

          <Button
            primary
            type="submit"
            disabled={formLoading}
          >
            {edit ? 'Save' : 'Add Supplier'}
          </Button>
        </div>
      </form>
    );
  }
}

export const validate = (values: SupplierFormData, { edit }: Props) => {
  const errors = {};

  if (!values.name.trim()) {
    errors.name = 'Name is required';
  }

  if (!edit) {
    if (!values.code.trim()) {
      errors.code = 'Code is required';
    }

    if (values.lastName.trim() && !values.firstName.trim()) {
      errors.firstName = 'First Name is required';
    }

    if (values.firstName.trim() && !values.lastName.trim()) {
      errors.lastName = 'Last Name is required';
    }

    if (!values.email.trim()) {
      errors.email = 'Email is required';
    }

    if (values.passwordConfirmation && !values.password) {
      errors.password = 'Password cannot be empty';
    } else if (values.password !== values.passwordConfirmation) {
      errors.passwordConfirmation = 'Password don\'t match';
    }
  }

  return errors;
};

export const initialValues = {
  name: '',
  code: '',
  city: '',
  state: '',
  country: '',
  email: '',
  password: '',
  passwordConfirmation: '',
  firstName: '',
  lastName: '',
};

export default reduxForm({
  form: 'supplierForm',
  validate,
  initialValues,
})(withStyles(styles)(SupplierForm));
