// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import SupplierForm from '../forms/SupplierForm';
import * as suppliersActions from '../../suppliers/redux/actions';
import { type AddSupplierT } from '../types';

type Props = {
  loading: boolean,
  formLoading: boolean,
  addSupplier: AddSupplierT,
};

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

class SupplierAddView extends Component<Props> {
  render() {
    const {
      loading,
      addSupplier,
      formLoading,
    } = this.props;

    if (loading) {
      return loader;
    }

    return (
      <div>
        <h1>Add Supplier</h1>
        <SupplierForm
          onSubmit={addSupplier}
          formLoading={formLoading}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...suppliersActions,
}, dispatch);

const mapStateToProps = (state) => {
  const { suppliers } = state;

  return {
    formLoading: suppliers.formLoading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SupplierAddView);
