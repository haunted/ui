// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { suppliers } from '../../../utils/apis';
import type { ActionType, Action, State, EditAction, Supplier } from '../types';
import history from '../../../common/history';

export function editSupplier(supplierData: Supplier): Action {
  return {
    type: 'suppliers/editSupplier',
    supplierData,
  };
}

export type EditSupplierT = typeof editSupplier;

export function* doEditSupplier({ supplierData }: EditAction): Saga<void> {
  yield put(({
    type: 'suppliers/editSupplierRequest',
  }: Action));

  try {
    const { data } = yield call(suppliers.editSupplier, supplierData);

    yield put(({
      type: 'suppliers/editSupplierSuccess',
      data,
    }: Action));

    yield call(history.push, '/suppliers');
    yield call(
      [NotificationManager, NotificationManager.success],
      'Supplier edited successfully',
    );
  } catch (e) {
    yield put(({
      type: 'suppliers/editSupplierFailure',
    }: Action));
    yield call([NotificationManager, NotificationManager.error], 'Failed to edit the supplier');
  }
}

export function* watchEditSupplier(): Iterable<Effect> {
  yield takeLatest(('suppliers/editSupplier': ActionType), doEditSupplier);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'suppliers/editSupplierRequest':
      return {
        ...state,
        formLoading: true,
      };

    case 'suppliers/editSupplierSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.code]: action.data,
        },
        formLoading: false,
      };

    case 'suppliers/editSupplierFailure':
      return {
        ...state,
        formLoading: false,
      };

    default:
      return state;
  }
}
