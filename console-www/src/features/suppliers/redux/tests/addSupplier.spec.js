import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doAddSupplier, reducer } from '../addSupplier';
import { suppliers } from '../../../../utils/apis';
import suppliersMock from '../../../../mocks/suppliers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import history from '../../../../common/history';
import initialState from '../initialState';

const code = 'CODE';
const supplier = suppliersMock.suppliers[0];

delete supplier.code;

const supplierData = supplier;


describe('Add Supplier Saga', () => {
  const err = new Error('error');

  it('should go through add supplier process correctly', () =>
    expectSaga(doAddSupplier, { supplierData })
      .provide([
        [
          matchers.call.fn(suppliers.addSupplier),
          wrapValidResponse(supplierData),
        ],
      ])
      .put({ type: 'suppliers/addSupplierRequest' })
      .put({
        type: 'suppliers/addSupplierSuccess',
        data: supplierData,
      })
      .call(history.push, '/suppliers')
      .call([NotificationManager, NotificationManager.success], 'Supplier created successfully')
      .run());

  it('should show notification when add supplier fails', () =>
    expectSaga(doAddSupplier, { supplierData })
      .provide([
        [matchers.call.fn(suppliers.addSupplier), throwError(err)],
      ])
      .put({ type: 'suppliers/addSupplierRequest' })
      .put({ type: 'suppliers/addSupplierFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to create the supplier')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'suppliers/addSupplierRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'suppliers/addSupplierSuccess',
        data: { ...supplierData, code },
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        formLoading: false,
        entities: {
          [code]: { ...supplierData, code },
        },
        keys: [code],
        loading: false,
        hasMore: true,
        listQueryParams: {
          limit: 50,
          offset: 0,
          sort: 'name',
          search: '',
        },
        ui: { searchSuppliersQuery: '' },
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'suppliers/addSupplierFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: false,
      })
      .run();
  });
});
