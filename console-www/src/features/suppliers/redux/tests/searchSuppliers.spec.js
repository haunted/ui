import { put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { doSearchSuppliers, reducer } from '../searchSuppliers';
import initialState from '../initialState';
import { listSuppliers } from '../listSuppliers';

const query = 'test query';


describe('Search Suppliers Saga', () => {
  it(
    'should go through search suppliers process correctly',
    () => expectSaga(doSearchSuppliers, { query })
      .withState({ passIssuers: initialState })
      .provide([
        [matchers.call.fn(delay, 500)],
      ])
      .put(listSuppliers({ search: query }))
      .run(),
  );

  it('should set search query to store', () => {
    function* saga() {
      yield put({ type: 'suppliers/searchSuppliers', query });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        entities: {},
        keys: [],
        loading: false,
        hasMore: true,
        listQueryParams: {
          limit: 50,
          offset: 0,
          sort: 'name',
          search: '',
        },
        formLoading: false,
        ui: { searchSuppliersQuery: 'test query' },
      })
      .run();
  });
});
