import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doEditSupplier, reducer } from '../editSupplier';
import { suppliers } from '../../../../utils/apis';
import suppliersMock from '../../../../mocks/suppliers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import history from '../../../../common/history';

const supplierData = suppliersMock.suppliers[0];
const changedSuppliersData = {
  ...supplierData,
  name: 'new name',
};


describe('Edit Supplier Saga', () => {
  const err = new Error('error');

  it('should go through edit supplier process correctly', () =>
    expectSaga(doEditSupplier, changedSuppliersData)
      .provide([
        [
          matchers.call.fn(suppliers.editSupplier),
          wrapValidResponse(changedSuppliersData),
        ],
      ])
      .put({ type: 'suppliers/editSupplierRequest' })
      .put({
        type: 'suppliers/editSupplierSuccess',
        data: changedSuppliersData,
      })
      .call(history.push, '/suppliers')
      .call([NotificationManager, NotificationManager.success], 'Supplier edited successfully')
      .run());

  it('should show notification when get supplier fails', () =>
    expectSaga(doEditSupplier, changedSuppliersData)
      .provide([
        [matchers.call.fn(suppliers.editSupplier), throwError(err)],
      ])
      .put({ type: 'suppliers/editSupplierRequest' })
      .put({ type: 'suppliers/editSupplierFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to edit the supplier')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'suppliers/editSupplierRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'suppliers/editSupplierSuccess',
        data: changedSuppliersData,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, {})
      .hasFinalState({
        formLoading: false,
        entities: {
          [supplierData.code]: changedSuppliersData,
        },
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'suppliers/editSupplierFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: false,
      })
      .run();
  });
});
