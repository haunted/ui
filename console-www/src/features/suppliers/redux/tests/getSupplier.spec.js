import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { normalize, schema } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doGetSupplier, reducer } from '../getSupplier';
import { suppliers } from '../../../../utils/apis';
import suppliersMock from '../../../../mocks/suppliers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';

const supplierSchema = new schema.Entity('suppliers', {}, { idAttribute: 'code' });
const code = 'CODE';


describe('Get Supplier Saga', () => {
  const err = new Error('error');
  const normalizedData = normalize(
    suppliersMock.suppliers[0],
    supplierSchema,
  );

  const initialState = {
    entities: {},
    keys: [],
    loading: false,
  };

  it('should go through get supplier process correctly', () =>
    expectSaga(doGetSupplier, { code })
      .provide([
        [
          matchers.call.fn(suppliers.getSupplier),
          wrapValidResponse(suppliersMock.suppliers[0]),
        ],
      ])
      .put({ type: 'suppliers/getSupplierRequest' })
      .put({
        type: 'suppliers/getSupplierSuccess',
        entities: normalizedData.entities.suppliers,
        key: normalizedData.result,
      })
      .run());

  it('should show notification when get supplier fails', () =>
    expectSaga(doGetSupplier, { code })
      .provide([
        [matchers.call.fn(suppliers.getSupplier), throwError(err)],
      ])
      .put({ type: 'suppliers/getSupplierRequest' })
      .put({ type: 'suppliers/getSupplierFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to fetch the supplier')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'suppliers/getSupplierRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'suppliers/getSupplierSuccess',
        entities: normalizedData.entities.suppliers,
        key: normalizedData.result,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        loading: false,
        entities: normalizedData.entities.suppliers,
        keys: [normalizedData.result],
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'suppliers/getSupplierFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });
});
