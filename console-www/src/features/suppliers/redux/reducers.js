import initialState from './initialState';
import { reducer as listSuppliersReducer } from './listSuppliers';
import { reducer as searchSuppliersReducer } from './searchSuppliers';
import { reducer as addSupplierReducer } from './addSupplier';
import { reducer as getSupplierReducer } from './getSupplier';
import { reducer as editSupplierReducer } from './editSupplier';

const reducers = [
  listSuppliersReducer,
  searchSuppliersReducer,
  addSupplierReducer,
  getSupplierReducer,
  editSupplierReducer,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    default:
      newState = state;
      break;
  }

  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
