// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import uniq from 'lodash.uniq';
import { suppliers } from '../../../utils/apis';
import type {
  ActionType,
  Action,
  Supplier,
  SupplierListResponse,
  State,
  GetSupplierAction,
} from '../types';
import type { AxiosResponse, NormalizedSingle } from '../../../common/types';
import { supplierSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';


export function getSupplier(code: string): Action {
  return {
    type: 'suppliers/getSupplier',
    code,
  };
}

export type GetSupplierT = typeof getSupplier;

export function* doGetSupplier({ code }: GetSupplierAction): Saga<void> {
  yield put(({
    type: 'suppliers/getSupplierRequest',
  }: Action));

  try {
    const response: AxiosResponse<SupplierListResponse> =
      yield call(suppliers.getSupplier, code);

    const normalizedData: NormalizedSingle<Supplier, 'suppliers'> = normalize(
      response.data,
      supplierSchema,
    );

    yield put(({
      type: 'suppliers/getSupplierSuccess',
      entities: normalizedData.entities.suppliers,
      key: normalizedData.result,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'suppliers/getSupplierFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to fetch the supplier');
    }
  }
}

export function* watchGetSupplier(): Iterable<Effect> {
  yield takeLatest(('suppliers/getSupplier': ActionType), doGetSupplier);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'suppliers/getSupplierRequest':
      return {
        ...state,
        loading: true,
      };

    case 'suppliers/getSupplierSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
        keys: uniq([...state.keys, action.key]),
        loading: false,
      };

    case 'suppliers/getSupplierFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
