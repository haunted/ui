// @flow

import { takeLatest, select, call, put } from 'redux-saga/effects';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import type { $AxiosXHR } from 'axios';
import type { Saga, Effect } from 'redux-saga';
import { suppliers } from '../../../utils/apis';
import { mergeListResponse, mergeQueryParams } from '../../../utils/redux';
import { supplierListSchema } from '../../../common/schemas';
import type {
  State,
  Supplier,
  SupplierListResponse,
  ActionType,
  Action,
  ListSuppliersAction,
} from '../types';
import type { Normalized, ListQueryParams, State as GlobalState } from '../../../common/types';
import isAuthorized from '../../../utils/isAuthorized';


/**
 * Action creator
 */
export function listSuppliers(params: $Shape<ListQueryParams> = {}): Action {
  return {
    type: 'suppliers/listSuppliers',
    params,
  };
}


/**
 * Action's logic
 */
export function* doListSuppliers({ params }: ListSuppliersAction): Saga<void> {
  const prevParams = yield select((state: GlobalState) => state.suppliers.listQueryParams);
  const listQueryParams = mergeQueryParams(prevParams, params);

  yield put(({
    type: 'suppliers/listSuppliersRequest',
  }: Action));

  try {
    const response: $AxiosXHR<SupplierListResponse> =
      yield call(suppliers.listSuppliers, listQueryParams);

    const normalizedData: Normalized<Supplier, 'suppliers'> = normalize(
      response.data.suppliers,
      supplierListSchema,
    );

    yield put(({
      type: 'suppliers/listSuppliersSuccess',
      entities: normalizedData.entities.suppliers || {},
      keys: normalizedData.result,
      listQueryParams,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'suppliers/listSuppliersRequest',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to get suppliers');
    }
  }
}

/**
 * Saga watcher
 */
export function* watchListSuppliers(): Iterable<Effect> {
  yield takeLatest(('suppliers/listSuppliers': ActionType), doListSuppliers);
}

/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'suppliers/listSuppliersRequest':
      return {
        ...state,
        loading: true,
      };

    case 'suppliers/listSuppliersSuccess':
      return {
        ...state,
        loading: false,
        ...mergeListResponse(state, action),
      };

    case 'suppliers/listSuppliersFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
