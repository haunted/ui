// @flow

import type { State } from '../../../common/types';
import type { Supplier } from '../types';

/**
 * Returns the array of all loaded suppliers
 */
export const supplierListSelector = (state: State): Array<Supplier> =>
  state.suppliers.keys.map(code => state.suppliers.entities[code]);
