// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import uniq from 'lodash.uniq';
import { suppliers } from '../../../utils/apis';
import type { ActionType, Action, State, AddAction, SupplierFormData } from '../types';
import history from '../../../common/history';

export function addSupplier(supplierData: SupplierFormData): Action {
  return {
    type: 'suppliers/addSupplier',
    supplierData,
  };
}

export type AddSupplierT = typeof addSupplier;

export function* doAddSupplier({ supplierData }: AddAction): Saga<void> {
  yield put(({
    type: 'suppliers/addSupplierRequest',
  }: Action));

  try {
    const { data } = yield call(suppliers.addSupplier, supplierData);

    yield put(({
      type: 'suppliers/addSupplierSuccess',
      data,
    }: Action));

    yield call(history.push, '/suppliers');
    yield call(
      [NotificationManager, NotificationManager.success],
      'Supplier created successfully',
    );
  } catch (e) {
    yield put(({
      type: 'suppliers/addSupplierFailure',
    }: Action));
    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to create the supplier',
    );
  }
}

export function* watchAddSupplier(): Iterable<Effect> {
  yield takeLatest(('suppliers/addSupplier': ActionType), doAddSupplier);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'suppliers/addSupplierRequest':
      return {
        ...state,
        formLoading: true,
      };

    case 'suppliers/addSupplierSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.code]: action.data,
        },
        keys: uniq([...state.keys, action.data.code]),
        formLoading: false,
      };

    case 'suppliers/addSupplierFailure':
      return {
        ...state,
        formLoading: false,
      };

    default:
      return state;
  }
}
