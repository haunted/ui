// @flow

import type { State } from '../types';

const state: State = {
  entities: {},
  keys: [],
  loading: false,
  formLoading: false,
  hasMore: true,
  listQueryParams: {
    limit: 50,
    offset: 0,
    sort: 'name',
    search: '',
  },
  ui: {
    searchSuppliersQuery: '',
  },
};

export default state;
