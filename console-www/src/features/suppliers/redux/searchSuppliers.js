// @flow

import { put, call, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import type { Saga, Effect } from 'redux-saga';
import type { Action, ActionType, State, SearchSuppliersActionT } from '../types';
import { listSuppliers } from './listSuppliers';


/**
 * Action creator
 */
export function searchSuppliers(query: string): Action {
  return {
    type: 'suppliers/searchSuppliers',
    query,
  };
}


/**
 * Export the type of the action creator
 */
export type SearchSuppliersT = typeof searchSuppliers;


/**
 * Action's logic
 */
export function* doSearchSuppliers({ query }: SearchSuppliersActionT): Saga<void> {
  yield call(delay, 500);
  yield put(listSuppliers({ search: query }));
}


/**
 * Saga watcher
 */
export function* watchSearchSuppliers(): Iterable<Effect> {
  yield takeLatest(('suppliers/searchSuppliers': ActionType), doSearchSuppliers);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'suppliers/searchSuppliers':
      return {
        ...state,
        listQueryParams: {
          ...state.listQueryParams,
          offset: 0,
        },
        ui: {
          ...state.ui,
          searchSuppliersQuery: action.query,
        },
      };

    default:
      return state;
  }
}
