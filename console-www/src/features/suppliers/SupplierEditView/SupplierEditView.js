// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import SupplierForm from '../forms/SupplierForm';
import type { EditSupplierT, GetSupplierT, Supplier } from '../types';
import * as supplierActions from '../redux/actions';

type Props = {
  supplier: Supplier,
  loading: boolean,
  formLoading: boolean,
  supplierCode: string,
  getSupplier: GetSupplierT,
  editSupplier: EditSupplierT,
};

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

class SupplierEditView extends Component<Props> {
  componentWillMount() {
    const { supplier, getSupplier, supplierCode } = this.props;

    if (!supplier) {
      getSupplier(supplierCode);
    }
  }

  render() {
    const {
      supplier,
      loading,
      editSupplier,
      formLoading,
    } = this.props;

    if (loading) {
      return loader;
    }

    return (
      <div>
        <h1>Edit Supplier</h1>
        <SupplierForm
          onSubmit={editSupplier}
          initialValues={supplier}
          formLoading={formLoading}
          edit
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...supplierActions,
}, dispatch);

const mapStateToProps = (state, props) => {
  const {
    suppliers,
  } = state;

  const {
    match,
  } = props;

  return {
    supplier: suppliers.entities[match.params.code],
    loading: suppliers.loading,
    formLoading: suppliers.formLoading,
    supplierCode: match.params.code,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SupplierEditView);
