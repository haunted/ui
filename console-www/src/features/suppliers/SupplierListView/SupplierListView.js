// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import Table from 'redeam-styleguide/lib/Table';
import InfiniteScroll from 'redeam-styleguide/lib/InfiniteScroll';
import Button from 'redeam-styleguide/lib/Button';
import * as supplierActions from '../redux/actions';
import { supplierListSelector } from '../redux/selectors';
import history from '../../../common/history';
import styles, { type Styles } from './SupplierListView.jss';
import type { State as SuppliersState, Supplier, SearchSuppliersT } from '../types';
import type { ListQueryParams, State } from '../../../common/types';
import tableChangeHandler from '../../../utils/tableChangeHandler';

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

const columns = [
  {
    dataField: 'name',
    text: 'Name',
    sort: true,
  },
  {
    dataField: 'code',
    text: 'Code',
    sort: true,
  },
  {
    dataField: 'city',
    text: 'City',
  },
  {
    dataField: 'state',
    text: 'State',
  },
  {
    dataField: 'country',
    text: 'Country',
  },
  {
    dataField: 'actions',
    text: 'Actions',
    formatter: (e, { code }) => (
      <div>
        <button onClick={() => history.push(`/suppliers/${code}/edit`)}>Edit</button>
        <button onClick={() => history.push(`/suppliers/${code}/devices`)}>Devices</button>
      </div>
    ),
  },
];

type Props = {
  listSuppliers: (params?: $Shape<ListQueryParams>) => any,
  searchSuppliers: SearchSuppliersT,
  suppliers: {
    list: Array<Supplier>,
  } & SuppliersState,
  nextOffset: number,
  classes: Styles,
};

class SupplierListView extends Component<Props> {
  componentWillMount() {
    this.props.listSuppliers();
  }

  render() {
    const {
      suppliers,
      classes,
      nextOffset,
      listSuppliers,
      searchSuppliers,
    } = this.props;

    return (
      <div>
        <div className={classes.header}>
          <h1>Suppliers</h1>

          <div>
            <input
              onChange={e => searchSuppliers(e.target.value)}
              value={suppliers.ui.searchSuppliersQuery}
              placeholder="Search suppliers..."
            />
            {' '}
            <Button primary onClick={() => history.push('/suppliers/add')}>Add Supplier</Button>
          </div>

        </div>

        <InfiniteScroll
          loading={suppliers.loading}
          loader={loader}
          hasMore={suppliers.hasMore}
          loadMore={() => listSuppliers({ offset: nextOffset })}
        >
          <Table
            keyField="code"
            data={suppliers.list}
            columns={columns}
            remote={{ sort: true }}
            loading={suppliers.loading}
            onTableChange={tableChangeHandler(listSuppliers)}
          />
        </InfiniteScroll>
      </div>
    );
  }
}

const mapStateToProps = (state: State) => {
  const {
    suppliers,
  } = state;

  const { limit, offset } = suppliers.listQueryParams;

  return {
    suppliers: {
      ...suppliers,
      list: supplierListSelector(state),
    },
    nextOffset: offset + limit,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...supplierActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(SupplierListView));
