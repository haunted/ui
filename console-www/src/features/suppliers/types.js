// @flow

import type { ListQueryParams } from '../../common/types';

/**
 *  Base type
 */
export type Supplier = {
  code: string,
  name: string,
  city: ?string,
  state: ?string,
  country: ?string,
  created: string,
  email: string,
};

export type SupplierFormData = {
  password: string,
  passwordConfirmation: string,
  lastName: string,
  firstName: string,
} & Supplier;


/**
 * Helper types
 */
type Entities = { [code: string]: Supplier };
type Keys = Array<string>;


/**
 * State type
 */
export type State = {
  entities: Entities,
  keys: Keys,
  loading: boolean,
  formLoading: boolean,
  hasMore: boolean,
  listQueryParams: ListQueryParams,
  ui: {
    searchSuppliersQuery: string,
  },
};


/**
 * Response types
 */
export type SupplierListResponse = {
  suppliers: Array<Supplier>,
};


/**
 * Action types
 */
type listSuppliers = 'suppliers/listSuppliers';
type listSuppliersRequest = 'suppliers/listSuppliersRequest';
type listSuppliersSuccess = 'suppliers/listSuppliersSuccess';
type listSuppliersFailure = 'suppliers/listSuppliersFailure';
type AddSupplier = 'suppliers/addSupplier';
type AddSupplierRequest = 'suppliers/addSupplierRequest';
type AddSupplierSuccess = 'suppliers/addSupplierSuccess';
type AddSupplierFailure = 'suppliers/addSupplierFailure';
type GetSupplier = 'suppliers/getSupplier';
type GetSupplierRequest = 'suppliers/getSupplierRequest';
type GetSupplierSuccess = 'suppliers/getSupplierSuccess';
type GetSupplierFailure = 'suppliers/getSupplierFailure';
type EditSupplier = 'suppliers/editSupplier';
type EditSupplierRequest = 'suppliers/editSupplierRequest';
type EditSupplierSuccess = 'suppliers/editSupplierSuccess';
type EditSupplierFailure = 'suppliers/editSupplierFailure';
type SearchSuppliersT = 'suppliers/searchSuppliers';

export type AddAction = {
  type: AddSupplier,
  supplierData: SupplierFormData,
};

export type EditAction = {
  type: EditSupplier,
  supplierData: Supplier,
};

export type GetSupplierAction = {
  type: GetSupplier,
  code: string,
};

export type ActionType =
  | listSuppliers
  | listSuppliersRequest
  | listSuppliersSuccess
  | listSuppliersFailure
  | AddSupplier
  | AddSupplierRequest
  | AddSupplierSuccess
  | AddSupplierFailure
  | GetSupplier
  | GetSupplierRequest
  | GetSupplierSuccess
  | GetSupplierFailure
  | EditSupplier
  | EditSupplierRequest
  | EditSupplierSuccess
  | EditSupplierFailure
  | SearchSuppliersT;

export type ListSuppliersAction = {
  type: listSuppliers,
  params: ListQueryParams,
};

export type SearchSuppliersActionT = {
  type: SearchSuppliersT,
  query: string,
};

export type Action =
  | ListSuppliersAction
  | {
      type: listSuppliersRequest,
    }
  | {
      type: listSuppliersSuccess,
      entities: Entities,
      keys: Keys,
      listQueryParams: ListQueryParams,
    }
  | { type: listSuppliersFailure }
  | {
      type: SearchSuppliersT,
      query: string,
    }
  | AddAction
  | { type: AddSupplierRequest }
  | {
      type: AddSupplierSuccess,
      data: Supplier,
    }
  | { type: AddSupplierFailure }
  | GetSupplierAction
  | { type: GetSupplierRequest }
  | {
      type: GetSupplierSuccess,
      entities: Entities,
      key: string,
    }
  | { type: GetSupplierFailure }
  | EditAction
  | { type: EditSupplierRequest }
  | {
      type: EditSupplierSuccess,
      data: Supplier,
    }
  | { type: EditSupplierFailure };


/**
 * Action creator types
 */
export { SearchSuppliersT } from './redux/searchSuppliers';
export { AddSupplierT } from './redux/addSupplier';
export { GetSupplierT } from './redux/getSupplier';
export { EditSupplierT } from './redux/editSupplier';
