// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { passIssuers } from '../../../utils/apis';
import type { ActionType, Action, State, EditAction, PassIssuer } from '../types';
import history from '../../../common/history';

export function editPassIssuer(passIssuerData: PassIssuer): Action {
  return {
    type: 'passIssuers/editPassIssuer',
    passIssuerData,
  };
}

export function* doEditPassIssuer({ passIssuerData }: EditAction): Saga<void> {
  yield put(({
    type: 'passIssuers/editPassIssuerRequest',
  }: Action));

  try {
    const { data } = yield call(passIssuers.editPassIssuer, passIssuerData);

    yield put(({
      type: 'passIssuers/editPassIssuerSuccess',
      data,
    }: Action));

    yield call(history.push, '/pass-issuers');
    yield call(
      [NotificationManager, NotificationManager.success],
      'Pass issuer edited successfully',
    );
  } catch (e) {
    yield put(({
      type: 'passIssuers/editPassIssuerFailure',
    }: Action));
    yield call([NotificationManager, NotificationManager.error], 'Failed to edit the pass issuer');
  }
}

export function* watchEditPassIssuer(): Iterable<Effect> {
  yield takeLatest(('passIssuers/editPassIssuer': ActionType), doEditPassIssuer);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'passIssuers/editPassIssuerRequest':
      return {
        ...state,
        formLoading: true,
      };

    case 'passIssuers/editPassIssuerSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.code]: action.data,
        },
        formLoading: false,
      };

    case 'passIssuers/editPassIssuerFailure':
      return {
        ...state,
        formLoading: false,
      };

    default:
      return state;
  }
}
