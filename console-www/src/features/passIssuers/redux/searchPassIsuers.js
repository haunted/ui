// @flow

import { put, call, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import type { Saga, Effect } from 'redux-saga';
import type { Action, ActionType, State, SearchPassIssuersActionT } from '../types';
import { listPassIssuers } from './listPassIssuers';


/**
 * Main action creator
 */
export function searchPassIssuers(query: string): Action {
  return {
    type: 'passIssuers/searchPassIssuers',
    query,
  };
}


/**
 * Export the type of the action creator
 */
export type SearchPassIssuersT = typeof searchPassIssuers;


/**
 * Action's logic
 */
export function* doSearchPassIssuers({ query }: SearchPassIssuersActionT): Saga<void> {
  yield call(delay, 500);
  yield put(listPassIssuers({ search: query }));
}


/**
 * Saga watcher
 */
export function* watchSearchPassIssuers(): Iterable<Effect> {
  yield takeLatest(('passIssuers/searchPassIssuers': ActionType), doSearchPassIssuers);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'passIssuers/searchPassIssuers':
      return {
        ...state,
        listQueryParams: {
          ...state.listQueryParams,
          offset: 0,
        },
        ui: {
          ...state.ui,
          searchPassIssuersQuery: action.query,
        },
      };

    default:
      return state;
  }
}
