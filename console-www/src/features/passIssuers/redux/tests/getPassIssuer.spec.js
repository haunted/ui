import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { normalize, schema } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doGetPassIssuer, reducer } from '../getPassIssuer';
import { passIssuers } from '../../../../utils/apis';
import passIssuersMock from '../../../../mocks/passIssuers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';

const passIssuerSchema = new schema.Entity('passIssuers', {}, { idAttribute: 'code' });
const code = 'CODE';


describe('Get PassIssuer Saga', () => {
  const err = new Error('error');
  const normalizedData = normalize(
    passIssuersMock.passIssuers[0],
    passIssuerSchema,
  );

  const initialState = {
    entities: {},
    keys: [],
    loading: false,
  };

  it('should go through get pass issuer process correctly', () =>
    expectSaga(doGetPassIssuer, { code })
      .provide([
        [
          matchers.call.fn(passIssuers.getPassIssuer),
          wrapValidResponse(passIssuersMock.passIssuers[0]),
        ],
      ])
      .put({ type: 'passIssuers/getPassIssuerRequest' })
      .put({
        type: 'passIssuers/getPassIssuerSuccess',
        entities: normalizedData.entities.passIssuers,
        key: normalizedData.result,
      })
      .run());

  it('should show notification when get pass issuer fails', () =>
    expectSaga(doGetPassIssuer, { code })
      .provide([
        [matchers.call.fn(passIssuers.getPassIssuer), throwError(err)],
      ])
      .put({ type: 'passIssuers/getPassIssuerRequest' })
      .put({ type: 'passIssuers/getPassIssuerFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to fetch the pass issuer')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'passIssuers/getPassIssuerRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'passIssuers/getPassIssuerSuccess',
        entities: normalizedData.entities.passIssuers,
        key: normalizedData.result,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        loading: false,
        entities: normalizedData.entities.passIssuers,
        keys: [normalizedData.result],
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'passIssuers/getPassIssuerFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });
});
