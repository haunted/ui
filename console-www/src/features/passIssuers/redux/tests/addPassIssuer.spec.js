import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doAddPassIssuer, reducer } from '../addPassIssuer';
import { passIssuers } from '../../../../utils/apis';
import passIssuersMock from '../../../../mocks/passIssuers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import history from '../../../../common/history';
import initialState from '../initialState';

const code = 'CODE';
const passIssuer = passIssuersMock.passIssuers[0];

delete passIssuer.code;

const passIssuerData = passIssuer;


describe('Add Pass Issuer Saga', () => {
  const err = new Error('error');

  it('should go through add pass issuer process correctly', () =>
    expectSaga(doAddPassIssuer, { passIssuerData })
      .provide([
        [
          matchers.call.fn(passIssuers.addPassIssuer),
          wrapValidResponse(passIssuerData),
        ],
      ])
      .put({ type: 'passIssuers/addPassIssuerRequest' })
      .put({
        type: 'passIssuers/addPassIssuerSuccess',
        data: passIssuerData,
      })
      .call(history.push, '/pass-issuers')
      .call([NotificationManager, NotificationManager.success], 'Pass issuer created successfully')
      .run());

  it('should show notification when add pass issuer fails', () =>
    expectSaga(doAddPassIssuer, { passIssuerData })
      .provide([
        [matchers.call.fn(passIssuers.addPassIssuer), throwError(err)],
      ])
      .put({ type: 'passIssuers/addPassIssuerRequest' })
      .put({ type: 'passIssuers/addPassIssuerFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to create the pass issuer')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'passIssuers/addPassIssuerRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'passIssuers/addPassIssuerSuccess',
        data: { ...passIssuerData, code },
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        formLoading: false,
        entities: {
          [code]: { ...passIssuerData, code },
        },
        keys: [code],
        loading: false,
        hasMore: true,
        listQueryParams: {
          limit: 50,
          offset: 0,
          sort: 'name',
          search: '',
        },
        ui: { searchPassIssuersQuery: '' },
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'passIssuers/addPassIssuerFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: false,
      })
      .run();
  });
});
