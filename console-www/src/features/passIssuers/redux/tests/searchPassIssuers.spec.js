import { put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { doSearchPassIssuers, reducer } from '../searchPassIsuers';
import initialState from '../initialState';
import { listPassIssuers } from '../listPassIssuers';

const query = 'test query';


describe('Search Pass Issuers Saga', () => {
  it(
    'should go through search pass issuers process correctly',
    () => expectSaga(doSearchPassIssuers, { query })
      .withState({ passIssuers: initialState })
      .provide([
        [matchers.call.fn(delay, 500)],
      ])
      .put(listPassIssuers({ search: query }))
      .run(),
  );

  it('should set search query to store', () => {
    function* saga() {
      yield put({ type: 'passIssuers/searchPassIssuers', query });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        entities: {},
        keys: [],
        loading: false,
        hasMore: true,
        listQueryParams: {
          limit: 50,
          offset: 0,
          sort: 'name',
          search: '',
        },
        formLoading: false,
        ui: { searchPassIssuersQuery: 'test query' },
      })
      .run();
  });
});
