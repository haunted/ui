import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doEditPassIssuer, reducer } from '../editPassIssuer';
import { passIssuers } from '../../../../utils/apis';
import passIssuersMock from '../../../../mocks/passIssuers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import history from '../../../../common/history';

const passIssuerData = passIssuersMock.passIssuers[0];
const changedPassIssuersData = {
  ...passIssuerData,
  name: 'new name',
};


describe('Edit Pass Issuer Saga', () => {
  const err = new Error('error');

  it('should go through edit pass issuer process correctly', () =>
    expectSaga(doEditPassIssuer, changedPassIssuersData)
      .provide([
        [
          matchers.call.fn(passIssuers.editPassIssuer),
          wrapValidResponse(changedPassIssuersData),
        ],
      ])
      .put({ type: 'passIssuers/editPassIssuerRequest' })
      .put({
        type: 'passIssuers/editPassIssuerSuccess',
        data: changedPassIssuersData,
      })
      .call(history.push, '/pass-issuers')
      .call([NotificationManager, NotificationManager.success], 'Pass issuer edited successfully')
      .run());

  it('should show notification when get pass issuer fails', () =>
    expectSaga(doEditPassIssuer, changedPassIssuersData)
      .provide([
        [matchers.call.fn(passIssuers.editPassIssuer), throwError(err)],
      ])
      .put({ type: 'passIssuers/editPassIssuerRequest' })
      .put({ type: 'passIssuers/editPassIssuerFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to edit the pass issuer')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'passIssuers/editPassIssuerRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'passIssuers/editPassIssuerSuccess',
        data: changedPassIssuersData,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, {})
      .hasFinalState({
        formLoading: false,
        entities: {
          [passIssuerData.code]: changedPassIssuersData,
        },
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'passIssuers/editPassIssuerFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: false,
      })
      .run();
  });
});
