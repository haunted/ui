import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { normalize, schema } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doListPassIssuers, reducer } from '../listPassIssuers';
import { passIssuers } from '../../../../utils/apis';
import passIssuersMock from '../../../../mocks/passIssuers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import initialState from '../initialState';

const passIssuerSchema = new schema.Entity('passIssuers', {}, { idAttribute: 'code' });
const passIssuerListSchema = [passIssuerSchema];
const params = {};


describe('List Pass Issuers Saga', () => {
  const err = new Error('error');
  const normalizedData = normalize(
    passIssuersMock.passIssuers,
    passIssuerListSchema,
  );

  it(
    'should go through list pass issuers process correctly',
    () => expectSaga(doListPassIssuers, { params })
      .withState({ passIssuers: initialState })
      .provide([
        [matchers.call.fn(passIssuers.listPassIssuers), wrapValidResponse(passIssuersMock)],
      ])
      .put({ type: 'passIssuers/listPassIssuersRequest' })
      .put({
        type: 'passIssuers/listPassIssuersSuccess',
        entities: normalizedData.entities.passIssuers,
        keys: normalizedData.result,
        listQueryParams: initialState.listQueryParams,
      })
      .run(),
  );

  it(
    'should show notification when list pass issuers fails',
    () => expectSaga(doListPassIssuers, { params })
      .withState({ passIssuers: initialState })
      .provide([
        [matchers.call.fn(passIssuers.listPassIssuers), throwError(err)],
      ])
      .put({ type: 'passIssuers/listPassIssuersRequest' })
      .put({ type: 'passIssuers/listPassIssuersFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to fetch pass issuers')
      .run(),
  );

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'passIssuers/listPassIssuersRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` when request successes', () => {
    function* saga() {
      yield put({
        type: 'passIssuers/listPassIssuersSuccess',
        entities: normalizedData.entities.passIssuers,
        keys: normalizedData.result,
        listQueryParams: initialState.listQueryParams,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        loading: false,
        entities: normalizedData.entities.passIssuers,
        keys: normalizedData.result,
        listQueryParams: initialState.listQueryParams,
        ui: initialState.ui,
        formLoading: false,
        hasMore: false,
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'passIssuers/listPassIssuersFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });
});
