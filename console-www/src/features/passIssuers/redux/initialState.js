// @flow

import type { State } from '../types';

const initialState: State = {
  entities: {},
  keys: [],
  loading: false,
  hasMore: true,
  listQueryParams: {
    limit: 50,
    offset: 0,
    sort: 'name',
    search: '',
  },
  formLoading: false,
  ui: {
    searchPassIssuersQuery: '',
  },
};

export default initialState;
