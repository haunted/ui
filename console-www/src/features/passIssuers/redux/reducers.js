import initialState from './initialState';
import { reducer as listPassIssuers } from './listPassIssuers';
import { reducer as getPassIssuer } from './getPassIssuer';
import { reducer as editPassIssuer } from './editPassIssuer';
import { reducer as addPassIssuer } from './addPassIssuer';
import { reducer as searchPassIssuers } from './searchPassIsuers';

const reducers = [
  listPassIssuers,
  searchPassIssuers,
  getPassIssuer,
  editPassIssuer,
  addPassIssuer,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    default:
      newState = state;
      break;
  }

  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
