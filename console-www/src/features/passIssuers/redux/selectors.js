// @flow

import type { State } from '../../../common/types';
import type { PassIssuer } from '../types';

/**
 * Returns the array of all loaded pass issuers
 */
export const passIssuerListSelector = (state: State): Array<PassIssuer> =>
  state.passIssuers.keys.map(code => state.passIssuers.entities[code]);
