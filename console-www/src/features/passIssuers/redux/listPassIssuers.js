// @flow

import { takeLatest, call, put, select } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import { passIssuers } from '../../../utils/apis';
import type {
  ActionType,
  Action,
  PassIssuer,
  PassIssuerListResponse,
  State,
  ListPassIssuersAction,
} from '../types';
import type {
  AxiosResponse,
  ListQueryParams,
  Normalized,
  State as GlobalState,
} from '../../../common/types';
import { passIssuerListSchema } from '../../../common/schemas';
import { mergeListResponse, mergeQueryParams } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';


export function listPassIssuers(params: $Shape<ListQueryParams> = {}): Action {
  return {
    type: 'passIssuers/listPassIssuers',
    params,
  };
}

/**
 * Export the type of the action creator
 */
export type ListPassIssuersT = typeof listPassIssuers;

export function* doListPassIssuers({ params }: ListPassIssuersAction): Saga<void> {
  const prevParams = yield select((state: GlobalState) => state.passIssuers.listQueryParams);
  const listQueryParams = mergeQueryParams(prevParams, params);

  yield put(({
    type: 'passIssuers/listPassIssuersRequest',
  }: Action));

  try {
    const response: AxiosResponse<PassIssuerListResponse> =
      yield call(passIssuers.listPassIssuers, listQueryParams);

    const normalizedData: Normalized<PassIssuer, 'passIssuers'> = normalize(
      response.data.passIssuers,
      passIssuerListSchema,
    );

    yield put(({
      type: 'passIssuers/listPassIssuersSuccess',
      entities: normalizedData.entities.passIssuers || {},
      keys: normalizedData.result,
      listQueryParams,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'passIssuers/listPassIssuersFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to fetch pass issuers');
    }
  }
}

export function* watchListPassIssuers(): Iterable<Effect> {
  yield takeLatest(('passIssuers/listPassIssuers': ActionType), doListPassIssuers);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'passIssuers/listPassIssuersRequest':
      return {
        ...state,
        loading: true,
      };

    case 'passIssuers/listPassIssuersSuccess':
      return {
        ...state,
        loading: false,
        ...mergeListResponse(state, action),
      };

    case 'passIssuers/listPassIssuersFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
