// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import uniq from 'lodash.uniq';
import { passIssuers } from '../../../utils/apis';
import type { ActionType, Action, State, AddAction, PassIssuerFormData } from '../types';
import history from '../../../common/history';

export function addPassIssuer(passIssuerData: PassIssuerFormData): Action {
  return {
    type: 'passIssuers/addPassIssuer',
    passIssuerData: {
      ...passIssuerData,
      skip_account_creation: true, // TODO hack
    },
  };
}

export function* doAddPassIssuer({ passIssuerData }: AddAction): Saga<void> {
  yield put(({
    type: 'passIssuers/addPassIssuerRequest',
  }: Action));

  try {
    const { data } = yield call(passIssuers.addPassIssuer, passIssuerData);

    yield put(({
      type: 'passIssuers/addPassIssuerSuccess',
      data,
    }: Action));

    yield call(history.push, '/pass-issuers');
    yield call(
      [NotificationManager, NotificationManager.success],
      'Pass issuer created successfully',
    );
  } catch (e) {
    yield put(({
      type: 'passIssuers/addPassIssuerFailure',
    }: Action));
    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to create the pass issuer',
    );
  }
}

export function* watchAddPassIssuer(): Iterable<Effect> {
  yield takeLatest(('passIssuers/addPassIssuer': ActionType), doAddPassIssuer);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'passIssuers/addPassIssuerRequest':
      return {
        ...state,
        formLoading: true,
      };

    case 'passIssuers/addPassIssuerSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.code]: action.data,
        },
        keys: uniq([...state.keys, action.data.code]),
        formLoading: false,
      };

    case 'passIssuers/addPassIssuerFailure':
      return {
        ...state,
        formLoading: false,
      };

    default:
      return state;
  }
}
