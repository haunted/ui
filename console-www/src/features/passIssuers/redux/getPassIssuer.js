// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import uniq from 'lodash.uniq';
import { passIssuers } from '../../../utils/apis';
import type {
  ActionType,
  Action,
  PassIssuer,
  PassIssuerListResponse,
  State,
  GetPassIssuerAction,
} from '../types';
import type { AxiosResponse, NormalizedSingle } from '../../../common/types';
import { passIssuerSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';


export function getPassIssuer(code: string): Action {
  return {
    type: 'passIssuers/getPassIssuer',
    code,
  };
}

export function* doGetPassIssuer({ code }: GetPassIssuerAction): Saga<void> {
  yield put(({
    type: 'passIssuers/getPassIssuerRequest',
  }: Action));

  try {
    const response: AxiosResponse<PassIssuerListResponse> =
      yield call(passIssuers.getPassIssuer, code);

    const normalizedData: NormalizedSingle<PassIssuer, 'passIssuers'> = normalize(
      response.data,
      passIssuerSchema,
    );

    yield put(({
      type: 'passIssuers/getPassIssuerSuccess',
      entities: normalizedData.entities.passIssuers,
      key: normalizedData.result,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'passIssuers/getPassIssuerFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call(
        [NotificationManager, NotificationManager.error],
        'Failed to fetch the pass issuer',
      );
    }
  }
}

export function* watchGetPassIssuer(): Iterable<Effect> {
  yield takeLatest(('passIssuers/getPassIssuer': ActionType), doGetPassIssuer);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'passIssuers/getPassIssuerRequest':
      return {
        ...state,
        loading: true,
      };

    case 'passIssuers/getPassIssuerSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
        keys: uniq([...state.keys, action.key]),
        loading: false,
      };

    case 'passIssuers/getPassIssuerFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
