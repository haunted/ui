// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import InfiniteScroll from 'redeam-styleguide/lib/InfiniteScroll';
import Table from 'redeam-styleguide/lib/Table';
import Button from 'redeam-styleguide/lib/Button';
import * as passIssuerActions from '../redux/actions';
import { passIssuerListSelector } from '../redux/selectors';
import type {
  State as PassIssuersState,
  PassIssuer,
  SearchPassIssuersT,
  ListPassIssuersT,
} from '../types';
import type { State } from '../../../common/types';
import history from '../../../common/history';
import styles, { type Styles } from './PassIssuerListView.jss';
import tableChangeHandler from '../../../utils/tableChangeHandler';

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

type Props = {
  listPassIssuers: ListPassIssuersT,
  searchPassIssuers: SearchPassIssuersT,
  passIssuers: {
    list: Array<PassIssuer>,
  } & PassIssuersState,
  classes: Styles,
  nextOffset: number,
};

const columns = [
  {
    text: 'Pass Issuer Name',
    dataField: 'name',
    sort: true,
  },
  {
    text: 'Pass Issuer Code',
    dataField: 'code',
    sort: true,
  },
  {
    text: 'City',
    dataField: 'city',
  },
  {
    text: 'State',
    dataField: 'state',
  },
  {
    text: 'Country',
    dataField: 'country',
  },
  {
    dataField: 'actions',
    text: 'Actions',
    formatter: (e, { code }) => (
      <div>
        <button onClick={() => history.push(`/pass-issuers/${code}/edit`)}>Edit</button>
      </div>
    ),
  },
];

class PassIssuerListView extends Component<Props> {
  componentWillMount() {
    this.props.listPassIssuers();
  }

  render() {
    const {
      passIssuers,
      classes,
      listPassIssuers,
      nextOffset,
      searchPassIssuers,
    } = this.props;

    return (
      <div>
        <div className={classes.header}>
          <h1>Pass Issuers</h1>

          <div>
            <input
              onChange={e => searchPassIssuers(e.target.value)}
              value={passIssuers.ui.searchPassIssuersQuery}
              placeholder="Search pass issuers..."
            />
            {' '}
            <Button primary onClick={() => history.push('/pass-issuers/add')}>
              Add new Pass Issuer
            </Button>
          </div>
        </div>

        <InfiniteScroll
          loading={passIssuers.loading}
          loader={loader}
          hasMore={passIssuers.hasMore}
          loadMore={() => listPassIssuers({ offset: nextOffset })}
        >
          <Table
            keyField="code"
            data={passIssuers.list}
            columns={columns}
            remote={{ sort: true }}
            onTableChange={tableChangeHandler(listPassIssuers)}
          />
        </InfiniteScroll>
      </div>
    );
  }
}

const mapStateToProps = (state: State) => {
  const {
    passIssuers,
  } = state;

  const { limit, offset } = passIssuers.listQueryParams;

  return {
    passIssuers: {
      ...passIssuers,
      list: passIssuerListSelector(state),
    },
    nextOffset: offset + limit,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...passIssuerActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(PassIssuerListView));
