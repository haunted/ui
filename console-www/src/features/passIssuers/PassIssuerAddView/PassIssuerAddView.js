// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import PassIssuerForm from '../forms/PassIssuerForm';

import * as passIssuerActions from '../redux/actions';

type Props = {
  loading: boolean,
  formLoading: boolean,
  addPassIssuer: Function,
};

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

class PassIssuerAddView extends Component<Props> {
  render() {
    const {
      loading,
      addPassIssuer,
      formLoading,
    } = this.props;

    if (loading) {
      return loader;
    }

    return (
      <div>
        <h1>Add Pass Issuer</h1>
        <PassIssuerForm
          onSubmit={addPassIssuer}
          formLoading={formLoading}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...passIssuerActions,
}, dispatch);

const mapStateToProps = (state) => {
  const { passIssuers } = state;

  return {
    formLoading: passIssuers.formLoading,
    loading: passIssuers.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PassIssuerAddView);
