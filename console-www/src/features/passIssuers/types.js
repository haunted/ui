// @flow

/**
 *  Base type
 */
import type { ListQueryParams } from '../../common/types';

export type PassIssuer = {
  code: string,
  name: string,
  country: string,
  state: string,
  city: string,
  id: string,
  billingCode: string,
  status: string,
  email: string,
  created: string,
};

export type PassIssuerFormData = {
  password: string,
  passwordConfirmation: string,
  lastName: string,
  firstName: string,
} & PassIssuer;


/**
 * Helper types
 */
export type Entities = { [code: string]: PassIssuer };
export type Keys = Array<string>;


/**
 * State type
 */
export type State = {
  entities: Entities,
  keys: Keys,
  loading: boolean,
  hasMore: boolean,
  formLoading: boolean,
  listQueryParams: ListQueryParams,
  ui: {
    searchPassIssuersQuery: string,
  },
};


/**
 * Response types
 */
export type PassIssuerListResponse = { passIssuers: Array<PassIssuer> };


/**
 * Action types
 */
type ListPassIssuers = 'passIssuers/listPassIssuers';
type ListPassIssuersRequest = 'passIssuers/listPassIssuersRequest';
type ListPassIssuersSuccess = 'passIssuers/listPassIssuersSuccess';
type ListPassIssuersFailure = 'passIssuers/listPassIssuersFailure';
type GetPassIssuer = 'passIssuers/getPassIssuer';
type GetPassIssuerRequest = 'passIssuers/getPassIssuerRequest';
type GetPassIssuerSuccess = 'passIssuers/getPassIssuerSuccess';
type GetPassIssuerFailure = 'passIssuers/getPassIssuerFailure';
type EditPassIssuer = 'passIssuers/editPassIssuer';
type EditPassIssuerRequest = 'passIssuers/editPassIssuerRequest';
type EditPassIssuerSuccess = 'passIssuers/editPassIssuerSuccess';
type EditPassIssuerFailure = 'passIssuers/editPassIssuerFailure';
type AddPassIssuer = 'passIssuers/addPassIssuer';
type AddPassIssuerRequest = 'passIssuers/addPassIssuerRequest';
type AddPassIssuerSuccess = 'passIssuers/addPassIssuerSuccess';
type AddPassIssuerFailure = 'passIssuers/addPassIssuerFailure';
type SearchPassIssuersT = 'passIssuers/searchPassIssuers';

export type GetPassIssuerAction = {
  type: GetPassIssuer,
  code: string,
};

export type EditAction = {
  type: EditPassIssuer,
  passIssuerData: PassIssuer,
};

export type AddAction = {
  type: AddPassIssuer,
  passIssuerData: PassIssuerFormData,
};

export type ListPassIssuersAction = {
  type: ListPassIssuers,
  params: $Shape<ListQueryParams>,
};

export type SearchPassIssuersActionT = {
  type: SearchPassIssuersT,
  query: string,
};

export type ActionType =
  | ListPassIssuers
  | ListPassIssuersRequest
  | ListPassIssuersSuccess
  | ListPassIssuersFailure
  | GetPassIssuer
  | GetPassIssuerRequest
  | GetPassIssuerSuccess
  | GetPassIssuerFailure
  | EditPassIssuer
  | EditPassIssuerRequest
  | EditPassIssuerSuccess
  | EditPassIssuerFailure
  | AddPassIssuer
  | AddPassIssuerRequest
  | AddPassIssuerSuccess
  | AddPassIssuerFailure
  | SearchPassIssuersT;

export type Action =
  | { type: ListPassIssuers }
  | { type: ListPassIssuersRequest }
  | {
      type: ListPassIssuersSuccess,
      entities: Entities,
      keys: Keys,
      listQueryParams: ListQueryParams,
    }
  | { type: ListPassIssuersFailure }
  | GetPassIssuerAction
  | { type: GetPassIssuerRequest }
  | {
      type: GetPassIssuerSuccess,
      entities: Entities,
      key: string,
    }
  | { type: GetPassIssuerFailure }
  | EditAction
  | { type: EditPassIssuerRequest }
  | {
      type: EditPassIssuerSuccess,
      data: PassIssuer,
    }
  | { type: EditPassIssuerFailure }
  | AddAction
  | { type: AddPassIssuerRequest }
  | {
      type: AddPassIssuerSuccess,
      data: PassIssuer,
    }
  | { type: AddPassIssuerFailure }
  | {
      type: SearchPassIssuersT,
      query: string,
    };


/**
 * Action creators
 */
export { SearchPassIssuersT } from './redux/searchPassIsuers';
export { ListPassIssuersT } from './redux/listPassIssuers';
