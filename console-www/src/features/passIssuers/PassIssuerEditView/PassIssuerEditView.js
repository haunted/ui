// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import type { Match } from 'react-router-dom';
import PassIssuerForm from '../forms/PassIssuerForm';

import type { PassIssuer } from '../types';
import * as passIssuerActions from '../redux/actions';

type Props = {
  passIssuer: PassIssuer,
  loading: boolean,
  formLoading: boolean,
  match: Match,
  getPassIssuer: Function,
  editPassIssuer: Function,
};

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

class PassIssuerEditView extends Component<Props> {
  componentWillMount() {
    const { passIssuer, getPassIssuer, match } = this.props;

    if (!passIssuer) {
      getPassIssuer(match.params.code);
    }
  }

  render() {
    const {
      passIssuer,
      loading,
      editPassIssuer,
      formLoading,
    } = this.props;

    if (loading) {
      return loader;
    }

    return (
      <div>
        <h1>Edit Pass Issuer</h1>
        <PassIssuerForm
          onSubmit={editPassIssuer}
          initialValues={passIssuer}
          formLoading={formLoading}
          edit
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...passIssuerActions,
}, dispatch);

const mapStateToProps = (state, props) => {
  const { passIssuers } = state;

  const { match } = props;

  return {
    passIssuer: passIssuers.entities[match.params.code],
    loading: passIssuers.loading,
    formLoading: passIssuers.formLoading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PassIssuerEditView);
