// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import { NotificationManager } from 'react-notifications';
import type { Saga, Effect } from 'redux-saga';
import type { Location } from 'react-router-dom';
import { auth } from '../../../utils/apis';
import history from '../../../common/history';
import type { State, LoginData, Action, ActionType, ActionLogin } from '../types';

export function login(loginData: LoginData, location: ?Location): Action {
  return {
    type: 'auth/login',
    loginData,
    location,
  };
}

export function* doLogin({ loginData, location }: ActionLogin): Saga<void> {
  yield put(({
    type: 'auth/loginRequest',
  }: Action));

  try {
    yield call(auth.login, loginData);
  } catch (err) {
    yield put(({
      type: 'auth/loginFailure',
    }: Action));

    yield call([NotificationManager, NotificationManager.error], 'Failed to login');

    return;
  }

  yield put(({
    type: 'auth/loginSuccess',
  }: Action));

  yield call(history.push, location || { pathname: '/suppliers' });
}

export function* watchLogin(): Iterable<Effect> {
  yield takeLatest(('auth/login': ActionType), doLogin);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'auth/loginRequest':
      return {
        ...state,
        loading: true,
      };

    case 'auth/loginSuccess':
      return {
        ...state,
        loading: false,
      };

    case 'auth/loginFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
