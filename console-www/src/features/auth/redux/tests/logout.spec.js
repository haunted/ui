import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { NotificationManager } from 'react-notifications';
import { doLogout } from '../logout';
import history from '../../../../common/history';
import { auth } from '../../../../utils/apis';


describe('Logout Saga', () => {
  const err = new Error('error');

  it('should go through logout process correctly', () =>
    expectSaga(doLogout)
      .provide([
        [matchers.call.fn(auth.logout)],
      ])
      .put({ type: 'auth/logoutRequest' })
      .put({ type: 'auth/logoutSuccess' })
      .call(
        [NotificationManager, NotificationManager.success],
        'You have successfully logged out',
      )
      .call(history.push, '/login')
      .run());

  it('should go through logout process correctly when error', () =>
    expectSaga(doLogout)
      .provide([
        [matchers.call.fn(auth.logout), throwError(err)],
      ])
      .put({ type: 'auth/logoutRequest' })
      .put({ type: 'auth/logoutFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to logout')
      .run());
});
