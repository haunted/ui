import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doLogin, reducer } from '../login';
import { auth } from '../../../../utils/apis';
import history from '../../../../common/history';

describe('Login Saga', () => {
  const loginData = {
    email: 'test@redeam.com',
    password: 'testPass123',
  };

  const loginDataWithLocation = {
    ...loginData,
    location: {
      pathname: '/devices',
    },
  };

  const err = new Error('error');

  it('should go through login process correctly', () => expectSaga(doLogin, loginData)
    .provide([
      [matchers.call.fn(auth.login), null],
    ])
    .put({ type: 'auth/loginRequest' })
    .put({ type: 'auth/loginSuccess' })
    .call(history.push, { pathname: '/suppliers' })
    .run());

  it('should pass location state correctly', () => expectSaga(doLogin, loginDataWithLocation)
    .provide([
      [matchers.call.fn(auth.login), null],
    ])
    .put({ type: 'auth/loginRequest' })
    .put({ type: 'auth/loginSuccess' })
    .call(history.push, loginDataWithLocation.location)
    .run());

  it('should show notification when login fails', () => expectSaga(doLogin, loginData)
    .provide([
      [matchers.call.fn(auth.login), throwError(err)],
    ])
    .put({ type: 'auth/loginRequest' })
    .put({ type: 'auth/loginFailure' })
    .call([NotificationManager, NotificationManager.error], 'Failed to login')
    .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'auth/loginRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` when request successes', () => {
    function* saga() {
      yield put({ type: 'auth/loginSuccess' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'auth/loginFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });
});
