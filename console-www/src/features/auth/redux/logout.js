// @flow

import type { Saga, Effect } from 'redux-saga';
import { takeLatest, call, put } from 'redux-saga/effects';
import { NotificationManager } from 'react-notifications';
import { auth } from '../../../utils/apis';
import history from '../../../common/history';
import type { State, Action } from '../types';

export function logout(): Action {
  return {
    type: 'auth/logout',
  };
}

export function* doLogout(): Saga<void> {
  yield put(({
    type: 'auth/logoutRequest',
  }: Action));

  try {
    yield call(auth.logout);

    yield put(({
      type: 'auth/logoutSuccess',
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.success],
      'You have successfully logged out',
    );

    yield call(history.push, '/login');
  } catch (err) {
    yield put(({
      type: 'auth/logoutFailure',
    }: Action));
    yield call([NotificationManager, NotificationManager.error], 'Failed to logout');
  }
}

export function* watchLogout(): Iterable<Effect> {
  yield takeLatest('auth/logout', doLogout);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    default:
      return state;
  }
}
