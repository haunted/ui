// @flow

const styles = {
  authContainer: {
    position: 'fixed',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    background: 'white',
    color: 'black',
  },
};

export type StylesT = { [$Keys<typeof styles>]: string };
export default styles;
