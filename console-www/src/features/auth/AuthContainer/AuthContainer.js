// @flow

import * as React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import type { State } from '../../../common/types';
import * as userActions from '../../users/redux/actions';
import type { GetSelfUserT } from '../../users/types';
import styles, { type StylesT } from './AuthContainer.jss';


type PropsT = {
  getSelfUser: GetSelfUserT,
  children?: React.Node,
  meLoaded: boolean,
  classes: StylesT,
};

class AuthContainer extends React.Component<PropsT> {
  componentWillMount() {
    this.props.getSelfUser();
  }

  render() {
    const {
      children,
      meLoaded,
      classes,
    } = this.props;

    return !meLoaded ? (
      <div className={classes.authContainer}>Loading console...</div>
    ) : children;
  }
}

const mapStateToProps = (state: State) => {
  const { me } = state.users;

  return {
    meLoaded: !!me,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...userActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(AuthContainer));
