import backgroundImage from '../../../images/login-bg.jpg';

const styles = theme => ({
  view: {
    background: `url(${backgroundImage}) center center / cover`,
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 1300,
    minHeight: '100vh',
    margin: [0, 'auto'],
  },
  content: {
    display: 'flex',
    borderRadius: 4,
    background: 'white',
    boxShadow: [0, 0, 10, 0, 'rgba(0, 0, 0, .5)'],
  },
  form: {
    width: 350,
    padding: 24,
  },
  info: {
    width: 200,
    padding: 24,
    background: theme.colors.primary,
  },
  logo: {
    width: 160,
  },
});

export default styles;
