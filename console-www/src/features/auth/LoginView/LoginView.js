// @flow

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import type { Location } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import { bindActionCreators } from 'redux';
import type { Dispatch } from 'redux';
import get from 'lodash.get';
import * as authActions from '../redux/actions';
import LoginForm from '../forms/LoginForm';
import type { State as AuthState, LoginData } from '../types';
import type { State } from '../../../common/types';
import styles from './LoginView.jss';
import logoImage from '../../../images/logo.svg';

type Props = {
  classes: Object,
  login: (data: LoginData, location: ?Location) => any,
  auth: AuthState,
  location: Location,
};

class LoginView extends Component<Props> {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired,
  };

  render() {
    const {
      classes,
      login,
      location,
      auth,
    } = this.props;

    return (
      <div className={classes.view}>
        <div className={classes.container}>
          <div className={classes.content}>
            <div className={classes.form}>
              <img src={logoImage} alt="Redeam" className={classes.logo} />

              <LoginForm
                onSubmit={data => login(data, get(location, 'state.referrer'))}
                loading={auth.loading}
              />
            </div>
            <div className={classes.info} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: State) => {
  const { auth } = state;

  return {
    auth,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...authActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(LoginView));
