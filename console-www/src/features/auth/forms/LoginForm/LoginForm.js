// @flow

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import type { FormProps } from 'redux-form';
import isEmail from 'validator/lib/isEmail';
import Button from 'redeam-styleguide/lib/Button';
import { TextField } from 'redeam-styleguide/lib/formElements';
import type { LoginData } from '../../types';

type Props = FormProps & {
  loading: boolean,
};

class LoginForm extends Component<Props> {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
  };

  render() {
    const {
      handleSubmit,
      loading,
    } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        <Field
          name="email"
          label="Email"
          component={TextField}
          disabled={loading}
        />

        <Field
          name="password"
          label="Password"
          component={TextField}
          type="password"
          disabled={loading}
        />

        <Button type="submit" primary disabled={loading}>Login</Button>

        {loading && (
          <span>loading...</span>
        )}
      </form>
    );
  }
}

export const validate = (values: LoginData) => {
  const errors = {};

  if (!values.email.trim()) {
    errors.email = 'Email cannot be empty';
  } else if (!isEmail(values.email)) {
    errors.email = 'Email is not correct';
  }

  if (!values.password.trim()) {
    errors.password = 'Password cannot be empty';
  }

  return errors;
};

export const initialValues = {
  email: '',
  password: '',
};

export default reduxForm({
  form: 'loginForm',
  initialValues,
  validate,
})(LoginForm);
