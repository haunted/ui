import { validate, initialValues } from './LoginForm';

describe('<LoginForm />', () => {
  describe('Login form validation', () => {
    it('should return errors if fields are empty', () => {
      expect(Object.keys(validate(initialValues)).sort())
        .toEqual(['email', 'password']);
    });

    it('should display error if email is not correct', () => {
      const values = {
        ...initialValues,
        email: 'not--valid-email',
        password: 'testPass',
      };

      expect(Object.keys(validate(values)).sort())
        .toEqual(['email']);
    });
  });
});
