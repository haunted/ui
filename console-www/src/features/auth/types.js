// @flow

import type { Location } from 'react-router-dom';

/**
 * Helper types
 */
export type LoginData = {
  email: string,
  password: string,
};


/**
 * State type
 */
export type State = {
  loading: boolean,
};


/**
 * Action types
 */
type Login = 'auth/login';
type LoginRequest = 'auth/loginRequest';
type LoginSuccess = 'auth/loginSuccess';
type LoginFailure = 'auth/loginFailure';
type Logout = 'auth/logout';
type LogoutRequest = 'auth/logoutRequest';
type LogoutSuccess = 'auth/logoutSuccess';
type LogoutFailure = 'auth/logoutFailure';

export type ActionType =
  | Login
  | LoginRequest
  | LoginSuccess
  | LoginFailure
  | Logout
  | LogoutRequest
  | LogoutSuccess
  | LogoutFailure;

export type ActionLogin = {
  type: Login,
  loginData: LoginData,
  location: ?Location,
};

export type Action =
  | ActionLogin
  | { type: LoginRequest }
  | { type: LoginSuccess }
  | { type: LoginFailure }
  | { type: Logout }
  | { type: LogoutRequest }
  | { type: LogoutSuccess }
  | { type: LogoutFailure };
