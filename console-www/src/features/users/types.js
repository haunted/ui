// @flow

import type { ListQueryParams } from '../../common/types';


/**
 *  Base type
 */
export type User = {
  id: string,
  lastName: string,
  firstName: string,
  email: string,
  password: string,
  newPassword?: string,
  newPasswordConfirmation?: string,
};


/**
 * Helper types
 */
export type Entities = { [id: string]: User };
export type Keys = Array<string>;


/**
 * State type
 */
export type State = {
  entities: Entities,
  keys: Keys,
  me: ?string,
  loading: boolean,
  formLoading: boolean,
  hasMore: boolean,
  listQueryParams: ListQueryParams,
};


/**
 * Response types
 */
export type UsersListResponse = {accounts: Array<User>};


/**
 * Action types
 */
type ListUsers = 'users/listUsers';
type ListUsersRequest = 'users/listUsersRequest';
type ListUsersSuccess = 'users/listUsersSuccess';
type ListUsersFailure = 'users/listUsersFailure';
type GetSelfUser = 'users/getSelfUser';
type GetSelfUserRequest = 'users/getSelfUserRequest';
type GetSelfUserSuccess = 'users/getSelfUserSuccess';
type GetSelfUserFailure = 'users/getSelfUserFailure';
type EditSelfUser = 'users/editSelfUser';
type EditSelfUserRequest = 'users/editSelfUserRequest';
type EditSelfUserSuccess = 'users/editSelfUserSuccess';
type EditSelfUserFailure = 'users/editSelfUserFailure';
type DeleteUser = 'users/deleteUser';
type DeleteUserRequest = 'users/deleteUserRequest';
type DeleteUserSuccess = 'users/deleteUserSuccess';
type DeleteUserFailure = 'users/deleteUserFailure';
type AddUser = 'users/addUser';
type AddUserRequest = 'users/addUserRequest';
type AddUserSuccess = 'users/addUserSuccess';
type AddUserFailure = 'users/addUserFailure';

export type ListUserActionT = {
  type: ListUsers,
  params: ListQueryParams,
};

export type DeleteUserAction = {
  type: DeleteUser,
  id: $PropertyType<User, 'id'>,
};

export type AddAction = {
  type: AddUser,
  userData: User,
};

export type GetSelfUserAction = {
  type: GetSelfUser,
};

export type EditAction = {
  type: EditSelfUser,
  userData: User,
};

export type ActionType =
  | ListUsers
  | ListUsersRequest
  | ListUsersSuccess
  | ListUsersFailure
  | GetSelfUser
  | GetSelfUserRequest
  | GetSelfUserSuccess
  | GetSelfUserFailure
  | EditSelfUser
  | EditSelfUserRequest
  | EditSelfUserSuccess
  | EditSelfUserFailure
  | DeleteUser
  | DeleteUserRequest
  | DeleteUserSuccess
  | DeleteUserFailure
  | AddUser
  | AddUserRequest
  | AddUserSuccess
  | AddUserFailure;

export type Action =
  | ListUserActionT
  | { type: ListUsersRequest }
  | {
      type: ListUsersSuccess,
      entities: Entities,
      keys: Keys,
      listQueryParams: ListQueryParams,
    }
  | { type: ListUsersFailure }
  | GetSelfUserAction
  | { type: GetSelfUserRequest }
  | {
      type: GetSelfUserSuccess,
      entities: Entities,
      key: string,
    }
  | { type: GetSelfUserFailure }
  | EditAction
  | { type: EditSelfUserRequest }
  | {
      type: EditSelfUserSuccess,
      data: User,
    }
  | { type: EditSelfUserFailure }
  | DeleteUserAction
  | { type: DeleteUserRequest }
  | {
      type: DeleteUserSuccess,
      id: string,
    }
  | { type: DeleteUserFailure }
  | AddAction
  | { type: AddUserRequest }
  | {
      type: AddUserSuccess,
      data: User,
    }
  | { type: AddUserFailure };


/**
 * Action creator types
 */
export { AddUserT } from './redux/addUser';
export { ListUsersT } from './redux/listUsers';
export { DeleteUserT } from './redux/deleteUser';
export { GetSelfUserT } from './redux/getSelfUser';
export { EditSelfUserT } from './redux/editSelfUser';
