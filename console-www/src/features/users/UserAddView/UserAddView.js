// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import UserForm from '../forms/UserForm';

import * as userActions from '../redux/actions';
import type { AddUserT } from '../types';

type Props = {
  loading: boolean,
  formLoading: boolean,
  addUser: AddUserT,
};

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

class UserAddView extends Component<Props> {
  render() {
    const {
      loading,
      addUser,
      formLoading,
    } = this.props;

    if (loading) {
      return loader;
    }

    return (
      <div>
        <h1>Add Staff Member</h1>
        <UserForm
          onSubmit={addUser}
          formLoading={formLoading}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...userActions,
}, dispatch);

const mapStateToProps = (state) => {
  const { users } = state;

  return {
    formLoading: users.formLoading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserAddView);
