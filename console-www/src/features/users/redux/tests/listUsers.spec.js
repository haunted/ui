import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { normalize, schema } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doListUsers, reducer } from '../listUsers';
import { users } from '../../../../utils/apis';
import usersMock from '../../../../mocks/users.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import initialState from '../initialState';

const userSchema = new schema.Entity('users');
const userListSchema = [userSchema];


describe('List Users Saga', () => {
  const err = new Error('error');
  const normalizedData = normalize(
    usersMock.accounts,
    userListSchema,
  );

  it('should go through list users process correctly', () => expectSaga(doListUsers, {})
    .withState({ users: initialState })
    .provide([
      [matchers.call.fn(users.listUsers), wrapValidResponse(usersMock)],
    ])
    .put({ type: 'users/listUsersRequest' })
    .put({
      type: 'users/listUsersSuccess',
      entities: normalizedData.entities.users,
      keys: normalizedData.result,
      listQueryParams: initialState.listQueryParams,
    })
    .run());

  it('should show notification when list users fails', () => expectSaga(doListUsers, {})
    .withState({ users: initialState })
    .provide([
      [matchers.call.fn(users.listUsers), throwError(err)],
    ])
    .put({ type: 'users/listUsersRequest' })
    .put({ type: 'users/listUsersFailure' })
    .call([NotificationManager, NotificationManager.error], 'Failed to fetch staff members')
    .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'users/listUsersRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` when request successes', () => {
    function* saga() {
      yield put({
        type: 'users/listUsersSuccess',
        entities: {},
        keys: [],
        listQueryParams: {},
      });
    }

    const state = {
      loading: false,
      entities: {},
      keys: [],
      listQueryParams: {},
    };

    return expectSaga(saga)
      .withReducer(reducer, state)
      .hasFinalState({
        loading: false,
        entities: {},
        keys: [],
        listQueryParams: {},
        hasMore: false,
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'users/listUsersFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });
});
