/* eslint-disable max-len */

import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doAddUser, reducer } from '../addUser';
import { users } from '../../../../utils/apis';
import usersMock from '../../../../mocks/users.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import history from '../../../../common/history';

const user = usersMock.accounts[0];

const userData = user;

const id = 'ID';

const initialState = {
  formLoading: false,
  entities: {},
  keys: [],
};

describe('Add User Saga', () => {
  const err = new Error('error');

  it('should go through add user process correctly', () =>
    expectSaga(doAddUser, { userData })
      .provide([
        [
          matchers.call.fn(users.addUser),
          wrapValidResponse(userData),
        ],
      ])
      .put({ type: 'users/addUserRequest' })
      .put({
        type: 'users/addUserSuccess',
        data: userData,
      })
      .call(history.push, '/users')
      .call([NotificationManager, NotificationManager.success], 'Staff member created successfully')
      .run());

  it('should show notification when add user fails', () =>
    expectSaga(doAddUser, { userData })
      .provide([
        [matchers.call.fn(users.addUser), throwError(err)],
      ])
      .put({ type: 'users/addUserRequest' })
      .put({ type: 'users/addUserFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to create staff member')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'users/addUserRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'users/addUserSuccess',
        data: { ...userData, id },
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        formLoading: false,
        entities: {
          [id]: { ...userData, id },
        },
        keys: [id],
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'users/addUserFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: false,
      })
      .run();
  });
});
