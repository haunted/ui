import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { normalize, schema } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doDeleteUser, reducer } from '../deleteUser';
import usersMock from '../../../../mocks/users.json';
import { users } from '../../../../utils/apis';

const userSchema = new schema.Entity('users');
const userListSchema = [userSchema];

const normalizedData = normalize(
  usersMock.accounts,
  userListSchema,
);

const id = 'SM1';
const email = 'sm1@redeam.com';

describe('Delete user Saga', () => {
  const err = new Error('error');

  it('should go through delete user process correctly if user accept action', () =>
    expectSaga(doDeleteUser, { id, email })
      .provide([
        [
          matchers.call.fn(users.deleteUser),
          {},
        ],
      ])
      .dispatch({ type: 'common/confirmAccept' })
      .put({ type: 'users/deleteUserRequest' })
      .put({
        type: 'users/deleteUserSuccess',
        id,
      })
      .call([NotificationManager, NotificationManager.success], 'Staff member deleted successfully')
      .run());

  it('should go through delete user process correctly if user decline action', () =>
    expectSaga(doDeleteUser, { id, email })
      .dispatch({ type: 'common/confirmCancel' })
      .run());

  it('should show notification when delete user fails', () =>
    expectSaga(doDeleteUser, { id, email })
      .provide([
        [matchers.call.fn(users.deleteUser), throwError(err)],
      ])
      .put({ type: 'users/deleteUserRequest' })
      .dispatch({ type: 'common/confirmAccept' })
      .put({ type: 'users/deleteUserFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to delete staff member')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'users/deleteUserRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer, {})
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'users/deleteUserSuccess',
        id,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, {
        entities: normalizedData.entities.users,
        keys: normalizedData.result,
      })
      .hasFinalState({
        entities:
          {
            SM2:
              {
                id: 'SM2',
                email: 'sm2@redeam.com',
                firstName: 'SM2Name',
                lastName: 'SM2LastName',
              },
            SM3:
              {
                id: 'SM3',
                email: 'sm3@redeam.com',
                firstName: 'SM3Name',
                lastName: 'SM3LastName',
              },
          },
        keys: ['SM2', 'SM3'],
        loading: false,
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'users/deleteUserFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer, {
        entities: normalizedData.entities.users,
        keys: normalizedData.result,
      })
      .hasFinalState({
        entities: normalizedData.entities.users,
        keys: normalizedData.result,
        loading: false,
      })
      .run();
  });
});
