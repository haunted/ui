// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { users } from '../../../utils/apis';
import type { ActionType, Action, State, EditAction, User } from '../types';
import history from '../../../common/history';

export function editSelfUser(userData: User): Action {
  return {
    type: 'users/editSelfUser',
    userData,
  };
}

export type EditSelfUserT = typeof editSelfUser;

export function* doEditSelfUser({ userData }: EditAction): Saga<void> {
  yield put(({
    type: 'users/editSelfUserRequest',
  }: Action));

  try {
    const { data } = yield call(users.editSelfUser, userData);

    yield put(({
      type: 'users/editSelfUserSuccess',
      data,
    }: Action));

    yield call(history.push, '/suppliers');
    yield call(
      [NotificationManager, NotificationManager.success],
      'User edited successfully',
    );
  } catch (e) {
    yield put(({
      type: 'users/editSelfUserFailure',
    }: Action));
    yield call([NotificationManager, NotificationManager.error], 'Failed to edit user');
  }
}

export function* watchEditSelfUser(): Iterable<Effect> {
  yield takeLatest(('users/editSelfUser': ActionType), doEditSelfUser);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'users/editSelfUserRequest':
      return {
        ...state,
        formLoading: true,
      };

    case 'users/editSelfUserSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.id]: action.data,
        },
        formLoading: false,
      };

    case 'users/editSelfUserFailure':
      return {
        ...state,
        formLoading: false,
      };

    default:
      return state;
  }
}
