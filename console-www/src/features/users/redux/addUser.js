// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import uniq from 'lodash.uniq';
import { users } from '../../../utils/apis';
import type { ActionType, Action, State, AddAction, User } from '../types';
import history from '../../../common/history';

export function addUser(userData: User): Action {
  return {
    type: 'users/addUser',
    userData,
  };
}

export type AddUserT = typeof addUser;

export function* doAddUser({ userData }: AddAction): Saga<void> {
  yield put(({
    type: 'users/addUserRequest',
  }: Action));

  try {
    const { data } = yield call(users.addUser, userData);

    yield put(({
      type: 'users/addUserSuccess',
      data,
    }: Action));

    yield call(history.push, '/users');
    yield call(
      [NotificationManager, NotificationManager.success],
      'Staff member created successfully',
    );
  } catch (e) {
    yield put(({
      type: 'users/addUserFailure',
    }: Action));
    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to create staff member',
    );
  }
}

export function* watchAddUser(): Iterable<Effect> {
  yield takeLatest(('users/addUser': ActionType), doAddUser);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'users/addUserRequest':
      return {
        ...state,
        formLoading: true,
      };

    case 'users/addUserSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.id]: action.data,
        },
        keys: uniq([...state.keys, action.data.id]),
        formLoading: false,
      };

    case 'users/addUserFailure':
      return {
        ...state,
        formLoading: false,
      };

    default:
      return state;
  }
}
