// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import uniq from 'lodash.uniq';
import { normalize } from 'normalizr';
import { users } from '../../../utils/apis';
import type { ActionType, Action, User, State as UserState } from '../types';
import type { AxiosResponse, NormalizedSingle } from '../../../common/types';
import { userSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';


export function getSelfUser(): Action {
  return {
    type: 'users/getSelfUser',
  };
}

export type GetSelfUserT = typeof getSelfUser;

export function* doGetSelfUser(): Saga<void> {
  yield put(({
    type: 'users/getSelfUserRequest',
  }: Action));

  try {
    const response: AxiosResponse<User> =
      yield call(users.getSelfUser);

    const normalizedData: NormalizedSingle<User, 'users'> = normalize(
      response.data,
      userSchema,
    );

    yield put(({
      type: 'users/getSelfUserSuccess',
      entities: normalizedData.entities.users || {},
      key: normalizedData.result,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'users/getSelfUserFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to fetch user data');
    }
  }
}

export function* watchGetSelfUser(): Iterable<Effect> {
  yield takeLatest(('users/getSelfUser': ActionType), doGetSelfUser);
}

export function reducer(state: UserState, action: Action): UserState {
  switch (action.type) {
    case 'users/getSelfUserRequest':
      return {
        ...state,
        lading: true,
      };

    case 'users/getSelfUserSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
        keys: uniq([...state.keys, action.key]),
        me: action.key,
        lading: false,
      };

    case 'users/getSelfUserFailure':
      return {
        ...state,
        lading: false,
      };

    default:
      return state;
  }
}
