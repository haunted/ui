import initialState from './initialState';
import { reducer as listUsers } from './listUsers';
import { reducer as addPassIssuer } from './addUser';
import { reducer as deleteUser } from './deleteUser';
import { reducer as getSelfUser } from './getSelfUser';
import { reducer as editSelfUser } from './editSelfUser';

const reducers = [
  listUsers,
  addPassIssuer,
  deleteUser,
  getSelfUser,
  editSelfUser,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    default:
      newState = state;
      break;
  }

  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
