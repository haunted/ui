// @flow

import { takeLatest, call, put, select } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import { users } from '../../../utils/apis';
import type { ActionType, Action, User, UsersListResponse, State, ListUserActionT } from '../types';
import type {
  AxiosResponse,
  ListQueryParams,
  Normalized,
  State as GlobalState,
} from '../../../common/types';
import { userListSchema } from '../../../common/schemas';
import { mergeListResponse, mergeQueryParams } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';


export function listUsers(params: $Shape<ListQueryParams> = {}): Action {
  return {
    type: 'users/listUsers',
    params,
  };
}

export type ListUsersT = typeof listUsers;

export function* doListUsers({ params }: ListUserActionT): Saga<void> {
  const prevParams = yield select((state: GlobalState) => state.users.listQueryParams);
  const listQueryParams = mergeQueryParams(prevParams, params);

  yield put(({
    type: 'users/listUsersRequest',
  }: Action));

  try {
    const response: AxiosResponse<UsersListResponse> =
      yield call(users.listUsers, listQueryParams);

    const normalizedData: Normalized<User, 'users'> = normalize(
      response.data.accounts,
      userListSchema,
    );

    yield put(({
      type: 'users/listUsersSuccess',
      entities: normalizedData.entities.users || {},
      keys: normalizedData.result,
      listQueryParams,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'users/listUsersFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to fetch staff members');
    }
  }
}

export function* watchListUsers(): Iterable<Effect> {
  yield takeLatest(('users/listUsers': ActionType), doListUsers);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'users/listUsersRequest':
      return {
        ...state,
        loading: true,
      };

    case 'users/listUsersSuccess':
      return {
        ...state,
        loading: false,
        ...mergeListResponse(state, action),
      };

    case 'users/listUsersFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
