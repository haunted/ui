// @flow

import type { State } from '../types';

const initialState: State = {
  entities: {},
  keys: [],
  me: null,
  loading: false,
  formLoading: false,
  hasMore: true,
  listQueryParams: {
    limit: 50,
    offset: 0,
    sort: 'name',
    search: '',
  },
};

export default initialState;
