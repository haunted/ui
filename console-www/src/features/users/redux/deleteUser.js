// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { users } from '../../../utils/apis';
import { confirm } from '../../common/redux/actions';
import type {
  ActionType,
  Action,
  State,
  DeleteUserAction,
  User,
} from '../types';


export function deleteUser(id: $PropertyType<User, 'id'>): Action {
  return {
    type: 'users/deleteUser',
    id,
  };
}

export type DeleteUserT = typeof deleteUser;

export function* doDeleteUser({ id }: DeleteUserAction): Saga<void> {
  if ((yield confirm('Are you sure you want to delete this user?')) === false) {
    return;
  }

  yield put(({
    type: 'users/deleteUserRequest',
  }: Action));

  try {
    yield call(users.deleteUser, id);

    yield put(({
      type: 'users/deleteUserSuccess',
      id,
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.success],
      'Staff member deleted successfully',
    );
  } catch (e) {
    yield put(({
      type: 'users/deleteUserFailure',
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to delete staff member',
    );
  }
}

export function* watchDeleteUser(): Iterable<Effect> {
  yield takeLatest(('users/deleteUser': ActionType), doDeleteUser);
}

export function reducer(state: State, action: Action): State {
  const entities = { ...state.entities };

  switch (action.type) {
    case 'users/deleteUserRequest':
      return {
        ...state,
        loading: true,
      };

    case 'users/deleteUserSuccess': {
      const { id } = action;

      delete entities[action.id];

      return {
        ...state,
        entities,
        keys: state.keys.filter(key => key !== id),
        loading: false,
      };
    }

    case 'users/deleteUserFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
