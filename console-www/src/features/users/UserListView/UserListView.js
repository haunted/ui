// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import InfiniteScroll from 'redeam-styleguide/lib/InfiniteScroll';
import Table from 'redeam-styleguide/lib/Table';
import Button from 'redeam-styleguide/lib/Button';
import * as userActions from '../redux/actions';
import type { State as UserState, ListUsersT, DeleteUserT } from '../types';
import type { State } from '../../../common/types';
import history from '../../../common/history';
import styles, { type Styles } from './UserListView.jss';


const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

type Props = {
  listUsers: ListUsersT,
  deleteUser: DeleteUserT,
  users: UserState,
  nextOffset: number,
  classes: Styles,
};

class UserListView extends Component<Props> {
  componentWillMount() {
    this.props.listUsers({ offset: 0 });
  }

  getColumns() {
    const { users } = this.props;

    return [
      {
        text: 'Email address',
        dataField: 'email',
      },
      {
        text: 'First name',
        dataField: 'firstName',
      },
      {
        text: 'Last name',
        dataField: 'lastName',
      },
      {
        dataField: 'actions',
        text: 'Actions',
        formatter: (e, { id }) => (
          <div>
            {users.me !== id && (
              <button onClick={() => this.props.deleteUser(id)}>Delete</button>
            )}
          </div>
        ),
      },
    ];
  }

  render() {
    const {
      users,
      classes,
      listUsers,
      nextOffset,
    } = this.props;

    return (
      <div>
        <div className={classes.header}>
          <h1>Staff Members</h1>

          <Button primary onClick={() => history.push('/users/add')}>
            Add new user
          </Button>
        </div>

        <InfiniteScroll
          loading={users.loading}
          loader={loader}
          hasMore={users.hasMore}
          loadMore={() => listUsers({ offset: nextOffset })}
        >
          <Table
            keyField="id"
            data={Object.values(users.entities)}
            columns={this.getColumns()}
          />
        </InfiniteScroll>
      </div>
    );
  }
}

const mapStateToProps = (state: State) => {
  const {
    users,
  } = state;

  const { limit, offset } = users.listQueryParams;

  return {
    users,
    nextOffset: offset + limit,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...userActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(UserListView));
