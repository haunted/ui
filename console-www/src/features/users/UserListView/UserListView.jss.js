// @flow

const styles = {
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
};

export type Styles = { [$Keys<typeof styles>]: string };
export default styles;
