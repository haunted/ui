/* eslint-disable max-len */
import { validate, initialValues } from './UserForm';

describe('<UserForm />', () => {
  describe('add form validation', () => {
    const props = { edit: false };

    it('should return errors if required values are empty', () => {
      expect(Object.keys(validate(initialValues, props))
        .sort())
        .toEqual(['firstName', 'lastName', 'email', 'password'].sort());
    });

    it('should return an validation error if `password` does not match `passwordConfirmation`', () => {
      const values = {
        ...initialValues,
        firstName: 'test',
        lastName: 'test',
        email: 'test@test.com',
        password: 'test',
        passwordConfirmation: 'wrong',
      };

      expect(Object.keys(validate(values, props))
        .sort())
        .toEqual(['passwordConfirmation'].sort());
    });

    it('should not return validation errors if all required fields provided', () => {
      const values = {
        ...initialValues,
        email: 'test@redeam.com',
        firstName: 'test first name',
        lastName: 'test last name',
        password: 'test pass',
        passwordConfirmation: 'test pass',
      };

      expect(Object.keys(validate(values, props)))
        .toEqual([]);
    });

    it('should not return validation errors if `password` matches `passwordConfirmation`', () => {
      const values = {
        ...initialValues,
        firstName: 'test first name',
        lastName: 'test last name',
        email: 'test@redeam.com',
        password: 'testPass',
        passwordConfirmation: 'testPass',
      };

      expect(Object.keys(validate(values, props)))
        .toEqual([]);
    });
  });

  describe('edit form validation', () => {
    const props = { edit: true };

    it('should not return validation errors if `newPassword` matches `newPasswordConfirmation`', () => {
      const values = {
        ...initialValues,
        firstName: 'test first name',
        lastName: 'test last name',
        password: 'testPass',
        newPassword: 'testPass1',
        newPasswordConfirmation: 'testPass1',
      };

      expect(Object.keys(validate(values, props)))
        .toEqual([]);
    });
  });
});
