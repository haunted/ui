// @flow

import React, { Component } from 'react';
import { reduxForm, Field, type FormProps } from 'redux-form';
import isEmail from 'validator/lib/isEmail';
import { withStyles } from 'redeam-styleguide/lib/utils';
import { TextField } from 'redeam-styleguide/lib/formElements';
import Button from 'redeam-styleguide/lib/Button';
import history from '../../../../common/history';
import styles, { type Styles } from './UserForm.jss';

type Props = {
  classes: Styles,
} & FormProps;

export const initialValues = {
  email: '',
  password: '',
  newPassword: '',
  passwordConfirmation: '',
  newPasswordConfirmation: '',
  firstName: '',
  lastName: '',
};


class UserForm extends Component<Props> {
  constructor(props) {
    super(props);

    (this: any).processSubmit = this.processSubmit.bind(this);
  }

  processSubmit(values: typeof initialValues) {
    const {
      edit,
      onSubmit,
    } = this.props;

    if (edit) {
      const { passwordConfirmation, ...data } = values;

      onSubmit(data);
    } else {
      const { passwordConfirmation, newPassword, ...data } = values;

      onSubmit(data);
    }
  }

  render() {
    const {
      classes,
      handleSubmit,
      formLoading,
      edit,
    } = this.props;

    return (
      <form onSubmit={handleSubmit(this.processSubmit)} className={classes.form} autoComplete="off">
        <div className={classes.formField}>
          <Field
            label="First Name"
            name="firstName"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formField}>
          <Field
            label="Last Name"
            name="lastName"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        {!edit &&
          <div className={classes.formField}>
            <Field
              label="Email"
              name="email"
              type="email"
              component={TextField}
              disabled={formLoading}
            />
          </div>
        }

        <div className={classes.formField}>
          <Field
            label={edit ? 'Current Password' : 'Password'}
            name="password"
            type="password"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        {edit && (
          <div className={classes.formField}>
            <Field
              label="New Password"
              name="newPassword"
              type="password"
              component={TextField}
              disabled={formLoading}
            />
          </div>
        )}

        <div className={classes.formField}>
          <Field
            label="Confirm Password"
            name={edit ? 'newPasswordConfirmation' : 'passwordConfirmation'}
            type="password"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formNavigation}>
          <Button
            tertiary
            onClick={() => history.push('/users')}
            className={classes.cancelButton}
          >
            Cancel
          </Button>

          <Button primary type="submit" disabled={formLoading}>
            {edit ? 'Save' : 'Add user'}
          </Button>
        </div>
      </form>
    );
  }
}

export const validate = (values: typeof initialValues, { edit }: Props) => {
  const errors: { [$Keys<typeof initialValues>]: any } = {};

  if (!values.firstName.trim()) {
    errors.firstName = 'First Name is required';
  }

  if (!values.lastName.trim()) {
    errors.lastName = 'Last Name is required';
  }

  if (edit && (values.password || values.newPassword || values.newPasswordConfirmation)) {
    if (!values.password) {
      errors.password = 'Password cannot be empty';
    }

    if (!values.newPassword) {
      errors.newPassword = 'New Password cannot be empty';
    } else if (values.newPassword !== values.newPasswordConfirmation) {
      errors.newPasswordConfirmation = 'Password don\'t match';
    }
  }

  if (!edit) {
    if (!values.email.trim()) {
      errors.email = 'Email cannot be empty';
    } else if (!isEmail(values.email)) {
      errors.email = 'Email is not correct';
    }

    if (!values.password) {
      errors.password = 'New Password cannot be empty';
    } else if (values.password !== values.passwordConfirmation) {
      errors.passwordConfirmation = 'Password don\'t match';
    }
  }

  return errors;
};

export default reduxForm({
  form: 'userForm',
  validate,
  initialValues,
})(withStyles(styles)(UserForm));
