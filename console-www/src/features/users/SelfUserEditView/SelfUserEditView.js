// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import UserForm from '../forms/UserForm';

import type { User, GetSelfUserT, EditSelfUserT } from '../types';
import * as usersActions from '../redux/actions';

type Props = {
  user: User,
  loading: boolean,
  formLoading: boolean,
  getSelfUser: GetSelfUserT,
  editSelfUser: EditSelfUserT,
};

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

class SelfUserEditView extends Component<Props> {
  componentDidMount() {
    const { user, getSelfUser } = this.props;

    if (!user) {
      getSelfUser();
    }
  }

  render() {
    const {
      user,
      loading,
      editSelfUser,
      formLoading,
    } = this.props;

    if (loading) {
      return loader;
    }

    return (
      <div>
        <h1>Edit My Profile</h1>
        <UserForm
          onSubmit={editSelfUser}
          initialValues={user}
          formLoading={formLoading}
          edit
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...usersActions,
}, dispatch);

const mapStateToProps = (state) => {
  const { users } = state;

  return {
    user: users.entities[users.me],
    loading: users.loading,
    formLoading: users.formLoading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SelfUserEditView);
