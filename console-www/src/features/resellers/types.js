// @flow

/**
 *  Base type
 */
import type { ListQueryParams } from '../../common/types';

export type Reseller = {
  code: string,
  name: string,
  aliases?: Array<string>,
  city: ?string,
  state: ?string,
  country: ?string,
  email: string,
};

export type ResellerFormData = {
  password: string,
  passwordConfirmation: string,
  lastName: string,
  firstName: string,
} & Reseller;


/**
 * Helper types
 */
type Entities = { [code: string]: Reseller };
type Keys = Array<string>;


/**
 * State type
 */
export type State = {
  entities: Entities,
  keys: Keys,
  loading: boolean,
  hasMore: boolean,
  formLoading: boolean,
  listQueryParams: ListQueryParams,
  ui: {
    searchResellersQuery: string,
  },
};


/**
 * Response types
 */
export type ResellerListResponse = { resellers: Array<Reseller> };


/**
 * Action types
 */
type ListResellers = 'resellers/listResellers';
type ListResellersRequest = 'resellers/listResellersRequest';
type ListResellersSuccess = 'resellers/listResellersSuccess';
type ListResellersFailure = 'resellers/listResellersFailure';
type AddReseller = 'resellers/addReseller';
type AddResellerRequest = 'resellers/addResellerRequest';
type AddResellerSuccess = 'resellers/addResellerSuccess';
type AddResellerFailure = 'resellers/addResellerFailure';
type GetReseller = 'resellers/getReseller';
type GetResellerRequest = 'resellers/getResellerRequest';
type GetResellerSuccess = 'resellers/getResellerSuccess';
type GetResellerFailure = 'resellers/getResellerFailure';
type EditReseller = 'resellers/editReseller';
type EditResellerRequest = 'resellers/editResellerRequest';
type EditResellerSuccess = 'resellers/editResellerSuccess';
type EditResellerFailure = 'resellers/editResellerFailure';
type SearchResellersT = 'resellers/searchResellers';

export type ListResellersAction = {
  type: ListResellers,
  params: $Shape<ListQueryParams>,
};

export type SearchResellersActionT = {
  type: SearchResellersT,
  query: string,
};

export type AddAction = {
  type: AddReseller,
  resellerData: ResellerFormData,
};

export type EditAction = {
  type: EditReseller,
  resellerData: Reseller,
};

export type GetResellerAction = {
  type: GetReseller,
  code: string,
};

export type ActionType =
  | ListResellers
  | ListResellersRequest
  | ListResellersSuccess
  | ListResellersFailure
  | AddReseller
  | AddResellerRequest
  | AddResellerSuccess
  | AddResellerFailure
  | GetReseller
  | GetResellerRequest
  | GetResellerSuccess
  | GetResellerFailure
  | EditReseller
  | EditResellerRequest
  | EditResellerSuccess
  | EditResellerFailure
  | SearchResellersT;

export type Action =
  | { type: ListResellers }
  | { type: ListResellersRequest }
  | {
      type: ListResellersSuccess,
      entities: Entities,
      keys: Keys,
      listQueryParams: ListQueryParams,
    }
  | { type: ListResellersFailure }
  | AddAction
  | { type: AddResellerRequest }
  | {
      type: AddResellerSuccess,
      data: Reseller,
    }
  | { type: AddResellerFailure }
  | GetResellerAction
  | { type: GetResellerRequest }
  | {
      type: GetResellerSuccess,
      entities: Entities,
      key: string,
    }
  | { type: GetResellerFailure }
  | EditAction
  | { type: EditResellerRequest }
  | {
      type: EditResellerSuccess,
      data: Reseller,
    }
  | { type: EditResellerFailure }
  | {
      type: SearchResellersT,
      query: string,
    };


export { ListResellersT } from './redux/listResellers';
export { SearchResellersT } from './redux/searchResellers';
export { AddResellerT } from './redux/addReseller';
export { GetResellerT } from './redux/getReseller';
export { EditResellerT } from './redux/editReseller';
