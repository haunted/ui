// @flow

import React, { Component } from 'react';
import { bindActionCreators, type Dispatch } from 'redux';
import { connect } from 'react-redux';
import ResellerForm from '../forms/ResellerForm';
import { type AddResellerT } from '../types';
import * as resellersActions from '../redux/actions';

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

type Props = {
  loading: boolean,
  formLoading: boolean,
  addReseller: AddResellerT,
};

class SupplierAddView extends Component<Props> {
  render() {
    const {
      loading,
      addReseller,
      formLoading,
    } = this.props;

    if (loading) {
      return loader;
    }

    return (
      <div>
        <h1>Add Reseller</h1>
        <ResellerForm
          onSubmit={addReseller}
          formLoading={formLoading}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...resellersActions,
}, dispatch);

const mapStateToProps = (state) => {
  const { resellers } = state;

  return {
    formLoading: resellers.formLoading,
    loading: resellers.loading,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SupplierAddView);
