// @flow

const styles = {
  form: {
    padding: 24,
  },

  aliasRow: {
    display: 'flex',
    alignItems: 'center',
  },

  cancelButton: {
    marginRight: 12,
  },

  formField: {
    maxWidth: 300,
    marginBottom: 24,
  },
};

export type Styles = { [$Keys<typeof styles>]: string };
export default styles;
