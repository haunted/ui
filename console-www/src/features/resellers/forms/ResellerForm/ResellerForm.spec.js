/* eslint-disable max-len */
import { validate, initialValues } from './ResellerForm';

describe('<ResellerForm />', () => {
  describe('add form validation', () => {
    const props = { edit: false };

    it('should return errors if required values (name, code, email) are empty', () => {
      expect(Object.keys(validate(initialValues, props))
        .sort())
        .toEqual(['name', 'code', 'email'].sort());
    });

    it('should make `firstName` required if `lastName` provided', () => {
      const values = {
        ...initialValues,
        lastName: 'test',
      };

      expect(Object.keys(validate(values, props))
        .sort())
        .toEqual(['name', 'code', 'email', 'firstName'].sort());
    });

    it('should make `lastName` required if `firstName` provided', () => {
      const values = {
        ...initialValues,
        firstName: 'test',
      };

      expect(Object.keys(validate(values, props))
        .sort())
        .toEqual(['name', 'code', 'email', 'lastName'].sort());
    });

    it('should make `password` required if `passwordConfirmation` provided', () => {
      const values = {
        ...initialValues,
        passwordConfirmation: 'test',
      };

      expect(Object.keys(validate(values, props))
        .sort())
        .toEqual(['name', 'code', 'email', 'password'].sort());
    });

    it('should return a validation error if `password` provided but `passwordConfirmation` is empty', () => {
      const values = {
        ...initialValues,
        password: 'test',
      };

      expect(Object.keys(validate(values, props))
        .sort())
        .toEqual(['name', 'code', 'email', 'passwordConfirmation'].sort());
    });

    it('should return an validation error if `password` does not match `passwordConfirmation`', () => {
      const values = {
        ...initialValues,
        password: 'test',
        passwordConfirmation: 'wrong',
      };

      expect(Object.keys(validate(values, props))
        .sort())
        .toEqual(['name', 'code', 'email', 'passwordConfirmation'].sort());
    });

    it('should not return validation errors if required fields provided', () => {
      const values = {
        ...initialValues,
        name: 'test name',
        code: 'test code',
        email: 'test@redeam.com',
      };

      expect(Object.keys(validate(values, props)))
        .toEqual([]);
    });

    it('should not return validation errors if `firstName` and `lastName` provided', () => {
      const values = {
        ...initialValues,
        name: 'test name',
        code: 'test code',
        email: 'test@redeam.com',
        firstName: 'test first name',
        lastName: 'test last name',
      };

      expect(Object.keys(validate(values, props)))
        .toEqual([]);
    });

    it('should not return validation errors if `password` matches `passwordConfirmation`', () => {
      const values = {
        ...initialValues,
        name: 'test name',
        code: 'test code',
        email: 'test@redeam.com',
        password: 'testPass',
        passwordConfirmation: 'testPass',
      };

      expect(Object.keys(validate(values, props)))
        .toEqual([]);
    });
  });

  describe('edit form validation', () => {
    const props = { edit: true };

    it('should return errors if name is empty', () => {
      expect(Object.keys(validate(initialValues, props))
        .sort())
        .toEqual(['name'].sort());
    });
  });
});
