// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import AddResellerForm from '../forms/ResellerForm';
import type { Reseller, GetResellerT, EditResellerT } from '../types';
import * as resellersActions from '../redux/actions';

type Props = {
  reseller: Reseller,
  loading: boolean,
  formLoading: boolean,
  resellerCode: string,
  getReseller: GetResellerT,
  editReseller: EditResellerT,
};

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

class ResellerEditView extends Component<Props> {
  componentWillMount() {
    const { reseller, getReseller, resellerCode } = this.props;

    if (!reseller) {
      getReseller(resellerCode);
    }
  }

  render() {
    const {
      reseller,
      loading,
      editReseller,
      formLoading,
    } = this.props;

    if (loading) {
      return loader;
    }

    return (
      <div>
        <h1>Edit Reseller</h1>
        <AddResellerForm
          onSubmit={editReseller}
          initialValues={reseller}
          formLoading={formLoading}
          edit
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...resellersActions,
}, dispatch);

const mapStateToProps = (state, props) => {
  const {
    resellers,
  } = state;

  const {
    match,
  } = props;

  return {
    reseller: resellers.entities[match.params.code],
    loading: resellers.loading,
    formLoading: resellers.formLoading,
    resellerCode: match.params.code,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResellerEditView);
