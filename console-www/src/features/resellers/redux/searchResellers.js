// @flow

import { put, call, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import type { Saga, Effect } from 'redux-saga';
import type { Action, ActionType, State, SearchResellersActionT } from '../types';
import { listResellers } from './listResellers';


/**
 * Main action creator
 */
export function searchResellers(query: string): Action {
  return {
    type: 'resellers/searchResellers',
    query,
  };
}


/**
 * Export the type of the action creator
 */
export type SearchResellersT = typeof searchResellers;


/**
 * Action's logic
 */
export function* doSearchResellers({ query }: SearchResellersActionT): Saga<void> {
  yield call(delay, 500);
  yield put(listResellers({ search: query }));
}


/**
 * Saga watcher
 */
export function* watchSearchResellers(): Iterable<Effect> {
  yield takeLatest(('resellers/searchResellers': ActionType), doSearchResellers);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'resellers/searchResellers':
      return {
        ...state,
        listQueryParams: {
          ...state.listQueryParams,
          offset: 0,
        },
        ui: {
          ...state.ui,
          searchResellersQuery: action.query,
        },
      };

    default:
      return state;
  }
}
