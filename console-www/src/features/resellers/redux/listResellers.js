// @flow

import { takeLatest, call, put, select } from 'redux-saga/effects';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import type { Saga, Effect } from 'redux-saga';
import { resellers } from '../../../utils/apis';
import { resellerListSchema } from '../../../common/schemas';
import type {
  State,
  Reseller,
  ActionType,
  Action,
  ListResellersAction,
  ResellerListResponse,
} from '../types';
import type {
  Normalized,
  ListQueryParams,
  State as GlobalState,
  AxiosResponse,
} from '../../../common/types';
import { mergeListResponse, mergeQueryParams } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';

export function listResellers(params: $Shape<ListQueryParams> = {}): Action {
  return {
    type: 'resellers/listResellers',
    params,
  };
}

export type ListResellersT = typeof listResellers;

export function* doListResellers({ params }: ListResellersAction): Saga<void> {
  const prevParams = yield select((state: GlobalState) => state.resellers.listQueryParams);
  const listQueryParams = mergeQueryParams(prevParams, params);

  yield put(({
    type: 'resellers/listResellersRequest',
  }: Action));

  try {
    const response: AxiosResponse<ResellerListResponse> =
      yield call(resellers.listResellers, listQueryParams);
    const normalizedData: Normalized<Reseller, 'resellers'> = normalize(
      response.data.resellers,
      resellerListSchema,
    );

    yield put(({
      type: 'resellers/listResellersSuccess',
      entities: normalizedData.entities.resellers || {},
      keys: normalizedData.result,
      listQueryParams,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'resellers/listResellersFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to fetch resellers');
    }
  }
}

export function* watchListResellers(): Iterable<Effect> {
  yield takeLatest(('resellers/listResellers': ActionType), doListResellers);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'resellers/listResellersRequest':
      return {
        ...state,
        loading: true,
      };

    case 'resellers/listResellersSuccess':
      return {
        ...state,
        loading: false,
        ...mergeListResponse(state, action),
      };

    case 'resellers/listResellersFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
