// @flow

import type { State } from '../../../common/types';
import type { Reseller } from '../types';

/**
 * Returns the array of all loaded resellers
 */
export const resellerListSelector = (state: State): Array<Reseller> =>
  state.resellers.keys.map(code => state.resellers.entities[code]);
