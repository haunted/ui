// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import uniq from 'lodash.uniq';
import { resellers } from '../../../utils/apis';
import type {
  ActionType,
  Action,
  Reseller,
  ResellerListResponse,
  State,
  GetResellerAction,
} from '../types';
import type { AxiosResponse, NormalizedSingle } from '../../../common/types';
import { resellerSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';


export function getReseller(code: string): Action {
  return {
    type: 'resellers/getReseller',
    code,
  };
}

export type GetResellerT = typeof getReseller;

export function* doGetReseller({ code }: GetResellerAction): Saga<void> {
  yield put(({
    type: 'resellers/getResellerRequest',
  }: Action));

  try {
    const response: AxiosResponse<ResellerListResponse> =
      yield call(resellers.getReseller, code);

    const normalizedData: NormalizedSingle<Reseller, 'resellers'> = normalize(
      response.data,
      resellerSchema,
    );

    yield put(({
      type: 'resellers/getResellerSuccess',
      entities: normalizedData.entities.resellers,
      key: normalizedData.result,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'resellers/getResellerFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to fetch the reseller');
    }
  }
}

export function* watchGetReseller(): Iterable<Effect> {
  yield takeLatest(('resellers/getReseller': ActionType), doGetReseller);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'resellers/getResellerRequest':
      return {
        ...state,
        loading: true,
      };

    case 'resellers/getResellerSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
        keys: uniq([...state.keys, action.key]),
        loading: false,
      };

    case 'resellers/getResellerFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
