import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { normalize, schema } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doListResellers, reducer } from '../listResellers';
import { resellers } from '../../../../utils/apis';
import resellersMock from '../../../../mocks/resellers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import initialState from '../initialState';

const resellerSchema = new schema.Entity('resellers', {}, { idAttribute: 'code' });
const resellerListSchema = [resellerSchema];
const params = {};


describe('List Resellers Saga', () => {
  const err = new Error('error');
  const normalizedData = normalize(
    resellersMock.resellers,
    resellerListSchema,
  );

  it(
    'should go through list resellers process correctly',
    () => expectSaga(doListResellers, { params })
      .withState({ resellers: initialState })
      .provide([
        [matchers.call.fn(resellers.listResellers), wrapValidResponse(resellersMock)],
      ])
      .put({ type: 'resellers/listResellersRequest' })
      .put({
        type: 'resellers/listResellersSuccess',
        entities: normalizedData.entities.resellers,
        keys: normalizedData.result,
        listQueryParams: initialState.listQueryParams,
      })
      .run(),
  );

  it(
    'should show notification when list resellers fails',
    () => expectSaga(doListResellers, { params })
      .withState({ resellers: initialState })
      .provide([
        [matchers.call.fn(resellers.listResellers), throwError(err)],
      ])
      .put({ type: 'resellers/listResellersRequest' })
      .put({ type: 'resellers/listResellersFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to fetch resellers')
      .run(),
  );

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'resellers/listResellersRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` when request successes', () => {
    function* saga() {
      yield put({
        type: 'resellers/listResellersSuccess',
        entities: normalizedData.entities.resellers,
        keys: normalizedData.result,
        listQueryParams: initialState.listQueryParams,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        loading: false,
        entities: normalizedData.entities.resellers,
        keys: normalizedData.result,
        listQueryParams: initialState.listQueryParams,
        ui: initialState.ui,
        formLoading: false,
        hasMore: false,
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'resellers/listResellersFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });
});
