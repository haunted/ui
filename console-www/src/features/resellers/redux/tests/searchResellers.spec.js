import { put } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { doSearchResellers, reducer } from '../searchResellers';
import initialState from '../initialState';
import { listResellers } from '../listResellers';

const query = 'test query';


describe('Search Resellers Saga', () => {
  it(
    'should go through search resellers process correctly',
    () => expectSaga(doSearchResellers, { query })
      .withState({ passIssuers: initialState })
      .provide([
        [matchers.call.fn(delay, 500)],
      ])
      .put(listResellers({ search: query }))
      .run(),
  );

  it('should set search query to store', () => {
    function* saga() {
      yield put({ type: 'resellers/searchResellers', query });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        entities: {},
        keys: [],
        loading: false,
        hasMore: true,
        listQueryParams: {
          limit: 50,
          offset: 0,
          sort: 'name',
          search: '',
        },
        formLoading: false,
        ui: { searchResellersQuery: 'test query' },
      })
      .run();
  });
});
