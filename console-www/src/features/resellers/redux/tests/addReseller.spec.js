import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doAddReseller, reducer } from '../addReseller';
import { resellers } from '../../../../utils/apis';
import resellersMock from '../../../../mocks/resellers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import history from '../../../../common/history';
import initialState from '../initialState';

const code = 'CODE';
const reseller = resellersMock.resellers[0];

delete reseller.code;

const resellerData = reseller;


describe('Add Reseller Saga', () => {
  const err = new Error('error');

  it('should go through add reseller process correctly', () =>
    expectSaga(doAddReseller, { resellerData })
      .provide([
        [
          matchers.call.fn(resellers.addReseller),
          wrapValidResponse(resellerData),
        ],
      ])
      .put({ type: 'resellers/addResellerRequest' })
      .put({
        type: 'resellers/addResellerSuccess',
        data: resellerData,
      })
      .call(history.push, '/resellers')
      .call([NotificationManager, NotificationManager.success], 'Reseller created successfully')
      .run());

  it('should show notification when add reseller fails', () =>
    expectSaga(doAddReseller, { resellerData })
      .provide([
        [matchers.call.fn(resellers.addReseller), throwError(err)],
      ])
      .put({ type: 'resellers/addResellerRequest' })
      .put({ type: 'resellers/addResellerFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to create the reseller')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'resellers/addResellerRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'resellers/addResellerSuccess',
        data: { ...resellerData, code },
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        formLoading: false,
        entities: {
          [code]: { ...resellerData, code },
        },
        keys: [code],
        loading: false,
        hasMore: true,
        listQueryParams: {
          limit: 50,
          offset: 0,
          sort: 'name',
          search: '',
        },
        ui: { searchResellersQuery: '' },
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'resellers/addResellerFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: false,
      })
      .run();
  });
});
