import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doEditReseller, reducer } from '../editReseller';
import { resellers } from '../../../../utils/apis';
import resellersMock from '../../../../mocks/resellers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import history from '../../../../common/history';

const resellerData = resellersMock.resellers[0];
const changedResellersData = {
  ...resellerData,
  name: 'new name',
};


describe('Edit Reseller Saga', () => {
  const err = new Error('error');

  it('should go through edit reseller process correctly', () =>
    expectSaga(doEditReseller, changedResellersData)
      .provide([
        [
          matchers.call.fn(resellers.editReseller),
          wrapValidResponse(changedResellersData),
        ],
      ])
      .put({ type: 'resellers/editResellerRequest' })
      .put({
        type: 'resellers/editResellerSuccess',
        data: changedResellersData,
      })
      .call(history.push, '/resellers')
      .call([NotificationManager, NotificationManager.success], 'Reseller edited successfully')
      .run());

  it('should show notification when get reseller fails', () =>
    expectSaga(doEditReseller, changedResellersData)
      .provide([
        [matchers.call.fn(resellers.editReseller), throwError(err)],
      ])
      .put({ type: 'resellers/editResellerRequest' })
      .put({ type: 'resellers/editResellerFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to edit the reseller')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'resellers/editResellerRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'resellers/editResellerSuccess',
        data: changedResellersData,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, {})
      .hasFinalState({
        formLoading: false,
        entities: {
          [resellerData.code]: changedResellersData,
        },
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'resellers/editResellerFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        formLoading: false,
      })
      .run();
  });
});
