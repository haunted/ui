import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { normalize, schema } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doGetReseller, reducer } from '../getReseller';
import { resellers } from '../../../../utils/apis';
import resellersMock from '../../../../mocks/resellers.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';

const resellerSchema = new schema.Entity('resellers', {}, { idAttribute: 'code' });
const code = 'CODE';


describe('Get Reseller Saga', () => {
  const err = new Error('error');
  const normalizedData = normalize(
    resellersMock.resellers[0],
    resellerSchema,
  );

  const initialState = {
    entities: {},
    keys: [],
    loading: false,
  };

  it('should go through get reseller process correctly', () =>
    expectSaga(doGetReseller, { code })
      .provide([
        [
          matchers.call.fn(resellers.getReseller),
          wrapValidResponse(resellersMock.resellers[0]),
        ],
      ])
      .put({ type: 'resellers/getResellerRequest' })
      .put({
        type: 'resellers/getResellerSuccess',
        entities: normalizedData.entities.resellers,
        key: normalizedData.result,
      })
      .run());

  it('should show notification when get reseller fails', () =>
    expectSaga(doGetReseller, { code })
      .provide([
        [matchers.call.fn(resellers.getReseller), throwError(err)],
      ])
      .put({ type: 'resellers/getResellerRequest' })
      .put({ type: 'resellers/getResellerFailure' })
      .call([NotificationManager, NotificationManager.error], 'Failed to fetch the reseller')
      .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'resellers/getResellerRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` and set valid data when request successes', () => {
    function* saga() {
      yield put({
        type: 'resellers/getResellerSuccess',
        entities: normalizedData.entities.resellers,
        key: normalizedData.result,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        loading: false,
        entities: normalizedData.entities.resellers,
        keys: [normalizedData.result],
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'resellers/getResellerFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });
});
