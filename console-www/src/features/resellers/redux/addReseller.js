// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import uniq from 'lodash.uniq';
import { resellers } from '../../../utils/apis';
import type { ActionType, Action, State, AddAction, ResellerFormData } from '../types';
import history from '../../../common/history';

export function addReseller(resellerData: ResellerFormData): Action {
  return {
    type: 'resellers/addReseller',
    resellerData,
  };
}

export type AddResellerT = typeof addReseller;

export function* doAddReseller({ resellerData }: AddAction): Saga<void> {
  yield put(({
    type: 'resellers/addResellerRequest',
  }: Action));

  try {
    const { data } = yield call(resellers.addReseller, resellerData);

    yield put(({
      type: 'resellers/addResellerSuccess',
      data,
    }: Action));

    yield call(history.push, '/resellers');
    yield call(
      [NotificationManager, NotificationManager.success],
      'Reseller created successfully',
    );
  } catch (e) {
    yield put(({
      type: 'resellers/addResellerFailure',
    }: Action));
    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to create the reseller',
    );
  }
}

export function* watchAddReseller(): Iterable<Effect> {
  yield takeLatest(('resellers/addReseller': ActionType), doAddReseller);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'resellers/addResellerRequest':
      return {
        ...state,
        formLoading: true,
      };

    case 'resellers/addResellerSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.code]: action.data,
        },
        keys: uniq([...state.keys, action.data.code]),
        formLoading: false,
      };

    case 'resellers/addResellerFailure':
      return {
        ...state,
        formLoading: false,
      };

    default:
      return state;
  }
}
