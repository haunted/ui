// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { resellers } from '../../../utils/apis';
import type { ActionType, Action, State, EditAction, Reseller } from '../types';
import history from '../../../common/history';

export function editReseller(resellerData: Reseller): Action {
  return {
    type: 'resellers/editReseller',
    resellerData,
  };
}

export type EditResellerT = typeof editReseller;

export function* doEditReseller({ resellerData }: EditAction): Saga<void> {
  yield put(({
    type: 'resellers/editResellerRequest',
  }: Action));

  try {
    const { data } = yield call(resellers.editReseller, resellerData);

    yield put(({
      type: 'resellers/editResellerSuccess',
      data,
    }: Action));

    yield call(history.push, '/resellers');
    yield call(
      [NotificationManager, NotificationManager.success],
      'Reseller edited successfully',
    );
  } catch (e) {
    yield put(({
      type: 'resellers/editResellerFailure',
    }: Action));
    yield call([NotificationManager, NotificationManager.error], 'Failed to edit the reseller');
  }
}

export function* watchEditReseller(): Iterable<Effect> {
  yield takeLatest(('resellers/editReseller': ActionType), doEditReseller);
}

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'resellers/editResellerRequest':
      return {
        ...state,
        formLoading: true,
      };

    case 'resellers/editResellerSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.code]: action.data,
        },
        formLoading: false,
      };

    case 'resellers/editResellerFailure':
      return {
        ...state,
        formLoading: false,
      };

    default:
      return state;
  }
}
