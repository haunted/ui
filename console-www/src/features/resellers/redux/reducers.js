import initialState from './initialState';
import { reducer as listResellersReducer } from './listResellers';
import { reducer as searchResellersReducer } from './searchResellers';
import { reducer as addResellerReducer } from './addReseller';
import { reducer as getResellerReducer } from './getReseller';
import { reducer as editResellerReducer } from './editReseller';

const reducers = [
  listResellersReducer,
  searchResellersReducer,
  addResellerReducer,
  getResellerReducer,
  editResellerReducer,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    default:
      newState = state;
      break;
  }

  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
