// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import Table from 'redeam-styleguide/lib/Table';
import InfiniteScroll from 'redeam-styleguide/lib/InfiniteScroll';
import Button from 'redeam-styleguide/lib/Button';
import * as resellerActions from '../redux/actions';
import { resellerListSelector } from '../redux/selectors';
import history from '../../../common/history';
import styles, { type Styles } from './ResellersListView.jss';
import type { State as ResellersState } from '../../passIssuers/types';
import type { State } from '../../../common/types';
import type { ListResellersT, SearchResellersT, Reseller } from '../types';
import tableChangeHandler from '../../../utils/tableChangeHandler';

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

const columns = [
  {
    dataField: 'name',
    text: 'Name',
    sort: true,
  },
  {
    dataField: 'code',
    text: 'Code',
    sort: true,
  },
  {
    dataField: 'aliases',
    text: 'Aliases',
    formatter: (e, { aliases }) => aliases.join(', '),
  },
  {
    dataField: 'actions',
    text: 'Actions',
    formatter: (e, { code }) => (
      <div>
        <button onClick={() => history.push(`/resellers/${code}/edit`)}>Edit</button>
      </div>
    ),
  },
];

type Props = {
  listResellers: ListResellersT,
  searchResellers: SearchResellersT,
  resellers: {
    list: Array<Reseller>,
  } & ResellersState,
  classes: Styles,
  nextOffset: number,
};

class SuppliersListView extends Component<Props> {
  componentWillMount() {
    this.props.listResellers();
  }

  render() {
    const {
      resellers,
      classes,
      nextOffset,
      listResellers,
      searchResellers,
    } = this.props;

    return (
      <div>
        <div className={classes.header}>
          <h1>Resellers</h1>

          <div>
            <input
              onChange={e => searchResellers(e.target.value)}
              value={resellers.ui.searchPassIssuersQuery}
              placeholder="Search resellers..."
            />
            {' '}
            <Button primary onClick={() => history.push('/resellers/add')}>Add Reseller</Button>
          </div>
        </div>

        <InfiniteScroll
          loading={resellers.loading}
          loader={loader}
          hasMore={resellers.hasMore}
          loadMore={() => listResellers({ offset: nextOffset })}
        >
          <Table
            keyField="code"
            data={resellers.list}
            columns={columns}
            remote={{ sort: true }}
            onTableChange={tableChangeHandler(listResellers)}
          />
        </InfiniteScroll>
      </div>
    );
  }
}

const mapStateToProps = (state: State) => {
  const {
    resellers,
  } = state;
  const { limit, offset } = resellers.listQueryParams;

  return {
    resellers: {
      ...resellers,
      list: resellerListSelector(state),
    },
    nextOffset: offset + limit,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...resellerActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(SuppliersListView));
