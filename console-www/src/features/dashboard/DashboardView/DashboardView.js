import React, { Component } from 'react';
import injectSheet from 'react-jss';
import { Switch, Route, Redirect } from 'react-router-dom';
import Header from '../../common/Header';
import Menu from '../../common/Menu';
import NotFoundView from '../../common/NotFoundView';
import AuthContainer from '../../auth/AuthContainer';
import SupplierListView from '../../suppliers/SupplierListView';
import SupplierAddView from '../../suppliers/SupplierAddView';
import SupplierEditView from '../../suppliers/SupplierEditView';
import ResellersListView from '../../resellers/ResellersListView';
import PassIssuerListView from '../../passIssuers/PassIssuerListView';
import DeviceListView from '../../devices/DeviceListView';
import UserListView from '../../users/UserListView';
import ResellerAddView from '../../resellers/ResellerAddView';
import ResellerEditView from '../../resellers/ResellerEditView';
import SupplierDeviceListView from '../../devices/SupplierDeviceListView';
import DeviceEditView from '../../devices/DeviceEditView';
import PassIssuerEditView from '../../passIssuers/PassIssuerEditView';
import PassIssuerAddView from '../../passIssuers/PassIssuerAddView';
import UserAddView from '../../users/UserAddView';
import SelfUserEditView from '../../users/SelfUserEditView';


import styles from './DashboardView.jss';

class DashboardView extends Component {
  render() {
    const { classes } = this.props;

    return (
      <AuthContainer>
        <div className={classes.wrapper}>
          <Header />

          <div className={classes.container}>
            <Menu />

            <main className={classes.main}>
              <Switch>
                <Route path="/suppliers" exact component={SupplierListView} />
                <Route path="/suppliers/add" exact component={SupplierAddView} />
                <Route path="/suppliers/:code/edit" exact component={SupplierEditView} />
                <Route
                  path="/suppliers/:supplierCode/devices"
                  exact
                  component={SupplierDeviceListView}
                />

                <Route path="/resellers" exact component={ResellersListView} />
                <Route path="/resellers/add" exact component={ResellerAddView} />
                <Route path="/resellers/:code/edit" exact component={ResellerEditView} />

                <Route path="/devices" exact component={DeviceListView} />
                <Route path="/devices/:id/edit" exact component={DeviceEditView} />

                <Route path="/pass-issuers" exact component={PassIssuerListView} />
                <Route path="/pass-issuers/add" exact component={PassIssuerAddView} />
                <Route path="/pass-issuers/:code/edit" exact component={PassIssuerEditView} />

                <Route path="/users" exact component={UserListView} />
                <Route path="/users/add" exact component={UserAddView} />

                <Route path="/profile" exact component={SelfUserEditView} />

                <Redirect from="/" to="/suppliers" />

                <Route component={NotFoundView} />
              </Switch>
            </main>
          </div>
        </div>
      </AuthContainer>
    );
  }
}

export default injectSheet(styles)(DashboardView);
