const styles = {
  wrapper: {
    display: 'flex',
    flexFlow: 'column',
    minHeight: '100vh',
    paddingTop: 60,
  },
  container: {
    display: 'flex',
    flex: '1',
  },
  main: {
    flex: '1',
    padding: 24,
  },
};

export default styles;
