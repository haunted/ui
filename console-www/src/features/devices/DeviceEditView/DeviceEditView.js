// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import type { Dispatch } from 'redux';
import EditDeviceForm from '../forms/DeviceForm';

import type { Device, GetDeviceT, EditDeviceT } from '../types';
import { normalizePhone } from '../../../utils/forms/phoneNumber';
import * as devicesActions from '../redux/actions';

type Props = {
  deviceId: string,
  device: Device,
  loading: Boolean,
  formLoading: Boolean,
  getDevice: GetDeviceT,
  editDevice: EditDeviceT,
};

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

class DeviceEditView extends Component<Props> {
  componentWillMount() {
    const { deviceId, getDevice } = this.props;

    getDevice(deviceId);
  }

  render() {
    const {
      device,
      loading,
      editDevice,
      formLoading,
    } = this.props;

    if (loading || !device) {
      return loader;
    }

    const initialValues = {
      ...device,
      phoneNumber: normalizePhone(device.phoneNumber),
    };

    return (
      <div>
        <h1>Edit Device</h1>
        <EditDeviceForm
          onSubmit={editDevice}
          initialValues={initialValues}
          formLoading={formLoading}
        />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...devicesActions,
}, dispatch);

const mapStateToProps = (state, props) => {
  const {
    devices,
  } = state;

  const {
    match,
  } = props;

  const deviceId = match.params.id;

  return {
    deviceId,
    device: devices.entities[deviceId],
    loading: devices.loading,
    formLoading: devices.editLoading,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DeviceEditView);
