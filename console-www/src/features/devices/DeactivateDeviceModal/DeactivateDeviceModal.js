// @flow

import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import DeviceDeactivateForm from '../forms/DeviceDeactivateForm';
import Modal from '../../common/Modal';
import * as deviceActions from '../redux/actions';
import * as commonActions from '../../common/redux/actions';
import { modals } from '../../../common/constants';
import type { Device, DeactivateFormDataT, DeactivateDeviceT } from '../types';
import type { CloseModalT } from '../../common/types';

type PropsT = {
  device: Device,
  closeModal: CloseModalT,
  deactivateDevice: DeactivateDeviceT,
};

function DeactivateDeviceModal(props: PropsT) {
  const {
    device,
    closeModal,
    deactivateDevice,
  } = props;

  return (
    <Modal id={modals.DEACTIVATE_DEVICE_MODAL} title="Deactivate Device">
      <DeviceDeactivateForm
        onSubmit={(data: DeactivateFormDataT) =>
          deactivateDevice(device, data.reason)
        }
        onCancel={closeModal}
        loading={device.deactivating}
      />
    </Modal>
  );
}

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...commonActions,
  ...deviceActions,
}, dispatch);

export default connect(
  null,
  mapDispatchToProps,
)(DeactivateDeviceModal);
