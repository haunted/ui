// @flow

import { put, call, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import type { Saga, Effect } from 'redux-saga';
import type { Action, ActionType, State, SearchDevicesActionT } from '../types';
import { listDevices } from './listDevices';


/**
 * Main action creator
 */
export function searchDevices(query: string): Action {
  return {
    type: 'devices/searchDevices',
    query,
  };
}


/**
 * Export the type of the action creator
 */
export type SearchDevicesT = typeof searchDevices;


/**
 * Action's logic
 */
export function* doSearchDevices({ query }: SearchDevicesActionT): Saga<void> {
  yield call(delay, 500);
  yield put(listDevices({ search: query }));
}


/**
 * Saga watcher
 */
export function* watchSearchDevices(): Iterable<Effect> {
  yield takeLatest(('devices/searchDevices': ActionType), doSearchDevices);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'devices/searchDevices':
      return {
        ...state,
        listQueryParams: {
          ...state.listQueryParams,
          offset: 0,
        },
        ui: {
          ...state.ui,
          searchDevicesQuery: action.query,
        },
      };

    default:
      return state;
  }
}
