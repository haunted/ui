// @flow

import { createSelector } from 'reselect';
import type { State } from '../../../common/types';
import type { Device } from '../types';
import { deviceStatus } from '../../../common/constants';

/**
 * Returns the array of all loaded devices
 */
export const deviceListSelector = (state: State): Array<Device> =>
  state.devices.keys
    .map(id => state.devices.entities[id])
    .filter(device => device.status !== deviceStatus.STATUS_DEACTIVATED);


/**
 * Returns the array of devices for specific supplier
 */
export const supplierDeviceListSelector = (state: State): Array<Device> =>
  state.devices.supplier.keys
    .map(id => state.devices.entities[id])
    .filter(device => device.status !== deviceStatus.STATUS_DEACTIVATED);


/**
 * Return the list of devices which are not currently provisioned
 */
export const unprovisionedDevices = createSelector(
  deviceListSelector,
  (devices: Array<Device>) => devices.filter(device =>
    [deviceStatus.STATUS_DEPROVISIONED, deviceStatus.STATUS_NEW].includes(device.status)),
);
