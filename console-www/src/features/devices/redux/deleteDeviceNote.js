// @flow


import { put, call, takeLatest } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { normalize } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import type {
  State,
  Action,
  DeviceNote,
  DeleteDeviceNoteAction,
  ActionType,
  Device,
} from '../types';
import { devices } from '../../../utils/apis';
import type { AxiosResponse, NormalizedSingle } from '../../../common/types';
import { deviceSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';


/**
 * Action creator
 */
export function deleteDeviceNote(
  deviceId: $PropertyType<Device, 'id'>,
  noteId: $PropertyType<DeviceNote, 'id'>,
): Action {
  return {
    type: 'devices/deleteDeviceNote',
    deviceId,
    noteId,
  };
}


/**
 * Export the type of the action creator
 */
export type DeleteDeviceNoteT = typeof deleteDeviceNote;


/**
 * Action's logic
 */
export function* doDeleteDeviceNote({ deviceId, noteId }: DeleteDeviceNoteAction): Saga<void> {
  yield put(({
    type: 'devices/deleteDeviceNoteRequest',
    deviceId,
    noteId,
  }: Action));

  try {
    const response: AxiosResponse<Device> = yield call(devices.deleteDeviceNote, deviceId, noteId);

    const normalizedData: NormalizedSingle<Device, 'devices'> = normalize(
      response.data,
      deviceSchema,
    );

    yield put(({
      type: 'devices/deleteDeviceNoteSuccess',
      entities: normalizedData.entities.devices,
      key: normalizedData.result,
    }: Action));
  } catch (err) {
    yield put(({
      type: 'devices/deleteDeviceNoteFailure',
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to delete a note',
    );
  }
}


/**
 * Saga watcher
 */
export function* watchDeleteDeviceNote(): Iterable<Effect> {
  yield takeLatest(('devices/deleteDeviceNote': ActionType), doDeleteDeviceNote);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'devices/deleteDeviceNoteRequest': {
      const { noteId } = action;

      return {
        ...state,
        entities: {
          ...state.entities,
          [action.deviceId]: {
            ...state.entities[action.deviceId],
            notes: state.entities[action.deviceId].notes.map(note => (note.id === noteId ? {
              ...note,
              deleting: true,
            } : note)),
          },
        },
      };
    }
    case 'devices/deleteDeviceNoteSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
        creatingNote: false,
      };
    case 'devices/deleteDeviceNoteFailure':
      return {
        ...state,
        creatingNote: false,
      };
    default:
      return state;
  }
}

