// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { normalize } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import { devices } from '../../../utils/apis';
import type {
  ActionType,
  Action,
  Device,
  DeviceListResponse,
  State,
  ListSupplierDevicesAction,
} from '../types';
import type { AxiosResponse, Normalized } from '../../../common/types';
import { deviceListSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';


/**
 * Action creator
 */
export function listSupplierDevices(code: string): Action {
  return {
    type: 'devices/listSupplierDevices',
    code,
  };
}


/**
 * Export the type of the action creator
 */
export type ListSupplierDevicesT = typeof listSupplierDevices;


/**
 * Action's logic
 */
export function* doListSupplierDevices({ code }: ListSupplierDevicesAction): Saga<void> {
  yield put(({
    type: 'devices/listSupplierDevicesRequest',
  }: Action));

  try {
    const response: AxiosResponse<DeviceListResponse> =
      yield call(devices.listSupplierDevices, code);

    const normalizedData: Normalized<Device, 'devices'> = normalize(
      response.data.devices,
      deviceListSchema,
    );

    yield put(({
      type: 'devices/listSupplierDevicesSuccess',
      entities: normalizedData.entities.devices || {},
      keys: normalizedData.result,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'devices/listSupplierDevicesFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to load devices');
    }
  }
}


/**
 * Saga watcher
 */
export function* watchListSupplierDevices(): Iterable<Effect> {
  yield takeLatest(('devices/listSupplierDevices': ActionType), doListSupplierDevices);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'devices/listSupplierDevicesRequest':
      return {
        ...state,
        supplier: {
          ...state.supplier,
          loading: true,
          keys: [],
        },
      };

    case 'devices/listSupplierDevicesSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
        supplier: {
          ...state.supplier,
          keys: action.keys,
          loading: false,
        },
      };

    case 'devices/listSupplierDevicesFailure':
      return {
        ...state,
        supplier: {
          ...state.supplier,
          loading: false,
        },
      };

    default:
      return state;
  }
}
