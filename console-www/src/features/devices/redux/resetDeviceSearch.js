// @flow

import type { Action, State as DeviceState } from '../types';

/**
 * Action creator
 */
export function resetDeviceSearch(): Action {
  return {
    type: 'devices/resetDeviceSearch',
  };
}

export type ResetDeviceSearchT = typeof resetDeviceSearch;


/**
 * Action's reducer
 */
export function reducer(state: DeviceState, action: Action): DeviceState {
  switch (action.type) {
    case 'devices/resetDeviceSearch':
      return {
        ...state,
        listQueryParams: {
          ...state.listQueryParams,
          search: '',
        },
        ui: {
          ...state.ui,
          searchDevicesQuery: '',
        },
      };
    default:
      return state;
  }
}
