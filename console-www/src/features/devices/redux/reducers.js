import initialState from './initialState';
import { reducer as listSupplierDevices } from './listSupplierDevices';
import { reducer as searchDevices } from './searchDevices';
import { reducer as provisionDevices } from './provisionDevices';
import { reducer as deprovisionDevice } from './deprovisionDevice';
import { reducer as deactivateDevice } from './deactivateDevice';
import { reducer as listDevices } from './listDevices';
import { reducer as getDevice } from './getDevice';
import { reducer as editDevice } from './editDevice';
import { reducer as addDeviceNote } from './addDeviceNote';
import { reducer as deleteDeviceNote } from './deleteDeviceNote';
import { reducer as resetDeviceSearch } from './resetDeviceSearch';

const reducers = [
  listSupplierDevices,
  searchDevices,
  provisionDevices,
  deprovisionDevice,
  deactivateDevice,
  listDevices,
  getDevice,
  editDevice,
  addDeviceNote,
  deleteDeviceNote,
  resetDeviceSearch,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    default:
      newState = state;
      break;
  }

  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
