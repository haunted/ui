// @flow

import { takeLatest, call, put, select } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import { devices } from '../../../utils/apis';
import type { ActionType, Action, State as DeviceState, DeactivateAction, Device } from '../types';
import type { NormalizedSingle, State } from '../../../common/types';
import { closeModal } from '../../common/redux/actions';
import { modals } from '../../../common/constants';
import { deviceSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';


/**
 * Action creator
 */
export function deactivateDevice(device: Device, reason: string): Action {
  return {
    type: 'devices/deactivateDevice',
    device,
    reason,
  };
}


/**
 * Export the type of the action creator
 */
export type DeactivateDeviceT = typeof deactivateDevice;


/**
 * Action's logic
 */
export function* doDeactivateDevice({ device, reason }: DeactivateAction): Saga<void> {
  const modal = yield select((state: State) => state.common.ui.modal);

  yield put(({
    type: 'devices/deactivateDeviceRequest',
    id: device.id,
  }: Action));

  try {
    const response = yield call(devices.deactivateDevice, device, reason);

    const normalizedData: NormalizedSingle<Device, 'devices'> = normalize(
      response.data,
      deviceSchema,
    );

    yield put(({
      type: 'devices/deactivateDeviceSuccess',
      entities: normalizedData.entities.devices,
      key: normalizedData.result,
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.success],
      'Device deactivated successfully',
    );
  } catch (e) {
    yield put(({
      type: 'devices/deactivateDeviceFailure',
      id: device.id,
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to deactivate the device',
    );
  }

  if (modal === modals.DEACTIVATE_DEVICE_MODAL) {
    yield put(closeModal());
  }
}


/**
 * Saga watcher
 */
export function* watchDeactivateDevice(): Iterable<Effect> {
  yield takeLatest(('devices/deactivateDevice': ActionType), doDeactivateDevice);
}


/**
 * Action's reducer
 */
export function reducer(state: DeviceState, action: Action): DeviceState {
  switch (action.type) {
    case 'devices/deactivateDeviceRequest':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.id]: {
            ...state.entities[action.id],
            deactivating: true,
          },
        },
      };

    case 'devices/deactivateDeviceSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
      };

    case 'devices/deactivateDeviceFailure':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.id]: {
            ...state.entities[action.id],
            deactivating: false,
          },
        },
      };

    default:
      return state;
  }
}
