// @flow

import { takeLatest, call, put, select } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { normalize } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import { devices } from '../../../utils/apis';
import type {
  ActionType,
  Action,
  Device,
  DeviceListResponse,
  State,
  ListDevicesAction,
} from '../types';
import type {
  AxiosResponse,
  ListQueryParams,
  Normalized,
  State as GlobalState,
} from '../../../common/types';
import { deviceListSchema } from '../../../common/schemas';
import { mergeListResponse, mergeQueryParams } from '../../../utils/redux';
import { deviceStatus } from '../../../common/constants';
import isAuthorized from '../../../utils/isAuthorized';


/**
 * Action creator
 */
export function listDevices(params: $Shape<ListQueryParams> = {}): Action {
  return {
    type: 'devices/listDevices',
    params,
  };
}


/**
 * Export the type of the action creator
 */
export type ListDevicesT = typeof listDevices;


/**
 * Action's logic
 */
export function* doListDevices({ params }: ListDevicesAction): Saga<void> {
  const prevParams = yield select((state: GlobalState) => state.devices.listQueryParams);
  const listQueryParams = mergeQueryParams(prevParams, {
    // load unprovisioned devices by default
    status: [deviceStatus.STATUS_DEPROVISIONED, deviceStatus.STATUS_NEW].join(),
    ...params,
  });

  yield put(({
    type: 'devices/listDevicesRequest',
    listQueryParams,
  }: Action));

  try {
    const response: AxiosResponse<DeviceListResponse> =
      yield call(devices.listDevices, listQueryParams);

    const normalizedData: Normalized<Device, 'devices'> = normalize(
      response.data.devices,
      deviceListSchema,
    );

    yield put(({
      type: 'devices/listDevicesSuccess',
      entities: normalizedData.entities.devices || {},
      keys: normalizedData.result,
      listQueryParams,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'devices/listDevicesFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to load devices');
    }
  }
}


/**
 * Saga watcher
 */
export function* watchListDevices(): Iterable<Effect> {
  yield takeLatest(('devices/listDevices': ActionType), doListDevices);
}

/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'devices/listDevicesRequest':
      return {
        ...state,
        loading: true,
      };

    case 'devices/listDevicesSuccess':
      return {
        ...state,
        loading: false,
        ...mergeListResponse(state, action),
      };

    case 'devices/listDevicesFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
