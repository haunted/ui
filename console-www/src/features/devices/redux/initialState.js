// @flow

import type { State } from '../types';

const initialState: State = {
  entities: {},
  keys: [],
  loading: false,
  provisioning: false,
  creatingNote: false,
  hasMore: true,
  listQueryParams: {
    limit: 50,
    offset: 0,
    sort: 'name',
    search: '',
  },
  supplier: {
    keys: [],
    loading: false,
  },
  ui: {
    searchDevicesQuery: '',
  },
};

export default initialState;
