// @flow

import { takeLatest, call, put, select } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { normalize } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import uniq from 'lodash.uniq';
import { closeModal } from '../../common/redux/actions';
import { devices as devicesApi } from '../../../utils/apis';
import type {
  Device,
  ActionType,
  Action,
  State as DeviceState,
  ProvisionAction,
  DeviceListResponse,
} from '../types';
import type { State, AxiosResponse, Normalized } from '../../../common/types';
import { deviceListSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';
import { modals } from '../../../common/constants';


/**
 * Action creator
 */
export function provisionDevices(supplierCode: string, deviceIds: Array<string>): Action {
  return {
    type: 'devices/provisionDevices',
    deviceIds,
    supplierCode,
  };
}


/**
 * Export the type of the action creator
 */
export type ProvisionDevicesT = typeof provisionDevices;


/**
 * Action's logic
 */
export function* doProvisionDevices({ deviceIds, supplierCode }: ProvisionAction): Saga<void> {
  const devices = yield select((state: State) => deviceIds.map(id => state.devices.entities[id]));
  const modal = yield select((state: State) => state.common.ui.modal);

  yield put(({
    type: 'devices/provisionDevicesRequest',
  }: Action));

  try {
    const response: AxiosResponse<DeviceListResponse> =
      yield call(devicesApi.provisionDevices, supplierCode, devices);

    const normalizedData: Normalized<Device, 'devices'> = normalize(
      response.data.devices,
      deviceListSchema,
    );

    yield put(({
      type: 'devices/provisionDevicesSuccess',
      entities: normalizedData.entities.devices || {},
      keys: normalizedData.result,
    }: Action));

    NotificationManager.success('Devices provisioned successfully');
  } catch (e) {
    yield put(({
      type: 'devices/provisionDevicesFailure',
    }: Action));

    NotificationManager.error('Failed to provision devices');
  }

  if (modal === modals.PROVISION_DEVICES_MODAL) {
    yield put(closeModal());
  }
}


/**
 * Saga watcher
 */
export function* watchProvisionDevices(): Iterable<Effect> {
  yield takeLatest(('devices/provisionDevices': ActionType), doProvisionDevices);
}


/**
 * Action's reducer
 */
export function reducer(state: DeviceState, action: Action): DeviceState {
  switch (action.type) {
    case 'devices/provisionDevicesRequest':
      return {
        ...state,
        provisioning: true,
      };

    case 'devices/provisionDevicesSuccess': {
      const { keys } = action;

      return {
        ...state,
        provisioning: false,
        entities: mergeEntities(state, action),
        keys: state.keys.filter(id => !keys.includes(id)),
        supplier: {
          ...state.supplier,
          keys: uniq([...state.supplier.keys, ...action.keys]),
        },
      };
    }

    case 'devices/provisionDevicesFailure':
      return {
        ...state,
        provisioning: false,
      };

    default:
      return state;
  }
}
