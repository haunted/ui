// @flow


import { put, call, takeLatest } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { normalize } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import { reset } from 'redux-form';
import type {
  State,
  Action,
  DeviceNote,
  AddDeviceNoteAction,
  ActionType,
  Device,
} from '../types';
import { devices } from '../../../utils/apis';
import type { AxiosResponse, NormalizedSingle } from '../../../common/types';
import { deviceSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';


/**
 * Action creator
 */
export function addDeviceNote(
  device: Device,
  message: $PropertyType<DeviceNote, 'message'>,
): Action {
  return {
    type: 'devices/addDeviceNote',
    device,
    message,
  };
}


/**
 * Export the type of the action creator
 */
export type AddDeviceNoteT = typeof addDeviceNote;


/**
 * Action's logic
 */
export function* doAddDeviceNote({ device, message }: AddDeviceNoteAction): Saga<void> {
  yield put(({
    type: 'devices/addDeviceNoteRequest',
  }: Action));

  try {
    const response: AxiosResponse<Device> = yield call(devices.addDeviceNote, device, message);

    const normalizedData: NormalizedSingle<Device, 'devices'> = normalize(
      response.data,
      deviceSchema,
    );

    yield put(({
      type: 'devices/addDeviceNoteSuccess',
      entities: normalizedData.entities.devices,
      key: normalizedData.result,
    }: Action));

    yield put(reset('deviceNoteForm'));
  } catch (err) {
    yield put(({
      type: 'devices/addDeviceNoteFailure',
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to add a note',
    );
  }
}


/**
 * Saga watcher
 */
export function* watchAddDeviceNote(): Iterable<Effect> {
  yield takeLatest(('devices/addDeviceNote': ActionType), doAddDeviceNote);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'devices/addDeviceNoteRequest':
      return {
        ...state,
        creatingNote: true,
      };
    case 'devices/addDeviceNoteSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
        creatingNote: false,
      };
    case 'devices/addDeviceNoteFailure':
      return {
        ...state,
        creatingNote: false,
      };
    default:
      return state;
  }
}

