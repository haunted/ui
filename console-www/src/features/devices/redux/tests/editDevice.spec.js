/* eslint-disable max-len */
import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doEditDevice, reducer } from '../editDevice';
import { devices } from '../../../../utils/apis/index';
import devicesMock from '../../../../mocks/devices.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';
import history from '../../../../common/history';

const deviceData = devicesMock[0];
const changedDeviceData = {
  ...deviceData,
  carrier: 'AT&T',
};

describe('Edit Device Saga', () => {
  const err = new Error('error');

  it('should go through edit device process correctly', () => expectSaga(doEditDevice, { deviceData })
    .provide([
      [matchers.call.fn(devices.editDevice), wrapValidResponse(changedDeviceData)],
    ])
    .put({ type: 'devices/editDeviceRequest' })
    .put({
      type: 'devices/editDeviceSuccess',
      data: changedDeviceData,
    })
    .call(history.push, '/devices')
    .call([NotificationManager, NotificationManager.success], 'Device edited successfully')
    .run());

  it('should show notification when get device fails', () => expectSaga(doEditDevice, { deviceData })
    .provide([
      [matchers.call.fn(devices.editDevice), throwError(err)],
    ])
    .put({ type: 'devices/editDeviceRequest' })
    .put({ type: 'devices/editDeviceFailure' })
    .call([NotificationManager, NotificationManager.error], 'Failed to edit the device')
    .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'devices/editDeviceRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        editLoading: true,
      })
      .run();
  });

  it('should set loading state to `false` when request successes', () => {
    function* saga() {
      yield put({
        type: 'devices/editDeviceSuccess',
        data: changedDeviceData,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, {})
      .hasFinalState({
        editLoading: false,
        entities: {
          [deviceData.id]: changedDeviceData,
        },
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'devices/editDeviceFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        editLoading: false,
      })
      .run();
  });
});
