/* eslint-disable max-len */
import { put } from 'redux-saga/effects';
import { expectSaga } from 'redux-saga-test-plan';
import { normalize, schema } from 'normalizr';
import { NotificationManager } from 'react-notifications';
import * as matchers from 'redux-saga-test-plan/matchers';
import { throwError } from 'redux-saga-test-plan/providers';
import { doGetDevice, reducer } from '../getDevice';
import { devices } from '../../../../utils/apis/index';
import devicesMock from '../../../../mocks/devices.json';
import { wrapValidResponse } from '../../../../utils/test/axiosResponse';

const deviceSchema = new schema.Entity('devices');
const id = 'TWIN';

describe('Get Device Saga', () => {
  const err = new Error('error');
  const normalizedData = normalize(
    devicesMock[0],
    deviceSchema,
  );

  const initialState = {
    entities: {},
    keys: [],
    loading: false,
  };

  it('should go through get device process correctly', () => expectSaga(doGetDevice, id)
    .provide([
      [matchers.call.fn(devices.getDevice), wrapValidResponse(devicesMock[0])],
    ])
    .put({ type: 'devices/getDeviceRequest' })
    .put({
      type: 'devices/getDeviceSuccess',
      entities: normalizedData.entities.devices,
      key: normalizedData.result,
    })
    .run());

  it('should show notification when get device fails', () => expectSaga(doGetDevice, id)
    .provide([
      [matchers.call.fn(devices.getDevice), throwError(err)],
    ])
    .put({ type: 'devices/getDeviceRequest' })
    .put({ type: 'devices/getDeviceFailure' })
    .call([NotificationManager, NotificationManager.error], 'Failed to fetch the device')
    .run());

  it('should set loading state to `true` when request was made', () => {
    function* saga() {
      yield put({ type: 'devices/getDeviceRequest' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: true,
      })
      .run();
  });

  it('should set loading state to `false` when request successes', () => {
    function* saga() {
      yield put({
        type: 'devices/getDeviceSuccess',
        entities: normalizedData.entities.devices,
        key: normalizedData.result,
      });
    }

    return expectSaga(saga)
      .withReducer(reducer, initialState)
      .hasFinalState({
        loading: false,
        entities: normalizedData.entities.devices,
        keys: [normalizedData.result],
      })
      .run();
  });

  it('should set loading state to `false` when request fails', () => {
    function* saga() {
      yield put({ type: 'devices/getDeviceFailure' });
    }

    return expectSaga(saga)
      .withReducer(reducer)
      .hasFinalState({
        loading: false,
      })
      .run();
  });
});
