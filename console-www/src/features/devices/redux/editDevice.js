// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { devices } from '../../../utils/apis';
import type { ActionType, Action, State, EditAction, Device } from '../types';
import history from '../../../common/history';


/**
 * Action creator
 */
export function editDevice(deviceData: Device): Action {
  return {
    type: 'devices/editDevice',
    deviceData,
  };
}


/**
 * Export the type of the action creator
 */
export type EditDeviceT = typeof editDevice;


/**
 * Action's logic
 */
export function* doEditDevice({ deviceData }: EditAction): Saga<void> {
  yield put(({
    type: 'devices/editDeviceRequest',
  }: Action));

  try {
    const { data } = yield call(devices.editDevice, deviceData);

    yield put(({
      type: 'devices/editDeviceSuccess',
      data,
    }: Action));

    yield call(history.push, '/devices');
    yield call([NotificationManager, NotificationManager.success], 'Device edited successfully');
  } catch (e) {
    yield put(({
      type: 'devices/editDeviceFailure',
    }: Action));
    yield call([NotificationManager, NotificationManager.error], 'Failed to edit the device');
  }
}


/**
 * Saga watcher
 */
export function* watchEditDevice(): Iterable<Effect> {
  yield takeLatest(('devices/editDevice': ActionType), doEditDevice);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'devices/editDeviceRequest':
      return {
        ...state,
        editLoading: true,
      };

    case 'devices/editDeviceSuccess':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.data.id]: action.data,
        },
        editLoading: false,
      };

    case 'devices/editDeviceFailure':
      return {
        ...state,
        editLoading: false,
      };

    default:
      return state;
  }
}
