// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import uniq from 'lodash.uniq';
import { confirm } from '../../common/redux/actions';
import { devices } from '../../../utils/apis';
import { deviceSchema } from '../../../common/schemas';
import type { AxiosResponse, NormalizedSingle } from '../../../common/types';
import type {
  ActionType,
  Action,
  State,
  DeprovisionAction,
  Device,
} from '../types';


/**
 * Action creator
 */
export function deprovisionDevice(device: Device): Action {
  return {
    type: 'devices/deprovisionDevice',
    device,
  };
}


/**
 * Export the type of the action creator
 */
export type DeprovisionDeviceT = typeof deprovisionDevice;


/**
 * Action's logic
 */
export function* doDeprovisionDevice({ device }: DeprovisionAction): Saga<void> {
  if (
    (yield confirm('You are about to deprovision the device. Do you want to continue?')) === false
  ) {
    return;
  }

  yield put(({
    type: 'devices/deprovisionDeviceRequest',
    device,
  }: Action));

  try {
    const response: AxiosResponse<Device> = yield call(devices.deprovisionDevice, device);

    const normalizedData: NormalizedSingle<Device, 'devices'> = normalize(
      response.data,
      deviceSchema,
    );

    yield put(({
      type: 'devices/deprovisionDeviceSuccess',
      entities: normalizedData.entities.devices,
      key: normalizedData.result,
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.success],
      'Device deprovisioned successfully',
    );
  } catch (e) {
    yield put(({
      type: 'devices/deprovisionDeviceFailure',
      device,
    }: Action));

    yield call(
      [NotificationManager, NotificationManager.error],
      'Failed to deprovision the device',
    );
  }
}


/**
 * Saga watcher
 */
export function* watchDeprovisionDevice(): Iterable<Effect> {
  yield takeLatest(('devices/deprovisionDevice': ActionType), doDeprovisionDevice);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'devices/deprovisionDeviceRequest':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.device.id]: {
            ...state.entities[action.device.id],
            deprovisioning: true,
          },
        },
      };

    case 'devices/deprovisionDeviceSuccess': {
      const { key } = action;

      return {
        ...state,
        entities: {
          ...state.entities,
          ...action.entities,
        },
        keys: uniq([...state.keys, key]),
        supplier: {
          ...state.supplier,
          keys: state.supplier.keys.filter(id => id !== key),
        },
      };
    }

    case 'devices/deprovisionDeviceFailure':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.device.id]: {
            ...state.entities[action.device.id],
            deprovisioning: false,
          },
        },
      };

    default:
      return state;
  }
}
