// @flow

import { takeLatest, call, put } from 'redux-saga/effects';
import type { Saga, Effect } from 'redux-saga';
import { NotificationManager } from 'react-notifications';
import { normalize } from 'normalizr';
import uniq from 'lodash.uniq';
import { devices } from '../../../utils/apis';
import type {
  ActionType,
  Action,
  Device,
  State,
  GetDeviceActionT,
} from '../types';
import type { AxiosResponse, NormalizedSingle } from '../../../common/types';
import { deviceSchema } from '../../../common/schemas';
import { mergeEntities } from '../../../utils/redux';
import isAuthorized from '../../../utils/isAuthorized';


/**
 * Action creator
 */
export function getDevice(id: string): Action {
  return {
    type: 'devices/getDevice',
    id,
  };
}


/**
 * Export the type of the action creator
 */
export type GetDeviceT = typeof getDevice;


/**
 * Action's logic
 */
export function* doGetDevice({ id }: GetDeviceActionT): Saga<void> {
  yield put(({
    type: 'devices/getDeviceRequest',
  }: Action));

  try {
    const response: AxiosResponse<Device> =
      yield call(devices.getDevice, id);

    const normalizedData: NormalizedSingle<Device, 'devices'> = normalize(
      response.data,
      deviceSchema,
    );

    yield put(({
      type: 'devices/getDeviceSuccess',
      entities: normalizedData.entities.devices,
      key: normalizedData.result,
    }: Action));
  } catch (error) {
    yield put(({
      type: 'devices/getDeviceFailure',
    }: Action));

    if (isAuthorized(error)) {
      yield call([NotificationManager, NotificationManager.error], 'Failed to fetch the device');
    }
  }
}


/**
 * Saga watcher
 */
export function* watchGetDevice(): Iterable<Effect> {
  yield takeLatest(('devices/getDevice': ActionType), doGetDevice);
}


/**
 * Action's reducer
 */
export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'devices/getDeviceRequest':
      return {
        ...state,
        loading: true,
      };

    case 'devices/getDeviceSuccess':
      return {
        ...state,
        entities: mergeEntities(state, action),
        keys: uniq([...state.keys, action.key]),
        loading: false,
      };

    case 'devices/getDeviceFailure':
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
