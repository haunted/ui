// @flow

const styles = {
  modalTableWrapper: {
    overflowX: 'auto',

    '& table': {
      width: 'auto',
    },
  },
  modalNavigation: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: [24, 0],
  },
  provisionButton: {
    marginLeft: 24,
  },
  searchInput: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
};

export type StylesT = { [$Keys<typeof styles>]: string };
export default styles;
