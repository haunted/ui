// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import InfiniteScroll from 'redeam-styleguide/lib/InfiniteScroll';
import Button from 'redeam-styleguide/lib/Button';
import Modal from '../../common/Modal';
import { unprovisionedDevices } from '../redux/selectors';
import * as deviceActions from '../redux/actions';
import * as commonActions from '../../common/redux/actions';
import { modals } from '../../../common/constants';
import type { State } from '../../../common/types';
import type {
  Device,
  ListDevicesT,
  ProvisionDevicesT,
  SearchDevicesT,
  State as DeviceStateT,
} from '../types';
import type { CloseModalT } from '../../common/types';
import DeviceTable from '../DeviceTable';
import styles, { type StylesT } from './ProvisionDevicesModal.jss';

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

type PropsT = {
  classes: StylesT,
  listDevices: ListDevicesT,
  provisionDevices: ProvisionDevicesT,
  closeModal: CloseModalT,
  devices: {
    list: Array<Device>,
  } & DeviceStateT,
  nextOffset: number,
  supplierCode: string,
  searchDevices: SearchDevicesT,
  searchDevicesQuery: string,
};

type LocalStateT = {
  devicesToProvision: Array<string>,
};

class ProvisionDevicesModal extends Component<PropsT, LocalStateT> {
  handleDeviceSelect: (Device, boolean) => void;
  handleAllDeviceSelect: (boolean, Array<Device>) => void;
  closeProvisionModal: () => void;

  constructor(props) {
    super(props);

    this.handleDeviceSelect = this.handleDeviceSelect.bind(this);
    this.handleAllDeviceSelect = this.handleAllDeviceSelect.bind(this);
    this.closeProvisionModal = this.closeProvisionModal.bind(this);

    this.state = {
      devicesToProvision: [],
    };

    // TODO check if it's ok to call apis from constructor
    this.props.listDevices({ offset: 0 });
  }

  static getDerivedStateFromProps(props, state) {
    const { devicesToProvision } = state;
    const deviceIdList = props.devices.keys;

    // remove devices that have already been provisioned
    if (devicesToProvision.some(id => !deviceIdList.includes(id))) {
      return {
        devicesToProvision: [],
      };
    }

    return null;
  }

  handleDeviceSelect(row: Device, isSelected: boolean) {
    const { devicesToProvision } = this.state;

    if (isSelected) {
      this.setState({
        devicesToProvision: [...devicesToProvision, row.id],
      });
    } else {
      this.setState({
        devicesToProvision: devicesToProvision.filter(id => id !== row.id),
      });
    }
  }

  handleAllDeviceSelect(isSelected: boolean, rows: Array<Device>) {
    if (isSelected) {
      this.setState({ devicesToProvision: rows.map(r => r.id) });
    } else {
      this.setState({ devicesToProvision: [] });
    }
  }

  closeProvisionModal() {
    const { closeModal } = this.props;

    this.setState({
      devicesToProvision: [],
    });


    closeModal();
  }

  render() {
    const {
      classes,
      devices,
      nextOffset,
      provisionDevices,
      listDevices,
      supplierCode,
      searchDevices,
      searchDevicesQuery,
    } = this.props;

    const {
      devicesToProvision,
    } = this.state;

    const selectRow = {
      mode: 'checkbox',
      clickToSelect: true,
      classes: '',
      selected: devicesToProvision,
      onSelect: this.handleDeviceSelect,
      onSelectAll: this.handleAllDeviceSelect,
    };

    return (
      <Modal id={modals.PROVISION_DEVICES_MODAL} title="Provision New Device">
        <div className={classes.searchInput}>
          <input
            onChange={e => searchDevices(e.target.value)}
            value={searchDevicesQuery}
            placeholder="Search devices..."
          />
        </div>
        <div className={classes.modalTableWrapper}>
          <InfiniteScroll
            disabled
            loading={devices.loading}
            loader={loader}
            loadMore={() => listDevices({ offset: nextOffset })}
            hasMore={devices.hasMore}
          >
            <DeviceTable
              data={devices.list}
              selectRow={selectRow}
              loading={devices.loading}
            />
          </InfiniteScroll>
        </div>

        <div className={classes.modalNavigation}>
          <Button tertiary onClick={this.closeProvisionModal}>
            Cancel
          </Button>
          <Button
            primary
            className={classes.provisionButton}
            disabled={devices.provisioning || !devicesToProvision.length}
            onClick={() => provisionDevices(supplierCode, devicesToProvision)}
          >
            Provision
          </Button>
        </div>
      </Modal>
    );
  }
}

const mapStateToProps = (state: State) => {
  const {
    devices,
  } = state;

  const { limit, offset } = devices.listQueryParams;

  return {
    devices: {
      ...devices,
      list: unprovisionedDevices(state),
    },
    searchDevicesQuery: devices.ui.searchDevicesQuery,
    nextOffset: offset + limit,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...commonActions,
  ...deviceActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(ProvisionDevicesModal));
