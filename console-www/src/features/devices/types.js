// @flow

import type { ListQueryParams } from '../../common/types';
import type { DeviceNoteStatusT, DeviceStatusT } from '../../common/constants';


/**
 *  Base type
 */
export type DeviceNote = {
  id: string,
  status: DeviceNoteStatusT,
  message: string,
  createdAt: string,
  deletedAt: ?string,
  author: string,
  deletedBy: ?string,
  deleting: ?boolean,
};

export type Device = {
  id: string,
  assetId: string,
  imei: string,
  serialNumber: string,
  simNumber: string,
  deviceType: string,
  phoneNumber: string,
  carrier: string,
  supplierCode: string,
  notes: Array<DeviceNote>,
  status: DeviceStatusT,
  created: string,
  version: number,
  deprovisioning?: boolean,
  deactivating?: boolean,
  deleting?: boolean,
};


/**
 * Helper types
 */
export type Entities = { [id: string]: Device };
export type Keys = Array<string>;


/**
 * State type
 */
export type State = {
  entities: Entities,
  keys: Keys,
  loading: boolean,
  provisioning: boolean,
  creatingNote: boolean,
  hasMore: boolean,
  listQueryParams: ListQueryParams,
  supplier: {
    keys: Keys,
    loading: boolean,
  },
  ui: {
    searchDevicesQuery: string,
  }
};


/**
 * Response types
 */
export type DeviceListResponse = {
  devices: Array<Device>,
};


/**
 * Action types
 */
type ListSupplierDevices = 'devices/listSupplierDevices';
type ListSupplierDevicesRequest = 'devices/listSupplierDevicesRequest';
type ListSupplierDevicesSuccess = 'devices/listSupplierDevicesSuccess';
type ListSupplierDevicesFailure = 'devices/listSupplierDevicesFailure';
type ProvisionDevices = 'devices/provisionDevices';
type ProvisionDevicesRequest = 'devices/provisionDevicesRequest';
type ProvisionDevicesSuccess = 'devices/provisionDevicesSuccess';
type ProvisionDevicesFailure = 'devices/provisionDevicesFailure';
type DeprovisionDevice = 'devices/deprovisionDevice';
type DeprovisionDeviceRequest = 'devices/deprovisionDeviceRequest';
type DeprovisionDeviceSuccess = 'devices/deprovisionDeviceSuccess';
type DeprovisionDeviceFailure = 'devices/deprovisionDeviceFailure';
type DeactivateDevice = 'devices/deactivateDevice';
type DeactivateDeviceRequest = 'devices/deactivateDeviceRequest';
type DeactivateDeviceSuccess = 'devices/deactivateDeviceSuccess';
type DeactivateDeviceFailure = 'devices/deactivateDeviceFailure';
type ListDevices = 'devices/listDevices';
type ListDevicesRequest = 'devices/listDevicesRequest';
type ListDevicesSuccess = 'devices/listDevicesSuccess';
type ListDevicesFailure = 'devices/listDevicesFailure';
type GetDevice = 'devices/getDevice';
type GetDeviceRequest = 'devices/getDeviceRequest';
type GetDeviceSuccess = 'devices/getDeviceSuccess';
type GetDeviceFailure = 'devices/getDeviceFailure';
type EditDevice = 'devices/editDevice';
type EditDeviceRequest = 'devices/editDeviceRequest';
type EditDeviceSuccess = 'devices/editDeviceSuccess';
type EditDeviceFailure = 'devices/editDeviceFailure';
type AddDeviceNote = 'devices/addDeviceNote';
type AddDeviceNoteRequest = 'devices/addDeviceNoteRequest';
type AddDeviceNoteSuccess = 'devices/addDeviceNoteSuccess';
type AddDeviceNoteFailure = 'devices/addDeviceNoteFailure';
type DeleteDeviceNote = 'devices/deleteDeviceNote';
type DeleteDeviceNoteRequest = 'devices/deleteDeviceNoteRequest';
type DeleteDeviceNoteSuccess = 'devices/deleteDeviceNoteSuccess';
type DeleteDeviceNoteFailure = 'devices/deleteDeviceNoteFailure';
type SearchDevicesT = 'devices/searchDevices';
type ResetDeviceSearchT = 'devices/resetDeviceSearch';

export type ActionType =
  | ListSupplierDevices
  | ListSupplierDevicesRequest
  | ListSupplierDevicesSuccess
  | ListSupplierDevicesFailure
  | ProvisionDevices
  | ProvisionDevicesRequest
  | ProvisionDevicesSuccess
  | ProvisionDevicesFailure
  | DeprovisionDevice
  | DeprovisionDeviceRequest
  | DeprovisionDeviceSuccess
  | DeprovisionDeviceFailure
  | DeactivateDevice
  | DeactivateDeviceRequest
  | DeactivateDeviceSuccess
  | DeactivateDeviceFailure
  | ListDevices
  | ListDevicesRequest
  | ListDevicesSuccess
  | ListDevicesFailure
  | GetDevice
  | GetDeviceRequest
  | GetDeviceSuccess
  | GetDeviceFailure
  | EditDevice
  | EditDeviceRequest
  | EditDeviceSuccess
  | EditDeviceFailure
  | AddDeviceNote
  | AddDeviceNoteRequest
  | AddDeviceNoteSuccess
  | AddDeviceNoteFailure
  | DeleteDeviceNote
  | DeleteDeviceNoteRequest
  | DeleteDeviceNoteSuccess
  | DeleteDeviceNoteFailure
  | SearchDevicesT
  | ResetDeviceSearchT;

export type ListDevicesAction = {
  type: ListDevices,
  params: $Shape<ListQueryParams>,
};

export type ListSupplierDevicesAction = {
  type: ListSupplierDevices,
  code: string,
};

export type GetDeviceActionT = {
  type: GetDevice,
  id: string,
};

export type ProvisionAction = {
  type: ProvisionDevices,
  deviceIds: Array<string>,
  supplierCode: string,
};

export type DeprovisionAction = {
  type: DeprovisionDevice,
  device: Device,
};

export type EditAction = {
  type: EditDevice,
  deviceData: Device,
};

export type DeactivateAction = {
  type: DeactivateDevice,
  device: Device,
  reason: string,
};

export type AddDeviceNoteAction = {
  type: AddDeviceNote,
  device: Device,
  message: $PropertyType<DeviceNote, 'message'>,
};

export type DeleteDeviceNoteAction = {
  type: DeleteDeviceNote,
  deviceId: $PropertyType<Device, 'id'>,
  noteId: $PropertyType<DeviceNote, 'id'>,
};

export type SearchDevicesActionT = {
  type: SearchDevicesT,
  query: string,
};

export type Action =
  | ListSupplierDevicesAction
  | { type: ListSupplierDevicesRequest }
  | {
      type: ListSupplierDevicesSuccess,
      entities: Entities,
      keys: Keys,
    }
  | { type: ListSupplierDevicesFailure }
  | ProvisionAction
  | { type: ProvisionDevicesRequest }
  | {
      type: ProvisionDevicesSuccess,
      entities: Entities,
      keys: Keys,
    }
  | { type: ProvisionDevicesFailure }
  | DeprovisionAction
  | {
      type: DeprovisionDeviceRequest,
      device: Device,
    }
  | {
      type: DeprovisionDeviceSuccess,
      entities: Entities,
      key: string,
    }
  | {
      type: DeprovisionDeviceFailure,
      device: Device,
    }
  | DeactivateAction
  | {
      type: DeactivateDeviceRequest,
      id: $PropertyType<Device, 'id'>,
    }
  | {
      type: DeactivateDeviceSuccess,
      entities: Entities,
      key: $PropertyType<Device, 'id'>,
    }
  | {
      type: DeactivateDeviceFailure,
      id: $PropertyType<Device, 'id'>,
    }
  | {
      type: ListDevices,
    }
  | {
      type: ListDevicesRequest,
      listQueryParams: ListQueryParams,
    }
  | {
      type: ListDevicesSuccess,
      entities: Entities,
      keys: Keys,
      listQueryParams: ListQueryParams,
    }
  | { type: ListDevicesFailure }
  | {
      type: GetDevice,
    }
  | { type: GetDeviceRequest }
  | {
      type: GetDeviceSuccess,
      entities: Entities,
      key: string,
    }
  | { type: GetDeviceFailure }
  | EditAction
  | { type: EditDeviceRequest }
  | {
      type: EditDeviceSuccess,
      data: Device,
    }
  | { type: EditDeviceFailure }
  | AddDeviceNoteAction
  | { type: AddDeviceNoteRequest }
  | {
      type: AddDeviceNoteSuccess,
      entities: Entities,
      key: $PropertyType<Device, 'id'>,
    }
  | { type: AddDeviceNoteFailure }
  | DeleteDeviceNoteAction
  | {
      type: DeleteDeviceNoteRequest,
      deviceId: $PropertyType<Device, 'id'>,
      noteId: $PropertyType<DeviceNote, 'id'>,
    }
  | {
      type: DeleteDeviceNoteSuccess,
      entities: Entities,
      key: string,
    }
  | { type: DeleteDeviceNoteFailure }
  | {
      type: SearchDevicesT,
      query: string,
    }
  | {
      type: ResetDeviceSearchT,
    };


/**
 * Action creator types
 */
export { GetDeviceT } from './redux/getDevice';
export { EditDeviceT } from './redux/editDevice';
export { ListDevicesT } from './redux/listDevices';
export { ListSupplierDevicesT } from './redux/listSupplierDevices';
export { DeprovisionDeviceT } from './redux/deprovisionDevice';
export { DeactivateDeviceT } from './redux/deactivateDevice';
export { ProvisionDevicesT } from './redux/provisionDevices';
export { AddDeviceNoteT } from './redux/addDeviceNote';
export { DeleteDeviceNoteT } from './redux/deleteDeviceNote';
export { SearchDevicesT } from './redux/searchDevices';
export { ResetDeviceSearchT } from './redux/resetDeviceSearch';


/**
 * Other types
 */
export type DeactivateFormDataT = {
  reason: string,
};
