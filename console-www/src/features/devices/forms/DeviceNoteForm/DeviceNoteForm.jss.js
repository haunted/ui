// @flow

const styles = {
  wrapper: {
    display: 'flex',
  },
  message: {
    flex: '1 0 auto',
    marginRight: 24,
  },
  button: {
    marginTop: 16,
  },
};

export type Styles = {[$Keys<typeof styles>]: string};
export default styles;
