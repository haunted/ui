// @flow

import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import type { FormProps } from 'redux-form';
import { TextField } from 'redeam-styleguide/lib/formElements';
import Button from 'redeam-styleguide/lib/Button';
import { withStyles } from 'redeam-styleguide/lib/utils';
import styles from './DeviceNoteForm.jss';
import type { Styles } from './DeviceNoteForm.jss';

type Props = {
  loading: boolean,
  classes: Styles,
} & FormProps;

class DeviceNoteForm extends Component<Props> {
  render() {
    const {
      handleSubmit,
      loading,
      classes,
    } = this.props;

    return (
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className={classes.wrapper}>
          <Field
            name="message"
            placeholder="Write a note..."
            component={TextField}
            wrapperClassName={classes.message}
            disabled={loading}
          />

          <Button
            tertiary
            type="submit"
            className={classes.button}
            disabled={loading}
          >
            Add Note
          </Button>
        </div>
      </form>
    );
  }
}

export const initialValues = {
  message: '',
};

export const validate = (values: {[$Keys<typeof initialValues>]: string}) => {
  const errors: {[$Keys<typeof initialValues>]: string} = {};

  if (!values.message.trim()) {
    errors.message = 'Message cannot be empty';
  }

  return errors;
};

export default reduxForm({
  form: 'deviceNoteForm',
  initialValues,
  validate,
})(withStyles(styles)(DeviceNoteForm));
