// @flow

const styles = {
  wrapper: {
    margin: [24, 0],
  },
  navigation: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  submitBtn: {
    marginLeft: 24,
  },
};

export type Styles = {[$Keys<typeof styles>]: string};
export default styles;
