// @flow

import React, { Component } from 'react';
import { reduxForm, Field, type FormProps } from 'redux-form';
import Button from 'redeam-styleguide/lib/Button';
import { TextField } from 'redeam-styleguide/lib/formElements';
import { withStyles } from 'redeam-styleguide/lib/utils';
import styles from './DeviceDeactivateForm.jss';
import type { Styles } from './DeviceDeactivateForm.jss';

type Props = {
  classes: Styles,
  onCancel: ?(...args: Array<any>) => any,
  loading: ?boolean,
} & FormProps;

class DeviceDeactivateForm extends Component<Props> {
  render() {
    const {
      handleSubmit,
      onCancel,
      loading,
      classes,
    } = this.props;

    return (
      <form onSubmit={handleSubmit} autoComplete="off">
        <div className={classes.wrapper}>
          <Field
            name="reason"
            label="Reason"
            placeholder="Write a reason..."
            component={TextField}
            disabled={loading}
          />

          <div className={classes.navigation}>
            {onCancel && (
              <Button tertiary onClick={onCancel}>Cancel</Button>
            )}

            <Button
              primary
              className={classes.submitBtn}
              type="submit"
              disabled={loading}
            >
              Deactivate
            </Button>
          </div>
        </div>
      </form>
    );
  }
}

export const initialValues = {
  reason: '',
};

export const validate = (values: {[$Keys<typeof initialValues>]: string}) => {
  const errors: {[$Keys<typeof initialValues>]: string} = {};

  if (!values.reason.trim()) {
    errors.reason = 'Reason cannot be empty';
  }

  return errors;
};

export default reduxForm({
  form: 'deviceDeactivateForm',
  initialValues,
  validate,
})(withStyles(styles)(DeviceDeactivateForm));
