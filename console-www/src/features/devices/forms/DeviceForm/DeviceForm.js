// @flow

import React, { Component } from 'react';
import { reduxForm, Field, type FormProps } from 'redux-form';
import { withStyles } from 'redeam-styleguide/lib/utils';
import { TextField } from 'redeam-styleguide/lib/formElements';
import Button from 'redeam-styleguide/lib/Button';
import history from '../../../../common/history';
import { formatPhone, normalizePhone } from '../../../../utils/forms/phoneNumber';
import { normalizeAssetId } from '../../../../utils/forms/normalizeAssetId';
import styles, { type Styles } from './DeviceForm.jss';
import type { Device } from '../../types';

type Props = FormProps & {
  handleSubmit: Function,
  classes: Styles,
}

export class DeviceForm extends Component<Props> {
  render() {
    const {
      classes,
      initialValues,
      handleSubmit,
      formLoading,
    } = this.props;

    return (
      <form onSubmit={handleSubmit} className={classes.form}>
        <div className={classes.formField}>
          <Field
            label="Device Type"
            name="deviceType"
            component="select"
            disabled={formLoading}
          >
            <option value="AXLG">LG Tablet</option>
            <option value="RDHH">C70</option>
          </Field>
        </div>

        <div className={classes.formField}>
          <Field
            label="Asset ID"
            name="assetId"
            component={TextField}
            disabled={formLoading}
            normalize={normalizeAssetId}
          />
        </div>

        <div className={classes.formField}>
          <span>Serial Number: {initialValues.serialNumber}</span>
        </div>

        <div className={classes.formField}>
          <span>IMEI Number: {initialValues.imei}</span>
        </div>

        <div className={classes.formField}>
          <Field
            label="Sim Number"
            name="simNumber"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formField}>
          <Field
            label="Phone Number"
            name="phoneNumber"
            component={TextField}
            format={formatPhone}
            normalize={normalizePhone}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formField}>
          <Field
            label="Carrier"
            name="carrier"
            component={TextField}
            disabled={formLoading}
          />
        </div>

        <div className={classes.formNavigation}>
          <Button
            tertiary
            onClick={() => history.push('/devices')}
            className={classes.cancelButton}
          >
            Cancel
          </Button>

          <Button primary type="submit">Save</Button>
        </div>
      </form>
    );
  }
}

export const validate = (values: Device) => {
  const errors = {};

  if (!values.assetId) {
    errors.assetId = 'Asset ID cannot be empty';
  }

  if (values.phoneNumber.length !== 10) {
    errors.phoneNumber = 'Phone number is not valid';
  }

  return errors;
};

export const initialValues = {
  deviceType: '',
  assetId: '',
  serialNumber: '',
  imei: '',
  simNumber: '',
  phoneNumber: '',
  carrier: '',
};

export default reduxForm({
  form: 'deviceForm',
  validate,
  initialValues,
})(withStyles(styles)(DeviceForm));
