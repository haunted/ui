// @flow

import React, { Component } from 'react';
import type { Node } from 'react';
import Table from 'redeam-styleguide/lib/Table';
import type { Device } from '../types';

const columns = [
  {
    text: 'Asset Id',
    dataField: 'assetId',
    formatter: (e: any, { deviceType, assetId }: Device) => <span>{deviceType}{assetId}</span>,
  },
  {
    text: 'UUID',
    dataField: 'id',
  },
  {
    text: 'Serial Number',
    dataField: 'serialNumber',
  },
  {
    text: 'IMEI',
    dataField: 'imei',
  },
  {
    text: 'Sim Number',
    dataField: 'simNumber',
  },
  {
    text: 'Phone Number',
    dataField: 'phoneNumber',
  },
  {
    text: 'Carrier',
    dataField: 'carrier',
  },
];

type Props = {
  data: Array<Device>,
  loading: boolean,
  actionColumn: {
    dataField: string,
    text: string,
    formatter: (any, Device) => Node
  },
};

class DeviceTable extends Component<Props> {
  static defaultProps = {
    actionColumn: null,
  };

  getColumns() {
    const { actionColumn } = this.props;

    if (actionColumn) {
      return [
        ...columns,
        actionColumn,
      ];
    }

    return columns;
  }

  render() {
    const {
      data,
      loading,
      ...otherProps
    } = this.props;

    return (
      <Table
        keyField="id"
        data={data}
        columns={this.getColumns()}
        noDataIndication={() => !loading && 'No devices found'}
        {...otherProps}
      />
    );
  }
}

export default DeviceTable;
