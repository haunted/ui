// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import Modal from '../../common/Modal';
import DeviceNoteForm from '../forms/DeviceNoteForm';
import * as deviceActions from '../redux/actions';
import type { Device, DeviceNote, AddDeviceNoteT, DeleteDeviceNoteT } from '../types';
import { modals } from '../../../common/constants';
import styles, { type StylesT } from './NotesModal.jss';
import type { State } from '../../../common/types';


type PropsT = {
  classes: StylesT,
  device: Device,
  creatingNote: boolean,
  addDeviceNote: AddDeviceNoteT,
  deleteDeviceNote: DeleteDeviceNoteT,
};

class NotesModal extends Component<PropsT> {
  render() {
    const {
      classes,
      device,
      creatingNote,
      addDeviceNote,
      deleteDeviceNote,
    } = this.props;

    return (
      <Modal id={modals.DEVICE_NOTES_MODAL} title="Device Notes">
        <ul className={classes.list}>
          {!device.notes.length && (
            <p className={classes.noNotes}>Device has not any notes</p>
          )}

          {device.notes.map(note => (
            <li key={note.id} className={classes.noteContainer}>
              <div className={classes.note}>
                <span className={classes.note}>{note.message}</span>

                <div>
                  <button
                    onClick={() => deleteDeviceNote(device.id, note.id)}
                    disabled={note.deleting}
                  >
                    Delete
                  </button>
                </div>
              </div>
            </li>
          ))}
        </ul>

        <DeviceNoteForm
          onSubmit={({ message }: $Shape<DeviceNote>) => addDeviceNote(device, message)}
          loading={creatingNote}
        />
      </Modal>
    );
  }
}

const mapStateToProps = (state: State) => {
  const { devices } = state;

  return {
    creatingNote: devices.creatingNote,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...deviceActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(NotesModal));
