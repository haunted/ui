// @flow

const styles = {
  list: {
    paddingLeft: 16,
  },
  noteContainer: {
    margin: [12, 0],
  },
  note: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  message: {
    paddingRight: 16,
  },
  noNotes: {
    textAlign: 'center',
  },
};

export type StylesT = { [$Keys<typeof styles>]: string };
export default styles;
