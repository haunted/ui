// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import InfiniteScroll from 'redeam-styleguide/lib/InfiniteScroll';
import DeviceTable from '../DeviceTable';
import * as devicesActions from '../redux/actions';
import { unprovisionedDevices } from '../redux/selectors';
import type { Device, ListDevicesT, SearchDevicesT, ResetDeviceSearchT } from '../types';
import type { State } from '../../../common/types';
import history from '../../../common/history';
import styles, { type Styles } from './DeviceListView.jss';
import tableChangeHandler from '../../../utils/tableChangeHandler';

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

type Props = {
  listDevices: ListDevicesT,
  resetDeviceSearch: ResetDeviceSearchT,
  devices: Array<Device>,
  loading: boolean,
  hasMore: boolean,
  nextOffset: number,
  classes: Styles,
  searchDevices: SearchDevicesT,
  searchDevicesQuery: string,
};

class DeviceListView extends Component<Props> {
  componentWillMount() {
    const {
      resetDeviceSearch,
      listDevices,
    } = this.props;

    resetDeviceSearch();
    listDevices({ offset: 0 });
  }

  getActionColumn() {
    return {
      dataField: 'actions',
      text: 'Actions',
      formatter: (e, { id }) => (
        <div>
          <button onClick={() => history.push(`/devices/${id}/edit`)}>Edit</button>
        </div>
      ),
    };
  }

  render() {
    const {
      devices,
      nextOffset,
      loading,
      hasMore,
      classes,
      listDevices,
      searchDevices,
      searchDevicesQuery,
    } = this.props;

    return (
      <div>
        <div className={classes.header}>
          <h1>Devices</h1>

          <input
            onChange={e => searchDevices(e.target.value)}
            value={searchDevicesQuery}
            placeholder="Search devices..."
          />
        </div>

        <InfiniteScroll
          loading={loading}
          loader={loader}
          hasMore={hasMore}
          loadMore={() => listDevices({ offset: nextOffset })}
        >
          <DeviceTable
            data={devices}
            actionColumn={this.getActionColumn()}
            onTableChange={tableChangeHandler(listDevices)}
            loading={loading}
          />
        </InfiniteScroll>
      </div>
    );
  }
}

const mapStateToProps = (state: State) => {
  const {
    devices,
  } = state;

  const { limit, offset } = devices.listQueryParams;

  return {
    devices: unprovisionedDevices(state),
    loading: devices.loading,
    hasMore: devices.hasMore,
    searchDevicesQuery: devices.ui.searchDevicesQuery,
    nextOffset: offset + limit,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...devicesActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(DeviceListView));
