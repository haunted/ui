// @flow

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, type Dispatch } from 'redux';
import { withStyles } from 'redeam-styleguide/lib/utils';
import Button from 'redeam-styleguide/lib/Button';
import * as devicesActions from '../redux/actions';
import * as commonActions from '../../common/redux/actions';
import { supplierDeviceListSelector } from '../redux/selectors';
import DeviceTable from '../DeviceTable';
import NotesModal from '../NotesModal';
import DeactivateDeviceModal from '../DeactivateDeviceModal';
import ProvisionDevicesModal from '../ProvisionDevicesModal';
import type {
  Device,
  State as DeviceState,
  ListSupplierDevicesT,
  DeprovisionDeviceT,
  ResetDeviceSearchT,
} from '../types';
import type { State } from '../../../common/types';
import { modals } from '../../../common/constants';
import styles, { type Styles } from './SupplierDeviceListView.jss';
import type { OpenModalT } from '../../common/redux/openModal';

const loader = <div style={{ padding: 24, textAlign: 'center' }}>loading...</div>;

type Props = {
  listSupplierDevices: ListSupplierDevicesT,
  deprovisionDevice: DeprovisionDeviceT,
  resetDeviceSearch: ResetDeviceSearchT,
  loading: boolean,
  devices: {
    list: Array<Device>,
  } & DeviceState,
  openModal: OpenModalT,
  supplierCode: string,
  classes: Styles,
};

type LocalStateT = {
  deviceIdForModal: ?string,
};


class SupplierDeviceListView extends Component<Props, LocalStateT> {
  constructor(props) {
    super(props);

    this.state = {
      deviceIdForModal: null,
    };
  }

  componentWillMount() {
    const {
      listSupplierDevices,
      resetDeviceSearch,
      supplierCode,
    } = this.props;

    resetDeviceSearch();
    listSupplierDevices(supplierCode);
  }

  getSupplierDevicesTableActions() {
    const {
      deprovisionDevice,
    } = this.props;

    return {
      dataField: 'actions',
      text: 'Actions',
      formatter: (e, device: Device) => (
        <div>
          <button onClick={() => this.openNotesModal(device)}>
            {device.notes.length ? (
              <span>Notes [{device.notes.length}]</span>
            ) : (
              <span>Add a note</span>
            )}
          </button>

          <button
            disabled={device.deprovisioning}
            onClick={() => deprovisionDevice(device)}
          >
            Deprovision
          </button>

          <button onClick={() => this.openDeactivateModal(device)}>
            Deactivate
          </button>
        </div>
      ),
    };
  }

  openDeactivateModal(device: Device) {
    const { openModal } = this.props;

    this.setState({
      deviceIdForModal: device.id,
    });

    openModal(modals.DEACTIVATE_DEVICE_MODAL);
  }

  openNotesModal(device: Device) {
    const { openModal } = this.props;

    this.setState({
      deviceIdForModal: device.id,
    });

    openModal(modals.DEVICE_NOTES_MODAL);
  }

  render() {
    const {
      loading,
      devices,
      classes,
      openModal,
      supplierCode,
    } = this.props;

    const { deviceIdForModal } = this.state;

    return (
      <div>
        <div className={classes.header}>
          <h1>Supplier Devices</h1>

          <Button
            primary
            onClick={() => openModal(modals.PROVISION_DEVICES_MODAL)}
          >
            Provision New Device
          </Button>

          <ProvisionDevicesModal supplierCode={supplierCode} />
        </div>

        <DeviceTable
          data={devices.list}
          actionColumn={this.getSupplierDevicesTableActions()}
          loading={loading}
        />

        {deviceIdForModal && (
          <div>
            <NotesModal device={devices.entities[deviceIdForModal]} />
            <DeactivateDeviceModal device={devices.entities[deviceIdForModal]} />
          </div>
        )}

        {loading && loader}
      </div>
    );
  }
}


const mapStateToProps = (state: State, { match }) => {
  const { devices } = state;

  return {
    loading: devices.supplier.loading,
    devices: {
      ...devices,
      list: supplierDeviceListSelector(state),
    },
    supplierCode: match.params.supplierCode,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<*>) => bindActionCreators({
  ...devicesActions,
  ...commonActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(SupplierDeviceListView));
