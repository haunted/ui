// @flow

const styles = {
  '@global *': {
    boxSizing: 'border-box',
  },
};

export type StylesT = { [$Keys<typeof styles>]: string };
export default styles;
