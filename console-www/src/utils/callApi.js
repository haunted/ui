import axios from 'axios';
import history from '../common/history';
import isAuthorized from './isAuthorized';


/**
 * Reading API URL from environment variables
 */
const API_URL = process.env.REACT_APP_API_URL;


/**
 * Default axios configuration
 */
const baseConfig = {
  baseURL: API_URL,
  timeout: 10000,
  withCredentials: true,
  headers: {
    'Content-Type': 'application/json',
  },
};

const callApi = axios.create(baseConfig);

// todo: add an interceptor to handle server errors


/**
 * Listen to 401 or 403 response status code to redirect user to Login View.
 * The requested page will be preserved after login.
 */
callApi.interceptors.response.use(config => config, (error) => {
  const { location } = history;

  if (!isAuthorized(error) && location.pathname !== '/login') {
    /**
     * Pass the current location to the LoginView to be redirected to after login.
     * Useful when the session expires while you're using the app so that you will be on the same
     * view even you've got logged out.
     */
    history.push('/login', { referrer: location });
  }

  return Promise.reject(error);
});

export default callApi;
