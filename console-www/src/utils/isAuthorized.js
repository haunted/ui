// @flow

import get from 'lodash.get';
import { UNAUTHORIZED_STATUSES } from '../common/constants';
import type { AxiosError } from '../common/types';

function isAuthorized(error: AxiosError) {
  return !UNAUTHORIZED_STATUSES.includes(get(error, 'response.status'));
}

export default isAuthorized;
