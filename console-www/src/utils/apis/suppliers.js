// @flow

import callApi from '../callApi';
import qs from '../qs';
import type { ListQueryParams } from '../../common/types';
import type { SupplierFormData, Supplier } from '../../features/suppliers/types';

export const listSuppliers = (params: ListQueryParams) => callApi.get(`/suppliers?${qs(params)}`);

export const addSupplier = (data: SupplierFormData) => callApi.post('/suppliers', data);

export const getSupplier = (code: string) => callApi.get(`/suppliers/${code}`);

export const editSupplier = (data: Supplier) => callApi.put(`/suppliers/${data.code}`, data);

