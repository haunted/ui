// @flow

import callApi from '../callApi';
import qs from '../qs';
import type { User } from '../../features/users/types';
import type { ListQueryParams } from '../../common/types';

export const listUsers = (params: ListQueryParams) => callApi.get(`/users?${qs(params)}`);

export const getSelfUser = () => callApi.get('/users/me');

export const editSelfUser = (data: User) => callApi.put('/users/me', data);

export const deleteUser = (id: string) => callApi.delete(`/users/${id}`);

export const addUser = (data: User) => callApi.post('/users', data);
