// @flow

import callApi from '../callApi';
import qs from '../qs';
import type { Device, DeviceNote } from '../../features/devices/types';
import type { ListQueryParams } from '../../common/types';


export const listSupplierDevices = (supplierCode: string) =>
  callApi.get(`/suppliers/${supplierCode}/devices`);

export const getDevice = (deviceId: string) => callApi.get(`/devices/${deviceId}`);

export const listDevices = (params: ListQueryParams) => callApi.get(`/devices?${qs(params)}`);

export const editDevice =
  (deviceData: Device) => callApi.put(`/devices/${deviceData.id}`, deviceData);

export const provisionDevices = (supplierCode: string, devices: Array<Device>) =>
  callApi.put(`/suppliers/${supplierCode}/provision`, { devices });

export const deprovisionDevice = (device: Device) =>
  callApi.post(`/devices/${device.id}/deprovision`, device);

export const deactivateDevice = (device: Device, reason: string) =>
  callApi.delete(`/devices/${device.id}/deactivate`, { data: { ...device, reason } });

export const addDeviceNote = (device: Device, message: string) =>
  callApi.post(`/devices/${device.id}/notes`, { version: device.version, message });

export const deleteDeviceNote = (
  deviceId: $PropertyType<Device, 'id'>,
  noteId: $PropertyType<DeviceNote, 'id'>,
) => callApi.delete(`/devices/${deviceId}/notes/${noteId}`);
