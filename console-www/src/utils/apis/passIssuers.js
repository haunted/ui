// @flow

import callApi from '../callApi';
import type { PassIssuer, PassIssuerFormData } from '../../features/passIssuers/types';
import qs from '../qs';
import type { ListQueryParams } from '../../common/types';

export const listPassIssuers = (params: ListQueryParams) =>
  callApi.get(`/passissuers?${qs(params)}`);

export const getPassIssuer = (code: string) => callApi.get(`/passissuers/${code}`);

export const editPassIssuer = (data: PassIssuer) => callApi.put(`/passissuers/${data.code}`, data);

export const addPassIssuer = (data: PassIssuerFormData) => callApi.post('/passissuers', data);
