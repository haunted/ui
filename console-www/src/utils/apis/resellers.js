// @flow

import callApi from '../callApi';
import type { ListQueryParams } from '../../common/types';
import qs from '../qs';
import type { ResellerFormData, Reseller } from '../../features/resellers/types';

export const listResellers = (params: ListQueryParams) => callApi.get(`/resellers?${qs(params)}`);

export const addReseller = (data: ResellerFormData) => callApi.post('/resellers', data);

export const getReseller = (code: string) => callApi.get(`/resellers/${code}`);

export const editReseller = (data: Reseller) => callApi.put(`/resellers/${data.code}`, data);
