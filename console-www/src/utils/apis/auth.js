// @flow

import callApi from '../callApi';
import type { LoginData } from '../../features/auth/types';

export const login = ({ email, password }: LoginData) => callApi.post('/login', {}, {
  auth: {
    username: email,
    password,
  },
});

export const logout = () => callApi.get('/logout');
