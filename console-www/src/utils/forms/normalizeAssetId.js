export const normalizeAssetId = (value) => {
  const parsed = parseInt(value, 10);

  return !Number.isNaN(parsed) ? parsed : '';
};
