// @flow

import type { TableChangeState, TableChangeType } from '../common/types';


/**
 * A helper function to handle table changes, such as 'sort', 'filter' etc
 */
export default function tableChangeHandler(callback: Function) {
  return (type: TableChangeType, newState: TableChangeState): void => {
    const {
      sortOrder,
      sortField,
    } = newState;

    if (type === 'sort') {
      const modifier = sortOrder === 'desc' ? '-' : '';

      callback({
        sort: `${modifier}${sortField}`,
      });
    }
  };
}
