// @flow

import uniq from 'lodash.uniq';
import type { ListQueryParams } from '../common/types';

/**
 * General type annotation for objects that should be merged
 */
type Entities = {
  entities: {},
};

type StateOrActionT = {
  listQueryParams: ListQueryParams,
  keys: Array<any>,
} & Entities;


/**
 * Merge entities
 */
export function mergeEntities(state: Entities, action: Entities) {
  return { ...state.entities, ...action.entities };
}


/**
 * If sort has changed then replace all keys
 * merge otherwise
 */
export function mergeKeys(state: StateOrActionT, action: StateOrActionT) {
  return (
    state.listQueryParams.sort === action.listQueryParams.sort &&
    state.listQueryParams.search === action.listQueryParams.search &&
    action.listQueryParams.offset
  ) ? uniq([...state.keys, ...action.keys])
    : [...action.keys];
}


/**
 * Merge query params
 */
export function mergeQueryParams(
  prevParams: ListQueryParams,
  params: $Shape<ListQueryParams>,
): ListQueryParams {
  const nextParams = {
    ...prevParams,
    ...params,
  };

  return {
    ...nextParams,
    offset: prevParams.sort === nextParams.sort ? nextParams.offset : 0,
  };
}

/**
 * Check if there are more data to load
 */
export function hasMore(action: StateOrActionT) {
  return action.keys.length === action.listQueryParams.limit;
}


/**
 * Merge the state with the action using function above
 */
export function mergeListResponse(state: StateOrActionT, action: StateOrActionT) {
  return {
    entities: mergeEntities(state, action),
    keys: mergeKeys(state, action),
    hasMore: hasMore(action),
    listQueryParams: action.listQueryParams,
  };
}
