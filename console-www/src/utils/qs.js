// @flow

import qs from 'qs';
import type { ListQueryParams } from '../common/types';

/**
 * The function which accepts an object and returns a query string excluding empty params
 */
export default function (params: ListQueryParams): string {
  return qs.stringify(Object.keys(params).reduce((newParams, key) => {
    if (params[key]) {
      return {
        ...newParams,
        [key]: params[key],
      };
    }

    return newParams;
  }, {}));
}
