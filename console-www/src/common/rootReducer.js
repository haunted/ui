import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import commonReducers from '../features/common/redux/reducers';
import authReducers from '../features/auth/redux/reducers';
import supplierReducers from '../features/suppliers/redux/reducers';
import resellerReducers from '../features/resellers/redux/reducers';
import deviceReducers from '../features/devices/redux/reducers';
import passIssuerReducers from '../features/passIssuers/redux/reducers';
import userReducers from '../features/users/redux/reducers';

const reducerMap = {
  form,
  common: commonReducers,
  auth: authReducers,
  suppliers: supplierReducers,
  resellers: resellerReducers,
  devices: deviceReducers,
  passIssuers: passIssuerReducers,
  users: userReducers,
};

export default combineReducers(reducerMap);
