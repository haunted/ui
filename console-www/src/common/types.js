// @flow

import type { $AxiosXHR, $AxiosError } from 'axios';
import type {
  Action as CommonAction,
  State as CommonState,
} from '../features/common/types';
import type {
  Action as AuthAction,
  State as AuthState,
} from '../features/auth/types';
import type {
  Action as SupplierAction,
  State as SupplierState,
} from '../features/suppliers/types';
import type {
  Action as ResellerAction,
  State as ResellerState,
} from '../features/resellers/types';
import type {
  Action as DeviceAction,
  State as DeviceState,
} from '../features/devices/types';
import type {
  Action as PassIssuerAction,
  State as PassIssuerState,
} from '../features/passIssuers/types';
import type {
  Action as UserAction,
  State as UserState,
} from '../features/users/types';

/**
 * Axios helpers
 */
export type AxiosResponse<R> = $AxiosXHR<any, R>;
export type AxiosError = $AxiosError<any>;

export type ListQueryParams = {
  limit: ?number,
  offset: ?number,
  sort: ?string,
  search: ?string,
  status?: Array<string> | string,
};


/**
 * Normalizr helpers
 */
export type Normalized<InstanceType, InstanceName: string, KeyType = string> = {
  entities: {
    [instanceName: InstanceName]: ?{
      [key: string]: InstanceType,
    },
  },
  result: Array<KeyType>,
};

export type NormalizedSingle<InstanceType, InstanceName: string, KeyType = string> = {
  entities: {
    [instanceName: InstanceName]: {
      [key: string]: InstanceType,
    },
  },
  result: KeyType,
};


/**
 * Global Action type definition
 */
export type Action =
  | CommonAction
  | AuthAction
  | SupplierAction
  | ResellerAction
  | DeviceAction
  | PassIssuerAction
  | UserAction;


/**
 * Global State type definition
 */
export type State = {
  common: CommonState,
  auth: AuthState,
  suppliers: SupplierState,
  resellers: ResellerState,
  devices: DeviceState,
  passIssuers: PassIssuerState,
  users: UserState,
};

export type TableChangeType = 'filter' | 'pagination' | 'sort' | 'cellEdit';

export type TableChangeState = {
  page: number,
  sizePerPage: number,
  sortField: string,
  sortOrder: 'asc' | 'desc',
  filters: any,
  data: any,
  cellEdit: {
    rowId: string | number,
    dataField: string,
    newValue: string,
  },
};

export type TableChangeHandler = (type: TableChangeType, newState: TableChangeState) => void;

/**
 * Global ModalId type definition
 */
export type { ModalId } from './constants';
