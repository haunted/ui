import { schema } from 'normalizr';

export const supplierSchema = new schema.Entity('suppliers', {}, { idAttribute: 'code' });
export const supplierListSchema = [supplierSchema];

export const resellerSchema = new schema.Entity('resellers', {}, { idAttribute: 'code' });
export const resellerListSchema = [resellerSchema];

export const deviceSchema = new schema.Entity('devices');
export const deviceListSchema = [deviceSchema];

export const passIssuerSchema = new schema.Entity('passIssuers', {}, { idAttribute: 'code' });
export const passIssuerListSchema = [passIssuerSchema];

export const userSchema = new schema.Entity('users', {}, { idAttribute: 'id' });
export const userListSchema = [userSchema];
