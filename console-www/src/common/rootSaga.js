import { all } from 'redux-saga/effects';
import * as authSagas from '../features/auth/redux/sagas';
import * as supplierSagas from '../features/suppliers/redux/sagas';
import * as resellerSagas from '../features/resellers/redux/sagas';
import * as deviceSagas from '../features/devices/redux/sagas';
import * as passIssuerSagas from '../features/passIssuers/redux/sagas';
import * as userSagas from '../features/users/redux/sagas';

const sagasList = [
  authSagas,
  supplierSagas,
  resellerSagas,
  deviceSagas,
  passIssuerSagas,
  userSagas,
];

const sagas = sagasList
  .reduce((prev, curr) => [
    ...prev,
    ...Object.keys(curr).map(k => curr[k]),
  ], [])
  .filter(s => typeof s === 'function');

function* rootSaga() {
  yield all(sagas.map(s => s()));
}

export default rootSaga;
