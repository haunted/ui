// @flow

/**
 * Modals
 */
export const modals = {
  DEVICE_NOTES_MODAL: 'DEVICE_NOTES_MODAL',
  PROVISION_DEVICES_MODAL: 'PROVISION_DEVICES_MODAL',
  DEACTIVATE_DEVICE_MODAL: 'DEACTIVATE_DEVICE_MODAL',
};

// Since $Keys should be used at the same file as a const we define it here, but it will be also
// exported from ./types.js
export type ModalId = $Keys<typeof modals>;


/**
 * Device statuses
 */
export const deviceStatus = {
  STATUS_UNKNOWN: 'STATUS_UNKNOWN',
  STATUS_NEW: 'STATUS_NEW',
  STATUS_PROVISIONED: 'STATUS_PROVISIONED',
  STATUS_DISABLED: 'STATUS_DISABLED',
  STATUS_DEPROVISIONED: 'STATUS_DEPROVISIONED',
  STATUS_DEACTIVATED: 'STATUS_DEACTIVATED',
  STATUS_DELETED: 'STATUS_DELETED',
};

export type DeviceStatusT = $Keys<typeof deviceStatus>;


/**
 * Device note statuses
 */
export const deviceNoteStatus = {
  STATUS_NEW: 'STATUS_NEW',
  STATUS_DELETED: 'STATUS_DELETED',
};

export type DeviceNoteStatusT = $Keys<typeof deviceNoteStatus>;


/**
 * Unauthorized request statuses
 */
export const UNAUTHORIZED_STATUSES = [401, 403];
