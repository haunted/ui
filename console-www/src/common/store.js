import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
  sagaMiddleware,
];

let devToolsExtension = f => f;

if (process.env.NODE_ENV === 'development') {
  const logger = createLogger({ collapsed: true });

  middlewares.push(logger);

  if (window.devToolsExtension) {
    devToolsExtension = window.devToolsExtension();
  }
}

const store = createStore(rootReducer, compose(
  applyMiddleware(...middlewares),
  devToolsExtension,
));

if (module.hot) {
  module.hot.accept('./rootReducer', () => {
    const nextRootReducer = require('./rootReducer').default; // eslint-disable-line

    store.replaceReducer(nextRootReducer);
  });
}

sagaMiddleware.run(rootSaga);

export default store;
