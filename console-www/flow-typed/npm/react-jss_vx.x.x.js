// @flow
// flow-typed signature: 2028d3e904e5e637d5cc82e00ddfc8d3
// flow-typed version: <<STUB>>/react-jss_v^8.4.0/flow_v0.72.0
/* eslint-disable max-len */

/**
 * This is an autogenerated libdef stub for:
 *
 *   'react-jss'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'react-jss' {
  import type { ComponentType } from 'react';


  /**
   * Value types of properties
   */
  declare type Value = string | number;


  /**
   * Property types
   */
  declare type Property = Value | Array<Value> | (props: { [string]: any }) => Value;


  /**
   * Single style block type
   */
  declare type StyleBlock = { [string]: Property | StyleBlock };


  /**
   * Styles object (supports nesting)
   */
  declare export type StylesObject = {
    [string]: {
      [string]: Property | StyleBlock
    }
  };


  /**
   * JSS styles
   */
  declare export type Styles = StylesObject | (theme: { [string]: any }) => StylesObject;


  /**
   * HOC type
   */
  declare function injectSheet<Props: {}, Component: ComponentType<Props>>(
    styles: Styles
  ): (WrappedComponent: Component) => ComponentType<$Diff<Props, { classes: {} | void }>>;

  declare export function jss(...args: Array<any>): any;
  declare export function ThemeProvider(...args: Array<any>): any;
  declare export default typeof injectSheet;
}
