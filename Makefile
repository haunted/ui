include ../mk/pre.mk
include ../mk/inc.mk

git-subtree-add:
	cd ..; git subtree add --prefix ui/$(SUBTREE_PREFIX) $(SUBTREE_REPO) $(SUBTREE_BRANCH) --squash

git-subtree-pull:
	cd ..; git subtree pull --prefix ui/$(SUBTREE_PREFIX) $(SUBTREE_REPO) $(SUBTREE_BRANCH) --squash

npm-install:
	cd $(SUBTREE_PREFIX) && npm install

npm-build:
	cd $(SUBTREE_PREFIX) && $(NPM_BUILD_ENV) npm run build

ui-dockerize: NAME=$(DOCKERIZE_NAME)
ui-dockerize:
	@echo "Dockerize ui/$(SUBTREE_PREFIX) as $(IMAGE)..."
	-$(DOCKER_CMD) rm --force $(NAME)
	-$(DOCKER_CMD) rmi --force $(IMAGE)
	cp .dockerignore $(SUBTREE_PREFIX)/
	cp publish.sh $(SUBTREE_PREFIX)/
	cp $(DOCKERFILE) $(SUBTREE_PREFIX)/
	sleep 1
	sync
	$(DOCKER_CMD) build --no-cache --file ./$(SUBTREE_PREFIX)/$(DOCKERFILE) --tag=$(IMAGE) ./$(SUBTREE_PREFIX)/
	$(DOCKER_CMD) inspect -f '{{.Id}}' $(IMAGE) > .dockerized-$(NAME)
	$(DOCKER_CMD) run $(IMAGE)
	rm $(SUBTREE_PREFIX)/$(DOCKERFILE)
	rm $(SUBTREE_PREFIX)/publish.sh
	rm $(SUBTREE_PREFIX)/.dockerignore

ui-docker-tag: NAME=$(DOCKERIZE_NAME)
ui-docker-tag:
	$(DOCKER_CMD) tag $(IMAGE):latest $(IMAGE):$(TAG)

ui-docker-push: NAME=$(DOCKERIZE_NAME)
ui-docker-push:
	$(DOCKER_CMD) push $(IMAGE):$(TAG) > /dev/null


include ./Makefile.admin
# include ./Makefile.bunraku
include ./Makefile.olci
include ./Makefile.papyrus
include ./Makefile.console

## Clean all
clean:
	rm -f .dockerized-*

## Build all
build:
	$(MAKE) console-build
	$(MAKE) admin-build
	# $(MAKE) bunraku-build
	$(MAKE) olci-build
	$(MAKE) papyrus-build

## Dockerize all
dockerize:
	$(MAKE) console-dockerize
	$(MAKE) admin-dockerize
	# $(MAKE) bunraku-dockerize
	$(MAKE) olci-dockerize
	$(MAKE) papyrus-dockerize

docker-tag:
	$(MAKE) console-docker-tag
	$(MAKE) admin-docker-tag
	# $(MAKE) bunraku-docker-tag
	$(MAKE) olci-docker-tag
	$(MAKE) papyrus-docker-tag

docker-push:
	$(MAKE) console-docker-push
	$(MAKE) admin-docker-push
	# $(MAKE) bunraku-docker-push
	$(MAKE) olci-docker-push
	$(MAKE) papyrus-docker-push

.DEFAULT_GOAL := help
include ../mk/help.mk

.PHONY: git-subtree-add git-subtree-pull \
        npm-install npm-build ui-dockerize \
        clean build dockerize
