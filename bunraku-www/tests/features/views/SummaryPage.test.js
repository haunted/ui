import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { SummaryPage } from 'src/features/views';

describe('views/SummaryPage', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <SummaryPage />
    );

    expect(
      renderedComponent.find('.views-summary-page').node
    ).to.exist;
  });
});
