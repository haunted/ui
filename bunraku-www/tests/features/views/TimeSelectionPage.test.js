import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { TimeSelectionPage } from 'src/features/views';

describe('views/TimeSelectionPage', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <TimeSelectionPage />
    );

    expect(
      renderedComponent.find('.views-time-selection-page').node
    ).to.exist;
  });
});
