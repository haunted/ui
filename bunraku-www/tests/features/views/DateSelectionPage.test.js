import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { DateSelectionPage } from 'src/features/views';

describe('views/DateSelectionPage', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <DateSelectionPage />
    );

    expect(
      renderedComponent.find('.views-date-selection-page').node
    ).to.exist;
  });
});
