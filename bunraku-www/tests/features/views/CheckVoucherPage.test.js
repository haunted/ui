import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { CheckVoucherPage } from 'src/features/views';

describe('views/CheckVoucherPage', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <CheckVoucherPage />
    );

    expect(
      renderedComponent.find('.views-check-voucher-page').node
    ).to.exist;
  });
});
