import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { PageNotFound } from 'src/features/views';

describe('components/PageNotFound', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <PageNotFound />
    );

    expect(
      renderedComponent.find('.views-page-not-found').node
    ).to.exist;
  });
});
