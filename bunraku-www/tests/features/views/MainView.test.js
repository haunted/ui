import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { MainView } from 'src/features/views';

describe('views/MainView', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(
      <MainView />
    );

    expect(
      renderedComponent.find('.views-main-view').node
    ).to.exist;
  });
});
