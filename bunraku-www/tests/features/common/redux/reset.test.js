import { expect } from 'chai';

import {
  COMMON_RESET,
} from 'src/features/common/redux/constants';

import { reset, } from 'src/features/common/redux/reset';
import reducer from 'src/features/common/redux/reducer';
import initialState from 'src/features/common/redux/initialState';

describe('common/redux/reset', () => {
  it('returns correct action by reset', () => {
    expect(reset()).to.have.property('type', COMMON_RESET);
  });

  it('handles action type COMMON_RESET correctly', () => {
    const prevState = {
      ...initialState,
    };

    const state = reducer(
      prevState,
      reset(),
    );
    const { config: newConfig, ...newState } = state;
    const { config, ...oldState } = initialState;

    // Should be immutable
    expect(state).to.not.equal(prevState);

    // State should be equal initialState (not including config)
    expect(newState).to.deep.equal(oldState);

    // Config should be kept after reset
    expect(prevState.config).to.equal(newConfig);
  });
});
