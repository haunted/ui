// Summary:
//   This is the entry of the application, works together with index.html.

import 'babel-polyfill';
import React from 'react';
import { AppContainer } from 'react-hot-loader';
import { render } from 'react-dom';
import { LocaleProvider } from 'antd';
import axios from 'axios';
import enUS from 'antd/lib/locale-provider/en_US';
import configStore from './common/configStore';
import routeConfig from './common/routeConfig';
import Root from './Root';

axios.defaults.baseURL = '/api';
axios.defaults.headers.common['Content-type'] = 'application/json';

const store = configStore();

function renderApp(app) {
  render(
    <AppContainer>
      <LocaleProvider locale={enUS}>
        {app}
      </LocaleProvider>
    </AppContainer>,
    document.getElementById('react-root')
  );
}

renderApp(<Root store={store} routeConfig={routeConfig} />);

// Hot Module Replacement API
/* istanbul ignore if  */
if (module.hot) {
  module.hot.accept('./common/routeConfig', () => {
    const nextRouteConfig = require('./common/routeConfig').default; // eslint-disable-line
    renderApp(<Root store={store} routeConfig={nextRouteConfig} />);
  });
}
