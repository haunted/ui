const WEBSOCKET_ENDPOINT = `${document.location.protocol == 'http:' ? 'ws' : 'wss'}://${window.location.host}/ws`
import ReconnectingWebSocket from 'reconnecting-websocket'

// const WEBSOCKET_ENDPOINT = process.env.WEBSOCKET_ENDPOINT

class Socket {
  constructor() {
    this.handlers = { default: data => { console.log('unhandled websocket message', data)}}
    this._onopen = () => {}
    this._onclose = (e) => { console.log(e) }
  }

  send({ type="", data=""}) {
    this.socket.send(JSON.stringify({
        type, data: JSON.stringify(data),
    }))
  }

  disconnect() {
    console.log("socket disconnect")
    this.socket.close()
    doExponentialBackoff()
  }

  connect(fn = () => {}) {
    this.socket = new ReconnectingWebSocket(WEBSOCKET_ENDPOINT)
    this.socket.onopen = () => {
      console.log("Websocket open")
      fn()
      this._onopen()
      console.log("Websocket open complete")      
    }
    this.socket.onmessage = this.onMessage.bind(this)
    this.socket.onclose = (e) => {
      console.log("socket closed", e)
      this._onclose(e)
    }
    this.socket.onerror = (e) => {
      console.log("socket error", e)
    }
  }

  onMessage(e) {
    const pkt = JSON.parse(e.data)
    const { data, type } = pkt
    if (typeof this.handlers[type] === 'undefined') {
      this.handlers.default(pkt)
    } else {
      this.handlers[type](data)
    }
  }
  onError(e) {
    console.log('websocket error', e)
  }
  onOpen(fn) {
    this._onopen = fn
  }
  onClose(fn) {
    this._onclose = fn
  }

  register(type, messageHandler) {
    this.handlers[type] = messageHandler;
  }
  registerJSON(type, messageHandler) {
    this.handlers[type] = data => messageHandler(JSON.parse(data));
  }
}

export default Socket
 