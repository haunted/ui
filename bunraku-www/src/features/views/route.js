import {
  MainView,
} from './';

export default {
  path: 'views',
  name: 'Views',
  childRoutes: [
    { path: '/',
      name: 'Main view',
      component: MainView,
      isIndex: true,
    },
  ],
};
