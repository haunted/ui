import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';
import * as commonActions from '../common/redux/actions';

import '../../styles/index.scss';

/*
  This is the root component of your app. Here you define the overall layout
  and the container of the react router. The default one is a two columns layout.
  You should adjust it according to the requirement of your app.
*/
class App extends Component {
  static propTypes = {
    children: PropTypes.node,
  };

  static defaultProps = {
    children: 'No content.',
  };

  constructor(props) {
    super(props);
  }

  state = {
    loaded: true,
  };


  render() {
    const { loaded } = this.state;

    if (!loaded) {
      return (
        <div className="app-preloader">
          <div>
            <Spin size="large" />
            <p>loading...</p>
          </div>
        </div>
      );
    }

    return (
      <div className="app">
        <div className="wrapper app-content">
          {this.props.children}
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    brandId: state.common.brandId,
    config: state.common.config,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(commonActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
