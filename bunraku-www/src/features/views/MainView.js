import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import { Button, Steps, Icon, Spin } from 'antd';
const Step = Steps.Step;

import * as commonActions from '../common/redux/actions';


class MainView extends Component {
  static propTypes = {
    reset: PropTypes.func.isRequired,
  };

  static defaultProps = {

  };

  constructor(p) {
    super(p)
    this.props.connect()
  }

  render() {
    const { reset, statuses, connected } = this.props;

    return (
      <div className="main-view">
        { connected || <Icon type="disconnect" className="disconnect-icon-overlay" /> }
        <Spin spinning={!connected}>
          {
            Object.keys(statuses).map(k => (
              <div className="flow-status">
                <h3>{statuses[k].ID}</h3>
                <Steps>
                  {
                    statuses[k].Expected.map((w, i) =>
                      <Step
                        name={i}
                        description={!!statuses[k].EventErrors[i] ? w.name + "\n" + statuses[k].EventErrors[i] : w.name}
                        status={
                          !!statuses[k].EventErrors[i] ? "error" : (
                            i < statuses[k].Received.length ? "finish" : (
                              i > statuses[k].Received.length ? "wait" : "process"
                            )
                          )
                        } />
                    )
                  }
                </Steps>
              </div>))
          }
        </Spin>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    statuses: state.views.statusById,
    connected: state.views.connected,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(commonActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainView);
