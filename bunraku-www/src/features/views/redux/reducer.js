import initialState from './initialState';
import { COMMON_RESET, RCV_FLOW_STATUS, COMMON_CONNECTED, COMMON_DISCONNECTED } from '../../common/redux/constants';

const reducers = [

];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    case COMMON_RESET:
      newState = initialState;
      break;
    case RCV_FLOW_STATUS:
      return {
        ...state,
        statusById: {
          ...state.statusById,
          [action.data.ID]: action.data
        }
      }

      return statusById

    case COMMON_CONNECTED:
      // reset statuses :p
      let statuses = {}
      Object.keys(state.statusById).map(k =>
        statuses[k] = {
          ...state.statusById[k],
          Received: [],
          Errors: [],
        }
      )
      return {
        ...state,
        statusById: statuses,
        connected: true,
      };

    case COMMON_DISCONNECTED:
      return {
        ...state,
        connected: false,
      };
    default:
      newState = state;
      break;
  }
  return reducers.reduce((s, r) => r(s, action), newState);
}
