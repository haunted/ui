import initialState from './initialState';
import { COMMON_RESET } from './constants';
import { reducer as resetReducer } from './reset';

const reducers = [,
  resetReducer,
];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    case COMMON_RESET:
      newState = {
        ...initialState,
        config: state.config,
      };
      break;
    default:
      newState = state;
      break;
  }
  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
