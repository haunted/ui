import {
  COMMON_RESET,
} from './constants';

export function reset() {
  return {
    type: COMMON_RESET,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case COMMON_RESET:
      return {
        ...state,
      };

    default:
      return state;
  }
}
