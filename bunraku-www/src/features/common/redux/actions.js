export { reset } from './reset';
import WS from '../../../websocket.js'
import {
    COMMON_CONNECT,
    RCV_FLOW_STATUS,
    COMMON_CONNECTED,
    COMMON_DISCONNECTED,
} from './constants'

export let connect = () =>
    dispatch => {
        let socket = new WS()

        socket.registerJSON("FlowStatusUpdated", data => {
            dispatch({
                type: RCV_FLOW_STATUS,
                data,
            });
        })

        socket.onOpen(() => {
            socket.send({ type: "stream-statuses" })

            dispatch({
                type: COMMON_CONNECTED,
            })
        })

        socket.onClose(() => {
            socket.send({ type: "stream-statuses" })

            dispatch({
                type: COMMON_DISCONNECTED,
            })
        })
        socket.connect()

        dispatch({
            type: COMMON_CONNECT,
        })
    }
