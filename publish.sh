#!/bin/bash

# Copy all our files to shared volume
cp -r /www/* /app/

# Never exit
exec tail -f /dev/null
