# Online Check-in Web

### Running for development
Create an empty `.env.local` file in the project root with the following content
```
# The domain name of the app
REACT_APP_DOMAIN=redeam.it

# Used to get brand specific configs/UI when working on localhost.
# If this variable is not set, subdomain name will be used instead.
REACT_APP_BRAND_ID=natgeoencounter
```

After that you are ready to run the app

```
npm install
npm start
```

### Building for production
```
npm install
npm run build
```

### Creating a new instance of OLCI

##### 1. Create a new directory inside `src/brands`
```
mkdir src/brands/<BRAND_ID>
```
Please note that BRAND_ID must be the same as subdomain name which will be used for this installation.

##### 2. Create a `config.js` in the previously created directory with the following content
```
module.exports = {
  attractionName: 'My new brand name',
  attractionLogo: 'logo.png',
  printLogo: 'logo_print.png',
  defaultTheme: false,
  style: 'styles/index.scss',
};
```
* `attractionName` REQUIRED name of the attraction
* `attractionLogo` REQUIRED path to the main logo
* `printLogo`OPTIONAL path to the logo in print mode (if it should be different)
* `defaultTheme` OPTIONAL whether to use the default theme
* `style` OPTIONAL path to custom styles of this specific brand (if the default theme is enabled this styles will take precedence)

##### 3. Update `BRAND_ID` value in `.env` for local development
