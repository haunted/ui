import React, { PureComponent } from 'react';

export default class PageNotFound extends PureComponent {
  render() {
    return (
      <div className="views-page-not-found">
        Page not found.
      </div>
    );
  }
}
