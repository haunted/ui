import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import { Button, Icon } from 'antd';
import { AttractionLogo, Footer, Steps, StepPanel } from '../common';
import * as commonActions from '../common/redux/actions';
import { getStepIndex } from '../../common/utils';
import {
  ResellerSelectionPage,
  CheckVoucherPage,
  DateSelectionPage,
  TimeSelectionPage,
  ConfirmationPage,
  SummaryPage,
} from '../views';

// TODO fix hardcoded attraction name
const subTitles = [
  'To get started, please select the company where you purchased your voucher.',
  'Please enter your confirmation number. You can see an example highlighted in the voucher below.',
  'Please select the date you wish to visit.',
  'Please select the time you wish to visit.',
  'Please enter your details to reserve your tickets.',
];

class MainView extends Component {
  static propTypes = {
    systemErrorStatus: PropTypes.shape({
      code: PropTypes.string,
      message: PropTypes.string,
    }),
    voucherData: PropTypes.object,
    selectedReseller: PropTypes.string,
    resellers: PropTypes.array.isRequired,
    date: PropTypes.string,
    dateTime: PropTypes.string,
    email: PropTypes.string,
    step: PropTypes.string.isRequired,
    reset: PropTypes.func.isRequired,
  };

  static defaultProps = {
    systemErrorStatus: null,
    selectedReseller: null,
    voucherData: null,
    date: null,
    dateTime: null,
    email: null,
  };

  renderStepComponent(component) {
    const { systemErrorStatus, reset } = this.props;

    if (systemErrorStatus) {
      return (
        <div className="status status--error">
          <Icon type="close-circle-o" />
          <p>{systemErrorStatus.message || systemErrorStatus.code}</p>

          <div className="main-view__nav">
            <Button onClick={reset}>Start Over</Button>
          </div>
        </div>
      );
    }

    return component;
  }

  render() {
    const {
      voucherData,
      selectedReseller,
      resellers,
      date,
      dateTime,
      email,
      step,
    } = this.props;

    const stepIndex = getStepIndex(step);
    const reseller = resellers.find(r => r.code === selectedReseller);
    const resellerName = stepIndex > 0 ? reseller && reseller.name : '';

    return (
      <div className="app main-view">
        <AttractionLogo />

        <div className="wrapper app-content">
          <Steps activePanel={stepIndex}>
            <StepPanel
              title="Select Voucher Issuer"
              activeSubtitle={subTitles[0]}
              completeSubtitle={resellerName}
            >
              {this.renderStepComponent(<ResellerSelectionPage />)}
            </StepPanel>

            <StepPanel
              title="Enter Voucher Number"
              activeSubtitle={subTitles[1]}
              completeSubtitle={voucherData && voucherData.ref}
            >
              {this.renderStepComponent(<CheckVoucherPage />)}
            </StepPanel>

            <StepPanel
              title="Select Date"
              activeSubtitle={subTitles[2]}
              completeSubtitle={date ? moment(date).format('dddd, MMMM D, YYYY') : ''}
            >
              {this.renderStepComponent(<DateSelectionPage />)}
            </StepPanel>

            <StepPanel
              title="Select Time"
              activeSubtitle={subTitles[3]}
              completeSubtitle={dateTime ? moment(dateTime).format('h:mm a z') : ''}
            >
              {this.renderStepComponent(<TimeSelectionPage />)}
            </StepPanel>

            <StepPanel
              title="Confirmation"
              activeSubtitle={subTitles[4]}
              completeSubtitle={email}
            >
              {this.renderStepComponent(<ConfirmationPage />)}
            </StepPanel>

            <StepPanel title="Summary">
              {this.renderStepComponent(<SummaryPage />)}
            </StepPanel>
          </Steps>
        </div>

        <Footer />
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    step: state.common.step,
    systemErrorStatus: state.common.systemErrorStatus,
    voucherData: state.common.voucherData,
    selectedReseller: state.common.selectedReseller,
    resellers: state.common.resellers,
    date: state.common.date,
    dateTime: state.common.dateTime,
    email: state.common.email,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(commonActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MainView);
