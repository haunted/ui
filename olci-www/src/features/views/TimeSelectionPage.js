import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Spin } from 'antd';
import { bindActionCreators } from 'redux';
import * as actions from '../common/redux/actions';
import { TimePicker, SystemError, PageNav } from '../common';
import {
  STEP_DATE_SELECTION,
  STEP_CONFIRMATION,
} from '../../common/steps';


class TimeSelectionPage extends Component {
  static propTypes = {
    getAvailableTimes: PropTypes.func.isRequired,
    setStep: PropTypes.func.isRequired,
    selectTime: PropTypes.func.isRequired,
    availableTimes: PropTypes.array.isRequired,
    getAvailableTimesPending: PropTypes.bool.isRequired,
    getAvailableTimesError: PropTypes.bool,
    dateTime: PropTypes.string,
  };

  static defaultProps = {
    getAvailableTimesError: null,
    dateTime: null,
  };

  componentWillMount() {
    this.props.getAvailableTimes();
  }

  render() {
    const {
      setStep,
      selectTime,
      dateTime,
      availableTimes,
      getAvailableTimes,
      getAvailableTimesPending,
      getAvailableTimesError,
    } = this.props;

    const pageNavProps = {
      nextButtonProps: {
        onClick: () => setStep(STEP_CONFIRMATION),
        disabled: !dateTime,
      },
      goBackButtonProps: {
        onClick: () => setStep(STEP_DATE_SELECTION),
      },
    };

    return (
      <div className="time-selection-page">
        {getAvailableTimesPending ? (
          <Spin />
        ) : (
          <div>
            {getAvailableTimesError ? (
              <SystemError
                text="Cannot load available times due to server error"
                onRetry={getAvailableTimes}
              />
            ) : (
              <div>
                <TimePicker
                  availableTimes={availableTimes}
                  onChange={selectTime}
                  value={dateTime}
                />

                <PageNav {...pageNavProps} />
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    time: state.common.time,
    dateTime: state.common.dateTime,
    availableTimes: state.common.availableTimes,
    getAvailableTimesPending: state.common.getAvailableTimesPending,
    getAvailableTimesError: state.common.getAvailableTimesError,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(actions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(TimeSelectionPage);
