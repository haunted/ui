import initialState from './initialState';
import { COMMON_RESET } from '../../common/redux/constants';

const reducers = [
];

export default function reducer(state = initialState, action) {
  let newState;
  switch (action.type) {
    case COMMON_RESET:
      newState = initialState;
      break;

    default:
      newState = state;
      break;
  }
  return reducers.reduce((s, r) => r(s, action), newState);
}
