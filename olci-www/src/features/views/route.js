import { paths } from '../../common/steps';
import {
  MainView,
  PrintView,
} from './';

export default {
  path: 'views',
  name: 'Views',
  childRoutes: [
    ...Object.keys(paths).map(p => ({
      path: p,
      component: MainView,
    })),
    {
      path: '/print',
      name: 'Print view',
      component: PrintView,
    },
  ],
};
