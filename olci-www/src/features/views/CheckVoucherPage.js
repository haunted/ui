import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Form, Input, Spin } from 'antd';
import { Image, SystemError, PageNav } from '../common';
import { STEP_RESELLER_SELECTION } from '../../common/steps';
import { getStoreValue } from '../../common/utils';
import * as actions from '../common/redux/actions';

const FormItem = Form.Item;

class CheckVoucherPage extends Component {
  static propTypes = {
    brandId: PropTypes.string.isRequired,
    resellerId: PropTypes.string,
    setStep: PropTypes.func.isRequired,
    checkVoucherNumberPending: PropTypes.bool.isRequired,
    checkVoucherNumberError: PropTypes.bool,
    checkVoucherNumber: PropTypes.func.isRequired,
    dismissCheckVoucherNumberError: PropTypes.func.isRequired,
    fetchResellersPending: PropTypes.bool.isRequired,
    resellerNotFound: PropTypes.bool.isRequired,
    match: PropTypes.shape({
      params: PropTypes.object,
    }).isRequired,
  };

  static defaultProps = {
    resellerId: null,
    checkVoucherNumberError: null,
  };

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

    const { voucherRef } = props.match.params;

    // TODO create a redux-form
    this.state = {
      number: voucherRef || getStoreValue('voucherRef') || null,
    };
  }

  handleSubmit(e) {
    const { checkVoucherNumber, resellerId } = this.props;
    const { number } = this.state;

    e.preventDefault();
    checkVoucherNumber({
      resellerId,
      number,
    });
  }

  handleInputChange(e) {
    const {
      checkVoucherNumberError,
      dismissCheckVoucherNumberError,
    } = this.props;

    this.setState({ number: e.target.value });

    if (checkVoucherNumberError) {
      dismissCheckVoucherNumberError();
    }
  }

  render() {
    const {
      brandId,
      resellerId,
      resellerNotFound,
      fetchResellersPending,
      checkVoucherNumberPending,
      checkVoucherNumberError,
      setStep,
    } = this.props;

    if (fetchResellersPending) {
      return (
        <div className="check-voucher-page">
          <Spin size="large" />
        </div>
      );
    }

    const pageNavProps = {
      nextButtonProps: {
        htmlType: 'submit',
        loading: checkVoucherNumberPending,
      },
      goBackButtonProps: {
        onClick: () => setStep(STEP_RESELLER_SELECTION),
      },
    };

    return (
      <div className="check-voucher-page">
        {resellerNotFound ? (
          <SystemError
            text="Voucher Issuer not found"
            onRetry={() => setStep(STEP_RESELLER_SELECTION)}
            btnLabel="Select Voucher Issuer"
          />
        ) : (
          <Form onSubmit={this.handleSubmit} className="check-voucher-form">
            <FormItem validateStatus={checkVoucherNumberError ? 'error' : null}>
              <Input
                placeholder="Confirmation number"
                onChange={this.handleInputChange}
                value={this.state.number}
                autoFocus
              />
            </FormItem>

            <PageNav {...pageNavProps} />

            {resellerId && (
              <div className="voucher-sample">
                <Image
                  src={`https://storage.googleapis.com/redeam-olci/${brandId}_${resellerId}_voucher.jpg`}
                  alt="No sample voucher"
                />
              </div>
            )}
          </Form>
        )}
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    brandId: state.common.brandId,
    resellers: state.common.resellers,
    resellerId: state.common.selectedReseller,
    resellerNotFound: state.common.resellerNotFound,
    fetchResellersPending: state.common.fetchResellersPending,
    checkVoucherNumberPending: state.common.checkVoucherNumberPending,
    checkVoucherNumberError: state.common.checkVoucherNumberError,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(actions, dispatch),
  };
}

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(CheckVoucherPage),
);
