import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';
import * as commonActions from '../common/redux/actions';

import '../../styles/index.scss';

/*
  This is the root component of your app. Here you define the overall layout
  and the container of the react router. The default one is a two columns layout.
  You should adjust it according to the requirement of your app.
*/
class App extends Component {
  static propTypes = {
    children: PropTypes.node,
    brandId: PropTypes.string.isRequired,
    config: PropTypes.object,
    setConfig: PropTypes.func.isRequired,
    init: PropTypes.func.isRequired,
    initPending: PropTypes.bool.isRequired,
  };

  static defaultProps = {
    children: 'No content.',
    config: null,
  };

  constructor(props) {
    super(props);

    this.loadModules = this.loadModules.bind(this);
  }

  state = {
    loaded: false,
  };

  componentWillMount() {
    const {
      brandId,
      setConfig,
      init,
    } = this.props;

    import(`../../brands/${brandId}/config`).then((config) => {
      setConfig(config);
      this.loadModules();
    });

    init();
  }

  loadModules() {
    const { brandId, config } = this.props;
    const modules = [];

    if (config.defaultTheme) {
      modules.push(import('../../styles/default.scss'));
    }

    if (config.style) {
      modules.push(import(`../../brands/${brandId}/${config.style}`));
    }

    // Wait some time to let browser apply dynamically loaded styles
    modules.push(new Promise(res => setTimeout(res, 500)));

    Promise.all(modules).then(() => {
      this.setState({ loaded: true });
    });
  }

  render() {
    const { initPending } = this.props;
    const { loaded } = this.state;

    if (!loaded || initPending) {
      return (
        <div className="app-preloader">
          <div>
            <Spin size="large" />
            <p>loading...</p>
          </div>
        </div>
      );
    }

    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    brandId: state.common.brandId,
    config: state.common.config,
    initPending: state.common.initPending,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(commonActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
