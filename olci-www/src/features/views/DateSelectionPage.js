import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment-timezone';
import * as actions from '../common/redux/actions';
import { DatePicker, VoucherData, SystemError, PageNav } from '../common';
import {
  STEP_VOUCHER_VERIFICATION,
  STEP_TIME_SELECTION,
} from '../../common/steps';


class DateSelectionPage extends Component {
  static propTypes = {
    setStep: PropTypes.func.isRequired,
    getAvailableDates: PropTypes.func.isRequired,
    dismissGetAvailableDatesError: PropTypes.func.isRequired,
    voucherData: PropTypes.object.isRequired,
    groupedDates: PropTypes.object.isRequired,
    getAvailableDatesPending: PropTypes.bool.isRequired,
    getAvailableDatesError: PropTypes.bool,
    selectDate: PropTypes.func.isRequired,
    date: PropTypes.string,
  };

  static defaultProps = {
    date: '',
    getAvailableDatesError: null,
  };

  constructor(props) {
    super(props);

    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date) {
    const { selectDate } = this.props;

    selectDate(date.toISOString());
  }

  render() {
    const {
      setStep,
      date: dateStr,
      voucherData,
      groupedDates,
      getAvailableDatesPending,
      getAvailableDatesError,
      getAvailableDates,
      dismissGetAvailableDatesError,
    } = this.props;

    const date = dateStr ? moment(dateStr) : null;

    const pageNavProps = {
      nextButtonProps: {
        onClick: () => setStep(STEP_TIME_SELECTION),
        disabled: !date,
      },
      goBackButtonProps: {
        onClick: () => setStep(STEP_VOUCHER_VERIFICATION),
      },
    };

    return (
      <div className="date-selection-page">
        <p className="subtitle">
          Your booking reference has been validated and confirmed.
          Our records indicate the following booking details.
        </p>

        <VoucherData voucherData={voucherData} />

        {getAvailableDatesError ? (
          <SystemError
            text="Cannot load available dates due to server error"
            onRetry={dismissGetAvailableDatesError}
          />
        ) : (
          <div>
            <DatePicker
              value={date}
              dates={groupedDates}
              onChange={this.onDateChange}
              loadData={getAvailableDates}
              isLoading={getAvailableDatesPending}
            />

            <PageNav {...pageNavProps} />
          </div>
        )}
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    voucherData: state.common.voucherData,
    date: state.common.date,
    groupedDates: state.common.groupedDates,
    getAvailableDatesPending: state.common.getAvailableDatesPending,
    getAvailableDatesError: state.common.getAvailableDatesError,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(actions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DateSelectionPage);
