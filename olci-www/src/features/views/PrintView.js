/* eslint-disable import/no-dynamic-require */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import qr from 'qrcode';
import * as actions from '../common/redux/actions';
import { BookingDetails, Icon } from '../common';


class PrintView extends Component {
  static propTypes = {
    brandId: PropTypes.string.isRequired,
    config: PropTypes.object.isRequired,
    getBookingData: PropTypes.func.isRequired,
    bookingData: PropTypes.array.isRequired,
    getBookingDataPending: PropTypes.bool.isRequired,
  };

  constructor(props) {
    super(props);

    this.createQrUrls = this.createQrUrls.bind(this);
  }

  state = {
    qrDataUrl: {},
  };

  componentWillMount() {
    document.body.classList.add('print');
    this.props.getBookingData();
  }

  componentWillReceiveProps(nextProps) {
    const { getBookingDataPending } = this.props;

    if (getBookingDataPending === true && nextProps.getBookingDataPending === false) {
      this.print();
    }

    if (nextProps.bookingData.length) {
      this.createQrUrls(nextProps.bookingData);
    }
  }

  createQrUrls(bookingData) {
    const promises = bookingData.map(b => new Promise((resolve) => {
      qr.toDataURL(b.barcode, { version: 6 }, (err, str) => resolve({ id: b.id, str }));
    }));

    Promise.all(promises).then((data) => {
      const qrDataUrl = data.reduce((obj, d) => ({
        ...obj,
        [d.id]: d.str,
      }), {});

      this.setState({ qrDataUrl });
    });
  }

  print() {
    window.print();
    window.close();
  }

  render() {
    const {
      brandId,
      config,
      getBookingDataPending,
      bookingData,
    } = this.props;

    const {
      qrDataUrl,
    } = this.state;

    const logo = config.printLogo || config.attactionLogo;

    if (getBookingDataPending) {
      return (
        <div>loading...</div>
      );
    }

    return (
      <div className="print-view">
        {bookingData.map(b => (
          <div className="print-view__confirmation" key={b.id}>
            <div className="print-view__logo">
              <img src={require(`../../brands/${brandId}/${logo}`)} alt={config.attractionName} />
            </div>

            <div className="print-view__qr">
              {qrDataUrl[b.id] && (
                <img src={qrDataUrl[b.id]} alt="qr" />
              )}
            </div>

            <BookingDetails bookingData={b} />

            <div className="print-view__footer">
              <Icon id="logo" width="120" height="20" fill="#28337c" />
            </div>

            <div className="page-break" />
          </div>
        ))}
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    config: state.common.config,
    brandId: state.common.brandId,
    bookingData: state.common.bookingData,
    getBookingDataPending: state.common.getBookingDataPending,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(actions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PrintView);
