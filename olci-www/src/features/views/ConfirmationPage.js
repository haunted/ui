import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ConfirmationForm from '../common/forms/ConfirmationForm';
import * as actions from '../common/redux/actions';
import { STEP_TIME_SELECTION } from '../../common/steps';

class ConfirmationPage extends Component {
  static propTypes = {
    setStep: PropTypes.func.isRequired,
    confirmBooking: PropTypes.func.isRequired,
    confirmBookingPending: PropTypes.bool.isRequired,
    voucherData: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(data) {
    const { confirmBooking } = this.props;

    confirmBooking(data);
  }

  render() {
    const {
      setStep,
      confirmBookingPending,
      voucherData,
    } = this.props;

    const hasName = !!voucherData.originalData.travelers[0].name;

    return (
      <div className="confirmation-view">
        <ConfirmationForm
          onSubmit={this.handleSubmit}
          onBackButtonClick={() => setStep(STEP_TIME_SELECTION)}
          submitting={confirmBookingPending}
          hasName={hasName}
        />
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    confirmBookingPending: state.common.confirmBookingPending,
    confirmBookingError: state.common.confirmBookingError,
    voucherData: state.common.voucherData,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(actions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ConfirmationPage);

