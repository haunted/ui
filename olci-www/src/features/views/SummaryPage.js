import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment-timezone';
import { Form, Input, Button, Spin, Icon } from 'antd';
import isEmail from 'validator/lib/isEmail';
import qr from 'qrcode';
import * as actions from '../common/redux/actions';
import { BookingDetails, SystemError } from '../common';
import ConfirmationForm from '../common/forms/ConfirmationForm';

const FormItem = Form.Item;

class SummaryPage extends Component {
  static propTypes = {
    confirmBooking: PropTypes.func.isRequired,
    sendConfirmation: PropTypes.func.isRequired,
    reset: PropTypes.func.isRequired,
    sendConfirmationPending: PropTypes.bool.isRequired,
    confirmBookingPending: PropTypes.bool.isRequired,
    confirmBookingError: PropTypes.bool,
    // bookingData: PropTypes.array.isRequired,
    voucherData: PropTypes.object.isRequired,
    email: PropTypes.string,
    dateTime: PropTypes.string,
  };

  static defaultProps = {
    confirmBookingError: null,
    email: '-',
    dateTime: null,
  };

  constructor(props) {
    super(props);

    this.createQrUrls = this.createQrUrls.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handlePrint = this.handlePrint.bind(this);
    this.validate = this.validate.bind(this);
  }

  state = {
    email: '',
    formTouched: false,
    formError: false,
    qrDataUrl: {},
  };

  componentWillUnmount() {
    this.props.reset();
  }

  // componentWillReceiveProps({ bookingData }) {
  //   if (bookingData.length) {
  //     this.createQrUrls(bookingData);
  //   }
  // }

  createQrUrls(bookingData) {
    const promises = bookingData.map(b => new Promise((resolve) => {
      qr.toDataURL(b.barcode, { version: 6 }, (err, str) => resolve({ id: b.id, str }));
    }));

    Promise.all(promises).then((data) => {
      const qrDataUrl = data.reduce((obj, d) => ({
        ...obj,
        [d.id]: d.str,
      }), {});

      this.setState({ qrDataUrl });
    });
  }

  handleChange(e) {
    this.setState({ email: e.target.value });
    this.validate();
  }

  handleBlur() {
    this.setState({ formTouched: true });
  }

  validate() {
    this.setState({ formError: !isEmail(this.state.email) });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ formTouched: true }, this.validate);
  }

  handlePrint() {
    window.open('/print', 'print');
  }

  renderBookingDetails() {
    const { bookingData } = this.props;

    return bookingData.map(b => (
      <div className="summary-page__confirmation" key={b.id}>
        <div className="summary-page__confirmation-table">
          <BookingDetails bookingData={b} />
        </div>
      </div>
    ));
  }

  renderForm() {
    const {
      sendConfirmation,
      sendConfirmationPending,
    } = this.props;

    const {
      formTouched,
      formError,
    } = this.state;

    return (
      <Form onSubmit={this.handleSubmit} className="email-form">
        <FormItem validateStatus={formTouched && formError ? 'error' : null}>
          <Input
            placeholder="Email address"
            onChange={this.handleChange}
            onBlur={this.handleBlur}
            autoFocus
          />

          {formTouched && formError && (
            <p className="form-error">Email is invalid</p>
          )}
        </FormItem>

        <div className="page__nav">
          <Button size="large" onClick={this.handlePrint}>
            Print confirmation
          </Button>

          <Button
            type="primary"
            htmlType="submit"
            size="large"
            loading={sendConfirmationPending}
            onClick={() => sendConfirmation(this.state.email)}
          >
            Send email confirmation
          </Button>
        </div>
      </Form>
    );
  }

  render() {
    const {
      confirmBooking,
      confirmBookingPending,
      confirmBookingError,
      voucherData,
      dateTime,
      reset,
    } = this.props;

    const { status } = voucherData.originalData;
    const travelDate = moment(dateTime);

    return (
      <div className="summary-page">
        {confirmBookingPending ? (
          <Spin size="large" />
        ) : (
          <div>
            {confirmBookingError ? (
              <SystemError
                text="Cannot confirm booking due to server error"
                onRetry={confirmBooking}
              />
            ) : (
              <div>
                {['STATUS_OPEN', 'STATUS_PARTIALLY_REDEEMED'].includes(status) && (
                  <div className="status status--success">
                    <Icon type="check-circle-o" />

                    <p className="status__text">
                      Your booking is confirmed for{' '}
                      <strong>
                        {travelDate.format('dddd, MMMM Do')}
                      </strong>
                      {' '}at{' '}
                      <strong>
                        {travelDate.format('h:mm a')}
                      </strong>
                      {' '}, your tickets will be delivered to your email address in the next few minutes.
                    </p>
                  </div>
                )}

                {['STATUS_REDEEMED', 'STATUS_OLCI_REDEEMED'].includes(status) && (
                  <div className="status status--warning">
                    <Icon type="exclamation-circle-o" />
                    <p className="status__text">This voucher has been already redeemed</p>

                    {status === 'STATUS_OLCI_REDEEMED' && (
                      <div>
                        <p>You can re-send your ticket using the form below</p>
                        <ConfirmationForm
                          hasName
                          onSubmit={confirmBooking}
                          submitLabel="Re-send ticket"
                        />
                      </div>
                    )}
                  </div>
                )}

                {['STATUS_OLCI_NOT_SUPPORTED'].includes(status) && (
                  <div className="status status--warning">
                    <Icon type="exclamation-circle-o" />
                    <p className="status__text">{voucherData.message}</p>
                  </div>
                )}

                {![
                  'STATUS_OPEN',
                  'STATUS_REDEEMED',
                  'STATUS_OLCI_REDEEMED',
                  'STATUS_PARTIALLY_REDEEMED',
                  'STATUS_OLCI_NOT_SUPPORTED',
                ].includes(status) && (
                    <div className="status status--error">
                      <Icon type="close-circle-o" />
                      <p>This voucher is recognized, but is no longer valid</p>
                    </div>
                  )}
                {/*{this.renderBookingDetails()}*/}
                {/*{this.renderForm()}*/}
              </div>
            )}
          </div>
        )}

        <div style={{ textAlign: 'center' }}>
          <Button size="large" onClick={reset}>Start Over</Button>
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    voucherData: state.common.voucherData,
    dateTime: state.common.dateTime,
    email: state.common.email,
    confirmBookingPending: state.common.confirmBookingPending,
    confirmBookingError: state.common.confirmBookingError,
    bookingData: state.common.bookingData,
    sendConfirmationPending: state.common.sendConfirmationPending,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(actions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SummaryPage);
