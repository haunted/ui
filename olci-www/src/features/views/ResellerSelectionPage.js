import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';
import * as actions from '../common/redux/actions';
import { ResellersList, SystemError } from '../common';


class ResellerSelectionPage extends Component {
  static propTypes = {
    fetchResellersError: PropTypes.bool,
    fetchResellersPending: PropTypes.bool.isRequired,
    fetchResellers: PropTypes.func.isRequired,
  };

  static defaultProps = {
    fetchResellersError: null,
  };

  render() {
    const {
      fetchResellers,
      fetchResellersError,
      fetchResellersPending,
    } = this.props;

    if (fetchResellersPending) {
      return (
        <div style={{ textAlign: 'center' }}>
          <Spin size="large" className="resellers-list__spin" />
        </div>
      );
    }

    return (
      <div className="reseller-selection-page">
        {fetchResellersError ? (
          <SystemError
            text="Cannot load a list of resellers due to server error"
            onRetry={fetchResellers}
          />
        ) : (
          <ResellersList />
        )}
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    fetchResellersError: state.common.fetchResellersError,
    fetchResellersPending: state.common.fetchResellersPending,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(actions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResellerSelectionPage);
