import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Button } from 'antd';
import * as actions from './redux/actions';


class PageNav extends Component {
  static propTypes = {
    nextButtonProps: PropTypes.shape({}),
    goBackButtonProps: PropTypes.shape({}),
    reset: PropTypes.func.isRequired,
    cancelButtonLabel: PropTypes.string,
  };

  static defaultProps = {
    nextButtonProps: null,
    goBackButtonProps: null,
    cancelButtonLabel: 'Cancel Check-In',
  };

  render() {
    const {
      nextButtonProps,
      goBackButtonProps,
      cancelButtonLabel,
      reset,
    } = this.props;

    return (
      <div className="page-nav">
        <button type="button" className="page-nav__cancel" onClick={reset}>
          {cancelButtonLabel}
        </button>

        <div>
          {goBackButtonProps && (
            <Button
              size="large"
              type="primary"
              {...goBackButtonProps}
            >
              Go back
            </Button>
          )}

          {nextButtonProps && (
            <Button
              size="large"
              type="primary"
              {...nextButtonProps}
            >
              Continue
            </Button>
          )}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  ...actions,
}, dispatch);

export default connect(
  null,
  mapDispatchToProps,
)(PageNav);
