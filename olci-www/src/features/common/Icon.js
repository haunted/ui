import React, { Component } from 'react';
import PropTypes from 'prop-types';
import sprite from '../../images/ui.svg';

export default class Icon extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
  };

  render() {
    const { id, ...otherProps } = this.props;

    return (
      <svg className={`icon icon-${id}`} {...otherProps}>
        <use xlinkHref={`${sprite}#${id}`} xmlnsXlink="http://www.w3.org/1999/xlink" />
      </svg>
    );
  }
}
