import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import history from '../../common/history';
import * as actions from './redux/actions';
import { ResellerTile } from './';

class ResellersList extends Component {
  static propTypes = {
    resellers: PropTypes.array.isRequired,
    fetchResellers: PropTypes.func.isRequired,
    selectReseller: PropTypes.func.isRequired,
  };

  render() {
    const { resellers } = this.props;
    const activeResellers = resellers.filter(r => r.active === true);

    return (
      <div className="resellers-list">
        {activeResellers.map(r => (
          <div key={r.code} className="resellers-list__col">
            <ResellerTile {...r} onClick={() => history.push(`/${r.code}`)} />
          </div>
        ))}
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    resellers: state.common.resellers,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    ...bindActionCreators(actions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResellersList);
