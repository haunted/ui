import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Calendar from 'antd/lib/calendar';
import cn from 'classnames';
import moment from 'moment-timezone';


export default class DatePickerCalendar extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired,
    selectedDate: PropTypes.object,
    month: PropTypes.number.isRequired,
    availableDates: PropTypes.array.isRequired,
  };

  static defaultProps = {
    selectedDate: null,
  };

  constructor(props) {
    super(props);

    this.isDateUnavailable = this.isDateUnavailable.bind(this);
  }

  isDateUnavailable(date) {
    const { availableDates } = this.props;
    const str = date.format('YYYY-MM-DD');

    return !availableDates.join(',').includes(str);
  }

  render() {
    const {
      onChange,
      selectedDate,
      month,
      availableDates,
    } = this.props;

    const selectedDateStr = selectedDate ? selectedDate.toISOString() : null;

    return (
      <div className="date-picker__calendar">
        <h5 className="date-picker__calendar-title">{moment().month(month).format('MMMM')}</h5>
        <Calendar
          fullscreen={false}
          dateFullCellRender={date => <Day date={date} active={!this.isDateUnavailable(date)} selected={date.isSame(selectedDateStr)} />}
          onSelect={onChange}
          value={moment(availableDates[0]) || moment().month(month)}
          disabledDate={this.isDateUnavailable}
        />
      </div>
    );
  }
}

const Day = ({ date, selected, active }) => (
  <div className="ant-fullcalendar-date">
    <div className={cn('ant-fullcalendar-value', { selected, active })}>
      {date.format('D')}
    </div>
  </div>
);

Day.propTypes = {
  date: PropTypes.object.isRequired,
  selected: PropTypes.bool.isRequired,
  active: PropTypes.bool.isRequired,
};
