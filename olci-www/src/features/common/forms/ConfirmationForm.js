import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { Col, Input } from 'antd';
import isEmail from 'validator/lib/isEmail';
import { TextField } from '../formElements';
import { PageNav } from '../';

const InputGroup = Input.Group;

class ConfirmationForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    onBackButtonClick: PropTypes.func,
    hasName: PropTypes.bool.isRequired,
    submitLabel: PropTypes.string,
  };

  static defaultProps = {
    submitLabel: 'Confirm booking',
    onBackButtonClick: null,
  };

  render() {
    const {
      handleSubmit,
      onBackButtonClick,
      submitting,
      hasName,
    } = this.props;

    const pageNavProps = {
      nextButtonProps: {
        htmlType: 'submit',
        loading: submitting,
      },
      goBackButtonProps: {
        onClick: onBackButtonClick || (() => {}),
      },
    };

    return (
      <form onSubmit={handleSubmit} className="confirmation-form">
        {!hasName && (
          <InputGroup>
            <Col span={11}>
              <Field
                name="firstName"
                label="First Name"
                component={TextField}
                autoFocus
              />
            </Col>
            <Col span={11} offset={2}>
              <Field
                name="lastName"
                label="Last Name"
                component={TextField}
              />
            </Col>
          </InputGroup>
        )}
        <Field
          name="email"
          label="Email address"
          component={TextField}
        />
        <Field
          name="confirmEmail"
          label="Confirm email address"
          component={TextField}
        />

        <PageNav {...pageNavProps} />
      </form>
    );
  }
}

const validate = (values, props) => {
  const errors = {};

  if (!props.hasName) {
    if (!values.firstName) {
      errors.firstName = 'First name is required';
    } else if (/\s/.test(values.firstName)) {
      errors.firstName = 'First name should not contain spaces';
    }

    if (!values.lastName) {
      errors.lastName = 'First name is required';
    } else if (/\s/.test(values.lastName)) {
      errors.lastName = 'First name should not contain spaces';
    }
  }

  if (!values.email) {
    errors.email = 'Email is required';
  } else if (!isEmail(values.email)) {
    errors.email = 'This email address is not valid';
  } else if (values.confirmEmail !== values.email) {
    errors.confirmEmail = 'Fields don\'t match';
  }

  return errors;
};

export default reduxForm({
  form: 'confirmationForm',
  validate,
})(ConfirmationForm);
