import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'antd';

export default class SystemError extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    btnLabel: PropTypes.string,
    onRetry: PropTypes.func.isRequired,
  };

  static defaultProps = {
    btnLabel: 'Try again',
  };

  render() {
    const { text, btnLabel, onRetry } = this.props;

    return (
      <div className="system-error">
        <div className="system-error__content">
          <p>{text}</p>
          <Button onClick={onRetry}>{btnLabel}</Button>
        </div>
      </div>
    );
  }
}
