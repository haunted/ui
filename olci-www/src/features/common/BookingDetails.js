import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';


export default class BookingDetails extends Component {
  static propTypes = {
    bookingData: PropTypes.object.isRequired,
  };

  render() {
    const {
      bookingData,
    } = this.props;

    const date = moment(bookingData.date);
    const excludeFields = ['id', 'barcode', 'date'];

    const displayData = Object.keys(bookingData).reduce((obj, key) => {
      if (!excludeFields.includes(key)) {
        obj[key] = bookingData[key]; // eslint-disable-line
      }

      return obj;
    }, {});

    return (
      <div className="booking-details">
        <p>
          You booking is confirmed for
          <br />
          {`${date.format('dddd MMMM DD')}th at ${date.format('hh:mm a')}`}
        </p>

        <table className="booking-details__table">
          <tbody>
            {Object.keys(displayData).map(key => (
              <tr key={key}>
                <th>{key}</th>
                <td>{displayData[key]}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
