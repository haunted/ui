import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class VoucherData extends Component {
  static propTypes = {
    voucherData: PropTypes.shape({
      bookingSource: PropTypes.string,
      travelers: PropTypes.object.isRequired,
    }).isRequired,
  };

  render() {
    const {
      voucherData: {
        name,
        bookingSource,
        travelers,
      },
    } = this.props;

    return (
      <table className="voucher-data">
        <tbody>
          {name && (
            <tr>
              <th>Name:</th>
              <td>{name}</td>
            </tr>
          )}

          <tr>
            <th>Booking Source:</th>
            <td>{bookingSource}</td>
          </tr>

          {Object.keys(travelers).map(type => (
            <tr key={type}>
              <th>Number of {type === 'Child' ? 'Children' : `${type}s`}:</th>
              <td>{travelers[type]}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}
