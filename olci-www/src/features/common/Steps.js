import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';

export default class Steps extends Component {
  static propTypes = {
    activePanel: PropTypes.number.isRequired,
    children: PropTypes.node.isRequired,
  };

  render() {
    const { activePanel, children } = this.props;

    return (
      <div className="common-steps">
        <div className="steps">
          {Children.map(children, (panel, index) => React.cloneElement(panel, {
            active: index === activePanel,
            complete: index < activePanel,
          }))}
        </div>
      </div>
    );
  }
}
