import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

export default class StepPanel extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string,
    activeSubtitle: PropTypes.string,
    completeSubtitle: PropTypes.string,
    active: PropTypes.bool,
    complete: PropTypes.bool,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    subtitle: '',
    activeSubtitle: '',
    completeSubtitle: '',
    active: false,
    complete: false,
  };

  render() {
    const {
      title,
      subtitle,
      activeSubtitle,
      completeSubtitle,
      active,
      complete,
      children,
    } = this.props;

    const panelClassName = cn('steps__panel', {
      'steps__panel--active': active,
      'steps__panel--complete': complete,
    });

    return (
      <div className={panelClassName}>
        <div className="steps__panel-header">
          <h2>{title}</h2>
          {subtitle && !active && !complete && (
            <p className="steps__panel-subtitle">{subtitle}</p>
          )}

          {activeSubtitle && active && (
            <p className="steps__panel-subtitle">{activeSubtitle}</p>
          )}

          {completeSubtitle && complete && (
            <p className="steps__panel-subtitle">{completeSubtitle}</p>
          )}
        </div>
        <div className="steps__panel-content">{active && children}</div>
      </div>
    );
  }
}
