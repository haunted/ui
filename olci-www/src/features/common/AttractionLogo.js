/* eslint-disable import/no-dynamic-require */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

class AttractionLogo extends Component {
  static propTypes = {
    brandId: PropTypes.string.isRequired,
    config: PropTypes.shape({
      logo: PropTypes.string,
    }).isRequired,
  };

  render() {
    const { brandId, config } = this.props;

    return (
      <div className="header">
        <div className="attraction-logo">
          <img
            src={require(`../../brands/${brandId}/${config.attractionLogo}`)}
            alt={config.attractionName}
          />
        </div>
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    brandId: state.common.brandId,
    config: state.common.config,
  };
}

export default connect(
  mapStateToProps,
)(AttractionLogo);
