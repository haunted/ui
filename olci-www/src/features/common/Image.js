import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import cn from 'classnames';

export default class Image extends Component {
  static propTypes = {
    src: PropTypes.string.isRequired,
    alt: PropTypes.string,
  };

  static defaultProps = {
    alt: '',
  };

  state = {
    isLoading: true,
    error: false,
  };

  render() {
    const { src, alt, ...otherProps } = this.props;
    const { isLoading, error } = this.state;

    return (
      <div className={cn('image', { 'is-loading': isLoading, 'has-error': error })}>
        <img
          src={src}
          alt={alt}
          onLoad={() => this.setState({ isLoading: false })}
          onError={() => this.setState({ error: true })}
          {...otherProps}
        />
        <Spin className="image__spin" size="small" spinning={isLoading && !error} />
        {error && (
          <p className="image__error">{alt}</p>
        )}
      </div>
    );
  }
}
