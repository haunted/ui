import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import { Button } from 'antd';

const ButtonGroup = Button.Group;

export default class TimePicker extends Component {
  static propTypes = {
    value: PropTypes.string,
    pattern: PropTypes.string,
    availableTimes: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    value: null,
    pattern: null,
  };

  constructor(props) {
    super(props);

    this.groupTimeByHours = this.groupTimeByHours.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.groupedTime = this.groupTimeByHours();
  }

  handleChange(time) {
    this.props.onChange(time);
  }

  groupTimeByHours() {
    const { availableTimes, pattern } = this.props;

    return availableTimes.reduce((obj, a) => {
      const hours = moment(a.time, pattern).hours();
      const data = {
        ...a,
        formattedTime: moment(a.time, pattern).format('hh:mm a'),
      };

      if (!obj[hours]) {
        return {
          ...obj,
          [hours]: [data],
        };
      }

      obj[hours].push(data);

      return obj;
    }, {});
  }

  renderButtons(data) {
    const { value } = this.props;

    return data.map(a => (
      <Button
        className="time-picker__button"
        key={a.time}
        disabled={!a.isAvailable}
        size="large"
        type={value && value === a.time ? 'primary' : 'default'}
        onClick={() => this.handleChange(a.time)}
      >
        {a.formattedTime}
      </Button>
    ));
  }

  render() {
    const groupedTimesByHours = this.groupedTime;

    return (
      <div className="time-picker">
        {Object.keys(groupedTimesByHours).map(key => (
          <ButtonGroup key={key} className="time-picker__row">
            {this.renderButtons(groupedTimesByHours[key])}
          </ButtonGroup>
        ))}
      </div>
    );
  }
}
