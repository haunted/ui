import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image } from '../common';

export default class ResellerTile extends Component {
  static propTypes = {
    logo: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  };

  render() {
    const { logo, name, onClick } = this.props;

    return (
      <button className="reseller-tile" onClick={onClick}>
        <div className="reseller-tile__content">
          <div className="reseller-tile__image">
            <Image src={logo} alt={name} />
          </div>
        </div>
      </button>
    );
  }
}
