import React, { Component } from 'react';
import { Icon } from './';

export default class Footer extends Component {
  static propTypes = {};

  render() {
    return (
      <div className="footer">
        <span className="copyright">Powered by</span>
        <a href="http://redeam.com/" target="_blank" rel="noopener noreferrer">
          <Icon id="logo" width="120" height="20" fill="#28337c" />
        </a>
      </div>
    );
  }
}
