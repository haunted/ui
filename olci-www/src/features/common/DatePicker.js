import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import { Spin } from 'antd';
import { DatePickerCalendar } from './';


export default class DatePicker extends Component {
  static propTypes = {
    value: PropTypes.object,
    dates: PropTypes.object.isRequired,
    onChange: PropTypes.func,
    loadData: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    displayMonths: PropTypes.number,
  };

  static defaultProps = {
    value: null,
    displayMonths: 3,
    onChange: () => {},
  };

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillMount() {
    const { loadData, displayMonths } = this.props;
    const now = moment();

    loadData({
      fromDate: now.toISOString(),
      toDate: moment().month((now.month() + displayMonths) - 1).endOf('month').toISOString(),
    });
  }

  handleChange(date) {
    this.props.onChange(date);
  }

  render() {
    const { dates, value, displayMonths, isLoading } = this.props;
    const now = moment();
    const calendars = [];

    for (let i = now.month(); i < now.month() + displayMonths; i += 1) {
      calendars.push({
        month: i % 12,
        selectedDate: value,
        availableDates: dates[i % 12] || [],
        onChange: this.handleChange,
        disabled: !dates[i],
        loading: isLoading,
      });
    }

    return (
      <div>
        <Spin spinning={isLoading}>
          <div className="date-picker">
            {calendars.map(calendarProps => (
              <div key={calendarProps.month} className="date-picker__col">
                <DatePickerCalendar {...calendarProps} />
              </div>
            ))}
          </div>
        </Spin>
      </div>
    );
  }
}
