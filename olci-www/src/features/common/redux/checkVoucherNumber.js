import axios from 'axios';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import { setStep, selectDate, selectTime } from './actions';
import { updateStore, clearStore } from '../../../common/utils';
import { STEP_DATE_SELECTION, STEP_SUMMARY } from '../../../common/steps';
import {
  COMMON_CHECK_VOUCHER_NUMBER_BEGIN,
  COMMON_CHECK_VOUCHER_NUMBER_SUCCESS,
  COMMON_CHECK_VOUCHER_NUMBER_FAILURE,
  COMMON_CHECK_VOUCHER_NUMBER_NOT_VALID,
  COMMON_CHECK_VOUCHER_NUMBER_DISMISS_ERROR,
} from './constants';

export function checkVoucherNumber(data, replace) {
  return {
    type: COMMON_CHECK_VOUCHER_NUMBER_BEGIN,
    data,
    replace,
  };
}

export function dismissCheckVoucherNumberError() {
  return {
    type: COMMON_CHECK_VOUCHER_NUMBER_DISMISS_ERROR,
  };
}

export function* doCheckVoucherNumber({ data, replace }) {
  let response;

  try {
    response = yield call(axios.get, `/vouchers/${data.number}`);
  } catch (err) {
    yield put({
      type: COMMON_CHECK_VOUCHER_NUMBER_FAILURE,
      error: 'Request failed. Please try again later.',
    });

    return;
  }

  if (!response.data.vouchers || !response.data.vouchers.length) {
    yield put({
      type: COMMON_CHECK_VOUCHER_NUMBER_NOT_VALID,
      error:
        'We could not find your voucher. Please verify your entry and try again.',
    });

    return;
  }

  const voucher = response.data.vouchers[0];
  const uiMessage = response.data.ui && response.data.ui.message;
  const xTicket = response.data.xTicket;
  const xPass = response.data.xPass;

  const { resellers, initPending } = yield select(state => state.common);
  const travelers = voucher.travelers.reduce((obj, { travelerType }) => {
    if (obj[travelerType]) {
      obj[travelerType] += 1; // eslint-disable-line

      return obj;
    }

    return {
      ...obj,
      [travelerType]: 1,
    };
  }, {});

  yield call(clearStore);
  yield call(updateStore, 'voucherRef', data.number);

  yield put({
    type: COMMON_CHECK_VOUCHER_NUMBER_SUCCESS,
    data: {
      originalData: voucher,
      ref: data.number,
      bookingSource: resellers.find(r => r.code === voucher.resellerCode).name,
      travelers,
      xPass,
      xTicket,
      message: uiMessage,
    },
  });

  if (['STATUS_OPEN', 'STATUS_PARTIALLY_REDEEMED'].includes(voucher.status)) {
    if (!initPending) {
      yield put(setStep(STEP_DATE_SELECTION, true, replace));
    }
  } else {
    yield call(updateStore, 'finished', true);

    yield put(selectDate(voucher.travelDate));
    yield put(selectTime(voucher.travelDate));
    yield put(setStep(STEP_SUMMARY, true, replace));
  }
}

function* showError(action) {
  yield call(message.error, action.error, 3);
}

export function* watchCheckVoucherNumber() {
  yield takeLatest(COMMON_CHECK_VOUCHER_NUMBER_BEGIN, doCheckVoucherNumber);

  // TODO make it better
  yield takeLatest(
    [
      COMMON_CHECK_VOUCHER_NUMBER_NOT_VALID,
      COMMON_CHECK_VOUCHER_NUMBER_FAILURE,
    ],
    showError,
  );
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case COMMON_CHECK_VOUCHER_NUMBER_BEGIN:
      return {
        ...state,
        checkVoucherNumberPending: true,
        checkVoucherNumberError: null,
      };

    case COMMON_CHECK_VOUCHER_NUMBER_SUCCESS:
      return {
        ...state,
        checkVoucherNumberPending: false,
        checkVoucherNumberError: null,
        voucherData: action.data,
      };

    case COMMON_CHECK_VOUCHER_NUMBER_FAILURE:
    case COMMON_CHECK_VOUCHER_NUMBER_NOT_VALID:
      return {
        ...state,
        checkVoucherNumberPending: false,
        checkVoucherNumberError: true,
      };

    case COMMON_CHECK_VOUCHER_NUMBER_DISMISS_ERROR:
      return {
        ...state,
        checkVoucherNumberError: null,
      };

    default:
      return state;
  }
}
