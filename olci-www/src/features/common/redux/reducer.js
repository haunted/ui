import initialState from './initialState';
import { reducer as setStepReducer } from './setStep';
import { reducer as checkVoucherNumberReducer } from './checkVoucherNumber';
import { reducer as resetReducer } from './reset';
import { reducer as fetchResellersReducer } from './fetchResellers';
import { reducer as selectResellerReducer } from './selectReseller';
import { reducer as selectDateReducer } from './selectDate';
import { reducer as selectTimeReducer } from './selectTime';
import { reducer as setConfigReducer } from './setConfig';
import { reducer as getAvailableDatesReducer } from './getAvailableDates';
import { reducer as confirmBookingReducer } from './confirmBooking';
import { reducer as systemErrorReducer } from './systemError';
import { reducer as sendConfirmationReducer } from './sendConfirmation';
import { reducer as getBookingDataReducer } from './getBookingData';
import { reducer as getAvailableTimesReducer } from './getAvailableTimes';
import { reducer as initReducer } from './init';

const reducers = [
  setStepReducer,
  checkVoucherNumberReducer,
  resetReducer,
  fetchResellersReducer,
  selectResellerReducer,
  selectDateReducer,
  selectTimeReducer,
  setConfigReducer,
  getAvailableDatesReducer,
  confirmBookingReducer,
  systemErrorReducer,
  sendConfirmationReducer,
  getBookingDataReducer,
  getAvailableTimesReducer,
  initReducer,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    default:
      newState = state;
      break;
  }
  /* istanbul ignore next */
  return reducers.reduce((s, r) => r(s, action), newState);
}
