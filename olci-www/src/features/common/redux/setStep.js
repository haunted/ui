import { put, select, call, takeLatest } from 'redux-saga/effects';
import history from '../../../common/history';
import { getStepByPath } from '../../../common/utils';
import {
  tryFetchResellers,
  trySelectReseller,
  tryVerifyVoucher,
} from './initHelpers';
import { systemError } from './actions';
import {
  STEP_RESELLER_SELECTION,
  STEP_VOUCHER_VERIFICATION,
  STEP_DATE_SELECTION,
  STEP_TIME_SELECTION,
  STEP_CONFIRMATION,
  STEP_SUMMARY,
} from '../../../common/steps';

import {
  COMMON_SET_STEP_BEGIN,
  COMMON_SET_STEP,
  PAGE_NOT_AVAILABLE,
} from './constants';

export function setStep(step, redirect = true, replace = false) {
  return {
    type: COMMON_SET_STEP_BEGIN,
    step,
    redirect,
    replace,
  };
}

function* doRedirect(path, condition, replace) {
  if (condition) {
    yield call(history[replace ? 'replace' : 'push'], path);
  }
}

export function* doSetStep({ step, redirect, replace }) {
  const {
    selectedReseller: resellerCode,
    voucherData,
    date,
    dateTime,
    initPending,
  } = yield select(state => state.common);

  let pageNotAvailable = false;
  let systemErrorStatus = null;

  if (!initPending) {
    try {
      yield tryFetchResellers(step === STEP_RESELLER_SELECTION);
    } catch (err) {
      yield put(
        systemError(err.message, 'Could not fetch resellers. Try again later.'),
      );
    }

    switch (step) {
      case STEP_RESELLER_SELECTION:
        yield doRedirect('/', redirect, replace);

        break;
      case STEP_VOUCHER_VERIFICATION:
        yield doRedirect(`/${resellerCode}`, redirect, replace);

        try {
          yield trySelectReseller();
          yield tryVerifyVoucher();
        } catch (err) {
          // do nothing additionally
        }

        break;
      case STEP_DATE_SELECTION:
        yield doRedirect(`/${resellerCode}/date`, redirect, replace);

        if (!voucherData) {
          pageNotAvailable = true;
        }

        break;
      case STEP_TIME_SELECTION:
        yield doRedirect(`/${resellerCode}/time`, redirect, replace);

        if (!voucherData || !date) {
          pageNotAvailable = true;
        }

        break;
      case STEP_CONFIRMATION:
        yield doRedirect(`/${resellerCode}/confirmation`, redirect, replace);

        if (!voucherData || !date || !dateTime) {
          pageNotAvailable = true;
        }

        break;
      case STEP_SUMMARY:
        yield doRedirect(`/${resellerCode}/summary`, redirect, replace);

        const status =
          voucherData &&
          voucherData.originalData &&
          voucherData.originalData.status;

        if (
          (!voucherData || !date || !dateTime) &&
          status !== 'STATUS_OLCI_NOT_SUPPORTED'
        ) {
          pageNotAvailable = true;
        }

        break;
      default:
    }

    // Set or clear systemError to with single action to avoid additional render
    if (pageNotAvailable) {
      systemErrorStatus = systemError(
        PAGE_NOT_AVAILABLE,
        'This page is no longer available',
      ).status;
    }
  }

  yield put({
    type: COMMON_SET_STEP,
    step,
    systemErrorStatus,
  });
}

export function reducer(state, { type, step, systemErrorStatus }) {
  switch (type) {
    case COMMON_SET_STEP_BEGIN:
      return {
        ...state,
      };

    case COMMON_SET_STEP:
      return {
        ...state,
        step,
        systemErrorStatus,
        resellerNotFound: false,
      };

    default:
      return state;
  }
}

function* handleLocationChange({ payload }) {
  yield put(setStep(getStepByPath(payload.pathname), false));
}

export function* watchSetStep() {
  yield takeLatest(COMMON_SET_STEP_BEGIN, doSetStep);

  // Change step automatically on location change
  yield takeLatest('@@router/LOCATION_CHANGE', handleLocationChange);
}
