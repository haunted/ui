import axios from 'axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import {
  COMMON_FETCH_RESELLERS_BEGIN,
  COMMON_FETCH_RESELLERS_SUCCESS,
  COMMON_FETCH_RESELLERS_FAILURE,
  COMMON_FETCH_RESELLERS_DISMISS_ERROR,
} from './constants';

export function fetchResellers() {
  return {
    type: COMMON_FETCH_RESELLERS_BEGIN,
  };
}

export function dismissFetchResellersError() {
  return {
    type: COMMON_FETCH_RESELLERS_DISMISS_ERROR,
  };
}

export function* doFetchResellers() {
  let response;

  try {
    response = yield call(axios.get, '/resellers');
  } catch (err) {
    yield put({
      type: COMMON_FETCH_RESELLERS_FAILURE,
      data: { error: err },
    });

    return;
  }

  // Add a logo fields to each reseller
  const payload = response.data.resellers.map(r => ({
    ...r,
    logo: `https://storage.googleapis.com/redeam-olci/${r.code}_logo.png`,
  }));

  yield put({
    type: COMMON_FETCH_RESELLERS_SUCCESS,
    payload,
  });
}

export function* watchFetchResellers() {
  yield takeLatest(COMMON_FETCH_RESELLERS_BEGIN, doFetchResellers);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case COMMON_FETCH_RESELLERS_BEGIN:
      return {
        ...state,
        fetchResellersPending: true,
        fetchResellersError: null,
        resellerNotFound: false,
      };

    case COMMON_FETCH_RESELLERS_SUCCESS:
      return {
        ...state,
        fetchResellersPending: false,
        fetchResellersError: null,
        resellers: action.payload,
      };

    case COMMON_FETCH_RESELLERS_FAILURE:
      return {
        ...state,
        fetchResellersPending: false,
        fetchResellersError: true,
      };

    case COMMON_FETCH_RESELLERS_DISMISS_ERROR:
      return {
        ...state,
        fetchResellersError: null,
      };

    default:
      return state;
  }
}
