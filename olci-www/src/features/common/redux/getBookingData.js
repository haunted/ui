import axios from 'axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import {
  COMMON_GET_BOOKING_DATA_BEGIN,
  COMMON_GET_BOOKING_DATA_SUCCESS,
  COMMON_GET_BOOKING_DATA_FAILURE,
  COMMON_GET_BOOKING_DATA_DISMISS_ERROR,
} from './constants';

export function getBookingData(id) {
  return {
    type: COMMON_GET_BOOKING_DATA_BEGIN,
    id,
  };
}

export function dismissGetBookingDataError() {
  return {
    type: COMMON_GET_BOOKING_DATA_DISMISS_ERROR,
  };
}

export function* doGetBookingData({ id }) {
  let response;

  try {
    response = yield call(axios.get, `/bookings/${id}`);
  } catch (err) {
    yield put({
      type: COMMON_GET_BOOKING_DATA_FAILURE,
      data: { error: err },
    });

    return;
  }

  yield put({
    type: COMMON_GET_BOOKING_DATA_SUCCESS,
    data: response.data.tickets,
  });
}

export function* watchGetBookingData() {
  yield takeLatest(COMMON_GET_BOOKING_DATA_BEGIN, doGetBookingData);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case COMMON_GET_BOOKING_DATA_BEGIN:
      return {
        ...state,
        getBookingDataPending: true,
        getBookingDataError: null,
      };

    case COMMON_GET_BOOKING_DATA_SUCCESS:
      return {
        ...state,
        getBookingDataPending: false,
        getBookingDataError: null,
        bookingData: action.data,
      };

    case COMMON_GET_BOOKING_DATA_FAILURE:
      return {
        ...state,
        getBookingDataPending: false,
        getBookingDataError: action.data.error,
      };

    case COMMON_GET_BOOKING_DATA_DISMISS_ERROR:
      return {
        ...state,
        getBookingDataError: null,
      };

    default:
      return state;
  }
}
