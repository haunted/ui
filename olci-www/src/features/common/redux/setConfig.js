import {
  COMMON_SET_CONFIG,
} from './constants';

export function setConfig(config) {
  return {
    type: COMMON_SET_CONFIG,
    payload: config,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case COMMON_SET_CONFIG:
      return {
        ...state,
        config: action.payload,
      };

    default:
      return state;
  }
}
