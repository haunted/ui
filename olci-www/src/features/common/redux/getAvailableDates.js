import axios from 'axios';
import moment from 'moment-timezone';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
  COMMON_GET_AVAILABLE_DATES_BEGIN,
  COMMON_GET_AVAILABLE_DATES_SUCCESS,
  COMMON_GET_AVAILABLE_DATES_FAILURE,
  COMMON_GET_AVAILABLE_DATES_DISMISS_ERROR,
} from './constants';

export function getAvailableDates(params) {
  return {
    type: COMMON_GET_AVAILABLE_DATES_BEGIN,
    params,
  };
}

export function dismissGetAvailableDatesError() {
  return {
    type: COMMON_GET_AVAILABLE_DATES_DISMISS_ERROR,
  };
}

export function* doGetAvailableDates(action) {
  let response;

  const now = moment();
  const { xTicket, xPass } = yield select(state => state.common.voucherData);
  const common = { xTicket, xPass };

  const defaults = {
    fromDate: now.toISOString(),
    toDate: moment()
      .month(now.month() + 2)
      .endOf('month')
      .toISOString(),
    ...common,
  };

  const params = action.params ? { ...action.params, ...common } : defaults;

  try {
    response = yield call(axios.post, '/available-dates', { ...params });
  } catch (err) {
    yield put({
      type: COMMON_GET_AVAILABLE_DATES_FAILURE,
      data: { error: err },
    });

    return;
  }
  moment.tz.setDefault(response.data.displayTz);

  // TODO API should return dates in RFC3339 format
  const dates = response.data.dates.map(d =>
    moment(d, 'M/D/YYYY').toISOString(),
  );

  const groupedDates = dates.reduce((obj, date) => {
    const month = moment(date).month();

    if (!obj[month]) {
      obj[month] = []; // eslint-disable-line
    }

    obj[month].push(date);

    return obj;
  }, {});

  yield put({
    type: COMMON_GET_AVAILABLE_DATES_SUCCESS,
    dates,
    groupedDates,
  });
}

export function* watchGetAvailableDates() {
  yield takeLatest(COMMON_GET_AVAILABLE_DATES_BEGIN, doGetAvailableDates);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case COMMON_GET_AVAILABLE_DATES_BEGIN:
      return {
        ...state,
        getAvailableDatesPending: true,
        getAvailableDatesError: null,
      };

    case COMMON_GET_AVAILABLE_DATES_SUCCESS:
      return {
        ...state,
        getAvailableDatesPending: false,
        getAvailableDatesError: null,
        availableDates: action.dates,
        groupedDates: action.groupedDates,
      };

    case COMMON_GET_AVAILABLE_DATES_FAILURE:
      return {
        ...state,
        getAvailableDatesPending: false,
        getAvailableDatesError: true,
      };

    case COMMON_GET_AVAILABLE_DATES_DISMISS_ERROR:
      return {
        ...state,
        getAvailableDatesError: null,
      };

    default:
      return state;
  }
}
