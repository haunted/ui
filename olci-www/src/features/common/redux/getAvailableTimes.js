import axios from 'axios';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
  COMMON_GET_AVAILABLE_TIMES_BEGIN,
  COMMON_GET_AVAILABLE_TIMES_SUCCESS,
  COMMON_GET_AVAILABLE_TIMES_FAILURE,
  COMMON_GET_AVAILABLE_TIMES_DISMISS_ERROR,
} from './constants';

export function getAvailableTimes() {
  return {
    type: COMMON_GET_AVAILABLE_TIMES_BEGIN,
  };
}

export function dismissGetAvailableTimesError() {
  return {
    type: COMMON_GET_AVAILABLE_TIMES_DISMISS_ERROR,
  };
}

export function* doGetAvailableTimes() {
  let response;

  const date = yield select(state => state.common.date);
  const { travelers, xTicket, xPass } = yield select(
    state => state.common.voucherData,
  );
  const travelersNumber = Object.values(travelers).reduce(
    (total, num) => total + num,
    0,
  );

  try {
    response = yield call(axios.post, '/available-times', {
      date,
      xTicket,
      xPass,
    });
  } catch (err) {
    yield put({
      type: COMMON_GET_AVAILABLE_TIMES_FAILURE,
      data: { error: err },
    });

    return;
  }

  const availableTimes = response.data.avails.map(a => ({
    time: a.t,
    isAvailable: travelersNumber <= a.c,
  }));

  yield put({
    type: COMMON_GET_AVAILABLE_TIMES_SUCCESS,
    data: availableTimes,
  });
}

export function* watchGetAvailableTimes() {
  yield takeLatest(COMMON_GET_AVAILABLE_TIMES_BEGIN, doGetAvailableTimes);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case COMMON_GET_AVAILABLE_TIMES_BEGIN:
      return {
        ...state,
        getAvailableTimesPending: true,
        getAvailableTimesError: null,
      };

    case COMMON_GET_AVAILABLE_TIMES_SUCCESS:
      return {
        ...state,
        getAvailableTimesPending: false,
        getAvailableTimesError: null,
        availableTimes: action.data,
      };

    case COMMON_GET_AVAILABLE_TIMES_FAILURE:
      return {
        ...state,
        getAvailableTimesPending: false,
        getAvailableTimesError: true,
      };

    case COMMON_GET_AVAILABLE_TIMES_DISMISS_ERROR:
      return {
        ...state,
        getAvailableTimesError: null,
      };

    default:
      return state;
  }
}
