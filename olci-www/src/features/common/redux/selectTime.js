import { updateStore } from '../../../common/utils';
import {
  COMMON_SELECT_TIME,
} from './constants';

export function selectTime(time) {
  updateStore('dateTime', time);

  return {
    type: COMMON_SELECT_TIME,
    payload: time,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case COMMON_SELECT_TIME:
      return {
        ...state,
        dateTime: action.payload,
      };

    default:
      return state;
  }
}
