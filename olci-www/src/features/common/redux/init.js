import { put, select, call, take, takeLatest } from 'redux-saga/effects';
import history from '../../../common/history';
import { clearStore, getStoreValue } from '../../../common/utils';
import {
  tryFetchResellers,
  trySelectReseller,
  tryVerifyVoucher,
  tryFetchDates,
  trySelectDate,
  tryFetchTimes,
  trySelectDateTime,
} from './initHelpers';
import { systemError } from './actions';
import {
  STEP_RESELLER_SELECTION,
  STEP_VOUCHER_VERIFICATION,
  STEP_DATE_SELECTION,
  STEP_TIME_SELECTION,
  STEP_CONFIRMATION,
  STEP_SUMMARY,
} from '../../../common/steps';
import {
  COMMON_INIT_BEGIN,
  COMMON_INIT_SUCCESS,
  COMMON_INIT_FAILURE,
  COMMON_INIT_DISMISS_ERROR,
  COMMON_FETCH_RESELLERS_FAILURE,
  COMMON_RESELLER_NOT_FOUND,
  COMMON_SET_STEP,
} from './constants';

export function init() {
  return {
    type: COMMON_INIT_BEGIN,
  };
}

export function dismissInitError() {
  return {
    type: COMMON_INIT_DISMISS_ERROR,
  };
}

function* redirectHome() {
  yield call(history.replace, '/');
  yield take(COMMON_SET_STEP);
}

export function* doInit() {
  const { step } = yield select(state => state.common);
  const finished = yield call(getStoreValue, 'finished');

  if (finished && step !== STEP_SUMMARY) {
    yield call(clearStore);
  }

  const voucherRef = yield call(getStoreValue, 'voucherRef');
  const date = yield call(getStoreValue, 'date');
  const dateTime = yield call(getStoreValue, 'dateTime');

  try {
    // Reseller list should be loaded for every step
    yield tryFetchResellers(true);

    switch (step) {
      case STEP_RESELLER_SELECTION:
        yield call(clearStore);

        break;
      case STEP_VOUCHER_VERIFICATION:
        yield trySelectReseller();
        yield tryVerifyVoucher(voucherRef);

        break;
      case STEP_DATE_SELECTION:
        yield trySelectReseller();

        if (voucherRef) {
          yield tryVerifyVoucher(voucherRef);
          yield tryFetchDates();

          if (date) {
            yield trySelectDate(date);
          }
        } else {
          yield redirectHome();
        }

        break;
      case STEP_TIME_SELECTION:
      case STEP_CONFIRMATION:
        yield trySelectReseller();

        if (voucherRef && date) {
          yield tryVerifyVoucher(voucherRef);
          yield tryFetchDates();
          yield trySelectDate(date);
          yield tryFetchTimes();

          if (dateTime) {
            yield trySelectDateTime(dateTime);
          }
        } else {
          yield redirectHome();
        }

        break;
      case STEP_SUMMARY:
        yield trySelectReseller();

        if (voucherRef) {
          yield tryVerifyVoucher(voucherRef);
        } else {
          yield redirectHome();
        }

        break;
      default:
    }
  } catch (err) {
    yield put(systemError(err.message, err.message));

    switch (err.message) {
      case COMMON_FETCH_RESELLERS_FAILURE:
        break;
      case COMMON_RESELLER_NOT_FOUND:
        break;
      default:
    }

    yield put({
      type: COMMON_INIT_FAILURE,
      reason: err.message,
    });

    return;
  }

  yield put({
    type: COMMON_INIT_SUCCESS,
  });
}

export function* watchInit() {
  yield takeLatest(COMMON_INIT_BEGIN, doInit);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case COMMON_INIT_BEGIN:
      return {
        ...state,
        initPending: true,
      };

    case COMMON_INIT_SUCCESS:
      return {
        ...state,
        initPending: false,
      };

    case COMMON_INIT_FAILURE:
      return {
        ...state,
        initPending: false,
      };

    case COMMON_INIT_DISMISS_ERROR:
      return {
        ...state,
      };

    default:
      return state;
  }
}
