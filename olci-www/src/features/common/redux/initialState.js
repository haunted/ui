import invariant from 'invariant';
import history from '../../../common/history';
import { getStepByPath } from '../../../common/utils';

/*
 * Get BRAND_ID from environment variables.
 * If it's not defined then fallback to subdomain.
 */
export function getBrandId() {
  const domain = process.env.REACT_APP_DOMAIN;
  const subDomain = window.location.host.split('.')[0];
  const brandId = process.env.REACT_APP_BRAND_ID;

  // If there is no BRAND_ID set in environment variables and we can't get subdomain from the url
  // then rise an error
  invariant(
    subDomain || brandId,
    `You must either set BRAND_ID in environment variables or run the app on "<brand id>.${domain}" domain`,
  );

  return brandId || subDomain;
}

const initialState = {
  systemErrorStatus: null,
  config: null,
  brandId: getBrandId(),
  step: getStepByPath(history.location.pathname),
  checkVoucherNumberPending: false,
  checkVoucherNumberError: null,
  voucherData: null,
  fetchResellersPending: false,
  fetchResellersError: null,
  resellers: [],
  selectedReseller: null,
  resellerNotFound: false,
  getAvailableDatesPending: false,
  getAvailableDatesError: null,
  availableDates: [],
  availableTimes: [],
  groupedDates: {},
  date: null,
  dateTime: null,
  email: null,
  confirmBookingPending: false,
  confirmBookingError: null,
  sendConfirmationPending: false,
  sendConfirmationError: null,
  bookingData: [],
  getBookingDataPending: false,
  getBookingDataError: null,
  getAvailableTimesPending: false,
  getAvailableTimesError: null,
  initPending: true,
  initError: null,
};

export default initialState;
