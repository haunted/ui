import { updateStore } from '../../../common/utils';
import {
  COMMON_SELECT_DATE,
} from './constants';

export function selectDate(date) {
  updateStore('date', date);

  return {
    type: COMMON_SELECT_DATE,
    payload: date,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case COMMON_SELECT_DATE:
      return {
        ...state,
        dateTime: null,
        date: action.payload,
      };

    default:
      return state;
  }
}
