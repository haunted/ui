import axios from 'axios';
import { message } from 'antd';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import {
  COMMON_SEND_CONFIRMATION_BEGIN,
  COMMON_SEND_CONFIRMATION_SUCCESS,
  COMMON_SEND_CONFIRMATION_FAILURE,
  COMMON_SEND_CONFIRMATION_DISMISS_ERROR,
} from './constants';

export function sendConfirmation(email) {
  return {
    type: COMMON_SEND_CONFIRMATION_BEGIN,
    email,
  };
}

export function dismissSendConfirmationError() {
  return {
    type: COMMON_SEND_CONFIRMATION_DISMISS_ERROR,
  };
}

export function* doSendConfirmation({ email }) {
  try {
    const bookingData = yield select(state => state.common.bookingData);

    yield call(axios.post, `/bookings/${bookingData.id}/email`, { email });
  } catch (err) {
    yield call(message.error, 'Cannot send email confirmation', 3);
    yield put({
      type: COMMON_SEND_CONFIRMATION_FAILURE,
      data: { error: err },
    });

    return;
  }

  yield call(message.success, 'Email confirmation sent successfully', 3);
  yield put({
    type: COMMON_SEND_CONFIRMATION_SUCCESS,
  });
}

export function* watchSendConfirmation() {
  yield takeLatest(COMMON_SEND_CONFIRMATION_BEGIN, doSendConfirmation);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case COMMON_SEND_CONFIRMATION_BEGIN:
      return {
        ...state,
        sendConfirmationPending: true,
        sendConfirmationError: null,
      };

    case COMMON_SEND_CONFIRMATION_SUCCESS:
      return {
        ...state,
        sendConfirmationPending: false,
        sendConfirmationError: null,
      };

    case COMMON_SEND_CONFIRMATION_FAILURE:
      return {
        ...state,
        sendConfirmationPending: false,
        sendConfirmationError: action.data.error,
      };

    case COMMON_SEND_CONFIRMATION_DISMISS_ERROR:
      return {
        ...state,
        sendConfirmationError: null,
      };

    default:
      return state;
  }
}
