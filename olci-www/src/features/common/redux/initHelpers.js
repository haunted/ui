import { put, select, take, call } from 'redux-saga/effects';
import { getLocationMatch, } from '../../../common/utils';

import {
  fetchResellers,
  selectReseller,
  checkVoucherNumber,
  getAvailableDates,
  selectDate,
  getAvailableTimes,
  selectTime,
} from './actions';

import {
  COMMON_FETCH_RESELLERS_SUCCESS,
  COMMON_FETCH_RESELLERS_FAILURE,

  COMMON_SELECT_RESELLER,
  COMMON_RESELLER_NOT_FOUND,

  COMMON_CHECK_VOUCHER_NUMBER_SUCCESS,
  COMMON_CHECK_VOUCHER_NUMBER_FAILURE,
  COMMON_CHECK_VOUCHER_NUMBER_NOT_VALID,

  COMMON_GET_AVAILABLE_DATES_SUCCESS,
  COMMON_GET_AVAILABLE_DATES_FAILURE,

  COMMON_GET_AVAILABLE_TIMES_SUCCESS,
  COMMON_GET_AVAILABLE_TIMES_FAILURE,
} from './constants';

export function* tryFetchResellers(forceUpdate = false) {
  const { resellers } = yield select(state => state.common);

  if (!resellers.length || forceUpdate) {
    yield put(fetchResellers());
    const action = yield take([COMMON_FETCH_RESELLERS_SUCCESS, COMMON_FETCH_RESELLERS_FAILURE]);

    if (action.type === COMMON_FETCH_RESELLERS_FAILURE) {
      throw new Error(COMMON_FETCH_RESELLERS_FAILURE);
    }
  }
}

export function* trySelectReseller() {
  const match = yield call(getLocationMatch);
  const resellerCode = match && match.params.resellerCode;

  if (resellerCode) {
    yield put(selectReseller(resellerCode));
    const action = yield take([COMMON_SELECT_RESELLER, COMMON_RESELLER_NOT_FOUND]);

    if (action.type === COMMON_RESELLER_NOT_FOUND) {
      throw new Error(action.type);
    }
  }
}

export function* tryVerifyVoucher(storeValue) {
  const match = yield call(getLocationMatch);
  const voucherRef = (match && match.params.voucherRef) || storeValue;

  if (voucherRef) {
    yield put(checkVoucherNumber({ number: voucherRef }, true));
    const action = yield take([
      COMMON_CHECK_VOUCHER_NUMBER_SUCCESS,
      COMMON_CHECK_VOUCHER_NUMBER_FAILURE,
      COMMON_CHECK_VOUCHER_NUMBER_NOT_VALID,
    ]);

    if ([COMMON_CHECK_VOUCHER_NUMBER_FAILURE, COMMON_CHECK_VOUCHER_NUMBER_NOT_VALID].includes(action.type)) {
      throw new Error(action.type);
    }
  }
}

export function* tryFetchDates() {
  yield put(getAvailableDates());
  const action = yield take([COMMON_GET_AVAILABLE_DATES_SUCCESS, COMMON_GET_AVAILABLE_DATES_FAILURE]);

  if (action.type === COMMON_GET_AVAILABLE_DATES_FAILURE) {
    throw new Error(COMMON_GET_AVAILABLE_DATES_FAILURE);
  }
}

export function* trySelectDate(storeValue) {
  // todo check if the date is still available
  yield put(selectDate(storeValue));
}

export function* tryFetchTimes() {
  yield put(getAvailableTimes());
  const action = yield take([COMMON_GET_AVAILABLE_TIMES_SUCCESS, COMMON_GET_AVAILABLE_TIMES_FAILURE]);

  if (action.type === COMMON_GET_AVAILABLE_TIMES_FAILURE) {
    throw new Error(COMMON_GET_AVAILABLE_TIMES_FAILURE);
  }
}

export function* trySelectDateTime(storeValue) {
  // todo check if the time is still available
  yield put(selectTime(storeValue));
}
