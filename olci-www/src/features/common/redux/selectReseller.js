import { put, select, takeLatest } from 'redux-saga/effects';
import {
  COMMON_SELECT_RESELLER,
  COMMON_SELECT_RESELLER_BEGIN,
  COMMON_RESELLER_NOT_FOUND,
} from './constants';

export function selectReseller(resellerCode) {
  return {
    type: COMMON_SELECT_RESELLER_BEGIN,
    resellerCode: resellerCode.toUpperCase(),
  };
}

export function* doSelectResellers({ resellerCode }) {
  const resellers = yield select(state => state.common.resellers);
  const reseller = resellers.find(r => r.code === resellerCode);

  if (!reseller) {
    yield put({
      type: COMMON_RESELLER_NOT_FOUND,
    });
  } else {
    yield put({
      type: COMMON_SELECT_RESELLER,
      resellerCode,
    });
  }
}

export function reducer(state, action) {
  switch (action.type) {
    case COMMON_SELECT_RESELLER:
      return {
        ...state,
        selectedReseller: action.resellerCode,
      };

    case COMMON_RESELLER_NOT_FOUND:
      return {
        ...state,
        resellerNotFound: true,
      };

    default:
      return state;
  }
}

export function* watchSelectReseller() {
  yield takeLatest(COMMON_SELECT_RESELLER_BEGIN, doSelectResellers);
}
