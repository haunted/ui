import { put, take, call, takeLatest } from 'redux-saga/effects';
import { setStep } from './actions';
import { STEP_RESELLER_SELECTION } from '../../../common/steps';
import { clearStore } from '../../../common/utils';
import initialState from './initialState';

import { COMMON_RESET_BEGIN, COMMON_RESET, COMMON_SET_STEP } from './constants';

export function reset() {
  return {
    type: COMMON_RESET_BEGIN,
  };
}

export function* doReset() {
  yield put(setStep(STEP_RESELLER_SELECTION));
  yield take(COMMON_SET_STEP);
  yield call(clearStore);

  yield put({
    type: COMMON_RESET,
  });
}

export function reducer(state, action) {
  const {
    systemErrorStatus,
    checkVoucherNumberError,
    voucherData,
    fetchResellersError,
    selectedReseller,
    resellerNotFound,
    getAvailableDatesError,
    date,
    dateTime,
    email,
    confirmBookingError,
    sendConfirmationError,
    bookingData,
    getBookingDataError,
    getAvailableTimesError,
  } = initialState;

  switch (action.type) {
    case COMMON_RESET:
      return {
        ...state,
        systemErrorStatus,
        checkVoucherNumberError,
        voucherData,
        fetchResellersError,
        selectedReseller,
        resellerNotFound,
        getAvailableDatesError,
        date,
        dateTime,
        email,
        confirmBookingError,
        sendConfirmationError,
        bookingData,
        getBookingDataError,
        getAvailableTimesError,
      };

    default:
      return state;
  }
}

export function* watchReset() {
  yield takeLatest(COMMON_RESET_BEGIN, doReset);
}
