import axios from 'axios';
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import { reset, clearFields } from 'redux-form';
import {
  COMMON_CONFIRM_BOOKING_BEGIN,
  COMMON_CONFIRM_BOOKING_SUCCESS,
  COMMON_CONFIRM_BOOKING_FAILURE,
  COMMON_CONFIRM_BOOKING_DISMISS_ERROR,
} from './constants';
import { setStep } from './actions';
import { STEP_SUMMARY } from '../../../common/steps';
import { updateStore } from '../../../common/utils';

export function confirmBooking(data) {
  return {
    type: COMMON_CONFIRM_BOOKING_BEGIN,
    ...data, // email, firstName, lastName
  };
}

export function dismissConfirmBookingError() {
  return {
    type: COMMON_CONFIRM_BOOKING_DISMISS_ERROR,
  };
}

export function* doConfirmBooking({ email, firstName, lastName }) {
  let response;

  const { originalData: data, xTicket, xPass } = yield select(
    state => state.common.voucherData,
  );

  const dateTime = yield select(state => state.common.dateTime);

  try {
    response = yield call(axios.post, '/bookings', {
      ref: data.ref,
      resellerCode: data.resellerCode,
      travelDate: dateTime,
      travelers: data.travelers,
      email,
      firstName,
      lastName,
      xTicket,
      xPass,
    });
  } catch (error) {
    if (error.response && error.response.status === 400 && error.response.data.message === 'Invalid email address') {
      yield call(message.error, 'Email is not valid');
      yield put(clearFields('confirmationForm', false, false, 'email', 'confirmEmail'));
    }

    return yield put({
      type: COMMON_CONFIRM_BOOKING_FAILURE,
      data: { error },
    });
  }

  yield call(updateStore, 'finished', true);

  yield put({
    type: COMMON_CONFIRM_BOOKING_SUCCESS,
    data: response.data.tickets,
    email,
  });

  if (data.status === 'STATUS_OLCI_REDEEMED') {
    yield call(
      message.success,
      'Your tickets will be delivered to your email address in the next few minutes',
      3,
    );
    yield put(reset('confirmationForm'));
  } else {
    yield put(setStep(STEP_SUMMARY));
  }
}

export function* watchConfirmBooking() {
  yield takeLatest(COMMON_CONFIRM_BOOKING_BEGIN, doConfirmBooking);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case COMMON_CONFIRM_BOOKING_BEGIN:
      return {
        ...state,
        confirmBookingPending: true,
        confirmBookingError: null,
      };

    case COMMON_CONFIRM_BOOKING_SUCCESS:
      return {
        ...state,
        confirmBookingPending: false,
        confirmBookingError: null,
        bookingData: action.data,
        email: action.email,
      };

    case COMMON_CONFIRM_BOOKING_FAILURE:
      return {
        ...state,
        confirmBookingPending: false,
        confirmBookingError: true,
      };

    case COMMON_CONFIRM_BOOKING_DISMISS_ERROR:
      return {
        ...state,
        confirmBookingError: null,
      };

    default:
      return state;
  }
}
