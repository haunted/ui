import {
  COMMON_SYSTEM_ERROR,
  COMMON_SYSTEM_ERROR_DISMISS_ERROR,
} from './constants';

export function dismissSystemError() {
  return {
    type: COMMON_SYSTEM_ERROR_DISMISS_ERROR,
  };
}

export function systemError(code, message = '') {
  return {
    type: COMMON_SYSTEM_ERROR,
    status: {
      code,
      message,
    }
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case COMMON_SYSTEM_ERROR:
      return {
        ...state,
        systemErrorStatus: action.status,
      };

    case COMMON_SYSTEM_ERROR_DISMISS_ERROR:
      return {
        ...state,
        systemErrorStatus: null,
      };

    default:
      return state;
  }
}
