module.exports = {
  attractionName: 'National Geographic Encounter',
  attractionLogo: 'logo.png',
  printLogo: 'logo_print.png',
  defaultTheme: false,
  style: 'styles/index.scss',
};
