// Summary:
//   This is the entry of the application, works together with index.html.

import React from 'react';
import { render } from 'react-dom';
import { LocaleProvider, message } from 'antd';
import axios from 'axios';
import enUS from 'antd/lib/locale-provider/en_US';
import configStore from './common/configStore';
import routeConfig from './common/routeConfig';
import Root from './Root';

const store = configStore();

const checkStatus = (error) => {
  const { status } = error.response;

  if (status >= 500) {
    message.error('Request failed');
  }

  return Promise.reject(error);
};

axios.defaults.baseURL = '/api';
axios.defaults.headers.common['Content-type'] = 'application/json';
axios.interceptors.response.use(r => r, checkStatus);

function renderApp(app) {
  render(
    <LocaleProvider locale={enUS}>
      {app}
    </LocaleProvider>,
    document.getElementById('root'),
  );
}

renderApp(<Root store={store} routeConfig={routeConfig} />);

// Hot Module Replacement API
/* istanbul ignore if  */
if (module.hot) {
  module.hot.accept('./common/routeConfig', () => {
    const nextRouteConfig = require('./common/routeConfig').default; // eslint-disable-line
    renderApp(<Root store={store} routeConfig={nextRouteConfig} />);
  });
}
