import { matchPath } from 'react-router-dom';
import store from 'store';
import history from './history';
import {
  STEP_RESELLER_SELECTION,
  STEP_VOUCHER_VERIFICATION,
  STEP_DATE_SELECTION,
  STEP_TIME_SELECTION,
  STEP_CONFIRMATION,
  STEP_SUMMARY,
  paths,
} from './steps';

export const getStepByPath = pathname => {
  const matchedPath = Object.keys(paths).find(p =>
    matchPath(pathname, {
      path: p,
      exact: true,
    }),
  );

  return paths[matchedPath];
};

export const getLocationMatch = () => {
  const { pathname } = history.location;

  const matchedPath = Object.keys(paths).find(p =>
    matchPath(pathname, {
      path: p,
      exact: true,
    }),
  );

  return matchPath(pathname, {
    path: matchedPath,
    exact: true,
  });
};

export const getStepIndex = step => {
  switch (step) {
    case STEP_RESELLER_SELECTION:
      return 0;
    case STEP_VOUCHER_VERIFICATION:
      return 1;
    case STEP_DATE_SELECTION:
      return 2;
    case STEP_TIME_SELECTION:
      return 3;
    case STEP_CONFIRMATION:
      return 4;
    case STEP_SUMMARY:
      return 5;
    default:
      return 0;
  }
};

export function updateStore(key, value) {
  const defaults = {
    voucherRef: null,
    date: null,
    dateTime: null,
    finished: false,
  };

  const data = store.get('olci') || defaults;

  store.set('olci', {
    ...data,
    [key]: value,
  });
}

export function getStoreValue(key) {
  const data = store.get('olci');

  return data && data[key];
}

export function clearStore() {
  store.remove('olci');
}
