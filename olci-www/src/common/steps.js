export const STEP_RESELLER_SELECTION = 'STEP_RESELLER_SELECTION';
export const STEP_VOUCHER_VERIFICATION = 'STEP_VOUCHER_VERIFICATION';
export const STEP_DATE_SELECTION = 'STEP_DATE_SELECTION';
export const STEP_TIME_SELECTION = 'STEP_TIME_SELECTION';
export const STEP_CONFIRMATION = 'STEP_CONFIRMATION';
export const STEP_SUMMARY = 'STEP_SUMMARY';

export const paths = {
  '/': STEP_RESELLER_SELECTION,
  '/:resellerCode': STEP_VOUCHER_VERIFICATION,
  '/:resellerCode/date': STEP_DATE_SELECTION,
  '/:resellerCode/time': STEP_TIME_SELECTION,
  '/:resellerCode/confirmation': STEP_CONFIRMATION,
  '/:resellerCode/summary': STEP_SUMMARY,
  '/:resellerCode/:voucherRef': STEP_VOUCHER_VERIFICATION,
};
