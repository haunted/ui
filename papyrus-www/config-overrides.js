const rewireLess = require('react-app-rewire-less');
const rewireAntd = require('react-app-rewire-antd');

/* config-overrides.js */
module.exports = function override(config, env) {
  config = rewireAntd()(config, env); // eslint-disable-line no-param-reassign

  config = rewireLess(config, env); // eslint-disable-line no-param-reassign

  return config;
};
