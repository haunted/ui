import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  RESELLER_FETCH_RESELLERS_BEGIN,
  RESELLER_FETCH_RESELLERS_SUCCESS,
  RESELLER_FETCH_RESELLERS_FAILURE,
  RESELLER_FETCH_RESELLERS_DISMISS_ERROR,
} from 'src/features/reseller/redux/constants';

import {
  fetchResellers,
  dismissFetchResellersError,
  doFetchResellers,
  reducer,
} from 'src/features/reseller/redux/fetchResellers';

describe('reseller/redux/fetchResellers', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by fetchResellers', () => {
    expect(fetchResellers()).to.have.property('type', RESELLER_FETCH_RESELLERS_BEGIN);
  });

  it('returns correct action by dismissFetchResellersError', () => {
    expect(dismissFetchResellersError()).to.have.property('type', RESELLER_FETCH_RESELLERS_DISMISS_ERROR);
  });

  // saga tests
  const generator = doFetchResellers();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches RESELLER_FETCH_RESELLERS_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: RESELLER_FETCH_RESELLERS_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches RESELLER_FETCH_RESELLERS_FAILURE action when failed', () => {
    const generatorForError = doFetchResellers();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: RESELLER_FETCH_RESELLERS_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type RESELLER_FETCH_RESELLERS_BEGIN correctly', () => {
    const prevState = { fetchResellersPending: false };
    const state = reducer(
      prevState,
      { type: RESELLER_FETCH_RESELLERS_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchResellersPending).to.be.true;
  });

  it('handles action type RESELLER_FETCH_RESELLERS_SUCCESS correctly', () => {
    const prevState = { fetchResellersPending: true };
    const state = reducer(
      prevState,
      { type: RESELLER_FETCH_RESELLERS_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchResellersPending).to.be.false;
  });

  it('handles action type RESELLER_FETCH_RESELLERS_FAILURE correctly', () => {
    const prevState = { fetchResellersPending: true };
    const state = reducer(
      prevState,
      { type: RESELLER_FETCH_RESELLERS_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchResellersPending).to.be.false;
    expect(state.fetchResellersError).to.exist;
  });

  it('handles action type RESELLER_FETCH_RESELLERS_DISMISS_ERROR correctly', () => {
    const prevState = { fetchResellersError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: RESELLER_FETCH_RESELLERS_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchResellersError).to.be.null;
  });
});
