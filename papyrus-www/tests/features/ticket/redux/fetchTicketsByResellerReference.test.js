import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_BEGIN,
  TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_SUCCESS,
  TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_FAILURE,
  TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_DISMISS_ERROR,
} from 'src/features/ticket/redux/constants';

import {
  fetchTicketsByResellerReference,
  dismissFetchTicketsByResellerReferenceError,
  doFetchTicketsByResellerReference,
  reducer,
} from 'src/features/ticket/redux/fetchTicketsByResellerReference';

describe('ticket/redux/fetchTicketsByResellerReference', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by fetchTicketsByReference', () => {
    expect(fetchTicketsByResellerReference()).to.have.property('type', TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_BEGIN);
  });

  it('returns correct action by dismissFetchTicketsByResellerReferenceError', () => {
    expect(dismissFetchTicketsByResellerReferenceError()).to.have.property('type', TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_DISMISS_ERROR);
  });

  // saga tests
  const generator = doFetchTicketsByResellerReference();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches TICKET_FETCH_TICKETS_BY_REFERENCE_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches TICKET_FETCH_TICKETS_BY_REFERENCE_FAILURE action when failed', () => {
    const generatorForError = doFetchTicketsByResellerReference();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_BEGIN correctly', () => {
    const prevState = { fetchTicketsByReferencePending: false };
    const state = reducer(
      prevState,
      { type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchTicketsByReferencePending).to.be.true;
  });

  it('handles action type TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_SUCCESS correctly', () => {
    const prevState = { fetchTicketsByReferencePending: true };
    const state = reducer(
      prevState,
      { type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchTicketsByReferencePending).to.be.false;
  });

  it('handles action type TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_FAILURE correctly', () => {
    const prevState = { fetchTicketsByReferencePending: true };
    const state = reducer(
      prevState,
      { type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchTicketsByReferencePending).to.be.false;
    expect(state.fetchTicketsByReferenceError).to.exist;
  });

  it('handles action type TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_DISMISS_ERROR correctly', () => {
    const prevState = { fetchTicketsByReferenceError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchTicketsByReferenceError).to.be.null;
  });
});
