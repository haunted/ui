import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { LoginPage } from 'src/features/user/LoginPage';

describe('user/LoginPage', () => {
  it('renders node with correct class name', () => {
    const props = {
      user: {},
      actions: {},
    };
    const renderedComponent = shallow(<LoginPage {...props} />);

    expect(renderedComponent.find('.user-login-page').node).to.exist;
  });
});
