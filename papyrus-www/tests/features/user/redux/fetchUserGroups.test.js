import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  USER_FETCH_USER_GROUPS_BEGIN,
  USER_FETCH_USER_GROUPS_SUCCESS,
  USER_FETCH_USER_GROUPS_FAILURE,
  USER_FETCH_USER_GROUPS_DISMISS_ERROR,
} from 'src/features/user/redux/constants';

import {
  fetchUserGroups,
  dismissFetchUserGroupsError,
  doFetchUserGroups,
  reducer,
} from 'src/features/user/redux/fetchUserGroups';

describe('user/redux/fetchUserGroups', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by fetchCurrentUserGroups', () => {
    expect(fetchUserGroups()).to.have.property('type', USER_FETCH_USER_GROUPS_BEGIN);
  });

  it('returns correct action by dismissFetchUserGroupsError', () => {
    expect(dismissFetchUserGroupsError()).to.have.property('type', USER_FETCH_USER_GROUPS_DISMISS_ERROR);
  });

  // saga tests
  const generator = doFetchUserGroups();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches USER_FETCH_CURRENT_USER_GROUPS_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: USER_FETCH_USER_GROUPS_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches USER_FETCH_CURRENT_USER_GROUPS_FAILURE action when failed', () => {
    const generatorForError = doFetchUserGroups();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: USER_FETCH_USER_GROUPS_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type USER_FETCH_USER_GROUPS_BEGIN correctly', () => {
    const prevState = { fetchCurrentUserGroupsPending: false };
    const state = reducer(
      prevState,
      { type: USER_FETCH_USER_GROUPS_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchCurrentUserGroupsPending).to.be.true;
  });

  it('handles action type USER_FETCH_USER_GROUPS_SUCCESS correctly', () => {
    const prevState = { fetchCurrentUserGroupsPending: true };
    const state = reducer(
      prevState,
      { type: USER_FETCH_USER_GROUPS_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchCurrentUserGroupsPending).to.be.false;
  });

  it('handles action type USER_FETCH_USER_GROUPS_FAILURE correctly', () => {
    const prevState = { fetchCurrentUserGroupsPending: true };
    const state = reducer(
      prevState,
      { type: USER_FETCH_USER_GROUPS_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchCurrentUserGroupsPending).to.be.false;
    expect(state.fetchCurrentUserGroupsError).to.exist;
  });

  it('handles action type USER_FETCH_USER_GROUPS_DISMISS_ERROR correctly', () => {
    const prevState = { fetchCurrentUserGroupsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: USER_FETCH_USER_GROUPS_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchCurrentUserGroupsError).to.be.null;
  });
});
