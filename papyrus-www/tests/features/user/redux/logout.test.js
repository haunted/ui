import { expect } from 'chai';

import { USER_LOGOUT } from 'src/features/user/redux/constants';

import {
  logout,
  reducer,
} from 'src/features/user/redux/logout';

describe('user/redux/logout', () => {
  it('returns correct action by logout', () => {
    expect(logout()).to.have.property('type', USER_LOGOUT);
  });

  it('handles action type USER_LOGOUT correctly', () => {
    const prevState = {};
    const state = reducer(
      prevState,
      { type: USER_LOGOUT },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state).to.deep.equal(prevState); // TODO: replace this line with real case.
  });
});
