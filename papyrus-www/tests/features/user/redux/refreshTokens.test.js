import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  USER_REFRESH_TOKENS_BEGIN,
  USER_REFRESH_TOKENS_SUCCESS,
  USER_REFRESH_TOKENS_FAILURE,
  USER_REFRESH_TOKENS_DISMISS_ERROR,
} from 'src/features/user/redux/constants';

import {
  refreshTokens,
  dismissRefreshTokensError,
  doRefreshTokens,
  reducer,
} from 'src/features/user/redux/refreshTokens';

describe('user/redux/refreshTokens', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by refreshTokens', () => {
    expect(refreshTokens()).to.have.property('type', USER_REFRESH_TOKENS_BEGIN);
  });

  it('returns correct action by dismissRefreshTokensError', () => {
    expect(dismissRefreshTokensError()).to.have.property('type', USER_REFRESH_TOKENS_DISMISS_ERROR);
  });

  // saga tests
  const generator = doRefreshTokens();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches USER_REFRESH_TOKENS_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: USER_REFRESH_TOKENS_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches USER_REFRESH_TOKENS_FAILURE action when failed', () => {
    const generatorForError = doRefreshTokens();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: USER_REFRESH_TOKENS_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type USER_REFRESH_TOKENS_BEGIN correctly', () => {
    const prevState = { refreshTokensPending: false };
    const state = reducer(
      prevState,
      { type: USER_REFRESH_TOKENS_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.refreshTokensPending).to.be.true;
  });

  it('handles action type USER_REFRESH_TOKENS_SUCCESS correctly', () => {
    const prevState = { refreshTokensPending: true };
    const state = reducer(
      prevState,
      { type: USER_REFRESH_TOKENS_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.refreshTokensPending).to.be.false;
  });

  it('handles action type USER_REFRESH_TOKENS_FAILURE correctly', () => {
    const prevState = { refreshTokensPending: true };
    const state = reducer(
      prevState,
      { type: USER_REFRESH_TOKENS_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.refreshTokensPending).to.be.false;
    expect(state.refreshTokensError).to.exist;
  });

  it('handles action type USER_REFRESH_TOKENS_DISMISS_ERROR correctly', () => {
    const prevState = { refreshTokensError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: USER_REFRESH_TOKENS_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.refreshTokensError).to.be.null;
  });
});
