import { expect } from 'chai';

import { USER_IS_LOGGED_IN } from 'src/features/user/redux/constants';

import {
  isLoggedIn,
  reducer,
} from 'src/features/user/redux/isLoggedIn';

describe('user/redux/isLoggedIn', () => {
  it('returns correct action by isLoggedIn', () => {
    expect(isLoggedIn()).to.have.property('type', USER_IS_LOGGED_IN);
  });

  it('handles action type USER_IS_LOGGED_IN correctly', () => {
    const prevState = {};
    const state = reducer(
      prevState,
      { type: USER_IS_LOGGED_IN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state).to.deep.equal(prevState); // TODO: replace this line with real case.
  });
});
