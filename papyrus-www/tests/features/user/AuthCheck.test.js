import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { AuthCheck } from 'src/features/user/AuthCheck';

describe('user/AuthCheck', () => {
  it('renders node with correct class name', () => {
    const props = {
      user: {},
      actions: {},
    };
    const renderedComponent = shallow(<AuthCheck {...props} />);

    expect(renderedComponent.find('.user-auth-check').node).to.exist;
  });
});
