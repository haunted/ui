import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { JobTranscription } from 'src/features/job/JobTranscription';

describe('job/JobTranscription', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<JobTranscription {...props} />);

    expect(renderedComponent.find('.job-job-transcription').node).to.exist;
  });
});
