import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { SubmittedJobs } from 'src/features/job/SubmittedJobs';

describe('job/SubmittedJobs', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<SubmittedJobs {...props} />);

    expect(renderedComponent.find('.job-submitted-jobs').node).to.exist;
  });
});
