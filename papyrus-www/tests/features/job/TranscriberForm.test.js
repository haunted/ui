import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { TranscriberForm } from 'src/features/job';

describe('job/TranscriberForm', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<TranscriberForm />);

    expect(renderedComponent.find('.job-transcriber-form').node).to.exist;
  });
});
