import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { JobInfo } from 'src/features/job';

describe('job/JobInfo', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<JobInfo />);

    expect(renderedComponent.find('.job-job-info').node).to.exist;
  });
});
