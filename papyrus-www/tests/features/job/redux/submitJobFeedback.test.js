import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  JOB_SUBMIT_JOB_FEEDBACK_BEGIN,
  JOB_SUBMIT_JOB_FEEDBACK_SUCCESS,
  JOB_SUBMIT_JOB_FEEDBACK_FAILURE,
  JOB_SUBMIT_JOB_FEEDBACK_DISMISS_ERROR,
} from 'src/features/job/redux/constants';

import {
  submitJobFeedback,
  dismissSubmitJobFeedbackError,
  doSubmitJobFeedback,
  reducer,
} from 'src/features/job/redux/submitJobFeedback';

describe('job/redux/submitJobFeedback', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by submitJobFeedback', () => {
    expect(submitJobFeedback()).to.have.property('type', JOB_SUBMIT_JOB_FEEDBACK_BEGIN);
  });

  it('returns correct action by dismissSubmitJobFeedbackError', () => {
    expect(dismissSubmitJobFeedbackError()).to.have.property('type', JOB_SUBMIT_JOB_FEEDBACK_DISMISS_ERROR);
  });

  // saga tests
  const generator = doSubmitJobFeedback();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches JOB_SUBMIT_JOB_FEEDBACK_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: JOB_SUBMIT_JOB_FEEDBACK_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches JOB_SUBMIT_JOB_FEEDBACK_FAILURE action when failed', () => {
    const generatorForError = doSubmitJobFeedback();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: JOB_SUBMIT_JOB_FEEDBACK_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type JOB_SUBMIT_JOB_FEEDBACK_BEGIN correctly', () => {
    const prevState = { submitJobFeedbackPending: false };
    const state = reducer(
      prevState,
      { type: JOB_SUBMIT_JOB_FEEDBACK_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.submitJobFeedbackPending).to.be.true;
  });

  it('handles action type JOB_SUBMIT_JOB_FEEDBACK_SUCCESS correctly', () => {
    const prevState = { submitJobFeedbackPending: true };
    const state = reducer(
      prevState,
      { type: JOB_SUBMIT_JOB_FEEDBACK_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.submitJobFeedbackPending).to.be.false;
  });

  it('handles action type JOB_SUBMIT_JOB_FEEDBACK_FAILURE correctly', () => {
    const prevState = { submitJobFeedbackPending: true };
    const state = reducer(
      prevState,
      { type: JOB_SUBMIT_JOB_FEEDBACK_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.submitJobFeedbackPending).to.be.false;
    expect(state.submitJobFeedbackError).to.exist;
  });

  it('handles action type JOB_SUBMIT_JOB_FEEDBACK_DISMISS_ERROR correctly', () => {
    const prevState = { submitJobFeedbackError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: JOB_SUBMIT_JOB_FEEDBACK_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.submitJobFeedbackError).to.be.null;
  });
});
