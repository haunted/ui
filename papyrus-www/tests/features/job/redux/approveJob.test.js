import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  JOB_APPROVE_JOB_BEGIN,
  JOB_APPROVE_JOB_SUCCESS,
  JOB_APPROVE_JOB_FAILURE,
  JOB_APPROVE_JOB_DISMISS_ERROR,
} from 'src/features/job/redux/constants';

import {
  approveJob,
  dismissApproveJobError,
  doApproveJob,
  reducer,
} from 'src/features/job/redux/approveJob';

describe('job/redux/approveJob', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by approveJob', () => {
    expect(approveJob()).to.have.property('type', JOB_APPROVE_JOB_BEGIN);
  });

  it('returns correct action by dismissApproveJobError', () => {
    expect(dismissApproveJobError()).to.have.property('type', JOB_APPROVE_JOB_DISMISS_ERROR);
  });

  // saga tests
  const generator = doApproveJob();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches JOB_APPROVE_JOB_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: JOB_APPROVE_JOB_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches JOB_APPROVE_JOB_FAILURE action when failed', () => {
    const generatorForError = doApproveJob();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: JOB_APPROVE_JOB_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type JOB_APPROVE_JOB_BEGIN correctly', () => {
    const prevState = { approveJobPending: false };
    const state = reducer(
      prevState,
      { type: JOB_APPROVE_JOB_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.approveJobPending).to.be.true;
  });

  it('handles action type JOB_APPROVE_JOB_SUCCESS correctly', () => {
    const prevState = { approveJobPending: true };
    const state = reducer(
      prevState,
      { type: JOB_APPROVE_JOB_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.approveJobPending).to.be.false;
  });

  it('handles action type JOB_APPROVE_JOB_FAILURE correctly', () => {
    const prevState = { approveJobPending: true };
    const state = reducer(
      prevState,
      { type: JOB_APPROVE_JOB_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.approveJobPending).to.be.false;
    expect(state.approveJobError).to.exist;
  });

  it('handles action type JOB_APPROVE_JOB_DISMISS_ERROR correctly', () => {
    const prevState = { approveJobError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: JOB_APPROVE_JOB_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.approveJobError).to.be.null;
  });
});
