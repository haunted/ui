import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  JOB_FETCH_STATS_BEGIN,
  JOB_FETCH_STATS_SUCCESS,
  JOB_FETCH_STATS_FAILURE,
  JOB_FETCH_STATS_DISMISS_ERROR,
} from 'src/features/job/redux/constants';

import {
  fetchStats,
  dismissFetchStatsError,
  doFetchStats,
  reducer,
} from 'src/features/job/redux/fetchStats';

describe('job/redux/fetchStats', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by fetchStats', () => {
    expect(fetchStats()).to.have.property('type', JOB_FETCH_STATS_BEGIN);
  });

  it('returns correct action by dismissFetchStatsError', () => {
    expect(dismissFetchStatsError()).to.have.property('type', JOB_FETCH_STATS_DISMISS_ERROR);
  });

  // saga tests
  const generator = doFetchStats();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches JOB_FETCH_STATS_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: JOB_FETCH_STATS_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches JOB_FETCH_STATS_FAILURE action when failed', () => {
    const generatorForError = doFetchStats();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: JOB_FETCH_STATS_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type JOB_FETCH_STATS_BEGIN correctly', () => {
    const prevState = { fetchStatsPending: false };
    const state = reducer(
      prevState,
      { type: JOB_FETCH_STATS_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchStatsPending).to.be.true;
  });

  it('handles action type JOB_FETCH_STATS_SUCCESS correctly', () => {
    const prevState = { fetchStatsPending: true };
    const state = reducer(
      prevState,
      { type: JOB_FETCH_STATS_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchStatsPending).to.be.false;
  });

  it('handles action type JOB_FETCH_STATS_FAILURE correctly', () => {
    const prevState = { fetchStatsPending: true };
    const state = reducer(
      prevState,
      { type: JOB_FETCH_STATS_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchStatsPending).to.be.false;
    expect(state.fetchStatsError).to.exist;
  });

  it('handles action type JOB_FETCH_STATS_DISMISS_ERROR correctly', () => {
    const prevState = { fetchStatsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: JOB_FETCH_STATS_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchStatsError).to.be.null;
  });
});
