import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  JOB_SET_FEEDBACK_AS_READ_BEGIN,
  JOB_SET_FEEDBACK_AS_READ_SUCCESS,
  JOB_SET_FEEDBACK_AS_READ_FAILURE,
  JOB_SET_FEEDBACK_AS_READ_DISMISS_ERROR,
} from 'src/features/job/redux/constants';

import {
  setFeedbackAsRead,
  dismissSetFeedbackAsReadError,
  doSetFeedbackAsRead,
  reducer,
} from 'src/features/job/redux/setFeedbackAsRead';

describe('job/redux/setFeedbackAsRead', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by setFeedbackAsRead', () => {
    expect(setFeedbackAsRead()).to.have.property('type', JOB_SET_FEEDBACK_AS_READ_BEGIN);
  });

  it('returns correct action by dismissSetFeedbackAsReadError', () => {
    expect(dismissSetFeedbackAsReadError()).to.have.property('type', JOB_SET_FEEDBACK_AS_READ_DISMISS_ERROR);
  });

  // saga tests
  const generator = doSetFeedbackAsRead();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches JOB_SET_FEEDBACK_AS_READ_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: JOB_SET_FEEDBACK_AS_READ_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches JOB_SET_FEEDBACK_AS_READ_FAILURE action when failed', () => {
    const generatorForError = doSetFeedbackAsRead();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: JOB_SET_FEEDBACK_AS_READ_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type JOB_SET_FEEDBACK_AS_READ_BEGIN correctly', () => {
    const prevState = { setFeedbackAsReadPending: false };
    const state = reducer(
      prevState,
      { type: JOB_SET_FEEDBACK_AS_READ_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.setFeedbackAsReadPending).to.be.true;
  });

  it('handles action type JOB_SET_FEEDBACK_AS_READ_SUCCESS correctly', () => {
    const prevState = { setFeedbackAsReadPending: true };
    const state = reducer(
      prevState,
      { type: JOB_SET_FEEDBACK_AS_READ_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.setFeedbackAsReadPending).to.be.false;
  });

  it('handles action type JOB_SET_FEEDBACK_AS_READ_FAILURE correctly', () => {
    const prevState = { setFeedbackAsReadPending: true };
    const state = reducer(
      prevState,
      { type: JOB_SET_FEEDBACK_AS_READ_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.setFeedbackAsReadPending).to.be.false;
    expect(state.setFeedbackAsReadError).to.exist;
  });

  it('handles action type JOB_SET_FEEDBACK_AS_READ_DISMISS_ERROR correctly', () => {
    const prevState = { setFeedbackAsReadError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: JOB_SET_FEEDBACK_AS_READ_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.setFeedbackAsReadError).to.be.null;
  });
});
