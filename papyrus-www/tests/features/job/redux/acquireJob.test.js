import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  JOB_ACQUIRE_JOB_BEGIN,
  JOB_ACQUIRE_JOB_SUCCESS,
  JOB_ACQUIRE_JOB_FAILURE,
  JOB_ACQUIRE_JOB_DISMISS_ERROR,
} from 'src/features/job/redux/constants';

import {
  acquireJob,
  dismissAcquireJobError,
  doAcquireJob,
  reducer,
} from 'src/features/job/redux/acquireJob';

describe('job/redux/acquireJob', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by acquireJob', () => {
    expect(acquireJob()).to.have.property('type', JOB_ACQUIRE_JOB_BEGIN);
  });

  it('returns correct action by dismissAcquireJobError', () => {
    expect(dismissAcquireJobError()).to.have.property('type', JOB_ACQUIRE_JOB_DISMISS_ERROR);
  });

  // saga tests
  const generator = doAcquireJob();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches JOB_ACQUIRE_JOB_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: JOB_ACQUIRE_JOB_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches JOB_ACQUIRE_JOB_FAILURE action when failed', () => {
    const generatorForError = doAcquireJob();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: JOB_ACQUIRE_JOB_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type JOB_ACQUIRE_JOB_BEGIN correctly', () => {
    const prevState = { acquireJobPending: false };
    const state = reducer(
      prevState,
      { type: JOB_ACQUIRE_JOB_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.acquireJobPending).to.be.true;
  });

  it('handles action type JOB_ACQUIRE_JOB_SUCCESS correctly', () => {
    const prevState = { acquireJobPending: true };
    const state = reducer(
      prevState,
      { type: JOB_ACQUIRE_JOB_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.acquireJobPending).to.be.false;
  });

  it('handles action type JOB_ACQUIRE_JOB_FAILURE correctly', () => {
    const prevState = { acquireJobPending: true };
    const state = reducer(
      prevState,
      { type: JOB_ACQUIRE_JOB_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.acquireJobPending).to.be.false;
    expect(state.acquireJobError).to.exist;
  });

  it('handles action type JOB_ACQUIRE_JOB_DISMISS_ERROR correctly', () => {
    const prevState = { acquireJobError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: JOB_ACQUIRE_JOB_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.acquireJobError).to.be.null;
  });
});
