import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  JOB_FETCH_JOBS_BEGIN,
  JOB_FETCH_JOBS_SUCCESS,
  JOB_FETCH_JOBS_FAILURE,
  JOB_FETCH_JOBS_DISMISS_ERROR,
} from 'src/features/job/redux/constants';

import {
  fetchJobs,
  dismissFetchJobsError,
  doFetchJobs,
  reducer,
} from 'src/features/job/redux/fetchJobs';

describe('job/redux/fetchJobs', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by fetchJobs', () => {
    expect(fetchJobs()).to.have.property('type', JOB_FETCH_JOBS_BEGIN);
  });

  it('returns correct action by dismissFetchJobsError', () => {
    expect(dismissFetchJobsError()).to.have.property('type', JOB_FETCH_JOBS_DISMISS_ERROR);
  });

  // saga tests
  const generator = doFetchJobs();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches JOB_FETCH_JOBS_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: JOB_FETCH_JOBS_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches JOB_FETCH_JOBS_FAILURE action when failed', () => {
    const generatorForError = doFetchJobs();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: JOB_FETCH_JOBS_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type JOB_FETCH_JOBS_BEGIN correctly', () => {
    const prevState = { fetchJobsPending: false };
    const state = reducer(
      prevState,
      { type: JOB_FETCH_JOBS_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchJobsPending).to.be.true;
  });

  it('handles action type JOB_FETCH_JOBS_SUCCESS correctly', () => {
    const prevState = { fetchJobsPending: true };
    const state = reducer(
      prevState,
      { type: JOB_FETCH_JOBS_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchJobsPending).to.be.false;
  });

  it('handles action type JOB_FETCH_JOBS_FAILURE correctly', () => {
    const prevState = { fetchJobsPending: true };
    const state = reducer(
      prevState,
      { type: JOB_FETCH_JOBS_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchJobsPending).to.be.false;
    expect(state.fetchJobsError).to.exist;
  });

  it('handles action type JOB_FETCH_JOBS_DISMISS_ERROR correctly', () => {
    const prevState = { fetchJobsError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: JOB_FETCH_JOBS_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchJobsError).to.be.null;
  });
});
