import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { JobsTable } from 'src/features/job';

describe('job/JobsTable', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<JobsTable />);

    expect(renderedComponent.find('.job-jobs-table').getElement()).to.exist;
  });
});
