import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { AcquiredJobs } from 'src/features/job/AcquiredJobs';

describe('job/AcquiredJobs', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<AcquiredJobs {...props} />);

    expect(renderedComponent.find('.job-acquired-jobs').node).to.exist;
  });
});
