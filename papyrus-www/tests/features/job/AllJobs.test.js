import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { AllJobs } from 'src/features/job/AllJobs';

describe('job/AllJobs', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<AllJobs {...props} />);

    expect(renderedComponent.find('.job-all-jobs').getElement()).to.exist;
  });
});
