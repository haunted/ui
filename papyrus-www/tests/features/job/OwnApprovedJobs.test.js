import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { OwnApprovedJobs } from 'src/features/job/OwnApprovedJobs';

describe('job/OwnApprovedJobs', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<OwnApprovedJobs {...props} />);

    expect(renderedComponent.find('.job-own-approved-jobs').getElement()).to.exist;
  });
});
