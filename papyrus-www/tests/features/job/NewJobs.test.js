import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { NewJobs } from 'src/features/job/NewJobs';

describe('job/NewJobs', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<NewJobs {...props} />);

    expect(renderedComponent.find('.job-new-jobs').node).to.exist;
  });
});
