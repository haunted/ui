import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { FeedbackList } from 'src/features/job';

describe('job/FeedbackList', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<FeedbackList />);

    expect(renderedComponent.find('.job-feedback-list').node).to.exist;
  });
});
