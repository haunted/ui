import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { JobReview } from 'src/features/job/JobReview';

describe('job/JobReview', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<JobReview {...props} />);

    expect(renderedComponent.find('.job-job-review').node).to.exist;
  });
});
