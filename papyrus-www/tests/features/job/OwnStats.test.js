import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { OwnStats } from 'src/features/job/OwnStats';

describe('job/OwnStats', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<OwnStats {...props} />);

    expect(renderedComponent.find('.job-own-stats').getElement()).to.exist;
  });
});
