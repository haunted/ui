import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { ApprovedJobs } from 'src/features/job/ApprovedJobs';

describe('job/ApprovedJobs', () => {
  it('renders node with correct class name', () => {
    const props = {
      job: {},
      actions: {},
    };
    const renderedComponent = shallow(<ApprovedJobs {...props} />);

    expect(renderedComponent.find('.job-approved-jobs').node).to.exist;
  });
});
