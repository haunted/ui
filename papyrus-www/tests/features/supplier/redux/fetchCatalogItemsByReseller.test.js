import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN,
  SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS,
  SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_FAILURE,
  SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_DISMISS_ERROR,
} from 'src/features/supplier/redux/constants';

import {
  fetchCatalogItemsByReseller,
  dismissFetchCatalogItemsByResellerError,
  doFetchCatalogItemsByReseller,
  reducer,
} from 'src/features/supplier/redux/fetchCatalogItemsByReseller';

describe('supplier/redux/fetchCatalogItemsByReseller', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by fetchCatalogItemsByReseller', () => {
    expect(fetchCatalogItemsByReseller()).to.have.property('type', SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN);
  });

  it('returns correct action by dismissFetchCatalogItemsByResellerError', () => {
    expect(dismissFetchCatalogItemsByResellerError()).to.have.property('type', SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_DISMISS_ERROR);
  });

  // saga tests
  const generator = doFetchCatalogItemsByReseller();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_FAILURE action when failed', () => {
    const generatorForError = doFetchCatalogItemsByReseller();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN correctly', () => {
    const prevState = { fetchCatalogItemsByResellerPending: false };
    const state = reducer(
      prevState,
      { type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchCatalogItemsByResellerPending).to.be.true;
  });

  it('handles action type SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS correctly', () => {
    const prevState = { fetchCatalogItemsByResellerPending: true };
    const state = reducer(
      prevState,
      { type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchCatalogItemsByResellerPending).to.be.false;
  });

  it('handles action type SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_FAILURE correctly', () => {
    const prevState = { fetchCatalogItemsByResellerPending: true };
    const state = reducer(
      prevState,
      { type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchCatalogItemsByResellerPending).to.be.false;
    expect(state.fetchCatalogItemsByResellerError).to.exist;
  });

  it('handles action type SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_DISMISS_ERROR correctly', () => {
    const prevState = { fetchCatalogItemsByResellerError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchCatalogItemsByResellerError).to.be.null;
  });
});
