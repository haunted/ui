import { delay } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import nock from 'nock';
import { expect } from 'chai';

import {
  SUPPLIER_FETCH_SUPPLIERS_BEGIN,
  SUPPLIER_FETCH_SUPPLIERS_SUCCESS,
  SUPPLIER_FETCH_SUPPLIERS_FAILURE,
  SUPPLIER_FETCH_SUPPLIERS_DISMISS_ERROR,
} from 'src/features/supplier/redux/constants';

import {
  fetchSuppliers,
  dismissFetchSuppliersError,
  doFetchSuppliers,
  reducer,
} from 'src/features/supplier/redux/fetchSuppliers';

describe('supplier/redux/fetchSuppliers', () => {
  afterEach(() => {
    nock.cleanAll();
  });

  // redux action tests
  it('correct action by fetchSuppliers', () => {
    expect(fetchSuppliers()).to.have.property('type', SUPPLIER_FETCH_SUPPLIERS_BEGIN);
  });

  it('returns correct action by dismissFetchSuppliersError', () => {
    expect(dismissFetchSuppliersError()).to.have.property('type', SUPPLIER_FETCH_SUPPLIERS_DISMISS_ERROR);
  });

  // saga tests
  const generator = doFetchSuppliers();

  it('calls delay when receives a begin action', () => {
    // Delay is just a sample, this should be replaced by real sync request.
    expect(generator.next().value).to.deep.equal(call(delay, 20));
  });

  it('dispatches SUPPLIER_FETCH_SUPPLIERS_SUCCESS action when succeeded', () => {
    expect(generator.next('something').value).to.deep.equal(put({
      type: SUPPLIER_FETCH_SUPPLIERS_SUCCESS,
      data: 'something',
    }));
  });

  it('dispatches SUPPLIER_FETCH_SUPPLIERS_FAILURE action when failed', () => {
    const generatorForError = doFetchSuppliers();

    generatorForError.next(); // call delay(20)

    const err = new Error('errored');

    expect(generatorForError.throw(err).value).to.deep.equal(put({
      type: SUPPLIER_FETCH_SUPPLIERS_FAILURE,
      data: { error: err },
    }));
  });

  it('returns done when finished', () => {
    expect(generator.next()).to.deep.equal({ done: true, value: undefined });
  });

  // reducer tests
  it('handles action type SUPPLIER_FETCH_SUPPLIERS_BEGIN correctly', () => {
    const prevState = { fetchSuppliersPending: false };
    const state = reducer(
      prevState,
      { type: SUPPLIER_FETCH_SUPPLIERS_BEGIN },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchSuppliersPending).to.be.true;
  });

  it('handles action type SUPPLIER_FETCH_SUPPLIERS_SUCCESS correctly', () => {
    const prevState = { fetchSuppliersPending: true };
    const state = reducer(
      prevState,
      { type: SUPPLIER_FETCH_SUPPLIERS_SUCCESS, data: {} },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchSuppliersPending).to.be.false;
  });

  it('handles action type SUPPLIER_FETCH_SUPPLIERS_FAILURE correctly', () => {
    const prevState = { fetchSuppliersPending: true };
    const state = reducer(
      prevState,
      { type: SUPPLIER_FETCH_SUPPLIERS_FAILURE, data: { error: new Error('some error') } },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchSuppliersPending).to.be.false;
    expect(state.fetchSuppliersError).to.exist;
  });

  it('handles action type SUPPLIER_FETCH_SUPPLIERS_DISMISS_ERROR correctly', () => {
    const prevState = { fetchSuppliersError: new Error('some error') };
    const state = reducer(
      prevState,
      { type: SUPPLIER_FETCH_SUPPLIERS_DISMISS_ERROR },
    );

    expect(state).to.not.equal(prevState); // should be immutable
    expect(state.fetchSuppliersError).to.be.null;
  });
});
