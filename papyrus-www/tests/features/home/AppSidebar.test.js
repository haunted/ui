import React from 'react';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import { AppSidebar } from 'src/features/home';

describe('home/AppSidebar', () => {
  it('renders node with correct class name', () => {
    const renderedComponent = shallow(<AppSidebar />);

    expect(renderedComponent.find('.home-app-sidebar').node).to.exist;
  });
});
