**Summary**

**Dependencies**
* [ ] 
* [ ] 
* [ ] 

**Tasks**
* [ ] (Example) Add new service
* [ ] (Example) Add new Gateway
* [ ] (Example) Add new Gateway endpoint
* Add tests:
 * [ ] Test case: 
 * [ ] Test case: 
 * [ ] Test case: 
* [ ] 
* [ ] Update *How to test* instructions
* [ ] Update Documentation in Redeam Manual

**Acceptance criteria**
* [ ] 
* [ ] 
* [ ] Logging, Montoring, Alerting
* [ ] What do we need to debug this feature? / What would we need to diagnose a problem with this feature?

**How to test**


/label ~Enhancement
