import * as userSagas from '../features/user/redux/sagas';
import * as jobSagas from '../features/job/redux/sagas';
import * as supplierSagas from '../features/supplier/redux/sagas';
import * as resellerSagas from '../features/reseller/redux/sagas';
import * as ticketSagas from '../features/ticket/redux/sagas';
import * as voucherSagas from '../features/voucher/redux/sagas';
// This file is auto maintained by rekit-plugin-redux-saga,
// you usually don't need to manually edit it.

// NOTE: DO NOT chanage featureSagas declearation pattern, it's used by rekit-plugin-redux-saga.
const featureSagas = [
  userSagas,
  jobSagas,
  supplierSagas,
  resellerSagas,
  ticketSagas,
  voucherSagas,
];

const sagas = featureSagas
  .reduce((prev, curr) => [...prev, ...Object.keys(curr).map(k => curr[k])], [])
  // a saga should be function, below filter avoids error if redux/sagas.js is empty;
  .filter(s => typeof s === 'function');

function* rootSaga() {
  yield sagas.map(saga => saga());
}

export default rootSaga;
