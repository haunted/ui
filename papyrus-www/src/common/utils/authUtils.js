export const isTranscriberSupervisor = groups =>
  groups.some(group => group.name === 'REDEAM_TRANSCRIBER_SUPERVISOR');

export const isTranscriberPlus = groups =>
  groups.some(group => group.name === 'REDEAM_TRANSCRIBER_PLUS');

