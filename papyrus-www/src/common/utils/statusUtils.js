import { statuses } from '../constants';

export const isSubmitted = status => status === statuses.STATUS_SUBMITTED;

export const isInReview = status => status === statuses.STATUS_IN_REVIEW;

export const isLocked = status => status === statuses.STATUS_LOCKED;

export const isRejected = status => status === statuses.STATUS_REJECTED;

export const isHasFeedback = status => status === statuses.STATUS_HAS_FEEDBACK;

export const isApproved = (status) => {
  if (!status) {
    return true;
  }

  return status === statuses.STATUS_APPROVED;
};

