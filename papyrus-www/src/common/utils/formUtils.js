export const normalizeNumber = (value) => {
  const parsed = parseInt(value, 10);

  return !Number.isNaN(parsed) ? Math.abs(parsed) : 0;
};
