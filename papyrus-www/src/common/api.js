import axios from 'axios';
import history from './history';

/**
 * Reading API URL from environment variables
 */
const API_URL = process.env.REACT_APP_API_URL;

const api = axios.create({
  baseURL: API_URL,
  timeout: 10000,
});

api.setTokens = (tokens) => {
  const now = new Date();
  const preparedTokens = tokens;

  preparedTokens.accessTokenExpiresAt =
    new Date(now.getTime() + (Math.floor(tokens.accessTokenExpiresIn) * 1000));
  preparedTokens.refreshTokenExpiresAt =
    new Date(now.getTime() + (Math.floor(tokens.refreshTokenExpiresIn) * 1000));

  localStorage.setItem('tokens', JSON.stringify(preparedTokens));
};

api.isAccessTokenValid = () => {
  const now = new Date();
  const tokens = JSON.parse(localStorage.getItem('tokens')) || null;

  if (!tokens) {
    return false;
  }

  tokens.accessTokenExpiresAt = Date.parse(tokens.accessTokenExpiresAt);

  if (tokens.accessTokenExpiresAt < now) {
    return false;
  }

  return true;
};

api.isRefreshTokenValid = () => {
  const now = new Date();
  const tokens = JSON.parse(localStorage.getItem('tokens')) || null;

  if (!tokens) {
    return false;
  }

  tokens.refreshTokenExpiresAt = Date.parse(tokens.refreshTokenExpiresAt);

  if (tokens.refreshTokenExpiresAt < now) {
    return false;
  }

  return true;
};

const cancel = axios.CancelToken.source();

export function apiAuthReqInterceptor(config) {
  const preparedConfig = config;
  let tokens = JSON.parse(localStorage.getItem('tokens')) || null;

  if (tokens) {
    // Access token expired
    if (!api.isAccessTokenValid()) {
      // If refresh token has not expired, try to refresh the tokens
      if (api.isRefreshTokenValid()) {
        axios
          .post(
            `${API_URL}/refresh`,
            {},
            {
              headers: {
                Authorization: `${tokens.refreshTokenType} ${
                  tokens.refreshToken
                }`,
              },
            },
          )
          .then((resp) => {
            api.setTokens(resp.data);
            tokens = resp.data;
          });
      } else {
        cancel.cancel('Access and Refresh tokens expired');
      }
    }

    preparedConfig.headers.Authorization = `${tokens.accessTokenType} ${
      tokens.accessToken
    }`;
  }

  return preparedConfig;
}

export function apiAuthReqInterceptorFail(err) {
  console.log(err);

  return Promise.reject(err);
}

export function apiAuthRespInterceptor(resp) {
  return resp;
}

export function apiAuthRespInterceptorFail(err) {
  if (err.response.status === 401 || err.response.status === 403) {
    history.push('/login');
  }
  console.log(err);

  return Promise.reject(err);
}

export default api;
