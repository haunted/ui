import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as form } from 'redux-form';
import commonReducer from '../features/common/redux/reducer';
import userReducer from '../features/user/redux/reducer';
import jobReducer from '../features/job/redux/reducer';
import supplierReducer from '../features/supplier/redux/reducer';
import resellerReducer from '../features/reseller/redux/reducer';
import ticketReducer from '../features/ticket/redux/reducer';
import voucherReducer from '../features/voucher/redux/reducer';

// NOTE 1: DO NOT CHANGE the 'reducerMap' name and the declaration pattern.
// This is used for Rekit cmds to register new features, remove features, etc.
// NOTE 2: always use the camel case of the feature folder name as the store branch name
// So that it's easy for others to understand it and Rekit could manage theme.

const reducerMap = {
  router: routerReducer,
  common: commonReducer,
  user: userReducer,
  job: jobReducer,
  supplier: supplierReducer,
  reseller: resellerReducer,
  ticket: ticketReducer,
  voucher: voucherReducer,
  form,
};

export default combineReducers(reducerMap);
