import { App } from '../features/home';
import { PageNotFound } from '../features/common';
import { LoginPage } from '../features/user';
import { NewJobs } from '../features/job';
import commonRoute from '../features/common/route';
import userRoute from '../features/user/route';
import jobRoute from '../features/job/route';
import supplierRoute from '../features/supplier/route';
import resellerRoute from '../features/reseller/route';
import ticketRoute from '../features/ticket/route';

// NOTE: DO NOT CHANGE the 'childRoutes' name and the declaration pattern.
// This is used for Rekit cmds to register
// routes config for new features, and remove config when remove features, etc.
const childRoutes = [
  commonRoute,
  userRoute,
  jobRoute,
  supplierRoute,
  resellerRoute,
  ticketRoute,
];

const routes = [
  {
    path: '/login',
    component: LoginPage,
  },
  {
    path: '/',
    component: App,
    childRoutes: [
      ...childRoutes,
      { path: '/', name: 'New jobs', component: NewJobs },
      { path: '*', name: 'Page not found', component: PageNotFound },
    ].filter(r => r.component || (r.childRoutes && r.childRoutes.length > 0)),
  },
];

// Handle isIndex property of route config:
//  Dupicate it and put it as the first route rule.
function handleIndexRoute(route) {
  if (!route.childRoutes || !route.childRoutes.length) {
    return;
  }

  const indexRoute = route.childRoutes.find(child => child.isIndex);

  if (indexRoute) {
    const first = { ...indexRoute };

    first.path = route.path;
    first.exact = true;
    first.autoIndexRoute = true; // mark it so that the simple nav won't show it.
    route.childRoutes.unshift(first);
  }
  route.childRoutes.forEach(handleIndexRoute);
}

routes.forEach(handleIndexRoute);
export default routes;
