import React from 'react';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';

function Select(props) {
  const {
    value,
    onChange,
    placeholder,
    disabled,
    meta: {
      touched,
      error,
    },
    simpleValue,
    options,
    ...custom
  } = props;

  return (
    <div>
      <ReactSelect
        value={value}
        placeholder={placeholder}
        options={options}
        simpleValue={simpleValue}
        onChange={onChange}
        clearable={false}
        disabled={disabled}
        {...custom}
      />
      {touched && error && <span className="form-error has-error">{error}</span>}
    </div>
  );
}

Select.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  meta: PropTypes.object,
  options: PropTypes.array.isRequired,
  disabled: PropTypes.bool,
  simpleValue: PropTypes.bool,
};

Select.defaultProps = {
  disabled: false,
  simpleValue: false,
  meta: {},
};

export default Select;
