import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

const FormItem = Form.Item;

function TextField(props) {
  const {
    input,
    label,
    meta: {
      touched,
      error,
    },
    ...custom
  } = props;

  return (
    <FormItem
      validateStatus={touched && error ? 'error' : null}
      className={touched && error ? 'error' : null}
    >
      <Input
        placeholder={label}
        {...input}
        {...custom}
      />

      {touched && error && (
        <p className="form-error has-error ant-form-explain">{error}</p>
      )}
    </FormItem>
  );
}

TextField.propTypes = {
  input: PropTypes.shape({}).isRequired,
  label: PropTypes.string,
  meta: PropTypes.shape({}).isRequired,
};

TextField.defaultProps = {
  label: '',
};

export default TextField;
