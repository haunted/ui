import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';

const { TextArea: AntTextArea } = Input;

const FormItem = Form.Item;

function TextArea(props) {
  const {
    input,
    label,
    meta: {
      touched,
      error,
    },
    ...custom
  } = props;

  return (
    <FormItem
      validateStatus={touched && error ? 'error' : null}
      className={touched && error ? 'error' : null}
    >
      <AntTextArea
        placeholder={label}
        {...input}
        {...custom}
        autosize={{ minRows: 2, maxRows: 6 }}
      />

      {touched && error && (
        <p className="form-error has-error ant-form-explain">{error}</p>
      )}
    </FormItem>
  );
}

TextArea.propTypes = {
  input: PropTypes.shape({}).isRequired,
  label: PropTypes.string,
  meta: PropTypes.shape({}).isRequired,
};

TextArea.defaultProps = {
  label: '',
};

export default TextArea;
