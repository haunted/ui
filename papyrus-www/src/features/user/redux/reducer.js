import initialState from './initialState';
import { reducer as loginReducer } from './login';
import { reducer as fetchCurrentUserReducer } from './fetchCurrentUser';
import { reducer as logoutReducer } from './logout';
import { reducer as refreshTokensReducer } from './refreshTokens';
import { reducer as fetchUserGroupsReducer } from './fetchUserGroups';
import { reducer as isLoggedInReducer } from './isLoggedIn';

const reducers = [
  loginReducer,
  fetchCurrentUserReducer,
  logoutReducer,
  refreshTokensReducer,
  fetchUserGroupsReducer,
  isLoggedInReducer,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    // Handle cross-topic actions here
    default:
      newState = state;
      break;
  }

  return reducers.reduce((s, r) => r(s, action), newState);
}
