import { delay } from 'redux-saga';
import { call, put, takeLatest } from 'redux-saga/effects';
import {
  USER_REFRESH_TOKENS_BEGIN,
  USER_REFRESH_TOKENS_SUCCESS,
  USER_REFRESH_TOKENS_FAILURE,
  USER_REFRESH_TOKENS_DISMISS_ERROR,
} from './constants';

export function refreshTokens() {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: USER_REFRESH_TOKENS_BEGIN,
  };
}

export function dismissRefreshTokensError() {
  return {
    type: USER_REFRESH_TOKENS_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on USER_REFRESH_TOKENS_BEGIN actions
export function* doRefreshTokens() {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    // Do Ajax call or other async request here. delay(20) is just a placeholder.
    res = yield call(delay, 20);
  } catch (err) {
    yield put({
      type: USER_REFRESH_TOKENS_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: USER_REFRESH_TOKENS_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchRefreshTokens() {
  yield takeLatest(USER_REFRESH_TOKENS_BEGIN, doRefreshTokens);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case USER_REFRESH_TOKENS_BEGIN:
      return {
        ...state,
        refreshTokensPending: true,
        refreshTokensError: null,
      };

    case USER_REFRESH_TOKENS_SUCCESS:
      return {
        ...state,
        refreshTokensPending: false,
        refreshTokensError: null,
      };

    case USER_REFRESH_TOKENS_FAILURE:
      return {
        ...state,
        refreshTokensPending: false,
        refreshTokensError: action.data.error,
      };

    case USER_REFRESH_TOKENS_DISMISS_ERROR:
      return {
        ...state,
        refreshTokensError: null,
      };

    default:
      return state;
  }
}
