import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  USER_FETCH_USER_GROUPS_BEGIN,
  USER_FETCH_USER_GROUPS_SUCCESS,
  USER_FETCH_USER_GROUPS_FAILURE,
  USER_FETCH_USER_GROUPS_DISMISS_ERROR,
} from './constants';

export function fetchUserGroups(userID) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: USER_FETCH_USER_GROUPS_BEGIN,
    userID,
  };
}

export function dismissFetchUserGroupsError() {
  return {
    type: USER_FETCH_USER_GROUPS_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on USER_FETCH_CURRENT_USER_GROUPS_BEGIN actions
export function* doFetchUserGroups(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.get, `/accounts/${args.userID}/groups`);
  } catch (err) {
    yield put({
      type: USER_FETCH_USER_GROUPS_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: USER_FETCH_USER_GROUPS_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchUserGroups() {
  yield takeLatest(USER_FETCH_USER_GROUPS_BEGIN, doFetchUserGroups);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case USER_FETCH_USER_GROUPS_BEGIN:
      return {
        ...state,
        fetchUserGroupsPending: true,
        fetchUserGroupsError: null,
        currentUserGroups: [],
      };

    case USER_FETCH_USER_GROUPS_SUCCESS:
      return {
        ...state,
        fetchUserGroupsPending: false,
        fetchUserGroupsError: null,
        currentUserGroups: action.data.data.groups,
      };

    case USER_FETCH_USER_GROUPS_FAILURE:
      return {
        ...state,
        fetchUserGroupsPending: false,
        fetchUserGroupsError: action.data.error,
        currentUserGroups: [],
      };

    case USER_FETCH_USER_GROUPS_DISMISS_ERROR:
      return {
        ...state,
        fetchUserGroupsError: null,
      };

    default:
      return state;
  }
}
