import { call, put, takeLatest } from 'redux-saga/effects';
import { clearFields } from 'redux-form';
import api, {
  apiAuthReqInterceptor,
  apiAuthRespInterceptor,
  apiAuthReqInterceptorFail,
  apiAuthRespInterceptorFail,
} from '../../../common/api';
import history from '../../../common/history';
import {
  USER_LOGIN_BEGIN,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGIN_DISMISS_ERROR,
  USER_FETCH_CURRENT_USER_BEGIN,
} from './constants';
import { doFetchCurrentUser } from './fetchCurrentUser';

export function login({ username, password, orgCode }) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: USER_LOGIN_BEGIN,
    username,
    password,
    orgCode,
  };
}

export function dismissLoginError() {
  return {
    type: USER_LOGIN_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on USER_LOGIN_BEGIN actions
export function* doLogin(args) {
  // If necessary, use argument to receive the begin action with parameters.
  try {
    const userResponse = yield call(
      api.post,
      '/login',
      {
        orgCode: args.orgCode,
      },
      {
        auth: {
          username: args.username,
          password: args.password,
        },
      },
    );

    yield put({
      type: USER_LOGIN_SUCCESS,
      data: userResponse,
    });

    yield put({
      type: USER_FETCH_CURRENT_USER_BEGIN,
    });

    yield doFetchCurrentUser();

    history.push('/job/new');
  } catch (err) {
    yield put(clearFields('loginForm', false, false, 'username', 'password', 'orgCode'));
    yield put({
      type: USER_LOGIN_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchLogin() {
  yield takeLatest(USER_LOGIN_BEGIN, doLogin);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case USER_LOGIN_BEGIN:
      localStorage.removeItem('tokens');

      return {
        ...state,
        loginPending: true,
        loginError: null,
        auth: null,
      };

    case USER_LOGIN_SUCCESS:
      api.setTokens(action.data.data);

      return {
        ...state,
        loginPending: false,
        loginError: null,
        auth: {
          apiRequestInterceptor: api.interceptors.request.use(
            apiAuthReqInterceptor,
            apiAuthReqInterceptorFail,
          ),
          apiResponseInterceptor: api.interceptors.response.use(
            apiAuthRespInterceptor,
            apiAuthRespInterceptorFail,
          ),
        },
      };

    case USER_LOGIN_FAILURE:
      return {
        ...state,
        loginPending: false,
        loginError: action.data.error,
        auth: null,
      };

    case USER_LOGIN_DISMISS_ERROR:
      return {
        ...state,
        loginError: null,
      };

    default:
      return state;
  }
}
