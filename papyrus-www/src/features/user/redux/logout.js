import api from '../../../common/api';
import { USER_LOGOUT } from './constants';

export function logout() {
  return {
    type: USER_LOGOUT,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case USER_LOGOUT:
      if (state.auth) {
        if (state.auth.apiRequestInterceptor !== undefined) {
          api.interceptors.request.eject(state.auth.apiRequestInterceptor);
        }
        if (state.auth.apiResponseInterceptor !== undefined) {
          api.interceptors.response.eject(state.auth.apiResponseInterceptor);
        }

        return {
          ...state,
          auth: null,
          currentUser: null,
          currentUserGroups: [],
        };
      }

      localStorage.removeItem('tokens');

      return {
        ...state,
        currentUser: null,
        currentUserGroups: [],
      };

    default:
      return state;
  }
}
