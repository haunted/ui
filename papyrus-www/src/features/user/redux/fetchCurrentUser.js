import { call, put, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import api from '../../../common/api';
import {
  USER_FETCH_CURRENT_USER_BEGIN,
  USER_FETCH_CURRENT_USER_SUCCESS,
  USER_FETCH_CURRENT_USER_FAILURE,
  USER_FETCH_CURRENT_USER_DISMISS_ERROR,
} from './constants';

export function fetchCurrentUser() {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: USER_FETCH_CURRENT_USER_BEGIN,
  };
}

export function dismissFetchCurrentUserError() {
  return {
    type: USER_FETCH_CURRENT_USER_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on USER_FETCH_CURRENT_USER_BEGIN actions
export function* doFetchCurrentUser() {
  // If necessary, use argument to receive the begin action with parameters.
  try {
    const res = yield call(api.get, '/me');

    yield put({
      type: USER_FETCH_CURRENT_USER_SUCCESS,
      data: res,
    });
  } catch (err) {
    yield call(message.error, 'Current user could not be loaded');

    yield put({
      type: USER_FETCH_CURRENT_USER_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchCurrentUser() {
  yield takeLatest(USER_FETCH_CURRENT_USER_BEGIN, doFetchCurrentUser);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case USER_FETCH_CURRENT_USER_BEGIN:
      return {
        ...state,
        fetchCurrentUserPending: true,
        fetchCurrentUserError: null,
        currentUser: null,
      };

    case USER_FETCH_CURRENT_USER_SUCCESS:
      return {
        ...state,
        fetchCurrentUserPending: false,
        fetchCurrentUserError: null,
        currentUser: action.data.data,
      };

    case USER_FETCH_CURRENT_USER_FAILURE:
      return {
        ...state,
        fetchCurrentUserPending: false,
        fetchCurrentUserError: action.data.error,
        currentUser: null,
      };

    case USER_FETCH_CURRENT_USER_DISMISS_ERROR:
      return {
        ...state,
        fetchCurrentUserError: null,
      };

    default:
      return state;
  }
}
