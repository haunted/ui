import api, {
  apiAuthReqInterceptor,
  apiAuthRespInterceptor,
  apiAuthReqInterceptorFail,
  apiAuthRespInterceptorFail,
} from '../../../common/api';
import { USER_IS_LOGGED_IN } from './constants';

export function isLoggedIn() {
  return {
    type: USER_IS_LOGGED_IN,
  };
}

export function reducer(state, action) {
  switch (action.type) {
    case USER_IS_LOGGED_IN:
      return {
        ...state,
        auth: api.isRefreshTokenValid()
          ? {
            apiRequestInterceptor: api.interceptors.request.use(
              apiAuthReqInterceptor,
              apiAuthReqInterceptorFail,
            ),
            apiResponseInterceptor: api.interceptors.response.use(
              apiAuthRespInterceptor,
              apiAuthRespInterceptorFail,
            ),
          }
          : null,
      };

    default:
      return state;
  }
}
