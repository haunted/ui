import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm } from 'redux-form';
import { Icon, Button, Form } from 'antd';
import TextField from '../common/formElements/TextField';

const FormItem = Form.Item;

class LoginForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    errorMsg: PropTypes.element,
    formLoading: PropTypes.bool.isRequired,
    buttonText: PropTypes.string.isRequired,
  };

  static defaultProps = {
    errorMsg: null,
  };

  render() {
    const {
      handleSubmit,
      errorMsg,
      formLoading,
      buttonText,
    } = this.props;

    return (
      <form onSubmit={handleSubmit} className="ant-form login-form">
        <Field
          name="username"
          label="Username"
          component={TextField}
          prefix={<Icon type="user" style={{ fontSize: 13 }} />}
        />
        <Field
          name="password"
          label="Password"
          type="password"
          component={TextField}
          prefix={<Icon type="lock" style={{ fontSize: 13 }} />}
        />
        <Field
          name="orgCode"
          label="Organization"
          component={TextField}
          prefix={<Icon type="desktop" style={{ fontSize: 13 }} />}
        />
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            className="login-form-button"
            icon="login"
            loading={formLoading}
          >
            {buttonText}
          </Button>
        </FormItem>
        <FormItem>{errorMsg}</FormItem>
      </form>
    );
  }
}

const validate = (values) => {
  const errors = {};

  if (!values.username) {
    errors.username = 'Please input your username!';
  }

  if (!values.password) {
    errors.password = 'Please input your Password!';
  }

  if (!values.orgCode) {
    errors.orgCode = 'Please input your Organization!';
  }

  return errors;
};

export const initialValues = {
  username: '',
  password: '',
  orgCode: '',
};

export default reduxForm({
  form: 'loginForm',
  validate,
  initialValues,
})(LoginForm);
