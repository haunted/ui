import { AuthCheck } from './';

export default {
  path: 'user',
  name: 'User',
  childRoutes: [
    {
      path: 'auth-check',
      name: 'Auth check',
      component: AuthCheck,
      isIndex: true,
    },
  ],
};
