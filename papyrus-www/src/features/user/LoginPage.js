import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
  Row,
  Col,
  Alert,
} from 'antd';
import * as actions from './redux/actions';
import LogoImg from '../../images/logo.png';
import LoginForm from './LoginForm';

export class LoginPage extends Component {
  static propTypes = {
    fetchCurrentUserError: PropTypes.bool,
    fetchUserGroupsError: PropTypes.bool,
    loginPending: PropTypes.bool.isRequired,
    fetchCurrentUserPending: PropTypes.bool.isRequired,
    loginError: PropTypes.object,
    actions: PropTypes.object.isRequired,
  };

  static defaultProps = {
    fetchCurrentUserError: false,
    fetchUserGroupsError: false,
    loginError: null,
  };

  componentDidMount() {
    this.props.actions.logout();
  }

  renderErrorMessage = () => {
    const {
      fetchCurrentUserError,
      fetchUserGroupsError,
      loginError,
    } = this.props;

    if (_.get(loginError, 'response.status', null) === 401) {
      return (
        <Alert
          message="Login failed, please check your credentials"
          type="error"
          closable
        />
      );
    }

    if (fetchCurrentUserError) {
      return (
        <Alert message="Could not fetch your account" type="error" closable />
      );
    }

    if (fetchUserGroupsError) {
      return (
        <Alert
          message="Could not fetch the groups that your account belongs to"
          type="error"
          closable
        />
      );
    }

    return null;
  };

  getButtonText = () => {
    const {
      fetchCurrentUserPending,
      loginPending,
    } = this.props;

    if (fetchCurrentUserPending) {
      return 'Fetching current user...';
    }

    if (loginPending) {
      return 'Authenticating...';
    }

    return 'Login';
  };

  render() {
    const {
      loginPending,
      fetchCurrentUserPending,
    } = this.props;

    return (
      <Row>
        <Col span={6} offset={9}>
          <div className="login-form">
            <img src={LogoImg} alt="logo" />
            <h1>Papyrus</h1>
            <LoginForm
              onSubmit={this.props.actions.login}
              errorMsg={this.renderErrorMessage()}
              buttonText={this.getButtonText()}
              formLoading={loginPending || fetchCurrentUserPending}
            />
          </div>
        </Col>
      </Row>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    ...state.user,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
