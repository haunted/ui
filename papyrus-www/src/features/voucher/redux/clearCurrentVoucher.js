import { CLEAR_CURRENT_VOUCHER } from './constants';

export function clearCurrentVoucher() {
  return {
    type: CLEAR_CURRENT_VOUCHER,
  };
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case CLEAR_CURRENT_VOUCHER:
      return {
        ...state,
        currentVoucher: {},
      };

    default:
      return state;
  }
}
