import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  FETCH_REJECT_REASONS_BEGIN,
  FETCH_REJECT_REASONS_SUCCESS,
  FETCH_REJECT_REASONS_FAILURE,
} from './constants';

export function fetchRejectReasons() {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: FETCH_REJECT_REASONS_BEGIN,
  };
}

export function* doFetchRejectReasons() {
  try {
    const response = yield call(api.get, '/vouchers/reject-reasons');

    yield put({
      type: FETCH_REJECT_REASONS_SUCCESS,
      data: response.data,
    });
  } catch (err) {
    yield put({
      type: FETCH_REJECT_REASONS_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchRejectReasons() {
  yield takeLatest(FETCH_REJECT_REASONS_BEGIN, doFetchRejectReasons);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case FETCH_REJECT_REASONS_BEGIN:
      return {
        ...state,
        loading: true,
      };

    case FETCH_REJECT_REASONS_SUCCESS:
      return {
        ...state,
        loading: false,
        rejectReasons: action.data,
      };

    case FETCH_REJECT_REASONS_FAILURE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
