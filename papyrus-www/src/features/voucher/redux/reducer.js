import initialState from './initialState';
import { reducer as rejectVoucherReducer } from './rejectVoucher';
import { reducer as fetchRejectReasonsReducer } from './fetchRejectReasons';
import { reducer as fetchVoucherReducer } from './fetchVoucher';
import { reducer as clearCurrentVoucherReducer } from './clearCurrentVoucher';

const reducers = [
  rejectVoucherReducer,
  fetchRejectReasonsReducer,
  fetchVoucherReducer,
  clearCurrentVoucherReducer,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    // Handle cross-topic actions here
    default:
      newState = state;
      break;
  }

  return reducers.reduce((s, r) => r(s, action), newState);
}
