import { call, put, takeLatest } from 'redux-saga/effects';
import { change } from 'redux-form';
import api from '../../../common/api';
import {
  FETCH_VOUCHER_BEGIN,
  FETCH_VOUCHER_SUCCESS,
  FETCH_VOUCHER_FAILURE,
} from './constants';

export function fetchVoucher(id) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: FETCH_VOUCHER_BEGIN,
    id,
  };
}

export function* doFetchVoucher({ id }) {
  try {
    const response = yield call(api.get, `/vouchers/${id}`);

    yield put({
      type: FETCH_VOUCHER_SUCCESS,
      data: response.data,
    });
    yield put(change('transcriberForm', 'reason', response.data.voucher.rejectReason));
  } catch (err) {
    yield put({
      type: FETCH_VOUCHER_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchVoucher() {
  yield takeLatest(FETCH_VOUCHER_BEGIN, doFetchVoucher);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case FETCH_VOUCHER_BEGIN:
      return {
        ...state,
        loading: true,
      };

    case FETCH_VOUCHER_SUCCESS:
      return {
        ...state,
        loading: false,
        currentVoucher: action.data.voucher,
      };

    case FETCH_VOUCHER_FAILURE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
