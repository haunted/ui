import { call, put, takeLatest, select } from 'redux-saga/effects';
import { message } from 'antd';
import api from '../../../common/api';
import {
  VOUCHER_REJECT_BEGIN,
  VOUCHER_REJECT_SUCCESS,
  VOUCHER_REJECT_FAILURE,
} from './constants';
import history from '../../../common/history';
import { isTranscriberSupervisor, isTranscriberPlus } from '../../../common/utils/authUtils';
import { rejectJob, submitJob } from '../../job/redux/actions';

export function rejectVoucher(currentJob, newJob) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: VOUCHER_REJECT_BEGIN,
    voucherId: currentJob.voucher.voucherId,
    jobId: currentJob.id,
    newJob,
  };
}

export function* doRejectVoucher({ voucherId, newJob, jobId }) {
  const currentUserGroups = yield select(state => state.user.currentUserGroups);
  const preparedValues = newJob;

  try {
    yield put({
      type: VOUCHER_REJECT_SUCCESS,
      data: yield call(
        api.put,
        `/vouchers/${voucherId}/reject`,
        { reason: newJob.reason },
      ),
    });
    delete preparedValues.submitType;
    delete preparedValues.reason;
    delete preparedValues.comment;

    if (isTranscriberSupervisor(currentUserGroups) || isTranscriberPlus(currentUserGroups)) {
      yield put(rejectJob(jobId));
      yield call(history.push, '/job/submitted');
    } else {
      yield call(history.push, '/job/new');
      yield put(submitJob(jobId, preparedValues));
    }
    yield call(message.info, 'Voucher has been successfully rejected');
  } catch (err) {
    yield put({
      type: VOUCHER_REJECT_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchRejectVoucher() {
  yield takeLatest(VOUCHER_REJECT_BEGIN, doRejectVoucher);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case VOUCHER_REJECT_BEGIN:
      return {
        ...state,
        loading: true,
      };

    case VOUCHER_REJECT_SUCCESS:
    case VOUCHER_REJECT_FAILURE:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
