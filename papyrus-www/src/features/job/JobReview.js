import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Form, Table, Tabs } from 'antd';
import moment from 'moment-timezone';
import _ from 'lodash';

import * as actions from './redux/actions';
import * as supplierActions from '../supplier/redux/actions';
import * as ticketActions from '../ticket/redux/actions';
import * as voucherActions from '../voucher/redux/actions';
import TranscriberFormWrapper from './TranscriberFormWrapper';
import JobInfo from './JobInfo';
import FeedbackList from './FeedbackList';
import { defaultDateFormat } from '../common/constants';

const { TabPane } = Tabs;

export class JobReview extends Component {
  static propTypes = {
    job: PropTypes.object.isRequired,
    supplier: PropTypes.object.isRequired,
    reseller: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    voucherActions: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.actions.clearCurrentJob();
  }

  componentDidMount() {
    const { params } = this.props.match;

    if (this.props.match.params.jobID) {
      this.props.actions.fetchJob(params.jobID);
      this.props.actions.fetchFeedbacksByJob(params.jobID);
      this.props.actions.fetchLogsByJob(params.jobID);
    }
  }

  approveJob = () => {
    const currentJob = _.get(this.props.job, 'currentJob');

    if (currentJob) {
      this.props.actions.approveJob(currentJob.id);
    }
  };

  rejectJob = (values) => {
    const currentJob = _.get(this.props.job, 'currentJob');

    if (currentJob) {
      this.props.voucherActions.rejectVoucher(
        currentJob,
        values,
      );
    }
  };

  submitJob = (values) => {
    const currentJob = _.get(this.props.job, 'currentJob');

    if (currentJob) {
      const preparedValues = { ...values };
      const { comment } = values;

      delete preparedValues.comment;
      delete preparedValues.submitType;
      delete preparedValues.reason;

      this.props.actions.submitJob(currentJob.id, preparedValues, true, comment);
    }
  };

  render() {
    const logColumns = [
      {
        title: 'Log ID',
        dataIndex: 'id',
      },
      {
        title: 'Event',
        dataIndex: 'event',
        render: text => this.props.job.logEvents[text],
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        render: datetime => moment(datetime).format(defaultDateFormat),
        sorter: (a, b) => {
          if (a.createdAt < b.createdAt) {
            return -1;
          }
          if (a.createdAt > b.createdAt) {
            return 1;
          }

          return 0;
        },
        // TODO: Show correct value for "By user"
        // }, {
        //   title: 'By user',
        //   dataIndex: 'voucher.supplierId',
      },
    ];

    const currentJob = _.get(this.props.job, 'currentJob', {});
    const status = _.get(currentJob, 'status', '');

    return (
      <Tabs className="job-job-review" defaultActiveKey="transcription">
        <TabPane tab="Transcription" key="transcription">
          <h1>Review job</h1>
          {status && this.props.supplier.suppliers && (
            <div>
              <JobInfo
                job={currentJob}
                statuses={this.props.job.statuses}
                suppliers={this.props.supplier.suppliers}
              />
              <TranscriberFormWrapper
                useComment
                submitLabel="Approve & comment"
                rejectJob={this.rejectJob}
                submitJob={this.submitJob}
                approveJob={this.approveJob}
              />
            </div>
          )}
        </TabPane>
        {this.props.job.currentJobFeedbacks &&
        !!this.props.job.currentJobFeedbacks.length && (
          <TabPane tab="Feedbacks" key="feedbacks">
            <FeedbackList
              feedbacks={this.props.job.currentJobFeedbacks}
              resellers={this.props.reseller.resellers}
            />
          </TabPane>
        )}
        {this.props.job.currentJobLogs &&
        !!this.props.job.currentJobLogs.length && (
          <TabPane tab="Logs" key="logs">
            <Table
              dataSource={this.props.job.currentJobLogs}
              rowKey="id"
              columns={logColumns}
              pagination={false}
            />
          </TabPane>
        )}
      </Tabs>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    job: state.job,
    supplier: state.supplier,
    reseller: state.reseller,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
    supplierActions: bindActionCreators({ ...supplierActions }, dispatch),
    ticketActions: bindActionCreators({ ...ticketActions }, dispatch),
    voucherActions: bindActionCreators({ ...voucherActions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(JobReview));
