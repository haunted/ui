import { call, put, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import api from '../../../common/api';
import {
  JOB_APPROVE_JOB_BEGIN,
  JOB_APPROVE_JOB_SUCCESS,
  JOB_APPROVE_JOB_FAILURE,
  JOB_APPROVE_JOB_DISMISS_ERROR,
} from './constants';
import history from '../../../common/history';

export function approveJob(jobID) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_APPROVE_JOB_BEGIN,
    jobID,
  };
}

export function dismissApproveJobError() {
  return {
    type: JOB_APPROVE_JOB_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_APPROVE_JOB_BEGIN actions
export function* doApproveJob(args) {
  // If necessary, use argument to receive the begin action with parameters.

  try {
    const res = yield call(api.put, `/jobs/${args.jobID}/approve`);

    yield put({
      type: JOB_APPROVE_JOB_SUCCESS,
      data: res,
    });
    yield call(history.push, '/job/submitted');
    yield call(message.info, 'Job has been approved');
  } catch (err) {
    yield put({
      type: JOB_APPROVE_JOB_FAILURE,
      data: { error: err },
    });
    yield call(message.error, 'Job could not be approved');
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchApproveJob() {
  yield takeLatest(JOB_APPROVE_JOB_BEGIN, doApproveJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_APPROVE_JOB_BEGIN:
      return {
        ...state,
        approveJobPending: true,
        approveJobError: null,
        currentJob: {},
      };

    case JOB_APPROVE_JOB_SUCCESS:
      return {
        ...state,
        approveJobPending: false,
        approveJobError: null,
        currentJob: action.data.data,
      };

    case JOB_APPROVE_JOB_FAILURE:
      return {
        ...state,
        approveJobPending: false,
        approveJobError: action.data.error,
        currentJob: {},
      };

    case JOB_APPROVE_JOB_DISMISS_ERROR:
      return {
        ...state,
        approveJobError: null,
      };

    default:
      return state;
  }
}
