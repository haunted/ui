import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_REJECT_JOB_BEGIN,
  JOB_REJECT_JOB_SUCCESS,
  JOB_REJECT_JOB_FAILURE,
} from './constants';

export function rejectJob(jobID) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_REJECT_JOB_BEGIN,
    jobID,
  };
}

// worker Saga: will be fired on JOB_REJECT_JOB_BEGIN actions
export function* doRejectJob(args) {
  // If necessary, use argument to receive the begin action with parameters.

  try {
    const response = yield call(
      api.put,
      `/jobs/${args.jobID}/reject`,
      {},
    );

    yield put({
      type: JOB_REJECT_JOB_SUCCESS,
      data: response,
    });
  } catch (err) {
    yield put({
      type: JOB_REJECT_JOB_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchRejectJob() {
  yield takeLatest(JOB_REJECT_JOB_BEGIN, doRejectJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_REJECT_JOB_BEGIN:
      return {
        ...state,
        rejectJobPending: true,
        rejectJobError: null,
      };

    case JOB_REJECT_JOB_SUCCESS:
      return {
        ...state,
        rejectJobPending: false,
        rejectJobError: null,
      };

    case JOB_REJECT_JOB_FAILURE:
      return {
        ...state,
        rejectJobPending: false,
        rejectJobError: action.data.error,
      };

    default:
      return state;
  }
}
