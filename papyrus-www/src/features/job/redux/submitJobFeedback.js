import { call, put, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import api from '../../../common/api';
import history from '../../../common/history';
import {
  JOB_SUBMIT_JOB_FEEDBACK_BEGIN,
  JOB_SUBMIT_JOB_FEEDBACK_SUCCESS,
  JOB_SUBMIT_JOB_FEEDBACK_FAILURE,
  JOB_SUBMIT_JOB_FEEDBACK_DISMISS_ERROR,
} from './constants';

export function submitJobFeedback(jobID, feedback, comment) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_SUBMIT_JOB_FEEDBACK_BEGIN,
    jobID,
    feedback,
    comment,
  };
}

export function dismissSubmitJobFeedbackError() {
  return {
    type: JOB_SUBMIT_JOB_FEEDBACK_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_SUBMIT_JOB_FEEDBACK_BEGIN actions
export function* doSubmitJobFeedback(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.post, `/jobs/${args.jobID}/feedbacks`, {
      comment: args.comment,
      feedback: args.feedback,
    });
    yield call(history.push, '/job/submitted');
    yield call(message.info, 'Feedback has been saved');
  } catch (err) {
    yield put({
      type: JOB_SUBMIT_JOB_FEEDBACK_FAILURE,
      data: { error: err },
    });
    yield call(message.error, 'Feedback for job could not be saved');

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_SUBMIT_JOB_FEEDBACK_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchSubmitJobFeedback() {
  yield takeLatest(JOB_SUBMIT_JOB_FEEDBACK_BEGIN, doSubmitJobFeedback);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_SUBMIT_JOB_FEEDBACK_BEGIN:
      return {
        ...state,
        submitJobFeedbackPending: true,
        submitJobFeedbackError: null,
      };

    case JOB_SUBMIT_JOB_FEEDBACK_SUCCESS:
      return {
        ...state,
        submitJobFeedbackPending: false,
        submitJobFeedbackError: null,
      };

    case JOB_SUBMIT_JOB_FEEDBACK_FAILURE:
      return {
        ...state,
        submitJobFeedbackPending: false,
        submitJobFeedbackError: action.data.error,
      };

    case JOB_SUBMIT_JOB_FEEDBACK_DISMISS_ERROR:
      return {
        ...state,
        submitJobFeedbackError: null,
      };

    default:
      return state;
  }
}
