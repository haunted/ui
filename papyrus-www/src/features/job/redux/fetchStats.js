import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_FETCH_STATS_BEGIN,
  JOB_FETCH_STATS_SUCCESS,
  JOB_FETCH_STATS_FAILURE,
  JOB_FETCH_STATS_DISMISS_ERROR,
} from './constants';

export function fetchStats() {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_FETCH_STATS_BEGIN,
  };
}

export function dismissFetchStatsError() {
  return {
    type: JOB_FETCH_STATS_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_FETCH_STATS_BEGIN actions
export function* doFetchStats() {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.get, '/jobs/stats');
  } catch (err) {
    yield put({
      type: JOB_FETCH_STATS_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_FETCH_STATS_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchStats() {
  yield takeLatest(JOB_FETCH_STATS_BEGIN, doFetchStats);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_FETCH_STATS_BEGIN:
      return {
        ...state,
        fetchStatsPending: true,
        fetchStatsError: null,
        ownJobStats: {},
      };

    case JOB_FETCH_STATS_SUCCESS:
      return {
        ...state,
        fetchStatsPending: false,
        fetchStatsError: null,
        ownJobStats: action.data.data.stats,
      };

    case JOB_FETCH_STATS_FAILURE:
      return {
        ...state,
        fetchStatsPending: false,
        fetchStatsError: action.data.error,
        ownJobStats: {},
      };

    case JOB_FETCH_STATS_DISMISS_ERROR:
      return {
        ...state,
        fetchStatsError: null,
      };

    default:
      return state;
  }
}
