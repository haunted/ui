import initialState from './initialState';
import { reducer as acquireJobReducer } from './acquireJob';
import { reducer as submitJobReducer } from './submitJob';
import { reducer as fetchJobReducer } from './fetchJob';
import { reducer as reviewJobReducer } from './reviewJob';
import { reducer as approveJobReducer } from './approveJob';
import { reducer as submitJobFeedbackReducer } from './submitJobFeedback';
import { reducer as fetchLogsByJobReducer } from './fetchLogsByJob';
import { reducer as fetchFeedbacksByJobReducer } from './fetchFeedbacksByJob';
import { reducer as setFeedbackAsReadReducer } from './setFeedbackAsRead';
import { reducer as unlockJobReducer } from './unlockJob';
import { reducer as fetchJobsReducer } from './fetchJobs';
import { reducer as fetchStatsReducer } from './fetchStats';
import { reducer as clearCurrentJobReducer } from './clearCurrentJob';
import { reducer as rejectJobReducer } from './rejectJob';

const reducers = [
  acquireJobReducer,
  submitJobReducer,
  fetchJobReducer,
  reviewJobReducer,
  approveJobReducer,
  submitJobFeedbackReducer,
  fetchLogsByJobReducer,
  fetchFeedbacksByJobReducer,
  setFeedbackAsReadReducer,
  unlockJobReducer,
  fetchJobsReducer,
  fetchStatsReducer,
  clearCurrentJobReducer,
  rejectJobReducer,
];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    // Handle cross-topic actions here
    default:
      newState = state;
      break;
  }

  return reducers.reduce((s, r) => r(s, action), newState);
}
