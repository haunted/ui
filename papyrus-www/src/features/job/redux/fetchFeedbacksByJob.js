import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_FETCH_FEEDBACKS_BY_JOB_BEGIN,
  JOB_FETCH_FEEDBACKS_BY_JOB_SUCCESS,
  JOB_FETCH_FEEDBACKS_BY_JOB_FAILURE,
  JOB_FETCH_FEEDBACKS_BY_JOB_DISMISS_ERROR,
} from './constants';

export function fetchFeedbacksByJob(jobID) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_FETCH_FEEDBACKS_BY_JOB_BEGIN,
    jobID,
  };
}

export function dismissFetchFeedbacksByJobError() {
  return {
    type: JOB_FETCH_FEEDBACKS_BY_JOB_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_FETCH_FEEDBACKS_BY_JOB_BEGIN actions
export function* doFetchFeedbacksByJob(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.get, `/jobs/${args.jobID}/feedbacks`);
  } catch (err) {
    yield put({
      type: JOB_FETCH_FEEDBACKS_BY_JOB_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_FETCH_FEEDBACKS_BY_JOB_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchFeedbacksByJob() {
  yield takeLatest(JOB_FETCH_FEEDBACKS_BY_JOB_BEGIN, doFetchFeedbacksByJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_FETCH_FEEDBACKS_BY_JOB_BEGIN:
      return {
        ...state,
        fetchFeedbacksByJobPending: true,
        fetchFeedbacksByJobError: null,
      };

    case JOB_FETCH_FEEDBACKS_BY_JOB_SUCCESS:
      return {
        ...state,
        fetchFeedbacksByJobPending: false,
        fetchFeedbacksByJobError: null,
        currentJobFeedbacks: action.data.data.feedbacks,
      };

    case JOB_FETCH_FEEDBACKS_BY_JOB_FAILURE:
      return {
        ...state,
        fetchFeedbacksByJobPending: false,
        fetchFeedbacksByJobError: action.data.error,
      };

    case JOB_FETCH_FEEDBACKS_BY_JOB_DISMISS_ERROR:
      return {
        ...state,
        fetchFeedbacksByJobError: null,
      };

    default:
      return state;
  }
}
