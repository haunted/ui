import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_FETCH_LOGS_BY_JOB_BEGIN,
  JOB_FETCH_LOGS_BY_JOB_SUCCESS,
  JOB_FETCH_LOGS_BY_JOB_FAILURE,
  JOB_FETCH_LOGS_BY_JOB_DISMISS_ERROR,
} from './constants';

export function fetchLogsByJob(jobID) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_FETCH_LOGS_BY_JOB_BEGIN,
    jobID,
  };
}

export function dismissFetchLogsByJobError() {
  return {
    type: JOB_FETCH_LOGS_BY_JOB_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_FETCH_LOGS_BY_JOB_BEGIN actions
export function* doFetchLogsByJob(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.get, `/jobs/${args.jobID}/logs`);
  } catch (err) {
    yield put({
      type: JOB_FETCH_LOGS_BY_JOB_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_FETCH_LOGS_BY_JOB_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchLogsByJob() {
  yield takeLatest(JOB_FETCH_LOGS_BY_JOB_BEGIN, doFetchLogsByJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_FETCH_LOGS_BY_JOB_BEGIN:
      return {
        ...state,
        fetchLogsByJobPending: true,
        fetchLogsByJobError: null,
      };

    case JOB_FETCH_LOGS_BY_JOB_SUCCESS:
      return {
        ...state,
        fetchLogsByJobPending: false,
        fetchLogsByJobError: null,
        currentJobLogs: action.data.data.logs,
      };

    case JOB_FETCH_LOGS_BY_JOB_FAILURE:
      return {
        ...state,
        fetchLogsByJobPending: false,
        fetchLogsByJobError: action.data.error,
      };

    case JOB_FETCH_LOGS_BY_JOB_DISMISS_ERROR:
      return {
        ...state,
        fetchLogsByJobError: null,
      };

    default:
      return state;
  }
}
