import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_REVIEW_JOB_BEGIN,
  JOB_REVIEW_JOB_SUCCESS,
  JOB_REVIEW_JOB_FAILURE,
  JOB_REVIEW_JOB_DISMISS_ERROR,
} from './constants';

export function reviewJob(jobID) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_REVIEW_JOB_BEGIN,
    jobID,
  };
}

export function dismissReviewJobError() {
  return {
    type: JOB_REVIEW_JOB_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_REVIEW_JOB_BEGIN actions
export function* doReviewJob(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.put, `/jobs/${args.jobID}/review`);
  } catch (err) {
    yield put({
      type: JOB_REVIEW_JOB_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_REVIEW_JOB_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchReviewJob() {
  yield takeLatest(JOB_REVIEW_JOB_BEGIN, doReviewJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_REVIEW_JOB_BEGIN:
      return {
        ...state,
        reviewJobPending: true,
        reviewJobError: null,
      };

    case JOB_REVIEW_JOB_SUCCESS:
      return {
        ...state,
        reviewJobPending: false,
        reviewJobError: null,
        currentJob: action.data.data,
      };

    case JOB_REVIEW_JOB_FAILURE:
      return {
        ...state,
        reviewJobPending: false,
        reviewJobError: action.data.error,
      };

    case JOB_REVIEW_JOB_DISMISS_ERROR:
      return {
        ...state,
        reviewJobError: null,
      };

    default:
      return state;
  }
}
