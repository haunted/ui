import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_SET_FEEDBACK_AS_READ_BEGIN,
  JOB_SET_FEEDBACK_AS_READ_SUCCESS,
  JOB_SET_FEEDBACK_AS_READ_FAILURE,
  JOB_SET_FEEDBACK_AS_READ_DISMISS_ERROR,
} from './constants';

export function setFeedbackAsRead(feedbackID) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_SET_FEEDBACK_AS_READ_BEGIN,
    feedbackID,
  };
}

export function dismissSetFeedbackAsReadError() {
  return {
    type: JOB_SET_FEEDBACK_AS_READ_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_SET_FEEDBACK_AS_READ_BEGIN actions
export function* doSetFeedbackAsRead(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.put, `/feedbacks/${args.feedbackID}/read`);
  } catch (err) {
    yield put({
      type: JOB_SET_FEEDBACK_AS_READ_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_SET_FEEDBACK_AS_READ_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchSetFeedbackAsRead() {
  yield takeLatest(JOB_SET_FEEDBACK_AS_READ_BEGIN, doSetFeedbackAsRead);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_SET_FEEDBACK_AS_READ_BEGIN:
      return {
        ...state,
        setFeedbackAsReadPending: true,
        setFeedbackAsReadError: null,
      };

    case JOB_SET_FEEDBACK_AS_READ_SUCCESS:
      return {
        ...state,
        setFeedbackAsReadPending: false,
        setFeedbackAsReadError: null,
      };

    case JOB_SET_FEEDBACK_AS_READ_FAILURE:
      return {
        ...state,
        setFeedbackAsReadPending: false,
        setFeedbackAsReadError: action.data.error,
      };

    case JOB_SET_FEEDBACK_AS_READ_DISMISS_ERROR:
      return {
        ...state,
        setFeedbackAsReadError: null,
      };

    default:
      return state;
  }
}
