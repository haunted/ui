import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_UNLOCK_JOB_BEGIN,
  JOB_UNLOCK_JOB_SUCCESS,
  JOB_UNLOCK_JOB_FAILURE,
  JOB_UNLOCK_JOB_DISMISS_ERROR,
} from './constants';

export function unlockJob(id) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_UNLOCK_JOB_BEGIN,
    id,
  };
}

export function dismissUnlockJobError() {
  return {
    type: JOB_UNLOCK_JOB_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_UNLOCK_JOB_BEGIN actions
export function* doUnlockJob(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.put, `/jobs/${args.id}/unlock`);
  } catch (err) {
    yield put({
      type: JOB_UNLOCK_JOB_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_UNLOCK_JOB_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchUnlockJob() {
  yield takeLatest(JOB_UNLOCK_JOB_BEGIN, doUnlockJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_UNLOCK_JOB_BEGIN:
      return {
        ...state,
        unlockJobPending: true,
        unlockJobError: null,
      };

    case JOB_UNLOCK_JOB_SUCCESS:
      if (state.acquiredJobs) {
        // TODO Refactor reassign
        // eslint-disable-next-line
        state.acquiredJobs = state.acquiredJobs.filter(job => job.id !== action.data.data.id);
      }

      return {
        ...state,
        unlockJobPending: false,
        unlockJobError: null,
        currentJob: action.data.data,
      };

    case JOB_UNLOCK_JOB_FAILURE:
      return {
        ...state,
        unlockJobPending: false,
        unlockJobError: action.data.error,
      };

    case JOB_UNLOCK_JOB_DISMISS_ERROR:
      return {
        ...state,
        unlockJobError: null,
      };

    default:
      return state;
  }
}
