// Initial state is the place you define all initial values for the Redux store of the feature.
// In the 'standard' way, initialState is defined
// in reducers: http://redux.js.org/docs/basics/Reducers.html
// But when application grows, there will be multiple reducers
// files, it's not intuitive what data is managed by the whole store.
// So Rekit extracts the initial state definition into a separate module so that you can have
// a quick view about what data is used for the feature, at any time.

// NOTE: initialState constant is necessary so that
// Rekit could auto add initial state when creating async actions.
const initialState = {
  currentJob: null,
  currentJobLogs: [],
  openJobs: [],
  acquiredJobs: [],
  submittedJobs: [],
  inReviewJobs: [],
  approvedJobs: [],
  statuses: {
    STATUS_UNKNOWN: 'Unknown',
    STATUS_OPEN: 'Open',
    STATUS_LOCKED: 'Locked',
    STATUS_SUBMITTED: 'Submitted',
    STATUS_IN_REVIEW: 'In review',
    STATUS_DELETED: 'Deleted',
    STATUS_HAS_FEEDBACK: 'Has feedback',
    STATUS_APPROVED: 'Approved',
    STATUS_REJECTED: 'Rejected',
  },
  logEvents: {
    EVENT_UNKNOWN: 'Unknown',
    EVENT_CREATION: 'Creation',
    EVENT_LOCKING: 'Locking',
    EVENT_EXPIRATION: 'Expiration',
    EVENT_SUBMISSION: 'Submission',
    EVENT_REVIEW: 'Review',
    EVENT_FEEDBACK_RECEPTION: 'Feedback reception',
    EVENT_APPROVAL: 'Approval',
    EVENT_DELETION: 'Deletion',
    EVENT_UPDATE: 'Update',
    EVENT_UNLOCK: 'Unlocked',
  },
  fetchAcquiredJobsPending: false,
  fetchAcquiredJobsError: null,
  acquireJobPending: false,
  acquireJobError: null,
  submitJobPending: false,
  submitJobError: null,
  rejectJobPending: false,
  rejectJobError: null,
  fetchJobPending: false,
  fetchJobError: null,
  reviewJobPending: false,
  reviewJobError: null,
  approveJobPending: false,
  approveJobError: null,
  submitJobFeedbackPending: false,
  submitJobFeedbackError: null,
  fetchLogsByJobPending: false,
  fetchLogsByJobError: null,
  fetchFeedbacksByJobPending: false,
  fetchFeedbacksByJobError: null,
  setFeedbackAsReadPending: false,
  setFeedbackAsReadError: null,
  unlockJobPending: false,
  unlockJobError: null,
  fetchJobsPending: false,
  fetchJobsError: null,
  fetchOwnApprovedJobsPending: false,
  fetchOwnApprovedJobsError: null,
  fetchStatsPending: false,
  fetchStatsError: null,
};

export default initialState;
