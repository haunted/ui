import { call, put, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';

import api from '../../../common/api';
import { submitJobFeedback } from './actions';
import {
  JOB_SUBMIT_JOB_BEGIN,
  JOB_SUBMIT_JOB_SUCCESS,
  JOB_SUBMIT_JOB_FAILURE,
  JOB_SUBMIT_JOB_DISMISS_ERROR,
} from './constants';
import history from '../../../common/history';

export function submitJob(jobID, transcribedVoucher, hasComment, comment) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_SUBMIT_JOB_BEGIN,
    jobID,
    transcribedVoucher,
    hasComment,
    comment,
  };
}

export function dismissSubmitJobError() {
  return {
    type: JOB_SUBMIT_JOB_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_SUBMIT_JOB_BEGIN actions
export function* doSubmitJob(args) {
  // If necessary, use argument to receive the begin action with parameters.

  try {
    const response = yield call(
      api.put,
      `/jobs/${args.jobID}/submit`,
      args.transcribedVoucher,
    );

    yield put({
      type: JOB_SUBMIT_JOB_SUCCESS,
      data: response,
    });

    if (args.hasComment) {
      return yield put(submitJobFeedback(args.jobID, args.transcribedVoucher, args.comment));
    }
    yield call(history.push, '/job/new');

    return yield call(message.info, 'Job has been submitted');
  } catch (err) {
    yield put({
      type: JOB_SUBMIT_JOB_FAILURE,
      data: { error: err },
    });

    return yield call(message.error, 'Could not submit job');
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchSubmitJob() {
  yield takeLatest(JOB_SUBMIT_JOB_BEGIN, doSubmitJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_SUBMIT_JOB_BEGIN:
      return {
        ...state,
        submitJobPending: true,
        submitJobError: null,
      };

    case JOB_SUBMIT_JOB_SUCCESS:
      return {
        ...state,
        submitJobPending: false,
        submitJobError: null,
      };

    case JOB_SUBMIT_JOB_FAILURE:
      return {
        ...state,
        submitJobPending: false,
        submitJobError: action.data.error,
      };

    case JOB_SUBMIT_JOB_DISMISS_ERROR:
      return {
        ...state,
        submitJobError: null,
      };

    default:
      return state;
  }
}
