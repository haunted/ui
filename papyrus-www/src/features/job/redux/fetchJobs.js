import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_FETCH_JOBS_BEGIN,
  JOB_FETCH_JOBS_SUCCESS,
  JOB_FETCH_JOBS_FAILURE,
  JOB_FETCH_JOBS_DISMISS_ERROR,
} from './constants';

export function fetchJobs(limit, page, filters, sorter, listType) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_FETCH_JOBS_BEGIN,
    limit,
    page,
    filters,
    sorter,
    listType,
  };
}

export function dismissFetchJobsError() {
  return {
    type: JOB_FETCH_JOBS_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_FETCH_JOBS_BEGIN actions
export function* doFetchJobs(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    let sort;
    const filters = [];
    const baseUrl = args.listType ? `/jobs/${args.listType}` : '/jobs'; // TODO fix when core 364

    if (args.sorter && args.sorter.field) {
      sort = args.sorter.field;
      if (args.sorter.order === 'descend') {
        sort = `-${sort}`;
      }
    }
    // TODO Refactor that
    // eslint-disable-next-line
    for (const field in args.filters) {
      filters.push(`${field}=${args.filters[field]}`);
    }

    res = yield call(
      api.get,
      `${baseUrl}?limit=${
        args.limit
      }&offset=${
        (args.page - 1) * args.limit
      }${sort && sort !== '' ? `&sort=${sort}` : ''
      }${filters.length > 0 ? `&${filters.join('&')}` : ''}`,
    );
  } catch (err) {
    yield put({
      type: JOB_FETCH_JOBS_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_FETCH_JOBS_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchJobs() {
  yield takeLatest(JOB_FETCH_JOBS_BEGIN, doFetchJobs);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_FETCH_JOBS_BEGIN:
      return {
        ...state,
        fetchJobsPending: true,
        fetchJobsError: null,
        jobs: [],
        totalJobs: 0,
      };

    case JOB_FETCH_JOBS_SUCCESS:
      return {
        ...state,
        fetchJobsPending: false,
        fetchJobsError: null,
        jobs: action.data.data.jobs,
        totalJobs: parseInt(action.data.data.totalCount, 10),
      };

    case JOB_FETCH_JOBS_FAILURE:
      return {
        ...state,
        fetchJobsPending: false,
        fetchJobsError: action.data.error,
        jobs: [],
        totalJobs: 0,
      };

    case JOB_FETCH_JOBS_DISMISS_ERROR:
      return {
        ...state,
        fetchJobsError: null,
      };

    default:
      return state;
  }
}
