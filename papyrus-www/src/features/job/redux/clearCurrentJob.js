import { CLEAR_CURRENT_JOB } from './constants';

export function clearCurrentJob() {
  return {
    type: CLEAR_CURRENT_JOB,
  };
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case CLEAR_CURRENT_JOB:
      return {
        ...state,
        currentJob: {},
      };
    default:
      return state;
  }
}
