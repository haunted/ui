import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  JOB_ACQUIRE_JOB_BEGIN,
  JOB_ACQUIRE_JOB_SUCCESS,
  JOB_ACQUIRE_JOB_FAILURE,
  JOB_ACQUIRE_JOB_DISMISS_ERROR,
} from './constants';

export function acquireJob(id) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_ACQUIRE_JOB_BEGIN,
    id,
  };
}

export function dismissAcquireJobError() {
  return {
    type: JOB_ACQUIRE_JOB_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_ACQUIRE_JOB_BEGIN actions
export function* doAcquireJob(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    if (args.id) {
      res = yield call(api.put, `/jobs/${args.id}/acquire`);
    } else {
      res = yield call(api.put, '/jobs');
    }
  } catch (err) {
    yield put({
      type: JOB_ACQUIRE_JOB_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: JOB_ACQUIRE_JOB_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchAcquireJob() {
  yield takeLatest(JOB_ACQUIRE_JOB_BEGIN, doAcquireJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_ACQUIRE_JOB_BEGIN:
      return {
        ...state,
        acquireJobPending: true,
        acquireJobError: null,
        currentJob: {},
      };

    case JOB_ACQUIRE_JOB_SUCCESS:
      return {
        ...state,
        acquireJobPending: false,
        acquireJobError: null,
        currentJob: action.data.data,
      };

    case JOB_ACQUIRE_JOB_FAILURE:
      return {
        ...state,
        acquireJobPending: false,
        acquireJobError: action.data.error,
        currentJob: {},
      };

    case JOB_ACQUIRE_JOB_DISMISS_ERROR:
      return {
        ...state,
        acquireJobError: null,
      };

    default:
      return state;
  }
}
