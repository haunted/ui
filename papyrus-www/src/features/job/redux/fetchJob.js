import { call, put, takeLatest, all, take, select } from 'redux-saga/effects';
import _ from 'lodash';
import { change } from 'redux-form';
import api from '../../../common/api';
import {
  JOB_FETCH_JOB_BEGIN,
  JOB_FETCH_JOB_SUCCESS,
  JOB_FETCH_JOB_FAILURE,
  JOB_FETCH_JOB_DISMISS_ERROR,
} from './constants';
import { fetchRejectReasons, fetchVoucher } from '../../voucher/redux/actions';
import { fetchResellers } from '../../reseller/redux/actions';
import { fetchCatalogItemsByReseller } from '../../supplier/redux/actions';

export function fetchJob(jobID) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: JOB_FETCH_JOB_BEGIN,
    jobID,
  };
}

export function dismissFetchJobError() {
  return {
    type: JOB_FETCH_JOB_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on JOB_FETCH_JOB_BEGIN actions
export function* doFetchJob(args) {
  try {
    const response = yield call(api.get, `/jobs/${args.jobID}`);

    const resellerId = _.get(response.data, 'transcribedVoucher.resellerId');
    const productId = _.get(response.data, 'transcribedVoucher.productId', '');
    const travelerFirstName = _.get(response.data, 'transcribedVoucher.travelerFirstName', '');
    const travelerLastName = _.get(response.data, 'transcribedVoucher.travelerLastName', '');
    const numberOfAdults = _.get(response.data, 'transcribedVoucher.numberOfAdults', 0);
    const numberOfChildren = _.get(response.data, 'transcribedVoucher.numberOfChildren', 0);
    const numberOfStudents = _.get(response.data, 'transcribedVoucher.numberOfStudents', 0);
    const numberOfInfants = _.get(response.data, 'transcribedVoucher.numberOfInfants', 0);
    const numberOfSeniors = _.get(response.data, 'transcribedVoucher.numberOfSeniors', 0);
    const bookingReferenceNumber =
      _.get(response.data, 'transcribedVoucher.bookingReferenceNumber', '');

    yield put({
      type: JOB_FETCH_JOB_SUCCESS,
      data: response,
    });

    yield put(fetchVoucher(response.data.voucher.voucherId));
    yield put(fetchResellers(response.data.voucher.supplierId));
    yield put(fetchRejectReasons());
    yield all([
      take('FETCH_VOUCHER_SUCCESS'),
      take('RESELLER_FETCH_RESELLERS_SUCCESS'),
      take('FETCH_REJECT_REASONS_SUCCESS'),
    ]);

    if (_.isEmpty(yield select(state => state.supplier.suppliers))) {
      yield take('SUPPLIER_FETCH_SUPPLIERS_SUCCESS');
    }

    if (resellerId) {
      yield put(fetchCatalogItemsByReseller(response.data.voucher.supplierId, resellerId));
      yield take('SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS');
    }

    if (resellerId) {
      yield put(change('transcriberForm', 'resellerId', resellerId));
    }
    if (productId) {
      yield put(change('transcriberForm', 'productId', productId));
    }
    if (bookingReferenceNumber) {
      yield put(change('transcriberForm', 'bookingReferenceNumber', bookingReferenceNumber));
    }
    if (travelerFirstName) {
      yield put(change('transcriberForm', 'travelerFirstName', travelerFirstName));
    }
    if (travelerLastName) {
      yield put(change('transcriberForm', 'travelerLastName', travelerLastName));
    }
    if (numberOfAdults) {
      yield put(change('transcriberForm', 'numberOfAdults', numberOfAdults));
    }
    if (numberOfChildren) {
      yield put(change('transcriberForm', 'numberOfChildren', numberOfChildren));
    }
    if (numberOfStudents) {
      yield put(change('transcriberForm', 'numberOfStudents', numberOfStudents));
    }
    if (numberOfInfants) {
      yield put(change('transcriberForm', 'numberOfInfants', numberOfInfants));
    }
    if (numberOfSeniors) {
      yield put(change('transcriberForm', 'numberOfSeniors', numberOfSeniors));
    }
  } catch (err) {
    yield put({
      type: JOB_FETCH_JOB_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchJob() {
  yield takeLatest(JOB_FETCH_JOB_BEGIN, doFetchJob);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case JOB_FETCH_JOB_BEGIN:
      return {
        ...state,
        fetchJobPending: true,
        fetchJobError: null,
      };

    case JOB_FETCH_JOB_SUCCESS:
      return {
        ...state,
        fetchJobPending: false,
        fetchJobError: null,
        currentJob: action.data.data,
      };

    case JOB_FETCH_JOB_FAILURE:
      return {
        ...state,
        fetchJobPending: false,
        fetchJobError: action.data.error,
      };

    case JOB_FETCH_JOB_DISMISS_ERROR:
      return {
        ...state,
        fetchJobError: null,
      };

    default:
      return state;
  }
}
