/* eslint-disable react/no-unused-state */
/* eslint-disable react/prop-types */
// TODO Check or refactor it and add prop-types

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Icon, Button, message } from 'antd';
import * as actions from './redux/actions';
import JobsTable from './JobsTable';

export class AcquiredJobs extends Component {
  static propTypes = {
    job: PropTypes.object.isRequired,
    supplier: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  state = {
    jobUnlocking: false,
    pageSize: 20,
    sorter: {
      field: 'createdAt',
      order: 'descend',
    },
  };

  transcribeJob = (id) => {
    this.props.history.push(`/job/transcribe/${id}`);
  };

  unlockJob = (id) => {
    this.props.actions.unlockJob(id);
    this.setState({
      jobUnlocking: true,
    });
  };

  reloadData = (pagination, filters, sorter) => {
    this.props.actions.fetchJobs(
      pagination.pageSize,
      pagination.current,
      filters,
      sorter,
      'acquired',
    );
  };

  componentDidMount() {
    this.reloadData(
      {
        pageSize: this.state.pageSize,
        current: 1,
      },
      {},
      this.state.sorter,
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.jobUnlocking && !nextProps.job.unlockJobPending) {
      if (!nextProps.job.unlockJobError && nextProps.job.currentJob.id) {
        this.setState(
          {
            jobUnlocking: false,
          },
          () => {
            this.reloadData(
              {
                pageSize: this.state.pageSize,
                current: 1,
              },
              {},
              this.state.sorter,
            );
          },
        );
      } else {
        message.error('Job could not be unlocked');
        this.setState({
          jobUnlocking: false,
        });
      }

      return false;
    }

    return true;
  }

  render() {
    return (
      <div className="job-acquired-jobs">
        <h1>Acquired jobs</h1>
        <JobsTable
          jobs={this.props.job.jobs}
          totalJobs={this.props.job.totalJobs || 0}
          cols={[
            'voucher.voucherId',
            'createdAt',
            'lockedAt',
            'expiresAt',
            'status',
            'voucher.supplierId',
            'action',
          ]}
          dataReloader={this.reloadData}
          dataLoading={this.props.job.fetchAcquiredJobsPending}
          actionRenderer={(text, record) => (
            <Button.Group>
              <Button onClick={() => this.transcribeJob(record.id)}>
                <Icon type="solution" /> Transcribe
              </Button>
              {record.status === 'STATUS_LOCKED' ? (
                <Button onClick={() => this.unlockJob(record.id)}>
                  <Icon type="unlock" /> Release
                </Button>
              ) : (
                ''
              )}
            </Button.Group>
          )}
          suppliers={this.props.supplier.suppliers}
          defaultPageSize={this.state.pageSize}
          defaultSorter={this.state.sorter}
          statuses={this.props.job.statuses}
        />
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    job: state.job,
    supplier: state.supplier,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AcquiredJobs);
