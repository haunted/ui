/* eslint-disable react/require-default-props */
// TODO Add prop-types

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import _ from 'lodash';
import { Table, Icon, Select, Button } from 'antd';
import { defaultDateFormat } from '../common/constants';

const { Option } = Select;

export default class JobsTable extends Component {
  static propTypes = {
    jobs: PropTypes.array,
    totalJobs: PropTypes.number.isRequired,
    cols: PropTypes.array.isRequired,
    dataReloader: PropTypes.func.isRequired,
    dataLoading: PropTypes.bool.isRequired,
    actionRenderer: PropTypes.func,
    suppliers: PropTypes.object,
    defaultPageSize: PropTypes.number,
    defaultSorter: PropTypes.object,
    statuses: PropTypes.object,
  };

  state = {
    pagination: {
      current: 1,
      pageSize: 20,
    },
    filters: {},
    sorter: {
      field: 'createdAt',
      order: 'descend',
    },
  };

  handleTableChange = (pagination, filters, sorter) => {
    const pager = { ...this.state.pagination };
    const { sorter: { field: stateField, order: stateOrder } } = this.state;
    const field = _.get(sorter, 'field');
    const order = _.get(sorter, 'order');

    if (
      (_.isEmpty(sorter) || (field === stateField && order === stateOrder))
      && _.isEqual(filters, this.state.filters) && _.isEqual(pagination, this.state.pagination)) {
      return;
    }

    pager.current = pagination.current;

    this.setState(
      {
        pagination: pager,
        filters: Object.assign(this.state.filters, filters),
        sorter,
      },
      () => {
        this.props.dataReloader(
          this.state.pagination,
          this.state.filters,
          this.state.sorter,
        );
      },
    );
  };

  setSupplierFilter = (supplierId) => {
    const filters = { ...this.state.filters };

    if (!supplierId) {
      delete filters['voucher.supplierId'];
    } else {
      filters['voucher.supplierId'] = supplierId;
    }

    this.setState(
      {
        pagination: Object.assign(this.state.pagination, { current: 1 }),
        filters,
      },
      () => {
        this.props.dataReloader(
          this.state.pagination,
          this.state.filters,
          this.state.sorter,
        );
      },
    );
  };

  componentDidMount() {
    // TODO Refactor that
    this.setState({ // eslint-disable-line react/no-did-mount-set-state
      pagination: Object.assign(this.state.pagination, {
        pageSize: this.props.defaultPageSize || this.state.pagination.pageSize,
      }),
      sorter: this.props.defaultSorter || this.state.sorter,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.totalJobs !== nextState.pagination.total) {
      const pagination = { ...nextState.pagination };

      // Read total count from server
      pagination.total = nextProps.totalJobs;
      this.setState({
        pagination,
      });

      return false;
    }

    return true;
  }

  render() {
    const cols = [];
    const { sorter: { field, order } } = this.state;
    const { statuses, suppliers } = this.props;
    const statusFilter = statuses && Object.keys(statuses).map(key => ({
      text: statuses[key],
      value: key,
    }));
    const supplierFilter = suppliers
      ? Object.keys(suppliers).map(key => ((
        <Option key={key} value={key}>
          {suppliers[key]}
        </Option>
      )))
      : [];

    this.props.cols.forEach((col) => {
      switch (col) {
        case 'voucher.voucherId':
          cols.push({
            title: 'Voucher ID',
            dataIndex: col,
          });
          break;
        case 'createdAt':
          cols.push({
            title: 'Created',
            dataIndex: col,
            render: datetime => moment(datetime).format(defaultDateFormat),
            defaultSortOrder: 'descend',
            sorter: true,
            sortOrder: field === col ? order : false,
          });
          break;
        case 'lockedAt':
          cols.push({
            title: 'Acquired at',
            dataIndex: col,
            render: (datetime) => {
              if (datetime === '1970-01-01T00:00:00Z') {
                return '-';
              }

              return moment(datetime).format(defaultDateFormat);
            },
            defaultSortOrder: 'descend',
            sorter: true,
            sortOrder: field === col ? order : false,
          });
          break;
        case 'expiresAt':
          cols.push({
            title: 'Expires at',
            dataIndex: col,
            render: (datetime) => {
              if (datetime === '1970-01-01T00:00:00Z') {
                return '-';
              }

              return moment(datetime).format(defaultDateFormat);
            },
            defaultSortOrder: 'descend',
            sorter: true,
            sortOrder: field === col ? order : false,
          });
          break;
        case 'status':
          cols.push({
            title: 'Status',
            dataIndex: col,
            filters: statusFilter,
            render: text =>
              (this.props.statuses && this.props.statuses[text]
                ? this.props.statuses[text]
                : text),
          });
          break;
        case 'username':
          cols.push({
            title: 'Locked by user',
            dataIndex: col,
          });
          break;
        case 'voucher.supplierId':
          cols.push({
            title: 'Supplier',
            dataIndex: col,
            filterIcon: (
              <Icon
                type="filter"
                style={{
                  color: this.state.filters[col] ? '#108ee9' : '#aaa',
                }}
              />
            ),
            filterDropdown: (
              <div className="supplier-filter-dropdown">
                <Select
                  showSearch
                  style={{ width: '100%' }}
                  placeholder="Filter by supplier"
                  onChange={this.setSupplierFilter}
                  filterOption={(input, option) =>
                    option.props.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                  allowClear
                >
                  {supplierFilter.length > 0 ? supplierFilter : ''}
                </Select>
              </div>
            ),
            render: text =>
              (this.props.suppliers && this.props.suppliers[text]
                ? this.props.suppliers[text]
                : `UNKNOWN (${text})`),
          });
          break;
        case 'action':
          cols.push({
            title: 'Action',
            key: col,
            render: this.props.actionRenderer || null,
          });
          break;
        default:
          break;
      }
    });

    return (
      <div className="job-jobs-table">
        <Button
          className="reload"
          onClick={() => this.props.dataReloader(
            this.state.pagination,
            this.state.filters,
            this.state.sorter,
          )}
        >
          <Icon type="reload" /> Refresh
        </Button>
        {this.props.dataLoading ? (
          <span>
            {' '}
            <Icon type="loading" /> Fetching data...
          </span>
        ) : (
          ''
        )}
        <Table
          dataSource={this.props.jobs}
          rowKey="id"
          columns={cols}
          pagination={this.state.pagination}
          onChange={this.handleTableChange}
        />
      </div>
    );
  }
}
