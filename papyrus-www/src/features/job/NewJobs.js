/* eslint-disable react/no-unused-state */
/* eslint-disable react/prop-types */
// TODO Check or refactor it and add prop-types

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button, Icon, Row, Col, message } from 'antd';
import * as actions from './redux/actions';
import JobsTable from './JobsTable';

export class NewJobs extends Component {
  static propTypes = {
    job: PropTypes.object.isRequired,
    supplier: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  state = {
    jobAcquiring: false, // TODO check it
    pageSize: 20,
    sorter: {
      field: 'createdAt',
      order: 'descend',
    },
  };

  acquireJob = (id) => {
    this.props.actions.acquireJob(id);
    this.setState({
      jobAcquiring: true,
    });
  };

  reloadData = (pagination, filters, sorter) => {
    this.props.actions.fetchJobs(
      pagination.pageSize,
      pagination.current,
      filters,
      sorter,
      'available',
    );
  };

  componentDidMount() {
    this.reloadData(
      {
        pageSize: this.state.pageSize,
        current: 1,
      },
      {},
      this.state.sorter,
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.jobAcquiring && !nextProps.job.acquireJobPending) {
      if (!nextProps.job.acquireJobError && nextProps.job.currentJob.id) {
        message.info('Job acquired');
        nextProps.history.push(`/job/transcribe/${nextProps.job.currentJob.id}`);
      } else {
        message.error('Job could not be acquired');
        this.setState({
          jobAcquiring: false,
        });
      }

      return false;
    }

    return true;
  }

  render() {
    return (
      <div className="job-new-jobs">
        <h1>New jobs</h1>
        {this.props.job &&
        this.props.job.jobs &&
        this.props.job.jobs.length > 0 ? (
          <Row>
            <Col offset={8} span={8}>
              <Button
                className="acquire-btn"
                type="primary"
                onClick={() => this.acquireJob(null)}
              >
                <Icon type="lock" /> Acquire a job to transcribe
              </Button>
            </Col>
          </Row>
        ) : (
          ''
        )}
        <JobsTable
          jobs={this.props.job.jobs}
          totalJobs={this.props.job.totalJobs || 0}
          cols={[
            'voucher.voucherId',
            'createdAt',
            'voucher.supplierId',
            'action',
          ]}
          dataReloader={this.reloadData}
          dataLoading={this.props.job.fetchJobsPending}
          actionRenderer={(text, record) => (
            <Button onClick={() => this.acquireJob(record.id)}>
              <Icon type="lock" /> Acquire
            </Button>
          )}
          suppliers={this.props.supplier.suppliers}
          defaultPageSize={this.state.pageSize}
          defaultSorter={this.state.sorter}
        />
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    job: state.job,
    supplier: state.supplier,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewJobs);
