/* eslint-disable react/no-unused-state */
/* eslint-disable react/prop-types */
// TODO Check or refactor it and add prop-types

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button, Icon, message } from 'antd';
import * as actions from './redux/actions';
import JobsTable from './JobsTable';

export class OwnApprovedJobs extends Component {
  static propTypes = {
    job: PropTypes.object.isRequired,
    supplier: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  state = {
    jobFetching: false, // TODO check it
    pageSize: 20,
    sorter: {
      field: 'createdAt',
      order: 'descend',
    },
  };

  reloadData = (pagination, filters, sorter) => {
    this.props.actions.fetchJobs(
      pagination.pageSize,
      pagination.current,
      filters,
      sorter,
      'own-approved',
    );
  };

  fetchJob = (id) => {
    this.props.actions.fetchJob(id);
    this.setState({
      jobFetching: true,
    });
  };

  componentDidMount() {
    this.reloadData(
      {
        pageSize: this.state.pageSize,
        current: 1,
      },
      {},
      this.state.sorter,
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.jobFetching && !nextProps.job.fetchJobPending) {
      if (!nextProps.job.fetchJobError && nextProps.job.currentJob.id) {
        nextProps.history.push(`/job/transcribe/${nextProps.job.currentJob.id}`);
      } else {
        message.error('Job could not be fetched');
        this.setState({
          jobFetching: false,
        });
      }

      return false;
    }

    return true;
  }

  render() {
    return (
      <div className="job-own-approved-jobs">
        <h1>Approved jobs</h1>
        <JobsTable
          jobs={this.props.job.jobs}
          totalJobs={this.props.job.totalJobs || 0}
          cols={[
            'voucher.voucherId',
            'createdAt',
            'lockedAt',
            'voucher.supplierId',
            'action',
          ]}
          dataReloader={this.reloadData}
          dataLoading={this.props.job.fetchOwnApprovedJobsPending}
          actionRenderer={(text, record) => (
            <Button.Group>
              <Button onClick={() => this.fetchJob(record.id)}>
                <Icon type="solution" /> View
              </Button>
            </Button.Group>
          )}
          suppliers={this.props.supplier.suppliers}
          defaultPageSize={this.state.pageSize}
          defaultSorter={this.state.sorter}
        />
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    job: state.job,
    supplier: state.supplier,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OwnApprovedJobs);
