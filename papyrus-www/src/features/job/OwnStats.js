import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Bar } from 'react-chartjs-2';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import * as actions from './redux/actions';
import { defaultDateFormat } from '../common/constants';

export class OwnStats extends Component {
  static propTypes = {
    job: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
  };

  reloadData = () => {
    this.props.actions.fetchStats();
  };

  componentDidMount() {
    this.reloadData();
  }

  render() {
    let data;

    if (this.props.job.ownJobStats && this.props.job.ownJobStats[0]) {
      data = {
        labels: ['Acquired', 'Submitted', 'Has feedback', 'Approved'],
        datasets: [
          {
            label:
              `${moment(this.props.job.ownJobStats[0].start).format(defaultDateFormat)
              } to ${
                moment(this.props.job.ownJobStats[0].end).format(defaultDateFormat)}`,
            data: [
              this.props.job.ownJobStats[0].acquired,
              this.props.job.ownJobStats[0].submitted,
              this.props.job.ownJobStats[0].hasFeedback,
              this.props.job.ownJobStats[0].approved,
            ],
            backgroundColor: [
              'rgba(255, 206, 86, 0.8)',
              'rgba(75, 192, 192, 0.8)',
              'rgba(255, 99, 132, 0.8)',
              'rgba(153, 102, 255, 0.8)',
            ],
            borderColor: [
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(255, 99, 132, 1)',
              'rgba(153, 102, 255, 1)',
            ],
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255, 159, 64, 0.8)',
            hoverBorderColor: 'rgba(255, 159, 64, 1)',
          },
        ],
      };
    }

    const options = {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              stepSize: 1,
            },
          },
        ],
      },
    };

    return (
      <div className="job-own-stats">
        {data
          ? <Bar data={data} options={options} width={100} height={50} />
          : ''
        }
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    job: state.job,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OwnStats);
