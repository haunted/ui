import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment-timezone';
import { Collapse, Row, Col } from 'antd';

const { Panel } = Collapse;

export default class FeedbackList extends Component {
  static propTypes = {
    feedbacks: PropTypes.array.isRequired,
    resellers: PropTypes.object.isRequired,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    onChange: () => {},
  };

  render() {
    return (
      <Collapse className="job-feedback-list" onChange={this.props.onChange}>
        {this.props.feedbacks.map((feedback) => {
          const header = moment(feedback.createdAt).format('YYYY-MM-DD HH:mm:ss');

          return (
            <Panel
              header={
                feedback.status === 'STATUS_UNREAD' ? (
                  <strong>{header}</strong>
                ) : (
                  header
                )
              }
              key={feedback.id}
            >
              <Row>
                <Col span={12}>
                  <dl>
                    <dt>Reseller</dt>
                    <dd>
                      {feedback.feedback.resellerId
                        ? this.props.resellers[feedback.feedback.resellerId]
                            .name
                        : '-'}
                    </dd>
                    <dt>Product ID</dt>
                    {/* TODO: Show product names instead of ID */}
                    <dd>
                      {feedback.feedback.productId
                        ? feedback.feedback.productId
                        : '-'}
                    </dd>
                    <dt>Traveler lastname</dt>
                    <dd>
                      {feedback.feedback.travelerLastName
                        ? feedback.feedback.travelerLastName
                        : '-'}
                    </dd>
                    <dt>Traveler firstname</dt>
                    <dd>
                      {feedback.feedback.travelerFirstName
                        ? feedback.feedback.travelerFirstName
                        : '-'}
                    </dd>
                    <dt>Number of adults</dt>
                    <dd>
                      {feedback.feedback.numberOfAdults
                        ? feedback.feedback.numberOfAdults
                        : '0'}
                    </dd>
                    <dt>Number of children</dt>
                    <dd>
                      {feedback.feedback.numberOfChildren
                        ? feedback.feedback.numberOfChildren
                        : '0'}
                    </dd>
                    <dt>Number of students</dt>
                    <dd>
                      {feedback.feedback.numberOfStudents
                        ? feedback.feedback.numberOfStudents
                        : '0'}
                    </dd>
                    <dt>Number of infants</dt>
                    <dd>
                      {feedback.feedback.numberOfInfants
                        ? feedback.feedback.numberOfInfants
                        : '0'}
                    </dd>
                    <dt>Number of seniors</dt>
                    <dd>
                      {feedback.feedback.numberOfSeniors
                        ? feedback.feedback.numberOfSeniors
                        : '0'}
                    </dd>
                    <dt>Reservation number</dt>
                    <dd>
                      {feedback.feedback.reservationNumber
                        ? feedback.feedback.reservationNumber
                        : '-'}
                    </dd>
                    <dt>Supplier confirmation number</dt>
                    <dd>
                      {feedback.feedback.supplierConfirmationNumber
                        ? feedback.feedback.supplierConfirmationNumber
                        : '-'}
                    </dd>
                    <dt>Booking reference number</dt>
                    <dd>
                      {feedback.feedback.bookingReferenceNumber
                        ? feedback.feedback.bookingReferenceNumber
                        : '-'}
                    </dd>
                    <dt>Total price</dt>
                    <dd>
                      {feedback.feedback.totalPrice
                        ? feedback.feedback.totalPrice
                        : '0'}
                    </dd>
                    <dt>Total price currency</dt>
                    <dd>
                      {feedback.feedback.totalPriceCurrency
                        ? feedback.feedback.totalPriceCurrency
                        : '-'}
                    </dd>
                  </dl>
                </Col>
                <Col span={12}>
                  <h5>Comment</h5>
                  {feedback.comment}
                </Col>
              </Row>
            </Panel>
          );
        })}
      </Collapse>
    );
  }
}
