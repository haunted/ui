import {
  NewJobs,
  AcquiredJobs,
  JobTranscription,
  SubmittedJobs,
  ApprovedJobs,
  JobReview,
  AllJobs,
  OwnApprovedJobs,
  OwnSubmittedJobs,
  OwnStats,
} from './';

export default {
  path: 'job',
  name: 'Job',
  childRoutes: [
    {
      path: 'new', name: 'New jobs', component: NewJobs, isIndex: true,
    },
    { path: 'acquired', name: 'Acquired jobs', component: AcquiredJobs },
    {
      path: 'transcribe/:jobID',
      name: 'Job transcription',
      component: JobTranscription,
    },
    { path: 'submitted', name: 'Submitted jobs', component: SubmittedJobs },
    { path: 'approved', name: 'Approved jobs', component: ApprovedJobs },
    { path: 'own-submitted', name: 'Submitter jobs', component: OwnSubmittedJobs },
    { path: 'review/:jobID', name: 'Job review', component: JobReview },
    { path: 'jobs', name: 'All jobs', component: AllJobs },
    {
      path: 'own-approved',
      name: 'Own approved jobs',
      component: OwnApprovedJobs,
    },
    { path: 'own-stats', name: 'Own stats', component: OwnStats },
  ],
};
