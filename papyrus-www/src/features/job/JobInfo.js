import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

export default class JobInfo extends Component {
  static propTypes = {
    job: PropTypes.object.isRequired,
    statuses: PropTypes.object.isRequired,
    suppliers: PropTypes.object.isRequired,
  };

  render() {
    const { job, statuses, suppliers } = this.props;
    const voucher = _.get(job, 'voucher', {});

    return (
      <dl className="job-job-info">
        <dt>Status</dt>
        <dd>{statuses[job.status]}</dd>
        <dt>Voucher ID:</dt>
        <dd>{voucher.voucherId}</dd>
        <dt>Supplier ID:</dt>
        <dd>{suppliers[voucher.supplierId]}</dd>
      </dl>
    );
  }
}
