import React from 'react';
import PropTypes from 'prop-types';
import { Field, reduxForm, SubmissionError } from 'redux-form';
import _ from 'lodash';
import {
  Icon,
  Button,
} from 'antd';
import { normalizeNumber } from '../../common/utils/formUtils';
import TextField from '../common/formElements/TextField';
import TextArea from '../common/formElements/TextArea';
import Select from '../common/formElements/Select';

const validateBeforeSubmit = (values) => {
  if (!values.resellerId) {
    throw new SubmissionError({
      resellerId: 'Reseller cannot be empty',
    });
  }
  if (!values.productId) {
    throw new SubmissionError({
      productId: 'Product cannot be empty',
    });
  }

  if (!values.bookingReferenceNumber) {
    throw new SubmissionError({
      bookingReferenceNumber: 'Reseller Ref cannot be empty',
    });
  }

  if (!values.travelerLastName) {
    throw new SubmissionError({
      travelerLastName: 'Last name cannot be empty',
    });
  }

  if (!values.travelerFirstName) {
    throw new SubmissionError({
      travelerFirstName: 'First name cannot be empty',
    });
  }
};

const prepareValues = (values) => {
  const preparedValues = { ...values };

  if (!values.numberOfAdults) {
    preparedValues.numberOfAdults = 0;
  }

  if (!values.numberOfChildren) {
    preparedValues.numberOfChildren = 0;
  }

  if (!values.numberOfStudents) {
    preparedValues.numberOfStudents = 0;
  }

  if (!values.numberOfInfants) {
    preparedValues.numberOfInfants = 0;
  }

  if (!values.numberOfSeniors) {
    preparedValues.numberOfSeniors = 0;
  }

  return preparedValues;
};

class TranscriberForm extends React.Component {
  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    onFilledResellerRef: PropTypes.func.isRequired,
    onSelectReseller: PropTypes.func.isRequired,
    transcribedVoucher: PropTypes.object.isRequired,
    resellers: PropTypes.object.isRequired,
    isDisabled: PropTypes.bool.isRequired,
    isVoucherRejected: PropTypes.bool.isRequired,
    showRejectReasons: PropTypes.bool.isRequired,
    showRejectButton: PropTypes.bool.isRequired,
    isProductsLoading: PropTypes.bool.isRequired,
    productItems: PropTypes.object,
    rejectReasons: PropTypes.object.isRequired,
    submitLabel: PropTypes.string.isRequired,
    useComment: PropTypes.bool,
    updateStickyHeight: PropTypes.func.isRequired,
  };

  static defaultProps = {
    useComment: false,
    productItems: {},
  };

  constructor(props) {
    super(props);

    this.state = {
      selectedProduct: props.transcribedVoucher.productId,
    };
  }

  renderResellerSelect = ({ input: { value, onChange }, meta }) => {
    const {
      resellers,
      isDisabled,
      onSelectReseller,
    } = this.props;
    const options = Object.keys(resellers).map(code => ({
      label: resellers[code].name,
      value: code,
    }));

    return (
      <Select
        value={value}
        placeholder={isDisabled ? '' : 'Select a reseller'}
        options={options}
        simpleValue
        onChange={(input) => {
          onChange(input);
          if (input) {
            onSelectReseller(input);
          }
        }}
        disabled={isDisabled}
        meta={meta}
      />
    );
  };

  renderProductSelect = (
    {
      input: { value, onChange },
      meta,
    },
    isProductsLoading,
  ) => {
    const { isDisabled, productItems, updateStickyHeight } = this.props;
    const options = Object.keys(productItems).length ? Object.keys(productItems).map((id) => {
      const productNames = _.get(productItems[id], 'map.from.productNames', []);

      return {
        label: `(${productItems[id].code}) ${productItems[id].name}`,
        value: id,
        aliases: productNames.join(','),
      };
    }) : [];

    return (
      <Select
        value={value}
        placeholder={
          !Object.keys(productItems).length
            ? 'No products found...'
            : 'Select a product'
        }
        options={options}
        filterOption={(option, filterString) => (
          option.label.toLowerCase()
            .indexOf(filterString.toLowerCase()) >= 0 ||
          option.aliases.toLowerCase()
            .indexOf(filterString.toLowerCase()) >= 0
        )}
        simpleValue
        onChange={(input) => {
          onChange(input);
          updateStickyHeight();
          this.setState({ selectedProduct: input });
        }}
        clearable={false}
        disabled={isDisabled || !Object.keys(productItems).length}
        isLoading={isProductsLoading}
        meta={meta}
      />
    );
  };

  renderQuantitySelects = () => {
    const { isDisabled } = this.props;

    return (
      <div className="visitors">
        <div className="field-group">
          <label>Adults</label>
          <Field
            name="numberOfAdults"
            component={TextField}
            min={0}
            disabled={isDisabled}
            normalize={normalizeNumber}
          />
        </div>
        <div className="field-group">
          <label>Children</label>
          <Field
            name="numberOfChildren"
            component={TextField}
            min={0}
            disabled={isDisabled}
            normalize={normalizeNumber}
          />
        </div>
        <div className="field-group">
          <label>Students</label>
          <Field
            name="numberOfStudents"
            component={TextField}
            min={0}
            disabled={isDisabled}
            normalize={normalizeNumber}
          />
        </div>
        <div className="field-group">
          <label>Infants</label>
          <Field
            name="numberOfInfants"
            component={TextField}
            min={0}
            disabled={isDisabled}
            normalize={normalizeNumber}
          />
        </div>
        <div className="field-group">
          <label>Seniors</label>
          <Field
            name="numberOfSeniors"
            component={TextField}
            min={0}
            disabled={isDisabled}
            normalize={normalizeNumber}
          />
        </div>
      </div>
    );
  };

  renderRejectReasonsSelect = ({ input: { value, onChange }, meta }) => {
    const {
      rejectReasons,
      isVoucherRejected,
    } = this.props;
    const options = Object.keys(rejectReasons).map(key => ({
      value: rejectReasons[key],
      label: rejectReasons[key],
    }));

    return (
      <Select
        value={value}
        placeholder={isVoucherRejected ? '' : 'Select a reject reason'}
        options={options}
        onChange={input => onChange(input && input.value)}
        disabled={isVoucherRejected}
        meta={meta}
      />
    );
  };

  render() {
    const {
      transcribedVoucher,
      isVoucherRejected,
      isDisabled,
      handleSubmit,
      useComment,
      submitLabel,
      showRejectReasons,
      showRejectButton,
      onFilledResellerRef,
      isProductsLoading,
    } = this.props;
    const { selectedProduct } = this.state;

    return (
      <div className="transcriber-form">
        <div>
          {!isVoucherRejected && (
            <div className="field-group">
              <label>Reseller:</label>
              <Field
                name="resellerId"
                component={this.renderResellerSelect}
              />
            </div>
          )}
          {!_.isEmpty(transcribedVoucher) && !isVoucherRejected && (
            <div className="field-group">
              <label>Product:</label>
              <Field
                name="productId"
                component={props => this.renderProductSelect(props, isProductsLoading)}
              />
            </div>
          )}
          {selectedProduct && !isVoucherRejected && (
            <div className="field-group">
              <label>Reseller Ref:</label>
              <Field
                name="bookingReferenceNumber"
                component={TextField}
                disabled={isDisabled}
                onBlur={onFilledResellerRef}
              />
            </div>
          )}
          <div className="full-name">
            {selectedProduct && !isVoucherRejected && (
              <div className="field-group">
                <label>Last name</label>
                <Field
                  name="travelerLastName"
                  component={TextField}
                  disabled={isDisabled}
                />
              </div>
            )}
            {selectedProduct && !isVoucherRejected && (
              <div className="field-group">
                <label>First name</label>
                <Field
                  name="travelerFirstName"
                  component={TextField}
                  disabled={isDisabled}
                />
              </div>
            )}
          </div>
          {selectedProduct && !isVoucherRejected && this.renderQuantitySelects()}
          {!isDisabled && !isVoucherRejected && useComment && (
            <div
              className={`field-group action-bar ${isDisabled || isVoucherRejected ? '' : 'show'}`}
            >
              <Button
                type="primary"
                size="large"
                htmlType="submit"
                onClick={handleSubmit((values) => {
                  validateBeforeSubmit(values);

                  const preparedValues = prepareValues(values);

                  return this.props.onSubmit({ values: preparedValues, type: 'approve' });
                })}
              >
                <Icon type="like" /> Approve
              </Button>
            </div>
          )}
          {!isDisabled && !isVoucherRejected && useComment && (
            <div className="field-group">
              <label>Comment:</label>
              <Field
                name="comment"
                component={TextArea}
              />
            </div>
          )}
          {!isDisabled && !isVoucherRejected && (
            <div className="field-group">
              <Button
                type="primary"
                htmlType="submit"
                onClick={handleSubmit((values) => {
                  validateBeforeSubmit(values);

                  const preparedValues = prepareValues(values);

                  return this.props.onSubmit({ values: preparedValues });
                })}
                size="large"
                className="transcriber-form-button"
              >
                {submitLabel}
              </Button>
            </div>
          )}
          {showRejectReasons && (
            <div className="field-group">
              <label>Reject reason:</label>
              <Field
                name="reason"
                component={this.renderRejectReasonsSelect}
              />
            </div>
          )}
          {showRejectButton && (
            <div className="field-group">
              <Button
                type="danger"
                htmlType="submit"
                onClick={handleSubmit((values) => {
                  if (!values.reason && !useComment) {
                    throw new SubmissionError({
                      reason: 'Reject reason cannot be empty',
                    });
                  }

                  return this.props.onSubmit({ values, type: 'reject' });
                })}
                size="large"
                className="transcriber-form-button submit2"
              >
                Reject
              </Button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

const initialValues = {
  resellerId: '',
  productId: '',
  bookingReferenceNumber: '',
  travelerLastName: '',
  travelerFirstName: '',
  numberOfAdults: 0,
  numberOfStudents: 0,
  numberOfInfants: 0,
  numberOfChildren: 0,
  numberOfSeniors: 0,
  comment: '',
  reason: '',
};

export default reduxForm({
  form: 'transcriberForm',
  initialValues,
})(TranscriberForm);
