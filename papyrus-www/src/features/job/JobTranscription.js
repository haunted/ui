import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Form, Table, Tabs } from 'antd';
import moment from 'moment-timezone';
import _ from 'lodash';
import * as actions from './redux/actions';
import * as voucherActions from '../voucher/redux/actions';
import TranscriberFormWrapper from './TranscriberFormWrapper';
import JobInfo from './JobInfo';
import FeedbackList from './FeedbackList';

const { TabPane } = Tabs;

export class JobTranscription extends Component {
  static propTypes = {
    job: PropTypes.object.isRequired,
    supplier: PropTypes.object.isRequired,
    voucherActions: PropTypes.object.isRequired,
    reseller: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.actions.clearCurrentJob();
  }

  componentDidMount() {
    const { params } = this.props.match;

    if (params.jobID) {
      this.props.actions.fetchJob(params.jobID);
      this.props.actions.fetchFeedbacksByJob(params.jobID);
      this.props.actions.fetchLogsByJob(params.jobID);
    }
  }

  readFeedback = (key) => {
    this.props.job.currentJobFeedbacks.forEach((feedback, index) => {
      if (feedback.id === key && feedback.status === 'STATUS_UNREAD') {
        this.props.actions.setFeedbackAsRead(key);
        this.props.job.currentJobFeedbacks[index].status = 'STATUS_READ';
      }
    });
  };

  submitJob = (values) => {
    const currentJob = _.get(this.props.job, 'currentJob');

    if (currentJob) {
      const preparedValues = { ...values };

      delete preparedValues.reason;
      delete preparedValues.comment;
      this.props.actions.submitJob(currentJob.id, preparedValues);
    }
  };

  rejectJob = (values) => {
    const currentJob = _.get(this.props.job, 'currentJob');

    if (currentJob) {
      this.props.voucherActions.rejectVoucher(
        currentJob,
        values,
      );
    }
  };

  render() {
    const logColumns = [
      {
        title: 'Log ID',
        dataIndex: 'id',
      },
      {
        title: 'Event',
        dataIndex: 'event',
        render: text => this.props.job.logEvents[text],
      },
      {
        title: 'Created At',
        dataIndex: 'createdAt',
        render: datetime => moment(datetime).format('YYYY-MM-DD HH:mm:ss'),
        sorter: (a, b) => {
          if (a.createdAt < b.createdAt) {
            return -1;
          }
          if (a.createdAt > b.createdAt) {
            return 1;
          }

          return 0;
        },
        // TODO: Show correct value for "By user"
        // }, {
        //   title: 'By user',
        //   dataIndex: 'voucher.supplierId',
      },
    ];

    return (
      <Tabs className="job-job-transcription" defaultActiveKey="transcription">
        <TabPane tab="Transcription" key="transcription">
          <h3>Voucher</h3>
          {this.props.job.currentJob && this.props.supplier.suppliers && (
            <div>
              <JobInfo
                job={this.props.job.currentJob}
                statuses={this.props.job.statuses}
                suppliers={this.props.supplier.suppliers}
              />
              <TranscriberFormWrapper submitJob={this.submitJob} rejectJob={this.rejectJob} />
            </div>
          )}
        </TabPane>
        {this.props.job.currentJobFeedbacks &&
        this.props.job.currentJobFeedbacks.length > 0 ? (
          <TabPane tab="Feedbacks" key="feedbacks">
            <FeedbackList
              feedbacks={this.props.job.currentJobFeedbacks}
              onChange={this.readFeedback}
              resellers={this.props.reseller.resellers}
            />
          </TabPane>
        ) : (
          ''
        )}
        {this.props.job.currentJobLogs &&
        this.props.job.currentJobLogs.length > 0 ? (
          <TabPane tab="Logs" key="logs">
            <Table
              dataSource={this.props.job.currentJobLogs}
              rowKey="id"
              columns={logColumns}
              pagination={false}
            />
          </TabPane>
        ) : (
          ''
        )}
      </Tabs>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    job: state.job,
    supplier: state.supplier,
    reseller: state.reseller,
    currentUserGroups: state.user.currentUserGroups,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
    voucherActions: bindActionCreators({ ...voucherActions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Form.create()(JobTranscription));
