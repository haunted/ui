import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Magnifier from 'react-magnifier';
import Select from 'react-select';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { change } from 'redux-form';
import Sticky from 'react-stickynode';


import TranscriberForm from './TranscriberForm';
import * as ticketActions from '../ticket/redux/actions';
import * as actions from './redux/actions';
import * as voucherActions from '../voucher/redux/actions';
import * as supplierActions from '../supplier/redux/actions';
import * as resellerActions from '../reseller/redux/actions';
import {
  isApproved,
  isSubmitted,
  isRejected,
  isHasFeedback,
  isInReview,
  isLocked,
} from '../../common/utils/statusUtils';
import { isTranscriberSupervisor } from '../../common/utils/authUtils';

class TranscriberFormWrapper extends React.Component {
  static propTypes = {
    submitLabel: PropTypes.string,
    useComment: PropTypes.bool,
    supplier: PropTypes.object.isRequired,
    voucher: PropTypes.object,
    job: PropTypes.object.isRequired,
    resellers: PropTypes.object,
    supplierActions: PropTypes.object.isRequired,
    ticketActions: PropTypes.object.isRequired,
    rejectReasons: PropTypes.object,
    approveJob: PropTypes.func,
    submitJob: PropTypes.func.isRequired,
    rejectJob: PropTypes.func.isRequired,
    currentUserGroups: PropTypes.array,
  };

  static defaultProps = {
    submitLabel: 'Submit',
    useComment: false,
    approveJob: null,
    rejectReasons: null,
    voucher: null,
    resellers: null,
    currentUserGroups: [],
  };

  constructor() {
    super();

    this.updateStickyHeight = this.updateStickyHeight.bind(this);

    this.state = {
      currentImageUrl: null,
      selectedReseller: {},
    };
  }

  // A workaround to make Sticky handle dynamically changed content
  updateStickyHeight() {
    window.requestAnimationFrame(() => {
      if (this.sticky) {
        this.sticky.updateInitialDimension();
        this.sticky.update();
      }
    });
  }

  onSelectReseller = (value) => {
    const { job, resellers } = this.props;

    this.setState(
      {
        selectedReseller: resellers[value],
      },
      () => {
        this.props.supplierActions.fetchCatalogItemsByReseller(job.voucher.supplierId, value);
        this.updateStickyHeight();
      },
    );
  };

  setCurrentImageUrl = ({ value }) => {
    this.setState({
      currentImageUrl: value,
    }, this.updateStickyHeight);
  };

  renderImagesSelect = (imageUrl) => {
    const { job } = this.props;
    const { selectedReseller } = this.state;
    const imageUrls = job.voucher.imageUrls || [];
    const exampleUrls = selectedReseller.exampleUrls || [];
    const options = imageUrls.length
      ? imageUrls.map((url, index) => ({
        label: `Voucher image ${index + 1} of ${imageUrls.length}`,
        value: url,
      }))
      : exampleUrls.map((url, index) => ({
        label: `Example voucher image ${index + 1} of ${exampleUrls.length}`,
        value: url,
      }));

    return (
      <div>
        <label>Select a voucher image:</label>
        <Select
          value={imageUrl}
          placeholder="Select a voucher image"
          options={options}
          onChange={this.setCurrentImageUrl}
          clearable={false}
          searchable={false}
        />
      </div>
    );
  };

  onFilledResellerRef = (e) => {
    const resellerRef = e.target.value;

    if (resellerRef) {
      this.props.ticketActions.fetchTicketsByResellerReference(
        this.props.job.voucher.supplierId,
        resellerRef,
      );
    }
  };

  handleSubmit = ({ values, type }) => {
    const { approveJob, submitJob, rejectJob } = this.props;

    switch (type) {
      case 'approve':
        return approveJob();
      case 'reject':
        return rejectJob(values);
      default:
        return submitJob(values);
    }
  };

  showRejectReasons = () => {
    const {
      job,
      currentUserGroups,
      voucher,
    } = this.props;

    const isVoucherRejected = isRejected(voucher.status);

    if (isTranscriberSupervisor(currentUserGroups)) {
      if (isInReview(job.status) && !isRejected(voucher.status)) {
        return false;
      }

      return !isApproved(job.status) || isRejected(job.status);
    }

    return isVoucherRejected || isLocked(job.status);
  };

  showRejectButton = () => {
    const {
      job,
      currentUserGroups,
      voucher,
    } = this.props;

    if (isTranscriberSupervisor(currentUserGroups)) {
      if (isInReview(job.status) && !isRejected(voucher.status)) {
        return false;
      }

      return !isApproved(job.status) && !isRejected(job.status);
    }

    return (!isSubmitted(job.status)
        && !isInReview(job.status)
        && !isApproved(job.status)
        && !isHasFeedback(job.status)
    )
      || isLocked(job.status);
  };

  render() {
    const {
      job,
      resellers,
      currentUserGroups,
      supplier,
      voucher,
      submitLabel,
      useComment,
      rejectReasons,
    } = this.props;
    const { currentImageUrl, selectedReseller } = this.state;

    if (
      !job.voucher
      || _.isEmpty(supplier.suppliers)
      || _.isEmpty(resellers)
      || _.isEmpty(voucher)
      || _.isEmpty(rejectReasons)
    ) {
      return <div />;
    }

    const productItems = supplier.resellerItems ?
      supplier.resellerItems[job.transcribedVoucher.resellerId || selectedReseller.code] : {};

    const imageUrl = currentImageUrl || job.voucher.imageUrls[0];
    const isDisabled = isTranscriberSupervisor(currentUserGroups)
      ? isApproved(job.status)
      : (
        isApproved(job.status)
        || isSubmitted(job.status)
        || isInReview(job.status)
        || isHasFeedback(job.status)
      );
    const isVoucherRejected = isRejected(voucher.status);

    return (
      <div className="transcriber-form-container">
        <div className="image-container-col">
          <Sticky innerZ={1}>
            <div className="image-select">
              {this.renderImagesSelect(imageUrl)}
            </div>
          </Sticky>

          {imageUrl && (
            <Magnifier
              src={imageUrl}
              width="100%"
              mgShape="square"
              mgWidth={250}
              mgHeight={250}
            />
          )}
        </div>
        <div className="transcriber-form-col">
          <Sticky
            ref={(node) => { this.sticky = node; }}
            bottomBoundary=".transcriber-form-container"
          >
            <TranscriberForm
              transcribedVoucher={job.transcribedVoucher}
              resellers={resellers}
              isDisabled={isDisabled}
              onSelectReseller={this.onSelectReseller}
              productItems={productItems}
              isVoucherRejected={isVoucherRejected}
              onFilledResellerRef={this.onFilledResellerRef}
              onSubmit={this.handleSubmit}
              submitLabel={submitLabel}
              useComment={useComment}
              rejectReasons={rejectReasons}
              showRejectReasons={this.showRejectReasons()}
              showRejectButton={this.showRejectButton()}
              isProductsLoading={supplier.fetchCatalogItemsByResellerPending}
              updateStickyHeight={this.updateStickyHeight}
              initialValues={job.transcribedVoucher}
            />
          </Sticky>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  job: state.job.currentJob,
  supplier: state.supplier,
  resellers: state.reseller.resellers,
  voucher: state.voucher.currentVoucher,
  rejectReasons: state.voucher.rejectReasons,
  currentUserGroups: state.user.currentUserGroups,
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators({ ...actions }, dispatch),
  formActions: bindActionCreators({ change }, dispatch),
  supplierActions: bindActionCreators({ ...supplierActions }, dispatch),
  ticketActions: bindActionCreators({ ...ticketActions }, dispatch),
  voucherActions: bindActionCreators({ ...voucherActions }, dispatch),
  resellerActions: bindActionCreators({ ...resellerActions }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(TranscriberFormWrapper);

