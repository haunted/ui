import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Button } from 'antd';
import * as actions from './redux/actions';
import JobsTable from './JobsTable';

export class OwnSubmittedJobs extends Component {
  static propTypes = {
    job: PropTypes.object.isRequired,
    supplier: PropTypes.object.isRequired,
    actions: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  state = {
    pageSize: 20,
    sorter: {
      field: 'createdAt',
      order: 'descend',
    },
  };

  review = (id) => {
    this.props.history.push(`/job/review/${id}`);
  };

  reloadData = (pagination, filters, sorter) => {
    this.props.actions.fetchJobs(
      pagination.pageSize,
      pagination.current,
      filters,
      sorter,
      'submitted',
    );
  };

  componentDidMount() {
    this.reloadData(
      {
        pageSize: this.state.pageSize,
        current: 1,
      },
      {},
      this.state.sorter,
    );
  }

  render() {
    return (
      <div className="job-submitted-jobs">
        <h1>Submitted jobs</h1>
        <JobsTable
          jobs={this.props.job.jobs}
          totalJobs={this.props.job.totalJobs || 0}
          cols={['voucher.voucherId', 'createdAt', 'voucher.supplierId', 'action']}
          dataReloader={this.reloadData}
          actionRenderer={(text, record) => (
            <Button onClick={() => this.review(record.id)}>Review</Button>
          )}
          dataLoading={this.props.job.fetchJobsPending}
          suppliers={this.props.supplier.suppliers}
          defaultPageSize={this.state.pageSize}
          defaultSorter={this.state.sorter}
        />
      </div>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    job: state.job,
    supplier: state.supplier,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({ ...actions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OwnSubmittedJobs);
