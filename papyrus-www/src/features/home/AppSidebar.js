/* eslint-disable react/require-default-props */
// TODO Add default props

import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import PropTypes from 'prop-types';
import history from '../../common/history';
import { isTranscriberSupervisor } from '../../common/utils/authUtils';

const { Sider } = Layout;
const MenuItemGroup = Menu.ItemGroup;

export default class AppSidebar extends Component {
  static propTypes = {
    collapsed: PropTypes.bool.isRequired,
    currentUserGroups: PropTypes.array.isRequired,
    selectedMenu: PropTypes.string,
  };

  navigate = (item) => {
    history.push(item.key);
  };

  render() {
    const selectedMenu = this.props.selectedMenu || '/job/new';

    return (
      <Sider
        trigger={null}
        collapsible
        collapsed={this.props.collapsed}
        className="home-app-sidebar"
      >
        <div className="logo" />
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={['/job/new']}
          selectedKeys={[selectedMenu]}
          onClick={this.navigate}
        >
          <MenuItemGroup key="transcriber" title="Transcription jobs">
            <Menu.Item key="/job/new">
              <span>Available</span>
            </Menu.Item>
            <Menu.Item key="/job/acquired">
              <span>Acquired</span>
            </Menu.Item>
            {!isTranscriberSupervisor(this.props.currentUserGroups) && (
              <Menu.Item key="/job/own-submitted">
                <span>Submitted</span>
              </Menu.Item>
            )}
            <Menu.Item key="/job/own-approved">
              <span>Approved</span>
            </Menu.Item>
            <Menu.Item key="/job/own-stats">
              <span>Stats</span>
            </Menu.Item>
          </MenuItemGroup>
          {isTranscriberSupervisor(this.props.currentUserGroups) && (
            <MenuItemGroup key="supervisor" title="Supervisor jobs">
              <Menu.Item key="/job/submitted">
                <span>In Review</span>
              </Menu.Item>
              <Menu.Item key="/job/approved">
                <span>Approved</span>
              </Menu.Item>
              <Menu.Item key="/job/jobs">
                <span>All jobs</span>
              </Menu.Item>
            </MenuItemGroup>
          )}
        </Menu>
      </Sider>
    );
  }
}
