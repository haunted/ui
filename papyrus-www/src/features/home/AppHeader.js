/* eslint-disable jsx-a11y/href-no-hash */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Layout, Menu, Dropdown, Icon, Row, Col } from 'antd';

const { Header } = Layout;

export default class AppHeader extends Component {
  static propTypes = {
    toggle: PropTypes.func.isRequired,
    collapsed: PropTypes.bool.isRequired,
    currentUser: PropTypes.object.isRequired,
    onLogout: PropTypes.func.isRequired,
  };

  render() {
    const menu = (
      <Menu>
        <Menu.Item>
          <a tabIndex={0} role="button" onClick={this.props.onLogout}>Logout</a>
        </Menu.Item>
      </Menu>
    );

    return (
      <Header className="app-header" style={{ background: '#fff', padding: 0 }}>
        <Row type="flex" justify="end" gutter={16}>
          <Col span={6}>
            <Icon
              className="trigger"
              type={this.props.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.props.toggle}
            />
          </Col>
          <Col className="mini-menu" span={6} offset={12}>
            <Dropdown overlay={menu}>
              <a className="ant-dropdown-link" href="#">
                {this.props.currentUser.account.givenName}{' '}
                {this.props.currentUser.account.surname} <Icon type="down" />
              </a>
            </Dropdown>
          </Col>
        </Row>
      </Header>
    );
  }
}
