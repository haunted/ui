/* eslint-disable react/prop-types */
// TODO Add prop-types

import 'react-select/dist/react-select.css';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Layout, message } from 'antd';
import history from '../../common/history';
import * as userActions from '../user/redux/actions';
import * as supplierActions from '../supplier/redux/actions';
import AppHeader from './AppHeader';
import AppSidebar from './AppSidebar';

const { Content } = Layout;

export class App extends Component {
  constructor() {
    super();

    this.toggle = this.toggle.bind(this);
    this.onLogout = this.onLogout.bind(this);
  }
  static propTypes = {
    user: PropTypes.object,
    supplier: PropTypes.object.isRequired,
    userActions: PropTypes.object.isRequired,
    supplierActions: PropTypes.object.isRequired,
    children: PropTypes.node,
  };

  static defaultProps = {
    children: 'No content.',
    user: {},
  };

  state = {
    collapsed: false,
  };

  toggle() {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  componentWillMount() {
    this.props.userActions.isLoggedIn();
  }

  shouldComponentUpdate(nextProps) {
    if (!nextProps.user.auth) {
      history.push('/login');

      return false;
    }

    if (nextProps.user.auth && !nextProps.user.currentUser) {
      if (nextProps.user.fetchCurrentUserError) {
        message.error('Could not load user');
      } else if (!nextProps.user.fetchCurrentUserPending) {
        nextProps.userActions.fetchCurrentUser();
        message.info('Loading user...');
      }

      return false;
    }

    if (
      nextProps.user.auth &&
      nextProps.user.currentUser &&
      (!nextProps.user.currentUserGroups ||
        nextProps.user.currentUserGroups.length === 0)
    ) {
      if (nextProps.user.fetchUserGroupsError) {
        message.error('Could not load user groups');
      } else if (!nextProps.user.fetchUserGroupsPending) {
        nextProps.userActions.fetchUserGroups(nextProps.user.currentUser.account.id);
        message.info('Loading user groups...');
      }

      return false;
    }

    if (nextProps.user.auth && !nextProps.supplier.suppliers) {
      if (nextProps.supplier.fetchSuppliersError) {
        message.error('Could not load suppliers');
      } else if (!nextProps.supplier.fetchSuppliersPending) {
        nextProps.supplierActions.fetchSuppliers();
        message.info('Loading suppliers...');
      }

      return false;
    }

    return true;
  }

  onLogout() {
    this.props.userActions.logout();
    history.push('/login');
  }

  render() {
    return (
      <Layout className="ant-layout-has-sider app">
        <AppSidebar
          collapsed={this.state.collapsed}
          currentUserGroups={this.props.user.currentUserGroups || []}
          selectedMenu={this.props.location.pathname}
        />
        <Layout>
          {this.props.user.currentUser ? (
            <AppHeader
              toggle={this.toggle}
              collapsed={this.state.collapsed}
              currentUser={this.props.user.currentUser}
              onLogout={this.onLogout}
            />
          ) : (
            ''
          )}
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              background: '#fff',
              minHeight: 280,
            }}
          >
            {this.props.children}
          </Content>
        </Layout>
      </Layout>
    );
  }
}

/* istanbul ignore next */
function mapStateToProps(state) {
  return {
    user: state.user,
    supplier: state.supplier,
    reseller: state.reseller,
  };
}

/* istanbul ignore next */
function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators({ ...userActions }, dispatch),
    supplierActions: bindActionCreators({ ...supplierActions }, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
