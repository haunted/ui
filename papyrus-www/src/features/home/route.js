import { NewJobs } from '../job';

export default {
  path: '/',
  name: 'Home',
  childRoutes: [
    {
      path: '/', name: 'New jobs', component: NewJobs, isIndex: true,
    },
  ],
};
