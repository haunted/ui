import { call, put, takeLatest } from 'redux-saga/effects';
import _ from 'lodash';
import { change } from 'redux-form';
import api from '../../../common/api';
import {
  TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_BEGIN,
  TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_SUCCESS,
  TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_FAILURE,
  TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_DISMISS_ERROR,
} from './constants';
import { defaultQuantityCounts } from '../../../common/constants';

export function fetchTicketsByResellerReference(supplierCode, resellerRef) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_BEGIN,
    supplierCode,
    resellerRef,
  };
}

export function dismissFetchTicketsByResellerReferenceError() {
  return {
    type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on TICKET_FETCH_TICKETS_BY_REFERENCE_BEGIN actions
export function* doFetchTicketsByResellerReference(args) {
  // If necessary, use argument to receive the begin action with parameters.
  try {
    const response = yield call(
      api.get,
      `/tickets/search/supplier/${
        args.supplierCode
      }/reseller-ref/${
        args.resellerRef}`,
    );

    const tickets = _.get(response.data, 'tickets', []);
    const foundTickets = args.resellerRef && tickets
      ? tickets.filter(d => d.resellerRef === args.resellerRef)
      : [];
    const foundTicket = foundTickets.length ? foundTickets[0] : {};

    const travelerCounts = foundTicket.travelers
      ? foundTicket.travelers.reduce((acc, key) => {
        if (acc[key.ageBand]) {
          acc[key.ageBand] += 1;
        } else {
          acc[key.ageBand] = 1;
        }

        return acc;
      }, defaultQuantityCounts)
      : defaultQuantityCounts;

    yield put(change('transcriberForm', 'numberOfAdults', travelerCounts.AGE_BAND_ADULT));
    yield put(change('transcriberForm', 'numberOfChildren', travelerCounts.AGE_BAND_CHILD));
    yield put(change('transcriberForm', 'numberOfStudents', travelerCounts.AGE_BAND_STUDENT));
    yield put(change('transcriberForm', 'numberOfInfants', travelerCounts.AGE_BAND_INFANT));
    yield put(change('transcriberForm', 'numberOfSeniors', travelerCounts.AGE_BAND_SENIOR));
    if (foundTicket.travelers && foundTicket.travelers.length) {
      yield put(change(
        'transcriberForm',
        'travelerFirstName',
        foundTicket.travelers[0].name.given || '',
      ));
      yield put(change(
        'transcriberForm',
        'travelerLastName',
        foundTicket.travelers[0].name.family || '',
      ));
    }

    yield put({
      type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_SUCCESS,
      data: {
        travelers: foundTicket.travelers,
      },
    });
  } catch (err) {
    yield put({
      type: TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchTicketsByResellerReference() {
  yield takeLatest(
    TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_BEGIN,
    doFetchTicketsByResellerReference,
  );
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_BEGIN:
      return {
        ...state,
        fetchTicketsByResellerReferencePending: true,
        fetchTicketsByResellerReferenceError: null,
      };

    case TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_SUCCESS:
      return {
        ...state,
        fetchTicketsByResellerReferencePending: false,
        fetchTicketsByResellerReferenceError: null,
      };

    case TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_FAILURE:
      return {
        ...state,
        fetchTicketsByResellerReferencePending: false,
        fetchTicketsByResellerReferenceError: action.data.error,
      };

    case TICKET_FETCH_TICKETS_BY_RESELLER_REFERENCE_DISMISS_ERROR:
      return {
        ...state,
        fetchTicketsByResellerReferenceError: null,
      };

    default:
      return state;
  }
}
