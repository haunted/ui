import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  RESELLER_FETCH_RESELLERS_BEGIN,
  RESELLER_FETCH_RESELLERS_SUCCESS,
  RESELLER_FETCH_RESELLERS_FAILURE,
  RESELLER_FETCH_RESELLERS_DISMISS_ERROR,
} from './constants';

export function fetchResellers(supplierId) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: RESELLER_FETCH_RESELLERS_BEGIN,
    supplierId,
  };
}

export function dismissFetchResellersError() {
  return {
    type: RESELLER_FETCH_RESELLERS_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on RESELLER_FETCH_RESELLERS_BEGIN actions
export function* doFetchResellers({ supplierId }) {
  // If necessary, use argument to receive the begin action with parameters.

  try {
    yield put({
      type: RESELLER_FETCH_RESELLERS_SUCCESS,
      data: yield call(api.get, `/suppliers/${supplierId}/resellers`),
    });
  } catch (err) {
    yield put({
      type: RESELLER_FETCH_RESELLERS_FAILURE,
      data: { error: err },
    });
  }
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchResellers() {
  yield takeLatest(RESELLER_FETCH_RESELLERS_BEGIN, doFetchResellers);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case RESELLER_FETCH_RESELLERS_BEGIN:
      return {
        ...state,
        fetchResellersPending: true,
        fetchResellersError: null,
        resellers: {},
      };

    case RESELLER_FETCH_RESELLERS_SUCCESS: {
      const resellers = {};

      action.data.data.resellers.forEach((item) => {
        resellers[item.code] = item;
      });

      return {
        ...state,
        fetchResellersPending: false,
        fetchResellersError: null,
        resellers,
      };
    }

    case RESELLER_FETCH_RESELLERS_FAILURE:
      return {
        ...state,
        fetchResellersPending: false,
        fetchResellersError: action.data.error,
        resellers: {},
      };

    case RESELLER_FETCH_RESELLERS_DISMISS_ERROR:
      return {
        ...state,
        fetchResellersError: null,
      };

    default:
      return state;
  }
}
