import initialState from './initialState';
import { reducer as fetchSuppliersReducer } from './fetchSuppliers';
import { reducer as fetchCatalogItemsByResellerReducer } from './fetchCatalogItemsByReseller';

const reducers = [fetchSuppliersReducer, fetchCatalogItemsByResellerReducer];

export default function reducer(state = initialState, action) {
  let newState;

  switch (action.type) {
    // Handle cross-topic actions here
    default:
      newState = state;
      break;
  }

  return reducers.reduce((s, r) => r(s, action), newState);
}
