import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN,
  SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS,
  SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_FAILURE,
  SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_DISMISS_ERROR,
} from './constants';

export function fetchCatalogItemsByReseller(supplierCode, resellerCode) {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN,
    supplierCode,
    resellerCode,
  };
}

export function dismissFetchCatalogItemsByResellerError() {
  return {
    type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN actions
export function* doFetchCatalogItemsByReseller(args) {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    // Do Ajax call or other async request here. delay(20) is just a placeholder.
    res = yield call(
      api.get,
      `/suppliers/${
        args.supplierCode
      }/resellers/${
        args.resellerCode
      }/items`,
    );
  } catch (err) {
    yield put({
      type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS,
    data: res,
    resellerCode: args.resellerCode,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchCatalogItemsByReseller() {
  yield takeLatest(
    SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN,
    doFetchCatalogItemsByReseller,
  );
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_BEGIN:
      return {
        ...state,
        fetchCatalogItemsByResellerPending: true,
        fetchCatalogItemsByResellerError: null,
      };

    case SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_SUCCESS: {
      const catalogItems = {};

      action.data.data.catalogItems.forEach((item) => {
        catalogItems[item.id] = item;
      });

      const resellerItems = state.resellerItems || {};

      resellerItems[action.resellerCode] = catalogItems;

      return {
        ...state,
        fetchCatalogItemsByResellerPending: false,
        fetchCatalogItemsByResellerError: null,
        resellerItems,
      };
    }

    case SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_FAILURE:
      return {
        ...state,
        fetchCatalogItemsByResellerPending: false,
        fetchCatalogItemsByResellerError: action.data.error,
      };

    case SUPPLIER_FETCH_CATALOG_ITEMS_BY_RESELLER_DISMISS_ERROR:
      return {
        ...state,
        fetchCatalogItemsByResellerError: null,
      };

    default:
      return state;
  }
}
