import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../../../common/api';
import {
  SUPPLIER_FETCH_SUPPLIERS_BEGIN,
  SUPPLIER_FETCH_SUPPLIERS_SUCCESS,
  SUPPLIER_FETCH_SUPPLIERS_FAILURE,
  SUPPLIER_FETCH_SUPPLIERS_DISMISS_ERROR,
} from './constants';

export function fetchSuppliers() {
  // If need to pass args to saga, pass it with the begin action.
  return {
    type: SUPPLIER_FETCH_SUPPLIERS_BEGIN,
  };
}

export function dismissFetchSuppliersError() {
  return {
    type: SUPPLIER_FETCH_SUPPLIERS_DISMISS_ERROR,
  };
}

// worker Saga: will be fired on SUPPLIER_FETCH_SUPPLIERS_BEGIN actions
export function* doFetchSuppliers() {
  // If necessary, use argument to receive the begin action with parameters.
  let res;

  try {
    res = yield call(api.get, '/suppliers?sort=name');
  } catch (err) {
    yield put({
      type: SUPPLIER_FETCH_SUPPLIERS_FAILURE,
      data: { error: err },
    });

    return;
  }
  // Dispatch success action out of try/catch so that render errors are not catched.
  yield put({
    type: SUPPLIER_FETCH_SUPPLIERS_SUCCESS,
    data: res,
  });
}

/*
  Alternatively you may use takeEvery.

  takeLatest does not allow concurrent requests. If an action gets
  dispatched while another is already pending, that pending one is cancelled
  and only the latest one will be run.
*/
export function* watchFetchSuppliers() {
  yield takeLatest(SUPPLIER_FETCH_SUPPLIERS_BEGIN, doFetchSuppliers);
}

// Redux reducer
export function reducer(state, action) {
  switch (action.type) {
    case SUPPLIER_FETCH_SUPPLIERS_BEGIN:
      return {
        ...state,
        fetchSuppliersPending: true,
        fetchSuppliersError: null,
        suppliers: {},
      };

    case SUPPLIER_FETCH_SUPPLIERS_SUCCESS: {
      const suppliers = {};

      action.data.data.suppliers.forEach((supplier) => {
        suppliers[supplier.code] = supplier.name;
      });

      return {
        ...state,
        fetchSuppliersPending: false,
        fetchSuppliersError: null,
        suppliers,
      };
    }

    case SUPPLIER_FETCH_SUPPLIERS_FAILURE:
      return {
        ...state,
        fetchSuppliersPending: false,
        fetchSuppliersError: action.data.error,
        suppliers: {},
      };

    case SUPPLIER_FETCH_SUPPLIERS_DISMISS_ERROR:
      return {
        ...state,
        fetchSuppliersError: null,
      };

    default:
      return state;
  }
}
