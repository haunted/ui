// Summary:
//   This is the entry of the application, works together with index.html.

import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import moment from 'moment-timezone';
import configStore from './common/configStore';
import routeConfig from './common/routeConfig';
import Root from './Root';

import './styles/index.less';
import 'antd/dist/antd.css'; // eslint-disable-line

const store = configStore();

moment.tz.setDefault('America/New_York'); // TODO BE should send correct time zone

function renderApp(app) {
  render(
    <div id="react-root">{app}</div>,
    document.getElementById('root'),
  );
}

renderApp(<Root store={store} routeConfig={routeConfig} />);

// Hot Module Replacement API
/* istanbul ignore if  */
if (module.hot) {
  module.hot.accept('./common/routeConfig', () => {
    // const nextRoot = require('./Root').default; // eslint-disable-line
    const nextRouteConfig = require('./common/routeConfig').default; // eslint-disable-line

    renderApp(<Root store={store} routeConfig={nextRouteConfig} />);
  });
}
