#!/bin/bash

mkdir -p /www

cp -rv public/* /www

if [[ "${NODE_ENV}" == "production" ]]; then
  browserify -g uglifyify -e src/app.js -t babelify | uglifyjs -c > /www/app.js
  # myth styles/app.css /www/app.css
  rm /www/index.html
  mv /www/index.prod.html /www/index.html
else
  browserify -d -e src/app.js -t babelify -o /www/app.js
fi

echo "contents of /www/"
ls -alh /www/
