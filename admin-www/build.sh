#!/bin/sh

BROWSERIFY=$(realpath ./node_modules/.bin/browserify)
WATCHMAN=$(realpath ./node_modules/.bin/watchman)
WATCHIFY=$(realpath ./node_modules/.bin/watchify)
STYLUS=$(realpath ./node_modules/.bin/stylus)

trap killgroup SIGINT
killgroup(){
  echo "killing..."
  kill 0
}

[ -d 'build' ] || mkdir -p 'build'
if [[ "${NODE_ENV}" == "production" ]]; then
  ${STYLUS} styles/app.styl -o public/app.css
  ${STYLUS} styles/print.styl -o public/app-print.css
  cp -rv public/* build/
  ${BROWSERIFY} -e src/app.js -t babelify > build/app.js
  # ${BROWSERIFY} -g uglifyify -e src/app.js -t babelify | uglifyjs -c > build/app.js
  mv -f build/index.prod.html build/index.html
elif [[ "${CI}" == "true" ]]; then
  export NODE_ENV="ci"
  ${STYLUS} styles/app.styl -o public/app.css
  ${STYLUS} styles/print.styl -o public/app-print.css
  cp -rv public/* build/
  browserify -e src/app.js -t babelify > build/app.js
else
  ${WATCHMAN} public 'cp -rv public/* build/' &
  ${WATCHIFY} -d -e src/app.js -t babelify -o build/app.js -v &
  ${WATCHMAN} styles "${STYLUS} -w styles/app.styl -o public/app.css" &
  ${WATCHMAN} styles "${STYLUS} -w styles/print.styl -o public/app-print.css" &
  wait
fi
