window.TZ = 'America/New_York'
Date.prototype.getUnixTime = function() { return this.getTime()/1000|0 };

import React from 'react'
import {render} from 'react-dom'
import injectTapEventPlugin from 'react-tap-event-plugin'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute, browserHistory, Redirect } from 'react-router'
import store from './ax-redux/index'
import setFeatures from 'feature-toggle'
import { syncHistoryWithStore } from 'react-router-redux'

window.featureToggle = setFeatures();
injectTapEventPlugin();

// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store)

import Root from './components/root'
import SideMenu from './components/side-menu'
import TopBar from './components/top-bar'

import Landing from './components/landing'
import Login from './components/login'
import App from './components/app'
import Register from './components/register'
import Dashboard from './components/dashboard'

import Redeam from './components/redeam'
import ManageRedeam from './components/manage-business/redeam'
import Suppliers from './components/manage-business/redeam/suppliers'
import Resellers from './components/manage-business/redeam/resellers'
import PassIssuers from './components/manage-business/redeam/pass-issuers'
import PassIssuerEdit from './components/manage-business/redeam/pass-issuer-edit'
import PassIssuerCreate from './components/manage-business/redeam/pass-issuer-create'

import ManageBusiness from './components/manage-business'
import Vouchers from './components/reports/vouchers'
import VoucherEdit from './components/reports/voucher-edit'
import VoucherExceptions from './components/reports/voucher-exceptions'
import SupplierVoucherExceptions from './components/reports/rejections'
import VoucherDetail from './components/manage-business/redeam/voucher-detail'

import Users from './components/manage-business/redeam/users'
import Groups from './components/manage-business/redeam/groups'
import Devices from './components/manage-business/redeam/devices'
import SupplierDevices from './components/manage-business/redeam/supplier-devices'



import UserEdit from './components/manage-business/redeam/user-edit'
import GroupEdit from './components/manage-business/redeam/group-edit'
import ContactEdit from './components/manage-business/contact-edit'
import SupplierEdit from './components/manage-business/redeam/supplier-edit'
import ResellerEdit from './components/manage-business/redeam/reseller-edit'
import SupplierOverview from './components/manage-business/supplier/overview'
import ResellerOverview from './components/manage-business/reseller/overview'
import ResellerDetail from './components/manage-business/supplier/reseller-detail'
import ProductEdit from './components/manage-business/product/product-edit'
import ProductDetail from './components/manage-business/product/product-detail'
import Products from './components/manage-business/product/products'
import ProductResellers from './components/manage-business/supplier/product-resellers'
import Contracts from './components/manage-business/supplier/contracts'
import OrganizationSettings from './components/manage-business/organization-settings'

import ResellerProductEdit from './components/manage-business/product/reseller-product-edit'
import ResellerProductSetup from './components/manage-business/product/reseller-product-setup'

import PriceLists from './components/manage-business/price-list/index'
import PriceListEdit from './components/manage-business/price-list/edit'

import ReportsContainer from './components/reports'
import Arrivals from './components/reports/arrivals'
import Exceptions from './components/reports/exceptions'
import Redeemed from './components/reports/redeemed'
import PassDetail from './components/reports/pass-detail'
import PassRedemptionsTable from './components/reports/pass-redemptions'
import ResellerRedemptions from './components/reports/reseller-redemptions'

import AddRedeemedVoucher from './components/reports/add-redeemed-voucher'
import ArrivalsDetail from './components/reports/arrivals-detail'
import ExceptionsDetail from './components/reports/exceptions-detail'
import RedeemedDetail from './components/reports/redeemed-detail'
import TopResellerTicketVolume from './components/reports/top-reseller-ticket-volume'
import TopResellerDollarVolume from './components/reports/top-reseller-dollar-volume'
import ResellerTicketDetail from './components/reports/reseller-ticket-detail'
import SupplierResellerInvoice from './components/reports/supplier-reseller-invoice'
import MasterShiftSummary from './components/reports/master-shift-summary'
import ShiftSummary from './components/reports/shift-summary'
import Shift from './components/reports/shift'
import ShiftTicket from './components/reports/shift-ticket'
import PageNotFound from './components/404'

import Me from './components/profile/me'
import CreateNews from './components/news/create'
import ListNews from './components/news/list'
import GlobalTransactionSearch from './components/manage-business/redeam/global-transaction-search'

import FailedOrders from './components/manage-business/redeam/failed-orders/index'
import FailedOrdersDetail from './components/manage-business/redeam/failed-orders/details'

render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={Root}>
        <IndexRoute component={Login}/>
        <Route path="login" component={Login} />
        <Route path="register" component={Register} />
        <Route path="app" component={App}>
          <Route path="dashboard" component={Dashboard} />
          <Route path="reports" component={ReportsContainer}>
            <IndexRoute component={Arrivals}/>

            <Route path="arrivals" component={Arrivals}>
              <Route path="?start=:start" component={Arrivals} ></Route>
            </Route>
            <Route path="arrivals/:supplierCode" component={Arrivals} >
              <Route path="?start=:start" component={Arrivals} ></Route>
              <Route path="?products=:products" component={Arrivals} ></Route>
              <Route path=":id" component={ArrivalsDetail} >
                <Route path="?start=:start" component={ArrivalsDetail} ></Route>
                <Route path="?products=:products" component={ArrivalsDetail} ></Route>
              </Route>
            </Route>
            {/*#419 admin: This report shows cancellations, and is not currently useful
            <Route path="exceptions" component={Exceptions} >
                <Route path="?start=:start&end=:end" component={Exceptions} ></Route>
            </Route>
            <Route path="exceptions/:supplierCode" component={Exceptions} >
              <Route path="?start=:start&end=:end" component={Exceptions} ></Route>
              <Route path=":id" component={ExceptionsDetail} >
                <Route path="?start=:start&end=:end" component={ExceptionsDetail} ></Route>
              </Route>
            </Route>
            */}
            <Route path="rejections" component={SupplierVoucherExceptions} >
                <Route path=":supplier" component={SupplierVoucherExceptions} />
                <Route path=":supplier/:reseller" component={SupplierVoucherExceptions} />
            </Route>
            <Route path="redeemed" component={Redeemed}>
                <Route path="?start=:start&end=:end" component={Redeemed} />
            </Route>
            <Route path="redeemed/:supplierCode" component={Redeemed}></Route>
            <Route path="redeemed/:supplierCode/:resellerCode" component={Redeemed}></Route>
            <Route path="redemption/:id" component={RedeemedDetail} ></Route>
            <Route path="redeemed/:supplierCode/:resellerCode/details/:id" component={RedeemedDetail} ></Route>
            <Route path="redeemed/add-voucher" component={AddRedeemedVoucher} />
            <Route path="redeemed/add-voucher/:supplierCode/:operator/:date" component={AddRedeemedVoucher} />
            <Route path="top-reseller-ticket-volume" component={TopResellerTicketVolume} >
              <Route path="?start=:start&end=:end" component={TopResellerTicketVolume} />
            </Route>
            <Route path="pass/:id" component={PassDetail} />
            <Route path="pass-redemptions" component={PassRedemptionsTable}>
                <Route path="?start=:start&end=:end" component={PassRedemptionsTable} />
            </Route>
            <Route path="pass-redemptions/:supplierCodes" component={PassRedemptionsTable}></Route>
            <Route path="pass-redemptions/:supplierCodes/details/:id" component={PassDetail} ></Route>
            <Route path="reseller-redemptions" component={ResellerRedemptions}>
                <Route path="?start=:start&end=:end" component={ResellerRedemptions} />
            </Route>
            <Route path="reseller-redemptions/:supplierCodes" component={ResellerRedemptions}></Route>
            <Route path="top-reseller-ticket-volume/:supplierCode" component={TopResellerTicketVolume} >
              <Route path="?start=:start&end=:end" component={TopResellerTicketVolume} />
              <Route path=":resellerCode" component={ResellerTicketDetail} >
                  <Route path="?start=:start&end=:end" component={ResellerTicketDetail} />
                  <Route path="ticket/:id" component={RedeemedDetail} >
                    <Route path="?start=:start&end=:end" component={RedeemedDetail} />
                  </Route>
              </Route>
            </Route>
            <Route path="top-reseller-dollar-volume" component={TopResellerDollarVolume} >
              <Route path="?start=:start&end=:end" component={TopResellerDollarVolume} />
            </Route>
            <Route path="top-reseller-dollar-volume/:supplierCode" component={TopResellerDollarVolume} >
                <Route path="?start=:start&end=:end" component={TopResellerDollarVolume} />
                <Route path=":resellerCode" component={ResellerTicketDetail} >
                    <Route path="?start=:start&end=:end" component={ResellerTicketDetail} />
                    <Route path="ticket/:id" component={RedeemedDetail} >
                      <Route path="?start=:start&end=:end" component={RedeemedDetail} />
                    </Route>
                </Route>
            </Route>

            <Route path="supplier-reseller-invoice" component={SupplierResellerInvoice} >
                <Route path=":resellerCode" component={SupplierResellerInvoice} />
                <Route path=":resellerCode/:supplierCode" component={SupplierResellerInvoice} />
            </Route>
            <Route path="supplier-reseller-invoice/details/:supplierCode/id/:id" component={RedeemedDetail}>
            </Route>

            <Route path="shift/:operatorId/:date" component={Shift} >
                <Route path="?sort=:sort" component={Shift} />
                <Route path="ticket/:id" component={ShiftTicket} />
            </Route>

            <Route path="shifts" component={ShiftSummary} />
            <Route path="shifts/:supplierCode" component={ShiftSummary} />

            <Route path="shifts/master-summary" component={MasterShiftSummary} >
              <Route path="ticket/:id" component={ShiftTicket} ></Route>
            </Route>
            <Route path="shifts/:supplierCode/master-summary" component={MasterShiftSummary} >
              <Route path="ticket/:id" component={ShiftTicket} ></Route>
            </Route>
            <Route path="vouchers" component={Vouchers} >
              <Route path="detail/:id" component={VoucherDetail} >
                <Route path="?start=:start&end=:end&limit=:limit&offset=:offset" component={VoucherDetail} />
              </Route>
            </Route>
            <Route path="vouchers/:supplierCode" component={Vouchers} >
              <Route path="detail/:id" component={VoucherDetail} >
                <Route path="?start=:start&end=:end&limit=:limit&offset=:offset" component={VoucherDetail} />
              </Route>
            </Route>
            <Route path="vouchers/:supplierCode/:resellerCode" component={Vouchers} >
              <Route path="?start=:start&end=:end&limit=:limit&offset=:offset" component={Vouchers} />
              <Route path="detail/:id" component={VoucherDetail} >
                <Route path="?start=:start&end=:end&limit=:limit&offset=:offset" component={VoucherDetail} />
              </Route>
            </Route>
            <Route path="voucher-exceptions" component={VoucherExceptions}>
                <Route path="edit/:id&limit=:limit&offset=:offset" component={VoucherEdit} />
            </Route>
            <Route path="voucher-exceptions/:supplierCode" component={VoucherExceptions}>
                <Route path="?start=:start&end=:end&limit=:limit&offset=:offset" component={VoucherExceptions} />
                <Route path="edit/:id" component={VoucherEdit} >
                  <Route path="?start=:start&end=:end&limit=:limit&offset=:offset" component={VoucherEdit} />
                </Route>
            </Route>
            <Route path="voucher-exceptions/:supplierCode/:resellerCode" component={VoucherExceptions}>
                <Route path="?start=:start&end=:end&limit=:limit&offset=:offset" component={VoucherExceptions} />
                <Route path="edit/:id" component={VoucherEdit} >
                  <Route path="?start=:start&end=:end&limit=:limit&offset=:offset" component={VoucherEdit} />
                </Route>
            </Route>
          </Route>
          <Route path="redeam" component={Redeam} >
              <Route path="search" component={GlobalTransactionSearch} />
              <Route path="users" component={Users}>
                <Route path="?limit=:limit&offset=:offset" component={Users}></Route>
              </Route>
              <Route path="users/:id" component={UserEdit} >
                <Route path="?limit=:limit&offset=:offset" component={UserEdit}></Route>
              </Route>
              <Route path="users/add" component={UserEdit} />
              <Route path="groups" component={Groups} />
              <Route path="groups/:id" component={GroupEdit} />
              <Route path="groups/add" component={GroupEdit} />
              <Route path="suppliers" component={Suppliers} />
              <Route path="suppliers/add" component={SupplierEdit} />
              <Route path="suppliers/:id" component={SupplierEdit} />
              <Route path="resellers" component={Resellers} />
              <Route path="resellers/:id" component={ResellerEdit} />
              <Route path="resellers/add" component={ResellerEdit} />
              <Route path="pass-issuers" component={PassIssuers} />
              <Route path="pass-issuers/add" component={PassIssuerCreate} />
              <Route path="pass-issuers/:id" component={PassIssuerEdit} />
              <Route path="failed-orders" component={FailedOrders}>
                <Route path="detail" component={FailedOrdersDetail}></Route>
                <Route path="detail/:supplierCode" component={FailedOrdersDetail}></Route>
                <Route path="detail/:supplierCode/:resellerCode" component={FailedOrdersDetail}></Route>
              </Route>
              <Route path="devices" component={Devices} />
              <Route path="devices/:id" component={Devices} />
              <Route path="devices/:id/:version/deactivate" component={Devices} />
              <Route path="devices/:id/:version/notes" component={Devices} />
              <Route path="supplier/:supplierCode/devices" component={SupplierDevices} />
              <Route path="supplier/:supplierCode/devices/provision" component={SupplierDevices} />
              <Route path="supplier/:supplierCode/devices/:id/:version/deprovision" component={SupplierDevices} />

          </Route>
          <Route path="manage-business" component={ManageBusiness}>
            <Route path="supplier-overview" component={SupplierOverview} />
            <Route path="reseller-detail/:id" component={ResellerDetail} />
            <Route path="contacts/add" component={ContactEdit} />
            <Route path="contacts/:id" component={ContactEdit} />
            <Route path="products" component={Products} />
            <Route path="product/detail/:supplierId/:catalogId/:id" component={ProductDetail} />
            <Route path="products/edit/:supplierId/:catalogId/:id" component={ProductEdit} />
            <Route path="products/reseller/setup/:catalogId/:id/:optionCode" component={ResellerProductSetup} />
            <Route path="products/reseller/setup/:catalogId/:id" component={ResellerProductSetup} />
            <Route path="products/reseller/edit/:catalogId/:id" component={ResellerProductEdit} />
            <Route path="products/add" component={ProductEdit} />
            <Route path="contracts" component={Contracts} />
            <Route path="price-lists" component={PriceLists} />
            <Route path="price-lists/:supplierId/:catalogId/:id" component={PriceListEdit} />
            <Route path="settings" component={OrganizationSettings} />
          </Route>
          <Route path='/404' component={PageNotFound} />
          <Redirect from='*' to='/404' />
         </Route>
      </Route>
     </Router>
  </Provider>,
  document.getElementById('app')
)
