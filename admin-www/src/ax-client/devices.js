import axios from 'axios'

// params: sort, status, supplierCode
export let getDevice = id => axios.get(`${GO_API_URL}/api/devices/${id}`)
export let listDevices = params => axios.get(`${GO_API_URL}/api/devices`, { params })
export let getNextAssetId = () => axios.get(`${GO_API_URL}/api/devices/next-asset-id-increment`)

export let createDevice = data => axios.post(`${GO_API_URL}/api/devices`, data)
export let updateDevice = data => axios.put(`${GO_API_URL}/api/devices/${data.id}`, data)

export let deactivateDevice = (id, v, reason) => axios.put(`${GO_API_URL}/api/devices/${id}/${v}/deactivate`, { reason })
export let provisionDevice = (id, v, supplierCode) => axios.put(`${GO_API_URL}/api/devices/${id}/${v}/provision/${supplierCode}`)
export let deprovisionDevice = (id, v) => axios.put(`${GO_API_URL}/api/devices/${id}/${v}/deprovision`)

export let appendNote = (id, v, message) => axios.post(`${GO_API_URL}/api/devices/${id}/${v}/notes`, { message })
export let deleteNote = (id, v, noteId) => axios.delete(`${GO_API_URL}/api/devices/${id}/${v}/notes/${noteId}`)