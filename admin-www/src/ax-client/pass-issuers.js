import axios from 'axios'

export let listPassIssuers = ( params ) => axios.get(GO_API_URL + "/api/passissuers", {params: params})
export let getPassIssuer = id => axios.get(GO_API_URL + "/api/passissuers/" + id)
export let createPassIssuer = data => axios.post(GO_API_URL + "/api/passissuers", data)
export let updatePassIssuer = (id, data) => axios.put(GO_API_URL + "/api/passissuers/" + id, data)
export let disablePassIssuer = (id, version) => axios.put(GO_API_URL + "/api/passissuers/disable/" + id + "/" + version)