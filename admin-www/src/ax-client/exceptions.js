import axios from 'axios'
import moment from 'moment-timezone'

let defaultParams = {
  limit: 100,
  offset: 0,
}

let prepareParams = params => {
  params = Object.assign({}, defaultParams, params)
  if (params.start) {
    params.start = moment(params.start).unix()
  }
  if (params.end) {
    params.end = moment(params.end).unix()
  }
  return params
}

export let listExceptions = data =>
    axios.get(GO_API_URL + "/api/exceptions", {params: prepareParams(data)});

export function exceptionsOpenCSV(params) {
  params.format = "csv"
  params = prepareParams(params)
  let endpoint = '/api/exceptions',
      url = GO_API_URL + endpoint;
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)
}

export function exceptionsOpenXLSX(params) {
  params.format = "xlsx"
  params = prepareParams(params)
  let endpoint = '/api/exceptions',
      url = GO_API_URL + endpoint;
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)

}
