import axios from 'axios'

export let listSuppliers = (params) => axios.get(`${GO_API_URL}/api/suppliers`, {params: params})
export let getSupplier = id => axios.get(`${GO_API_URL}/api/suppliers/${id}`)
export let createSupplier = data => axios.post(`${GO_API_URL}/api/suppliers`, data)
export let updateSupplier = (id, data) => axios.put(`${GO_API_URL}/api/suppliers/${id}`, data)
export let disableSupplier = (id, version) => axios.put(`${GO_API_URL}/api/suppliers/disable/${id}/${version}`)

export let createSupplierCatalog = (supplierId, data) => axios.post(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs`, data)
export let listSupplierCatalogs = (supplierId) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs`)
export let updateSupplierCatalog = (supplierId, catalogId, data) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/${catalogId}`, data)
export let getSupplierCatalog = (supplierId) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/${catalogId}`)
export let disableSupplierCatalog = (supplierId, catalogId, version) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/disable/${catalogId}/${version}`)

export let createSupplierCatalogItem = (supplierId, catalogId, data) => axios.post(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/${catalogId}/items`, data)
export let updateSupplierCatalogItem = (supplierId, catalogId, id, data) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/${catalogId}/items/${id}`, data)
export let listSupplierCatalogItems = (supplierId, catalogId, params) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/${catalogId}/items`, {params: params})
export let listSupplierResellerCatalogItems = (supplierId, resellerId, params) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/reseller/${resellerId}/items`, {params: params})
export let getSupplierCatalogItem = (supplierId, catalogId, id) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/${catalogId}/items/${id}`)
export let disableSupplierCatalogItem = (supplierId, catalogId, id) => axios.post(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/${catalogId}/items/disable/${id}`)

export let listPriceLists = (supplierId) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists`)
export let getPriceList = (supplierId, id) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists/${id}`)
export let createPriceList = (supplierId, data) => axios.post(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists`, data)
export let updatePriceList = (supplierId, id, data) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists/${id}`, data)
export let disablePriceList = (supplierId, id, version) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists/disable/${id}/${version}`)
export let listPriceListsByCatalogItem = (supplierId, catalogId, id) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/catalogs/${catalogId}/items/${id}/price-lists`)

export let listPriceListItems = (supplierId, priceListId) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists/${priceListId}/items`)
export let getPriceListItem = (supplierId, priceListId, id) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists/${priceListId}/items/${id}`)
export let createPriceListItem = (supplierId, priceListId, data) => axios.post(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists/${priceListId}/items`, data)
export let updatePriceListItem = (supplierId, priceListId, id, data) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists/${priceListId}/items/${id}`, data)
export let disablePriceListItem = (supplierId, priceListId, id, version) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/price-lists/${priceListId}/items/disable/${id}/${version}`)

export let createContract = (supplierId, data) => axios.post(`${GO_API_URL}/api/suppliers/${supplierId}/contracts`, data)
export let listContracts = (supplierId) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/contracts`)
export let readContract = (supplierId, id) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/contracts/${id}`)
export let updateContract = (supplierId, id, data) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/contracts/${id}`, data)
export let disableContract = (supplierId, id, version) => axios.put(`${GO_API_URL}/api/suppliers/${supplierId}/contacts/disable/${id}/${version}`)
