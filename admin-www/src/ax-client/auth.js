import axios from 'axios'
import Cookies from 'cookies-js';

let cookies = Cookies(window);
let tokenH = null
let interceptorReq = null
let interceptorRes = null

export function loadAuthorization() {
  let tokenjson = localStorage.getItem("token")
  if (tokenjson == null) {
    return false
  }

  try {
    let data = JSON.parse(tokenjson)
    setAuthorization(data)
    return data
  } catch(e) {
    console.error(e)
    localStorage.removeItem("token")
    return false
  }

}

export function logout() {
  localStorage.removeItem("token")
}

export function setAuthorization(data) {
    let cookie = "";

    tokenH = `${data.accessTokenType} ${data.accessToken}`
    localStorage.setItem("token", JSON.stringify(data))
    cookie = tokenH.split(" ")[1];
    Cookies.set('authorization', cookie)

  interceptorReq = axios.interceptors.request.use(
    config => {
      if (tokenH != null) {
        config.headers.Authorization = tokenH
      }
      return config
    },
    err => Promise.reject(err)
  )

  interceptorRes = axios.interceptors.response.use(
    res => res,
    err => {
    // test for for #authPingPong fail
      if (!!err.config && err.config.url.substring(GO_API_URL.length) == "/api/me") {
          if (err.status === 403) {
            if (err.data == "unauthenticated") {
                _onFailAuthorization(err)
            }
          }
      } else {
          if (!!err.status && err.status === 403) {
              if (err.data.Error == "unauthenticated" || err.data == "unauthenticated") {
                  _onFailAuthorization(err)
              }
          }
      }
    return Promise.reject(err);
  });
}

export function refreshToken (token) {
    return axios.post(GO_API_URL + '/api/refresh', {}, {headers: {
        Authorization: token
    }})
}

// hook for auth failing
let _onFailAuthorization = res => {}
export function onFailAuthorzation(cb) {
    _onFailAuthorization = cb
}

export function unsetAuthorization() {
  axios.interceptors.request.eject(interceptorReq)
  interceptorReq = null
  axios.interceptors.request.eject(interceptorRes)
  interceptorRes = null
  tokenH = null
}

export function login({org, email, password}) {
  email = email + "@" + org
  return axios.post(GO_API_URL + '/api/login', {}, { auth: {username:email, password} } )
}

export function register({email, password}) {
  return axios.post(GO_API_URL + '/api/register', {}, { auth: {username:email, password} } )
}

export function me() {
  return axios.get(GO_API_URL + '/api/me')
}
