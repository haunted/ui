import axios from 'axios'

export let getResults = (params) => axios.get(GO_API_URL + "/api/search", {params: params})
