import axios from 'axios'

export let listSettings = (data) => axios.get(`${GO_API_URL}/api/settings`, {params: data})
export let readSettings = (orgCode, orgType) => axios.get(`${GO_API_URL}/api/settings/${orgCode}/${orgType}`)
export let updateSettings = (data) => axios.post(GO_API_URL + `/api/settings`, data)
export let deleteSettings = (orgCode, orgType) => axios.delete(GO_API_URL + `/api/settings/${orgCode}/${orgType}`)
