import axios from 'axios'

export let createAccount = data => axios.post(GO_API_URL + "/api/accounts", data)
export let listAccounts = (data) => axios.get(GO_API_URL + "/api/accounts", {params: data})
export let readAccount = id => axios.get(GO_API_URL + "/api/accounts/" + id)
export let updateAccount = (id, data) => axios.put(GO_API_URL + `/api/accounts/${id}`, data)
export let updateAccountPassword = (id, data) => axios.put(GO_API_URL + `/api/accounts/${id}/update-password`, data)
export let deleteAccount = id => axios.delete(GO_API_URL + `/api/accounts/${id}`)
export let listAccountPermissions = id => axios.get(GO_API_URL + `/api/accounts/${id}/permissions`)
export let updateAccountPermissions = (id, data) => axios.put(GO_API_URL + `/api/accounts/${id}/permissions`, data)
export let listAccountGroups = id => axios.get(GO_API_URL + `/api/accounts/${id}/groups`)
