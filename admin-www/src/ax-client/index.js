import axios from 'axios'

import moment from 'moment-timezone'

import * as accounts from './accounts'
import * as auth from './auth'
import * as contacts from './contacts'
import * as dashboard from './dashboard'
import * as devices from './devices'
import * as exceptions from './exceptions'
import * as failedOrders from './failed-orders'
import * as finances from './finances'
import * as groups from './groups'
import * as news from './news'
import * as orgSettings from './org-settings'
import * as passes from './passes'
import * as passIssuers from './pass-issuers'
import * as products from './products'
import * as redemptions from './redemptions'
import * as resellers from './resellers'
import * as search from './search'
import * as shifts from './shifts'
import * as suppliers from './suppliers'
import * as tablets from './tablets'
import * as tickets from './tickets'
import * as vouchers from './vouchers'

let client = {
  accounts,
  auth,
  contacts,
  dashboard,
  devices,
  exceptions,
  failedOrders,
  finances,
  groups,
  news,
  orgSettings,
  passes,
  passIssuers,
  products,
  redemptions,
  resellers,
  search,
  shifts,
  suppliers,
  tablets,
  tickets,
  vouchers,
}

export default client