import axios from 'axios'
import moment from 'moment-timezone'

export let voidRedemption = id => axios.put(`${GO_API_URL}/api/redemption/${id}/void`)

export function redemptions(params) {
  params = prepareRedemptionParams(params)
  return axios.get(GO_API_URL + '/api/redemptions', {params})
}

export function allRedemptions(params) {
  params = prepareAllRedemptionParams(params)
  return axios.get(`${GO_API_URL}/api/redemptions`, {params})
}

export function redemptionById(id, params) {
  return axios.get(GO_API_URL + '/api/redemption/'+id, {params})
}

export function redemptionsOpenCSV(params) {
  params.format = "csv"
  params = prepareRedemptionParams(params)
  let endpoint = (params.endpoint != null && params.endpoint!="" ? params.endpoint : '/api/redemptions'),
      url = GO_API_URL + endpoint;
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)
}

export function redemptionsOpenXLSX(params) {
  params.format = "xlsx"
  params = prepareRedemptionParams(params)
  let endpoint = (params.endpoint != null && params.endpoint!="" ? params.endpoint : '/api/redemptions'),
      url = GO_API_URL + endpoint;
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)

}

let prepareRedemptionParams = params => {
  if (params.start) {
    params.start = moment(params.start).unix()
  }
  if (params.end) {
    params.end = moment(params.end).unix()
  }
  if (params.sources) {
    params.sources = params.sources.join('|')
  }
  params = Object.assign({}, {
    offset: 0,
    start: '01-01-2000',
    end: '01-01-2030',
  }, params)
  return params
}

let prepareAllRedemptionParams = params => {
  if (params.start) {
    params.start = moment(params.start).unix()
  }
  if (params.end) {
    params.end = moment(params.end).unix()
  }
  if (params.sources) {
    params.sources = params.sources.join('|')
  }
  return params
}
