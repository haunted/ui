import axios from 'axios'

export let createContact = data => axios.post(GO_API_URL + "/api/contacts", data)
export let listContacts = (data) => axios.get(GO_API_URL + "/api/contacts", {params: data})
export let readContact = id => axios.get(GO_API_URL + "/api/contacts/" + id)
export let updateContact = (id, data) => axios.put(GO_API_URL + `/api/contacts/${id}`, data)
export let deleteContact = id => axios.delete(GO_API_URL + `/api/contacts/${id}`)
