import axios from 'axios'

export let listProducts = (catalogId, params) => axios.get(`${GO_API_URL}/api/catalogs/${catalogId}/catalog-items`, {params: params})
export let getSupplierProducts = (supplierCode, params) => axios.get(GO_API_URL + "/api/v1/suppliers/"+supplierCode+"/products", {params: params})
export let listPassProducts = (params) => axios.get(`${GO_API_URL}/api/catalogitem/pass/`, {params: params})
export let readProduct = id => axios.get(GO_API_URL + "/api/catalogitem/" + id)
export let getPassProduct = id => axios.get(GO_API_URL + "/api/catalogitem/" + id +"/pass")
export let createProduct = (supplierCode, data) => axios.post(GO_API_URL + "/api/catalogitem", data)
export let createPassProduct = (data) => axios.post(`${GO_API_URL}/api/catalogitem/pass/`, data)
export let updateProduct = (id, data) => axios.put(GO_API_URL + "/api/catalogitem/"+id, data)
export let deleteProduct = id => axios.delete(GO_API_URL + "/api/catalogitem/"+id)
export let listProductResellers = id => axios.get(`${GO_API_URL}/api/catalogitem/${id}/resellers`)

export let listConnectedProducts = (resellerId, params) => axios.get(`${GO_API_URL}/api/resellers/${resellerId}/catalog-items/connected`, {params: params})
export let listPendingProducts = (resellerId, catalogId, params) => axios.get(`${GO_API_URL}/api/resellers/${resellerId}/catalog/${catalogId}/available`, {params: params})
export let getConnectedProduct = (resellerId, productId) => axios.get(`${GO_API_URL}/api/resellers/${resellerId}/catalog-items/connected/${productId}`)
export let getPendingProduct = (resellerId, productId) => axios.get(`${GO_API_URL}/api/resellers/${resellerId}/catalog-items/pending/${productId}`)
export let editConnectedProduct = (catalogId, productId, data) => axios.put(`${GO_API_URL}/api/catalogs/${catalogId}/catalog-items/connected/${productId}`, data)
export let setupPendingProduct = (catalogId, productId, data) => axios.put(`${GO_API_URL}/api/catalogs/${catalogId}/catalog-items/pending/${productId}`, data)
