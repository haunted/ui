import axios from 'axios'
import moment from 'moment-timezone'

export let getVoucher = id => axios.get(`${GO_API_URL}/api/vouchers/${id}`)
export let getVouchers = (params) => axios.get(`${GO_API_URL}/api/vouchers`, {params: params})
export let getVoucherExceptions = (params) => axios.get(`${GO_API_URL}/api/vouchers/exceptions`, {params: params})
export let getVoucherImage = id => axios.post(`${GO_API_URL}/api/vouchers/${id}/redemption-image-url`)
export let updateVoucher = (id, data) => axios.put(`${GO_API_URL}/api/vouchers/${id}`, {params: data})
export let createRedeemedVoucher = data => axios.post(`${GO_API_URL}/api/vouchers/redeemed`, data)
export let voidVoucher = id => axios.put(`${GO_API_URL}/api/vouchers/${id}/void`)
export let mergeVoucher = id => axios.put(`${GO_API_URL}/api/vouchers/${id}/merge`)
export let rejectVoucher = (id, data) => {
  if (!!!data) { // no reason
    data = {params: data};
  }
  return axios.put(GO_API_URL + "/api/vouchers/"+id+"/reject", data);
}
export let correctVoucher = (id, data) => axios.put(GO_API_URL + "/api/vouchers/"+id+"/correct", data)

export function vouchersCSV(params) {
  params.format = "csv"
  params = prepareVoucherParams(params)
  let url = GO_API_URL + '/api/vouchers';
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)
}
export function vouchersXLSX(params) {
  params.format = "xlsx"
  params = prepareVoucherParams(params)
  let url = GO_API_URL + '/api/vouchers';
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)
}
export function voucherExceptionsCSV(params) {
  params.format = "csv"
  params = prepareVoucherParams(params)
  let url = GO_API_URL + '/api/vouchers/exceptions';
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)
}
export function voucherExceptionsXLSX(params) {
  params.format = "xlsx"
  params = prepareVoucherParams(params)
  let url = GO_API_URL + '/api/vouchers/exceptions';
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)
}

let prepareVoucherParams = params => {
  if (params.start) {
    params.start = moment(params.start).unix()*1000
  }
  if (params.end) {
    params.end = moment(params.end).unix()*1000
  }
  if (params.sources) {
    params.sources = params.sources.join('|')
  }
  params = Object.assign({
    offset: 0,
    start: '01-01-2000',
    end: '01-01-2030'
  }, params)
  return params
}
