import axios from 'axios'
import moment from 'moment-timezone'

export const STATUS_REDEEMED = 'STATUS_REDEEMED'
export const STATUS_CANCELLED = 'STATUS_CANCELLED'
export const STATUS_OPEN = 'STATUS_OPEN'

export function tickets(params) {
  params = prepareTicketParams(params)
  return axios.get(GO_API_URL + '/api/tickets', {params})
}

export function allTickets(params) {
  console.log("A L L _ T I C K E T _ P A R A M S")
  console.log(params)
  params = prepareAllTicketParams(params)
  console.log(params)
  return axios.get(`${GO_API_URL}/api/tickets`, {params})
}

export function ticketById(id, params) {
  return axios.get(GO_API_URL + '/api/ticket/'+id, {params})
}

export function ticketsOpenCSV(params) {
  params.format = "csv"
  params = prepareTicketParams(params)
  let endpoint = (params.endpoint != null && params.endpoint!="" ? params.endpoint : '/api/tickets'),
      url = GO_API_URL + endpoint;
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)
}

export function ticketsOpenXLSX(params) {
  params.format = "xlsx"
  params = prepareTicketParams(params)
  let endpoint = (params.endpoint != null && params.endpoint!="" ? params.endpoint : '/api/tickets'),
      url = GO_API_URL + endpoint;
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)

}

let prepareTicketParams = params => {
  if (!! params.start) {
    params.start = moment(params.start).unix()
  }
  if (!! params.end) {
    params.end = moment(params.end).unix()
  }
  if (!! params.date) {
    params.start = 0
    params.end = 0
  }
  if (!! params.sources) {
    params.sources = params.sources.join('|')
  }
  params = Object.assign({
    offset: 0,
    start: '01-01-2000',
    end: '01-01-2030',
    status: STATUS_OPEN,
    sources: "",
  }, params)
  return params
}

let prepareAllTicketParams = params => {
  if (params.start) {
    params.start = moment(params.start).unix()
  }
  if (params.end) {
    params.end = moment(params.end).unix()
  }
  if (params.sources) {
    params.sources = params.sources.join('|')
  }
  return params
}
