import axios from 'axios'
import moment from 'moment-timezone'

export let getPass = (id, supplierCode) => axios.get(GO_API_URL + '/api/passes/' + id, { params: { supplierCode } })

export let createPass = data => axios.post( `${GO_API_URL}/api/passes`, data )

export let getPassRedemptions = params => axios.get( `${GO_API_URL}/api/passes/redemptions`, { params } )

let prepareRedemptionParams = params => {
  if (params.start) {
    params.start = moment(params.start).unix()
  }
  if (params.end) {
    params.end = moment(params.end).unix()
  }
  if (params.sources) {
    params.sources = params.sources.join('|')
  }
  params = Object.assign({}, {
    offset: 0,
    limit: 100000,
    start: '01-01-2000',
    end: '01-01-2030',
  }, params)
  return params
}

export function passRedemptionsCSV(params) {
  params.format = "csv"
  params = prepareRedemptionParams(params)
  let endpoint = (params.endpoint != null && params.endpoint!="" ? params.endpoint : '/api/passes/redemptions'),
      url = GO_API_URL + endpoint;
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)
}

export function passRedemptionsXLSX(params) {
  params.format = "xlsx"
  params = prepareRedemptionParams(params)
  let endpoint = (params.endpoint != null && params.endpoint!="" ? params.endpoint : '/api/passes/redemptions'),
      url = GO_API_URL + endpoint;
  for (var k in params) {
    if (params.hasOwnProperty(k) && k != 'endpoint') {
      url += `&${k}=${encodeURIComponent(params[k])}`
    }
  }
  url = url.replace(/&/, '?')
  window.open(url)

}