import axios from 'axios'

export let getNews = () => axios.get(GO_API_URL + '/api/news')
export let createNews = (news) => axios.post(GO_API_URL + '/api/news', news)
