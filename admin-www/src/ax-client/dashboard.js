import axios from 'axios'
import {
    ACTOUREX_CUSTOMER_SUPPORT_STAFF,
    ACTOUREX_BUSINESS_STAFF,
    ACTOUREX_TECHNICAL_STAFF,
    SUPPLIER_REDEMPTION_STAFF,
    SUPPLIER_REDEMPTION_SUPERVISOR,
    SUPPLIER_BUSINESS_STAFF,
    RESELLER_BUSINESS_STAFF
} from '../ax-redux/constants'

export function dashboard(params = {}, role) {
  // if (!params.today) {
  //   var start = new Date()
  //   start.setHours(0,0,0,0);
  //   params.today = start.getTime() / 1000;
  // }
  switch (role) {
      case ACTOUREX_BUSINESS_STAFF:
        return axios.get(GO_API_URL + '/api/dashboard/actourex-business', {params})
      break;
      case SUPPLIER_BUSINESS_STAFF:
        return axios.get(GO_API_URL + '/api/dashboard/supplier-business', {params})
      break;
      case RESELLER_BUSINESS_STAFF:
        return axios.get(GO_API_URL + '/api/dashboard/reseller-business', {params})
      break;
      case ACTOUREX_TECHNICAL_STAFF:
        return axios.get(GO_API_URL + '/api/dashboard/actourex-technical', {params})
      break;
      case SUPPLIER_REDEMPTION_SUPERVISOR:
        return axios.get(GO_API_URL + '/api/dashboard/staff-performance', {params})
      break;
      case SUPPLIER_REDEMPTION_STAFF:
        return axios.get(GO_API_URL + '/api/dashboard/limited', {params})
      break;
      case ACTOUREX_CUSTOMER_SUPPORT_STAFF:
        return axios.get(GO_API_URL + '/api/dashboard/support', {params})
      break;
  }
}
