import axios from 'axios'

export let createGroup =  data => axios.post(GO_API_URL + `/api/groups`, data)
export let listGroups =  (params) => axios.get(GO_API_URL + `/api/groups`, {params: params})
export let readGroup =  id => axios.get(GO_API_URL + `/api/groups/${id}`)
export let updateGroup =  (id, data) => axios.put(GO_API_URL + `/api/groups/${id}`, data)
export let deleteGroup =  id => axios.delete(GO_API_URL + `/api/groups/${id}`)
export let listAccountsByGroup =  id => axios.get(GO_API_URL + `/api/groups/${id}/accounts`)
export let addAccountToGroup =  (id, accountId) => axios.post(GO_API_URL + `/api/groups/${id}/accounts/${accountId}`)
export let removeAccountFromGroup =  (id, accountId) => axios.delete(GO_API_URL + `/api/groups/${id}/accounts/${accountId}`)
export let listGroupPermissions =  id => axios.get(GO_API_URL + `/api/groups/${id}/permissions`)
export let updateGroupPermissions =  (id, data) => axios.put(GO_API_URL + `/api/groups/${id}/permissions`, data)
