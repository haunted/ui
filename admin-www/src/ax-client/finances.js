import axios from 'axios'
import moment from 'moment-timezone'

let prepareParams = params => {
  if (params.start) {
    params.start = moment(params.start).unix()
  }
  if (params.end) {
    params.end = moment(params.end).unix()
  }
  return params
}

export let listResellerInvoices = data =>
    axios.get(GO_API_URL + "/api/finance/supplier-reseller-invoices", {params: prepareParams(data)});

export let getResellerInvoices = data =>
    axios.get(GO_API_URL + "/api/finance/supplier-reseller-invoice", {params: prepareParams(data)});

export let getResellerTicketVolume = data =>
    axios.get(GO_API_URL + "/api/finance/supplier-top-reseller-ticket-volume", {params: prepareParams(data)});

export let getResellerDollarVolume = data =>
    axios.get(GO_API_URL + "/api/finance/supplier-top-reseller-dollar-volume", {params: prepareParams(data)});
