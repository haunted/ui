import axios from 'axios'

export let getTablets = (params) => axios.get(GO_API_URL + "/api/tablets", {params: params})
export let getTablet = id => axios.get(GO_API_URL + "/api/tablets/" + id)
export let createTablet = data => axios.post(GO_API_URL + "/api/tablets", data)
export let updateTablet = (id, data) => axios.put(GO_API_URL + "/api/tablets/"+id, data)
export let deleteTablet = id => axios.delete(GO_API_URL + "/api/tablets"+id)
