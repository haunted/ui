import axios from 'axios'
import moment from 'moment-timezone'

let prepareSummaryParams = params => {
  if (params.start) {
    params.start = moment(params.start).unix()
  }
  if (params.end) {
    params.end = moment(params.end).unix()
  }
  return params
}

let prepareParams = params => {
  if (params.date) {
    params.date = moment(params.date).unix()
  }
  return params
}

export let getMasterShiftSummaryReport = data =>
    axios.get(GO_API_URL + "/api/reports/master-shift-summary", {params: prepareSummaryParams(data)});

export let getShiftReportSummary = data =>
    axios.get(GO_API_URL + "/api/reports/shift-summary", {params: data});

export let getShiftReport = data =>
    axios.get(GO_API_URL + "/api/reports/shift", {params: prepareParams(data)});
