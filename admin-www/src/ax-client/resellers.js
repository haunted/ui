import axios from 'axios'

export let listResellers = (params) => axios.get(GO_API_URL + "/api/resellers", {params: params})
export let listSupplierResellers = ( supplierId, params ) => axios.get(`${GO_API_URL}/api/suppliers/${supplierId}/resellers`, {params: params})
export let getReseller = id => axios.get(GO_API_URL + "/api/resellers/" + id)
export let createReseller = data => axios.post(GO_API_URL + "/api/resellers", data)
export let updateReseller = (id, data) => axios.put(GO_API_URL + "/api/resellers/" + id, data)
export let disableReseller = (id, version) => axios.put(GO_API_URL + "/api/resellers/disable/" + id + "/" + version)

export let createResellerCatalog = (resellerId, data) => axios.post(`${GO_API_URL}/api/resellers/${resellerId}/catalogs`, data)
export let listResellerCatalogs = (resellerId) => axios.get(`${GO_API_URL}/api/resellers/${resellerId}/catalogs`)
export let updateResellerCatalog = (resellerId, catalogId, data) => axios.put(`${GO_API_URL}/api/resellers/${resellerId}/catalogs/${catalogId}`, data)
export let getResellerCatalog = (resellerId) => axios.get(`${GO_API_URL}/api/resellers/${resellerId}/catalogs/${catalogId}`)
export let disableResellerCatalog = (resellerId, catalogId, version) => axios.put(`${GO_API_URL}/api/resellers/${resellerId}/catalogs/disable/${catalogId}/${version}`)

export let createResellerCatalogItem = (resellerId, catalogId, data) => axios.post(`${GO_API_URL}/api/resellers/${resellerId}/catalogs/${catalogId}/items`, data)
export let updateResellerCatalogItem = (resellerId, catalogId, id, data) => axios.put(`${GO_API_URL}/api/resellers/${resellerId}/catalogs/${catalogId}/items/${id}`, data)
export let listResellerCatalogItems = (resellerId, catalogId, params) => axios.get(`${GO_API_URL}/api/resellers/${resellerId}/catalogs/${catalogId}/items`, {params: params})
export let getResellerCatalogItem = (resellerId, catalogId, id) => axios.get(`${GO_API_URL}/api/resellers/${resellerId}/catalogs/${catalogId}/items/${id}`)
export let disableResellerCatalogItem = ( resellerId, catalogId, id, version ) => axios.post(`${GO_API_URL}/api/resellers/${resellerId}/catalogs/${catalogId}/items/disable/${id}/${version}`)
