import axios from 'axios'
import moment from 'moment-timezone'


export let listFailedOrders = params =>
    axios.get(GO_API_URL + "/api/orders/failed", {params});

export let listFailedOrderDetails = params =>
    axios.get(GO_API_URL + "/api/orders/failed/details", {params});

export let reprocessFailedOrders = ids =>
    axios.post(`${GO_API_URL}/api/orders/failed/reprocess?ids=${ids}`);