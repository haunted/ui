// format a currency/value
export function formatCurrency(currency, value) {
  value /= 100;
  if (value == 0) {
    return '–';
  }
  return value.toLocaleString('en-US', { style: 'currency', currency });
}

// format an api price
export function formatPrice(p) {
  return formatCurrency(p.currencyCode, p.amount);
}

// format a broken api price with int64's encoded as strings
export function formatBrokenPrice(p) {
  p.amount = parseInt(p.amount);
  return formatPrice(p);
}

// returns just the unit for dashboard tiles
export function formatCurrencyUnit(currency, value) {
  if (currency == 'USD') {
    return '$';
  }
  return '';
}

// returns the numeral for dashboard tiles
export function formatCurrencyNumeral(currency, value) {
  let str = '';
  if (value == 0) {
    return '–';
  }
  str = formatCurrency(currency, value);
  // replace all characters except 0-9,. in string
  return str.replace(/[^0-9,.]/g, '');
}

export function decodeUnit(unit) {
  if (!!unit && typeof unit.search === 'function') {
    if (unit.indexOf('GENDER_') > -1) {
      unit = unit.replace('GENDER_', '');
    } else if (unit.indexOf('UNITS_') > -1) {
      unit = unit.replace('UNITS_', '');
    } else if (unit.indexOf('AGE_BAND_') > -1) {
      unit = unit.replace('AGE_BAND_', '');
    } else if (unit.indexOf('STATUS_') > -1) {
      unit = unit.replace('STATUS_', '');
    } else if (unit.indexOf('SOURCE_') > -1) {
      unit = unit.replace('SOURCE_', '');
    } else if (unit.indexOf('TYPE_') > -1) {
      unit = unit.replace('TYPE_', '');
    } else if (unit.indexOf('PRICE_ON_') > -1) {
      unit = unit.replace('PRICE_ON_', '');
    }
  }
  return unit;
}

export function capitalize(name) {
  const letters = name.split('');
  letters[0] = letters[0].toUpperCase();
  return letters.join('');
}

export function formatStatusEnum(status) {
  return toTitleCase(status.substring(7).replace('_', ' '));
}

export function formatEnum(str) {
  if (str == undefined || str == null) {
    return '–';
  }
  return toTitleCase(decodeUnit(str).replace(/_/g, ' '));
}
export function formatUnknown(str) {
  return str.toLowerCase().indexOf('unknown') > -1 ? 'Unknown' : str;
}

function toTitleCase(str) {
  // http://stackoverflow.com/questions/196972/convert-string-to-title-case-with-javascript
  return str.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
}
export const enDash = (value) => {
  if (value && value != '' && value.toLowerCase().indexOf('unknown') < 0) {
    return value;
  }
  return '–';
};
export const featureEnabled = f =>
  window.featureToggle && window.featureToggle.active(f);

export const getLocale = (f) => {
  let search = window.location.search,
    remove = '',
    keys = [];

  if (search.indexOf('locale') > -1) {
    if (search.indexOf('&') > -1) {
      keys = search.split('&');
      keys.map((v) => {
        if (v.indexOf('locale') > -1) {
          locale = v;
        }
      });
      remove = 'locale=';
    } else {
      remove = '?locale=';
    }
    return locale.replace(remove, '');
  }
  return false;
};
export const getOrgType = (role) => {
  let type = 'TYPE_UNKNOWN',
    types = ['supplier', 'reseller', 'pass'];
  if (role) {
    types.map((t, i) => {
      if (role.toLowerCase().indexOf(t) > -1) {
        type = `TYPE_${t.toUpperCase()}`;
      }
    });
  }
  return type;
};

export const toTravelerEnum = type => `TYPE_${type.toUpperCase().split(' ').join('_')}`;
export const serializeTravelerTypes = (input, useType) => {
  const types = [];
  input.map((t) => {
    if (useType) {
      if (typeof t.type === 'undefined') {
        types.push({
          type: t.typeOf.indexOf('TYPE_') > -1 ? t.typeOf : toTravelerEnum(t.typeOf),
          from: t.ageMin,
          to: t.ageMax,
        });
      } else {
        types.push({
          type: t.type.indexOf('TYPE_') > -1 ? t.type : toTravelerEnum(t.type),
          from: t.from,
          to: t.to,
        });
      }
    } else if (typeof t.type === 'undefined') {
      types.push({
        typeOf: t.typeOf.indexOf('TYPE_') > -1 ? t.typeOf : toTravelerEnum(t.typeOf),
        ageMin: t.ageMin,
        ageMax: t.ageMax,
      });
    } else {
      types.push({
        typeOf: t.type.indexOf('TYPE_') > -1 ? t.type : toTravelerEnum(t.type),
        ageMin: t.from,
        ageMax: t.to,
      });
    }
  });
  return types;
};

export const isInternalStaff = staff => staff !== false && (staff.indexOf('REDEAM') > -1 || staff.indexOf('ACTOUREX') > -1);
export const isSupplierStaff = staff => staff !== false && staff.indexOf('SUPPLIER') > -1;
export const isResellerStaff = staff => staff !== false && staff.indexOf('RESELLER') > -1;
export const isPassIssuerStaff = staff => staff !== false && staff.indexOf('PASS_ISSUER') > -1;
export const isSupplierBusinessStaff = staff => staff !== false && staff.indexOf('SUPPLIER_BUSINESS_STAFF') > -1;

export function copyTextToClipboard(text) {
  const textArea = document.createElement('textarea');

  //
  // *** This styling is an extra step which is likely not required. ***
  //
  // Why is it here? To ensure:
  // 1. the element is able to have focus and selection.
  // 2. if element was to flash render it has minimal visual impact.
  // 3. less flakyness with selection and copying which **might** occur if
  //    the textarea element is not visible.
  //
  // The likelihood is the element won't even render, not even a flash,
  // so some of these are just precautions. However in IE the element
  // is visible whilst the popup box asking the user for permission for
  // the web page to copy to the clipboard.
  //

  // Place in top-left corner of screen regardless of scroll position.
  textArea.style.position = 'fixed';
  textArea.style.top = 0;
  textArea.style.left = 0;

  // Ensure it has a small width and height. Setting to 1px / 1em
  // doesn't work as this gives a negative w/h on some browsers.
  textArea.style.width = '2em';
  textArea.style.height = '2em';

  // We don't need padding, reducing the size if it does flash render.
  textArea.style.padding = 0;

  // Clean up any borders.
  textArea.style.border = 'none';
  textArea.style.outline = 'none';
  textArea.style.boxShadow = 'none';

  // Avoid flash of white box if rendered for any reason.
  textArea.style.background = 'transparent';


  textArea.value = text;

  document.body.appendChild(textArea);

  textArea.select();

  try {
    const successful = document.execCommand('copy');
    const msg = successful ? 'successful' : 'unsuccessful';
    console.log(`Copying text command was ${msg}`);
  } catch (err) {
    console.log('Oops, unable to copy');
  }

  document.body.removeChild(textArea);
}
