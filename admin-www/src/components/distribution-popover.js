import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

export default class DistributionPopover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      label: '',
      distributions: [
        {name: "Public", value: 'public'},
        {name: "Private", value: 'private'}
      ]
    }
  }
  componentDidMount () {
    this.setState({
      label: this.props.value == '' ? "Choose Distribution Type" : this.props.value
    })
  }
  handleTouchTap (event) {
    event.preventDefault();
    this.setState({
        open: true,
        anchorEl: event.currentTarget
    })
  }
  handleRequestClose (evt) {
    this.setState({
       open: false
    })
  }
  onMenuItem(distribution) {
    this.props.onChange(distribution.value)
    this.setState({
      open: false,
      label: distribution.name
    })
  }
  render() {
    return (
      <div className="roles-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={e => this.handleRequestClose(e)}
        >
          <Menu>
            {
                this.state.distributions.map((distribution, i) => {
                    return (
                        <MenuItem onClick={()=>{this.onMenuItem(distribution)}}
                                  primaryText={distribution.name}
                                  key={i}
                         />
                    )
                })
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}

DistributionPopover.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
}
