import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { spoofUserRole } from '../ax-redux/actions/auth'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

let component = null;

export default class RolesPopover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      label: "Add Role To User"
    };
    component = this;
  }

  handleTouchTap (event) {
    // This prevents ghost click.
    event.preventDefault();
    component.setState({
        open: true,
        anchorEl: event.currentTarget
    });

  }

  handleRequestClose (evt) {
    component.setState({
       open: false
    });
  }

  onMenuItem(item, label) {
    this.props.onRole(item)
    this.setState({
      open: false
    })
  }

  render() {
      let roles = [
          {role: "ACTOUREX_CUSTOMER_SUPPORT_STAFF", name: "Redeam Customer Support Staff"},
          {role: "ACTOUREX_BUSINESS_STAFF", name: "Redeam Business Staff"},
          {role: "ACTOUREX_TECHNICAL_STAFF", name: "Redeam Technical Staff"},
          {role: "SUPPLIER_REDEMPTION_STAFF", name: "Supplier Redemption Staff"},
          {role: "SUPPLIER_REDEMPTION_SUPERVISOR", name: "Supplier Redemption Supervisor"},
          {role: "SUPPLIER_BUSINESS_STAFF", name: "Supplier Business Staff"},
          {role: "RESELLER_BUSINESS_STAFF", name: "Reseller Business Staff"},
          {role: "PASS_ISSUER_BUSINESS_STAFF", name: "Pass Issuer Business Staff"}
      ],
      options = this.props.options;

    return (
      <div className="roles-popover">
        <RaisedButton
          onTouchTap={this.handleTouchTap}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}
        >
          <Menu>
            {
                roles.map((role, i) => {
                    let allowed = true;
                    if (!!options) {
                        allowed = false;
                        options.map(opt => {
                            if (opt == role.role) {
                                allowed = true;
                            }
                        });
                    }
                    if (!allowed) {
                        return "";
                    } else {
                        return (
                            <MenuItem key={i} onClick={()=>{component.onMenuItem(role.role, role.name)}} primaryText={role.name} />
                        )
                    }
                })
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}

RolesPopover.propTypes = {
  onRole: PropTypes.func.isRequired
}
