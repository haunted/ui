import React, { Component, PropTypes } from 'react'

class Display extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    let out = null,
        hide = this.props.cfg[this.props.col]
    if (hide == null || hide == false) {
       out = this.props.children
    }
    return out
  }
}

Display.propTypes = {
  cfg: PropTypes.array.isRequired,
  col: PropTypes.string.isRequired
}

export default Display
