import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { spoofUserRole } from '../ax-redux/actions/auth'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

export default class CurrencyPopover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      label: this.props.value
    }
  }
  handleTouchTap (event) {
    event.preventDefault();
    this.setState({
        open: true,
        anchorEl: event.currentTarget
    })
  }
  handleRequestClose (evt) {
    this.setState({
       open: false
    })
  }
  onMenuItem(currency) {
    this.props.onChange(currency)
    this.setState({
      open: false,
      label: currency
    })
  }
  render() {
      let currencies = [
        "USD"
      ]

    return (
      <div className="roles-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={e => this.handleRequestClose(e)}
        >
          <Menu>
            {
                currencies.map((currency, i) => {
                    return (
                        <MenuItem onClick={()=>{this.onMenuItem(currency)}}
                                  primaryText={currency}
                                  key={i}
                         />
                    )
                })
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}

CurrencyPopover.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
}
