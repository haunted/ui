import React, {Component, PropTypes} from 'react'
import TopBar from './top-bar.js'
import DashMenu from './dash-menu.js'
import SideMenu from './side-menu.js'
import AdminMenu from './admin-menu.js'
import MainView from './main-view.js'


class App extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
        <div className="app">
            <TopBar title={this.props.pageTitle} />
            <MainView>
              {this.props.children}
            </MainView>
            <SideMenu />
            <AdminMenu />
        </div>
    );
  }
}

App.propTypes = {

}

import { connect } from 'react-redux'

export default connect(
  state => {
    return {
      pageTitle: state.app.pageTitle,
    }
  },
  dispatch => {
    return {

    }
  }
)(App)
