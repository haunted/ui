import React, { Component, PropTypes } from 'react'
import CircularProgress from 'material-ui/CircularProgress';


const styles = {
  container: {
    display: 'flex',
    height: '100vh',
    justifyContent: 'center',
  },
  inner: {
    alignSelf: 'center',
  },
}

class LoadingScreen extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div style={styles.container}>
        <div style={styles.inner}>
          <CircularProgress color={"#27367a"} size={96} />
        </div>
      </div>
    )
  }
}

import { connect } from 'react-redux'

export default connect(
  state => {
    return {
      // auth: state.auth
    }
  },
  dispatch => {
    return {
      // listAccounts: () => {
      //   dispatch(listAccounts())
      // },
    }
  }
)(LoadingScreen)
