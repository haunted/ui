import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import DropDownMenu from 'material-ui/DropDownMenu'
import moment from 'moment-timezone'


const styles = {
  to: {
    position: 'relative',
    bottom: '1.8em'
  }
}

export default class OpenHours extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      from: 32400000,
      to: 32400000,
      times: []
    }
  }
  componentDidMount () {
    let i = 0,
        times = []
    while (i < 48) {
      times.push(1800000 * i)
      i++
    }
    this.setState({
      from: this.props.from,
      to: this.props.to,
      times
    })
  }
  handleChange (event, index, value, which) {
    if (which == 'to') {
      this.setState({ to: value })
      this.props.onChange({
        to: value,
        from: this.state.from
      })
    } else {
      this.setState({ from: value })
      this.props.onChange({
        to: this.state.to,
        from: value
      })
    }
  }
  displayTime (time) {
    let date = new Date()
    date.setHours(0)
    date.setMinutes(0)
    date.setSeconds(0)
    let dateTime = new Date(date.getTime() + time),
        hours = dateTime.getHours(),
        minutes = dateTime.getMinutes()
    return moment(dateTime).format("hh:mm A")
    //return `${hours}:${minutes < 10 ? '0'+minutes : minutes}`
  }
  render() {
    return (
      <div>
        <DropDownMenu value={this.state.from}
                      onChange={ (evt, i, v) => { this.handleChange(evt, i, v, 'from')} }
        >
          {
            this.state.times.map((time, i)=> {
              let timeValue = this.displayTime(time)
              return (
                <MenuItem primaryText={timeValue}
                          value={time}
                          key={i}
                />
              )
            })
          }
        </DropDownMenu>
        <span style={styles.to}> To </span>
        <DropDownMenu value={this.state.to}
                      onChange={ (evt, i, v) => { this.handleChange(evt, i, v, 'to')} }
        >
          {
            this.state.times.map((time, i) => {
              let timeValue = this.displayTime(time)
              return (
                <MenuItem primaryText={timeValue}
                          value={time}
                          key={i}
                />
              )
            })
          }
        </DropDownMenu>
      </div>
    )
  }
}

OpenHours.propTypes = {
  onChange: PropTypes.func.isRequired,
  from: PropTypes.number.isRequired,
  to: PropTypes.number.isRequired
}
