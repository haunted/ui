import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import moment from 'moment-timezone'

export default class TimezonesPopover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      label: '',
      options: []
    }
  }
  componentDidMount () {
    let options = []
    moment.tz.names().map((n, i) => {
      options.push({
        name: `${n} ( ${moment.tz("2016-01-01", n).format("Z")} )`,
        value: n
      })
    })
    this.setState({
      options
    })
  }
  handleTouchTap (event) {
    event.preventDefault();
    this.setState({
        open: true,
        anchorEl: event.currentTarget
    })
  }
  handleRequestClose (evt) {
    this.setState({
       open: false
    })
  }
  onMenuItem(option) {
    this.props.onChange(option.value)
    this.setState({
      open: false
    })
  }
  render() {
    return (
      <div className="roles-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.props.value == '' ? "Select Timezone" : this.props.value}
          style={this.props.style || {}}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={e => this.handleRequestClose(e)}
        >
          <Menu>
            {
                this.state.options.map((option, i) => {
                    return (
                        <MenuItem onClick={()=>{this.onMenuItem(option)}}
                                  primaryText={option.name}
                                  key={i}
                         />
                    )
                })
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}

TimezonesPopover.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  style: PropTypes.object
}
