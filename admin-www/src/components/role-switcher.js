import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import {
  isInternalStaff,
  formatEnum,
} from '../util';
import * as appActions from '../ax-redux/actions/app';
import * as suppliersActions from '../ax-redux/actions/suppliers';
import * as resellersActions from '../ax-redux/actions/resellers';
import * as passIssuersActions from '../ax-redux/actions/pass-issuers';

class RoleSwitcher extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      label: '',
    };
  }

  componentWillMount() {
    if (isInternalStaff(this.props.role)) {
      this.props.listSuppliers();
      this.props.listResellers();
      this.props.listPassIssuers();
    }
  }

  componentWillReceiveProps(nextProps) {
    const roleChanged = nextProps.role !== this.props.role;

    if (roleChanged && isInternalStaff(nextProps.role)) {
      this.props.listSuppliers();
      this.props.listResellers();
      this.props.listPassIssuers();
    }
  }

  handleTouchTap(event) {
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  }

  handleRequestClose() {
    this.setState({ open: false });
  }

  label(defaultValue) {
    return this.state.label ? formatEnum(this.state.label) : defaultValue;
  }

  switchRoles(role) {
    this.props.spoofUserRole(role);

    this.setState({
      label: role,
      open: false,
    });
  }

  render() {
    return (
      <div className="role-switcher">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.label('Switch User Role')}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={e => this.handleRequestClose(e)}
        >
          <Menu>
            <MenuItem
              onClick={() => { this.switchRoles('ACTOUREX_CUSTOMER_SUPPORT_STAFF'); }}
              primaryText="Redeam Customer Support Staff"
            />
            <MenuItem
              onClick={() => { this.switchRoles('ACTOUREX_BUSINESS_STAFF'); }}
              primaryText="Redeam Business Staff"
            />
            <MenuItem
              onClick={() => { this.switchRoles('ACTOUREX_TECHNICAL_STAFF'); }}
              primaryText="Redeam Technical Staff"
            />
            <MenuItem
              onClick={() => { this.switchRoles('SUPPLIER_REDEMPTION_STAFF'); }}
              primaryText="Supplier Redemption Staff"
            />
            <MenuItem
              onClick={() => { this.switchRoles('SUPPLIER_REDEMPTION_SUPERVISOR'); }}
              primaryText="Supplier Redemption Supervisor"
            />
            <MenuItem
              onClick={() => { this.switchRoles('SUPPLIER_BUSINESS_STAFF'); }}
              primaryText="Supplier Business Staff"
            />
            <MenuItem
              onClick={() => { this.switchRoles('RESELLER_BUSINESS_STAFF'); }}
              primaryText="Reseller Business Staff"
            />
            <MenuItem
              onClick={() => { this.switchRoles('PASS_ISSUER_BUSINESS_STAFF'); }}
              primaryText="Pass Issuer Business Staff"
            />
          </Menu>
        </Popover>
      </div>
    );
  }
}

RoleSwitcher.propTypes = {
  role: PropTypes.string.isRequired,
  spoofUserRole: PropTypes.func.isRequired,
  listSuppliers: PropTypes.func.isRequired,
  listResellers: PropTypes.func.isRequired,
  listPassIssuers: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  role: state.app.role,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  ...appActions,
  ...suppliersActions,
  ...resellersActions,
  ...passIssuersActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RoleSwitcher);
