import React, {Component} from 'react'
import { browserHistory } from 'react-router'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import IconMenu from 'material-ui/IconMenu'
import MenuItem from 'material-ui/MenuItem'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import NavigationMenu from 'material-ui/svg-icons/navigation/menu'
import RoleSwitcher from './role-switcher'
import OrgSwitcher from './org-switcher'
import CatalogSwitcher from './catalog-switcher'
import { grey50 } from 'material-ui/styles/colors'

class TopBar extends Component {
  
  constructor(props) {
    super(props)
  }

  componentWillMount ( ) {

    this.setState({
      supplierId: !!this.props.supplierId && this.props.supplierId != -1 ? this.props.supplierId : "",
      resellerId: !!this.props.resellerId && this.props.resellerId != -1 ? this.props.resellerId : "",
      passIssuerId: !!this.props.passIssuerId && this.props.passIssuerId != -1 ? this.props.passIssuerId : ""
    })

  }

  componentWillReceiveProps ( nextProps ) {

    if ( nextProps.supplierId != this.props.supplierId && !!nextProps.supplierId && nextProps.supplierId != -1 )

      this.setState({
        supplierId: nextProps.supplierId
      })

      
    if ( nextProps.resellerId != this.props.resellerId && !!nextProps.resellerId && nextProps.resellerId != -1 )

      this.setState({
        resellerId: nextProps.resellerId
      })


    if ( nextProps.passIssuerId != this.props.passIssuerId && !!nextProps.passIssuerId && nextProps.passIssuerId != -1 )

      this.setState({
        passIssuerId: nextProps.passIssuerId
      })


  }

  logout() {
    browserHistory.push("/")
    this.props.logout()
  }

  render() {
    let orgSwitcher = this.props.initialRole !== false ? (
      <OrgSwitcher key="1" />
    ) : ''
    return (
        <AppBar
          title={(this.props.title.length > 0 ? this.props.title : "Redeam Admin")}
      	  className={"top-bar no-print"}
          iconElementLeft={
              <IconButton onClick={e => {this.props.toggleAdminMenu()}} ><NavigationMenu /></IconButton>
          }
          iconElementRight={
              <div className="app-bar-right">
                  <CatalogSwitcher supplierId={ this.state.supplierId }
                                   resellerId={ this.state.resellerId }
                                   passIssuerId={ this.state.passIssuerId }
                  />
                  {
                    this.props.enableRoleSwitcher ? [orgSwitcher, <RoleSwitcher  key="2"/>] : ""
                  }
                  <IconMenu
                    iconButtonElement={
                      <IconButton>
                            <MoreVertIcon color={grey50} />
                      </IconButton>
                    }
                    targetOrigin={{horizontal: 'right', vertical: 'top'}}
                    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
                    style={{zIndex: 9999}}
                  >
                    <MenuItem primaryText="Logout" onClick={e => {this.logout()}} />
                  </IconMenu>
              </div>
          }
        />
    )
  }
}

import { connect } from 'react-redux'
import { logout } from '../ax-redux/actions/auth'
import { dashboardToggleAdminMenu } from '../ax-redux/actions/dashboard'
import { ACTOUREX_TECHNICAL_STAFF } from '../ax-redux/constants'

export default connect(
  state => {

    return {
      supplierId: state.reportFilters.adminSupplierId != -1 ? state.reportFilters.adminSupplierId : state.app.user.supplierId,
      resellerId: state.reportFilters.adminResellerId != -1 ? state.reportFilters.adminResellerId : state.app.user.resellerId,
      passIssuerId: state.reportFilters.adminPassIssuerId != -1 ? state.reportFilters.adminPassIssuerId : state.app.user.passIssuerId,
      adminPassIssuerId: state.reportFilters.adminPassIssuerId,
      initialRole: state.app.initialRole,
      enableRoleSwitcher: state.app.role == ACTOUREX_TECHNICAL_STAFF ||
                          state.app.initialRole == ACTOUREX_TECHNICAL_STAFF
    }
  },
  dispatch => {
    return {
      logout: () => {
        dispatch(logout())
      },
      toggleAdminMenu: () => {
        dispatch(dashboardToggleAdminMenu())
      }
    }
  }
)(TopBar)
