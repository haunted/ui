import React, { Component, PropTypes } from 'react'
import {
    ACTOUREX_CUSTOMER_SUPPORT_STAFF,
    ACTOUREX_BUSINESS_STAFF,
    ACTOUREX_TECHNICAL_STAFF,
    SUPPLIER_REDEMPTION_STAFF,
    SUPPLIER_REDEMPTION_SUPERVISOR,
    SUPPLIER_BUSINESS_STAFF,
    RESELLER_BUSINESS_STAFF,
    PASS_ISSUER_BUSINESS_STAFF
} from '../../ax-redux/constants'
import { browserHistory } from 'react-router'
import RedeamManagement from '../manage-business/redeam'

class Redeam extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    renderSwitch() {
        switch (this.props.role) {
            case ACTOUREX_CUSTOMER_SUPPORT_STAFF:
            return <RedeamManagement>{this.props.children}</RedeamManagement>
            case ACTOUREX_BUSINESS_STAFF:
            return <RedeamManagement>{this.props.children}</RedeamManagement>
            case ACTOUREX_TECHNICAL_STAFF:
            return <RedeamManagement>{this.props.children}</RedeamManagement>
        }
        return "role not supported"
    }
    render() {
        return (
            <div className="manage-business">
            {this.renderSwitch()}
            </div>
        )
    }

  setItemsFor(role) {
    switch (role) {
      case ACTOUREX_CUSTOMER_SUPPORT_STAFF:
        this.props.setPageTitle("Redeam")
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
            { title: 'Failed Orders', url: '/app/redeam/failed-orders'},
            { title: 'Devices', url: '/app/redeam/devices'},
        ])
        break
      case ACTOUREX_BUSINESS_STAFF:
        this.props.setPageTitle("Redeam")
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
            { title: 'Search', url:'/app/redeam/search' },
            { title: 'Resellers', url:'/app/redeam/resellers' },
            { title: 'Suppliers', url:'/app/redeam/suppliers' },
            { title: 'Pass Issuers', url:'/app/redeam/pass-issuers' },
            { title: 'Failed Orders', url: '/app/redeam/failed-orders'},
            { title: 'Devices', url: '/app/redeam/devices'},
        ])
        break
      case ACTOUREX_TECHNICAL_STAFF:
        this.props.setPageTitle("Redeam")
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
          { title: 'Search', url:'/app/redeam/search'},
          { title: 'Users', url:'/app/redeam/users'},
          { title: 'Groups', url:'/app/redeam/groups'},
          { title: 'Resellers', url:'/app/redeam/resellers'},
          { title: 'Suppliers', url:'/app/redeam/suppliers'},
          { title: 'Pass Issuers', url:'/app/redeam/pass-issuers'},
          { title: 'Failed Orders', url: '/app/redeam/failed-orders'},
          { title: 'Devices', url: '/app/redeam/devices'},
        ])
        break
      case SUPPLIER_REDEMPTION_STAFF:
        this.props.setSideMenuItems([

        ])
        break
      case SUPPLIER_REDEMPTION_SUPERVISOR:
        this.props.setSideMenuItems([])
        break
      case SUPPLIER_BUSINESS_STAFF:
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
         
        ])
        break
      case RESELLER_BUSINESS_STAFF:
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
           
        ])
      break
      case PASS_ISSUER_BUSINESS_STAFF:
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
            
        ])
        break
    }

  }
      componentDidMount () {
        this.props.setPageTitle("Manage Business");
        //this.props.impersonate("ACTOUREX_TECHNICAL_STAFF");
        this.setItemsFor(this.props.role);
        if (this.props.role == PASS_ISSUER_BUSINESS_STAFF) {
          browserHistory.push("/app/manage-business/products")
        }
      }

      componentWillUpdate(nextProps, nextState) {
          if (this.props.role != nextProps.role) {
            this.setItemsFor(nextProps.role);
          }
      }
}

import { connect } from 'react-redux'

import { setSideMenuItems, setPageTitle, setSideMenuTitle } from '../../ax-redux/actions/app'
import { spoofUserRole } from '../../ax-redux/actions/auth'

export default connect(
    state => {
        return {
            subsection: state.routing.locationBeforeTransitions.pathname.split("manage-business").pop(),
            role: state.app.role,
            app: state.app,
        }
    },
    dispatch => {
        return {
            setPageTitle: title => {
                dispatch(setPageTitle(title))
            },
            setSideMenuItems: items => {
                dispatch(setSideMenuItems(items))
            },
            setSideMenuTitle: title => {
                dispatch(setSideMenuTitle(title))
            },
            impersonate: (role) => {
                dispatch(spoofUserRole(role))
            }
        }
    }
)(Redeam)
