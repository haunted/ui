import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import {
  TextField,
  Toggle,
  DatePicker,
  DropDown,
  AutoComplete,
} from '../formElements';
import * as resellerActions from '../../ax-redux/actions/resellers';

const styles = {
  labelColumn: {
    width: '200px',
  },
  cell: {
    overflow: 'visible',
  },
};

const priceOnOptions = [
  { label: 'Sale', value: 'PRICE_ON_SALE' },
  { label: 'Redemption', value: 'PRICE_ON_REDEMPTION' },
];

class ContractForm extends Component {
  constructor(props) {
    super(props);

    this.handleResellerChange = this.handleResellerChange.bind(this);
    this.handleCatalogChange = this.handleCatalogChange.bind(this);
  }

  handleResellerChange(e, name) {
    const {
      listResellerCatalogs,
      change,
      resellers,
    } = this.props;

    const reseller = resellers.find(r => r.name === name);

    if (reseller) {
      listResellerCatalogs(reseller.id);
      change('sellerCatalogId', '');
    }
  }

  handleCatalogChange(e, catalogId) {
    const {
      formValues,
      change,
      resellers,
      resellerCatalogs,
      supplier,
    } = this.props;

    if (!formValues.name) {
      const reseller = resellers.find(r => r.name === formValues.resellerName);
      const catalog = resellerCatalogs.find(c => c.id === catalogId);

      const defaultName = `${supplier ? `${supplier.name} - ` : ''}${reseller.name} (${catalog.name})`;

      change('name', defaultName);
    }
  }

  render() {
    const {
      formValues,
      priceLists,
      resellers,
      resellersFetching,
      resellerCatalogs,
      resellerCatalogsFetching,
      handleSubmit,
      onCancel,
    } = this.props;

    return (
      <form onSubmit={handleSubmit}>
        <Table fixedHeader={false}>
          <TableBody displayRowCheckbox={false}>
            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelColumn}>
                Reseller
              </TableRowColumn>
              <TableRowColumn style={styles.cell}>
                {resellersFetching ? (
                  <CircularProgress size={24} thickness={3} />
                ) : (
                  <Field
                    name="resellerName"
                    component={AutoComplete}
                    hintText="Select reseller"
                    dataSource={resellers.map(r => r.name)}
                    onChange={this.handleResellerChange}
                  />
                )}
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelColumn}>
                Reseller Catalog
              </TableRowColumn>
              <TableRowColumn style={styles.cell}>
                {resellerCatalogsFetching ? (
                  <CircularProgress size={24} thickness={3} />
                ) : (
                  <Field
                    name="sellerCatalogId"
                    label="Select reseller catalog"
                    component={DropDown}
                    options={resellerCatalogs.map(c => ({ label: c.name, value: c.id }))}
                    disabled={!resellerCatalogs.length}
                    onChange={this.handleCatalogChange}
                  />
                )}
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelColumn}>
                Name
              </TableRowColumn>
              <TableRowColumn style={styles.cell}>
                <Field
                  name="name"
                  label="Contract Name"
                  component={TextField}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelColumn}>
                Auto Renew
              </TableRowColumn>
              <TableRowColumn style={styles.cell}>
                <Field
                  name="autoRenew"
                  component={Toggle}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelColumn}>
                Valid From Range
              </TableRowColumn>
              <TableRowColumn style={styles.cell}>
                <Field
                  name="hasDateRange"
                  component={Toggle}
                />
              </TableRowColumn>
            </TableRow>

            {formValues.hasDateRange && (
              <TableRow selectable={false}>
                <TableRowColumn style={styles.labelColumn}>
                  Valid From
                </TableRowColumn>
                <TableRowColumn style={styles.cell}>
                  <Field
                    name="validFrom"
                    component={DatePicker}
                    hintText="Enter Date"
                    maxDate={new Date(formValues.validUntil)}
                  />
                </TableRowColumn>
              </TableRow>
            )}

            {formValues.hasDateRange && (
              <TableRow selectable={false}>
                <TableRowColumn style={styles.labelColumn}>
                  Valid Until
                </TableRowColumn>
                <TableRowColumn style={styles.cell}>
                  <Field
                    name="validUntil"
                    component={DatePicker}
                    hintText="Enter Date"
                    minDate={new Date(formValues.validFrom)}
                  />
                </TableRowColumn>
              </TableRow>
            )}

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelColumn}>
                Price List
              </TableRowColumn>
              <TableRowColumn style={styles.cell}>
                <Field
                  name="priceListId"
                  label="Select price list"
                  component={DropDown}
                  options={priceLists.map(pl => ({ label: pl.name, value: pl.id }))}
                  disabled={!priceLists.length}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelColumn}>
                Priced At
              </TableRowColumn>
              <TableRowColumn style={styles.cell}>
                <Field
                  name="terms.priceOn"
                  component={DropDown}
                  options={priceOnOptions}
                />
              </TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
        <div style={{ float: 'right' }}>
          <FlatButton
            label="Cancel"
            primary={false}
            onClick={onCancel}
          />
          <FlatButton
            label="Submit"
            onClick={handleSubmit}
            primary
          />
        </div>
      </form>
    );
  }
}

ContractForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  change: PropTypes.func.isRequired,
  resellersFetching: PropTypes.bool.isRequired,
  listResellerCatalogs: PropTypes.func.isRequired,
  formValues: PropTypes.shape({
    hasDateRange: PropTypes.bool,
    validFrom: PropTypes.string,
    validUntil: PropTypes.string,
    name: PropTypes.string,
    resellerName: PropTypes.string,
  }).isRequired,
  priceLists: PropTypes.arrayOf(PropTypes.object).isRequired,
  resellers: PropTypes.arrayOf(PropTypes.object).isRequired,
  resellerCatalogs: PropTypes.arrayOf(PropTypes.object).isRequired,
  resellerCatalogsFetching: PropTypes.bool.isRequired,
  supplier: PropTypes.shape({
    name: PropTypes.string,
  }),
};

ContractForm.defaultProps = {
  supplier: null,
  selectedReseller: null,
};

const mapStateToProps = (state) => {
  const resellers = state.resellers.list.data ? state.resellers.list.data.resellers : [];

  const fields = [
    'hasDateRange',
    'validFrom',
    'validUntil',
    'name',
    'resellerName',
  ];
  const formSelector = formValueSelector('addContract');
  const formValues = formSelector(state, ...fields);

  return {
    resellers,
    resellersFetching: state.resellers.list.fetching,
    resellerCatalogs: state.resellers.listCatalogs.data ?
      state.resellers.listCatalogs.data.catalogs :
      [],
    resellerCatalogsFetching: state.resellers.listCatalogs.fetching,
    supplier: state.suppliers.get.data ?
      state.suppliers.get.data.supplier.supplier :
      null,
    priceLists: state.priceLists.listPriceLists.data ?
      state.priceLists.listPriceLists.data.priceLists :
      [],
    formValues,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  ...resellerActions,
}, dispatch);

const defaultFrom = new Date();
const defaultUntil = new Date();

const validate = (values, props) => {
  const errors = {};

  if (!values.resellerName) {
    errors.resellerName = 'Reseller is required';
  } else if (!props.resellers.find(r => r.name === values.resellerName)) {
    errors.resellerName = 'Please select a reseller from the list';
  } else if (!values.sellerCatalogId) {
    errors.sellerCatalogId = 'Reseller catalog is required';
  }

  if (!values.name) {
    errors.name = 'Name is required';
  }

  if (values.hasDateRange && !values.validFrom) {
    errors.validFrom = 'Valid From date is required';
  }

  if (values.hasDateRange && !values.validUntil) {
    errors.validUntil = 'Valid Until date is required';
  }

  if (!values.priceListId) {
    errors.priceListId = 'Price List is required';
  }

  return errors;
};

defaultUntil.setFullYear(defaultUntil.getFullYear() + 1);

const ContractFormRedux = reduxForm({
  form: 'addContract',
  initialValues: {
    resellerName: '',
    sellerCatalogId: '',
    name: '',
    autoRenew: false,
    hasDateRange: false,
    validFrom: defaultFrom.toISOString(),
    validUntil: defaultUntil.toISOString(),
    priceListId: '',
    terms: {
      priceOn: 'PRICE_ON_SALE',
    },
  },
  validate,
})(ContractForm);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContractFormRedux);
