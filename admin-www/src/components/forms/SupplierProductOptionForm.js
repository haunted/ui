import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  formValueSelector,
  Field,
} from 'redux-form';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import {
  TextField,
  DropDown,
  DatePicker,
  Toggle,
} from '../formElements';

const styles = {
  overflow: {
    overflow: 'visible',
  },
  labelCell: {
    width: '40%',
  },
};

class SupplerProductOptionForm extends Component {
  render() {
    const {
      handleSubmit,
      travelerTypes,
      formValues,
      onCancel,
    } = this.props;

    const travelerTypesOptions = travelerTypes.map(t => ({
      value: t.type,
      label: t.type.replace('TYPE_', ''),
    }));

    return (
      <form onSubmit={handleSubmit}>
        <Table>
          <TableBody displayRowCheckbox={false}>
            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                Title
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <Field
                  label="Option Title"
                  name="title"
                  component={TextField}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                Description
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <Field
                  label="Option Description"
                  name="desc"
                  component={TextField}
                  multiLine
                  rowsMax={8}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                Code
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <Field
                  label="Option Code"
                  name="code"
                  component={TextField}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                Traveler Type
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <Field
                  label="Select Traveler Type"
                  name="travelerType"
                  component={DropDown}
                  options={travelerTypesOptions}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                Min Travelers
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <Field
                  label="Min Travelers"
                  name="minTravelers"
                  component={TextField}
                  type="number"
                  min={1}
                  max={99}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                Max Travelers
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <Field
                  label="Max Travelers"
                  name="maxTravelers"
                  component={TextField}
                  type="number"
                  min={1}
                  max={99}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                Valid From
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <Field
                  name="validFrom"
                  component={DatePicker}
                  hintText="Enter Date"
                  maxDate={new Date(formValues.validUntil)}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                Valid Ending?
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <Field
                    name="validEnding"
                    component={Toggle}
                    style={{ width: 'auto', marginRight: 12 }}
                  />

                  {formValues.validEnding && (
                    <Field
                      name="validUntil"
                      component={DatePicker}
                      hintText="Enter Date"
                      minDate={new Date(formValues.validFrom)}
                    />
                  )}
                </div>
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false}>
              <TableRowColumn style={styles.labelCell}>
                In Advanced Purchase?
              </TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                  <Field
                    name="useInAdvancedPurchase"
                    component={Toggle}
                    style={{ width: 'auto', marginRight: 12 }}
                  />

                  {formValues.useInAdvancedPurchase && (
                    <Field
                      label="Days"
                      name="inAdvance"
                      component={TextField}
                      type="number"
                      min={1}
                      max={99}
                    />
                  )}
                </div>
              </TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>

        <div style={{ textAlign: 'right' }}>
          <FlatButton
            label="Cancel"
            onClick={onCancel}
          />

          <RaisedButton
            label="Ok"
            type="submit"
            primary
          />
        </div>
      </form>
    );
  }
}

SupplerProductOptionForm.propTypes = {
  travelerTypes: PropTypes.arrayOf(PropTypes.object).isRequired,
  formValues: PropTypes.shape({}).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};

SupplerProductOptionForm.defaultProps = {
  product: null,
};

const mapStateToProps = (state) => {
  const {
    app,
    suppliers,
    orgSettings,
  } = state;

  const selector = formValueSelector('supplerProductOptionForm');
  const formValues = selector(state, 'validFrom', 'validEnding', 'validUntil', 'useInAdvancedPurchase');

  return {
    role: app.role,
    productSaving: suppliers.updateCatalogItem.fetching || suppliers.createCatalogItem.fetching,
    product: suppliers.getCatalogItem.data ? suppliers.getCatalogItem.data.item : null,
    travelerTypes: orgSettings.readOrgSettings.data ?
      orgSettings.readOrgSettings.data.travelerTypes :
      [],
    formValues,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch);

const SupplerProductOptionFormConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SupplerProductOptionForm);

const validate = (values) => {
  const errors = {};

  if (!values.title) {
    errors.title = 'Title is required';
  }

  if (!values.code) {
    errors.code = 'Code is required';
  }

  if (!values.travelerType) {
    errors.travelerType = 'Traveler Type is required';
  }

  if (values.useInAdvancedPurchase && !values.inAdvance) {
    errors.inAdvance = 'Number of days is required';
  }

  return errors;
};

const defaultFrom = new Date();
const defaultUntil = new Date();

defaultUntil.setFullYear(defaultUntil.getFullYear() + 1);

export default reduxForm({
  form: 'supplerProductOptionForm',
  validate,
  initialValues: {
    id: '',
    title: '',
    desc: '',
    code: '',
    travelerType: '',
    minTravelers: 1,
    maxTravelers: 99,
    validFrom: defaultFrom,
    validEnding: false,
    validUntil: defaultUntil,
    useInAdvancedPurchase: false,
    inAdvance: '',
  },
})(SupplerProductOptionFormConnected);
