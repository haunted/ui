import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  FieldArray,
} from 'redux-form';
import CircularProgress from 'material-ui/CircularProgress';
import Dialog from 'material-ui/Dialog';
import Divider from 'material-ui/Divider';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import RemoveIcon from 'material-ui/svg-icons/content/remove-circle-outline';
import EditIcon from 'material-ui/svg-icons/content/create';
import CreateIcon from 'material-ui/svg-icons/content/add-circle-outline';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import SupplierProductOptionForm from './SupplierProductOptionForm';
import {
  TextField,
  DropDown,
} from '../formElements';
import {
  isInternalStaff,
} from '../../util';
import * as supplierActions from '../../ax-redux/actions/suppliers';

const styles = {
  formFooter: {
    padding: '24px',
    textAlign: 'right',
  },
  submitButton: {
    marginRight: '1em',
  },
  overflow: {
    overflow: 'visible',
  },
};

const statuses = [
  { value: 'STATUS_DRAFT', label: 'Draft' },
  { value: 'STATUS_TEST_MODE', label: 'Test Mode' },
  { value: 'STATUS_LIVE', label: 'Live' },
  { value: 'STATUS_DISABLED', label: 'Disabled' },
  { value: 'STATUS_DELETED', label: 'Deleted' },
];

const types = [
  { value: 'TYPE_PRODUCT', label: 'Single Product' },
  { value: 'TYPE_PASS', label: 'Pass' },
  { value: 'TYPE_BUNDLE', label: 'Bundle' },
];

const cancel = () => browserHistory.push('/app/manage-business/products');

class SupplerProductForm extends Component {
  constructor(props) {
    super(props);

    this.processSubmit = this.processSubmit.bind(this);
    this.renderOptions = this.renderOptions.bind(this);
    this.addOption = this.addOption.bind(this);
    this.closeOptionDialog = this.closeOptionDialog.bind(this);

    this.state = {
      addOptionDialog: false,
      editOptionIndex: -1,
    };
  }

  processSubmit(values) {
    const {
      editMode,
      supplierId,
      adminSupplierId,
      supplierCatalogId,
      product,
      updateSupplierCatalogItem,
      createSupplierCatalogItem,
      travelerTypes,
    } = this.props;

    const productData = {
      ...values,
      id: editMode ? product.id : '',
      catalogId: editMode ? product.catalogId : supplierCatalogId,
      version: editMode ? product.version : 0,
      options: values.options.map((option, index) => {
        const { travelerType } = option;
        const typeObject = travelerTypes.find(t => t.type === travelerType);

        // If traveler type cannot be found then put the data which we've got from API
        return {
          ...option,
          status: values.status,
          minTravelers: parseInt(option.minTravelers, 10) || 0,
          maxTravelers: parseInt(option.maxTravelers, 10) || 0,
          inAdvance: parseInt(option.inAdvance, 10) || 0,
          travelerType: typeObject ? {
            typeOf: typeObject.type,
            ageMin: typeObject.from,
            ageMax: typeObject.to,
          } : product.options[index].travelerType,
        };
      }),
    };

    const actualSupplierId = adminSupplierId || supplierId;

    if (editMode) {
      updateSupplierCatalogItem(actualSupplierId, supplierCatalogId, product.id, productData);
    } else {
      createSupplierCatalogItem(actualSupplierId, supplierCatalogId, productData);
    }
  }

  addOption(data, optionsFields) {
    const {
      useInAdvancedPurchase,
      validEnding,
      ...optionData
    } = data;

    optionsFields.push(optionData);

    this.closeOptionDialog();
  }

  editOption(data, optionsFields) {
    const { editOptionIndex } = this.state;

    const {
      useInAdvancedPurchase,
      validEnding,
      ...optionData
    } = data;

    // remove old option at index
    optionsFields.remove(editOptionIndex);

    // add edited option
    optionsFields.push(optionData);

    // move edit option to the previous place
    optionsFields.move(optionsFields.length - 1, editOptionIndex);

    this.closeOptionDialog();
  }

  closeOptionDialog() {
    this.setState({
      addOptionDialog: false,
      editOptionIndex: -1,
    });
  }

  renderOptions({ fields }) {
    const {
      addOptionDialog,
      editOptionIndex,
    } = this.state;

    const options = fields.getAll();

    const optionFormProps = {
      onSubmit: editOptionIndex > -1 ?
        data => this.editOption(data, fields) :
        data => this.addOption(data, fields),
      onCancel: this.closeOptionDialog,
    };

    if (editOptionIndex > -1) {
      const optionData = fields.get(editOptionIndex);

      optionFormProps.initialValues = {
        ...optionData,
        useInAdvancedPurchase: optionData.inAdvance > 0,
        validEnding: !!optionData.validUntil,
        travelerType: optionData.travelerType.typeOf,
      };
    }

    return (
      <div>
        <Table className="products-table">
          <TableHeader
            adjustForCheckbox={false}
            displayRowCheckbox={false}
            enableSelectAll={false}
            displaySelectAll={false}
          >
            <TableRow>
              <TableHeaderColumn>Code</TableHeaderColumn>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Price</TableHeaderColumn>
              <TableHeaderColumn />
            </TableRow>
          </TableHeader>

          <TableBody displayRowCheckbox={false}>
            {options.map((o, index) => (
              <TableRow key={o.id || index} selectable={false}>
                <TableRowColumn>{o.code}</TableRowColumn>
                <TableRowColumn>{o.title}</TableRowColumn>
                <TableRowColumn>{o.retailPrice}</TableRowColumn>
                <TableRowColumn style={{ textAlign: 'right' }}>
                  <IconButton onClick={() => this.setState({ editOptionIndex: index })}>
                    <EditIcon color="#9ea0ae" />
                  </IconButton>

                  <IconButton onClick={() => fields.remove(index)}>
                    <RemoveIcon color="#d50201" />
                  </IconButton>
                </TableRowColumn>
              </TableRow>
            ))}

            <TableRow selectable={false}>
              <TableRowColumn colSpan={3} />
              <TableRowColumn style={{ textAlign: 'right' }}>
                <FlatButton
                  onClick={() => this.setState({ addOptionDialog: true })}
                  label="Add Product Option"
                  labelPosition="before"
                  icon={<CreateIcon />}
                  primary
                />
              </TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>

        <Dialog
          title="Product Option"
          modal={false}
          open={addOptionDialog || editOptionIndex > -1}
          autoDetectWindowHeight={false}
          style={{ overflow: 'auto' }}
          contentStyle={{ marginBottom: 48 }}
        >
          <SupplierProductOptionForm {...optionFormProps} />
        </Dialog>
      </div>
    );
  }

  render() {
    const {
      editMode,
      product,
      role,
      productSaving,
      handleSubmit,
    } = this.props;

    const {
      addOptionDialog,
      editOptionIndex,
    } = this.state;

    return (
      <form onSubmit={handleSubmit(this.processSubmit)}>
        <h2 style={{ color: '#434343' }}>
          {editMode ? 'Edit' : 'Add New'} Product
        </h2>

        <Table className="products-table">
          <TableBody displayRowCheckbox={false}>
            <TableRow selectable={false} >
              <TableRowColumn>Name</TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                <Field
                  label="Product Name"
                  name="name"
                  component={TextField}
                />
              </TableRowColumn>
            </TableRow>

            <TableRow selectable={false} >
              <TableRowColumn>Code</TableRowColumn>
              <TableRowColumn style={styles.overflow}>
                {editMode ? (
                  <span>{product.code}</span>
                ) : (
                  <Field
                    label="Product Code"
                    name="code"
                    component={TextField}
                  />
                )}
              </TableRowColumn>
            </TableRow>

            {isInternalStaff(role) && (
              <TableRow selectable={false}>
                <TableRowColumn>Type</TableRowColumn>
                <TableRowColumn>
                  <Field
                    name="typeOf"
                    component={DropDown}
                    options={types}
                  />
                </TableRowColumn>
              </TableRow>
            )}

            <TableRow selectable={false}>
              <TableRowColumn>Status</TableRowColumn>
              <TableRowColumn>
                <Field
                  name="status"
                  component={DropDown}
                  options={statuses}
                />
              </TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>

        <h2 style={{ color: '#434343' }}>Product Options</h2>

        <FieldArray
          name="options"
          component={this.renderOptions}
          addOptionDialog={addOptionDialog}
          editOptionIndex={editOptionIndex}
        />

        <Divider />

        <div style={styles.formFooter}>
          <RaisedButton
            onClick={cancel}
            label="Cancel"
            style={{ marginRight: '1em' }}
            disabled={productSaving}
          />

          <RaisedButton
            primary
            type="submit"
            label={editMode ? 'Save Changes' : 'Add Product'}
            icon={productSaving ? (
              <CircularProgress color="#ffffff" size={18} thickness={2} />
            ) : (
              <span />
            )}
            disabled={productSaving}
          />
        </div>
      </form>
    );
  }
}

SupplerProductForm.propTypes = {
  editMode: PropTypes.bool.isRequired,
  productSaving: PropTypes.bool.isRequired,
  product: PropTypes.shape({}),
  role: PropTypes.string.isRequired,
  supplierId: PropTypes.string,
  adminSupplierId: PropTypes.string,
  supplierCatalogId: PropTypes.string,
  travelerTypes: PropTypes.arrayOf(PropTypes.object).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  createSupplierCatalogItem: PropTypes.func.isRequired,
  updateSupplierCatalogItem: PropTypes.func.isRequired,
};

SupplerProductForm.defaultProps = {
  product: null,
  supplierId: null,
  adminSupplierId: null,
  supplierCatalogId: null,
};

const mapStateToProps = (state) => {
  const {
    app,
    suppliers,
    reportFilters,
    orgSettings,
  } = state;

  return {
    role: app.role,
    supplierId: app.user.supplierId,
    productSaving: suppliers.updateCatalogItem.fetching || suppliers.createCatalogItem.fetching,
    product: suppliers.getCatalogItem.data ? suppliers.getCatalogItem.data.item : null,
    supplierCatalogId: reportFilters.supplierCatalogId !== -1 ?
      reportFilters.supplierCatalogId :
      null,
    adminSupplierId: reportFilters.adminSupplierId !== -1 ?
      reportFilters.adminSupplierId :
      null,
    travelerTypes: orgSettings.readOrgSettings.data ?
      orgSettings.readOrgSettings.data.travelerTypes :
      [],
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  createSupplierCatalogItem: supplierActions.createSupplierCatalogItem,
  updateSupplierCatalogItem: supplierActions.updateSupplierCatalogItem,
}, dispatch);

const SupplerProductFormConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(SupplerProductForm);

const validate = (values) => {
  const errors = {};

  if (!values.name) {
    errors.name = 'Name is required';
  }

  if (!values.code) {
    errors.code = 'Code is required';
  }

  return errors;
};

export default reduxForm({
  form: 'supplierProductForm',
  validate,
  initialValues: {
    name: '',
    code: '',
    typeOf: 'TYPE_PRODUCT',
    status: 'STATUS_LIVE',
    options: [],
  },
})(SupplerProductFormConnected);
