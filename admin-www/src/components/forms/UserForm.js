/* globals window */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  Field,
  FieldArray,
  formValueSelector,
} from 'redux-form';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import ContentRemoveCircleOutline from 'material-ui/svg-icons/content/remove-circle-outline';
import ContentAddOutline from 'material-ui/svg-icons/content/add-circle-outline';
import { NotificationManager } from 'react-notifications';
import { TextField, DropDown, Toggle } from '../formElements';
import * as accountsActions from '../../ax-redux/actions/accounts';
import * as groupsActions from '../../ax-redux/actions/groups';

const passwordRegex = /^((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})$/;
const phoneRegex = /^(\+?[01])?[-.\s]?\(?[1-9]\d{2}\)?[-.\s]?\d{3}[-.\s]?\d{4}/;
const emailRegex = /@/;

const styles = {
  cell: {
    height: 80,
  },
  alignMiddle: {
    verticalAlign: 'middle',
  },
};

const defaultRoles = {
  ACTOUREX_CUSTOMER_SUPPORT_STAFF: 'Redeam Customer Support Staff',
  ACTOUREX_BUSINESS_STAFF: 'Redeam Business Staff',
  ACTOUREX_TECHNICAL_STAFF: 'Redeam Technical Staff',
  SUPPLIER_REDEMPTION_STAFF: 'Supplier Redemption Staff',
  SUPPLIER_REDEMPTION_SUPERVISOR: 'Supplier Redemption Supervisor',
  SUPPLIER_BUSINESS_STAFF: 'Supplier Business Staff',
  RESELLER_BUSINESS_STAFF: 'Reseller Business Staff',
  PASS_ISSUER_BUSINESS_STAFF: 'Pass Issuer Business Staff',
};

const roleOptions = (groups, selectedGroups, groupId) => groups
  .filter(g => !!defaultRoles[g.name])
  .filter(g => g.id === groupId || !selectedGroups.includes(g.id))
  .map(g => ({ label: defaultRoles[g.name], value: g.id }));

class UserForm extends React.Component {
  constructor(props) {
    super(props);

    this.save = this.save.bind(this);
    this.deleteAccount = this.deleteAccount.bind(this);
    this.addAccountToGroup = this.addAccountToGroup.bind(this);
    this.removeAccountFromGroup = this.removeAccountFromGroup.bind(this);
    this.renderRoles = this.renderRoles.bind(this);
  }

  save(values) {
    const {
      editMode,
      user,
      updateAccount,
      createAccountWithGroups,
      updateAccountPassword,
    } = this.props;

    const {
      passworded,
      password,
      roles,
      ...accountData
    } = values;

    const update = () => updateAccount(user.id, {
      id: user.id,
      ...accountData,
    });

    if (editMode) {
      if (password) {
        updateAccountPassword(user.id, { new_pass: password }).then((err) => {
          if (err) {
            try {
              NotificationManager.error(err.error.data.Error);
            } catch (e) {
              NotificationManager.error('Error occurred. Password has not been changed.');
            }
          }

          update();
        });
      } else {
        update();
      }
    } else {
      createAccountWithGroups({
        ...accountData,
        roles,
        password: passworded ? password : accountData.username,
      });
    }
  }

  deleteAccount() {
    const {
      user,
      deleteAccount,
    } = this.props;

    if (window.confirm('Confirm disabling user?')) {
      deleteAccount(user.id);
    }
  }

  addAccountToGroup(groupId) {
    const {
      user,
      editMode,
      addAccountToGroup,
    } = this.props;

    if (editMode) {
      addAccountToGroup(groupId, user.id).then((err) => {
        if (err) {
          NotificationManager.error('Error occurred. Role has not been added.');
        } else {
          NotificationManager.success('Role successfully added');
        }
      });
    }
  }

  removeAccountFromGroup(groupId, callback) {
    const {
      user,
      editMode,
      removeAccountFromGroup,
    } = this.props;

    if (editMode && groupId) {
      removeAccountFromGroup(groupId, user.id).then((err) => {
        if (err) {
          NotificationManager.error('Error occurred. Role has not been removed.');
        } else {
          NotificationManager.success('Role successfully removed');
        }

        callback();
      });
    } else {
      callback();
    }
  }

  renderRoles({ fields }) {
    const {
      editMode,
      groups,
      addGroupFetching,
      removeGroupFetching,
      formValues: {
        roles: selectedRoles,
      },
    } = this.props;

    const fetching = addGroupFetching || removeGroupFetching;
    const filteredGroups = groups.filter(g => !!defaultRoles[g.name]);

    const remove = index => () => fields.remove(index);

    return (
      <div>
        {fields.map((role, index) => (
          <div key={role}>
            <Field
              label="Select Role"
              name={role}
              component={DropDown}
              options={roleOptions(groups, selectedRoles, selectedRoles[index])}
              autoComplete="new-password"
              disabled={editMode && !!selectedRoles[index]}
              onChange={(e, id) => this.addAccountToGroup(id)}
              style={{ width: 300, ...styles.alignMiddle }}
            />

            <IconButton
              onTouchTap={() => this.removeAccountFromGroup(selectedRoles[index], remove(index))}
              style={styles.alignMiddle}
              disabled={fetching}
            >
              <ContentRemoveCircleOutline />
            </IconButton>
          </div>
        ))}

        {fetching ? (
          <div style={{ padding: 12 }}>
            <CircularProgress color={'#27367a'} size={24} thickness={2} />
          </div>
        ) : selectedRoles.length < filteredGroups.length && (
          <IconButton onTouchTap={() => fields.push('')}>
            <ContentAddOutline />
          </IconButton>
        )}
      </div>
    );
  }

  renderFormTable() {
    const {
      user,
      editMode,
      suppliers,
      addGroupFetching,
      removeGroupFetching,
      formValues: {
        passworded,
        roles: selectedRoles,
      },
    } = this.props;

    if (editMode && !user) {
      return (
        <div>User no found</div>
      );
    }

    const supplierOptions = suppliers.map(s => ({
      label: s.supplier.name,
      value: s.supplier.code,
    }));

    return (
      <Table selectable={false} className="edit-table">
        <TableBody displayRowCheckbox={false}>
          <TableRow selectable={false} style={{ display: 'none' }}>
            <TableRowColumn style={styles.cell}>ID</TableRowColumn>
            <TableRowColumn>
              <Field
                label="ID"
                name="id"
                component={TextField}
                autoComplete="new-password"
              />
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false}>
            <TableRowColumn style={styles.cell}>Username</TableRowColumn>
            <TableRowColumn>
              <Field
                label="Username"
                name="username"
                component={TextField}
                autoComplete="new-password"
              />
            </TableRowColumn>
          </TableRow>

          {!editMode && (
            <TableRow selectable={false}>
              <TableRowColumn>Passworded</TableRowColumn>
              <TableRowColumn>
                <Field
                  name="passworded"
                  component={Toggle}
                  onChange={this.togglePassword}
                />
              </TableRowColumn>
            </TableRow>
          )}

          {(passworded || editMode) && (
            <TableRow selectable={false}>
              <TableRowColumn style={styles.cell}>Password</TableRowColumn>
              <TableRowColumn>
                <Field
                  label="Password"
                  name="password"
                  type="password"
                  component={TextField}
                  autoComplete="new-password"
                />
              </TableRowColumn>
            </TableRow>
          )}

          <TableRow selectable={false}>
            <TableRowColumn style={styles.cell}>First Name</TableRowColumn>
            <TableRowColumn>
              <Field
                label="First Name"
                name="givenName"
                component={TextField}
                autoComplete="new-password"
              />
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false}>
            <TableRowColumn style={styles.cell}>Last Name</TableRowColumn>
            <TableRowColumn>
              <Field
                label="Last Name"
                name="surname"
                component={TextField}
                autoComplete="new-password"
              />
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false}>
            <TableRowColumn style={styles.cell}>Phone</TableRowColumn>
            <TableRowColumn>
              <Field
                label="Phone"
                name="phone"
                component={TextField}
                autoComplete="new-password"
              />
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false}>
            <TableRowColumn style={styles.cell}>Email</TableRowColumn>
            <TableRowColumn>
              <Field
                label="Email"
                name="email"
                component={TextField}
                autoComplete="new-password"
              />
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false}>
            <TableRowColumn style={styles.cell}>Roles</TableRowColumn>
            <TableRowColumn>
              <FieldArray
                key={selectedRoles.join()}
                name="roles"
                component={this.renderRoles}
                fetching={addGroupFetching || removeGroupFetching}
              />
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false}>
            <TableRowColumn style={styles.cell}>Organization</TableRowColumn>
            <TableRowColumn style={styles.cell}>
              {editMode ? (
                <span style={styles.alignMiddle}>{user.orgCode || '-'}</span>
              ) : (
                <div style={{ lineHeight: '80px' }}>
                  <Field
                    floatingLabelText="Organization"
                    floatingLabelFixed
                    name="orgCode"
                    component={TextField}
                    style={{ display: 'inline-block', verticalAlign: 'middle', width: '320px' }}
                    autoComplete="new-password"
                  />

                  <span style={{ margin: '0 1em', verticalAlign: 'middle' }}>OR</span>

                  <Field
                    floatingLabelText="Select Supplier"
                    floatingLabelFixed
                    name="orgCode"
                    component={DropDown}
                    options={supplierOptions}
                    autoComplete="new-password"
                    style={{ display: 'inline-block', verticalAlign: 'middle', width: '320px' }}
                  />
                </div>
              )}
            </TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
    );
  }

  render() {
    const {
      editMode,
      handleSubmit,
      updatingPassword,
      creating,
      updating,
      deleting,
    } = this.props;

    const loading = updatingPassword || creating || updating || deleting;

    return (
      <form autoComplete="off" onSubmit={handleSubmit(this.save)}>
        {this.renderFormTable()}

        {loading ? (
          <div style={{ padding: 12 }}>
            <CircularProgress color={'#27367a'} size={24} thickness={2} />
          </div>
        ) : (
          <div style={{ padding: '24px 0' }}>
            <RaisedButton
              type="submit"
              className="save-button"
              label="Save"
            />

            {editMode && (
              <RaisedButton
                type="button"
                className="delete-button"
                label="Disable User"
                onClick={this.deleteAccount}
              />
            )}
          </div>
        )}
      </form>
    );
  }
}

UserForm.propTypes = {
  user: PropTypes.shape({}),
  formValues: PropTypes.shape({}).isRequired,
  suppliers: PropTypes.arrayOf(PropTypes.object).isRequired,
  groups: PropTypes.arrayOf(PropTypes.object).isRequired,
  editMode: PropTypes.bool,
  addGroupFetching: PropTypes.bool.isRequired,
  removeGroupFetching: PropTypes.bool.isRequired,
  updatingPassword: PropTypes.bool.isRequired,
  creating: PropTypes.bool.isRequired,
  updating: PropTypes.bool.isRequired,
  deleting: PropTypes.bool.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  createAccountWithGroups: PropTypes.func.isRequired,
  deleteAccount: PropTypes.func.isRequired,
  updateAccount: PropTypes.func.isRequired,
  updateAccountPassword: PropTypes.func.isRequired,
  addAccountToGroup: PropTypes.func.isRequired,
  removeAccountFromGroup: PropTypes.func.isRequired,
};

UserForm.defaultProps = {
  user: null,
  editMode: false,
};

const mapStateToProps = (state) => {
  const {
    accounts,
    groups,
    suppliers,
  } = state;

  const selector = formValueSelector('user');
  const formValues = selector(state, 'passworded', 'roles');

  return {
    user: accounts.read.current || null,
    groups: groups.list.data,
    accountGroups: accounts.listGroups.data,
    suppliers: suppliers.list.data ? state.suppliers.list.data.suppliers : [],
    addGroupFetching: groups.addAccount.fetching,
    removeGroupFetching: groups.removeAccount.fetching,
    updatingPassword: accounts.update.updatingPassword,
    updating: accounts.update.fetching,
    creating: accounts.create.fetching,
    deleting: accounts.remove.fetching,
    formValues,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  ...accountsActions,
  ...groupsActions,
}, dispatch);

const UserFormConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserForm);

const validate = (values, props) => {
  const errors = {};

  if (!values.username) {
    errors.username = 'User name is required';
  }

  if (!values.givenName) {
    errors.givenName = 'Given name is required';
  }

  if (!values.surname) {
    errors.surname = 'Last name is required';
  }

  if (!props.editMode && values.passworded && !values.password) {
    errors.password = 'Password is required';
  } else if (!passwordRegex.test(values.password)) {
    if (!props.editMode || (props.editMode && values.password)) {
      errors.password = 'Password must contain at least one capital, one number and should have 6 to 20 characters';
    }
  }

  if (values.email && !emailRegex.test(values.email)) {
    errors.email = 'Email is not valid';
  }

  if (values.phone && !phoneRegex.test(values.phone)) {
    errors.phone = 'Phone is not valid';
  }

  return errors;
};

export default reduxForm({
  form: 'user',
  validate,
  initialValues: {
    username: '',
    passworded: true,
    password: '',
    givenName: '',
    surname: '',
    phone: '',
    email: '',
    roles: [],
    orgCode: '',
  },
})(UserFormConnected);
