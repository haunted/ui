import React, { Component, PropTypes } from 'react'
import IconButton from 'material-ui/IconButton'
import SvgIcon from 'material-ui/SvgIcon'
import Popover from 'material-ui/Popover'
import Toggle from 'material-ui/Toggle';
import ActionSettings from 'material-ui/svg-icons/action/settings';

const styles = {
  button: {
    position: 'absolute',
    right: '0',
    top: '72px',
    zIndex: 4
  },
  menu: {
    maxWidth: 250,
    padding: '1em'
  }
}

class TableSettings extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activated: false,
      anchorEl: null,
      disabled: [] /* map[string]bool */
    }
  }
  componentDidMount () {
    this.init()
  }
  init () {
    let disabled = []
    this.props.columns.map( c=> {
      disabled[c] = false
    })
    this.setState({
      disabled: disabled
    })
  }
  onUpdateSettings () {
    this.props.callback && this.props.callback(this.state.disabled)
  }
  toggleColumn (col) {
    let disabled = this.state.disabled
    disabled[col] = !disabled[col]
    this.setState({
      disabled
    })
    this.onUpdateSettings()
  }
  isEnabled (col) {
    let disabled = this.state.disabled[col]
    return disabled == null || disabled == false
  }
  handleTouchTap (event) {
    event.preventDefault()
    this.setState({
        activated: true,
        anchorEl: event.currentTarget
    });
  }
  handleRequestClose (evt) {
    this.setState({
       activated: false
    });
  }
  render() {
    return (
      <div style={this.props.style || styles.button}>
        {this.state.activated ? (
          <Popover
            open={this.state.activated}
            anchorEl={this.state.anchorEl}
            anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
            onRequestClose={e => this.handleRequestClose()}
          >
            <div style={styles.menu}>
              { this.props.columns.map((c, i) => {
                  return (
                    <Toggle
                      key={i}
                      label={c}
                      toggled={this.isEnabled(c)}
                      onToggle={e => this.toggleColumn(c)}
                      style={styles.toggle}
                    />
                  )
              })}
            </div>
          </Popover>
        ) : ""}
      <IconButton onTouchTap={e => this.handleTouchTap(e)} >
        <ActionSettings style={styles.icon} />
      </IconButton>
    </div>
    )
  }
}

TableSettings.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.string),
  callback: PropTypes.func
}

export default TableSettings
