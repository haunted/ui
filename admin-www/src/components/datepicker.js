import React, { Component, PropTypes } from 'react';
import DatePicker from 'material-ui/DatePicker';
import moment from 'moment-timezone';

// shiftPickerDateToTZDate converts a date from the datepicker (start of day in whatever the browser timezone is)
// and shifts it so that it is the start of day in the organization TZ
function shiftPickerDateToTZDate(pickerDate) {
  const pickerOffset = pickerDate.getTimezoneOffset();
  const utcDate = new Date();
  utcDate.setTime(pickerDate.getTime() - pickerOffset * 60000);

  const tzOffset = moment.tz(pickerDate, TZ).utcOffset();
  const tzDate = new Date();
  tzDate.setTime(utcDate.getTime() - tzOffset * 60000);
  return tzDate;
}

function shiftTzDateToPickerDate(tzDate) {
  const tzUTCOffset = moment.tz(tzDate, TZ).utcOffset();
  const utcDate = new Date();
  utcDate.setTime(tzDate.getTime() + tzUTCOffset * 60000);

  const pickerDate = new Date();
  const pickerOffset = pickerDate.getTimezoneOffset();
  pickerDate.setTime(utcDate.getTime() + pickerOffset * 60000);

  return pickerDate;
}

function formatDateOnly(date) {
  return true;
  // return moment(date).startOf('day').toDate().getTime() == date.getTime()
}

function formatDate(date) {
  if (formatDateOnly(date)) {
    return moment(date).format('DD-MMM-YYYY');
  }
  return moment(date).format('DD-MMM-YYYY h:mm a');
}

// TZDatePicker is a timezone aware component that selects a date.
// It will return the date that equals the start of the day in the location provided by the windows.TZ variable
export class TZDatePicker extends Component {
  onChange(date) {
    return this.props.onChange(date);
  }

  render() {
    const { value, onChange, ...other } = this.props;
    const pickerValue = shiftTzDateToPickerDate(value);
    return (
      <DatePicker
        floatingLabelText="Date"
        value={pickerValue}
        onChange={(ev, date) => onChange(shiftPickerDateToTZDate(date))}
        formatDate={date => formatDate(date)}
        hintText="Date"
        className={`date-picker ${!formatDateOnly(pickerValue) ? 'date-picker-wide' : ''}`}
        {...other}
      />
    );
  }
}
TZDatePicker.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.object,
};

// DateRangePicker is a timezone aware component that implements date range logic.
// it is organization timezone aware and outputs starts and ends of days as per the window.TZ variable
// the minimum selected range is one day and the end date is inclusive of the date that is specified
class DateRangePicker extends Component {
  shouldDisableStartDate(date) {
    return shiftPickerDateToTZDate(date).getTime() > subDay(this.props.end).getTime();
  }

  shouldDisableEndDate(date) {
    return shiftPickerDateToTZDate(date).getTime() < this.props.start.getTime();
  }

  setStartDate(date) {
    return this.props.onStartDate(date);
  }
  setEndDate(date) {
    return this.props.onEndDate(date);
  }

  render() {
    return (
      <div className="date-range-picker">
        <TZDatePicker
          floatingLabelText="Start Date"
          hintText="Start Date"
          value={this.props.start}
          shouldDisableDate={day => this.shouldDisableStartDate(day)}
          onChange={date => this.setStartDate(date)}
        />
        <TZDatePicker
          floatingLabelText="End Date"
          hintText="End Date"
          value={subDay(this.props.end)}
          shouldDisableDate={day => this.shouldDisableEndDate(day)}
          onChange={date => this.setEndDate(addDay(date))}
        />
      </div>
    );
  }
}
DateRangePicker.propTypes = {
  onStartDate: PropTypes.func,
  onEndDate: PropTypes.func,
  start: PropTypes.object,
  end: PropTypes.object,
};
export default DateRangePicker;

// DomainTime is a timezone aware component that takes a domain timestamp object and displays
// it like: "Jan 1, 2006" blankReplacer can be used to replace empty timestamps like ""
// format is a moment.js date format
export const DomainTime = ({ ts = '', ...other }) => (ts && ts.seconds ? <TimestampTitle ts={ts.seconds} {...other} /> : null);

// TimestampTitle is a timezone aware component that takes a unix timestamp as a string, and displays it like: "Jan 1, 2006"
//   blankReplacer can be used to replace empty timestamps like ""
//   format is a moment.js date format
export const TimestampTitle = ({ ts = '', format = 'DD-MMM-YYYY', blankReplacer = '—' }) => (
  <span>{
    !ts ? blankReplacer : moment.unix(parseInt(ts)).tz(TZ).format(format)
  }</span>
);
TimestampTitle.propTypes = {
  format: PropTypes.string,
};

// DateStringTitle is a timezone aware component that takes a moment-worthy date string
// such as like "2006-01-02T03:54:23Z", and displays it like: "Jan 2, 2006".
//   blankReplacer can be used to replace empty timestamps like ""
//   format is a moment.js date format
export const DateStringTitle = ({ value = '', format = 'DD-MMM-YYYY', blankReplacer = '—' }) => (
  <span>{
    !value ? blankReplacer : moment(value).tz(TZ).format(format)
  }</span>
);
DateStringTitle.propTypes = {
  format: PropTypes.string,
};


// DateRangeTitle is a timezone aware component that displays a date range like: "( Jan 1, 2006 - Jan 5, 2006 )"
//   format is a moment.js date format
//   if the start and end only encompass one day, it prints only the date: "Jan 1, 2006"
export const DateRangeTitle = ({ start, end, format = 'DD-MMM-YYYY' }) => {
  const startDate = moment(start).tz(TZ);
  const endDate = moment(end).tz(TZ).subtract(1, 'day');

  let dateString = startDate.format(format);

  if (startDate < endDate) {
    dateString = `( ${dateString} - ${endDate.format(format)} )`;
  }

  return (
    <span className="title-date">
      {dateString}
    </span>
  );
};
DateRangeTitle.propTypes = {
  start: PropTypes.object,
  end: PropTypes.object,
  format: PropTypes.string,
};


// addDay conviniently adds a day to a date
function addDay(date) {
  return moment(date).tz(TZ).add(1, 'day').toDate();
}

// subDay conviniently subtracts a day from a date
function subDay(date) {
  return moment(date).tz(TZ).subtract(1, 'day').toDate();
}
