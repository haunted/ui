import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

export default class PriceListsPopover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      hasLookedUp: false,
      label: ''
    }
  }
  componentDidMount () {
    this.setState({
      label: this.props.value == '' ? "Select Price List" : this.props.value
    })
  }
  componentWillUpdate (nextProps, nextState) {
    if (nextState.hasLookedUp == false && !!nextProps.lists) {
      this.props.lists.map((option, i) => {
        if (option.id == this.props.value) {
          this.setState({
            label: option.name,
            hasLookedUp: true
          })
        }
      })
    }
  }
  handleTouchTap (event) {
    event.preventDefault();
    this.setState({
        open: true,
        anchorEl: event.currentTarget
    })
  }
  handleRequestClose (evt) {
    this.setState({
       open: false
    })
  }
  onMenuItem(option) {
    this.props.onChange(option.id)
    this.setState({
      open: false,
      label: option.name
    })
  }
  render() {
    return (
      <div className="roles-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={e => this.handleRequestClose(e)}
        >
          <Menu>
            {
                !!this.props.lists && this.props.lists.map((option, i) => {
                    return (
                        <MenuItem onClick={()=>{this.onMenuItem(option)}}
                                  primaryText={option.name}
                                  key={i}
                         />
                    )
                })
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}

PriceListsPopover.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  lists: PropTypes.array.isRequired
}
