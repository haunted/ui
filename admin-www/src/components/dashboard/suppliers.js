import React, { Component } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress'
import Tile from '../reports/tile.js'
import { connect } from 'react-redux'
import { dashboardFetch }  from '../../ax-redux/actions/dashboard'
import { browserHistory } from 'react-router'
import {formatCurrencyUnit, formatCurrencyNumeral} from '../../util'


class Suppliers extends Component {
  constructor (props) {
    super(props)
  }

  componentDidMount() {
      this.props.loadDashboard({
          role: this.props.role,
          supplierId: this.props.supplierId
      });
  }

  weeklyChangeText (change) {
      if (change != 0) {
          return (change > 0 ? "Up" : "Down") + " by "+change+" this week.";
      } else {
          return "No change this week."
      }
  }

  render() {
      let dashboard = this.props.dashboard ? this.props.dashboard.dashboard : {},
          fetching = this.props.fetching;
    return (
        <div className="inner-dashboard">
            <section className="tiles">
                <Tile key="Ticket Volume" title="Ticket Volume" fetching={fetching}
                      subtitle="In preceding 24 hours"
                      link="/app/reports/top-reseller-ticket-volume"
                      figure={ dashboard && dashboard.ticketVolume ? dashboard.ticketVolume.quantity : 0 } />

                <Tile key="Dollar Volume" title="Dollar Volume" fetching={fetching}
                      subtitle=""
                      link="/app/reports/top-reseller-dollar-volume"
                      figure={dashboard && dashboard.ticketVolume ? formatCurrencyNumeral("USD", parseInt(dashboard.ticketVolume.dollarTotal)) : 0}
                      unit={dashboard && dashboard.ticketVolume ? formatCurrencyUnit("USD", parseInt(dashboard.ticketVolume.dollarTotal)) : ""} />

                <Tile key="Redemption Exceptions" title="Redemption Exceptions"
                      subtitle="In preceding 24 hours" fetching={fetching}
                      figure={ dashboard && dashboard.redemptionExceptions} />

                <Tile key="Products" title="Products" fetching={fetching}
                      subtitle={ dashboard && this.weeklyChangeText(dashboard.products ? dashboard.products.weeklyChange : 0) }
                      figure={ dashboard && dashboard.products ? dashboard.products.number : 0 } />

                <Tile key="Connected Resellers" title="Connected Resellers" fetching={fetching}
                      link="/app/reports/supplier-reseller-invoice"
                      subtitle={ dashboard && this.weeklyChangeText(dashboard.connectedResellers ? dashboard.connectedResellers.weeklyChange : 0) }
                      figure={ dashboard && dashboard.connectedResellers ? dashboard.connectedResellers.number : 0 } />
            </section>
        </div>
    )
  }
}
/*
Overall Ticket volume in preceding 24 hours
Total $ amount
Number of tickets
Number of redemption exceptions in preceding 24 hours
Number of products in system, and gain/loss in preceding 7 days.
Number of connected resellers in system, and gain/loss in preceding 7 days.
*/

const mapStateToProps = (state) => {
  return {
    dashboard: state.dashboard.data,
    fetching: state.dashboard.fetching,
    supplierId: state.app.user.account.orgCode,
    role: state.app.role
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loadDashboard: (data) => {
        dispatch(dashboardFetch(data))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Suppliers);
