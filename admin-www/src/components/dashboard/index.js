import React, { Component, PropTypes } from 'react'
import TopBar from '../top-bar.js'
import DashMenu from '../dash-menu.js'
import SideMenu from '../side-menu.js'
import AdminMenu from '../admin-menu.js'
import Tile from '../reports/tile.js'

import Business from './business'
import Technology from './technology'
import StaffPerformance from './staff-performance'
import CustomerSupport from './customer-support'
import Suppliers from './suppliers'
import Resellers from './resellers'
import Limited from './limited'

import {
  fetch,
  exportCSV,
  exportXLSX,
} from '../../ax-redux/actions/ticket-exceptions' // for compatibility with management-ui

class Dashboard extends Component {

  constructor (props) {
    super(props);
    this.state = {
      start: 0,
      end: 0
    };
  }

  componentWillMount() {
    switch (this.props.userRole) {
      case "SUPPLIER_BUSINESS_STAFF":
        this.props.setSideMenuItems([]);
      break;
      case "RESELLER_BUSINESS_STAFF":
        this.props.setSideMenuItems([]);
      break;
      case "ACTOUREX_CUSTOMER_SUPPORT_STAFF":
        this.props.setSideMenuItems([]);
      break;
      case "ACTOUREX_BUSINESS_STAFF":
        this.props.setSideMenuItems([]);
      break;
      case "SUPPLIER_REDEMPTION_STAFF":
        this.props.setSideMenuItems([]);
      break;
      case "SUPPLIER_REDEMPTION_SUPERVISOR":
           this.props.setSideMenuItems([]);
       break;
      default:
        this.props.setSideMenuItems([
          // { title: 'Redeemed', url:'/app/reports/redeemed'},
          // { title: 'Redeemed', url:'/app/reports/redeemed'},
          // { title: 'Redeemed', url:'/app/reports/redeemed'},
        ])
    }
  }
  componentDidMount() {
    this.props.setPageTitle("Dashboard")
    this.props.fetch({
      start: this.state.start,
      end: this.state.end,
    })
  }

  render() {
    let innerDashboard = "";
    switch (this.props.userRole) {
        case "ACTOUREX_TECHNICAL_STAFF":
            innerDashboard = <Technology />;
        break;
        case "ACTOUREX_BUSINESS_STAFF":
            innerDashboard = <Business />
        break;
        case "ACTOUREX_CUSTOMER_SUPPORT_STAFF":
             innerDashboard = <CustomerSupport />;
        break;
        case "SUPPLIER_BUSINESS_STAFF":
            innerDashboard = <Suppliers />;
        break;
        case "RESELLER_BUSINESS_STAFF":
            innerDashboard = <Resellers />;
        break;
        case "SUPPLIER_REDEMPTION_STAFF":
            innerDashboard = <Limited />
        break;
        case "SUPPLIER_REDEMPTION_SUPERVISOR":
             innerDashboard = <StaffPerformance />
        break;
        default:
          innerDashboard = "";
    }
    return (
        <div className="dashboard home">
          {/*<DashMenu title={(!!this.props.userRole ? this.props.userRole : "Internal")+" Dashboard"} />*/}
          <AdminMenu />
          {innerDashboard}
      </div>
    )
  }
}

import { connect } from 'react-redux'
import { setPageTitle, setSideMenuItems } from '../../ax-redux/actions/app'
import { dashboardFetch } from '../../ax-redux/actions/dashboard'

export default connect(
  state => {
    return {
      app: state.auth,
      userRole: state.app.role,
      dashboard: state.dashboard,
    }
  },
  dispatch => {
    return {
      setSideMenuItems: items => {
        dispatch(setSideMenuItems(items))
      },
       getDashboard: () => {
         dispatch(dashboardFetch())
      },
      setPageTitle: title => {
        dispatch(setPageTitle(title))
      },
      fetch: (opts) => {
        dispatch(fetch(opts))
      },
    }
  }
)(Dashboard)
