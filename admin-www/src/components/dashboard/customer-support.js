import React, { Component } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress'
import Tile from '../reports/tile.js'
import { connect } from 'react-redux'
import { dashboardFetch } from '../../ax-redux/actions/dashboard'

class CustomerSupport extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
      this.props.loadDashboard({role: this.props.role});
  }

  weeklyChangeText (change) {
      if (change != 0) {
          return (change > 0 ? "Up" : "Down") + " by "+change+" this week.";
      } else {
          return "No change this week."
      }
  }

  render() {
      let fetching = this.props.fetching,
          dashboard = this.props.dashboard;
    return (
        <div className="inner-dashboard">
            <section className="tiles">
              <Tile key="Open Support Tickets" title="Open Support Tickets"
                    subtitle=""
                    figure={dashboard.openSupportTickets} unit="" />
              <Tile key="Average Response Time" title="Average Response Time"
                    subtitle=""
                    figure={dashboard.averageTicketResponseTime} unit="" />
            </section>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    dashboard: state.dashboard.data,
    fetching: state.dashboard.fetching,
    role: state.app.role
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loadDashboard: (data) => {
        dispatch(dashboardFetch(data))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerSupport);
