import React, { Component } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress'
import Tile from '../reports/tile.js'
import { connect } from 'react-redux'
import { dashboardFetch } from '../../ax-redux/actions/dashboard'

class Resellers extends Component {
  constructor (props) {
    super(props)
  }

  componentDidMount() {
      this.props.loadDashboard({role: this.props.role});
  }

  weeklyChangeText (change) {
      if (change != 0) {
          return (change > 0 ? "Up" : "Down") + " by "+change+" this week.";
      } else {
          return "No change this week."
      }
  }

  render() {
      let dash = this.props.dashboard ? this.props.dashboard.dashboard : {},
          fetching = this.props.fetching;
    return (
        <div className="inner-dashboard">
                <section className="tiles">
                    <Tile key="Ticket Volume" title="Ticket Volume"
                          subtitle="In preceding 24 hours"
                          fetching={fetching}
                          figure={ dash && dash.ticketVolume} />

                    <Tile key="Tickets" title="Dollar Volume"
                          subtitle=""
                          fetching={fetching}
                          unit="$"
                          figure={ dash && dash.dollarAmount} />

                    <Tile key="Redemption Exceptions" title="Redemption Exceptions"
                          subtitle="In preceding 24 hours"
                          fetching={fetching}
                          figure={ dash && dash.redemptionExceptions} />

                    <Tile key="Products" title="Products"
                          fetching={fetching}
                          subtitle={ this.weeklyChangeText(dash && dash.products ? dash.products.weeklyChange : 0) }
                          figure={ dash && dash.products ? dash.products.number : 0} />

                    <Tile key="Connected Suppliers" title="Connected Suppliers"
                          fetching={fetching}
                          subtitle={ this.weeklyChangeText(dash && dash.connectedSuppliers ? dash.connectedSuppliers.weeklyChange : 0) }
                          figure={ dash && dash.connectedSuppliers ? dash.connectedSuppliers.number : 0 } />
                </section>
        </div>
    )
  }
}
/*
Overall Ticket volume in preceding 24 hours
Total $ amount
Number of tickets
Number of redemption exceptions in preceding 24 hours
Number of products in system, and gain/loss in preceding 7 days.
Number of connected suppliers in system, and gain/loss in preceding 7 days.
*/

const mapStateToProps = (state) => {
  return {
    dashboard: state.dashboard.data,
    fetching: state.dashboard.fetching,
    role: state.app.role
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loadDashboard: (data) => {
        dispatch(dashboardFetch(data))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Resellers);
