import React, { Component } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress'
import Tile from '../reports/tile.js'
import { connect } from 'react-redux'
import { dashboardFetch }  from '../../ax-redux/actions/dashboard'

class Technology extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
      this.props.loadDashboard({role: this.props.role});
  }

  weeklyChangeText (change) {
      if (change != 0) {
          return (change > 0 ? "Up" : "Down") + " by "+change+" this week.";
      } else {
          return "No change this week."
      }
  }

  render() {
      let dash = this.props.dashboard.dashboard, // ? this.props.dashboard.dashboard : false,
          fetching = this.props.fetching;
    return (
        <div className="inner-dashboard">
                <section className="tiles">
                  <Tile key="email-parsing-count" title="Email Parsing Count"
                        fetching={fetching}
                        subtitle="In preceding 24 hours"
                        figure={dash && dash.emailParsingCount} unit="" />

                  <Tile key="ocr-parsing-count" title="OCR Parsing Count"
                        fetching={fetching}
                        subtitle="In preceding 24 hours"
                        figure={dash && dash.ocrParsingCount}
                        unit="" />

                  <Tile key="ocr-runs" title="OCR Runs"
                        fetching={fetching}
                        subtitle="In preceding 24 hours"
                        figure={dash && dash.ocrRuns} unit="" />

                  <Tile key="cloud-factory-count" title="Cloud Factory Count"
                        fetching={fetching}
                        subtitle="In preceding 24 hours"
                        figure={dash && dash.cloudFactoryCount} unit="" />

                  <Tile key="cloud-factory-performance" title="Cloud Factory Performance"
                        fetching={fetching}
                        subtitle="In preceding 24 hours"
                        figure={dash && dash.cloudFactoryCount} unit="" />
                </section>
        </div>
    )
  }
}


const mapStateToProps = (state) => {
  return {
    dashboard: state.dashboard.data,
    fetching: state.dashboard.fetching,
    role: state.app.role
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loadDashboard: (data) => {
        dispatch(dashboardFetch(data))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Technology);
