import React, { Component } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress'
import Tile from '../reports/tile.js'
import { connect } from 'react-redux'
import { dashboardFetch }  from '../../ax-redux/actions/dashboard'


class Limited extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
      this.props.loadDashboard({role: this.props.role});
  }

  render() {
      let dashboard = this.props.dashboard,
          fetching = this.props.fetching,
          supplierName = this.props.supplierName,
          date = this.props.date,
          shiftReportLink = "/app/reports/shifts/"+supplierName+"/"+date;

    return (
        <div className="inner-dashboard">
            <section className="tiles">
              <Tile key="Ticket Volume" title="Ticket Volume"
                    subtitle="Current & Preceding Days"
                    figure={dashboard.ticketVolume} unit="" />
              <Tile key="Redemption Exceptions" title="Redemption Exceptions"
                    subtitle="Current & Preceding Days"
                    figure={dashboard.redemptionExceptions} unit="" />
              <Tile key="Shift Report" title="Shift Report"
                    subtitle="Click for details"
                    figure="View Report"
                    link={shiftReportLink} />
            </section>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    dashboard: state.dashboard.data,
    fetching: state.dashboard.fetching,
    supplierName: "SEED", //state.suppliers.
    date: "–", //
    role: state.app.role
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loadDashboard: (data) => {
        dispatch(dashboardFetch(data))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Limited);
