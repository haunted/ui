import React, { Component } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress'
import Tile from '../reports/tile.js'
import { connect } from 'react-redux'
import { dashboardFetch }  from '../../ax-redux/actions/dashboard'
import {formatCurrencyUnit, formatCurrencyNumeral} from '../../util'

class BusinessDashboard extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
      this.props.loadDashboard({role: this.props.role});
  }

  weeklyChangeText (change) {
      if (change != 0) {
          return (change > 0 ? "Up" : "Down") + " by "+change+" this week.";
      } else {
          return "No change this week."
      }
  }

  render() {
      let fetching = this.props.fetching,
          dash = this.props.dashboard.dashboard;
    return (
        <div className="inner-dashboard">
            <section className="tiles">
              <Tile key="Dollar Total" title="Dollar Total"
                    subtitle="In Preceding 24 Hours"
                    fetching={fetching}
                    link="/app/reports/top-reseller-dollar-volume"
                    figure={dash && dash.ticketVolume ? formatCurrencyNumeral("USD", parseInt(dash.ticketVolume.dollarTotal)) : 0}
                    unit={dash && dash.ticketVolume ? formatCurrencyUnit("USD", parseInt(dash.ticketVolume.dollarTotal)) : ""} />

              <Tile key="Tickets" title="Ticket Volume"
                    fetching={fetching}
                    link="app/reports/top-reseller-ticket-volume"
                    subtitle="In Preceding 24 Hours"
                    figure={dash && dash.ticketVolume ? dash.ticketVolume.quantity : 0} unit="" />

              <Tile key="Resellers" title="Resellers"
                    fetching={fetching}
                    link="/app/manage-business/resellers"
                    subtitle={ this.weeklyChangeText(dash && dash.resellers ? dash.resellers.change : 0) }
                    figure={dash && dash.resellers ? dash.resellers.number: 0} unit="" />

              <Tile key="Suppliers" title="Suppliers"
                    fetching={fetching}
                    link="/app/manage-business/suppliers"
                    subtitle={ this.weeklyChangeText(dash && dash.suppliers ? dash.suppliers.change : 0) }
                    figure={dash && dash.resellers ? dash.suppliers.number: 0} unit="" />

            </section>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    dashboard: state.dashboard.data,
    fetching: state.dashboard.fetching,
    role: state.app.role
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loadDashboard: (data) => {
        dispatch(dashboardFetch(data))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessDashboard);
