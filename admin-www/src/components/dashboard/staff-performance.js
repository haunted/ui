import React, { Component } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress'
import Tile from '../reports/tile.js'
import { connect } from 'react-redux'
import { dashboardFetch }  from '../../ax-redux/actions/dashboard'

class StaffPerformance extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
      this.props.loadDashboard({role: this.props.role});
  }

  weeklyChangeText (change) {
      if (change != 0) {
          return (change > 0 ? "Up" : "Down") + " by "+change+" this week.";
      } else {
          return "No change this week."
      }
  }

  render() {
      let dashboard = this.props.dashboard,
          fetching = this.props.fetching;
    return (
        <div className="inner-dashboard">
            {(!dashboard || fetching ?
                <div className="edit loading-circle">
                  <CircularProgress color={"#27367a"} size={96} />
                </div> :
                <section className="tiles">
                  <Tile key="Ticket Volume" title="Ticket Volume"
                        subtitle="Current & Preceding Days"
                        figure={dashboard.ticketVolume} unit="" />
                  <Tile key="Redemption Exceptions" title="Redemption Exceptions"
                        subtitle="Current & Preceding Days"
                        figure={dashboard.redemptionExceptions} unit="" />
                  <Tile key="Shift Summary" title="Shift Summary"
                        subtitle="Click for details"
                        figure="View Report"
                        link="/app/reports/shifts" />
                </section>
            )}
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    dashboard: state.dashboard.data,
    fetching: state.dashboard.fetching,
    role: state.app.role
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    loadDashboard: (data) => {
        dispatch(dashboardFetch(data))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StaffPerformance);
