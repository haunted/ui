import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import {
  decodeUnit,
  capitalize
} from '../util'

export default class AgeBandsPopover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      label: ''
    }
  }
  componentDidMount () {
    this.setState({
      label: this.props.value == '' ? "Choose Traveler Type" : this.makeHumanReadable({type:this.props.value})
    })
  }
  handleTouchTap (event) {
    event.preventDefault();
    this.setState({
        open: true,
        anchorEl: event.currentTarget
    })
  }
  handleRequestClose (evt) {
    this.setState({
       open: false
    })
  }
  makeHumanReadable (type) {
    if (!!type && !!type.type) {
      return type.type.indexOf("TYPE") > -1 ? capitalize(decodeUnit(type.type).toLowerCase()) : type.type
    }
    if (!!type && !!type.typeOf) {
      return type.typeOf.indexOf("TYPE") > -1 ? capitalize(decodeUnit(type.typeOf).toLowerCase()) : type.typeOf
    }
    return ""
  }
  onMenuItem(travelerType) {
    this.props.onChange(travelerType)
    this.setState({
      open: false,
      label: this.makeHumanReadable(travelerType)
    })
  }
  render() {
    return (
      <div className="roles-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={e => this.handleRequestClose(e)}
        >
          <Menu>
            {
                this.props.values.map((travelerType, i) => {
                    let label = this.makeHumanReadable(travelerType)
                    return (
                        <MenuItem onClick={()=>{this.onMenuItem(travelerType)}}
                                  primaryText={label}
                                  key={i}
                         />
                    )
                })
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}

AgeBandsPopover.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  values: PropTypes.array.isRequired
}
