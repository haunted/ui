import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import CircularProgress from 'material-ui/CircularProgress'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import Snackbar from 'material-ui/Snackbar'
import { browserHistory } from 'react-router'
import AddPriceListModal from './add-price-list-modal'
import {
  formatCurrency,
  isInternalStaff,
  isSupplierStaff
} from '../../../util'
const styles = {
  gridHeader: {
    width: '100%',
    height: '60px'
  },
  editProduct: {
    width: '110px',
    overflow: 'visible'
  },
  editReseller: {
    width: '130px',
    overflow: 'visible'
  },
  checkboxCol: {
    width: '48px'
  },
  priceCol: {
    width: '96px'
  },
  productName: {
    cursor: 'pointer'
  },
  addContractButton: {
    float: 'right'
  },
  addButton: {
    float: 'right',
    marginTop: '1em',
    marginRight: '1em'
  },
  filterProducts: {
    verticalAlign: 'top',
    marginTop: '0.75em',
    backgroundColor: '#fff',
		paddingLeft: '1em',
		boxShadow: 'inset 1px 1px 4px rgba(0,0,0,0.15)',
		borderRadius: '2px',
		height: '46px'
  },
  title: {
   color: 'rgb(67, 67, 67)',
   display: 'inline-block',
   marginLeft: '1em',
   marginRight: '1em'
  }
}
class PriceLists extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    let limit = 20,
        offset = 0,
        product = '',
        sort = '',
        supplierId = -1

    if (!!this.props.location.query) {
        if (!!this.props.location.query.offset) {
          offset = this.props.location.query.offset;
        }
        if (this.props.location.query.product) {
          product = this.props.location.query.product
        }
    }
    this.setState({
      offset,
      limit,
      sort,
      product,
      snackbarText: "",
      showSnackbar: false
    })
    supplierId = this.props.adminSupplierId != -1 ? this.props.adminSupplierId : this.props.supplierId
    this.props.listPriceLists(supplierId)
    this.setState({
      supplierId
    })
  }

  componentWillReceiveProps ( nextProps ) {

    if ( this.props.pathname != nextProps.pathname ||
        this.props.queryParams != nextProps.queryParams ||
        this.props.adminSupplierId != nextProps.adminSupplierId ||
        this.props.supplierId != nextProps.supplierId ) {

        let nextQuery = null,
            requestParams = {
                  limit: this.state.limit,
                  offset: this.state.offset
            }

        if (nextProps.location.query) {

            nextQuery = nextProps.location.query
            if (nextQuery.offset) {
                requestParams.offset = nextQuery.offset
            }
            if (nextQuery.limit) {
                requestParams.limit = nextQuery.limit
            }
            if (nextQuery.sort) {
                requestParams.sort = nextQuery.sort
            }

        }

        this.props.listPriceLists(nextProps.adminSupplierId != -1 ? nextProps.adminSupplierId : nextProps.supplierId)
      }
      if (this.props.priceListError == false && nextProps.priceListError != false) {

        this.setState({
          showSnackbar: true,
          snackbarText: nextProps.priceListError.data.Error
        })

      }
      if (this.props.priceListSaving != false && nextProps.priceListSaving == false && nextProps.priceListError == false) {

        this.props.listPriceLists(nextProps.adminSupplierId != -1 ? nextProps.adminSupplierId : nextProps.supplierId)
        this.setState({
          showSnackbar: true,
          snackbarText: "Price List Added"
        })

      }

  }

  componentWillUpdate(nextProps, nextState) {


  }

  edit (id) {
    let catalogId = this.props.supplierCatalogId,
        supplierId = this.props.adminSupplierId != -1 ? this.props.adminSupplierId : this.props.supplierId
    browserHistory.push(`/app/manage-business/price-lists/${supplierId}/${catalogId}/${id}`)
  }

  sortBy(column) {
    let offset = this.state.offset,
        limit = this.state.limit,
        sort = column

    if (sort == this.state.sort) {
        if (sort.indexOf("-") > -1) {
          sort = sort.replace("-", "");
        } else {
          sort = "-"+sort;
        }
    }
    this.setState({
        sort: sort
    });
    browserHistory.push(`/app/manage-business/price-lists?offset=${offset}&limit=${limit}&sort=${sort}`)
  }

  addPriceList (data) {
    data.supplierCode = this.props.orgCode
    data.catalogId = this.props.supplierCatalogId
    let supplierId = this.props.adminSupplierId != -1 ? this.props.adminSupplierId : this.props.supplierId
    this.props.createPriceList(supplierId, data)
  }

  renderPriceLists (priceLists) {
      let catalogId = this.props.supplierCatalogId
      return (
          <Table className="products-table">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>
                      <div className="inner">Name</div>
                    </TableHeaderColumn>
                    <TableHeaderColumn className="sortable">
                      <div className="inner" onClick={ e => this.sortBy("commission") }>Commission</div>
                    </TableHeaderColumn>
                    <TableHeaderColumn>
                      <div className="inner"></div>
                    </TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                { !!priceLists && priceLists.length != null && priceLists.filter((list)=> {return catalogId == -1 || catalogId == list.catalogId}).map((priceList, p)=> {
                  let name = priceList.name,
                      commission = !!priceList.rate ? priceList.rate.percent : 0,
                      fixed = !!priceList.rate && !!priceList.rate.fixed && priceList.rate.fixed != 0 ? formatCurrency("USD", priceList.rate.fixed)+' + ' : ''
                    return (
                        <TableRow key={p} selectable={false} >
                            <TableRowColumn>
                              { name }
                            </TableRowColumn>
                            <TableRowColumn>
                              { `${fixed}${commission}%` }
                            </TableRowColumn>
                            <TableRowColumn>
                              <FlatButton
                                    onTouchTap={e => { this.edit(priceList.id) } }
                                    style={(this.props.style  || {})}
                                    disabled={this.props.suppliersFetching}
                                    title={this.props.suppliersFetching ? "Loading..." : "View / Edit"}
                                    label={"View / Edit"}
                               />
                            </TableRowColumn>
                        </TableRow>
                )
              })}
            </TableBody>
         </Table>
      )
  }
  handleRequestClose () {
      this.setState({
        showSnackbar: false,
      })
  }
  render() {
      let fetching = this.props.resellerFetching;
    return (
      <div className="reseller-view" >
        <h2 style={styles.title}>Price Lists</h2>
        <AddPriceListModal onSave={ v=> this.addPriceList(v) }
                           style={styles.addButton}
                           primary={true}
        />
        <Card className="inner-edit">
            <div className="reseller-view-section reseller-products">
                { this.renderPriceLists(this.props.priceLists) }
            </div>
        </Card>
        <Snackbar message={this.state.snackbarText}
                  onRequestClose={(e)=>this.handleRequestClose()}
                  open={this.state.showSnackbar}
                  autoHideDuration={4000}
        />
      </div>
    )
  }
}

import { connect } from 'react-redux'
import {
  listPriceLists,
  createPriceList
} from '../../../ax-redux/actions/price-lists'
export default connect(
  state => {
    return {
      priceLists: state.priceLists.listPriceLists.data != false ? state.priceLists.listPriceLists.data.priceLists : false,
      priceListsFetching: state.priceLists.listPriceLists.fetching,
      priceListSaving: state.priceLists.createPriceList.fetching,
      priceListError: state.priceLists.createPriceList.error,
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      supplierId: state.app.user.supplierId,
      role: state.app.role,
      pathname: state.routing.locationBeforeTransitions.pathname,
      queryParams: state.routing.locationBeforeTransitions.search,
      adminSupplierId: state.reportFilters.adminSupplierId,
      supplierCatalogId: state.reportFilters.supplierCatalogId,
      resellerCatalogId: state.reportFilters.resellerCatalogId
    }
  },
  dispatch => {
    return {
      listPriceLists: (supplierId) => {
          dispatch(listPriceLists(supplierId))
      },
      createPriceList: (supplierId, data) => {
        dispatch(createPriceList(supplierId, data))
      }
    }
  }
)(PriceLists)
