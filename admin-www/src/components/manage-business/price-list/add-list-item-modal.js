import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Checkbox from 'material-ui/Checkbox'
import moment from 'moment-timezone'
import ProductsAutocomplete from '../../reports/products-autocomplete'

const styles = {
  lightbox: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    cursor: 'pointer',
    background: 'rgba(0,0,0,0.62)',
    zIndex: 1999
  },
  modal: {
    display: 'inline-block',
    overflowY: 'auto',
    overflowX: 'hidden',
    width: '90vw',
    height: 'auto',
    background: '#fff',
    position: 'absolute',
    margin: 'auto',
    top: '25%',
    left: '0',
    right: '0',
    zIndex: '999999999',
    padding: '0.5em',
    borderRadius: '2px'
  },
  header: {
    marginLeft: '0.5em'
  },
  form: {},
  modalOption: {
    float: 'right',
    marginTop: '1em',
    marginRight: '1em'
  },
  modalOptions: {
    paddingLeft: '3px'
  },
  priceColumn: {
    width: "80px"
  },
  labelColumn: {
    width: '200px'
  },
  overflow: {
    overflow: 'visible'
  },
  error: {
    marginLeft: '1em',
    color: 'red'
  },
  checkbox: {
    width: '50px'
  }
}

class AddListItemModal extends Component {

  constructor ( props ) {

    super(props)
    this.defaultValues = {
      activated: false,
      priceListItem: null,
      product: '',
      itemId: -1,
      options: [],
      productName: '',
      productCode: ''
    }
    this.state = Object.assign({}, this.defaultValues)

  }

  componentWillMount () {

    if ( this.props.priceListItem ) {

      this.setState( { priceListItem: this.props.priceListItem }  )

    } else {

      this.setState( Object.assign({}, this.defaultValues ) )
      this.setState({
        itemId: -1
      })

    }

  }

  componentWillReceiveProps ( nextProps ) {

    if ( !!! this.props.priceListItem && !! nextProps.priceListItem ) {

      this.setState( { priceListItem: this.props.priceListItem } )

    }

  }

  componentWillUpdate ( nextProps, nextState ) {

    if ( false == nextState.activated && true == this.state.activated ) {

      this.reset()

    }

    if ( nextState.productName != this.state.productName ) {

        this.clearErrors(nextState)

    }

  }

  setProduct ( product ) {

    let options = Array.from( product.options )

    options.map( opt => {

      if ( !! opt.net ) {

        opt.net.amount = 0

      }

      if ( !! opt.retail ) {

        opt.retail.amount = 0

      }

      opt.enabled = false

    })

    this.setState({
        options,
        productName: product.name,
        itemId: product.id,
        productCode: product.code,
        product: Object.assign({}, product)
    })

  }

  toggleProductOption ( id ) {

    let options = this.state.options

    options.map( opt => {

      if ( opt.id == id ) {

        opt.enabled = !opt.enabled

      }

    })

    this.setState({
      options
    })

  }

  setRetailPrice ( id, price, editingItem ) {

    let options = this.state.options,
        parsedPrice = parseFloat(price)

    if ( isNaN( parsedPrice ) ) {

      parsedPrice = 0

    }

    if ( editingItem ) {

      this.setState({
        priceListItem: Object.assign({}, this.state.priceListItem, {
          item: Object.assign({}, this.state.priceListItem.item, {
            retail: Object.assign({}, this.state.priceListItem.item.retail, {
              amount: Math.round(price * 100)
            })
          })
        })
      })

    } else {

      options.map(opt => {

      if ( opt.id == id ) {

        if ( opt.retail == undefined ) {

          opt.retail = {
            currencyCode: "USD",
            amount: 0
          }

        }

        opt.retail.amount = parsedPrice

      }

      })
      this.setState({
        options
      })

    }

  }

  setNetPrice ( id, price, editingItem ) {

    let options = this.state.options,
        parsedPrice = parseFloat( price )

    if ( isNaN( parsedPrice ) ) {

      parsedPrice = 0
    
    }

    if ( editingItem ) {

      this.setState({
        priceListItem: Object.assign({}, this.state.priceListItem, {
          item: Object.assign({}, this.state.priceListItem.item, {
            net: Object.assign({}, this.state.priceListItem.item.net, {
              amount: Math.round(price * 100)
            })
          })
        })
      })

    } else {

      options.map(opt => {

        if ( opt.id == id ) {

          if ( opt.net == undefined ) {

            opt.net = {
              currencyCode: "USD",
              amount: 0
            }

          }

          opt.net.amount = parsedPrice

        }

      })
      this.setState({
        options
      })

    }

  }

  reset ( callback ) {

    this.setState(
      this.defaultValues, () => {

        this.setState({
          options: []
        })

        this.setState({
          errorProduct: '',
          errorOptions: ''
        })

        !! callback && callback()

      }
    )
    
  }

  toggleForm ( force ) {

    if ( force != undefined ) {

      this.setState({
        activated: force
      })

    } else {

      this.setState({
        activated: !this.state.activated
      })

    }
    

  }

  validate ( data ) {

    let valid = true,
        selected = 0,
        errorProduct = '',
        errorOptions = ''

    if (data.productName == '') {
      errorProduct = 'Product is required.'
      valid = false
    }
    
    data.options.map( option => {

      console.log(option)

      if ( option.enabled ) {

        if ( option.net == undefined ) {

          option.net = {
            currencyCode: "USD",
            amount: 0
          }

        }

        if ( option.retail == undefined ) {

          option.retail = {
            currencyCode: "USD",
            amount: 0
          }

        }

        if ( option.net.amount < 0 ) {

          errorOptions = 'Net price must be at least 0.00'
          valid = false

        } 
        
        if ( option.retail.amount < 0 ) {

          errorOptions = errorOptions == '' ? 'Retail price must be at least 0.00.' : errorOptions+' Retail price must be at least 0.00.'
          valid = false

        } else {

          selected ++

        }

      }

    })

    if ( data.options.length == 0 || selected == 0 ) {

      if ( errorOptions == '' ) {

        errorOptions = 'At least one product option must be selected.'

      }

      valid = false

    }

    this.setState({
      errorProduct,
      errorOptions,
      valid
    })
    return valid

  }

  clearErrors(data) {

    let errorProduct = this.state.errorProduct,
        errorOptions = this.state.errorOptions

    if (data.product != '') {
      errorProduct = ''
    }

    if (data.options.length > 0) {
      errorOptions = ''
    }

    this.setState({
      errorProduct,
      errorOptions
    })

  }

  submit () {

    let data = false

    if ( this.props.priceListItem ) {

      this.props.updatePriceListItem( this.props.supplierId, this.props.priceListId, this.state.priceListItem.item.id, this.state.priceListItem.item )
      this.toggleForm( false )

    } else {

      if ( this.validate(this.state) ) {

      this.state.options.filter( option => { // don't try to add items that have already been added
          
          let added = false

          !!this.props.priceListItems && this.props.priceListItems.map(item=>{

            let itemAddr = item.item.catalogAddress
            if (itemAddr.itemId == this.state.itemId && itemAddr.optionId == option.id) {
              added = true
            }

          })

          return !added
        }).map((opt, i) => { console.log("saving list item: option:", opt)
        
        if (opt.enabled) {

         data = {
            priceListId: this.props.priceListId,
            catalogAddress: {
              catalogId: this.state.product.catalogId,
              itemId: this.state.product.id,
              optionId: opt.id
            },
            status: "STATUS_UNKNOWN",
            typeOf: "TYPE_UNKNOWN",
            net: opt.net != null ? Object.assign({}, opt.net, {
              amount: Math.round(opt.net.amount * 100)
            }) : {
              amount: 0,
              currencyCode: "USD"
            },
            retail: opt.retail != null ? Object.assign({}, opt.retail, {
              amount: Math.round(opt.retail.amount * 100)
            }) : {
              amount: 0,
              currencyCode: "USD"
            },
            rate: {
              fixed: 0,
              percent: 0
            }
          }

          this.props.createPriceListItem(this.props.supplierId, this.props.priceListId, data)

        }

      }) 
      this.setState({
        errorProduct: '',
        errorOptions: ''
      })
      this.reset( () => {

        this.props.onSave(data)
        this.toggleForm( false )

      })
      
    }

    }

  }

  cancel () {

      this.reset( () => {

        this.toggleForm( false )

      })
      
  }

  renderProductOptions ( options, priceListItems ) {

    return (
      <TableBody displayRowCheckbox={false}>
      {
        options.filter(option=>{
          let added = false
          !!this.props.priceListItems && this.props.priceListItems.map(item=>{
            let itemAddr = item.item.catalogAddress
            if (itemAddr.itemId == this.state.itemId && itemAddr.optionId == option.id) {
              added = true
            }
          }) 
          return !added
        }).map((option, i) => {
          return (
            <TableRow selectable={false}>
              <TableRowColumn>
                { this.state.productName }
              </TableRowColumn>
              <TableRowColumn>
                { this.state.productCode }
              </TableRowColumn>
              <TableRowColumn>
                { option.title }
              </TableRowColumn>
              <TableRowColumn>
                { option.code }
              </TableRowColumn>
              <TableRowColumn style={styles.priceColumn}> 
                { option.currency || "USD" }
              </TableRowColumn>
              <TableRowColumn style={styles.priceColumn}>
                { option.retailPrice }
                <TextField onChange={e=> { this.setRetailPrice(option.id, e.target.value) }}
                           defaultValue={!!option.retail ? option.retail.amount : 0.0 }
                           style={styles.numeric}
                           type='text'
                           key={"numeric"+i}
                           id={"numeric"+i}
                />
              </TableRowColumn>
              <TableRowColumn style={styles.priceColumn}>
                <TextField onChange={e=> { this.setNetPrice(option.id, e.target.value) }}
                           defaultValue={!!option.net ? option.net.amount : 0.0 }
                           style={styles.numeric}
                           type='text'
                           key={"numeric"+i}
                           id={"numeric"+i}
                />
              </TableRowColumn>
              <TableRowColumn style={styles.checkbox}>
                <Checkbox onKeyDown={ (e) => { if ( e.which == 32 || e.which == 13 ) { this.toggleProductOption(option.id) } } }
                          onCheck={(e, v)=> this.toggleProductOption(option.id)}
                          style={styles.checkbox}
                />
              </TableRowColumn>
              
            </TableRow>
        )
      })
    }
    </TableBody>
    )

  }

  renderPriceListItem ( priceListItem, options, priceListItems ) {

    let meta = priceListItem,
        item = meta.item

    return (
      <TableBody displayRowCheckbox={ false }>
            <TableRow selectable={ false }>
              <TableRowColumn>
                <TextField defaultValue={ meta.catItemName }
                           style={ styles.numeric }
                           disabled={ true }
                           type='text'
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField defaultValue={ meta.catItemCode }
                           style={ styles.numeric }
                           disabled={ true }
                           type='text'
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField defaultValue={ meta.catItemOptName }
                           style={ styles.numeric }
                           disabled={ true }
                           type='text'
                />
              </TableRowColumn>
              <TableRowColumn>
                <TextField defaultValue={ meta.catItemOptCode }
                           style={ styles.numeric }
                           disabled={ true }
                           type='text'
                />
              </TableRowColumn>
              <TableRowColumn style={ styles.priceColumn }> 
                <TextField defaultValue={ item.currency || "USD" }
                           style={ styles.numeric }
                           disabled={ true }
                           type='text'
                />
              </TableRowColumn>
              <TableRowColumn style={ styles.priceColumn }>
                <TextField onChange={ e => { this.setRetailPrice( -1, e.target.value, true ) } }
                           defaultValue={ !!item.retail ? item.retail.amount /100 : 0.0 }
                           style={ styles.numeric }
                           key={ "numeric" }
                           id={ "retailInput" }
                           type='text'
                />
              </TableRowColumn>
              <TableRowColumn style={ styles.priceColumn }>
                <TextField onChange={ e => { this.setNetPrice( -1, e.target.value, true) } }
                           defaultValue={ !!item.net ? item.net.amount /100 : 0.0 }
                           style={ styles.numeric }
                           type='text'
                           key={ "numeric" }
                           id={ "netInput" }
                />
              </TableRowColumn>
              <TableRowColumn style={styles.checkbox}></TableRowColumn>
            </TableRow>
    </TableBody>
    )

  }

  render() {

      if (this.state.activated) {
          return (
              <div style={styles.lightbox}>
                  <div style={styles.modal}>
                  <h2 style={styles.header}>{this.props.label || "Add Product to Price List"}</h2>
                    { this.props.priceListItem ? "" : this.props.supplierCatalogItemsFetching ? "Loading..." : (
                    <ProductsAutocomplete onProduct={ r=> this.setProduct(r) }
                                          supplierId={ this.props.adminSupplierId != "-1" ? this.props.adminSupplierId : this.props.supplierId }
                                          priceListItems={ this.props.priceListItems || [] }
                                          fetching={ this.props.supplierCatalogItemsFetching }
                                          catalogMode={true} // don't use old v1 products endpoint
                                          style={{width: '450px'}}
                    />
                    ) }
                  <span style={styles.error}>{ this.state.errorProduct }</span>
                  <Table style={styles.form}>
                    <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                        <TableRow>
                          <TableHeaderColumn>
                            Product Name
                          </TableHeaderColumn>
                          <TableHeaderColumn>
                            Product Code
                          </TableHeaderColumn>
                          <TableHeaderColumn>
                            Name
                          </TableHeaderColumn>
                          <TableHeaderColumn>
                            Code
                          </TableHeaderColumn>
                          <TableHeaderColumn style={styles.priceColumn}>
                            Currency
                          </TableHeaderColumn>
                          <TableHeaderColumn style={styles.priceColumn}>
                            Retail Price
                          </TableHeaderColumn>
                          <TableHeaderColumn style={styles.priceColumn}>
                            Net Price
                          </TableHeaderColumn>
                          <TableHeaderColumn style={styles.checkbox}>
                           
                          </TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    { this.props.priceListItem ? 
                    this.renderPriceListItem( this.props.priceListItem, this.state.options, this.props.priceListItems ) :
                    this.renderProductOptions( this.state.options, this.props.priceListItems ) }
                 </Table>
                 <div style={styles.modalOptions}>
                    <span style={styles.error}>{ this.state.errorOptions }</span>
                    <RaisedButton onTouchTap={e => { this.submit() } }
                                  style={ styles.modalOption }
                                  primary={true}
                                  label="Save"
                     />
                     <RaisedButton onTouchTap={e => { this.cancel() } }
                                   style={ styles.modalOption }
                                   label="Cancel"
                      />
                  </div>
                </div>
            </div>
          )

      } else {

          return (
            !!this.props.flat ? (
              <FlatButton onTouchTap={e => { this.toggleForm() } }
                            style={( this.props.style  || {} )}
                            label={ this.props.label || "Add Product" }
                            primary={ this.props.primary !== false }
               />
            ) : (
              <RaisedButton onTouchTap={e => { this.toggleForm() } }
                            style={( this.props.style  || {} )}
                            label={ this.props.label || "Add Product" }
                            primary={ this.props.primary !== false }
              />
            )
              
          )

      }
  }

}

AddListItemModal.propTypes = {
  onSave: PropTypes.func.isRequired,
  supplierId: PropTypes.string.isRequired,
  adminSupplierId: PropTypes.string,
  priceListId: PropTypes.string.isRequired,
  priceListItem: PropTypes.object
}


import { connect } from 'react-redux'
import { 
  updatePriceList,
  createPriceListItem,
  updatePriceListItem
} from '../../../ax-redux/actions/price-lists'
export default connect(
  state => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      pathname: state.routing.locationBeforeTransitions.pathname,
      queryParams: state.routing.locationBeforeTransitions.search
    }
  },
  dispatch => {
    return {
      updatePriceList: ( supplierId, id, data ) => {
        dispatch(updatePriceList( supplierId, id, data ))
      },
      createPriceListItem: ( supplierId, priceListId, data ) => {
        dispatch(createPriceListItem( supplierId, priceListId, data ))
      },
      updatePriceListItem: ( supplierId, priceListId, id, data ) => {
        dispatch(updatePriceListItem( supplierId, priceListId, id, data ))
      }
    }
  }
)(AddListItemModal)
