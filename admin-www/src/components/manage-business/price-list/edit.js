import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import CircularProgress from 'material-ui/CircularProgress'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import Snackbar from 'material-ui/Snackbar'
import { browserHistory } from 'react-router'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import {formatCurrency} from '../../../util'
import AddListItemModal from './add-list-item-modal'
import DisableListItemModal from './disable-list-item-modal'

const styles = {
  gridHeader: {
    width: '100%',
    height: '60px'
  },
  editProduct: {
    width: '110px',
    overflow: 'visible'
  },
  editReseller: {
    width: '130px',
    overflow: 'visible'
  },
  checkboxCol: {
    width: '48px'
  },
  priceCol: {
    width: '96px'
  },
  productName: {
    cursor: 'pointer'
  },
  addContractButton: {
    float: 'right'
  },
  title: {
     color: 'rgb(67, 67, 67)',
     display: 'inline-block',
     marginLeft: '1em',
     marginRight: '1em'
  },
  price: {
    width: '60px'
  },
  buttons: {
    width: '195px',
    paddingLeft: '0px',
    paddingRight: '0px'
  }
}

class PriceListEdit extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    let limit = 20,
        offset = 0,
        product = '',
        sort = ''
        

    if (!!this.props.location.query) {
        if (!!this.props.location.query.offset) {
          offset = this.props.location.query.offset;
        }
        if (this.props.location.query.product) {
          product = this.props.location.query.product
        }
    }
    this.setState({
      offset,
      limit,
      sort,
      product,
      showSnackbar: false,
      supplierCatalogId: -1
    })
    this.props.getPriceList(this.props.params.supplierId, this.props.params.id)
    this.props.listPriceListItems(this.props.params.supplierId, this.props.params.id)
  }

  componentWillReceiveProps( nextProps ) {

    let priceListItemAdded = this.props.priceListItemSaving != false && nextProps.priceListItemSaving == false && nextProps.priceListItemError == false,
        priceListItemUpdated = this.props.priceListItemUpdating && nextProps.priceListItemUpdating == false

    if ( priceListItemAdded || priceListItemUpdated ) {

      this.props.listPriceListItems(nextProps.params.supplierId, nextProps.params.id)
      this.setState({
        showSnackbar: true,
        snackbarText: `Price List Item ${priceListItemAdded ? "Added" : "Updated"}`
      })

    }

    if (this.props.pathname != nextProps.pathname ||
       this.props.queryParams != nextProps.queryParams) {

      let nextQuery = null,
          requestParams = {
              limit: this.state.limit,
              offset: this.state.offset
      }

      if (nextProps.location.query) {
        nextQuery = nextProps.location.query
        if (nextQuery.offset) {
            requestParams.offset = nextQuery.offset
        }
        if (nextQuery.limit) {
            requestParams.limit = nextQuery.limit
        }
        if (nextQuery.sort) {
            requestParams.sort = nextQuery.sort
        }
      }
      // fetch contracts here
    }

    if ( this.props.disablePriceListItemFetching == true && nextProps.disablePriceListItemFetching == false && nextProps.disablePriceListItemData != false ) {
      this.props.priceListItemsFilterUpdate()
    }

    if ( this.props.priceListItemError == false && nextProps.priceListItemError != false ) {
      this.setState({
        showSnackbar: true,
        snackbarText: nextProps.priceListItemError.data.Error
      })
    }

    if ( nextProps.priceListItems == false && this.props.priceListItemsFetching == false && nextProps.priceListItemsFetching == false && !!nextProps.params.supplierId && !!nextProps.params.id )  {

      this.props.listPriceListItems( nextProps.params.supplierId, nextProps.params.id )

    }

    if ( this.props.supplierCatalogId != nextProps.supplierCatalogId ) {

      let supplierId = nextProps.params.supplierId,
          id = nextProps.params.id
      browserHistory.push(`/app/manage-business/price-lists/${supplierId}/${nextProps.supplierCatalogId}/${id}`)

    }

    if ( this.props.supplierCatalogId != nextProps.supplierCatalogId || (!!!nextProps.supplierCatalogItemsFetching && nextProps.supplierCatalogId != -1 && !!!nextProps.supplierCatalogItems) ) {

       if ( nextProps.adminSupplierId != -1 ) {

          this.props.listSupplierCatalogItems( nextProps.adminSupplierId, nextProps.supplierCatalogId )

        } else {

          this.props.listSupplierCatalogItems( nextProps.supplierId, nextProps.supplierCatalogId )

        }

    }

  }

  componentWillUpdate( nextProps, nextState ) {
    
     
  }

  onAddItemCallback (data) {
      // display snack bar...

  }

  remove (id, version) {
    this.props.disablePriceListItem(this.props.params.supplierId, this.props.params.id, id, version)
  }

  handleRequestClose () {
      this.setState({
        showSnackbar: false,
      })
  }

  renderListItems (listItems) {

    let catalogId = this.props.params.catalogId
      return (
          <Table className="products-table">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>
                      <div className="inner">Product Code</div>
                    </TableHeaderColumn>
                    <TableHeaderColumn className="sortable">
                      <div className="inner">Product Name</div>
                    </TableHeaderColumn>
                    <TableHeaderColumn>
                      Option Code
                    </TableHeaderColumn>
                    <TableHeaderColumn>
                      Option Name
                    </TableHeaderColumn>
                    <TableHeaderColumn style={ styles.price }>
                      Currency
                    </TableHeaderColumn>
                    <TableHeaderColumn style={ styles.price }>
                      Retail Price
                    </TableHeaderColumn>
                    <TableHeaderColumn style={ styles.price }>
                      Net Price
                    </TableHeaderColumn>
                    <TableHeaderColumn style={ styles.buttons } >

                    </TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                { listItems != false && listItems.filter((item)=> {
                    return catalogId == -1 || catalogId == item.item.catalogAddress.catalogId
                  }).map((listItem, p)=> {
                  let hasRetailPrice = !!listItem.item.retail
                    return (
                        <TableRow key={p} selectable={false} >
                            <TableRowColumn>
                              { listItem.catItemCode }
                            </TableRowColumn>
                            <TableRowColumn>
                              { listItem.catItemName }
                            </TableRowColumn>
                            <TableRowColumn>
                              { listItem.catItemOptCode }
                            </TableRowColumn>
                            <TableRowColumn>
                              { listItem.catItemOptName }
                            </TableRowColumn>
                            <TableRowColumn style={ styles.price } >
                              { listItem.item.net.currencyCode }
                            </TableRowColumn>
                            <TableRowColumn style={ styles.price }>
                              { hasRetailPrice ? formatCurrency(listItem.item.retail.currencyCode, listItem.item.retail.amount) : '–' }
                            </TableRowColumn>
                            <TableRowColumn style={ styles.price }>
                              { formatCurrency(listItem.item.net.currencyCode, listItem.item.net.amount) }
                            </TableRowColumn>
                            <TableRowColumn style={ styles.buttons } >
                              <AddListItemModal supplierCatalogItemsFetching={ this.props.supplierCatalogItemsFetching }
                                                style={ { marginRight: '1em' } }
                                                adminSupplierId={ ""+this.props.adminSupplierId }
                                                onSave={ v=> {} }
                                                priceListItems={ this.props.priceListItems }
                                                supplierId={ this.props.params.supplierId }
                                                priceListId={ this.props.params.id }
                                                priceListItem={ listItem }
                                                label={ 'Edit' }
                                                flat={ true }
                                                primary={ false }
                              />
                              <DisableListItemModal onSubmit={ ()=> { this.remove(listItem.item.id, listItem.item.version) } } />
                            </TableRowColumn>
                        </TableRow>
                )
              })}
            </TableBody>
         </Table>
      )

  } //  style={ {float: 'right', marginTop: '1em', marginRight: '1em'} }
  
  render() {

      let fetching = this.props.resellerFetching,
          canRenderListItemModal = (!!this.props.priceListItems || this.props.noPriceListItems) && !!!this.props.priceListItemsFetching
    
    return (
      <div className="reseller-view" >
        <h2 style={styles.title}>Price List ({ this.props.priceList ? this.props.priceList.name : "" })</h2>
          
          { canRenderListItemModal ? (

              <AddListItemModal supplierCatalogItemsFetching={ this.props.supplierCatalogItemsFetching }
                                style={ {float: 'right', marginTop: '1em', marginRight: '1em'} }
                                adminSupplierId={ ""+this.props.adminSupplierId }
                                onSave={ v=> this.onAddItemCallback(v) }
                                priceListItems={ this.props.priceListItems }
                                supplierId={ this.props.params.supplierId }
                                priceListId={ this.props.params.id }  
              />

          ) : "Loading Items..." }

        <Card className="inner-edit">
            <div className="reseller-view-section reseller-products">
                { this.renderListItems(this.props.priceListItems) }
            </div>
        </Card>
        <Snackbar message={this.state.snackbarText}
                  onRequestClose={(e)=>this.handleRequestClose()}
                  open={this.state.showSnackbar}
                  autoHideDuration={4000}
        />
      </div>
    )

  }

}

import { connect } from 'react-redux'
import {
  listSupplierCatalogs,
  listSupplierCatalogItems
} from '../../../ax-redux/actions/suppliers'
import {
  disablePriceListItem,
  listPriceListItems,
  getPriceList,
  priceListItemsFilterUpdate,
} from '../../../ax-redux/actions/price-lists'
export default connect(
  state => {
    return {
      priceListItemUpdating: state.priceLists.updatePriceListItem.fetching,
      priceListItemSaving: state.priceLists.createPriceListItem.fetching,
      priceListItemError: state.priceLists.createPriceListItem.error,
      priceList: state.priceLists.getPriceList.data != false ? state.priceLists.getPriceList.data.priceList.priceList : false,
      priceListFetching: state.priceLists.getPriceList.fetching,
      priceListItems: !!state.priceLists.listPriceListItems.data ? (!!state.priceLists.listPriceListItems.data.items ?
                      state.priceLists.listPriceListItems.data.items : false) : false,
      noPriceListItems: !!state.priceLists.listPriceListItems.data && !!!state.priceLists.listPriceListItems.data.items,
      priceListItemsFetching: state.priceLists.listPriceListItems.fetching,
      disablePriceListItemData: state.priceLists.disablePriceListItem.data != false ? state.priceLists.disablePriceListItem.data.priceListItem.priceListItem : false,
      disablePriceListItemFetching: state.priceLists.disablePriceListItem.fetching,
      supplierCatalogs: !!state.suppliers.listCatalogs.data ? state.suppliers.listCatalogs.data.catalogs : false,
      supplierCatalogsFetching: state.suppliers.listCatalogs.fetching,
      supplierCatalogItemsFetching: state.suppliers.listCatalogItems.fetching,
      supplierCatalogItems: state.suppliers.listCatalogItems.data,
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      pathname: state.routing.locationBeforeTransitions.pathname,
      queryParams: state.routing.locationBeforeTransitions.search,
      supplierId: state.app.user.supplierId,
      adminSupplierId: state.reportFilters.adminSupplierId,
      supplierCatalogId: state.reportFilters.supplierCatalogId,
      resellerCatalogId: state.reportFilters.resellerCatalogId
    }
  },
  dispatch => {
    return {
      getPriceList: (supplierId, id) => {
          dispatch(getPriceList(supplierId, id))
      },
      listPriceListItems: (supplierId, priceListId) => {
          dispatch(listPriceListItems(supplierId, priceListId))
      },
      disablePriceListItem: (supplierId, priceListId, id, version) => {
          dispatch(disablePriceListItem(supplierId, priceListId, id, version))
	    },
	    priceListItemsFilterUpdate: () => {
          dispatch(priceListItemsFilterUpdate())
	    },
      listSupplierCatalogItems: (supplierId, catalogId) => {
          dispatch(listSupplierCatalogItems(supplierId, catalogId))
      }
    }
  }
)(PriceListEdit)
