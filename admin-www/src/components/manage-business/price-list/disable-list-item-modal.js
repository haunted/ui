import React, { Component, PropTypes } from 'react'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'

let styles = {
    modalButton: {
        float: 'right',
        marginTop: '7em',
        marginRight: '1em'
    },
    modal: {
        
    },
    title: {
        fontSize: '1.5em',
        display: 'block'
    },
    innerModal: {
        width: '400px',
        height: '165px'
    }
}

export default class DisableListItemModal extends Component {

  constructor( props ) {
 
    super(props);

    this.state = {
      activated: false,
      comments: ""
    }

  }

  toggleForm () {

    this.setState({
        activated: !this.state.activated
    })

  }

  submit () {

      this.toggleForm()
      !! this.props.onSubmit && this.props.onSubmit()

  }

  cancel () {

      this.toggleForm()

  }

  onCommentsChange ( e ) {

      this.setState({
          comments: e.target.value
      })

  }

  render() {

      if ( this.state.activated ) {

          return (
              <div className="ticket-lightbox queue-for-review-button" style={ styles.modal } >
                  <div className="inner-lightbox inner" style={ styles.innerModal } >
                    <span style={ styles.title } >Confirm disabling price list item?</span>
                    <RaisedButton onTouchTap={e => { this.submit() } } label="Disable" style={ styles.modalButton } primary={true} />
                    <RaisedButton onTouchTap={e => { this.cancel() } } label="Cancel" style={ styles.modalButton } />
                </div>
            </div>
          )

      } else {

          return (
              <RaisedButton
                    onTouchTap={ e => { this.toggleForm() } }
                    style={ this.props.style  || {} }
                    label={ this.props.label || "Disable" }
               />
          )

      }

  }

}

DisableListItemModal.propTypes = {
  onSubmit: PropTypes.func.isRequired
}
