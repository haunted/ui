import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import DatePicker from 'material-ui/DatePicker'
import Checkbox from 'material-ui/Checkbox'
import moment from 'moment-timezone'

const styles = {
  lightbox: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    cursor: 'pointer',
    background: 'rgba(0,0,0,0.62)',
    zIndex: 1999
  },
  modal: {
    minHeight: '220px',
    display: 'inline-block',
    overflowY: 'auto',
    overflowX: 'hidden',
    minWidth: '389px',
    padding: '1em',
    borderRadius: '2.5px',
    background: '#fff',
    position: 'absolute',
    margin: 'auto',
    top: '48px',
    left: '0',
    right: '0',
    bottom: '0',
    width: '0',
    height: '0',
    zIndex: '999999999'
  },
  header: {},
  form: {},
  modalOption: {
    marginTop: '1em',
    marginLeft: '1em',
    float: 'right'
  },
  modalOptions: {
    paddingLeft: '3px'
  },
  labelColumn: {
    width: '122px'
  },
  overflow: {
    overflow: 'visible'
  },
  error: {
    marginLeft: '1em',
    color: 'red'
  },
  numeric: {
    padding: '0.5em',
    borderRadius: '2px',
    border: 'none',
    boxShadow: 'rgba(0, 0, 0, 0.117647) 0px 1px 6px, rgba(0, 0, 0, 0.117647) 0px 1px 4px'
  }
}

export default class AddPriceListModal extends Component {
  constructor(props) {
    super(props)
    this.defaultValues = {
      activated: false,
      errorName: '',
      errorCommission: '',
      name: '',
      commission: 0
    }
    this.state = Object.assign({}, this.defaultValues)
  }
  componentDidMount () {
    if (this.props.value) {
      this.setState(this.props.value)
    } else {
      this.setState(this.defaultValues)
    }
  }
  componentWillUpdate(nextProps, nextState) {
    if (false == nextState.activated && true == this.state.activated) {
      this.reset()
    }
    if (nextState.name != this.state.name ||
        nextState.commission != this.state.commission) {
          this.clearErrors(nextState)
    }
  }
  reset () {
    this.setState(
      this.defaultValues
    )
    this.setState({
      errorName: '',
      errorCommission: ''
    })
  }
  setCommission(value) {
    let out = value
    if (isNaN(value)) {
      out = -1
    } else {
      out = parseInt(value)
    }
    this.setState({
      commission: out
    })
  }
  toggleForm () {
    this.setState({
        activated: !this.state.activated
    })
  }
  validate (data) {
    let valid = true,
        errorName = '',
        errorCommission = ''
    if (data.name == '') {
      errorName = 'Name is required.'
      valid = false
    }
    if (data.commission < 0 || data.commission > 100) {
      errorCommission = 'Commission must be a valid percentage.'
      valid = false
    }

    this.setState({
      errorCommission,
      errorName,
      valid
    })
    return valid
  }
  clearErrors(data) {
    let errorName = this.state.errorName,
        errorCommission = this.state.errorCommission
    if (data.name != '') {
      errorName = ''
    }
    if (data.commission != '' && data.commission <= 100 && data.commission > 0) {
      errorCommission = ''
    }

    this.setState({
      errorName,
      errorCommission
    })
  }
  submit () {
    let data = false
    if (this.validate(this.state)) {
      data = {
        rate: {
          percent: this.state.commission,
          fixed: 0
        },
        name: this.state.name
      }
      this.toggleForm()
      this.props.onSave(data)
    }
  }
  cancel () {
      this.reset()
      this.toggleForm()
  }
  render() {
      if (this.state.activated) {
          return (
              <div style={styles.lightbox}>
                  <div style={styles.modal}>
                  <h2 style={styles.header}>{this.props.label || "Add New Price List"}</h2>
                  <Table style={styles.form}>
                    <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                        <TableRow style={{display: 'none'}}>
                            <TableHeaderColumn style={styles.labelColumn}></TableHeaderColumn>
                            <TableHeaderColumn></TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                      <TableRow selectable={false} >
                            <TableRowColumn style={styles.labelColumn}>
                                Name
                            </TableRowColumn>
                            <TableRowColumn style={styles.overflow}>
                              <TextField defaultValue={this.state.name} hintText={"Name"}
                                         key="Name"
                                         errorText={this.state.errorName}
                                         onChange={e=>{ this.setState({name: e.target.value}) }}
                              />
                            </TableRowColumn>
                        </TableRow>
                        <TableRow selectable={false} >
                            <TableRowColumn>
                                Commission
                            </TableRowColumn>
                            <TableRowColumn>
                              <input onChange={e=> { this.setCommission(e.target.value) }}
                                     value={this.state.commission}
                                     style={styles.numeric}
                                     type='number'
                                     min='1'
                                     max='99'
                              />
                              <span style={styles.error}>{ this.state.errorCommission }</span>
                            </TableRowColumn>
                        </TableRow>
                    </TableBody>
                 </Table>
                 <div style={styles.modalOptions}>
                    <RaisedButton
                          onTouchTap={e => { this.submit() } }
                          style={styles.modalOption}
                          primary={true}
                          label="Save"
                     />
                     <RaisedButton
                           onTouchTap={e => { this.cancel() } }
                           style={styles.modalOption}
                           label="Cancel"
                      />
                  </div>
                </div>
            </div>
          )
      } else {
          return (
              <RaisedButton
                    onTouchTap={e => { this.toggleForm() } }
                    style={(this.props.style  || {})}
                    label={this.props.label || "Add New Price List"}
                    primary={this.props.primary || false}
               />
          )
      }
  }
}

AddPriceListModal.propTypes = {
  onSave: PropTypes.func.isRequired,
  label: PropTypes.string,
  value: PropTypes.object
}
