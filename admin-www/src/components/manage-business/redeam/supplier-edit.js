/* globals window */

import React, { PropTypes, Component } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { reduxForm, Field } from 'redux-form';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import { TextField, Location } from '../../formElements';
import ViewOrgDataButton from '../view-org-data-button';
import { formatStatusEnum } from '../../../util';
import * as supplierActions from '../../../ax-redux/actions/suppliers';
import countries from '../../../resources/countries.json';
import states from '../../../resources/states.json';


const styles = {
  valueCol: {
    height: 100,
    fontSize: 16,
  },
  formOptions: {
    marginBottom: '1em',
    marginTop: '3em',
    marginLeft: '1em',
    height: '1em',
  },
  formOption: {
    float: 'right',
    marginLeft: '1em',
  },
  topAligned: {
    verticalAlign: 'top',
  },
};


class SupplierEditor extends Component {
  constructor(props) {
    super(props);

    this.save = this.save.bind(this);
    this.disable = this.disable.bind(this);
    this.openDevices = this.openDevices.bind(this);
  }

  componentWillMount() {
    const {
      params,
      getSupplier,
      editMode,
    } = this.props;

    if (editMode) {
      getSupplier(params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      change,
      supplier,
      editMode,
    } = this.props;

    // todo figure out a better way to pre-populate the form
    if (editMode && !supplier && nextProps.supplier) {
      change('name', nextProps.supplier.name);
      change('code', nextProps.supplier.code);
      change('mainLocation', nextProps.supplier.mainLocation);
      change('fee.percentage', nextProps.supplier.fee.percentage);
      change('fee.min', nextProps.supplier.fee.min);
      change('fee.cap', nextProps.supplier.fee.cap);
    }
  }

  save(values) {
    const {
      editMode,
      createSupplier,
      updateSupplier,
      supplier,
    } = this.props;

    const {
      fee: { percentage, min, cap },
      ...otherValues
    } = values;

    const data = {
      fee: {
        percentage: parseFloat(percentage) || 0,
        min: parseFloat(min) || 0,
        cap: parseFloat(cap) || 0,
      },
      ...otherValues,
    };

    if (editMode) {
      updateSupplier(supplier.id, {
        ...supplier,
        ...data,
      });
    } else {
      createSupplier(data);
    }
  }

  disable() {
    const {
      supplier,
      disableSupplier,
    } = this.props;

    if (window.confirm('Disable this supplier?')) {
      disableSupplier(supplier.id, supplier.version);
    }
  }

  openDevices() {
    const { supplier } = this.props;

    browserHistory.push(`app/redeam/supplier/${supplier.code}/devices`);
  }

  renderForm() {
    const {
      editMode,
      change,
      supplier,
    } = this.props;

    return (
      <Table className="edit-table">
        <TableBody displayRowCheckbox={false}>
          {editMode && (
            <TableRow selectable={false}>
              <TableRowColumn>Id</TableRowColumn>
              <TableRowColumn style={styles.valueCol}>
                {supplier.id}
              </TableRowColumn>
            </TableRow>
          )}

          {editMode && (
            <TableRow selectable={false}>
              <TableRowColumn>Version</TableRowColumn>
              <TableRowColumn style={styles.valueCol}>
                {supplier.version}
              </TableRowColumn>
            </TableRow>
          )}

          {editMode && (
            <TableRow selectable={false}>
              <TableRowColumn>Status</TableRowColumn>
              <TableRowColumn style={styles.valueCol}>
                {formatStatusEnum(supplier.status)}
              </TableRowColumn>
            </TableRow>
          )}

          <TableRow selectable={false}>
            <TableRowColumn>Name</TableRowColumn>
            <TableRowColumn style={styles.valueCol}>
              <Field
                label="Name"
                name="name"
                component={TextField}
              />
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false} style={styles.row}>
            <TableRowColumn>Code</TableRowColumn>
            <TableRowColumn style={styles.valueCol}>
              {editMode ? (
                <span>{supplier.code}</span>
              ) : (
                <Field
                  label="Code"
                  name="code"
                  component={TextField}
                />
              )}
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false} style={styles.row}>
            <TableRowColumn>Main Location</TableRowColumn>
            <TableRowColumn style={styles.valueCol}>
              <Field
                name="mainLocation"
                component={Location}
                change={change}
                style={styles.topAligned}
              />
            </TableRowColumn>
          </TableRow>

          <TableRow selectable={false} style={styles.row}>
            <TableRowColumn>Fee Structure</TableRowColumn>
            <TableRowColumn style={styles.valueCol}>
              <Field
                floatingLabelText="Percentage"
                floatingLabelFixed
                name="fee.percentage"
                component={TextField}
                style={styles.topAligned}
              />

              <Field
                floatingLabelText="Min"
                floatingLabelFixed
                name="fee.min"
                component={TextField}
                style={styles.topAligned}
              />

              <Field
                floatingLabelText="Cap"
                floatingLabelFixed
                name="fee.cap"
                component={TextField}
                style={styles.topAligned}
              />
            </TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
    );
  }

  render() {
    const {
      fetching,
      savingSupplier,
      creatingSupplier,
      editMode,
      handleSubmit,
      supplier,
    } = this.props;

    const anyFetching = fetching || savingSupplier || creatingSupplier;

    return (
      <form onSubmit={handleSubmit(this.save)} autoComplete="off">
        <Paper className="inner-edit" zDepth={1}>
          <h2>{editMode ? 'Edit' : 'Add'} Supplier</h2>

          {editMode && anyFetching ? (
            <div className="loading-circle">
              <CircularProgress color={'#27367a'} size={96} />
            </div>
          ) : (
            <div>
              {this.renderForm()}

              <div style={styles.formOptions}>
                <RaisedButton
                  style={styles.formOption}
                  type="submit"
                  className="save-button"
                  label="Save"
                  disabled={anyFetching}
                  primary
                />

                {editMode && (
                  <div>
                    <RaisedButton
                      onClick={this.disable}
                      style={styles.formOption}
                      className="delete-button"
                      label="Disable"
                    />

                    <ViewOrgDataButton
                      style={styles.formOption}
                      id={supplier.id}
                      orgCode={supplier.code}
                      model="price-lists"
                      orgType="supplier"
                    />

                    <ViewOrgDataButton
                      style={styles.formOption}
                      id={supplier.id}
                      orgCode={supplier.code}
                      model="contracts"
                      orgType="supplier"
                    />

                    <ViewOrgDataButton
                      style={styles.formOption}
                      id={supplier.id}
                      orgCode={supplier.code}
                      model="products"
                      orgType="supplier"
                    />

                    <RaisedButton
                      onClick={this.openDevices}
                      style={styles.formOption}
                      label="Devices"
                    />
                  </div>
                )}
              </div>
            </div>
          )}
        </Paper>
      </form>
    );
  }
}

SupplierEditor.propTypes = {
  fetching: PropTypes.bool.isRequired,
  savingSupplier: PropTypes.bool.isRequired,
  creatingSupplier: PropTypes.bool.isRequired,
  editMode: PropTypes.bool.isRequired,
  supplier: PropTypes.shape({
    name: PropTypes.string,
    code: PropTypes.string,
    mainLocation: PropTypes.object,
    fee: PropTypes.object,
  }),
  handleSubmit: PropTypes.func.isRequired,
  params: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  change: PropTypes.func.isRequired,
  getSupplier: PropTypes.func.isRequired,
  disableSupplier: PropTypes.func.isRequired,
  createSupplier: PropTypes.func.isRequired,
  updateSupplier: PropTypes.func.isRequired,
};

SupplierEditor.defaultProps = {
  supplier: null,
};

const validate = (values) => {
  const errors = {};
  const locationErrors = {};
  const feeErrors = {};
  const { country, state, city } = values.mainLocation;
  const { percentage, min, cap } = values.fee;

  if (!values.name) {
    errors.name = 'Name is required';
  }

  if (!values.code) {
    errors.code = 'Code is required';
  }

  if (country || state || city) {
    if (!country) {
      locationErrors.country = 'Country is required';
    } else if (!countries.includes(country)) {
      locationErrors.country = 'Select country from the list';
    }

    if (!state) {
      locationErrors.state = 'State is required';
    } else if (country.includes('usa') && !states.includes(state)) {
      locationErrors.state = 'Select state from the list';
    }

    if (!city) {
      locationErrors.city = 'City is required';
    }

    if (Object.keys(locationErrors).length) {
      errors.mainLocation = locationErrors;
    }
  }

  if (percentage && isNaN(parseFloat(percentage))) {
    feeErrors.percentage = 'Percentage should be a number';
  }

  if (min && isNaN(parseFloat(min))) {
    feeErrors.min = 'Min should be a number';
  }

  if (cap && isNaN(parseFloat(cap))) {
    feeErrors.cap = 'Cap should be a number';
  }

  if (Object.keys(feeErrors).length) {
    errors.fee = feeErrors;
  }

  return errors;
};

const mapStateToProps = (state, ownProps) => {
  const {
    suppliers,
    routing,
  } = state;

  const editMode = !routing.locationBeforeTransitions.pathname.includes('/add');
  const supplierId = ownProps.params.id;
  const createdSupplier = suppliers.create.data ?
    suppliers.create.data.supplier.supplier :
    null;
  const supplier = suppliers.list.suppliersById[supplierId] ?
    suppliers.list.suppliersById[supplierId].supplier :
    createdSupplier;

  return {
    editMode,
    supplier,
    savingSupplier: suppliers.update.fetching,
    saveError: suppliers.update.error,
    creatingSupplier: suppliers.create.fetching,
    createdSupplier: suppliers.create.data,
    fetching: suppliers.list.fetching,
    initialValues: editMode && supplier ? {
      name: supplier.name,
      code: supplier.code,
      mainLocation: supplier.mainLocation,
      fee: supplier.fee,
    } : {
      name: '',
      code: '',
      mainLocation: {
        city: '',
        state: '',
        country: '',
      },
      fee: {
        percentage: 0,
        min: 0,
        cap: 0,
      },
    },
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getSupplier: supplierActions.getSupplier,
  createSupplier: supplierActions.createSupplier,
  disableSupplier: supplierActions.disableSupplier,
  updateSupplier: supplierActions.updateSupplier,
}, dispatch);

const SupplierEditorForm = reduxForm({
  form: 'supplierEditor',
  validate,
})(SupplierEditor);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SupplierEditorForm);
