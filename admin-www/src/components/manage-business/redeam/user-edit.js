import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card } from 'material-ui/Card';
import CircularProgress from 'material-ui/CircularProgress';
import UserForm from '../../forms/UserForm';
import * as accountsActions from '../../../ax-redux/actions/accounts';
import * as groupsActions from '../../../ax-redux/actions/groups';


class UserEdit extends React.Component {
  componentWillMount() {
    const {
      editMode,
      listGroups,
      listAccountGroups,
      readAccount,
      params,
    } = this.props;

    listGroups();

    if (editMode) {
      readAccount(params.id);
      listAccountGroups(params.id);
    }
  }

  render() {
    const {
      user,
      editMode,
      fetching,
      fetchingGroups,
      fetchingSuppliers,
      fetchingAccountGroups,
      accountGroups,
    } = this.props;

    const anyFetching = fetching ||
      fetchingGroups ||
      fetchingSuppliers ||
      fetchingAccountGroups;

    const initialValues = user && editMode ? {
      username: user.username,
      givenName: user.givenName,
      surname: user.surname,
      phone: user.phone,
      email: user.email,
      orgCode: user.orgCode,
      roles: accountGroups.map(g => g.id),
    } : null;

    const formProps = {
      onSubmit: this.save,
      editMode,
    };

    if (initialValues) {
      formProps.initialValues = initialValues;
    }

    return (
      <div className="user-edit">
        <Card className="inner-edit">
          <h2 >{`${editMode ? 'Edit' : 'Add'} `}User</h2>

          {anyFetching ? (
            <div className="loading-circle">
              <CircularProgress color={'#27367a'} size={96} />
            </div>
          ) : (
            <div>
              <UserForm {...formProps} />
            </div>
          )}
        </Card>
      </div>
    );
  }
}

UserEdit.propTypes = {
  user: PropTypes.shape({}),
  fetchingSuppliers: PropTypes.bool.isRequired,
  editMode: PropTypes.bool.isRequired,
  fetching: PropTypes.bool.isRequired,
  fetchingGroups: PropTypes.bool.isRequired,
  fetchingAccountGroups: PropTypes.bool.isRequired,
  accountGroups: PropTypes.arrayOf(PropTypes.object).isRequired,
  listGroups: PropTypes.func.isRequired,
  listAccountGroups: PropTypes.func.isRequired,
  readAccount: PropTypes.func.isRequired,
  params: PropTypes.shape({}).isRequired,
};

UserEdit.defaultProps = {
  user: null,
};

const mapStateToProps = (state) => {
  const {
    accounts,
    groups,
    routing,
    suppliers,
  } = state;

  return {
    editMode: !routing.locationBeforeTransitions.pathname.includes('/add'),
    user: accounts.read.current || null,
    fetching: accounts.read.fetching,
    fetchingGroups: groups.list.fetching,
    accountGroups: accounts.listGroups.data,
    fetchingAccountGroups: accounts.listGroups.fetching,
    fetchingSuppliers: suppliers.list.fetching,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  ...accountsActions,
  ...groupsActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserEdit);
