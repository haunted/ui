import React, { Component, PropTypes } from 'react';
import TextField from 'material-ui/TextField';
import AutoComplete from '../auto-complete';

const countries = ['Afghanistan', 'Albania', 'Algeria', 'Andorra', 'Angola', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Australia',
  'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin',
  'Bhutan', 'Bolivia', 'Bosnia and Herzegovina', 'Botswana', 'Brazil', 'Brunei', 'Bulgaria', 'Burkina Faso', 'Burundi',
  'Cabo Verde', 'Cambodia', 'Cameroon', 'Canada', 'Central African Republic (CAR)', 'Chad', 'Chile', 'China', 'Colombia',
  'Comoros', 'Democratic Republic of the Congo', 'Republic of the Congo', 'Costa Rica', "Cote d'Ivoire", 'Croatia', 'Cuba',
  'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt',
  'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Fiji', 'Finland', 'France', 'Gabon',
  'Gambia', 'Georgia', 'Germany', 'Ghana', 'Greece', 'Grenada', 'Guatemala', 'Guinea', 'Guinea-Bissau', 'Guyana',
  'Haiti', 'Honduras', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran', 'Iraq', 'Ireland', 'Israel', 'Italy',
  'Jamaica', 'Japan', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Kosovo', 'Kuwait', 'Kyrgyzstan', 'Laos',
  'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macedonia', 'Madagascar',
  'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Mauritania', 'Mauritius', 'Mexico', 'Micronesia',
  'Moldova', 'Monaco', 'Mongolia', 'Montenegro', 'Morocco', 'Mozambique', 'Myanmar (Burma)', 'Namibia', 'Nauru', 'Nepal',
  'Netherlands', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'North Korea', 'Norway', 'Oman', 'Pakistan', 'Palau',
  'Palestine', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Poland', 'Portugal', 'Qatar', 'Romania',
  'Russia', 'Rwanda', 'St. Kitts and Nevis', 'St. Lucia', 'St. Vincent and the Grenadines', 'Samoa', 'San Marino', 'Sao Tome and Principe',
  'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia',
  'South Africa', 'South Korea', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Swaziland', 'Sweden', 'Switzerland', 'Syria',
  'Taiwan', 'Tajikistan', 'Tanzania', 'Thailand', 'Timor-Leste', 'Togo', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey',
  'Turkmenistan', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates (UAE)', 'United Kingdom (UK)', 'United States of America (USA)',
  'Uruguay', 'Uzbekistan', 'Vanuatu', 'Vatican City (Holy See)', 'Venezuela', 'Vietnam', 'Yemen', 'Zambia', 'Zimbabwe',
];
const states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois',
  'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana',
  'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania',
  'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming',
];

class LocationEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: '',
      state: '',
      city: '',
      autoState: '',
      autoCountry: '',
    };
  }

  filterSuggestions(key, value) {
    if (key == 'state') {
      this.setState({
        autoState: value,
      });
    } else if (key == 'country') {
      this.setState({
        autoCountry: value,
      });
    }
  }

  isUSState(state) {
    let s = 0;
    while (s < states.length) {
      if (states[s].toLowerCase() == state.toLowerCase()) {
        return true;
      }
      s++;
    }
    return false;
  }

  isUSA(country) {
    return (
      country.toLowerCase() == 'united states of america (usa)' ||
          country.toLowerCase() == 'united states' ||
          country.toLowerCase() == 'usa' ||
          country.toLowerCase() == 'us'
    );
  }

  onChange(key, value) {
    const location = this.props.value;
    location[key] = value;
    if (key == 'city') {
      this.setState({
        city: value,
      });
    } else if (key == 'state') {
      if (this.isUSA((this.state.country != '' ? this.state.country : this.props.value.country))) {
        if (this.isUSState(value)) {
          this.setState({
            state: value,
          });
        } else {
          this.props.onError('Please enter a valid US state.');
          location[key] = '';
          this.setState({ state: '' });
          this.overrideInput('textfield-state', '');
        }
      } else {
        this.setState({
          state: value,
        });
      }
    } else if (key == 'country') {
      this.setState({
        country: value,
      });
    }
    this.props.onChange(location);
  }

  changeFocus(key) {
    // this.setState({"autoComplete": key});
  }

  overrideInput(id, value) {
    document.getElementById(id).value = value;
  }

  labelText(text, label) {
    if (text.length > 0) {
      return '';
    }
    return label;
  }

  render() {
    let autoCountry = this.state.autoCountry,
      autoState = this.state.autoState;

    const { value = {} } = this.props;
    const { city = '', state = '', country = '' } = value;
    return (
      <div className="text-edit location-editor" >
        <div className="inputs">
          <TextField
            hintText={this.state.city == '' ? this.labelText(city, 'City') : ''}
            defaultValue={(value ? city : '')}
            key={`city.${city}`}
            id={'textfield.city'}
            onBlur={e => this.onChange('city', e.target.value)}
            onChange={(e) => {
              this.setState({
                city: e.target.value,
              });
            }}
            onFocus={(e) => { this.changeFocus('city'); }}
          />

          <TextField
            hintText={this.labelText(state, 'State')}
            defaultValue={state}
            key={`state.${state}`}
            id={'textfield-state'}
            onChange={e => this.filterSuggestions('state', e.target.value)}
            onFocus={(e) => { this.changeFocus('state'); }}
            onBlur={e => this.onChange('state', e.target.value)}
          />

          <TextField
            hintText={this.labelText(country, 'Country')}
            defaultValue={country}
            key={`country.country${country}`}
            id={'textfield-country'}
            onChange={e => this.filterSuggestions('country', e.target.value)}
            onFocus={(e) => { this.changeFocus('country'); }}
            onBlur={e => this.onChange('country', e.target.value)}
          />

        </div>
        <AutoComplete
          searchText={this.state.autoState}
          values={states}
          left={'256px'}
          onComplete={(value) => {
            this.overrideInput('textfield-state', value);
            this.onChange('state', value);
          }}
        />

        <AutoComplete
          searchText={this.state.autoCountry}
          values={countries}
          left={'512px'}
          onComplete={(value) => {
            this.overrideInput('textfield-country', value);
            this.onChange('country', value);
          }}
        />
      </div>
    );
  }
}

LocationEditor.propTypes = {
  value: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onError: PropTypes.func,
};

LocationEditor.defaultProps = {
  value: {
    city: '',
    state: '',
    country: '',
  },
};
// type Location struct {
// 	Country string `protobuf:"bytes,20,opt,name=country,proto3" json:"country,omitempty"`
// 	State   string `protobuf:"bytes,21,opt,name=state,proto3" json:"state,omitempty"`
// 	City    string `protobuf:"bytes,22,opt,name=city,proto3" json:"city,omitempty"`
// }
export default LocationEditor;
