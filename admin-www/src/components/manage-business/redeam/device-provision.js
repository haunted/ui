import React, { Component, PropTypes } from 'react'
import FlatButton from 'material-ui/FlatButton'
import { browserHistory } from 'react-router'
import Dialog from '../../standard/dialog'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'

class Provision extends Component {
    constructor(props) {
        super(props)
        this.state = {
            deviceId: "",
            deviceV: 0,
        }
        this.props.listUnprovisionedDevices()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.provision.version > this.props.provision.version) {
            this.resetForm()
            this.close(true)
        }
    }
    resetForm() {
        this.setState({
            deviceId: "",
            deviceV: 0,
        })
    }
    close(saved) {
        this.resetForm()
        this.props.onUpdated(saved)
    }

    render() {
        const { open, supplierCode, devices, fetching } = this.props
        const { deviceId, deviceV } = this.state

        return <ProvisionModal
            provisioning={fetching}
            open={open}
            devices={devices}
            deviceId={deviceId}
            deviceV={deviceV}
            setDevice={(deviceId, deviceV) => this.setState({ deviceId, deviceV })}
            onSubmit={() => this.props.provisionDevice(deviceId, deviceV, supplierCode)}
            onCancel={() => this.close(false)}
        />
    }
}

const ProvisionModal = ({
    provisioning,
    open,
    deviceId,
    deviceV,
    devices = [],
    setDevice = () => { },
    onSubmit = () => { },
    onCancel = () => { }
}) => (
        <Dialog
            fetching={provisioning}
            title={`Provision Device`}
            actions={[
                <FlatButton
                    label="Cancel"
                    primary={false}
                    onClick={e => onCancel()}
                />,
                <FlatButton
                    label={'Provision'}
                    primary={true}
                    onClick={e => onSubmit()}
                />
            ]}
            modal={true}
            open={open}
            autoScrollBodyContent={true}
        >
            <SelectField
                floatingLabelFixed={true}
                floatingLabelText="Device"
                value={deviceId+":"+deviceV}
                onChange={(e, i, v) => setDevice(...v.split(":"))}>
                {
                    devices.map(
                        v => <MenuItem
                            value={v.id+":"+v.version}
                            label={v.assetId}
                            primaryText={v.assetId}
                        />
                    )
                }
            </SelectField>
        </Dialog>
    )


import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as deviceActions from '../../../ax-redux/actions/devices'

export default connect(
    (state, ownProps) => {
        return {
            fetching: state.devices.listUnprovisioned.fetching || state.devices.provision.fetching,
            devices: state.devices.listUnprovisioned.data,
            provision: state.devices.provision,
        }
    },
    dispatch => bindActionCreators(deviceActions, dispatch),
)(Provision)