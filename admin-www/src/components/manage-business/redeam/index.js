import React, { Component, PropTypes } from 'react'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import Groups from './groups'
// For Redeam Technical Staff, Redeam Business Staff
//
// Redeam Management
// This section will enable Redeam team members to view, add, edit and disable any actor in the Redeam system.
// By default, the view should be an expandable sub-tree on the left-hand navigation with the following items:
//
// - Suppliers
// - Resellers
// - Redeam Internal


const tabStyle = {
    color: "black"
};

class RedeamManagement extends Component {

    constructor(props) {
        super(props)
    }

    render () {
        
        return (

            <div className="redeam-management-index">
            { !!this.props.children ? this.props.children : <Groups /> }
            </div>

        )
    }
    
}

import { connect } from 'react-redux'
export default connect(
    state => {
        return {
            role: state.app.role,
            app: state.app,
        }
    },
    dispatch => {
        return {

        }
    }
)(RedeamManagement)
