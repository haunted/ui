import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'

export default class VoucherStatusPopover extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      label: (this.props.defaultLabel != null ? this.props.defaultLabel : "Select Status")
    }
  }

  componentWillMount() {

  }

  handleTouchTap(event) {
    event.preventDefault()
    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
    console.log(event.currentTarget);
  }

  handleRequestClose() {
    this.setState({
      open: false,
    })
  }

  onMenuItem(e, item, i) {
    this.props.onStatus(item.props.value)
    this.setState({
      open: false,
      label: item.props.primaryText,
    })
  }

  render() {
    return (
      <div className="supplier-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={() => this.handleRequestClose()}
        >
          <Menu onItemTouchTap={(e, item, i) => this.onMenuItem(e, item, i)}>
            {
                this.props.statuses.map((v, i) => <MenuItem key={i} primaryText={v.name} value={v.status} />)
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}
VoucherStatusPopover.propTypes = {
  onStatus: PropTypes.func.isRequired,
  defaultLabel: PropTypes.string
}
VoucherStatusPopover.defaultProps = {
    statuses: [
        {name: "Unknown", status: "STATUS_UNKNOWN"},
        {name: "New", status: "STATUS_NEW"},
        {name: "Pending", status: "STATUS_PENDING"},
        {name: "Flagged", status: "STATUS_FLAGGED"}
    ]
}
