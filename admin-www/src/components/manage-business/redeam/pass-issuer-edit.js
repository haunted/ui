import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import Page from '../../standard/page'
import { ActionFooter, ActionButton } from '../../standard/actions'
import RaisedButton from 'material-ui/RaisedButton'
import PassIssuerForm, { validateForSubmit } from './pass-issuer-form'

class PassIssuerEdit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            passIssuer: {},
            errors: {},
        }
        this.fetch()
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.get.version != this.props.get.version) {
            this.setState({ passIssuer: nextProps.get.data.passIssuer.passIssuer })
        }
    }

    fetch() {
        this.props.getPassIssuer(this.props.params.id)
    }

    onCancel() {
        browserHistory.push("/app/redeam/pass-issuers");
    }

    onUpdate() {
        const {passIssuer, errors} = validateForSubmit(this.state.passIssuer)
        if (Object.keys(errors).reduce((s, k) => s || !!errors[k], false)) {
            this.setState({ errors })
            return
        }
        this.props.updatePassIssuer(this.state.passIssuer.id, passIssuer)
    }

    onChange(passIssuer) {
        this.setState({
            errors: {},
            passIssuer,
        })
    }

    onDisable() {
        if (confirm("Are you sure you want to disable this pass issuer?")) {
            this.props.disablePassIssuer(this.state.passIssuer.id, this.state.passIssuer.version)
        }
    }

    render() {
        let fetching = this.props.get.fetching || this.props.update.fetching || this.props.disable.fetching
        let error = this.props.update.error ? this.props.update.error.data.Error : false

        return (
            <Page title="Edit Pass Issuer" error={error} fetching={fetching}>
                <PassIssuerForm
                    passIssuer={this.state.passIssuer}
                    onChange={passIssuer => this.onChange(passIssuer)}
                    error={error}
                    fieldErrors={this.state.errors}
                    isEdit={true}
                    />
                <ActionFooter>
                    <ActionButton
                        label="Cancel"
                        onClick={() => this.onCancel()}
                        />
                    <ActionButton
                        disabled={this.state.passIssuer.status == 'STATUS_DISABLED'}
                        label="Deactivate"
                        secondary={true}
                        onClick={() => this.onDisable()}
                        />
                    <ActionButton
                        label="Update"
                        primary={true}
                        onClick={() => this.onUpdate()}
                        />
                </ActionFooter>
            </Page>
        )
    }
}

import { connect } from 'react-redux'
import { getPassIssuer, updatePassIssuer, disablePassIssuer } from '../../../ax-redux/actions/pass-issuers'

export default connect(
    state => {
        return {
            get: state.passIssuers.get,
            update: state.passIssuers.update,
            disable: state.passIssuers.disable,
        }
    },
    dispatch => ({
        getPassIssuer: id => dispatch(getPassIssuer(id)),
        updatePassIssuer: (id, issuer) => dispatch(updatePassIssuer(id, issuer)),
        disablePassIssuer: (id, version) => dispatch(disablePassIssuer(id, version)),
    })
)(PassIssuerEdit)