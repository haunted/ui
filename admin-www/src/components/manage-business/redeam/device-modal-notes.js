import React, { Component, PropTypes } from 'react'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from '../../standard/dialog'

class DeviceModalNotes extends Component {
    constructor(props) {
        super(props)
    }

    componentWillReceiveProps(nextProps) {
        const { deviceId, deviceV } = this.props

        if (nextProps.open && !nextProps.fetching && (
            deviceId != nextProps.device.id || nextProps.deviceV != nextProps.device.version 
        )) {
            this.props.getDevice(nextProps.deviceId)
        }
    }

    setError(err) {
        this.setState({ error: err })
    }

    close() {
        this.props.onUpdated(true)
    }

    render() {
        const { device, fetching, open, deviceId, deviceV } = this.props

        const actions = [
            <FlatButton
                label="Close"
                primary={false}
                onClick={e => this.close()}
            />,
        ];
        return (
            <Dialog
                fetching={fetching}
                title={`${!!device.assetId ? device.assetId : ""} Notes`}
                actions={actions}
                modal={true}
                open={this.props.open}
                autoScrollBodyContent={true}
            >
                <Notes
                    onAdd={message => this.props.appendNote(deviceId, deviceV, message)}
                    onRemove={id => this.props.deleteNote(deviceId, deviceV, id)}
                    notes={device.notes}
                 />
            </Dialog> 

        )
    }
}

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as deviceActions from '../../../ax-redux/actions/devices'

export default connect(
    (state, ownProps) => ({
        device: state.devices.get.data,
        fetching: state.devices.get.fetching || state.devices.appendNote.fetching || state.devices.deleteNote.fetching,
    }),
    dispatch => bindActionCreators(deviceActions, dispatch),
)(DeviceModalNotes)


import { Label } from '../../standard/form-fields'
import Paper from 'material-ui/Paper'
import RemoveIcon from 'material-ui/svg-icons/action/delete';
import IconButton from 'material-ui/IconButton'

class Notes extends Component {
    constructor(props) {
        super(props)
        this.state = {
            adding: false,
            addingText: "",
        }
    }
    onAdd() {
        this.setState({
            adding: true,
            addingText: "",
        })
    }
    onAddSave() {
        const { onAdd } = this.props

        onAdd(this.state.addingText)
        this.setState({
            adding: false,
            addingText: "",
        })
    }
    onAddCancel() {
        this.setState({
            adding: false,
            addingText: "",
        })
    }
    onRemove({ id }) {
        const { onRemove } = this.props
        onRemove(id)
    }
    render() {
        const { notes=[] } = this.props
        return <div style={{ display: 'flex' }}>
            <div>
                <Label>Notes</Label>
                {
                    // Display Notes
                    notes.map((v, k) => (
                        <Paper key={"a" + k} style={{ position: 'relative', width: '600px', padding: '1rem 2rem 1rem 1rem', margin: '0.5rem 0 1rem 0' }}>
                            <div style={{ whiteSpace: 'pre-wrap' }}>{v.message}</div>
                            <IconButton
                                style={{ position: 'absolute', top: '0rem', right: '0rem' }}
                                onClick={e => this.onRemove(v)}
                            >
                                <RemoveIcon />
                            </IconButton>
                        </Paper>
                    ))
                }
                <div style={{ alignSelf: "flex-end" }}>
                    {
                        // Adding Note
                        !this.state.adding
                            ?
                            <RaisedButton
                                label="Add"
                                onClick={e => this.onAdd()}
                            />
                            :
                            <div>
                                <TextField
                                    value={this.state.addingText}
                                    onChange={(e, v) => this.setState({ addingText: v })}
                                    multiLine={true}
                                />
                                <div>
                                    <RaisedButton
                                        label="Cancel"
                                        onClick={e => this.onAddCancel()}
                                    />
                                    <RaisedButton
                                        primary={true}
                                        label="Save Note"
                                        onClick={e => this.onAddSave()}
                                    />
                                </div>
                            </div>
                    }
                </div>
            </div>
        </div>
    }
}
