import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import moment from 'moment-timezone'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import Toggle from 'material-ui/Toggle'
import DatePicker from 'material-ui/DatePicker'
import RaisedButton from 'material-ui/RaisedButton'
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from '../../../reports/export-button'
import PrintButton from '../../../reports/print-button'
import SuppliersPopover from '../../../reports/suppliers-popover'
import { formatEnum } from '../../../../util.js'

import DateRangePicker, {DateRangeTitle} from '../../../datepicker'


const styles = {
  masterSummaryButton: {
    marginLeft: "1em",
    minWidth: '220px'
  },
  pagination: {
    width: '100%',
    marginTop: '1em',
    marginBottom: '1em'
  },
  narrow: {
    width: '11%'
  },
  wide: {
    width: '56%',
    whiteSpace: 'pre-wrap'
  },
  tall: {
    paddingTop: '0.5em',
    paddingBottom: '0.5em'
  }
}

class FailedOrders extends Component {

  constructor(props) {
    super(props)
    props.contextFilters(props.params, props.location)
  }

  componentDidUpdate(prevProps, prevState) {
    const summaryMode = (this.props.children == null || this.props.children.length < 1)
    if (summaryMode) {
      if (this.props.version != prevProps.version) {
        this.fetch()
      }
    }
  }
  fetch() {
    const { startDate, endDate, sort, limit, offset } = this.props
    
    const url = `/app/redeam/failed-orders?start=${
      startDate.getUnixTime()
    }&end=${
      endDate.getUnixTime()
    }&sort=${
      sort
    }&limit=${
      limit
    }&offset=${
      offset
    }`
    
    browserHistory.replace( url )
    
    this.props.listFailedOrders({
      start: startDate.getUnixTime(),
      end: endDate.getUnixTime(),
      sort,
      limit,
      offset
    })

    this.props.setReturnToFailedOrdersURL(url)
  }

  export(format) {
    const { startDate, endDate } = this.props

    this.props.export({
      start: startDate.getUnixTime(),
      end: endDate.getUnixTime(),
      endpoint: "/api/orders/failed"
    }, format)

  }

  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode()
  }

  viewReport ( type, supplierCode, resellerCode ) {
      browserHistory.push(`/app/redeam/failed-orders/detail${
        (supplierCode !="" ? "/"+supplierCode :"")
      }${
        (resellerCode !="" ? "/"+resellerCode :"")
      }`)
      
  }

  render() {
      let results = this.props.failedOrders,
          fetching = this.props.failedOrdersFetching,
          summaryMode = (this.props.children == null || this.props.children.length < 1)

    return (
      <div className="shift-summary">
        <div className="exceptions-header no-print" style={{display: (!summaryMode ? "none" : "inherit")}}>
          <div className="exceptions-header-left">
            <h2 style={{color:"#434343"}} className="no-print">
                Failed Orders
                <DateRangeTitle
                  start={this.props.startDate}
                  end={this.props.endDate}
                />
            </h2>
          </div>
        <div className="exceptions-header-right">
            <div className={"no-print report-controls"+ (this.props.printMode?" print-mode" : "")}>
              <DateRangePicker
                onStartDate={d => this.props.setStartDate(d)}
                onEndDate={d => this.props.setEndDate(d)}
                start={this.props.startDate}
                end={this.props.endDate}
              />
          </div>
          <PrintButton
             style={{marginLeft: "1em", marginRight: "1em"}}
             onPrint={data=> { this.print() }} printMode={this.props.printMode}
          />
        </div>
      </div>
      <div className="report-details">
          {this.props.children}
      </div>
         {
           fetching ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
           (this.props.children == null || this.props.children.length < 1 ?
            <Table className="index" selectable={false} key="2">
               <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                 <TableRow>
                   <TableHeaderColumn className="sortable" style={ styles.narrow }>
                     <div className="inner" onClick={e => this.props.setSort("count")}>Count</div>
                   </TableHeaderColumn>
                   <TableHeaderColumn className="sortable" style={ styles.narrow }>
                     <div className="inner" onClick={e => this.props.setSort("typeOf")}>Type</div>
                   </TableHeaderColumn>
                   <TableHeaderColumn className="sortable" style={ styles.narrow }>
                     <div className="inner" onClick={e => this.props.setSort("resellerCode")}>Supplier Code</div>
                   </TableHeaderColumn>
                   <TableHeaderColumn className="sortable" style={ styles.narrow }>
                     <div className="inner" onClick={e => this.props.setSort("supplierCode")}>Reseller Code</div>
                   </TableHeaderColumn>
                   <TableHeaderColumn className="sortable" style={ styles.wide }>
                     <div className="inner" onClick={e => this.props.setSort("reason")}>Reason</div>
                   </TableHeaderColumn>
                 </TableRow>
               </TableHeader>
               <TableBody displayRowCheckbox={false}>
                 {!!results ? results.map((result, i) => {

                  let reason = result.reason.replace('["', '').replace('"]', ''),
                      reasonStyle = reason.length > 80 ? Object.assign({}, styles.wide, styles.tall) : styles.wide,
                      type = formatEnum(result.typeOf)

                    return (
                      <TableRow key={i} onTouchTap={ e=> { this.viewReport( result.typeOf, result.supplierCode, result.resellerCode ) }}>
                        <TableRowColumn title={result.count} style={ styles.narrow }>{result.count}</TableRowColumn>
                        <TableRowColumn title={type} style={ styles.narrow }>{type}</TableRowColumn>
                        <TableRowColumn title={result.supplierCode} style={ styles.narrow }>{result.supplierCode}</TableRowColumn>
                        <TableRowColumn title={result.resellerCode} style={ styles.narrow }>{result.resellerCode}</TableRowColumn>
                        <TableRowColumn title={reason} style={ reasonStyle }>{reason}</TableRowColumn>
                      </TableRow>
                    )

                  }
                   
                 ) : ""}
               </TableBody>
            </Table> 
          : "" )
        }
        { summaryMode ? (
          <div style={ styles.pagination } >
          {this.props.offset > 0 ?
            <RaisedButton label="Previous Page" 
                          onClick={(e)=>{
                            this.props.prevPage()
                          }}
                          style={{ marginLeft: "1em" }}
            /> : ""
          }
          <div style={{display: "inline-block", marginRight: "1em"}}></div>
            <RaisedButton label="Next Page" onClick={(e)=>{
              this.props.nextPage()
            }}/>
          </div>
        ) : ""}
      </div>
    )
  }
}


import { connect } from 'react-redux'
import { setPageTitle, togglePrintMode} from '../../../../ax-redux/actions/app'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../../../ax-redux/actions/report-filters'

import { listFailedOrders } from '../../../../ax-redux/actions/failed-orders'
import { setReturnToFailedOrdersURL } from '../../../../ax-redux/actions/navigation'

import { exportCSV, exportXLSX } from '../../../../ax-redux/actions/finances'

export default connect(
  state => ({
      failedOrders: state.failedOrders.listFailedOrders.data ? state.failedOrders.listFailedOrders.data.results : false,
      failedOrdersFetching: state.failedOrders.listFailedOrders.fetching,      
      printMode: state.app.printMode,      
    ...state.reportFilters,
  }),
  dispatch => ({
      listFailedOrders: (params) => {
        dispatch(listFailedOrders(params))
      },
      setReturnToFailedOrdersURL: (params) => {
          dispatch(setReturnToFailedOrdersURL(params))
      },
      setPageTitle: title => {
          dispatch(setPageTitle(title))
      },
      togglePrintMode: (opts) => {
          dispatch(togglePrintMode(opts))
      },
      export: (opts, format) => {
        switch (format) {
          case "CSV":
            dispatch(exportCSV(opts))
            return
          case "XLSX":
            dispatch(exportXLSX(opts))
            return
        }
      },
      ...bindActionCreators(filterActions, dispatch),
  })
)(FailedOrders);
