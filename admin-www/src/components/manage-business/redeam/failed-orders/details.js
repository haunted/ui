import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import Card, { CardTitle, CardText, CardActions } from 'material-ui/Card'

import Checkbox from 'material-ui/Checkbox'
import CircularProgress from 'material-ui/CircularProgress'
import PrintButton from '../../../reports/print-button'
import RaisedButton from 'material-ui/RaisedButton'
import RedeamTable from '../../../standard/table'

let styles = {
  tableRow: v => {
    let style = {};
    if (isException(v.voucherStatus)) {
      style.background = "rgba(255, 0, 0, 0.5)";
    }
    if (isNew(v.voucherStatus)) {
      style.background = "rgba(0, 255, 0, 0.25)";
    }
    if (v.ticketId != null && v.ticketId != "") {
      style.cursor = "initial";
    }
    return style;
  },
  icon: v => {
    return {
      opacity: 0.8,
      position: "relative",
      top: "4px",
      left: "6px",
      marginRight: "8px"
    }
  },
  addVoucherButton: {
    minWidth: '190px'
  },
  addVoucherButtonContainer: {
    display: 'inline-block',
    margin: '1em',
    marginLeft: '3em',
    width: '100%'
  },
  h4: {
    marginLeft: '1em'
  },
  checkbox: {
    maxWidth: '48px',
    display: 'inline-block',
    verticalAlign: 'bottom'
  },
  card: {
    marginTop: '0.5em',
    marginBottom: '1.5em',
    marginLeft: '1em',
    marginRight: '1em'
  },
  narrow: {
    width: '100px'
  }
}

let isException = status => status == "Exception" || status == "Rejected",
  isNew = status => status == "New";

class FailedOrdersDetail extends Component {
  constructor(props) {
    super(props)

    this.state = { 
      selected: {},
    }
    props.contextFilters(props.params, props.location)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.version != prevProps.version) {
      this.fetch()
    }
  }

  fetch() {
    let { resellerCode, supplierCode, limit } = this.props

    browserHistory.push(
      `/app/redeam/failed-orders/detail${
        !! supplierCode ? `/${supplierCode}` : ""
      }${
        !! resellerCode ? `/${resellerCode}` : ""
      }?limit=${
        limit
      }`
    );

    this.props.fetchReport({
      supplierCode,
      resellerCode,
      limit,
    })    
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.reprocessing && nextProps.reprocessing == false) {
      this.returnToSummary()
    }
  }

  reprocessFailedOrders() {
    this.props.reprocess(Object.keys(this.state.selected).join(','))
  }

  toggleOrder(id) {
    const { selected } = this.state

    if (!!selected[id]) {
      selected[id] = false
    } else {
      selected[id] = true
    }

    this.setState({
      selected
    })
  }

  print() {
    if (!this.props.printMode) {
      setTimeout(() => {
        window.print()
      }, 500)
    }

    this.props.togglePrintMode()
  }

  returnToSummary() {
    browserHistory.push(this.props.returnToFailedOrdersURL)
  }

  renderProducts(products=[]) {
    return (
      <RedeamTable
        columns={[
          { title: 'Title' },
          { title: 'Description' },
          { title: 'Reference' },
          { title: 'Supplier Code' },
          { title: 'Reseller Code' },
          { title: 'Language Code' },
        ]}
        data={products}
        rows={[
          v => v.title,
          v => v.desc,
          v => v.ref,
          v => v.supplierCode,
          v => v.resellerCode,
          v => v.languageCode,
        ]}
      />
    )
  }

  renderOrderDetails(v) {

    let supplier = '',
      reseller = ''

    if (v.ticket) {

      supplier = v.supplier ? v.supplier.name + `${v.supplier.name} (${v.ticket.supplierCode})` : `(${v.ticket.supplierCode})`
      reseller = v.ticket && v.reseller ? `${v.reseller.name} (${v.ticket.resellerCode})` : `(${v.ticket.resellerCode})`

    }

    return (
      <div>
        <Table className="index detail" selectable={false}>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn className="sortable">
                <div className="inner">Property</div>
              </TableHeaderColumn>
              <TableHeaderColumn className="sortable">
                <div className="inner">Value</div>
              </TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            <TableRow>
              <TableRowColumn>Supplier</TableRowColumn>
              <TableRowColumn>{supplier}</TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn>Reseller</TableRowColumn>
              <TableRowColumn>{reseller}</TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn>Type</TableRowColumn>
              <TableRowColumn>{v.type}</TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn>Status</TableRowColumn>
              <TableRowColumn>{v.status}</TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn>Reseller Channel Id</TableRowColumn>
              <TableRowColumn>{v.resellerChannelId}</TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn>Failed Reasons</TableRowColumn>
              <TableRowColumn style={{ whiteSpace: 'pre-wrap' }}>{v.failedReason.join("\n")}</TableRowColumn>
            </TableRow>
            <TableRow>
              <TableRowColumn>Skip Authority</TableRowColumn>
              <TableRowColumn>{v.skipAuthority ? "Yes" : "No"}</TableRowColumn>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    )
  }

  renderDetails(fetching, report) {

    if (fetching) return (<CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} />)

    return (
      <div>
        {
          report && report.map((order, i) => {

            return (
              <Card key={i} style={styles.card}>
                <CardTitle>
                  <span style={styles.checkbox}>
                    <Checkbox checked={!!this.state.selected[order.id]}
                      onCheck={(e, v) => this.toggleOrder(order.id)}
                    />
                  </span>
                  Order {order.id}
                </CardTitle>
                {this.renderOrderDetails(order)}
                {!!order.ticket && !!order.ticket.products && this.renderProducts(order.ticket.products)}
              </Card>
            )

          })
        }
      </div>
    )
  }

  render() {

    let report = this.props.report,
      fetching = this.props.fetchingReport

    return (
      <div className="shifts">
        <div className="exceptions-header">
          <div className="exceptions-header-left">
            <h2 style={{ color: "#434343" }}>
              Failed Orders Detail
            </h2>
          </div>
          <div className="exceptions-header-right">
            <div className={"no-print report-controls" + (this.props.printMode ? " print-mode" : "")}>
              <RaisedButton onTouchTap={e => { this.reprocessFailedOrders() }}
                className="no-print"
                style={{ minWidth: "190px", float: 'left', 'marginLeft': '1em' }}
                label="Reprocess Selected"
              />
              <RaisedButton onTouchTap={e => { this.returnToSummary() }}
                className="no-print"
                style={{ minWidth: "190px", float: 'left', 'marginLeft': '1em' }}
                label="Return to Summary"
              />
            </div>
            <PrintButton
              className="no-print"
              onPrint={data => { this.print() }} printMode={this.props.printMode}
            />
          </div>
        </div>
        {this.renderDetails(fetching, report)}
      </div>
    )
  }
}

import { connect } from 'react-redux'
import { togglePrintMode } from '../../../../ax-redux/actions/app'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../../../ax-redux/actions/report-filters'


import {
  listFailedOrderDetails,
  reprocessFailedOrders,
} from '../../../../ax-redux/actions/failed-orders'

export default connect(
  state => ({
    report: state.failedOrders.listFailedOrdersDetail.data ? state.failedOrders.listFailedOrdersDetail.data.orders : false,
    fetchingReport: state.failedOrders.listFailedOrdersDetail.fetching,
    reprocessing: state.failedOrders.reprocessFailedOrders.fetching,
    printMode: state.app.printMode,
    returnToFailedOrdersURL: state.navigation.returnToFailedOrdersURL,
    ...state.reportFilters,    
  }),
  dispatch => ({
    fetchReport: (params) => {
      dispatch(listFailedOrderDetails(params))
    },
    reprocess: (params) => {
      dispatch(reprocessFailedOrders(params))
    },
    togglePrintMode: (opts) => {
      dispatch(togglePrintMode(opts))
    },
    ...bindActionCreators(filterActions, dispatch),
  })
)(FailedOrdersDetail);
