import React, { Component, PropTypes } from 'react'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import Dialog from '../../standard/dialog'

class Deprovision extends Component {
    constructor(props) {
        super(props)
        this.state = {
            reason: "",
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.deprovision.version > this.props.deprovision.version) {
            this.resetForm()
            this.close(true)
        }
    }
    resetForm() {
        this.setState({
            reason: "",
        })
    }
    close(saved) {
        this.resetForm()
        this.props.onUpdated(saved)
    }
    render() {
        const { deviceId, deviceV, onUpdated, open } = this.props
        const { reason } = this.state
        const assetId = this.props.devicesById[deviceId] ? this.props.devicesById[deviceId].assetId : ""

        return <DeprovisionModal
            open={open}
            assetId={assetId}
            deviceId={deviceId}
            deviceV={deviceV}
            deprovisioning={this.props.deprovision.fetching}
            onCancel={() => this.close(false)}
            onSubmit={() => this.props.deprovisionDevice(deviceId, deviceV)}
        />
    }
}

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as deviceActions from '../../../ax-redux/actions/devices'

export default connect(
    (state, ownProps) => {
        return {
            deprovision: state.devices.deprovision,
            devicesById: state.devices.list.devicesById,
        }
    },
    dispatch => bindActionCreators(deviceActions, dispatch),
)(Deprovision)


const DeprovisionModal = ({
    deprovisioning,
    open,
    assetId,
    onSubmit = () => { },
    onCancel = () => { }
}) => (
        <Dialog
            fetching={deprovisioning}
            title={`Deprovision ${assetId}`}
            actions={[
                <FlatButton
                    label="Cancel"
                    primary={false}
                    onClick={e => onCancel()}
                />,
                <FlatButton
                    label={'Deprovision Device'}
                    primary={true}
                    onClick={e => onSubmit()}
                />
            ]}
            modal={true}
            open={open}
            autoScrollBodyContent={true}
        >
            Are you sure you'd like to deprovision this device?
        </Dialog>
    )