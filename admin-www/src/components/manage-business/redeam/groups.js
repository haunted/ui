import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import CircularProgress from 'material-ui/CircularProgress';
import {Card, CardActions, CardHeader, CardText, CardMedia} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import { browserHistory } from 'react-router'

let styles = {
  title: {
    color: 'rgb(67, 67, 67)',
    display: 'inline-block',
    marginLeft: '1em',
    marginRight: '1em'
  },
  addNew: {
    float: 'right',
    marginTop: '1em',
    marginRight: '1em'
  }
}
class Groups extends Component {
    constructor(props) {
      super(props)
      let sort = "";
      if (this.props.location && this.props.location.query) {
        if (this.props.location.query.sort) {
            sort = this.props.location.query.sort;
        }
      }
      this.state = {
        selected: false,
        sort: sort
      }
    }
    componentWillMount() {
      this.props.listGroups({
        sort: this.state.sort
      })
    }
    componentWillUpdate(nextProps, nextState) {
      let requestParams = {};
      if (this.props.pathname != nextProps.pathname ||
          this.props.queryParams != nextProps.queryParams) {
        if ( nextProps.location && nextProps.location.query ) {
          if (nextProps.location.query.sort) {
              requestParams.sort = nextProps.location.query.sort;
          }
        }
        console.log("request params", requestParams)
        this.props.listGroups(requestParams);
      }
    }
    onRowClick(keys) {
      if (keys.length == 0)
        return
      let group = this.props.groups.list.data[keys[0]]
      browserHistory.push("/app/redeam/groups/"+group.id);
    }

    addGroup () {
        browserHistory.push("/app/redeam/groups/add");
    }
    sortBy(column) {
      let sort = column;
      if (sort == this.state.sort) {
          if (sort.indexOf("-") > -1) {
            sort = sort.replace("-", "");
          } else {
            sort = "-"+sort;
          }
      }
      this.setState({
          sort: sort
      });
      browserHistory.push(`/app/redeam/groups?sort=${sort}`);
    }

    renderRows(rows) {
            return rows.map((group, k) => (
              <TableRow key={k}>
                <TableRowColumn>{group.name}</TableRowColumn>
              </TableRow>
            ))
    }

    render() {
        let groups = this.props.groups.list.data,
            fetching = this.props.fetching,
            error = this.props.error;

        return (
          <div className="user-management">
            <h2 style={styles.title}>Groups</h2>
            <RaisedButton onClick={e=>{ this.addGroup() }}
                          style={styles.addNew}
                          label="Add New Group"
                          primary={true}
            />
            <Card className="group-management">
                    <CardText expandable={false}>
                        <Table onRowSelection={keys => this.onRowClick(keys)} className="groups index">
                            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                                <TableRow>
                                  <TableHeaderColumn className="sortable">
                                      <div className="inner" onClick={ e => this.sortBy("name") }>Name</div>
                                  </TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                        <TableBody displayRowCheckbox={false}>
                            { !fetching && this.renderRows(groups) }
                        </TableBody>
                    </Table>
                </CardText>
                {(!fetching && (!groups || groups.length == 0)) ?
                    (!!error && error.length > 0 ?
                        <div className="table-error">Network Error</div> :
                        <div className="table-error">No Records To Display</div>
                    )
                : ""}
                {(this.props.groups.list.fetching ?
                    <div className="loading-circle">
                      <CircularProgress color={"#27367a"} size={96} />
                    </div> : ""
                )}
            </Card>
          </div>
        )
    }
}

import { connect } from 'react-redux'
import {
  listAccounts,
  listAccountPermissions,
}  from '../../../ax-redux/actions/groups'

import {
  listGroups,
  listAccountsByGroup,
  listGroupPermissions,
}  from '../../../ax-redux/actions/groups'

const mapStateToProps = (state) => {
  return {
    groups: state.groups,
    fetching: state.groups.list.fetching,
    error: state.groups.list.error,
    pathname: state.routing.locationBeforeTransitions.pathname,
    queryParams: state.routing.locationBeforeTransitions.search
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    listAccountPermissions: id => {
      dispatch(listAccountPermissions(id))
    },
    listGroups: (params) => {
      dispatch(listGroups(params))
    },
    listAccountsByGroup: id => {
      dispatch(listGroupPermissions(id))
    },
    listGroupPermissions: id => {
      dispatch(listGroupPermissions(id))
    },
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Groups);
