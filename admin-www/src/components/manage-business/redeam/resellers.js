import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import { browserHistory } from 'react-router'

let styles = {
  title: {
    color: 'rgb(67, 67, 67)',
    display: 'inline-block',
    marginLeft: '1em',
    marginRight: '1em'
  },
  addNew: {
    float: 'right',
    marginTop: '1em',
    marginRight: '1em'
  },
  narrowCol: {
    width: '8%'
  },
  mediumCol: {
    width: '16%'
  }
}
class Resellers extends Component {
  constructor(props) {
    super(props)
    this.state = {
        sort: "name"
    }
  }
  componentWillMount() {
    this.props.listResellers({
      sort: this.state.sort
    })
  }
  componentWillUpdate(nextProps, nextState) {
    let requestParams = {};
    if (this.props.pathname != nextProps.pathname ||
        this.props.queryParams != nextProps.queryParams) {
      if (nextProps.location.query) {
        if (nextProps.location.query.sort) {
            requestParams.sort = nextProps.location.query.sort;
        }
      }
      this.props.listResellers(requestParams);
    }
  }

  addReseller() {
    browserHistory.push("/app/redeam/resellers/add");
  }
  editReseller (id) {
      browserHistory.push("/app/redeam/resellers/"+id);
  }
  editReseller(index) {
     let id = this.props.resellers.resellers[index].id
     browserHistory.push("/app/redeam/resellers/"+id);
  }

  sortBy(column) {
    let sort = column;
    if (sort == this.state.sort) {
        if (sort.indexOf("-") > -1) {
          sort = sort.replace("-", "");
        } else {
          sort = "-"+sort;
        }
    }
    this.setState({
        sort: sort
    });
    browserHistory.push(`/app/redeam/resellers?sort=${sort}`);
  }

  renderRows(data, fetching) {

    if (fetching || (!data || !data.resellers))
      
      return "";

    return data.resellers.map((reseller, s) => (
      <TableRow key={s}>
        <TableRowColumn style={styles.mediumCol} key={s+".1"}>{ `${reseller.name} (${reseller.code}) `}</TableRowColumn>
        <TableRowColumn style={styles.mediumCol} key={s+".2"}>{reseller.mainLocation && reseller.mainLocation.city ? reseller.mainLocation.city : ""}</TableRowColumn>
        <TableRowColumn style={styles.narrowCol} key={s+".3"}></TableRowColumn>
        <TableRowColumn style={styles.narrowCol} key={s+".3"}></TableRowColumn>
        <TableRowColumn style={styles.mediumCol} key={s+".4"}></TableRowColumn>
        <TableRowColumn style={styles.mediumCol} key={s+".5"}></TableRowColumn>
      </TableRow>
    ))

  }

  render() {
      let fetching = this.props.fetching,
          data = this.props.resellers;
    return (
      <div className="user-management">
        <h2 style={styles.title}>Resellers</h2>
        <RaisedButton onClick={e=>{ this.addReseller() }}
                      style={styles.addNew}
                      label="Add New Reseller"
                      primary={true}
        />
          <Card className="redeam-reseller-management">
            <Table className="resellers index" onRowSelection={index => this.editReseller(index)}>
              <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                  <TableHeaderColumn className="sortable" style={styles.mediumCol}>
                      <div className="inner" onClick={ e => this.sortBy("name") }>Name</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable" style={styles.mediumCol}>
                      <div className="inner" onClick={ e => this.sortBy("location") }>Location</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn style={styles.narrowCol}>Number of Redemption Tablets</TableHeaderColumn>
                  <TableHeaderColumn style={styles.narrowCol}>Number of Products</TableHeaderColumn>
                  <TableHeaderColumn style={styles.mediumCol}>Total Lifetime Ticket Volume in # and $</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                    {this.renderRows(data, fetching)}
              </TableBody>
            </Table>
            {(fetching ?
                <div className="loading-circle">
                  <CircularProgress color={"#27367a"} size={96} />
                </div> : ""
            )}
           </Card>
        </div>
    )
  }
}

import { connect } from 'react-redux'
import { listResellers } from '../../../ax-redux/actions/resellers'

export default connect(
  state => {
    return {
      resellers: state.resellers.list.data,
      fetching: state.resellers.list.fetching,
      error: state.resellers.list.error,
      pathname: state.routing.locationBeforeTransitions.pathname,
      queryParams: state.routing.locationBeforeTransitions.search
    }
  },
  dispatch => {
    return {
      listResellers: (params) => {
        dispatch(listResellers(params))
      },
    }
  }
)(Resellers)
