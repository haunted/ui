import React, { Component, PropTypes } from 'react'
import FlatButton from 'material-ui/FlatButton'
import Dialog from '../../standard/dialog'
import DeviceForm from './device-form'

class DeviceModalUpdate extends Component {
    constructor(props) {
        super(props)

        this.state = {
            device: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open && !this.props.open) {
            this.resetForm()
            this.props.getDevice(nextProps.deviceId)
            this.props.getNextAssetId()
        }

        if (nextProps.device.version != this.props.device.version) {
            this.resetForm(nextProps.device.data)
        }

        if (nextProps.savedDevice.version != this.props.savedDevice.version) {
            if (!!!nextProps.savedDevice.error) {
                this.close(true)
            } else {
                this.setError(nextProps.savedDevice.error)
            }
        }
    }

    resetForm(device = {}) {
        this.setState({ device, error: false })
    }
    setError(err) {
        this.setState({ error: err })
    }

    close(saved) {
        this.setState({ device: {} })
        this.props.onUpdated(saved)
    }

    save() {
        this.props.updateDevice(this.state.device)
    }

    render() {
        const { deviceId, nextAssetId } = this.props
        const { device } = this.state

        const actions = [
            <FlatButton
                label="Cancel"
                primary={false}
                onClick={e => this.close(false)}
            />,
            <FlatButton
                label={'Update Device Information'}
                primary={true}
                onClick={e => this.save()}
            />
        ];
        return (
            <Dialog
                fetching={this.props.fetching}
                title={`Update Device ${!!device.assetId ? device.assetId : ""}`}
                actions={actions}
                modal={true}
                open={this.props.open}
                autoScrollBodyContent={true}
            >
                <DeviceForm
                    device={device}
                    nextAssetId={nextAssetId}
                    onChange={v => this.setState({ device: v })}
                    updateMode={true}
                />

            </Dialog>

        )
    }
}

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as deviceActions from '../../../ax-redux/actions/devices'

export default connect(
    (state, ownProps) => ({
        device: state.devices.get,
        nextAssetId: state.devices.nextAssetId.data.nextAssetIdIncrement,
        fetching: state.devices.get.fetching || state.devices.update.fetching || state.devices.nextAssetId.fetching,
        savedDevice: state.devices.update,
    }),
    dispatch => bindActionCreators(deviceActions, dispatch),
)(DeviceModalUpdate)

