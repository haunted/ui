import React, { Component, PropTypes } from 'react'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import Dialog from '../../standard/dialog'

class Deactivate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            reason: "",
        }
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.deactivate.version > this.props.deactivate.version) {
            this.resetForm()
            this.close(true)
        }
    }
    resetForm() {
        this.setState({
            reason: "",
        })
    }
    close(saved) {
        this.resetForm()
        this.props.onUpdated(saved)
    }
    render() {
        const { deviceId, deviceV, onUpdated, open } = this.props
        const { reason } = this.state
        const assetId = this.props.devicesById[deviceId] ? this.props.devicesById[deviceId].assetId : ""
        return <DeactivateModal
            open={open}
            assetId={assetId}
            deviceId={deviceId}
            deviceV={deviceV}
            deactivating={this.props.deactivate.fetching}
            reason={reason}
            setReason={reason => this.setState({ reason })}
            onCancel={() => this.close(false)}
            onSubmit={() => this.props.deactivateDevice(deviceId, deviceV, reason)}
        />
    }
}

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as deviceActions from '../../../ax-redux/actions/devices'

export default connect(
    (state, ownProps) => {
        return {
            deactivate: state.devices.deactivate,
            devicesById: state.devices.list.devicesById,
        }
    },
    dispatch => bindActionCreators(deviceActions, dispatch),
)(Deactivate)


const DeactivateModal = ({
    deactivating,
    open,
    assetId,
    reason,
    setReason = () => { },
    onSubmit = () => { },
    onCancel = () => { }
}) => (
        <Dialog
            fetching={deactivating}
            title={`Deactivate ${assetId}`}
            actions={[
                <FlatButton
                    label="Cancel"
                    primary={false}
                    onClick={e => onCancel()}
                />,
                <FlatButton
                    label={'Deactivate Device'}
                    primary={true}
                    onClick={e => onSubmit()}
                />
            ]}
            modal={true}
            open={open}
            autoScrollBodyContent={true}
        >
            <TextField
                hintText="Reason"
                floatingLabelText="Reason"
                floatingLabelFixed={true}
                value={reason}
                onChange={(e, v) => setReason(v)}
            />
        </Dialog>
    )