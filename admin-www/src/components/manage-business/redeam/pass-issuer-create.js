import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import Page from '../../standard/page'
import { ActionFooter, ActionButton } from '../../standard/actions'
import RaisedButton from 'material-ui/RaisedButton'
import PassIssuerForm from './pass-issuer-form'

class PassIssuerCreate extends Component {
    constructor(props) {
        super(props)
        this.state = {
            passIssuer: {},
        }
    }
    
    onCancel() {
        browserHistory.push("/app/redeam/pass-issuers");
    }

    onSave() {
        this.props.createPassIssuer(this.state.passIssuer)
    }

    onChange(passIssuer) {
        this.setState({
            passIssuer,
        })
    }
    
    render() { 
        const fetching =  this.props.get.fetching || this.props.create.fetching
        const error = this.props.create.error ? this.props.update.error.data.Error : false 

        return (
            <Page title="Create Pass Issuer" error={error} fetching={fetching}>
                <PassIssuerForm 
                    passIssuer={this.state.passIssuer} 
                    onChange={passIssuer => this.onChange(passIssuer)}
                    error={error}
                />
                <ActionFooter>
                    <ActionButton 
                        label="cancel"
                        onClick={() => this.onCancel()}
                    />
                    <ActionButton 
                        label="Save" 
                        primary={true} 
                        onClick={() => this.onSave()}
                    />
                </ActionFooter>
            </Page>
        )
    }
}

import { connect } from 'react-redux'
import { getPassIssuer, updatePassIssuer, createPassIssuer } from '../../../ax-redux/actions/pass-issuers'

export default connect(
  state => {
    return {
      get: state.passIssuers.get,
      create: state.passIssuers.create,
    }
  },
  dispatch => ({
    getPassIssuer: id => dispatch(getPassIssuer(id)),
    createPassIssuer: issuer => dispatch(createPassIssuer(issuer)),
  })
)(PassIssuerCreate)