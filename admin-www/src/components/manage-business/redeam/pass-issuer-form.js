import React, { Component } from 'react'

import LocationEditor from './location-editor'
import TextField from 'material-ui/TextField'
import { PageForm } from '../../standard/page'
import { validateFeeStructureForSubmit, FeeStructure, RepeatedField, Label } from '../../standard/form-fields'


export const validateForSubmit = passIssuer => {
    let errors = {}

    if (!! passIssuer.fee) {
        const {value, error} = validateFeeStructureForSubmit(passIssuer.fee)
        passIssuer.fee = value
        errors.fee = error
    }
    
    return {passIssuer, errors}
}

const PassIssuerForm = ({ passIssuer, onChange, error, fieldErrors={}, isEdit=false }) => (
    <PageForm error={error}>
        <Label>Name</Label>
        <TextField
            hintText="Name"
            value={passIssuer.name}
            onChange={(e, v) => onChange(Object.assign(passIssuer, { name: v }))}
        />

        <Label>Code</Label>
        <TextField
            disabled={isEdit}
            hintText="Code"
            value={passIssuer.code}
            onChange={(e, v) => onChange(Object.assign(passIssuer, { code: v }))}
        />

        <Label>Billing Code</Label>
        <TextField
            hintText="Billing Code"
            value={passIssuer.billingCode}
            onChange={(e, v) => onChange(Object.assign(passIssuer, { billingCode: v }))}
        />

        <Label style={{ margin: '1em 0 0.5em' }}>Main Location</Label>
        <LocationEditor
            hintText="Main Location"
            value={passIssuer.mainLocation ? passIssuer.mainLocation : {}}
            onChange={v => onChange(Object.assign(passIssuer, { mainLocation: v }))}
        />
        
        <RepeatedField
            label="Aliases"
            createField={
                (v, onChange) => <TextField 
                    hintText="Alias"
                    value={v} 
                    onChange={(e, v) => onChange(v)}
                />
            }
            value={passIssuer.aliases}
            onChange={v => onChange(Object.assign(passIssuer, { aliases: v }))}
        />
        <RepeatedField
            label="Example URLs"
            createField={
                (v, onChange) => <TextField 
                    hintText="URL"
                    value={v} 
                    onChange={(e, v) => onChange(v)}
                />
            }
            value={passIssuer.exampleUrls}
            onChange={v => onChange(Object.assign(passIssuer, { exampleUrls: v }))}
        />
        
        <Label>Fee Structure</Label>
        <FeeStructure
            error={fieldErrors.fee}
            value={!!passIssuer.fee ? passIssuer.fee : {}}
            onChange={v => onChange(Object.assign(passIssuer, { fee: v }))}
        />
    </PageForm>
)

export default PassIssuerForm