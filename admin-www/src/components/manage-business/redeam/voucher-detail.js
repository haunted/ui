import React, {PropTypes, Component} from 'react'
import { browserHistory } from 'react-router'
import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import CircularProgress from 'material-ui/CircularProgress'
import LocationEditor from './location-editor'
import VoucherStatusPopover from './voucher-status-popover'
import QueueForReviewButton from '../../queue-for-review-button'
import moment from 'moment-timezone'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {
  cyan700,
} from 'material-ui/styles/colors';
import getInclusiveRange from '../../reports/get-inclusive-range'
import { enDash, formatStatusEnum, formatUnknown} from '../../../util'

const styles = {
  headerControls : {
    display: "inline-block",
    width: "100%"
  },
  returnToSummaryButton: {
    minWidth: "190px",
    float: "right",
    marginTop: "1em",
    marginRight: "1em"
  }
}

class VoucherDetail extends Component {
  constructor(props) {
    super(props)
    let start = new Date()
    start.setDate(start.getDate() - 30);
    this.state = {
      start: start,
      end: new Date(),
      fetching: false,
      created: null,
      status: "",
      supplierCode: "",
      resellerCode: "",
      productName: "",
      productCode: "",
      cloudFactory: null,
      validationMessage: ""
    }
  }

  componentWillMount() {
    let supplierCode = "",
        resellerCode = "";

      if (!!this.props.params) {
        if (!!this.props.params.supplierCode) {
            supplierCode = this.props.params.supplierCode;
        }
        if (!!this.props.params.resellerCode) {
            resellerCode = this.props.params.resellerCode;
        }
      }
      if (this.isSupplierStaff()) {
        supplierCode = this.props.orgCode;
      } else if (this.isResellerStaff()) {
        resellerCode = this.props.orgCode;
      }
      this.setState({
        supplierCode,
        resellerCode
      })

      this.props.getVoucher(this.props.params.id)
      this.props.getVoucherImage(this.props.params.id)
  }

  componentWillUpdate(nextProps, nextState) { // map fields to view

      if (this.props.fetchingVoucher && nextProps.fetchingVoucher == false && nextProps.voucher != false) {
          let meta = nextProps.voucher,
              voucher = meta.voucher,
              hasCF = (voucher.cloudFactory != null && voucher.cloudFactory.output != null),
              output = hasCF ? voucher.cloudFactory.output : {};

              this.setState({
                created: voucher.created,
                status: voucher.status,
                cloudFactoryStatus: hasCF ? voucher.cloudFactory.status : "",
                bookingReferenceNumber: hasCF ? output.bookingReferenceNumber : "",
                numberOfAdults: hasCF ? output.numberOfAdults : "",
                numberOfChildren: hasCF ? output.numberOfChildren : "",
                numberOfInfants: hasCF ? output.numberOfInfants : "",
                numberOfSeniors: hasCF ? output.numberOfSeniors : "",
                numberOfStudents: hasCF ? output.numberOfStudents : "",
                product: hasCF ? output.product : "",
                productCode: hasCF ? output.productCode : "",
                productId: hasCF ? output.productId : "",
                productName: hasCF ? output.productName : "",
                resellerCode: hasCF ? output.resellerCode : "",
                resellerId: hasCF ? output.resellerId : "",
                resellerName: !!meta.resellerRootName ? meta.resellerRootName : "",
                voucherBrand: !!meta.resellerName ? meta.resellerName : "",
                reseller: hasCF ? output.reseller : "",
                reservationNumber: hasCF ? output.reservationNumber : "",
                supplierConfirmationNumber: hasCF ? output.supplierConfirmationNumber : "",
                totalPrice: hasCF ? output.totalPrice : "",
                totalPriceCurrency: hasCF ? output.totalPriceCurrency : "",
                travelerFirstName: hasCF ? output.travelerFirstName : "",
                travelerLastName: hasCF ? output.travelerLastName : "",
                supplierCode: voucher.supplierCode != null ? voucher.supplierCode : "",
                cloudFactory: hasCF ? voucher.cloudFactory : "",
                cloudVision: voucher.cloudVision != null ? voucher.cloudVision : {},
                rejectReason: hasCF ? output.rejectReason : "",
                operatorId: voucher.operatorId || ""
              })
        }
  }

  rejectRedirect () {
      let start = new Date(this.props.location.query.start * 1000),
          end = new Date(this.props.location.query.end * 1000),
          supplierCode = this.state.supplierCode,
          resellerCode = !!this.props.params.resellerCode ? this.props.params.resellerCode : "";

      setTimeout(()=>{
        browserHistory.push(`/app/reports/voucher-exceptions/${supplierCode+(resellerCode != "" ? ("/"+resellerCode) : "")}/edit/${this.props.params.id}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}`)
      }, 500);
  }

  flagAsUnreadable() {
    let start = new Date(this.props.location.query.start * 1000),
        end = new Date(this.props.location.query.end * 1000),
        supplierCode = this.state.supplierCode,
        resellerCode = !!this.props.params.resellerCode ? this.props.params.resellerCode : "";

    this.props.flagAsUnreadable(this.props.params.id, "unreadable");
    setTimeout(()=>{
      browserHistory.push(`/app/reports/voucher-exceptions/${supplierCode+(resellerCode != "" ? ("/"+resellerCode) : "")}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}`)
    }, 500);
  }

  isSupplierStaff () {
    return this.props.role.indexOf("SUPPLIER") > -1;
  }
  isResellerStaff () {
    return this.props.role.indexOf("RESELLER") > -1;
  }
  returnToSummary () {
      let supplierCode = this.props.params.supplierCode,
          start = this.props.location.query.start,
          end = this.props.location.query.end,
          sort = this.props.location.query.sort;

      browserHistory.push(`/app/reports/vouchers/${supplierCode}?start=${start}&end=${end}&sort=${sort}`);
  }

  renderBasics () {
      return (
          <Table className="edit-table edit-form">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
              <TableRow>
                  <TableHeaderColumn>Property</TableHeaderColumn>
                  <TableHeaderColumn>Value</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow>
                  <TableRowColumn>Date</TableRowColumn>
                  <TableRowColumn>
                  { !!this.state.created && this.state.created.seconds != "" ? moment(this.state.created.seconds * 1000).tz(TZ).format("DD-MMM-YYYY") : "–" }
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                      <TableRowColumn>Status</TableRowColumn>
                      <TableRowColumn>
                        {formatStatusEnum(this.state.status)}
                      </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                      <TableRowColumn>Supplier Code</TableRowColumn>
                      <TableRowColumn>
                        { this.state.supplierCode }
                      </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                      <TableRowColumn>Operator ID</TableRowColumn>
                      <TableRowColumn>
                        { this.state.operatorId }
                      </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                      <TableRowColumn>Timestamp</TableRowColumn>
                      <TableRowColumn>
                        { !!this.state.created && this.state.created.seconds != "" ? moment(this.state.created.seconds * 1000).tz(TZ).format("HH:MM A") : "–" }
                      </TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
      )
  }
  renderCloudVision () {
      return (
          <Table className="edit-table edit-form">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
              <TableRow>
                  <TableHeaderColumn>Property</TableHeaderColumn>
                  <TableHeaderColumn>Value</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow key={"1"} selectable={false}>
                  <TableRowColumn key={"1.1"}>Image ID</TableRowColumn>
                  <TableRowColumn key={"1.2"}>
                  { !!this.state.cloudVision && enDash(this.state.cloudVision.imageId) }
                  </TableRowColumn>
              </TableRow>
                  <TableRow selectable={false}>
                      <TableRowColumn key={"1.3"}>Status</TableRowColumn>
                      <TableRowColumn key={"1.2"}>
                        { !!this.state.cloudVision && formatStatusEnum(enDash(this.state.cloudVision.status)) }
                      </TableRowColumn>
                  </TableRow>
            </TableBody>
          </Table>
      )
  }
  renderCloudFactory () {
    let resellerName = this.state.resellerName
      if (this.state.voucherBrand != this.state.resellerName) {
        resellerName = `${this.state.voucherBrand} (${this.state.resellerName})`
      }
      return (
          <Table className="edit-table edit-form">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
              <TableRow>
                  <TableHeaderColumn>Property</TableHeaderColumn>
                  <TableHeaderColumn>Value</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Transaction ID</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { !!this.state.cloudFactory && enDash(this.state.cloudFactory.id)}
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Status</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    {formatStatusEnum(enDash(this.state.cloudFactoryStatus))}
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Reseller Name</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { formatUnknown(enDash(resellerName)) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Reseller Code</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { formatUnknown(enDash(this.state.resellerCode)) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Product Name</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { formatUnknown(enDash(this.state.productName)) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Product Code</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { formatUnknown(enDash(this.state.productCode)) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Adults</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.numberOfAdults) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Children</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.numberOfChildren) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Infants</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.numberOfInfants) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Seniors</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.numberOfSeniors) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Students</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.numberOfStudents) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Reservation Number</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.reservationNumber) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Booking Reference Number</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.bookingReferenceNumber) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Supplier Confirmation Number</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.supplierConfirmationNumber) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Total Price</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.totalPrice) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Total Price Currency</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.totalPriceCurrency) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Traveler First Name</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.travelerFirstName) }
                    </TableRowColumn>
                </TableRow>
                <TableRow selectable={false} >
                    <TableRowColumn key={"1.1"}>Traveler Last name</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    { enDash(this.state.travelerLastName) }
                    </TableRowColumn>
                </TableRow>
            </TableBody>
          </Table>
      )
  }

  renderError (err) {
        let msg = err.length > 0 ?
            <span className="form-error">
              {err}
            </span>
           : ""
        return msg;
  }

  render() {
      let saveError = this.props.saveError,
           images = this.props.images,
           fetching = this.props.fetching
                    || this.props.savingVoucher;
    return (
        <div className="supplier-edit voucher-edit">
                <div style={styles.headerControls}>
                  <h2>Voucher Detail</h2>
                  <RaisedButton onTouchTap={e => { this.returnToSummary() } }
                                className="no-print"
                                style={styles.returnToSummaryButton}
                                label="Return to Summary"
                  />
                </div>
                { !!saveError ? this.renderError(saveError.message) : "" }
                { this.renderError(this.state.validationMessage) }
                <section className="image">
                {
                  images.map((image, i) => (
                    <a key={i} href={image} title="Open In New Tab" target="_blank" className="voucher-detail-image-link">
                        <img src={image} title="Open In New Tab" className="voucher-image" style={{cursor: "pointer"}} />
                    </a>
                  ))
                }
                </section>
                <section className="tables">
                    { !fetching ? this.renderBasics() :
                        <div className="loading-circle">
                            <CircularProgress color={"#27367a"} size={2} />
                        </div>
                    }
                    <h2>CloudFactory Output
                        {/* <RaisedButton className="save-button" label="Reject" secondary={true}
                                      style={{marginLeft: "1em"}} onClick={e => this.reject()} /> */}
                        <QueueForReviewButton className="save-button" label="Reject" secondary={true} voucherId={this.props.params.id}
                                              style={{marginLeft: "1em"}} callback={e => this.rejectRedirect()} />
                        <RaisedButton className="save-button" label="Unreadable" onClick={e => this.flagAsUnreadable()} />
                    </h2>
                    { !fetching ? this.renderCloudFactory() :
                        <div className="loading-circle">
                            <CircularProgress color={"#27367a"} size={2} />
                        </div>
                    }
                    {/*<h2>CloudVision Output</h2>*/}
                    {/*{ !fetching ? this.renderCloudVision() :
                        <div className="loading-circle">
                            <CircularProgress color={"#27367a"} size={2} />
                        </div>
                    }*/}
                {/*<div className="cloud-vision-text">
                    {!!this.state.cloudVision ? this.state.cloudVision.text : ""}
                </div>*/}
            </section>
      </div>
    );
  }
}

VoucherDetail.propTypes = {

};

import { connect } from 'react-redux';
import {
    rejectVoucher,
    getVoucher,
    getVoucherImage
 } from '../../../ax-redux/actions/vouchers'

export default connect(
  (state, ownProps) => {
    return {
      role: state.app.role,
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      voucher: state.vouchers.getVoucher.data,
      savingVoucher: state.vouchers.rejectVoucher.fetching,
      saveError: false, //state.vouchers.correctVoucher.error,
      fetching: state.vouchers.getVoucher.fetching || state.vouchers.rejectVoucher.fetching,
      fetchingVoucher: state.vouchers.getVoucher.fetching,
      images: !!state.vouchers.getVoucherImage.data ? (
        !!state.vouchers.getVoucherImage.data.urls ? state.vouchers.getVoucherImage.data.urls  :
        [state.vouchers.getVoucherImage.data.url]
      ) : [""]
    }
  },
  dispatch => {
    return {
      getVoucher: (id) => {
        dispatch(getVoucher(id))
      },
      rejectVoucher: (id) => {
        dispatch(rejectVoucher(id))
      },
      flagAsUnreadable: (id, reason) => {
        dispatch(rejectVoucher(id, {reason: reason}))
      },
      getVoucherImage: (id) => {
          dispatch(getVoucherImage(id))
      }
    }
  }
)(VoucherDetail)
