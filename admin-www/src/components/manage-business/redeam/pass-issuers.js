import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import RedeamTable, { ActionButton } from '../../standard/table';
import Page from '../../standard/page';
import { listPassIssuers } from '../../../ax-redux/actions/pass-issuers';

class PassIssuers extends Component {
  constructor(props) {
    super(props);
    this.fetch();
  }

  onCreate() {
    browserHistory.push('app/redeam/pass-issuers/add');
  }

  onEdit(id) {
    browserHistory.push(`app/redeam/pass-issuers/${id}`);
  }

  fetch() {
    this.props.listPassIssuers(this.props.list.params);
  }

  refresh(saved = false) {
    if (saved) {
      this.fetch();
    }
    browserHistory.replace('app/redeam/pass-issuers');
  }

  render() {
    const { list, onSort } = this.props;
    const { error, fetching, data, params } = list;

    return (
      <Page title="Pass Issuers" error={error} fetching={fetching}>
        <PassIssuersTable
          data={data}
          onEdit={id => this.onEdit(id)}
          onSort={name => onSort(name, params)}
          onCreate={() => this.onCreate()}
          onRowClick={data => this.onEdit(data.id)}
        />
      </Page>
    );
  }
}

const statuses = {
  STATUS_UNKNOWN: 'Unknown',
  STATUS_NEW: 'New',
  STATUS_TEST_MODE: 'Test Mode',
  STATUS_LIVE: 'Live',
  STATUS_DISABLED: 'Disabled',
  STATUS_DELETED: 'Deleted',
};
const PassIssuersTable = ({ data, onSort, onCreate, onRowClick }) => (
  <RedeamTable
    onSort={onSort}
    onRowClick={(e, data) => onRowClick(data)}
    columns={[
      { title: 'Name' },
      { title: 'Code' },
      { title: 'Billing Code' },
      { title: 'Location' },
      { title: 'Status' },
    ]}
    data={data}
    rows={[
      v => v.name,
      v => v.code,
      v => v.billingCode,
      v => (v.mainLocation ? v.mainLocation.city : ''),
      v => statuses[v.status],
    ]}
    actions={
      <RaisedButton
        style={{ alignSelf: 'flex-start' }}
        primary
        onClick={e => onCreate()}
        label="Add Pass Issuer"
      />
    }
  />
);

export default connect(
  state => ({
    list: state.passIssuers.list,
  }),
  dispatch => ({
    listPassIssuers: () => dispatch(listPassIssuers()),
  }),
)(PassIssuers);
