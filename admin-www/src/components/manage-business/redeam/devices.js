import React, { Component } from 'react';
import { browserHistory } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import { connect } from 'react-redux';
import RedeamTable, { UUIDCell, NotesCell } from '../../standard/table';
import { ActionButton } from '../../standard/actions';
import Page from '../../standard/page';
import DeviceModalUpdate from './device-modal-update';
import DeviceModalCreate from './device-modal-create';
import DeviceModalNotes from './device-modal-notes';
import Deactivate from './device-deactivate';
import {
  listDevices,
  openDeviceModal,
  deactivateDevice,
} from '../../../ax-redux/actions/devices';


const deviceStatuses = {
  STATUS_UNKNOWN: 'Unknown',
  STATUS_NEW: 'New',
  STATUS_PROVISIONED: 'Provisioned',
  STATUS_DISABLED: 'Disabled',
  STATUS_DELETED: 'Deleted',
  STATUS_DEPROVISIONED: 'Deprovisioned',
  STATUS_DEACTIVATED: 'Deactivated',
};
const deviceStatusLabel = status => (deviceStatuses[status] ? deviceStatuses[status] : status);

class Devices extends Component {
  constructor(props) {
    super(props);
    this.fetch();
  }

  onCreate() {
    browserHistory.replace('app/redeam/devices/new');
  }

  onEdit(id) {
    browserHistory.replace(`app/redeam/devices/${id}`);
  }

  onNotes({ id, version }) {
    browserHistory.replace(`app/redeam/devices/${id}/${version}/notes`);
  }

  onDeactivate({ id, version }) {
    browserHistory.replace(`app/redeam/devices/${id}/${version}/deactivate`);
  }

  fetch() {
    this.props.listDevices({
      sort: this.props.list.params.sort,
    });
  }

  refresh(saved = false) {
    if (saved) {
      this.fetch();
    }
    browserHistory.replace('app/redeam/devices');
  }

  render() {
    const { list, onSort } = this.props;
    const { error, fetching, data, params } = list;
    const isEdit = !!this.props.params.id && !this.props.params.version && this.props.params.id != 'new';
    const isCreate = this.props.params.id == 'new';
    const isDeactivate = !!this.props.params.id && !!this.props.params.version && this.props.location.pathname.search(/\/deactivate$/) > -1;
    const isNotes = !!this.props.params.id && !!this.props.params.version && this.props.location.pathname.search(/\/notes$/) > -1;

    return (
      <Page title="Devices" error={error} fetching={fetching}>
        <DeviceTable
          data={data}
          onNotes={d => this.onNotes(d)}
          onEdit={id => this.onEdit(id)}
          onSort={name => onSort(name, params)}
          onCreate={() => this.onCreate()}
          onDeactivate={data => this.onDeactivate(data)}
        />
        <DeviceModalUpdate
          open={isEdit}
          deviceId={this.props.params.id}
          onUpdated={saved => this.refresh(saved)}
        />
        <DeviceModalCreate
          open={isCreate}
          onUpdated={saved => this.refresh(saved)}
        />
        <DeviceModalNotes
          open={isNotes}
          onUpdated={saved => this.refresh(saved)}
          deviceId={this.props.params.id}
          deviceV={this.props.params.version}
        />
        <Deactivate
          open={isDeactivate}
          deviceId={this.props.params.id}
          deviceV={this.props.params.version}
          onUpdated={saved => this.refresh(saved)}
        />
      </Page>
    );
  }
}


const DeviceTable = ({ data, onNotes, onEdit, onDeactivate, onSort, onCreate }) => (
  <RedeamTable
    onSort={onSort}
    columns={[
      { width: 8, title: 'Asset ID', sort: 'assetID' },
      { title: 'Status', sort: 'status' },
      { width: 10, title: 'Supplier Code', sort: 'supplierCode' },
      { width: 37, title: 'Device UUID' },
      { title: 'Serial Number', sort: 'serialNumber' },
      { title: 'IMEI Number' },
      { title: 'SIM Number', sort: 'simNumber' },
      { title: 'Phone Number' },
      { title: 'Carrier', sort: 'carrier' },
      { title: 'Location' },
      { title: 'Notes' },
      { title: 'Actions' },
    ]}
    data={data}
    rows={[
      v => v.assetId,
      v => deviceStatusLabel(v.status),
      v => v.supplierCode,
      v => <UUIDCell value={v.id} />,
      v => v.serialNumber,
      v => v.imei,
      v => v.simNumber,
      v => v.phoneNumber,
      v => v.carrier,
      v => v.location,
      v => <NotesCell show={v.notes.length > 0} value={v.notes} />,
      v => [
        <ActionButton onClick={e => onNotes(v)} key={1}>Notes</ActionButton>,
        <ActionButton onClick={e => onEdit(v.id)} key={2}>Edit</ActionButton>,
        <ActionButton disabled={v.status == 'STATUS_DEACTIVATED'} onClick={e => onDeactivate(v)} key={3}>Deactivate</ActionButton>,
      ],
    ]}
    actions={
      <RaisedButton
        style={{ alignSelf: 'flex-start' }}
        primary
        onClick={e => onCreate()}
        label="Add Device"
      />
    }
  />
);


const mapStateToProps = state => ({
  list: state.devices.list,
});

const mapDispatchToProps = dispatch => ({
  listDevices: (params) => {
    dispatch(listDevices(params));
  },
  deactivateDevice: (id, v, reason) => dispatch(deactivateDevice(id, v, reason)),
  openDeviceModal: (id) => {
    dispatch(openDeviceModal(id));
  },
  onSort: (name, params) => {
    if (!params.sort || params.sort == `-${name}`) {
      params.sort = name;
    } else {
      params.sort = `-${name}`;
    }
    dispatch(listDevices(params));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Devices);

