import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress';
import { Card, CardText } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { browserHistory } from 'react-router';
import { formatEnum } from '../../../util';
import * as accountsActions from '../../../ax-redux/actions/accounts';
import * as groupsActions from '../../../ax-redux/actions/groups';

const styles = {
  title: {
    color: 'rgb(67, 67, 67)',
    display: 'inline-block',
    marginLeft: '1em',
    marginRight: '1em',
  },
  searchUsername: {
    display: 'inline-block',
    margin: '0.75em 1em 0 0',
    background: 'white',
    padding: '0 1em',
  },
  addNew: {
    float: 'right',
    marginTop: '1em',
    marginRight: '1em',
  },
};

const addUser = () => browserHistory.push('/app/redeam/users/add');
const renderRoles = (roles) => {
  if (!roles.length) {
    return '–';
  } else {
    return (
      <div>
        {roles.map(group => (
          <div key={group.id}>
            {formatEnum(group.name)}
          </div>
        ))}
      </div>
    );
  }
};

class Users extends Component {
  constructor(props) {
    super(props);

    const {
      location,
    } = props;

    const limit = location.query.limit || 100;
    const offset = location.query.offset || 0;
    const username = location.query.username || '';

    this.onRowClick = this.onRowClick.bind(this);
    this.fetchData = this.fetchData.bind(this);
    this.changePage = this.changePage.bind(this);
    this.search = this.search.bind(this);
    this.handleSearchInput = this.handleSearchInput.bind(this);
    this.clearSearch = this.clearSearch.bind(this);

    this.state = {
      selected: false,
      lastLoadCount: 0,
      offset: parseInt(offset, 10),
      limit: parseInt(limit, 10),
      username,
    };
  }

  componentWillMount() {
    this.fetchData();
  }

  onRowClick(keys) {
    const username = this.state.username;
    const offset = this.state.offset;
    const limit = this.state.limit;

    if (keys.length) {
      const account = this.props.accounts[keys[0]];

      browserHistory.push(`/app/redeam/users/${account.id}?offset=${offset}&limit=${limit}&username=${username}`);
    }
  }

  fetchData() {
    const { listAccounts } = this.props;

    const {
      limit,
      offset,
      username,
    } = this.state;

    listAccounts({
      limit,
      offset,
      username,
    });
  }

  changePage(direction) {
    const {
      username,
      offset,
      limit,
    } = this.state;

    const newOffset = offset + (limit * direction);

    this.setState({
      offset: newOffset,
    }, this.fetchData);

    browserHistory.push(`/app/redeam/users?offset=${newOffset}&limit=${limit}&username=${username}`);
  }

  search() {
    const {
      limit,
      username,
    } = this.state;

    clearTimeout(this.searchTimeout);

    this.searchTimeout = setTimeout(() => {
      browserHistory.push(`/app/redeam/users?offset=0&limit=${limit}&username=${username}`);
      this.fetchData();
    }, 500);
  }

  handleSearchInput(e) {
    this.setState({
      offset: 0,
      username: e.target.value,
    }, this.search);
  }

  clearSearch() {
    this.setState({
      username: '',
    }, this.search);
  }

  renderRows() {
    const { accounts } = this.props;

    return accounts.map(account => (
      <TableRow key={account.id}>
        <TableRowColumn>{account.username}</TableRowColumn>
        <TableRowColumn>{account.orgCode}</TableRowColumn>
        <TableRowColumn>{account.givenName}</TableRowColumn>
        <TableRowColumn>{account.surname}</TableRowColumn>
        <TableRowColumn>{account.email}</TableRowColumn>
        <TableRowColumn>{account.phone}</TableRowColumn>
        <TableRowColumn>{renderRoles(account.groups)}</TableRowColumn>
      </TableRow>
    ));
  }

  renderPaginationControls() {
    const {
      accounts,
      fetching,
    } = this.props;

    const {
      offset,
      limit,
    } = this.state;

    return (
      <div className="pagination-controls">
        {!fetching && offset > 0 && (
          <RaisedButton label="Previous Page" onClick={() => this.changePage(-1)} />
        )}

        <div style={{ display: 'inline-block', marginRight: '1em' }} />

        {!fetching && accounts.length === limit && (
          <RaisedButton label="Next Page" onClick={() => this.changePage(1)} />
        )}
      </div>
    );
  }

  render() {
    const {
      fetching,
      accounts,
    } = this.props;

    const {
      username,
    } = this.state;

    return (
      <div className="user-management">
        <h2 style={styles.title}>Users</h2>
        <div style={styles.searchUsername}>
          <TextField
            hintText="Search Username"
            value={username}
            onChange={this.handleSearchInput}
          />
        </div>

        {username && (
          <RaisedButton label="Clear Search" onClick={this.clearSearch} />
        )}

        <RaisedButton
          onClick={addUser}
          style={styles.addNew}
          label="Add New User"
          primary
        />
        <Card>
          <CardText expandable={false}>
            <Table onRowSelection={keys => this.onRowClick(keys)} className="users index">
              <TableHeader
                adjustForCheckbox={false}
                displayRowCheckbox={false}
                enableSelectAll={false}
                displaySelectAll={false}
              >
                <TableRow>
                  <TableHeaderColumn className="sortable">
                    <div className="inner">Username</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner">Organization</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner">First Name</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner">Last Name</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner">Email</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner">Phone</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner">Roles</div>
                  </TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                {!fetching && this.renderRows()}
              </TableBody>
            </Table>
          </CardText>

          {!fetching && !accounts.length && (
            <div className="table-error">No Records To Display</div>
          )}
        </Card>

        {fetching && (
          <div className="loading-circle">
            <CircularProgress color={'#27367a'} size={96} />
          </div>
        )}

        {this.renderPaginationControls()}
      </div>
    );
  }
}

Users.propTypes = {
  location: PropTypes.shape({
    location: PropTypes.object,
  }).isRequired,
  accounts: PropTypes.arrayOf(PropTypes.object).isRequired,
  fetching: PropTypes.bool.isRequired,
  listAccounts: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  accounts: state.accounts.list.data,
  offset: state.accounts.list.offset,
  fetching: state.accounts.list.fetching,
  groups: state.groups.list.data,
  fetchingGroups: state.groups.list.fetching,
  error: state.accounts.list.error,
  pathname: state.routing.locationBeforeTransitions.pathname,
  queryParams: state.routing.locationBeforeTransitions.search,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  listAccounts: accountsActions.listAccounts,
  listAccountPermissions: accountsActions.listAccountPermissions,
  listGroups: groupsActions.listGroups,
  listAccountsByGroup: groupsActions.listAccountsByGroup,
  listGroupPermissions: groupsActions.listGroupPermissions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Users);
