import React, {PropTypes, Component} from 'react'
import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import CircularProgress from 'material-ui/CircularProgress'
import LocationEditor from './location-editor'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {formatStatusEnum} from '../../../util'
import ViewOrgDataButton from '../view-org-data-button'

let styles = {
  formOptions: {
    marginBottom: "1em",
    marginTop: "3em",
    marginLeft: "1em",
    height: '1em'
  },
  formOption: {
    float: 'right',
    marginLeft: '1em'
  }
}

class ResellerEdit extends Component {
  constructor(props) {
    super(props)
    this.state = {
      resellerId: "",
      resellerVersion: "",
      resellerName: "",
      resellerCode: "",
      resellerStatus: "",
      resellerPercentage: "",
      resellerCap: "",
      resellerMin: "",
      resellerAliases: [],
      newResellerAlias: "",
      resellerMainLocation: {
        country: "",
        state: "",
        city: "",
      },
      fetching: false,
      nameError: false,
      codeError: false,
      percentageError: false,
      capError: false,
      minError: false,
      validationMessage: ""
    }
  }

  componentWillMount() {
    if (this.props.mode == "edit") {
      this.props.getReseller(this.props.params.id)
      this.setState({fetching: true})
    }
  }

  componentWillUpdate(nextProps, nextState) {
    // map fields to view
    let record = null
    if (!this.props.createdReseller && !!nextProps.createdReseller) {
      record = nextProps.createdReseller
      this.setState({
        fetching: false,
        resellerId: record.id,
        resellerVersion: record.version,
        resellerName: record.name,
        resellerCode: record.code,
        resellerPercentage: record.fee ? record.fee.percentage : 0,
        resellerCap: record.fee ? record.fee.cap : 0,
        resellerMin: record.fee ? record.fee.min : 0,
        resellerStatus: record.status,
        resellerAliases: record.aliases || [],
        resellerMainLocation: record.mainLocation
      })
    }
    if (this.props.mode == "edit") {
      if (this.state.fetching && !nextProps.resellers.get.fetching && this.props.resellers.get.fetching) {
        if (nextProps.resellers.get.error) {
          this.setState({
            fetching: false,
          })
        } else {
          let nextResellerData = nextProps.resellers.get.data.reseller.reseller;
          this.setState({
            fetching: false,
            resellerId: nextResellerData.id,
            resellerVersion: nextResellerData.version,
            resellerName: nextResellerData.name,
            resellerCode: nextResellerData.code,
            resellerPercentage: nextResellerData.fee ? nextResellerData.fee.percentage : 0,
            resellerCap: nextResellerData.fee ? nextResellerData.fee.cap : 0,
            resellerMin: nextResellerData.fee ? nextResellerData.fee.min : 0,
            resellerStatus: nextResellerData.status,
            resellerAliases: nextResellerData.aliases || [],
            resellerMainLocation: nextResellerData.mainLocation
                                  ? nextResellerData.mainLocation : {
                                      country: "",
                                      state: "",
                                      city: ""
                                  }
          })
        }
      }
    }
    /*/ Clear Errors /*/
    if (this.state.percentageError && this.state.resellerPercentage != nextState.resellerPercentage) { // clear percentage error
      this.setState({ percentageError: false })
    }
    if (this.state.capError && this.state.resellerCap != nextState.resellerCap) { // clear cap error
      this.setState({ capError: false })
    }
    if (this.state.minError && this.state.resellerMin != nextState.resellerMin) { // clear min error
      this.setState({ capError: false })
    }
    if (this.state.nameError && this.state.resellerName != nextState.resellerName) { // clear name error
      this.setState({ nameError: false })
    }
    if (this.state.codeError && this.state.resellerCode != nextState.resellerCode) { // clear code error
      this.setState({ codeError: false })
    }
  }

  save() {
    let data = {
      name: this.state.resellerName,
      code: this.state.resellerCode,
      aliases: this.state.resellerAliases,
      contacts: [],
      otherLocations: [],
      mainLocation: this.state.resellerMainLocation,
      fee: {
          percentage: this.state.resellerPercentage != "" ? parseFloat(this.state.resellerPercentage) : 0,
          cap: this.state.resellerCap != "" ? parseFloat(this.state.resellerCap) : 0

      },
      trialExpirationDate: null,
    }
    if (!!this.state.resellerMin && this.state.resellerMin != "") {
        data.fee.min = parseFloat(this.state.resellerMin);
    }

    if (data.name == "") {
      this.setState({nameError: "Name is required"})
      return
    }
    if (data.code == "") {
      this.setState({codeError: "Code is required"})
      return
    }

    if (this.props.mode == "edit") {
      data.id = this.state.resellerId
      data.version = this.state.resellerVersion
      data.status = this.state.resellerStatus,
      this.props.updateReseller(data.id, data)
    } else {
      this.props.createReseller(data)
    }
  }

  disable () {
      if (confirm("Disable this reseller?")) {
          this.props.disableReseller(this.state.resellerId, this.state.resellerVersion);
      }
  }

  addAlias() {
    let aliases = this.state.resellerAliases;
    if (this.state.newResellerAlias != "") {
      aliases.push(this.state.newResellerAlias)
    }
    this.setState({
      resellerAliases: aliases
    })
  }
  removeAlias(index) {
    let aliases = this.state.resellerAliases;
    aliases.splice(index, 1);
    this.setState({
      resellerAliases: []
    })
    setTimeout(()=>{
      this.setState({
        resellerAliases: aliases
      })
    },100);
  }
  updateAliases (value, index) {
    let aliases = this.state.resellerAliases;
    aliases[index] = value;
    this.setState({
      resellerAliases: aliases
    })
  }

  renderForm (mode) {
      return (
          <Table className="edit-table">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>Property</TableHeaderColumn>
                    <TableHeaderColumn>Value</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                <TableRow key={"1"} selectable={false} >
                    <TableRowColumn key={"1.1"}>Name</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                        <TextField
                            floatingLabelText="Name"
                            className="text-edit no-label"
                            value={this.state.resellerName}
                            onChange={e => this.setState({ resellerName: e.target.value })}
                            errorText={this.state.nameError}
                          />
                    </TableRowColumn>
                </TableRow>
                {mode == "edit" ? [
                    <TableRow key={"2"} selectable={false} >
                        <TableRowColumn key={"1.1"}>Id</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              floatingLabelText="Id"
                              className="text-edit no-label"
                              readOnly={true}
                              value={this.state.resellerId}
                            />
                        </TableRowColumn>
                    </TableRow>,
                    <TableRow key={"3"} selectable={false} >
                        <TableRowColumn key={"1.1"}>Version</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              floatingLabelText="Version"
                              className="text-edit no-label"
                              readOnly={true}
                              value={this.state.resellerVersion}
                            />
                        </TableRowColumn>
                    </TableRow>,
                    <TableRow key={"4"} selectable={false} >
                        <TableRowColumn key={"1.1"}>Status</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              floatingLabelText="Status"
                              className="text-edit no-label"
                              value={formatStatusEnum(this.state.resellerStatus)}
                              readOnly={true}
                              onChange={e => this.setState({ resellerStatus: e.target.value })}
                            />
                        </TableRowColumn>
                    </TableRow>
                ]: []}
                <TableRow key={"5"} selectable={false} >
                    <TableRowColumn key={"1.1"}>Code</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                    {
                        this.props.mode == "add" ?
                        <TextField
                          floatingLabelText="Code"
                          className="no-label"
                          value={this.state.resellerCode}
                          onChange={e => this.setState({ resellerCode: e.target.value })}
                          errorText={this.state.codeError}
                        /> :
                        <TextField value={this.state.resellerCode}
                                   disabled={true}
                                   id="text-field-disabled"
                        />
                    }
                    </TableRowColumn>
                </TableRow>
                <TableRow key={"6"} selectable={false} >
                    <TableRowColumn key={"1.1"}>Main Location</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                        <LocationEditor
                          value={this.state.resellerMainLocation}
                          onChange={location => this.setState({resellerMainLocation: location})}
                          onError={error => {this.setState({validationMessage: error})} }
                        />
                    </TableRowColumn>
                </TableRow>
                <TableRow key={"7"} selectable={false} >
                    <TableRowColumn key={"1.1"}>Fee Structure</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                        <TextField
                          floatingLabelText="Percentage"
                          className="text-edit label"
                          value={this.state.resellerPercentage}
                          onChange={e => this.setState({ resellerPercentage: e.target.value })}
                          errorText={this.state.percentageError}
                        />
                        <TextField
                          floatingLabelText="Min"
                          className="text-edit label"
                          value={this.state.resellerMin}
                          onChange={e => this.setState({ resellerMin: e.target.value })}
                          errorText={this.state.minError}
                        />
                        <TextField
                          floatingLabelText="Cap"
                          className="text-edit label"
                          value={this.state.resellerCap}
                          onChange={e => this.setState({ resellerCap: e.target.value })}
                          errorText={this.state.capError}
                        />
                    </TableRowColumn>
                </TableRow>
            </TableBody>
         </Table>
      )
  }

  renderAliases () {
      return (
          <Table className="edit-table">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>Property</TableHeaderColumn>
                    <TableHeaderColumn>Value</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                <TableRow key={"1"} selectable={false} >
                    <TableRowColumn key={"1.1"}>Aliases</TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                        <TextField
                            floatingLabelText="Reseller Alias"
                            className="text-edit no-label"
                            onChange={e => this.setState({ newResellerAlias: e.target.value })}
                          />
                          <RaisedButton className="add-alias-button" label="Add" onClick={e => this.addAlias()} />
                    </TableRowColumn>
                </TableRow>
                {
                  this.state.resellerAliases.map((alias, index) => (
                    <TableRow key={index} selectable={false} >
                        <TableRowColumn key={"1.1"}></TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                          <TextField
                              id={"resellerAlias-"+index}
                              defaultValue={alias}
                              className="text-edit no-label"
                              onBlur={e => this.updateAliases(e.target.value, index) }
                            />
                          <RaisedButton className="remove-alias-button" label="Remove" onClick={e => this.removeAlias(index)} />
                        </TableRowColumn>
                    </TableRow>
                  ))
                }
            </TableBody>
         </Table>
      )
  }


  renderError (err) {
      let msg = err.length > 0 ?
          <span className="form-error">
            {err}
          </span>
         : ""
      return msg;
  }

  render() {
      let saveError = this.props.saveError,
          fetching  = this.props.fetching ||
                      this.props.savingReseller ||
                      this.props.disablingReseller || 
                      this.props.creatingReseller
    return (
        <div className="reseller-edit edit-form">
            <Paper className="inner-edit" zIndex={1}>
            <h2>{`${this.props.mode == "add" ? "Add" : "Edit"} Reseller`}</h2>
            { !!saveError ? this.renderError(saveError.message) : "" }
            { this.renderError(this.state.validationMessage) }
            {
                !fetching ? this.renderForm(this.props.mode) :
                <div className="loading-circle">
                  <CircularProgress color={"#27367a"} size={96} />
                </div>
            }
            { !fetching ? this.renderAliases() : "" }
              <div style={styles.formOptions}>
                { !fetching ? <RaisedButton onClick={e => this.save()}
                                            style={styles.formOption}
                                            className="save-button"
                                            primary={true}
                                            label="Save"
                              /> : ""
                }
                { !fetching && this.props.mode == "edit" ?
                    <RaisedButton onClick={e => {this.disable()}}
                                  style={styles.formOption}
                                  className="delete-button"
                                  label="Disable"
                    /> : ""
                }
                { this.props.mode != "add" ? (
                  <ViewOrgDataButton style={styles.formOption}
                                   id={this.props.params.id}
                                   model={"products"}
                                   orgType="reseller"
                  />
                ): ""}
              </div>
            </Paper>
        </div>
    );
  }
}

ResellerEdit.propTypes = {

};

import { connect } from 'react-redux';
import { getReseller, createReseller, updateReseller, disableReseller } from '../../../ax-redux/actions/resellers'

export default connect(
  (state, ownProps) => {
    return {
      mode: (state.routing.locationBeforeTransitions.pathname.search("/add") > -1 ? "add" : "edit"),
      resellers: state.resellers,
      fetching: state.resellers.get.fetching,
      savingReseller: state.resellers.update.fetching,
      saveError: state.resellers.update.error,
      disablingReseller: state.resellers.disable.fetching,
      createdReseller: !!state.resellers.create.data ? state.resellers.create.data.reseller.reseller : false,
      creatingReseller: state.resellers.create.fetching
    }
  },
  dispatch => {
    return {
      getReseller: (id) => {
        dispatch(getReseller(id))
      },
      createReseller: (data) => {
        dispatch(createReseller(data))
      },
      disableReseller: (id, version) => {
        dispatch(disableReseller(id, version))
      },
      updateReseller: (id, data) => {
        dispatch(updateReseller(id, data))
      }
    }
  }
)(ResellerEdit)




// // Reseller is the...
// type Reseller struct {
// }

// var Reseller_Status_name = map[int32]string{
// 	0: "STATUS_UNKNOWN",
// 	1: "STATUS_NEW",
// 	2: "STATUS_TEST_MODE",
// 	3: "STATUS_LIVE",
// 	4: "STATUS_DISABLED",
// }



// type Contact struct {
// 	Name     string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
// 	Title    string `protobuf:"bytes,2,opt,name=title,proto3" json:"title,omitempty"`
// 	Role     string `protobuf:"bytes,3,opt,name=role,proto3" json:"role,omitempty"`
// 	Email    string `protobuf:"bytes,4,opt,name=email,proto3" json:"email,omitempty"`
// 	Username string `protobuf:"bytes,5,opt,name=username,proto3" json:"username,omitempty"`
// }

// type FeeStructure struct {
// 	Percentage float32 `protobuf:"fixed32,1,opt,name=percentage,proto3" json:"percentage,omitempty"`
// 	Cap        float32 `protobuf:"fixed32,2,opt,name=cap,proto3" json:"cap,omitempty"`
// }
