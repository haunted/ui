import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'

import RedeamTable, { UUIDCell, NotesCell } from '../../standard/table'
import Page from '../../standard/page'
import RaisedButton from 'material-ui/RaisedButton'
import DeviceProvision from './device-provision'
import DeviceDeprovision from './device-deprovision'
import { ActionButton } from '../../standard/actions'

class Devices extends Component {
    constructor(props) {
        super(props)
        this.fetch()
    }
    fetch() {
        this.props.listDevices({
            sort: this.props.list.params.sort,
            supplierCode: this.props.params.supplierCode,
            status: 'STATUS_PROVISIONED',
        })
    }
    refresh(saved = false) {
        if (saved) {
            this.fetch()
        }
        browserHistory.replace(`app/redeam/supplier/${this.props.params.supplierCode}/devices`)
    }
    onProvision() {
        browserHistory.replace(`app/redeam/supplier/${this.props.params.supplierCode}/devices/provision`)        
    }
    onDeprovision({id, version}) {
        browserHistory.replace(`app/redeam/supplier/${this.props.params.supplierCode}/devices/${id}/${version}/deprovision`)        
    }
    render() {
        const { list, onSort } = this.props
        const { error, fetching, data, params } = list
        const isDeprovision = this.props.location.pathname.search(/\/deprovision$/) > -1
        const isProvision = this.props.location.pathname.search(/\/provision$/) > -1
        
        return (
            <Page title="Devices" error={error} fetching={fetching}>
                <DeviceTable
                    data={data}                    
                    onSort={name => onSort(name, params)}
                    onProvision={() => this.onProvision()}
                    onDeprovision={data => this.onDeprovision(data)}
                />
                <DeviceProvision 
                    open={isProvision}
                    supplierCode={this.props.params.supplierCode}
                    onUpdated={(saved) => this.refresh(saved)}                    
                />
                <DeviceDeprovision
                    open={isDeprovision}
                    onUpdated={(saved) => this.refresh(saved)}
                    deviceId={this.props.params.id}           
                    deviceV={this.props.params.version}           
                />

            </Page>
        )
    }
}


const deviceStatuses = {
    STATUS_UNKNOWN: "Unknown",
    STATUS_NEW: "New",
    STATUS_PROVISIONED: "Provisioned",
    STATUS_DISABLED: "Disabled",
    STATUS_DELETED: "Deleted",
    STATUS_DEPROVISIONED: "Deprovisioned",
    STATUS_DEACTIVATED: "Deactivated",
}
const deviceStatusLabel = status => !!deviceStatuses[status] ? deviceStatuses[status] : status

const DeviceTable = ({ data, onSort, onDeprovision, onProvision }) => (
    <RedeamTable
        onSort={onSort}
        columns={[
            { width: 8, title: 'Asset ID', sort: 'assetID' },
            { width: 37, title: 'Device UUID' },
            { title: 'Serial Number', sort: 'serialNumber' },
            { title: 'IMEI Number' },
            { title: 'Phone Number' },
            { title: 'Status', sort: 'status' },
            { title: 'Notes' },,
            { title: 'Actions' },
        ]}
        data={data}
        rows={[
            v => v.assetId,
            v => <UUIDCell value={v.id} />,            
            v => v.serialNumber,
            v => v.imei,
            v => v.phoneNumber,
            v => deviceStatusLabel(v.status),
            v => <NotesCell show={v.notes.length > 0} value={v.notes}></NotesCell>,
            v => [
                <ActionButton disabled={v.status != 'STATUS_PROVISIONED'} onClick={e => onDeprovision(v)} key={2}>Deprovision</ActionButton>
            ],
        ]}
        actions={
            <RaisedButton
                style={{ alignSelf: "flex-start" }}
                primary={true}
                onClick={e => onProvision()}
                label="Provision Device" />
        }
    />
)


import { connect } from 'react-redux'
import {
    listDevices,
    openDeviceModal,
    deactivateDevice,
} from '../../../ax-redux/actions/devices'

const mapStateToProps = (state) => {
    return {
        list: state.devices.list,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        listDevices: (params) => {
            dispatch(listDevices(params))
        },
        onSort: (name, params) => {
            if (!!!params.sort || params.sort == "-" + name) {
                params.sort = name
            } else {
                params.sort = "-" + name
            }
            dispatch(listDevices(params))
        },
    }
}
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Devices);


