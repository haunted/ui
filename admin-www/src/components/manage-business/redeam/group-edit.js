import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import TextField from 'material-ui/TextField';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {
    readAccount,
    listAccounts,
    updateAccount,
    listAccountPermissions,
    updateAccountPermissions

} from '../../../ax-redux/actions/accounts';
import {
    readGroup,
    createGroup,
    updateGroup,
    deleteGroup,
    listGroupPermissions,
    updateGroupPermissions,
    listAccountsByGroup,
    addAccountToGroup,
    removeAccountFromGroup
} from '../../../ax-redux/actions/groups';

class GroupEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        id: "",
        name: "",
        username: "",
        permissions: [

        ],
        defaultPermissions: [
            ["account", "*", "*", "*"],
            ["finance", "*", "*", "*"],
            ["group", "*", "*", "*"],
            ["reseller", "*", "*", "*"],
            ["supplier", "*", "*", "*"],
            ["user", "*", "*", "*"],
            ["ticket", "*", "*", "*"]
        ],
        validationMessage: ""
    }
  }

  componentWillMount () {
      if (this.props.mode == "Edit") {
          if (!this.props.fetching) {
              this.props.readGroup(this.props.params.id);
              this.props.listAccountsByGroup(this.props.params.id);
              this.props.listGroupPermissions(this.props.params.id);
              this.props.listAccounts();
          }
      }
  }

  shouldComponentUpdate(nextProps, nextState) {
    var key = null;
    if (this.props.mode == "Edit") {
        if ((this.props.group == null && nextProps.group != null) ||
            (this.state.id == "" && nextProps.group != null) ) {
            this.setState({
                id: nextProps.group.id,
                name: nextProps.group.name
            });
        }
        if ((this.props.permissions == null && nextProps.permissions != null) ||
            (this.parsePermissions(this.props.permissions).length != this.parsePermissions(nextProps.permissions).length) ||
            (this.props.fetchingPerms && !nextProps.fetchingPerms)) {
            this.setState({
                "permissions": this.parsePermissions(nextProps.permissions)
            })
        }
        if ((this.props.addingAccount == true && nextProps.addingAccount == false) ||
            (this.props.removingAccount == true && nextProps.removingAccount == false)) {
            this.props.listAccountsByGroup(this.props.params.id);
        }
    }
    // if (this.state.permissions.length != nextState.permissions.length) {
    //     this.props.listGroupPermissions(this.props.params.id);
    // }
    return true;
  }

  save () {
      if (this.props.mode == "Edit") {
          this.props.updateGroup(this.state);
      } else {
          this.props.createGroup(this.state);
      }
  }

  deleteGroup () {
      if (confirm("Confirm deleting group?")) {
          this.props.deleteGroup(this.props.group.id);
      }
  }

  addAccountToGroup () {
     let username = this.state.username,
            groupId = this.props.group.id,
            users = this.props.allAccounts,
            accountId = false;
    users.map( user => {
        if (user.username == username) {
            accountId = user.id;
        }
    })
    if (!!accountId ) {
        this.props.addAccountToGroup(groupId, accountId);
        this.setState({"validationMessage": ""})
    } else {
        this.setState({"validationMessage": "Error: Couldn't find user '"+username+"'"})
    }
    this.setState({"username": ""});
  }

  removeAccountFromGroup (id) {
      if (confirm("Confirm removing account from group?")) {
          this.props.removeAccountFromGroup(this.props.group.id, id)
      }
  }

  handlePermissionChange (e, set, value) {
      let data = this.state.permissions;
      data[set][value] = e.target.value;
      this.setState({
          permissions: data
      })
  }

  parsePermissions (data) {
      let output = [];
      Object.keys(data).map((perm, s)=>{
          let permissions = perm.split(".");
          output.push(permissions);
      })
      return output;
  }

  savePermissions () {
      let perms = null,
          data = {};
        if (this.state.permissions.length == 0 && (this.props.permissions == null || this.parsePermissions(this.props.permissions).length == 0)) {
            perms = this.state.defaultPermissions;
        } else {
            perms = this.state.permissions;
        }
        perms.map(set => {
            let p = set.join(".");
            data[p] = true;
        })
        this.props.updateGroupPermissions(this.props.group.id, data);
  }

  addPermission () {
      let permissions = this.state.permissions;
      permissions.push(["permission", "*", "*", "*"]);
      this.setState({
          "permissions": permissions
      })
  }

  removePermission (resource) {
      let permissions = this.state.permissions,
          p = 0;
      while (p < permissions.length) {
          if (permissions[p][0] == resource) {
              if (confirm("Confirm removing "+permissions[p][0]+" permissions?")) {
                  permissions.splice(p, 1);
                  this.setState({
                      permissions: permissions
                  })
                  //this.savePermissions();
              }
          }
          p ++;
      }
  }

  renderForm (data, mode) {
      let group = data;
      if (mode == "Add") {
          group = this.state;
      }
      if (group == null) {
          return;
      }
      return (
          <Table className="edit-table">
              <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                  <TableRow>
                      <TableHeaderColumn key="1">Property</TableHeaderColumn>
                      <TableHeaderColumn key="2">Value</TableHeaderColumn>
                  </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                  <TableRow key={"1"} selectable={false} style={{display: "none"}}>
                      <TableRowColumn key={"1"}>ID</TableRowColumn>
                      <TableRowColumn key={"2"}>
                          <TextField defaultValue={group.id} onChange={e => { this.setState({"id": e.target.value}) }} hintText="ID"/>
                      </TableRowColumn>
                  </TableRow>
                  <TableRow key={"2"} selectable={false}>
                      <TableRowColumn key={"1"}>Name</TableRowColumn>
                      <TableRowColumn key={"2"}>
                          <TextField defaultValue={group.name} onChange={e => { this.setState({"name": e.target.value}) }} hintText="Group Name"/>
                      </TableRowColumn>
                  </TableRow>
              </TableBody>
          </Table>
      )
  }

  renderUsers (accounts, fetching, mode) {
      return (
          <section className="group-users">
            <h2>Users</h2>
              {mode == "Edit" ? ([
              <Table key="edit-table" className="edit-table">
                <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                    <TableRow>
                        <TableHeaderColumn>Username</TableHeaderColumn>
                        <TableHeaderColumn>Roles</TableHeaderColumn>
                        <TableHeaderColumn>First Name</TableHeaderColumn>
                        <TableHeaderColumn>Last Name</TableHeaderColumn>
                        <TableHeaderColumn>Email</TableHeaderColumn>
                        <TableHeaderColumn>Phone</TableHeaderColumn>
                        <TableHeaderColumn></TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    { !!accounts && accounts.map((account, k)=>{
                        return (
                              <TableRow key={k} selectable={false}>
                                <TableRowColumn key={k+".1"}>{account.username}</TableRowColumn>
                                <TableRowColumn key={k+".2"}>{account.roles}</TableRowColumn>
                                <TableRowColumn key={k+".3"}>{account.givenName}</TableRowColumn>
                                <TableRowColumn key={k+".4"}>{account.surname}</TableRowColumn>
                                <TableRowColumn key={k+".5"}>{account.email}</TableRowColumn>
                                <TableRowColumn key={k+".6"}>{account.phone}</TableRowColumn>
                                <TableRowColumn key="remove">
                                    <RaisedButton className="delete-button" label="Remove" onClick={e => this.removeAccountFromGroup(account.id) } />
                                </TableRowColumn>
                              </TableRow>
                        )
                    })}
                </TableBody>
              </Table>,
              <div key="manage" className="manage">
                  { fetching ?
                      <div className="edit loading-circle">
                        <CircularProgress color={"#27367a"} size={96} />
                      </div>
                   : ""}
                  <RaisedButton className="save-button" label="Add User" onClick={e => {
                      this.addAccountToGroup();
                  }} />
                  <TextField defaultValue="" hintText={"Username"}
                          key={"add-user"} style={{paddingLeft: "0.5em"}}
                          onChange={e=>{ this.setState({username: e.target.value }); }}
                  />
              </div>
          ]) : ""}
          </section>
      )
  }

  renderPerms (perms, mode) {
      return (
          <section className="group-permissions">
              <h2>Permissions</h2>
              {(!!perms || mode == "Edit") ? (
              <Table className="edit-table" style={{marginBottom: "0.5em"}}>
                <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                    <TableRow>
                        <TableHeaderColumn>Resource</TableHeaderColumn>
                        <TableHeaderColumn>Entity</TableHeaderColumn>
                        <TableHeaderColumn>Field</TableHeaderColumn>
                        <TableHeaderColumn>Action</TableHeaderColumn>
                        <TableHeaderColumn></TableHeaderColumn>
                    </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                    {
                        perms.map((permissions, s)=>{
                            return (
                                <TableRow key={permissions[0]+"."+s} selectable={false}>
                                    {permissions.map((permission, p) => {
                                        return (
                                            <TableRowColumn key={p}>
                                                <TextField defaultValue={permission} hintText="Role"
                                                           key={p+".text"}
                                                           onBlur={e=>{ this.handlePermissionChange(e, s, p); }}
                                                />
                                            </TableRowColumn>
                                        )
                                    })}
                                    <TableRowColumn key="remove">
                                        <RaisedButton className="delete-button" label="Remove" onClick={e => this.removePermission(permissions[0])} />
                                    </TableRowColumn>
                                </TableRow>
                            )
                        })
                    }
                </TableBody>
              </Table>
          ) : (<span style={{marginRight:"1em"}}>No permissions associated with this group.</span>)}
          { mode == "Edit" ?
                [
                    <RaisedButton className="save-button" label={(perms.length > 0 ? "Update" : "Add")+ " Permissions"} onClick={e => this.savePermissions()} />,
                    <RaisedButton className="delete-button" label="Add Permission" onClick={e => this.addPermission() } />
                ] : ""
          }
          </section>
      )
  }

  renderError (err) {
      let msg = err.length > 0 ?
          <span className="form-error">
            {err}
          </span>
         : ""
      return msg;
  }

  render() {
      let mode = this.props.mode,
          fetching = this.props.fetching ||
                     this.props.fetchingPerms ||
                     this.props.removingAccount ||
                     this.props.addingAccount ||
                     this.props.deleteFetching ||
                     this.props.saving ||
                     this.props.savingPerms,
          fetchingAccounts = this.props.fetchingAllAccounts ||
                             this.props.fetchingAccounts;
    return (
        <div className="group-edit">
            <Card className="inner-edit">
                <h2>{this.props.mode+" "}Group</h2>
                { !fetching && this.renderError(this.state.validationMessage) }
                { !fetching ? this.renderForm(this.props.group, this.props.mode) :
                    <div className="edit loading-circle">
                      <CircularProgress color={"#27367a"} size={96} />
                    </div>
                }
                { !fetching && this.renderPerms(this.state.permissions, mode)}
                { !fetching && this.renderUsers(this.props.accounts, fetchingAccounts, mode) }
                { !fetching ? <RaisedButton className="save-button" label="Save Group" onClick={e => this.save()} /> : "" }
                { !fetching && this.props.mode == "Edit" ?
                    <RaisedButton className="delete-button" label="Delete Group" onClick={e => this.deleteGroup()} />
                    : ""
                }
            </Card>
        </div>
    );
  }
}

GroupEdit.propTypes = {

};

export default connect(
  state => {
    return {
        saving: state.groups.update.fetching,
        savingPerms: state.groups.updatePermissions.fetching,
        fetching: state.groups.read.fetching,
        fetchingAccounts: state.groups.listAccounts.fetching,
        fetchingAllAccounts: state.accounts.list.fetching,
        fetchingPerms: state.groups.listPermissions.fetching,
        deleteFetching: state.groups.remove.fetching,
        removingAccount: state.groups.removeAccount.fetching,
        addingAccount: state.groups.addAccount.fetching,
        accounts: state.groups.listAccounts.data,
        allAccounts: state.accounts.list.data,
        permissions: state.groups.listPermissions.data,
        group: state.groups.read.current,
        mode: (state.routing.locationBeforeTransitions.pathname.search("/add") > -1 ? "Add" : "Edit")
    }
  },
  dispatch => {
    return {
        listAccounts: () => {
            dispatch(listAccounts())
        },
        readGroup: (id) => {
            dispatch(readGroup(id))
        },
        listGroupPermissions: (id) => {
            dispatch(listGroupPermissions(id))
        },
        listAccountsByGroup: (id) => {
            dispatch(listAccountsByGroup(id))
        },
        addPermission: (data) => {
                dispatch()
        },
        addUser: (data) => {
                dispatch()
        },
        createGroup: (data) => {
            let group = {
                name: data.name
            }
            dispatch(createGroup(group))
        },
        deleteGroup: (id) => {
            dispatch(deleteGroup(id))
        },
        updateGroup: (data) => {
            let group = {
                name: data.name
            }
            dispatch(updateGroup(data.id, group))
        },
        updateGroupPermissions: (id, data) => {
                dispatch(updateGroupPermissions(id, data))
        },
        addAccountToGroup: (group, account) => {
            dispatch(addAccountToGroup(group, account))
        },
        removeAccountFromGroup: (group, account) => {
                dispatch(removeAccountFromGroup(group, account))
        }
    }
  }
)(GroupEdit)
