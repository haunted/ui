import React, { Component, PropTypes } from 'react'
import FlatButton from 'material-ui/FlatButton'
import Dialog from '../../standard/dialog'
import DeviceForm from './device-form'

class DeviceModalCreate extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            device: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open && !this.props.open) {
            this.resetForm()
            this.props.getNextAssetId()
        }

        if (nextProps.savedDevice.version != this.props.savedDevice.version) {
            if (!!!nextProps.savedDevice.error) {
                this.close(true)
            } else {
                this.setError(nextProps.savedDevice.error)
            }
        }
    }

    resetForm(device = {}) {
        this.setState({ device, error: false })
    }

    setError(err) {
        this.setState({ error: err })
    }

    close(saved) {
        this.setState({ device: {} })
        this.props.onUpdated(saved)
    }

    save() {
        this.props.createDevice(this.state.device)
    }

    render() {
        const { deviceId, nextAssetId } = this.props
        const { device } = this.state

        const actions = [
            <FlatButton
                label="Cancel"
                primary={false}
                onClick={e => this.close(false)}
            />,
            <FlatButton
                label={'Create Device'}
                primary={true}
                onClick={e => this.save()}
            />
        ];
        return (
            <Dialog
                fetching={this.props.fetching}
                title={`Create Device ${!!device.assetId ? device.assetId : ""}`}
                actions={actions}
                modal={true}
                open={this.props.open}
                autoScrollBodyContent={true}
            >
                <DeviceForm
                    device={device}
                    nextAssetId={nextAssetId}
                    onChange={v => this.setState({ device: v })}
                />

            </Dialog>

        )
    }
}

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as deviceActions from '../../../ax-redux/actions/devices'

export default connect(
    (state, ownProps) => ({
        device: state.devices.get,
        nextAssetId: state.devices.nextAssetId.data.nextAssetIdIncrement,
        fetching: state.devices.get.fetching || state.devices.create.fetching || state.devices.nextAssetId.fetching,
        savedDevice: state.devices.create,
    }),
    dispatch => bindActionCreators(deviceActions, dispatch),
)(DeviceModalCreate)

