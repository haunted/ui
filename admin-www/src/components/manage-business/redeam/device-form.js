import React, { Component, PropTypes } from 'react'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'

import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import RemoveIcon from 'material-ui/svg-icons/content/remove';
import AddIcon from 'material-ui/svg-icons/content/add';

const DeviceForm = ({ device, updateMode=false, onChange, error = "", fieldError = {}, nextAssetId }) => (
    <form>
        {
            error ? <span>{error}</span> : ""
        }<br />

        <DeviceAssetIDInput
            value={device.assetId}
            nextAssetId={nextAssetId}
            onChange={v => {
                onChange(Object.assign(device, { assetId: v }))
            }}
        />

        <TextField
            hintText="Serial Number"
            floatingLabelText="Serial Number"
            disabled={updateMode}
            floatingLabelFixed={true}
            value={device.serialNumber}
            onChange={(e, v) => onChange(Object.assign(device, { serialNumber: v }))}
        /><br />

        <TextField
            hintText="IMEI Number"
            floatingLabelText="IMEI Number"
            disabled={updateMode}
            floatingLabelFixed={true}
            value={device.imei}
            onChange={(e, v) => onChange(Object.assign(device, { imei: v }))}
        /><br />

        <TextField
            hintText="SIM Number"
            floatingLabelText="SIM Number"
            floatingLabelFixed={true}
            value={device.simNumber}
            onChange={(e, v) => onChange(Object.assign(device, { simNumber: v }))}
        /><br />

        <TextField
            hintText="Phone Number"
            floatingLabelText="Phone Number"
            floatingLabelFixed={true}
            value={device.phoneNumber}
            onChange={(e, v) => onChange(Object.assign(device, { phoneNumber: v }))}
        /><br />

        <TextField
            hintText="Carrier"
            floatingLabelText="Carrier"
            floatingLabelFixed={true}
            value={device.carrier}
            onChange={(e, v) => onChange(Object.assign(device, { carrier: v }))}
        /><br />
    </form>
)
export default DeviceForm

const assetTypes = {
    AXLG: 'LG Tablet',
    RDHH: 'Redeam Handheld',
}

export const DeviceAssetIDInput = ({ value = "", onChange, nextAssetId, errorText }) => {
    let prefix = ""
    let suffix = ""    
    if (value.length >= 4) {
        prefix = value.substring(0,4)
        suffix = value.substring(4)
    }
    if (suffix == "" && prefix == "") {
        suffix = nextAssetId
    }

    return <div style={{ display: 'flex' }}>
        <SelectField
            floatingLabelFixed={true}
            floatingLabelText="Device Type"
            value={prefix}
            style={{ width: '7rem' }}
            menuStyle={{ width: '15rem' }}
            errorText={errorText}
            labelStyle={{ whitespace: 'nowrap' }}
            onChange={(e, i, v) => onChange(`${v}${suffix}`)}>
            {
                Object.keys(assetTypes).map(
                    v => <MenuItem
                        value={v}
                        label={v}
                        primaryText={assetTypes[v]}
                    />
                )
            }
        </SelectField>

        <TextField
            style={{ width: '5rem' }}
            hintText="Asset ID"
            floatingLabelText="Asset ID"
            floatingLabelFixed={true}
            errorText={errorText ? " " : ""}
            disabled={value.length < 4}
            value={suffix}
            onChange={(e, v) => onChange(`${prefix}${v}`)}
        />
    </div>
}
