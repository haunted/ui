import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import Toggle from 'material-ui/Toggle'
import DatePicker from 'material-ui/DatePicker'
import { browserHistory } from 'react-router'
import RaisedButton from 'material-ui/RaisedButton'
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from '../../reports/export-button'
import PrintButton from '../../reports/print-button'
import SuppliersPopover from '../../reports/suppliers-popover'
import AutoComplete from 'material-ui/AutoComplete'
import LightBox from '../../reports/lightbox'
import moment from 'moment-timezone'
import ResellersPopover from '../../reports/resellers-popover'
import getInclusiveRange from '../../reports/get-inclusive-range'
import validateDateRange from '../../reports/validate-date-range'
import { formatStatusEnum } from '../../../util'
import { ACTOUREX_TECHNICAL_STAFF } from '../../../ax-redux/constants'


let self = null;
const dataSourceConfig = {
  text: 'text',
  value: 'value',
};

class GlobalTransactionSearch extends Component {
  constructor(props) {
    super(props)
    let customerName = "",
        tourName = "",
        referenceNum = "",
        issuedStart = "",
        issuedEnd = "",
        ticketedStart = "",
        ticketedEnd = "",
        redeemedStart = "",
        redeemedEnd = "",
        supplierCode = "",
        resellerCode = "",
        dateRange = [],
        limit = 100,
        offset = 0;

    console.log("query", this.props.location.query);
    console.log("supplierCode", supplierCode);

    if (!!this.props.location.query) {
      if (!!this.props.location.query.offset) {
        offset = this.props.location.query.offset;
      }
      if (!!this.props.location.query.supplierCode) {
          supplierCode = this.props.location.query.supplierCode;
      }
      if (!!this.props.location.query.resellerCode) {
          resellerCode = this.props.location.query.resellerCode;
      }
      if (!!this.props.location.query.customerName) {
        customerName = this.props.location.query.customerName;
      }
      if (!!this.props.location.query.tourName) {
        tourName = this.props.location.query.tourName;
      }
      if (!!this.props.location.query.referenceNum) {
        referenceNum = this.props.location.query.referenceNum;
      }
      if (!!this.props.location.query.issuedStart) {
        issuedStart = new Date(this.props.location.query.issuedStart * 1000);
      }
      if (!!this.props.location.query.issuedEnd) {
        issuedEnd = new Date(this.props.location.query.issuedEnd * 1000);
      }
      if (!!this.props.location.query.ticketedStart) {
        ticketedStart = new Date(this.props.location.query.ticketedStart * 1000);
      }
      if (!!this.props.location.query.ticketedEnd) {
        ticketedEnd = new Date(this.props.location.query.ticketedEnd * 1000);
      }
      if (!!this.props.location.query.redeemedStart) {
        redeemedStart = new Date(this.props.location.query.redeemedStart * 1000);
      }
      if (!!this.props.location.query.redeemedEnd) {
        redeemedEnd = new Date(this.props.location.query.redeemedEnd * 1000);
      }
    }

    if (this.isSupplierStaff()) {
      supplierCode = this.props.orgCode;
    } else if (this.isResellerStaff()) {
      resellerCode = this.props.orgCode;
    }

    this.state = {
        supplierCode,
        resellerCode,
        limit,
        offset,
        customerName,
        tourName,
        referenceNum,
        issuedStart,
        issuedEnd,
        ticketedStart,
        ticketedEnd,
        redeemedStart,
        redeemedEnd,
        resellersByName: [],
        resellersByCode: [],
        suppliersByName: [],
        suppliersByCode: [],
        productsByName: [],
        productsByCode: [],
        customersByName: [],
        refNumDataSource: []
    }
    self = this;
  }

  componentWillMount() {
    if (this.props.resellers == false && !this.props.fetchingResellers) {
        this.props.listResellers();
    } else if (!this.props.fetchingResellers) {
        this.generateResellerAutoComplete(this.props.resellers);
    }
    if (this.props.suppliers == false && !this.props.fetchingSuppliers) {
        this.props.listSuppliers();
    } else if (!this.props.fetchingSuppliers) {
        this.generateSupplierAutoComplete(this.props.suppliers);
    }
    if ((this.props.products == false && !this.props.fetchingProducts) ||
        this.state.supplierCode != this.props.location.query.supplierCode) {
        if (this.props.location.query.supplierCode != undefined) {
            let newSupplierCode = this.isSupplierStaff() ? this.props.orgCode : this.props.location.query.supplierCode;
            this.setState({
              supplierCode: newSupplierCode
            })
            this.props.getSupplierProducts(newSupplierCode);
            console.log("getSupplierProducts", newSupplierCode);
        }
    } else if (!this.props.fetchingProducts) {
        this.generateProductAutoComplete(this.props.products);
    }

    this.props.search({
      limit: this.state.limit,
      offset: this.state.offset,
      resellerCode: this.state.resellerCode,
      supplierCode: this.state.supplierCode,
    })

  }


  componentWillUpdate(nextProps, nextState) {
    if (this.state.issuedStart != nextState.issuedStart ||
        this.state.issuedEnd != nextState.issuedEnd ||
        this.state.ticketedStart != nextState.ticketedStart ||
        this.state.ticketedEnd != nextState.ticketedEnd ||
        this.state.redeemedStart != nextState.redeemedStart ||
        this.state.redeemedEnd != nextState.redeemedEnd ||
        this.state.supplierCode != nextState.supplierCode ||
        this.state.resellerCode != nextState.resellerCode ||
        this.state.resellerName != nextState.resellerName ||
        this.state.tourName != nextState.tourName ||
        this.state.customerName != nextState.customerName ||
        this.state.referenceNum != nextState.referenceNum
      ) {
        let supplierCode = (nextState.supplierCode !="" ?("supplierCode="+nextState.supplierCode+"&"):""),
            resellerCode = (nextState.resellerCode !="" ?("resellerCode="+nextState.resellerCode+"&"):""),
            customerName = (nextState.customerName !="" ?("customerName="+nextState.customerName+"&"):""),
            tourName = (nextState.tourName !="" ?("tourName="+nextState.tourName+"&"):""),
            referenceNum = (nextState.referenceNum != "" ? ("referenceNum="+nextState.referenceNum+"&"):""),
            issuedStart = (nextState.issuedStart !="" ?("issuedStart="+moment(nextState.issuedStart).tz(TZ).unix()+"&"):""),
            issuedEnd = (nextState.issuedEnd !="" ?("issuedEnd="+moment(nextState.issuedEnd).tz(TZ).unix()+"&"):""),
            ticketedStart = (nextState.ticketedStart !="" ?("ticketedStart="+moment(nextState.ticketedStart).tz(TZ).unix()+"&"):""),
            ticketedEnd = (nextState.ticketedEnd !="" ?("ticketedEnd="+moment(nextState.ticketedEnd).tz(TZ).unix()+"&"):""),
            redeemedStart = (nextState.redeemedStart !="" ?("redeemedStart="+moment(nextState.redeemedStart).tz(TZ).unix()+"&"):""),
            redeemedEnd = (nextState.redeemedEnd !="" ?("redeemedEnd="+moment(nextState.redeemedEnd).tz(TZ).unix()+"&"):""),
            offset = nextState.offset;

            if (this.state.supplierCode != nextState.supplierCode || //reset pagination
                this.state.resellerCode != nextState.resellerCode ||
                this.state.tourName != nextState.tourName ||
                this.state.customerName != nextState.customerName ||
                this.state.referenceNum != nextState.referenceNum) {
              offset = 0
              this.setState({
                offset
              })
            }
        browserHistory.push(`/app/redeam/search?${supplierCode}${resellerCode}${customerName}${tourName}${referenceNum}${issuedStart}${issuedEnd}${ticketedStart}${ticketedEnd}${redeemedStart}${redeemedEnd}offset=${offset}&limit=${nextState.limit}`);
    }
    if (this.props.pathname != nextProps.pathname ||
        this.props.queryParams != nextProps.queryParams) {
        if (!!!nextProps.children || nextProps.children.length == 0) { // if summary mode / not detail view
            let reqSupplierCode = nextState.supplierCode, // locking down url params for non tech-staff
                reqResellerCode = nextState.resellerCode;

            if (this.isSupplierStaff()) {
              if (nextState.supplierCode != nextProps.orgCode) {
                reqSupplierCode = nextProps.orgCode;
              }
            } else if (this.isResellerStaff()) {
              if (nextState.supplierCode != nextProps.orgCode) {
                reqSupplierCode = nextProps.orgCode;
              }
            }

            let requestParams = {
                limit: nextState.limit,
                offset: nextState.offset,
                supplierCode: reqSupplierCode,
                resellerCode: reqResellerCode,
                customerName: nextState.customerName,
                tourName: nextState.tourName,
                referenceNum: nextState.referenceNum
            };

            if (nextState.issuedStart !="") {
              requestParams.issuedStart = moment(nextState.issuedStart).tz(TZ).unix();
            }
            if (nextState.issuedEnd !="") {
              requestParams.issuedEnd = moment(nextState.issuedEnd).tz(TZ).unix();
            }
            if (nextState.ticketedStart !="") {
              requestParams.ticketedStart = moment(nextState.ticketedStart).tz(TZ).unix();
            }
            if (nextState.ticketedEnd !="") {
              requestParams.ticketedEnd = moment(nextState.ticketedEnd).tz(TZ).unix();
            }
            if (nextState.redeemedStart !="") {
              requestParams.redeemedStart = moment(nextState.redeemedStart).tz(TZ).unix();
            }
            if (nextState.redeemedEnd !="") {
              requestParams.redeemedEnd = moment(nextState.redeemedEnd).tz(TZ).unix();
            }

            if (nextProps.location.query) {
              if (nextProps.location.query.offset) {
                  requestParams.offset = nextProps.location.query.offset;
              }
              if (nextProps.location.query.limit) {
                requestParams.limit = nextProps.location.query.limit;
              }
            }
            this.props.search(requestParams)
        }
    }
    if (this.props.resellers == false && nextProps.resellers != false) {
        this.generateResellerAutoComplete(nextProps.resellers);
    }
    if (this.props.suppliers == false && nextProps.suppliers != false) {
        this.generateSupplierAutoComplete(nextProps.suppliers);
    }
    if (this.props.products == false && nextProps.products !== false){
        this.generateProductAutoComplete(nextProps.products);
    }
    if (this.props.location.query.supplierCode != nextProps.location.query.supplierCode) {
        this.props.getSupplierProducts(nextProps.location.query.supplierCode);
    }
  }
  generateSupplierAutoComplete (suppliers) {
      let byName = [],
          byCode = [];
      suppliers.map((supplier) =>{
          byCode.push({
              text: supplier.supplier.code,
              value: supplier.supplier
          });
          byName.push({
              text: supplier.supplier.name,
              value: supplier.supplier
          });
      })
      this.setState({
        suppliersByName: byName,
        suppliersByCode: byCode
      })
  }
  generateResellerAutoComplete (resellers) {
      let byName = [],
          byCode = [];
      resellers.map((reseller) =>{
          byCode.push({
              text: reseller.code,
              value: reseller
          });
          byName.push({
              text: reseller.name,
              value: reseller
          });
          if (!!reseller.aliases) {
            reseller.aliases.forEach(alias=>{
              byName.push({
                  text: alias,
                  value: reseller
              });
            })
          }
      })
      this.setState({
          resellersByName: byName,
          resellersByCode: byCode
      })
  }
  generateProductAutoComplete (products) {
      let byName = [],
          byCode = [];
      products.map((product) =>{
          byCode.push({
              text: product.code,
              value: product
          });
          byName.push({
              text: product.name,
              value: product
          });
      })
      this.setState({
          productsByName: byName,
          productsByCode: byCode
      })
  }

  setStartDate(date) {
      this.setState({
        start: date
      })
  }
  setEndDate(date) {
      this.setState({
        end: date
      })
  }
  setSupplier(supplier) {
      console.log(supplier)
      this.setState({ supplierCode: supplier.supplier.code })
  }
  setReseller(reseller) {
      console.log(reseller)
      this.setState({ resellerCode: reseller.code })
  }
  nextPage (direction) {
    let limit = this.state.limit,
        offset = this.state.offset,
        supplierCode = (this.state.supplierCode !="" ?("supplierCode="+this.state.supplierCode+"&"):""),
        resellerCode = (this.state.resellerCode !="" ?("resellerCode="+this.state.resellerCode+"&"):""),
        customerName = (this.state.customerName !="" ?("customerName="+this.state.customerName+"&"):""),
        tourName = (this.state.tourName !="" ?("tourName="+this.state.tourName+"&"):""),
        referenceNum = (this.state.referenceNum !="" ?("referenceNum="+this.state.referenceNum+"&"):""),
        issuedStart = (this.state.issuedStart !="" ?("issuedStart="+moment(this.state.issuedStart).tz(TZ).unix()+"&"):""),
        issuedEnd = (this.state.issuedEnd !="" ?("issuedEnd="+moment(this.state.issuedEnd).tz(TZ).unix()+"&"):""),
        ticketedStart = (this.state.ticketedStart !="" ?("ticketedStart="+moment(this.state.ticketedStart).tz(TZ).unix()+"&"):""),
        ticketedEnd = (this.state.ticketedEnd !="" ?("ticketedEnd="+moment(this.state.ticketedEnd).tz(TZ).unix()+"&"):""),
        redeemedStart = (this.state.redeemedStart !="" ?("redeemedStart="+moment(this.state.redeemedStart).tz(TZ).unix()+"&"):""),
        redeemedEnd = (this.state.redeemedEnd !="" ?("redeemedEnd="+moment(this.state.redeemedEnd).tz(TZ).unix()+"&"):"");

    if (direction > 0 || offset > 0) {
      offset = (parseInt(offset)+(parseInt(limit) * direction));
    }
    this.setState({offset: offset});
    browserHistory.push(`/app/redeam/search?${supplierCode}${resellerCode}${customerName}${tourName}${referenceNum}${issuedStart}${issuedEnd}${ticketedStart}${ticketedEnd}${redeemedStart}${redeemedEnd}offset=${offset}&limit=${limit}`);
}

  export(format) {
    this.props.export({
      start: this.state.start,
      end: this.state.end,
      resellerCode: this.state.resellerCode,
      supplierCode: this.state.supplierCode
    }, format)
  }
  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }
  setDate(type, startEnd, date) {
    switch (type) {
      case "issued":
        if (startEnd=="start") {
          this.setState({issuedStart: date});
        } else {
          this.setState({issuedEnd: date});
        }
      break;
      case "ticketed":
        if (startEnd=="start") {
          this.setState({ticketedStart: date});
        } else {
          this.setState({ticketedEnd: date});
        }
      break;
      case "redeemed":
        if (startEnd=="start") {
          this.setState({redeemedStart: date});
        } else {
          this.setState({redeemedEnd: date});
        }
      break;
    }
  }
  nameFilter (name) {
      let output = "";
      output = name.replace('given:"', '');
      output = output.replace('" family:"', ' ');
      output = output.replace('"', '');
      return output;
  }
  shouldDisableIssuedStartDate (day) {
    return  !validateDateRange(day.getTime(), self.state.issuedEnd);
  }
  shouldDisableIssuedEndDate (day) {
     return  !validateDateRange(self.state.issuedStart, day.getTime());
  }
  shouldDisableTicketedStartDate (day) {
    return  !validateDateRange(day.getTime(), self.state.ticketedEnd);
  }
  shouldDisableTicketedEndDate (day) {
     return  !validateDateRange(self.state.ticketedStart, day.getTime());
  }
  shouldDisableRedeemedStartDate (day) {
    return  !validateDateRange(day.getTime(), self.state.redeemedEnd);
  }
  shouldDisableRedeemedEndDate (day) {
     return  !validateDateRange(self.state.redeemedStart, day.getTime());
  }
  isSupplierStaff () {
    return this.props.role.indexOf("SUPPLIER") > -1;
  }
  isResellerStaff () {
    return this.props.role.indexOf("RESELLER") > -1;
  }
  parseStatus (code) {
      if (typeof code != 'undefined') {
          return code.replace("STATUS_", "")
      } else {
          return ""
      }
  }
  onRowTap (e, v, voucher) {
      e.preventDefault();
      let start = new Date(),
          end = new Date(),
          offset = this.state.offset,
          limit = this.state.limit,
          supplierCode = v.supplierCode || this.state.supplierCode,
          resellerCode = v.resellerCode || this.state.resellerCode;

      if (e.target.tagName != "DIV" && e.target.tagName != "SPAN" && e.target.tagname != "BUTTON") {
        if (voucher) {
          browserHistory.push(`/app/reports/vouchers/${supplierCode}${(resellerCode != "" ? ("/"+resellerCode) : "")}/detail/${v.id}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}`)
        } else {
          browserHistory.push(`/app/reports/arrivals/${supplierCode}/${v.id}`)
        }
    }
  }
  displayDefaultDate (date) {
    if (date === "" || (typeof date != "object" && isNaN(date))) {
      date = new Date();
    } else {
      if (typeof date == "string" && parseInt(date) < 10000000000) {
        date = new Date(parseInt(date)*1000);
      }
    }
    return date;
  }
  render() {
      let summaryMode = (this.props.children == null || this.props.children.length < 1);
    return (
      <div className="global-search">
        <div className="exceptions-header" style={{display: (summaryMode ? "inherit" : "none")}}>
            <div className="exceptions-header-left" style={{marginLeft: "1em"}}>
                <h2 style={{color:"#434343"}}>
                    Transaction Search
                </h2>
                <PrintButton
                   style={{marginLeft: "1em"}}
                   onPrint={data=> { this.print() }} printMode={this.props.printMode}
                />
            </div>
            <div className="exceptions-header-right" style={{marginRight: "1em"}}>
                <div className="no-print detail-view-controls" style={{display: this.props.printMode ? "none" : "inherit"}}>
                                      <div className="date-picker-group">
                                        <h3>Issued</h3>
                                        <DatePicker
                                            floatingLabelText="Issued Start Date"
                                            id="issued-start-date"
                                            hintText="Date"
                                            className="date-picker"
                                            formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                                            value={this.displayDefaultDate(this.state.issuedStart)}
                                            shouldDisableDate={this.shouldDisableIssuedStartDate}
                                            onChange={(ev, date) => this.setDate("issued", "start", date)}
                                         />
                                         <DatePicker
                                              floatingLabelText="Issued End Date"
                                              style={{marginRight: "1em"}}
                                              hintText="Date"
                                              id="issued-end-date"
                                              className="date-picker"
                                              formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                                              value={this.displayDefaultDate(this.state.issuedEnd)}
                                              shouldDisableDate={this.shouldDisableIssuedEndDate}
                                              onChange={(ev, date) => this.setDate("issued", "end", date)}
                                        />
                                      </div>
                                      <div className="date-picker-group">
                                        <h3>Ticketed</h3>
                                        <DatePicker
                                          floatingLabelText="Ticketed Start Date"
                                          hintText="Date"
                                          id="ticketed-start-date"
                                          className="date-picker"
                                          formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                                          value={this.displayDefaultDate(this.state.ticketedStart)}
                                          shouldDisableDate={this.shouldDisableTicketedStartDate}
                                          onChange={(ev, date) => this.setDate("ticketed", "start", date)}
                                         />
                                         <DatePicker
                                              floatingLabelText="Ticketed End Date"
                                              style={{marginRight: "1em"}}
                                              hintText="Date"
                                              id="ticketed-end-date"
                                              className="date-picker"
                                              formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                                              value={this.displayDefaultDate(this.state.ticketedEnd)}
                                              shouldDisableDate={this.shouldDisableTicketedEndDate}
                                              onChange={(ev, date) => this.setDate("ticketed", "end", date)}
                                        />
                                      </div>
                                      <div className="date-picker-group">
                                        <h3>Redeemed</h3>
                                        <DatePicker
                                            floatingLabelText="Redeemed Start Date"
                                            hintText="Date"
                                            id="redeemed-start-date"
                                            className="date-picker"
                                            formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                                            value={this.displayDefaultDate(this.state.redeemedStart)}
                                            shouldDisableDate={this.shouldDisableRedeemedStartDate}
                                            onChange={(ev, date) => this.setDate("redeemed", "start", date)}
                                         />
                                         <DatePicker
                                              floatingLabelText="Redeemed End Date"
                                              style={{marginRight: "1em"}}
                                              hintText="Date"
                                              id="redeemed-end-date"
                                              className="date-picker"
                                              formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                                              value={this.displayDefaultDate(this.state.redeemedEnd)}
                                              shouldDisableDate={this.shouldDisableRedeemedEndDate}
                                              onChange={(ev, date) => this.setDate("redeemed", "end", date)}
                                        />
                                      </div>
                                      <div className="search-auto-complete-group">
                                        { this.isSupplierStaff() == false ? (
                                          <div className="search-auto-complete" >
                                            <AutoComplete
                                               hintText="Supplier Code"
                                               searchText={this.state.supplierCode}
                                               filter={AutoComplete.fuzzyFilter}
                                               key="supplier-name-field" id="supplier-name-field"
                                               openOnFocus={true}
                                               fullWidth={true}
                                               dataSource={this.state.suppliersByName}
                                               dataSourceConfig={dataSourceConfig}
                                               onNewRequest={(chosenRequest, index) => {
                                                  console.log("onNewRequest", chosenRequest);
                                                  let supplierCode = "";
                                                  if (typeof chosenRequest == "string") {
                                                    supplierCode = chosenRequest;
                                                  } else {
                                                    supplierCode = chosenRequest.value.code;
                                                  }
                                                  this.setState({
                                                      supplierCode: supplierCode
                                                  })
                                               }}
                                               onUpdateInput={(searchText, dataSource) => {}}
                                               onBlur={e => {
                                                   this.setState({
                                                       "supplierCode": e.target.value
                                                   })
                                               }}
                                             />
                                          </div>
                                        ) : ""}
                                        { this.isResellerStaff() == false ? (
                                          <div className="search-auto-complete">
                                            <AutoComplete
                                               hintText="Reseller Code"
                                               searchText={this.state.resellerCode}
                                               filter={AutoComplete.fuzzyFilter}
                                               key="reseller-name-field" id="reseller-name-field"
                                               openOnFocus={true}
                                               fullWidth={true}
                                               dataSource={this.state.resellersByName}
                                               dataSourceConfig={dataSourceConfig}
                                               onNewRequest={(chosenRequest, index) => {
                                                  console.log("onNewRequest", chosenRequest);
                                                  let resellerCode = "";
                                                  if (typeof chosenRequest == "string") {
                                                    resellerCode = chosenRequest;
                                                  } else {
                                                    resellerCode = chosenRequest.value.code;
                                                  }
                                                  this.setState({
                                                      resellerCode: resellerCode
                                                  })
                                               }}
                                               onUpdateInput={(searchText, dataSource) => {}}
                                               onBlur={e => {
                                                   this.setState({
                                                       "resellerCode": e.target.value
                                                   })
                                               }}
                                             />
                                          </div>
                                        ) : ""}
                                        <div className="search-auto-complete">
                                          <AutoComplete
                                             hintText="Tour Name"
                                             searchText={this.state.tourName}
                                             filter={AutoComplete.fuzzyFilter}
                                             key="product-name-field" id="product-name-field"
                                             openOnFocus={true}
                                             fullWidth={true}
                                             dataSource={this.state.productsByName}
                                             dataSourceConfig={dataSourceConfig}
                                             onNewRequest={(chosenRequest, index) => {
                                                 this.setState({
                                                    tourName: chosenRequest.value.name
                                                 })
                                             }}
                                             onUpdateInput={(searchText, dataSource) => {}}
                                             onBlur={e => {
                                                 this.setState({
                                                     tourName: e.target.value
                                                 })
                                             }}
                                           />
                                      </div>
                                      <div className="search-auto-complete">
                                        <AutoComplete
                                           hintText="Customer Name"
                                           searchText={this.state.customerName}
                                           filter={AutoComplete.fuzzyFilter}
                                           key="customer-name-field" id="customer-name-field"
                                           openOnFocus={true}
                                           fullWidth={true}
                                           dataSource={this.state.customersByName}
                                           dataSourceConfig={dataSourceConfig}
                                           onNewRequest={(chosenRequest, index) => {
                                               this.setState({
                                                  customerName: chosenRequest.value.name
                                               })
                                           }}
                                           onUpdateInput={(searchText, dataSource) => {}}
                                           onBlur={e => {
                                               this.setState({
                                                   customerName: e.target.value
                                               })
                                           }}
                                         />
                                    </div>
                                    <div className="search-auto-complete">
                                      <AutoComplete
                                         hintText="Reference Number"
                                         searchText={this.state.referenceNum}
                                         filter={AutoComplete.fuzzyFilter}
                                         key="reference-num-field" id="reference-num-field"
                                         openOnFocus={true}
                                         fullWidth={true}
                                         dataSource={this.state.refNumDataSource}
                                         dataSourceConfig={dataSourceConfig}
                                         onNewRequest={(chosenRequest, index) => {
                                             this.setState({
                                                referenceNum: chosenRequest.value.name
                                             })
                                         }}
                                         onUpdateInput={(searchText, dataSource) => {}}
                                         onBlur={e => {
                                             this.setState({
                                                 referenceNum: e.target.value
                                             })
                                         }}
                                       />
                                  </div>
                                      </div>
              </div>
            </div>
      </div>
      <div className="report-details">
          {this.props.children}
      </div>
         {
            this.props.children == null || this.props.children.length < 1  ? (
               this.props.fetchingResults
               ? <CircularProgress className="search-circular-progress" color={"#27367a"} size={96} /> :
               (
                 <section className="inner-report">
                     <Table className="detail-tabulation index">
                         <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                            <TableRow>
                              <TableHeaderColumn className="medium-narrow-column left-padded">Ticket ID</TableHeaderColumn>
                              <TableHeaderColumn className="medium-column">Supplier ID</TableHeaderColumn>
                              <TableHeaderColumn className="medium-column">Supplier Name</TableHeaderColumn>
                              <TableHeaderColumn className="medium-column">Reseller ID</TableHeaderColumn>
                              <TableHeaderColumn className="medium-column">Reseller Name</TableHeaderColumn>
                              <TableHeaderColumn className="narrow-column">Supplier Product ID</TableHeaderColumn>
                              <TableHeaderColumn className="medium-column">Tour Name</TableHeaderColumn>
                              <TableHeaderColumn className="narrow-column">Issued Date</TableHeaderColumn>
                              <TableHeaderColumn className="narrow-column">Ticketed Date</TableHeaderColumn>
                              <TableHeaderColumn className="narrow-column">Status</TableHeaderColumn>
                              <TableHeaderColumn className="narrow-column">Redeemed Date</TableHeaderColumn>
                            </TableRow>
                          </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        {
                          this.props.searchResults && this.props.searchResults.tickets && this.props.searchResults.tickets.map((v,i) =>
                              {
                                let travelDate = "–";
                                if (!!v.travelDate && v.travelDate != "0") {
                                    travelDate = moment.unix(v.travelDate).tz(TZ).format("DD-MMM-YYYY");
                                }
                                  return (
                                      <TableRow key={i} onTouchTap={e => { this.onRowTap(e, v, false) }}  >
            								              <TableRowColumn className="medium-narrow-column leftPaddedColumn small-text-print-only hide-overflow-print-only" title={v.id}>
                                            {v.id}
                                          </TableRowColumn>
                                          <TableRowColumn className="extra-narrow-column small-text-print-only show-overflow-print" title={v.supplierCode}>
                                            {v.supplierCode}
                                          </TableRowColumn>
                                          <TableRowColumn className="medium-column small-text-print-only" title={v.supplierName}>
                                            {v.supplierName}
                                          </TableRowColumn>
                                          <TableRowColumn className="narrow-column small-text-print-only" title={v.resellerCode}>
                                            {v.resellerCode}
                                          </TableRowColumn>
                                          <TableRowColumn className="medium-narrow-column small-text-print-only" title={v.resellerName}>
                                            {v.resellerName}
                                          </TableRowColumn>
                                          <TableRowColumn className="narrow-column small-text-print-only">
                                            {v.supplierProductId}
                                          </TableRowColumn>
                                          <TableRowColumn className="extra-narrow-column small-text-print-only show-overflow-print" title={v.productName}>
                                            {v.productName}
                                          </TableRowColumn>
                                          <TableRowColumn className="narrow-column small-text-print-only">
                                            {v.issuedDate}
                                          </TableRowColumn>
                                          <TableRowColumn className="narrow-column small-text-print-only">
                                            {v.ticketedDate}
                                          </TableRowColumn>
                                          <TableRowColumn className="narrow-column small-text-print-only" title={formatStatusEnum(v.status)}>
                                            {formatStatusEnum(v.status)}
                                          </TableRowColumn>
                                          <TableRowColumn className="narrow-column small-text-print-only" title={travelDate}>
                                            {travelDate}
                                          </TableRowColumn>
                        							</TableRow>
                                  )
                              }
                          )
                        }
                      </TableBody>
                    </Table>
                    <Table className="detail-tabulation index" style={{marginTop: "1em", marginBottom: "0em"}}>
                        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                           <TableRow>
                             <TableHeaderColumn >Voucher ID</TableHeaderColumn>
                             <TableHeaderColumn >Lead Traveler</TableHeaderColumn>
                             <TableHeaderColumn >Product Name</TableHeaderColumn>
                             <TableHeaderColumn >Reseller Code</TableHeaderColumn>
                             <TableHeaderColumn >Reseller Name</TableHeaderColumn>
                             <TableHeaderColumn >Supplier Code</TableHeaderColumn>
                             <TableHeaderColumn >Supplier Name</TableHeaderColumn>
                             <TableHeaderColumn >Status</TableHeaderColumn>
                             <TableHeaderColumn >Travel Date</TableHeaderColumn>
                           </TableRow>
                         </TableHeader>
                     <TableBody displayRowCheckbox={false}>
                       {
                         this.props.searchResults && this.props.searchResults.vouchers && this.props.searchResults.vouchers.map((v,i) =>
                             {
                                let travelDate = "–";
                                if (!!v.travelDate && v.travelDate != "0") {
                                    travelDate = moment.unix(v.travelDate).tz(TZ).format("DD-MMM-YYYY");
                                }
                                 return (
                                     <TableRow key={i} onTouchTap={e => { this.onRowTap(e, v, true) }}  >
                                         <TableRowColumn title={v.id} className="small-text-print-only">
                                           {v.id}
                                         </TableRowColumn>
                                         <TableRowColumn title={v.leadTraveler} className="small-text-print-only compact-show-overflow-print">
                                           {v.leadTraveler}
                                         </TableRowColumn>
                                         <TableRowColumn title={v.productName} className="small-text-print-only compact-show-overflow-print">
                                           {v.productName}
                                         </TableRowColumn>
                                         <TableRowColumn title={v.resellerCode} className="small-text-print-only compact-show-overflow-print">
                                           {v.resellerCode}
                                         </TableRowColumn>
                                         <TableRowColumn title={v.resellerName} className="small-text-print-only compact-show-overflow-print">
                                           {v.resellerName}
                                         </TableRowColumn>
                                         <TableRowColumn title={v.supplierCode} className="small-text-print-only compact-show-overflow-print">
                                           {v.supplierCode}
                                         </TableRowColumn>
                                         <TableRowColumn title={v.supplierName} className="small-text-print-only">
                                           {v.supplierName}
                                         </TableRowColumn>
                                         <TableRowColumn title={formatStatusEnum(v.status)} className="compact-show-overflow-print">
                                           {formatStatusEnum(v.status)}
                                         </TableRowColumn>
                                         <TableRowColumn title={travelDate} className="compact-show-overflow-print">
                                           {travelDate}
                                         </TableRowColumn>
                                     </TableRow>
                                 )
                             }
                         )
                       }
                     </TableBody>
                   </Table>
                    <div className="pagination-controls no-print">
                      {this.state.offset > 0 ?
                        <RaisedButton label="Previous Page" onClick={(e)=>{
                          this.nextPage(-1);
                        }}/> : ""
                      }
                      <div style={{display: "inline-block", marginRight: "1em"}}></div>
                        <RaisedButton label="Next Page" onClick={(e)=>{
                          this.nextPage(1);
                        }}/>
                    </div>
                </section>
              )
          ) : ""
        }
      </div>
    )
  }
}


import { connect } from 'react-redux'
import {
    setPageTitle,
    togglePrintMode
} from '../../../ax-redux/actions/app'
import {
    getVoucherImage,
    exportCSV,
    exportXLSX
} from '../../../ax-redux/actions/vouchers'
import {
    listSuppliers
} from '../../../ax-redux/actions/suppliers'
import {
    listResellers
} from '../../../ax-redux/actions/resellers'
import {
  getSupplierProducts
} from '../../../ax-redux/actions/products'
import {
  getResults
} from '../../../ax-redux/actions/search'
export default connect(
  state => ({
        role: state.app.role,
        orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
        vouchersData: state.vouchers.getVouchers.data,
        fetchingVouchers: state.vouchers.getVouchers.fetching,
        searchResults: state.search.getResults.data,
        fetchingResults: state.search.getResults.fetching,
        printMode: state.app.printMode,
        voucherImage: state.vouchers.getVoucherImage.data,
        pathname: state.routing.locationBeforeTransitions.pathname,
        queryParams: state.routing.locationBeforeTransitions.search,
        suppliers: !!state.suppliers.list.data ? state.suppliers.list.data.suppliers : false,
        fetchingSuppliers: state.suppliers.list.fetching,
        resellers: !!state.resellers.list.data ? state.resellers.list.data.resellers : false,
        fetchingResellers: state.resellers.list.fetching,
        products: !!state.products.getSupplierProducts.data ? state.products.getSupplierProducts.data.products : false,
        fetchingProducts: state.products.getSupplierProducts.fetching
    }),
  dispatch => ({
    search: (data) => {
        dispatch(getResults(data))
    },
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportCSV(opts))
          return
        case "XLSX":
          dispatch(exportXLSX(opts))
          return
      }
    },
    getVoucherImage: (id) => {
        dispatch(getVoucherImage(id))
    },
    listSuppliers: () => {
        dispatch(listSuppliers())
    },
    listResellers: () => {
        dispatch(listResellers())
    },
    getSupplierProducts: (supplierCode) => {
        dispatch(getSupplierProducts(supplierCode))
    }
  })
)(GlobalTransactionSearch);
