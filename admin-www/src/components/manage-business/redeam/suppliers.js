import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import { browserHistory } from 'react-router'
import {
  cyan700,
} from 'material-ui/styles/colors';

let styles = {
  title: {
    color: 'rgb(67, 67, 67)',
    display: 'inline-block',
    marginLeft: '1em',
    marginRight: '1em'
  },
  addNew: {
    float: 'right',
    marginTop: '1em',
    marginRight: '1em'
  },
  narrowCol: {
    width: '8%'
  },
  mediumCol: {
    width: '16%'
  }
}

class Suppliers extends Component {
  constructor(props) {
    super(props)
    this.state = {
        sort: "name"
    }
  }
  componentWillMount() {
    this.props.listSuppliers({sort: "name"})
  }
  componentWillUpdate(nextProps, nextState) {
    let requestParams = {};
    if (this.props.pathname != nextProps.pathname ||
        this.props.queryParams != nextProps.queryParams) {
      if (nextProps.location.query) {
        if (nextProps.location.query.sort) {
            requestParams.sort = nextProps.location.query.sort;
        }
      }
      this.props.listSuppliers(requestParams);
    }
  }
  addSupplier() {
    browserHistory.push("/app/redeam/suppliers/add");
  }
  editSupplier(index) {
    let id = this.props.suppliers.list.data.suppliers[index].supplier.id
    browserHistory.push("/app/redeam/suppliers/"+id);
  }

  sortBy(column) {
    let sort = column;
    if (sort == this.state.sort) {
        if (sort.indexOf("-") > -1) {
          sort = sort.replace("-", "");
        } else {
          sort = "-"+sort;
        }
    }
    this.setState({
        sort: sort
    });
    browserHistory.push(`/app/redeam/suppliers?sort=${sort}`);
  }

  renderRows() {
    
    if ( !this.props.suppliers.list.data || !this.props.suppliers.list.data.suppliers )
      
      return []
    
    return this.props.suppliers.list.data.suppliers.map((supplier, s) => (
      <TableRow key={s}>
        <TableRowColumn style={styles.mediumCol} key={s+".1"}>{ `${supplier.supplier.name} (${supplier.supplier.code}) `}</TableRowColumn>
        <TableRowColumn style={styles.narrowCol} key={s+".2"}>{supplier.supplier.mainLocation && supplier.supplier.mainLocation.city ? supplier.supplier.mainLocation.city : ""}</TableRowColumn>
        <TableRowColumn style={styles.narrowCol} key={s+".3"}>unknown</TableRowColumn>
        <TableRowColumn style={styles.narrowCol} key={s+".4"}>unknown</TableRowColumn>
        <TableRowColumn style={styles.mediumCol} key={s+".5"}>unknown</TableRowColumn>
      </TableRow>
    ))

  }

  render() {
    return (
      <div className="user-management">
        <h2 style={styles.title}>Suppliers</h2>
        <RaisedButton onClick={e=>{ this.addSupplier() }}
                      style={styles.addNew}
                      label="Add New Supplier"
                      primary={true}
        />
            <Card className="redeam-supplier-management">
            <Table onRowSelection={index => this.editSupplier(index)}
                   className="suppliers index"
            >
                <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                  <TableRow>
                  <TableHeaderColumn className="sortable" style={styles.mediumCol}>
                      <div className="inner" onClick={ e => this.sortBy("name") }>Name</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable" style={styles.narrowCol}>
                      <div className="inner" onClick={ e => this.sortBy("location") }>Location</div>
                  </TableHeaderColumn>
                    <TableHeaderColumn style={styles.narrowCol}>Number of Redemption Tablets</TableHeaderColumn>
                    <TableHeaderColumn style={styles.narrowCol}>Number of Products</TableHeaderColumn>
                    <TableHeaderColumn style={styles.mediumCol}>Total Lifetime Ticket Volume in # and $</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                  {this.renderRows()}
                </TableBody>
              </Table>
              {(this.props.fetching ?
                  <div className="loading-circle">
                    <CircularProgress color={"#27367a"} size={96} />
                  </div> : ""
              )}
             </Card>
          </div>
    )
  }
}

import { connect } from 'react-redux'
import { listSuppliers } from '../../../ax-redux/actions/suppliers'

export default connect(
  state => {
    return {
      suppliers: state.suppliers,
      fetching: state.suppliers.list.fetching,
      pathname: state.routing.locationBeforeTransitions.pathname,
      queryParams: state.routing.locationBeforeTransitions.search
    }
  },
  dispatch => {
    return {
      listSuppliers: (params) => {
        dispatch(listSuppliers(params))
      },
    }
  }
)(Suppliers)


// Expanding the Supplier section should display a list of suppliers with the following data columns:
//
// - Name
// - Location (City/Country)
// - Number of Redemption Tablets
// - Number of Products
// - Total Lifetime Ticket Volume in # and $
//
// Clicking on a specific supplier should result in a detail view that enables the management of the supplier’s account. In essence, this would enable an Redeam team member to “impersonate” the supplier and manage their account just as if they were logged in as that account. Items which should be editable are specified later in this section.
//
// The following outlines the list of editable items under each section:
//
// - Add, Update and Delete Suppliers
//   - Supplier Name
//   - Locations
//     - Supplier Addresses
//     - Supplier Telephones
//   - Supplier Contacts
//     - Name
//     - Title
//     - Role
//     - E-Mail Address
//     - Login / Operator ID (if req’d)
//     - Password (if req’d)
//   - Supplier Business Terms
//     - Fee Structure
//       - Percentage of Ticket $ Amount
//       - Absolute Maximum $ amount (which makes the % “up to $XX”)
//     - Trial Period
