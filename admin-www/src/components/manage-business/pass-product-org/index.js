import React, { Component, PropTypes } from 'react'

class PassProductOrgManagement extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="supplier-management-index">
            {this.props.children}
            </div>
        )
    }
}

import { connect } from 'react-redux'
export default connect(
    state => {
        return {
            role: state.app.role,
            app: state.app,
        }
    },
    dispatch => {
        return {

        }
    }
)(PassProductOrgManagement)
