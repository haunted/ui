import React, { Component, PropTypes } from 'react'
import {
    ACTOUREX_CUSTOMER_SUPPORT_STAFF,
    ACTOUREX_BUSINESS_STAFF,
    ACTOUREX_TECHNICAL_STAFF,
    SUPPLIER_REDEMPTION_STAFF,
    SUPPLIER_REDEMPTION_SUPERVISOR,
    SUPPLIER_BUSINESS_STAFF,
    RESELLER_BUSINESS_STAFF,
    PASS_ISSUER_BUSINESS_STAFF
} from '../../ax-redux/constants'
import { browserHistory } from 'react-router'
import RedeamManagement from './redeam'
import ResellerManagement from './reseller'
import SupplierManagement from './supplier'
import PassProductOrgManagement from './pass-product-org'

class ManageBusiness extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    renderSwitch() {
        switch (this.props.role) {
            case ACTOUREX_CUSTOMER_SUPPORT_STAFF:
            return ""
            case ACTOUREX_BUSINESS_STAFF:
            return <RedeamManagement>{this.props.children}</RedeamManagement>
            case ACTOUREX_TECHNICAL_STAFF:
            return <RedeamManagement>{this.props.children}</RedeamManagement>
            case SUPPLIER_REDEMPTION_STAFF:
            return ""
            case SUPPLIER_REDEMPTION_SUPERVISOR:
            return ""
            case SUPPLIER_BUSINESS_STAFF:
            return <SupplierManagement>{this.props.children}</SupplierManagement>
            case RESELLER_BUSINESS_STAFF:
            return <ResellerManagement>{this.props.children}</ResellerManagement>
            case PASS_ISSUER_BUSINESS_STAFF:
            return <PassProductOrgManagement>{this.props.children}</PassProductOrgManagement>
        }
        return "role not supported"
    }
    render() {
        return (
            <div className="manage-business">
            {this.renderSwitch()}
            </div>
        )
    }

  setItemsFor(role) {
    switch (role) {
      case ACTOUREX_CUSTOMER_SUPPORT_STAFF:
        this.props.setSideMenuItems([])
        break
      case ACTOUREX_BUSINESS_STAFF:
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
            { title: 'Products', url:'/app/manage-business/products' },
            { title: 'Price Lists', url:'/app/manage-business/price-lists' },
            { title: 'Contracts', url:'/app/manage-business/contracts' },
            { title: 'Settings', url:'/app/manage-business/settings' }
        ])
        break
      case ACTOUREX_TECHNICAL_STAFF:
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
          { title: 'Products', url:'/app/manage-business/products' },
          { title: 'Price Lists', url:'/app/manage-business/price-lists' },
          { title: 'Contracts', url:'/app/manage-business/contracts' },
          { title: 'Settings', url:'/app/manage-business/settings' }
        ])
        break
      case SUPPLIER_REDEMPTION_STAFF:
        this.props.setSideMenuItems([

        ])
        break
      case SUPPLIER_REDEMPTION_SUPERVISOR:
        this.props.setSideMenuItems([])
        break
      case SUPPLIER_BUSINESS_STAFF:
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
          { title: "Profile", url:'/app/manage-business'},
          { title: 'Search', url:'/app/manage-business/search', feature: "supplier-transaction-search" },
          { title: 'Products', url:'/app/manage-business/products' },
          { title: 'Price Lists', url:'/app/manage-business/price-lists' },
          { title: 'Contracts', url:'/app/manage-business/contracts' },
          { title: 'Settings', url:'/app/manage-business/settings' }
        ])
        break
      case RESELLER_BUSINESS_STAFF:
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
            { title: "Profile", url:'/app/manage-business'},
            { title: 'Products', url:'/app/manage-business/products' },
            { title: 'Settings', url:'/app/manage-business/settings' }
        ])
      break
      case PASS_ISSUER_BUSINESS_STAFF:
        this.props.setSideMenuTitle("Manage Business")
        this.props.setSideMenuItems([
            { title: 'Products', url:'/app/manage-business/products' },
            { title: 'Settings', url:'/app/manage-business/settings' }
        ])
        break
    }

  }
      componentDidMount () {
        this.props.setPageTitle("Manage Business");
        //this.props.impersonate("ACTOUREX_TECHNICAL_STAFF");
        this.setItemsFor(this.props.role);
        if (this.props.role == PASS_ISSUER_BUSINESS_STAFF) {
          browserHistory.push("/app/manage-business/products")
        }
      }

      componentWillUpdate(nextProps, nextState) {
          if (this.props.role != nextProps.role) {
            this.setItemsFor(nextProps.role);
          }
      }
}

import { connect } from 'react-redux'

import { setSideMenuItems, setPageTitle, setSideMenuTitle } from '../../ax-redux/actions/app'
import { spoofUserRole } from '../../ax-redux/actions/auth'

export default connect(
    state => {
        return {
            subsection: state.routing.locationBeforeTransitions.pathname.split("manage-business").pop(),
            role: state.app.role,
            app: state.app,
        }
    },
    dispatch => {
        return {
            setPageTitle: title => {
                dispatch(setPageTitle(title))
            },
            setSideMenuItems: items => {
                dispatch(setSideMenuItems(items))
            },
            setSideMenuTitle: title => {
                dispatch(setSideMenuTitle(title))
            },
            impersonate: (role) => {
                dispatch(spoofUserRole(role))
            }
        }
    }
)(ManageBusiness)
