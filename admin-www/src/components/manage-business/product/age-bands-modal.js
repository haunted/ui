import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Slider from 'material-ui/Slider'
import IconButton from 'material-ui/IconButton'
import ContentRemoveCircleOutline from 'material-ui/svg-icons/content/remove-circle-outline'
import ContentAddOutline from 'material-ui/svg-icons/content/add-circle-outline'
import AgeBandsPopover from '../../age-bands-popover'
import AgeRangeSlider from '../../age-range-slider'
import {
  decodeUnit,
  capitalize
} from '../../../util'

let styles = {
  lightbox: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    cursor: 'pointer',
    background: 'rgba(0,0,0,0.62)',
    zIndex: 1999
  },
  modal: {
    minWidth: '570px',
    padding: '1em',
    borderRadius: '2.5px',
    display: 'inline-block',
    overflowY: 'auto',
    overflowX: 'hidden',
    width: '0',
    background: '#fff',
    position: 'absolute',
    margin: 'auto',
    top: '38px',
    left: '0',
    right: '0',
    zIndex: '999999999'
  },
  header: {
    color: 'rgb(67, 67, 67)'
  },
  form: {},
  modalOption: {
    marginTop: '1em',
    marginRight: '1em'
  },
  modalOptions: {
    paddingLeft: '3px'
  },
  travelerType: {},
  medium: {
   width: 96,
   height: 96,
   padding: 24,
 },
 mediumIcon: {
   width: 48,
   height: 48,
 },
 travelerTypeColumn: {
   width: '100px',
   paddingRight: 0,
   overflow: 'visible'
 },
 sliderColumn: {

 },
 bottomSlider: {

 },
 numeric: {
   marginRight: '0.5em',
   padding: '0.5em',
   borderRadius: '2px',
   border: 'none',
   boxShadow: 'rgba(0, 0, 0, 0.117647) 0px 1px 6px, rgba(0, 0, 0, 0.117647) 0px 1px 4px'
 },
 rangeTextColumn: (valid) => {
   return {
     color: valid ? 'black' : 'red',
     width: '48px',
     paddingLeft: 0,
     paddingRight: 0
   }
 },
 buttonColumn: {
   width: '80px',
   paddingLeft: 0
 },
 nonModal: {
   pointerEvents: 'all',
   position: 'relative',
   marginLeft: '3%',
   width: '60%',
   height: 'auto',
   minWidth: 0,
   boxShadow: 'none'
 },
 noLightbox: {
   height: 'auto',
   pointerEvents: 'none',
   background: 'transparent',
   position: 'relative'
 }
}

export default class AgeBandsModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activated: false,
      display: true,
      initialAgeBands: [],
      overlapError: '',
      travelerTypes: []
    }
  }
  componentWillMount () {
    this.loadAgeBands(this.props.travelerTypes)
  }
  componentWillUpdate (nextProps, nextState) {
    if (nextProps.travelerTypes.length != this.props.travelerTypes.length) {
      this.loadAgeBands(nextProps.travelerTypes)
    }
  }
  loadAgeBands(input) {
    let travelerType = null,
        travelerTypes = []
    input.map((travelerType, i) =>{
      if (typeof travelerType.typeOf == 'undefined') {
        travelerTypes.push(Object.assign({}, {
          ageMax: travelerType.to,
          ageMin: travelerType.from,
          typeOf: travelerType.type //capitalize(decodeUnit())
        }))  
      } else {
        travelerTypes.push(Object.assign({}, {
          ageMax: travelerType.ageMax,
          ageMin: travelerType.ageMin,
          typeOf: travelerType.typeOf //capitalize(decodeUnit())
        }))
      }
    })
    this.setState({
      travelerTypes: travelerTypes
    })
  }
  toggleForm () {
    this.setState({
        activated: !this.state.activated
    })
    let beforeAgeBands = []
    this.state.travelerTypes.map((a, i)=> {
      beforeAgeBands.push(Object.assign({}, a))
    })
    this.setState({
      initialAgeBands: beforeAgeBands
    })
  }
  setAgeBandType (o, type) {
    let travelerTypes = this.state.travelerTypes
    travelerTypes[o].typeOf = type
    this.setState({ travelerTypes })
    this.validate()
    this.props.modal == false && this.submit()
  }
  addAgeBand() {
    let travelerTypes = this.state.travelerTypes
    travelerTypes.push({
      typeOf: "TYPE_ADULT",
      ageMin: 18,
      ageMax: 65
    })
    this.setState({ travelerTypes })
    this.props.modal == false && this.submit(true)
  }
  removeAgeBand(index) {
    let travelerTypes = this.state.travelerTypes
    travelerTypes.splice(index, 1)
    this.setState({ travelerTypes })
    this.props.modal == false && this.submit()
    this.setState({
      display: false
    })
    setTimeout(()=>{
      this.setState({
        display: true
      })
    }, 50)
  }
  isAgeBand(TravelerType) {
    let t = TravelerType.toLowerCase()
    if (t == "adult" || t == "child" || t == "youth" || t == "infant" || t == "senior") {
      return true
    }
  }
  validate (noErrorMsg = false) {
    let valid = true,
        overlap = false,
        travelerTypes = this.state.travelerTypes
    travelerTypes.map((band, a)=>{
      let ageMin = band.ageMin,
          ageMax = band.ageMax
      if (ageMin > ageMax) {
        valid = false
      }
      if (this.isAgeBand(band.typeOf)) {
        travelerTypes.map((otherBand, b)=>{
          if (this.isAgeBand(otherBand.typeOf)) {
            if (a != b) {
              if ((ageMax > otherBand.ageMin && !(ageMin > otherBand.ageMax)) ||
                  (ageMin > otherBand.ageMax && !(ageMax > otherBand.ageMin))) {
                valid = false
                overlap = true
                if (noErrorMsg == false) {
                  this.setState({overlapError: "Age Ranges Can Not Overlap"})
                }
              }
            }
          }
        })
      }
    })
    if (!overlap) {
      this.setState({overlapError: ""})
    }
    return valid
  }
  reset() {
    this.setState({
      overlapError: '',
      travelerTypes: this.state.initialAgeBands
    })
  }
  submit (noErrorMsg = false) {
      if (this.validate(noErrorMsg)) {
        this.props.modal == true && this.toggleForm()
        this.props.onSave(this.state.travelerTypes)
      }
  }
  cancel () {
      this.reset()
      this.toggleForm()
  }
  setAge (how, index, value) {
    let travelerTypes = this.state.travelerTypes
    if (how == 'from') {
      travelerTypes[index].ageMin = parseInt(value)
    } else {
      travelerTypes[index].ageMax = parseInt(value)
    }
    this.setState({ travelerTypes })
    this.validate()
    this.props.modal == false && this.submit()
  }
   /* <Slider
              onChange={(e,v) => { this.setAge('from', index, v) }}
              defaultValue={data.ageMin}
              className='age-slider top'
              min={0}
              max={100}
              step={1}
            />
            <Slider
              onChange={(e,v) => { this.setAge('to', index, v)}}
              defaultValue={data.ageMax}
              className='age-slider bottom'
              style={styles.bottomSlider}
              min={0}
              max={100}
              step={1}
            /> 
            <AgeRangeSlider onSave={(v) => {
                              this.setAge('to', index, v.ageMax)
                              this.setAge('from', index, v.ageMin)
                            }}
                            from={data.ageMin}
                            to={data.ageMax}
      /> */
  renderAgeBand(data, index) {
    let rangeTextStyle = styles.rangeTextColumn(data.ageMin < data.ageMax)
    return (
      <TableRow key={index} selectable={false} style={styles.travelerType}>
          <TableRowColumn style={styles.travelerTypeColumn}>
            <AgeBandsPopover onChange={travelerType => this.setAgeBandType(index, travelerType.typeOf)}
                             values={[
                               {typeOf: "TYPE_ADULT"},
                               {typeOf: "TYPE_YOUTH"},
                               {typeOf: "TYPE_CHILD"},
                               {typeOf: "TYPE_INFANT"},
                               {typeOf: "TYPE_SENIOR"},
                               {typeOf: "TYPE_STUDENT"},
                               {typeOf: "TYPE_MILITARY"}
                             ]}
                             value={data.typeOf}
            />
          </TableRowColumn>
          <TableRowColumn style={styles.sliderColumn}>
          <input onChange={(e) => { this.setAge('from', index, e.target.value) }}
                 value={data.ageMin}
                 style={styles.numeric}
                 type='number'
                 min='0'
                 max='100'
          />
          <span>to </span>
          <input onChange={(e) => { this.setAge('to', index, e.target.value) }}
                 value={data.ageMax}
                 style={styles.numeric}
                 type='number'
                 min='0'
                 max='100'
          />
         
          </TableRowColumn>
          <TableRowColumn style={rangeTextStyle}>
            {data.ageMin < data.ageMax ? `${data.ageMin} - ${data.ageMax}` :  (['Invalid', 'Age', 'Range'].map(t=>(
              <div key={t}>{ t }</div>
            )))}
          </TableRowColumn>
          <TableRowColumn style={styles.buttonColumn}>
            <IconButton
              iconStyle={styles.mediumIcon}
              style={styles.medium}
              onClick={ e=> this.removeAgeBand(index)}
             >
             <ContentRemoveCircleOutline />
           </IconButton>
          </TableRowColumn>
      </TableRow>
    )
  }
 
  render() {
    let lightboxStyle = styles.noLightbox,
        modalStyle = styles.nonModal
    if (this.props.modal) {
        lightboxStyle = styles.lightbox
        modalStyle = styles.modal
    }
      if (this.state.activated || this.props.modal == false) {

        if ( !!this.props.refreshing ) {

          return (
            <span>Refreshing...</span>
          )

        }

        console.log("age bands modal traveler types: ", this.state.travelerTypes)
          return (
              <div style={lightboxStyle}>
                  <div style={modalStyle}>
                  <h2 style={styles.header}>{this.props.label || "Manage Traveler Types"}</h2>
                  { this.state.overlapError != '' ? <h2 style={{color:'red'}}>{this.state.overlapError}</h2> : '' }
                  <Table style={styles.form}>
                    <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                        <TableRow style={{display: 'none'}}>
                            <TableHeaderColumn></TableHeaderColumn>
                            <TableHeaderColumn></TableHeaderColumn>
                            <TableHeaderColumn></TableHeaderColumn>
                            <TableHeaderColumn></TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                      {this.state.display && this.state.travelerTypes.map((travelerType, i) => {
                          return this.renderAgeBand(travelerType, i)
                      })}
                      <TableRow>
                        <TableRowColumn style={styles.travelerTypeColumn}></TableRowColumn>
                        <TableRowColumn style={styles.sliderColumn}></TableRowColumn>
                        <TableRowColumn style={styles.rangeTextColumn}></TableRowColumn>
                        <TableRowColumn style={styles.buttonColumn}>
                          <IconButton
                            iconStyle={styles.mediumIcon}
                            style={styles.medium}
                            onTouchTap={ e=> this.addAgeBand()}
                           >
                           <ContentAddOutline />
                         </IconButton>
                        </TableRowColumn>
                      </TableRow>
                    </TableBody>
                  </Table>
                  { this.props.modal == true ? (
                    <div style={styles.modalOptions}>
                      <RaisedButton
                            onTouchTap={e => { this.cancel() } }
                            style={styles.modalOption}
                            label="Cancel"
                       />
                      <RaisedButton
                            onTouchTap={e => { this.submit() } }
                            style={styles.modalOption}
                            label="Save"
                       />
                    </div>
                  ) : ''}
                </div>
            </div>
          )
      } else {
          return (
              <RaisedButton
                    onTouchTap={e => { this.toggleForm() } }
                    className={"no-print " + (this.props.className || "") + (this.props.secondary ? " secondary-button" : "")}
                    secondary={this.props.secondary}
                    style={(this.props.style  || {})}
                    label={this.props.label || "Manage Traveler Types"}
               />
          )
      }
  }
}

AgeBandsModal.propTypes = {
  travelerTypes: PropTypes.array.isRequired,
  onSave: PropTypes.func.isRequired,
  modal: PropTypes.bool,
  secondary: PropTypes.bool
}

AgeBandsModal.defaultProps = {
  modal: true,
  secondary: false,
  refreshing: false
}
