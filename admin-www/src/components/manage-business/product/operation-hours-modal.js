import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import IconButton from 'material-ui/IconButton'
import TimePicker from 'material-ui/TimePicker'
import DatePicker from 'material-ui/DatePicker'
import moment from 'moment-timezone'
import ContentRemoveCircleOutline from 'material-ui/svg-icons/content/remove-circle-outline'
import ContentAddOutline from 'material-ui/svg-icons/content/add-circle-outline'
import DaysOpen from '../../days-open'
import OpenHours from '../../open-hours'

const styles = {
  lightbox: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    cursor: 'pointer',
    background: 'rgba(0,0,0,0.62)',
    zIndex: 1999
  },
  modal: {
    minWidth: '1050px',
    display: 'inline-block',
    width: '0',
    position: 'absolute',
    margin: 'auto',
    top: '34px',
    left: '0',
    right: '0',
    zIndex: '1999'
  },
  innerModal: {
    padding: '1em',
    background: '#fff',
    padding: '1em',
    borderRadius: '2.5px'
  },
  header: {
    color: 'rgb(67, 67, 67)'
  },
  form: {},
  modalOption: {
    marginTop: '1em',
    marginRight: '1em'
  },
  modalOptions: {
    paddingLeft: '3px'
  },
  openingHours: {},
  closedDates: {
    width: '33%'
  },
  medium: {
   paddingLeft: 0,
   width: 96,
   height: 64,
   padding: 24,
   paddingTop: 0,
   paddingBottom: 0
  },
  mediumIcon: {
    width: 48,
    height: 48,
  },
  daysColumn: {

  },
  hoursColumn: {
    width: '222px'
  },
  buttonColumn: {
     width: '48px'
  },
  dialog: {
    color: 'black'
  },
  nonModal: {
    pointerEvents: 'all',
    position: 'relative',
    width: '94%',
    height: 'auto',
    minWidth: '1024px',
    boxShadow: 'none'
  },
  noLightbox: {
    height: 'auto',
    pointerEvents: 'none',
    background: 'transparent',
    position: 'relative'
  }
}

export default class OperationHoursModal extends Component {
  constructor(props) {
    super(props)
    this.state = {
      errorMessage: "",
      activated: false,
      openingHours: [],
      closedDates: []
    }
  }
  componentDidMount () {
    let openingHours = [],
        closedDates = []
    this.props.openingHours.map((hours, i) =>{
      openingHours.push(Object.assign({}, hours))
    })
    this.props.closedDates.map((date, i) =>{
      closedDates.push(Object.assign({}, date))
    })
    this.setState({
      openingHours: openingHours,
      closedDates: closedDates
    })
  }
  toggleForm () {
    this.setState({
        activated: !this.state.activated
    })
    let beforeHours = [],
        beforeDates = []
    this.state.openingHours.map((h,i) => {
      beforeHours.push(Object.assign({}, h))
    })
    this.state.closedDates.map((d,i) => {
      beforeDates.push(Object.assign({}, d))
    })
    this.setState({
      beforeHours,
      beforeDates
    })
  }
  removeOpeningHours (index) {
    let openingHours = this.state.openingHours
    openingHours.splice(index, 1)
    this.setState({ openingHours })
    this.setState({
        activated: false
    })
    setTimeout(()=>{
      this.setState({
          activated: true
      })
      this.props.modal == false && this.submit()
    }, 50)
  }
  removeClosedDate (index) {
    let closedDates = this.state.closedDates
    closedDates.splice(index, 1)
    this.setState({ closedDates })
    this.props.modal == false && this.submit()
  }
  addOpeningHours () {
    let openingHours = this.state.openingHours
    openingHours.push({
      days: [true, true, true, true, true, true, true],
      from: 32400000, // 9am
      to: 75600000 // 9pm
    })
    this.setState({ openingHours })
    this.props.modal == false && this.submit()
  }
  setDays(days, index) {
    let openingHours = this.state.openingHours
    openingHours[index].days = days
    this.setState({ openingHours })
    this.validate()
    this.props.modal == false && this.submit()
  }
  setHours(value, index) {
    let openingHours = this.state.openingHours
    openingHours[index].from = value.from
    openingHours[index].to = value.to
    this.setState({ openingHours })
  }
  setClosedDate(value, index) {
    let closedDates = this.state.closedDates
    closedDates[index] = value
    this.setState({ closedDates })
    this.props.modal == false && this.submit()
  }
  addClosedDate () {
    let closedDates = this.state.closedDates
    closedDates.push(new Date())
    this.setState({ closedDates })
    this.props.modal == false && this.submit()
  }
  validate (hours) {
    let valid = true,
        openingHours = hours || this.state.openingHours
    if (openingHours.length > 0) {
      openingHours.map(opening => {
        let days = 0
        if (opening.from == opening.to) {
          valid = false
        }
        opening.days.map(day => {
          if (day) {
            days ++
          }
        })
        if (days == 0) {
          valid = false
          this.setState({errorMessage: "One or more week days must be selected."})
        }
      })
    }
    if (valid) {
      this.setState({errorMessage: ""})
    }
    return valid
  }
  reset () {
    this.setState({
      openingHours: this.state.beforeHours,
      closedDates: this.state.beforeDates
    })
    this.setState({
      errorMessage: ""
    })
  }
  submit () {
      if (this.validate()) {
        this.props.modal && this.toggleForm()
        this.props.onSave({
          openingHours: this.state.openingHours,
          closedDates: this.state.closedDates
        })
      }
  }
  cancel () {
    this.reset()
    this.toggleForm()
  }
  renderOpeningHours(data) {
    return (
      <Table style={styles.openingHours}>
        <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
            <TableRow style={{display: 'none'}}>
                <TableHeaderColumn style={styles.daysColumn}></TableHeaderColumn>
                <TableHeaderColumn style={styles.hoursColumn}></TableHeaderColumn>
                <TableHeaderColumn style={styles.buttonColumn}></TableHeaderColumn>
            </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
        {
          data.map((openingHours, i) => {
            return (
              <TableRow key={i} selectable={false} >
                  <TableRowColumn style={styles.daysColumn}>
                    <DaysOpen days={openingHours.days}
                              onChange={v=> this.setDays(v, i)  }
                    />
                  </TableRowColumn>
                  <TableRowColumn style={styles.hoursColumn}>
                    <OpenHours from={openingHours.from}
                               to={openingHours.to}
                               onChange={v=> this.setHours(v, i)}
                    />
                  </TableRowColumn>
                  <TableRowColumn style={styles.buttonColumn}>
                    <IconButton
                      iconStyle={styles.mediumIcon}
                      style={styles.medium}
                      onClick={ e=> { this.removeOpeningHours(i)} }
                     >
                     <ContentRemoveCircleOutline />
                   </IconButton>
                  </TableRowColumn>
              </TableRow>
            )
          })
        }
          <TableRow>
            <TableRowColumn style={styles.daysColumn}>
              <IconButton
                iconStyle={styles.mediumIcon}
                style={styles.medium}
                onTouchTap={ e=> this.addOpeningHours()}
               >
               <ContentAddOutline />
             </IconButton>
            </TableRowColumn>
            <TableRowColumn style={styles.hoursColumn}></TableRowColumn>
            <TableRowColumn style={styles.buttonColumn}></TableRowColumn>
          </TableRow>
        </TableBody>
     </Table>
    )
  }
  renderClosedDates() {
    let data = this.state.closedDates
    return (
      <Table style={styles.closedDates}>
        <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
          <TableRow style={{display: 'none'}}>
            <TableHeaderColumn></TableHeaderColumn>
            <TableHeaderColumn></TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
        {
          data.map((closingDate, i) => (
            <TableRow key={i} selectable={false} >
                <TableRowColumn>
                  <DatePicker formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM")}
                              onChange={(ev, date) => { this.setClosedDate(date, i) } }
                              className="form-date-picker"
                              value={closingDate}
                              hintText="Enter Closed Date"
                              container="inline"
                  />
                </TableRowColumn>
                <TableRowColumn style={styles.buttonColumn}>
                  <IconButton
                    iconStyle={styles.mediumIcon}
                    style={styles.medium}
                    onClick={ e=> this.removeClosedDate(i)}
                   >
                   <ContentRemoveCircleOutline />
                 </IconButton>
                </TableRowColumn>
            </TableRow>
          ))
        }
        <TableRow selectable={false} >
            <TableRowColumn>
              <IconButton
                iconStyle={styles.mediumIcon}
                style={styles.medium}
                onClick={ e=> this.addClosedDate()}
               >
               <ContentAddOutline />
             </IconButton>
            </TableRowColumn>
            <TableRowColumn style={styles.buttonColumn}>

            </TableRowColumn>
        </TableRow>
        </TableBody>
     </Table>
    )
  }
  render() {
    let lightboxStyle = styles.noLightbox,
        modalStyle = styles.nonModal
    if (this.props.modal) {
        lightboxStyle = styles.lightbox
        modalStyle = styles.modal
    }
      if (this.state.activated || this.props.modal == false) {
          return (
              <div style={lightboxStyle}>
                  <div style={modalStyle}>
                    <div style={styles.innerModal}>
                      <h2 style={styles.header}>{this.props.label || "Manage Hours of Operation"}</h2>
                      { this.state.errorMessage != '' ? <h2 style={{color:'red'}}>{this.state.errorMessage}</h2> : '' }
                      <section style={styles.form}>
                        <h3>Opening Hours</h3>
                        { this.renderOpeningHours(this.state.openingHours) }
                        <h3>Closed Dates</h3>
                        { this.renderClosedDates() }
                      </section>
                      { this.props.modal == true ? (
                        <div style={styles.modalOptions}>
                        <RaisedButton
                              onTouchTap={e => { this.cancel() } }
                              style={styles.modalOption}
                              label="Cancel"
                         />
                        <RaisedButton
                              onTouchTap={e => { this.submit() } }
                              style={styles.modalOption}
                              label="Save"
                         />
                      </div>
                      ) : ''}
                  </div>
               </div>
            </div>
          )
      } else {
          return (
              <RaisedButton
                    onTouchTap={e => { this.toggleForm() } }
                    className={"no-print toggle-queue-for-review-button " + (this.props.className || "")}
                    secondary={this.props.secondary}
                    className={this.props.secondary ? "secondary-button" : ""}
                    style={(this.props.style  || {})}
                    label={this.props.label || "Manage Hours of Operation"}
               />
          )
      }
  }
}

OperationHoursModal.propTypes = {
  openingHours: PropTypes.array.isRequired,
  closedDates: PropTypes.array.isRequired,
  onSave: PropTypes.func.isRequired,
  modal: PropTypes.bool,
  secondary: PropTypes.bool
}

OperationHoursModal.defaultProps = {
  modal: true,
  secondary: false
}
