import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import DatePicker from 'material-ui/DatePicker'
import Checkbox from 'material-ui/Checkbox'
import moment from 'moment-timezone'
import CurrencyPopover from '../../currency-popover'
import AgeBandsPopover from '../../age-bands-popover'
import DaysOpen from '../../days-open'
import DistributionPopover from '../../distribution-popover'

const styles = {
  lightbox: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    top: '0',
    left: '0',
    cursor: 'pointer',
    background: 'rgba(0,0,0,0.62)',
    zIndex: 1999,
    overflowX: 'hidden',
    overflowY: 'auto'
  },
  modal: {
    minHeight: '708px',
    minWidth: '530px',
    padding: '1em',
    display: 'inline-block',
    overflowY: 'auto',
    overflowX: 'hidden',
    width: '0',
    background: '#fff',
    borderRadius: '2.5px',
    position: 'absolute',
    margin: 'auto',
    top: '18px',
    left: '0',
    right: '0',
    zIndex: '999999999'
  },
  header: {
    marginTop: 0,
    marginBottom: 0
  },
  form: {},
  modalOption: {
    marginTop: '1em',
    marginLeft: '1em',
    float: 'right'
  },
  modalOptions: {
    paddingLeft: '3px'
  },
  labelColumn: {
    width: '180px'
  },
  overflow: {
    overflow: 'visible'
  },
  numeric: {
    padding: '0.5em',
    borderRadius: '2px',
    border: 'none',
    boxShadow: 'rgba(0, 0, 0, 0.117647) 0px 1px 6px, rgba(0, 0, 0, 0.117647) 0px 1px 4px'
  }
}

export default class AddOptionModal extends Component {

  constructor(props) {

    super(props)
    let validFrom = new Date(),
        validUntil = new Date()

    validUntil.setFullYear(validUntil.getFullYear()+1)
    this.defaultValues = {
      activated: false,
      loading: false,
      id: "",
      errorTitle: '',
      errorCode: '',
      errorAgeBand: '',
      errorPrice: '',
      validEnding: false,
      title: "",
      desc: "",
      code: "",
      //id: "",
      retailCurrency: "USD",
      travelerType: false,
      retailPrice: 0.0,
      minTravelers: 1,
      maxTravelers: 99,
      validFrom,
      validUntil,
      days: [false, false, false, false, false, false, false],
      openingHours: [],
      closedDates: [],
      useInAdvancedPurchase: false,
      inAdvancePurchase: 0, // days
      distribution: "public"
    }
    this.state = Object.assign({}, this.defaultValues)

  }

  componentDidMount () {

  }

  componentWillUpdate ( nextProps, nextState) {

    if (false == nextState.activated && true == this.state.activated) {
      this.reset()
    }

    if (nextState.title != this.state.title ||
        nextState.code != this.state.code ||
        nextState.travelerType.type != this.state.travelerType.type ||
        nextState.travelerType.typeOf != this.state.travelerType.typeOf ||
        nextState.travelerType.from != this.state.travelerType.from ||
        nextState.travelerType.to != this.state.travelerType.to ||
        nextState.retailPrice != this.state.retailPrice) {
        
        this.clearErrors(nextState) // clear Errors
    }

    if (this.state.loading == true && nextState.loading == true) {
      this.setState({
        loading: false
      })
    }

    if ( !!nextProps.value && this.state.title == "" ) {
      
      this.setState(nextProps.value)
      this.setState({
        validFrom: new Date(nextProps.value.validFrom),
        validUntil: new Date(nextProps.value.validUntil),
        loading: true
      })

    }

  }

  reset () {

    if (!!this.props.mode && this.props.mode == 'edit') {
      return
    }
    this.setState(
      this.defaultValues
    )
    this.setState({
      days: [false, false, false, false, false, false, false]
    })
    this.setState({
      errorTitle: '',
      errorCode: '',
      errorAgeBand: '',
      errorPrice: ''
    })

  }

  toggleForm () {

    this.setState({
        activated: !this.state.activated
    })

  }

  validate (data) {

    let valid = true,
        errorTitle = '',
        errorCode = '',
        errorAgeBand = '',
        errorPrice = ''

    if (data.title == '') {
      errorTitle = 'Title is required.'
      valid = false
    } else {
      errorTitle = ''
    }
    if (data.code == '') {
      errorCode = 'Code is required.'
      valid = false
    } else {
      errorCode = ''
    }
    if (data.travelerType == '') {
      errorAgeBand = 'Traveler Type is required.'
      valid = false
    } else {
      errorAgeBand = ''
    }

    this.setState({
      errorTitle,
      errorCode,
      errorAgeBand,
      errorPrice,
      valid
    })

    return valid

  }

  clearErrors ( data ) {
    
    let errorTitle = this.state.errorTitle,
        errorCode = this.state.errorCode,
        errorAgeBand = this.state.errorAgeBand,
        errorPrice = this.state.errorPrice

    
    if (data.title != '') {
      errorTitle = ''
    }

    if (data.code != '') {
      errorCode = ''
    }

    if (data.travelerType != '') {
      errorAgeBand = ''
    }
    // if (data.retailPrice != '' && !isNaN(data.retailPrice)) {
    //   errorPrice = ''
    // }
    this.setState({
      errorTitle,
      errorCode,
      errorAgeBand,
      errorPrice
    })

  }

  submit () {
    
    let data = false

    if ( this.validate( this.state ) ) {

      data = {
        id: this.state.id,
        validEnding: this.state.validEnding,
        title: this.state.title,
        desc: this.state.desc,
        code: this.state.code,
        //retailCurrency: this.state.retailCurrency,
        travelerType: this.state.travelerType,
        //retailPrice: parseFloat(this.state.retailPrice),
        minTravelers: parseInt(this.state.minTravelers),
        maxTravelers: parseInt(this.state.maxTravelers),
        validFrom: this.state.validFrom,
        validUntil: this.state.validUntil,
        // hours: {
        //   openingHours: this.state.openingHours,
        //   closedDates: this.state.closedDates,
        // }

        inAdvancePurchase: this.state.inAdvancePurchase,
        //distribution: this.state.distribution
      }

      this.toggleForm()
      this.props.onSave(data)
    
    }

  }

  cancel () {
      this.reset()
      this.toggleForm()
  }

  shouldDisableValidFromDate (day) {
    let to = this.state.validUntil.getTime()
    return  day.getTime() > to
  }

  shouldDisableValidToDate (day) {
    let from = this.state.validFrom.getTime()
    return day.getTime() < from
  }

  render() {
      if (!!this.props.mode == 'edit' && typeof this.props.value != 'object') {
        return (
          <span></span>
        )
      }
      if (this.state.activated) {
          return (
              <div style={styles.lightbox}>
                  <div style={styles.modal}>
                  <h2 style={styles.header}>{this.props.label || "Add Product Option"}</h2>
                  <Table style={styles.form}>
                    <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                        <TableRow style={{display: 'none'}}>
                            <TableHeaderColumn style={styles.labelColumn}></TableHeaderColumn>
                            <TableHeaderColumn></TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                      <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Title
                                </TableRowColumn>
                                <TableRowColumn style={styles.overflow}>
                                    {!this.state.loading ? 
                                    <TextField defaultValue={this.state.title} hintText={"Option Title"}
                                               key="Title"
                                               errorText={this.state.errorTitle}
                                               onChange={e=>{ this.setState({title: e.target.value}) }}
                                    /> : ""}
                                </TableRowColumn>
                            </TableRow>
                            <TableRow selectable={false} >
                                <TableRowColumn  style={styles.labelColumn}key={"1.1"}>
                                    Description
                                </TableRowColumn>
                                <TableRowColumn>
                                     {!this.state.loading ? 
                                    <TextField defaultValue={this.state.desc}
                                               onChange={e=>{ this.setState({desc: e.target.value}) }}
                                               errorText={this.state.errorDescription}
                                               floatingLabelText="Option Description"
                                               key="Description"
                                               multiLine={true}
                                               rowsMax={8}
                                               rows={2}
                                    /> : ""}
                                </TableRowColumn>
                            </TableRow>
                            <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Code
                                </TableRowColumn>
                                <TableRowColumn style={styles.overflow}>
                                   {!this.state.loading ? 
                                    <TextField defaultValue={this.state.code} hintText={"Option Code"}
                                             key="Code"
                                             errorText={this.state.errorCode}
                                             onChange={e=>{ this.setState({code: e.target.value}) }}
                                  /> : ""}
                                </TableRowColumn>
                            </TableRow>
                            <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Traveler Type
                                </TableRowColumn>
                                <TableRowColumn>
                                     {!this.state.loading ? 
                                    <AgeBandsPopover onChange={ data=> { this.setState({travelerType: data})}}
                                                     values={this.props.travelerTypes}
                                                     value={!!this.state.travelerType ? (!!this.state.travelerType.typeOf ? this.state.travelerType.typeOf : this.state.travelerType.type) : ''}
                                    /> : ""}
                                    { this.state.errorAgeBand != '' ? <span style={{color:'red'}}>{this.state.errorAgeBand}</span> : ''}
                                </TableRowColumn>
                            </TableRow>
                            {/* <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Currency
                                </TableRowColumn>
                                <TableRowColumn style={styles.overflow}>
                                    <CurrencyPopover onChange={currency=> this.setState({retailCurrency})}
                                                     value={this.state.retailCurrency}
                                    />
                                </TableRowColumn>
                            </TableRow>
                            <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Retail Price
                                </TableRowColumn>
                                <TableRowColumn style={styles.overflow}>
                                  <TextField defaultValue={this.state.retailPrice} hintText={"Retail Price"}
                                             key="Price"
                                             errorText={this.state.errorPrice}
                                             onChange={e=>{ this.setState({retailPrice: e.target.value}) }}
                                  />
                                </TableRowColumn>
                            </TableRow> */}
                            <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Min Travelers
                                </TableRowColumn>
                                <TableRowColumn>
                                   {!this.state.loading ? 
                                    <input onChange={e=> { this.setState({minTravelers: e.target.value}) }}
                                         value={this.state.minTravelers}
                                         style={styles.numeric}
                                         type='number'
                                         min='1'
                                         max='99'
                                  /> : ""}
                                </TableRowColumn>
                            </TableRow>
                            <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Max Travelers
                                </TableRowColumn>
                                <TableRowColumn>
                                   {!this.state.loading ? 
                                    <input onChange={e=> { this.setState({maxTravelers: e.target.value}) }}
                                         value={this.state.maxTravelers}
                                         style={styles.numeric}
                                         type='number'
                                         min='1'
                                         max='99'
                                  /> : ""}
                                </TableRowColumn>
                            </TableRow>
                            <TableRow selectable={false} >
                                <TableRowColumn>
                                    Valid From
                                </TableRowColumn>
                                <TableRowColumn>
                                   {!this.state.loading ? 
                                    <DatePicker
                                       hintText="Enter Date"
                                       className="form-date-picker"
                                       formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                                       shouldDisableDate={(day) => { return this.shouldDisableValidFromDate(day) }}
                                       value={this.state.validFrom}
                                       container="inline"
                                       onChange={(ev, date) => { this.setState({ validFrom: date}) } }
                                 /> : ""}
                                </TableRowColumn>
                            </TableRow>
                            <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Valid Ending?
                                </TableRowColumn>
                                <TableRowColumn>
                                   {!this.state.loading ? 
                                    <Checkbox style={styles.checkbox}
                                            checked={this.state.validEnding}
                                            onCheck={(e, v)=> this.setState({validEnding: v })}
                                  /> : ""}
                                  { this.state.validEnding && !this.state.loading ? (
                                    <DatePicker
                                         hintText="Enter Date"
                                         className="form-date-picker"
                                         formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                                         shouldDisableDate={(day) => { return this.shouldDisableValidToDate(day) }}
                                         value={this.state.validUntil}
                                         container="inline"
                                         onChange={(ev, date) => { this.setState({ validUntil: date}) } }
                                   />
                                    ) : ''
                                  }
                                </TableRowColumn>
                            </TableRow>
                            {/* <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Days
                                </TableRowColumn>
                                <TableRowColumn>
                                  <DaysOpen days={this.state.days}
                                            onChange={v=> this.setState({days: v}) }
                                  />
                                </TableRowColumn>
                            </TableRow> */}
                            <TableRow selectable={false} >
                                <TableRowColumn>
                                    In Advanced Purchase?
                                </TableRowColumn>
                                <TableRowColumn>
                                  {!this.state.loading ? 
                                    <Checkbox style={styles.checkbox}
                                              checked={this.state.useInAdvancedPurchase}
                                              onCheck={(e, v)=> this.setState({useInAdvancedPurchase: v })}
                                  /> : "" }
                                  { this.state.useInAdvancedPurchase && ! this.state.loading ? (
                                    [<input onChange={e=> { this.setState({inAdvancePurchase: e.target.value}) }}
                                           value={this.state.inAdvancePurchase}
                                           style={styles.numeric}
                                           type='number'
                                           min='1'
                                           max='99'
                                          key='1'
                                    />, <span key='2'> Days</span>]
                                  ) : ""}
                                </TableRowColumn>
                            </TableRow>
                            {/* <TableRow selectable={false} >
                                <TableRowColumn style={styles.labelColumn}>
                                    Distribution
                                </TableRowColumn>
                                <TableRowColumn>
                                  <DistributionPopover onChange={v => this.setState({distribution: v})}
                                                       value={this.state.distribution} />
                                </TableRowColumn>
                            </TableRow> */}
                    </TableBody>
                 </Table>
                 <div style={styles.modalOptions}>
                    <RaisedButton
                          onTouchTap={e => { this.submit() } }
                          style={styles.modalOption}
                          primary={true}
                          label="OK"
                     />
                     <RaisedButton
                           onTouchTap={e => { this.cancel() } }
                           style={styles.modalOption}
                           label="Cancel"
                      />
                  </div>
                </div>
            </div>
          )
      } else {
          return (
              <RaisedButton
                    onTouchTap={e => { this.toggleForm() } }
                    style={(this.props.style  || {})}
                    label={this.props.label || "Add Product Option"}
                    className={this.props.secondary ? "secondary-button" : ""}
               />
          )
      }
  }
}

AddOptionModal.propTypes = {
  onSave: PropTypes.func.isRequired,
  travelerTypes: PropTypes.array.isRequired,
  secondary: PropTypes.bool,
  value: PropTypes.object
}
