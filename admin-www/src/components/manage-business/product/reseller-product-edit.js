import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import moment from 'moment-timezone'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import {
  isInternalStaff,
  enDash
} from '../../../util'
import CircularProgress from 'material-ui/CircularProgress'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import RadioButton from 'material-ui/RadioButton'
import Checkbox from 'material-ui/Checkbox'
import IconButton from 'material-ui/IconButton'
import ContentRemoveCircleOutline from 'material-ui/svg-icons/content/remove-circle-outline'
import ContentAddOutline from 'material-ui/svg-icons/content/add-circle-outline'
import AddOptionModal from '../product/add-option-modal'
import AgeBandsModal from '../product/age-bands-modal'
import OperationHoursModal from '../product/operation-hours-modal'
import ProductStatusPopover from '../../product-status-popover'

const styles = {
  formFooter: {
    width: '100%',
    height: '2em'
  },
  submitButton: {
    marginRight: '1em'
  },
  overflow: {
    overflow: 'visible'
  },
  hoursOfOperation: {

  },
  error: {
    marginLeft: '1em',
    color: 'red'
  }
}

class ResellerProductEdit extends Component {

  constructor(props) {
    super(props)
    this.state = {
        showAliases: true,
        clearForm: false,
        errorAliases: "",
        errorCode: "",
        errorName: "",
        errorPrice: "",
        resellerProductOptionCode: "",
        resellerProductCode: "",
        resellerProductName: "",
        aliases: [],
        status: "",
        supplier: "",
        id: "",
        name: "",
        code: "",
        option: "",
        supplierOptionCode: "",
        supplierCode: "",
        map: {
          from: {
            productNames: []
          }
        }
    }
  }

  componentDidMount () {

      this.setState({
        id: ""
      })
      console.log("mounting reseller product edit.. setting id to ''")
      let resellerId = this.props.adminResellerId != -1 ? this.props.adminResellerId : this.props.resellerId,
          catalogId = this.props.resellerCatalogId != -1 ? this.props.resellerCatalogId : this.props.params.catalogId
      this.props.getResellerCatalogItem(resellerId, catalogId, this.props.params.id)

  }

  componentWillReceiveProps ( nextProps ) {

    if (this.props.mode == "edit") {

        let catalogId = this.props.params.catalogId

        if ( this.props.resellerCatalogId != -1 )

          catalogId = this.props.resellerCatalogId
        
        if ( (this.props.productFetching && nextProps.productFetching == false) && nextProps.product != false) {
              
          let nextProduct = nextProps.product,
            map = nextProduct.map

            if (map.from.productNames == undefined)

              map.from.productNames = [""]
            
          this.setState({
            clearForm: true,
            showAliases: false,
            id: nextProduct.id,
            name: nextProduct.name,
            code: nextProduct.code,
            status: nextProduct.status,
            map: map,
            typeOf: nextProduct.typeOf,
            version: nextProduct.version,
            catalogId,
            itemId: nextProduct.map.to.catalogAddress.itemId,
            optionId: nextProduct.map.to.catalogAddress.optionId,
            supplier: nextProduct.supplierName,
            option: !!nextProduct.map.to.productOptionTitle ? nextProduct.map.to.productOptionTitle : "",
            supplierOptionCode: nextProduct.supplierOptionCode,
            supplierCode: nextProduct.supplierCode,
            resellerProductOptionCode: map.from.productOptionCode,
            resellerProductCode: map.from.productCode,
            resellerProductName: map.from.productNames[0]
          })

          setTimeout(()=>{
            this.setState({
                showAliases: true,
                clearForm: false
            })
          }, 75)

          if (!!nextProps.updateResult) {
            console.log("update result!!!")
            if (nextProps.updateResult.id == nextProduct.id) {
              this.setState({
                version: nextProps.updateResult.version
              })
            } 
          }

      }

    }

  }

  componentWillUpdate(nextProps, nextState) {

    if (this.props.productSaving == true && nextProps.productSaving == false) {
      console.log("finished updating!!!!", nextProps.updateResult)
      browserHistory.push("/app/manage-business/products")
    }

    if (this.state.errorAliases != "") {
      this.validateAliases(nextState.map.from.productNames, true) // clear errors
    }

  }

  updateProduct () {

    let resellerId = this.props.adminResellerId != -1 ? this.props.adminResellerId : this.props.resellerId,
        aliases = this.state.map.from.productNames,
        valid = true,
        names = []

    aliases.map(name=>{
      names.push(name.trim())
    })
    this.state.map.from.productNames = names
    
     let data = {
            catalogId: this.state.catalogId,
            code: this.state.code,
            id: this.state.id,
            map: this.state.map,
            name: this.state.name,
            status: this.state.status || "STATUS_LIVE",
            typeOf: this.state.typeOf,
            version: this.state.version
        }
    console.log("DATA", data)
     if (this.validate(this.state)) {
        console.log("valid")
        this.props.updateResellerCatalogItem(resellerId, this.props.params.catalogId, this.props.params.id, data)
        // call edit reseller product endpoint here
     }

  }

  validate (data) {

    let valid = this.validateAliases(data.map.from.productNames)
    return valid

  }

  validateAliases(aliases, clear = false) {

    let valid = true,
        errorAliases = ''

    aliases.map((v, k) => {
      console.log(aliases, v, k)
      if (v.trim() == '') {
        errorAliases = 'Aliases must have at least one character.'
      }
    })
    if (!clear || errorAliases == '') {
      this.setState({
        errorAliases
      })
    }
    return valid

  }

  isInternalStaff () {

    let role = this.props.role,
        initialRole = this.props.initialRole
    return isInternalStaff(role) || isInternalStaff(initialRole)

  }

  cancel () {
    browserHistory.goBack()
  }

  setAlias (a, i) {
    let aliases = this.state.map.from.productNames
    aliases[i] = a
    this.setState({
      map: Object.assign({}, this.state.map, {
        from: Object.assign({}, this.state.map.from, {
          productNames: aliases
        }),
      }),
      formModified: true
    })
  }
  addAlias () {
    let aliases = this.state.map.from.productNames
    aliases.push("")
    this.setState({
      map: Object.assign({}, this.state.map, {
        from: Object.assign({}, this.state.map.from, {
          productNames: aliases
        }),
      }),
      formModified: true
    })
  }
  removeAlias (i) {
    let aliases = this.state.map.from.productNames
    aliases.splice(i, 1)
   this.setState({
      map: Object.assign({}, this.state.map, {
        from: Object.assign({}, this.state.map.from, {
          productNames: aliases
        }),
      }),
      formModified: true,
      showAliases: false
    })
    setTimeout(()=>{
      this.setState({
        showAliases: true
      })
    }, 50)
  }
  setProductCode(code) {
    let map = this.state.map
    map.from.productCode = code
    this.setState({map}) 
  }
  setProductOptionCode(code) {
    let map = this.state.map
    map.from.productOptionCode = code
    this.setState({map}) 
  }
  setProductName(v) {
    let map = this.state.map
    map.from.productNames[0] = v
    this.setState({map}) 
  }
  renderAliases (aliases) {
    if (!aliases) {
      return ''
    }
    
    return aliases.map((v,k) => {
        if (k == 0) {
          return ""
        }
        return (
          <div style={styles.alias} key={k}>
          <TextField hintText={"Product Alias"}
                    defaultValue={v}
                    key={k}
                    onChange={e=>{ this.setAlias(e.target.value, k) }}
          />
          <IconButton onClick={ e=> this.removeAlias(k)}
                      iconStyle={styles.mediumIcon}
                      style={styles.medium}
          >
          <ContentRemoveCircleOutline />
        </IconButton>
        </div>
        )
    })
  }
  renderTable (mode, fetching) {
    if (fetching || this.state.clearForm) {
        return (
          <div className="loading-circle">
            <CircularProgress color={"#27367a"} size={96} />
          </div>
        )
      }
      return (
          <Table className="products-table">
            <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn></TableHeaderColumn>
                    <TableHeaderColumn></TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow selectable={false} >
                <TableRowColumn>
                    Supplier
                </TableRowColumn>
                <TableRowColumn style={styles.overflow}>
                  { enDash(this.props.supplierName) }
                </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                <TableRowColumn>
                    Option
                </TableRowColumn>
                <TableRowColumn style={styles.overflow}>
                  { enDash(this.props.itemOptionName) }
                </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                <TableRowColumn>
                    Supplier Option Code
                </TableRowColumn>
                <TableRowColumn style={styles.overflow}>
                  { this.state.map && this.state.map.to ? this.state.map.to.productOptionCode : '' }
                </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                <TableRowColumn>
                    Supplier Code
                </TableRowColumn>
                <TableRowColumn style={styles.overflow}>
                  { enDash(this.props.supplierCode) }
                </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn>
                      Your Product Option Code
                  </TableRowColumn>
                  <TableRowColumn style={styles.overflow}>
                    <TextField defaultValue={this.state.resellerProductOptionCode} hintText={"Product Option Code"}
                               key={"TourName"}
                               errorText={this.state.errorOptionCode}
                               onChange={e=>{ this.setProductOptionCode(e.target.value) }}
                      /> 
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                <TableRowColumn key={"1.1"}>
                    Your Product Code
                </TableRowColumn>
                <TableRowColumn style={styles.overflow}>
                     <TextField defaultValue={this.state.resellerProductCode} hintText={"Product Code"}
                               key={"productCode"}
                               errorText={this.state.errorCode}
                               onChange={e=>{ this.setProductCode(e.target.value) }}
                      />
                </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                  <TableRowColumn>
                      Your Product Name
                  </TableRowColumn>
                  <TableRowColumn>
                     <TextField defaultValue={this.state.resellerProductName} hintText={"Product Name"}
                               key={"productName"}
                               errorText={this.state.errorName}
                               onChange={e=>{ this.setProductName(e.target.value)}}
                      />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                  <TableRowColumn>
                      Aliases
                  </TableRowColumn>
                  <TableRowColumn>
                    <span style={styles.error}>{ this.state.errorAliases }</span>
                    { this.state.showAliases ? this.renderAliases(this.state.map.from.productNames) : '' }
                    <IconButton
                      iconStyle={styles.mediumIcon}
                      style={styles.medium}
                      onTouchTap={ e=> this.addAlias()}
                     >
                     <ContentAddOutline />
                    </IconButton>
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                <TableRowColumn>
                    Status
                </TableRowColumn>
                <TableRowColumn>
                  <ProductStatusPopover onStatus={ (value, name) => { this.setState({ status: value }) }} 
                                        status={ this.state.status }  
                  />
                </TableRowColumn>
              </TableRow>
            </TableBody>
         </Table>
      )
  }
  render() {
      let fetching = this.props.productFetching;
    return (
      <div className="product-edit" >
        <Card className="inner-edit">
                <h2 style={{color:"#434343"}}>Edit {this.state.name != "" ? this.state.name : "Product"}</h2>
                { this.renderTable(this.props.mode) }
                <div style={styles.formFooter}></div>
                <RaisedButton onClick={e => { this.cancel() }}
                              className="view-report"
                              style={styles.submitButton}
                              label="Cancel"
                />
                <RaisedButton onClick={e => { this.updateProduct() }}
                              className="view-report"
                              style={styles.submitButton}
                              label="Save Changes"
                />
        </Card>
      </div>
    )
  }
}

import { connect } from 'react-redux'
import { 
  getReseller,
  getResellerCatalogItem,
  updateResellerCatalogItem
} from '../../../ax-redux/actions/resellers'


export default connect(
  state => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      mode: (state.routing.locationBeforeTransitions.pathname.search("/add") > -1 ? "add" : "edit"),
      role: state.app.role,
      initialRole: state.app.initialRole,
      adminResellerId: state.reportFilters.adminResellerId,
      resellerId: state.app.user.resellerId,
      productSaving: state.resellers.updateCatalogItem.fetching || state.resellers.createCatalogItem.fetching,
      product: !!state.resellers.getCatalogItem.data ? state.resellers.getCatalogItem.data.item : false,
      itemOptionName: !!state.resellers.getCatalogItem.data ? state.resellers.getCatalogItem.data.itemOptionName : "",
      supplierCode:  !!state.resellers.getCatalogItem.data ? state.resellers.getCatalogItem.data.supplierCode : "",
      supplierName: !!state.resellers.getCatalogItem.data ? state.resellers.getCatalogItem.data.supplierName : "",
      productFetching: state.resellers.getCatalogItem.fetching,
      resellerCatalogId: state.reportFilters.resellerCatalogId,
      updateResult: !!state.resellers.updateCatalogItem.data ? state.resellers.updateCatalogItem.data.catalogItem : false
    }
  },
  dispatch => {
    return {
     getResellerCatalogItem: (resellerId, catalogId, id) => {
          dispatch(getResellerCatalogItem(resellerId, catalogId, id))
      },
      updateResellerCatalogItem: (resellerId, catalogId, id, data) => {
        dispatch(updateResellerCatalogItem(resellerId, catalogId, id, data))
      },
      listResellerCatalogs: (resellerId) => {
        dispatch(listResellerCatalogs(resellerId))
      },
    }
  }
)(ResellerProductEdit)
