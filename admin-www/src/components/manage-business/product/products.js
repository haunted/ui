import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card } from 'material-ui/Card';
import { Tabs, Tab } from 'material-ui/Tabs';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { browserHistory } from 'react-router';
import ProductsTable from './ProductsTable';
import * as resellerActions from '../../../ax-redux/actions/resellers';
import * as suppliersActions from '../../../ax-redux/actions/suppliers';
import * as productsActions from '../../../ax-redux/actions/products';
import {
  isInternalStaff,
  isSupplierStaff,
  isResellerStaff,
  isPassIssuerStaff,
} from '../../../util';

const styles = {
  addProductButton: {
    float: 'right',
    marginTop: '1em',
    marginRight: '1em',
  },
  filterProducts: {
    verticalAlign: 'top',
    marginTop: '0.75em',
    marginBottom: '0.5em',
    marginLeft: '0.5em',
    backgroundColor: '#fff',
    paddingLeft: '1em',
    boxShadow: 'inset 1px 1px 4px rgba(0,0,0,0.15)',
    borderRadius: '2px',
    height: '46px',
  },
};

const addProduct = () => browserHistory.push('/app/manage-business/products/add');
const getProductName = p => (
  (p.map && p.map.from && p.map.from.productNames && p.map.from.productNames[0]) || p.name
);

class Products extends Component {
  constructor(props) {
    super(props);

    const {
      resellerCatalogId,
      supplierCatalogId,
      role,
    } = this.props;

    const limit = 40;
    const offset = 0;
    const tab = isInternalStaff(role) || isSupplierStaff(role) ? 'supplier' : 'connected';

    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.changePage = this.changePage.bind(this);
    this.filterProducts = this.filterProducts.bind(this);
    this.loadProductsForTab = this.loadProductsForTab.bind(this);
    this.changeTab = this.changeTab.bind(this);
    this.clearFilter = this.clearFilter.bind(this);

    this.state = {
      offset,
      limit,
      tab,
      supplierCatalogId,
      resellerCatalogId,
      firstLoad: false,
      filter: '',
    };
  }

  componentWillMount() {
    this.loadProductsForTab();
  }

  componentWillReceiveProps(nextProps) {
    const {
      supplierCatalogId,
      resellerCatalogId,
    } = this.props;

    if (
      supplierCatalogId !== nextProps.supplierCatalogId ||
      resellerCatalogId !== nextProps.resellerCatalogId
    ) {
      this.loadProductsForTab(nextProps);
    }
  }

  handleFilterChange(e) {
    this.setState({ filter: e.target.value });
  }

  loadProductsForTab(props) {
    const {
      resellerCatalogId,
      supplierCatalogId,
      adminResellerId,
      adminSupplierId,
      resellerId,
      supplierId,
      listPendingProducts,
      listResellerCatalogItems,
      supplierCatalogItemsFetching,
      listSupplierCatalogItems,
    } = props || this.props;

    const {
      tab,
      limit,
      offset,
    } = this.state;

    const defaultFilters = {
      offset,
      limit,
    };

    const actualSupplierId = adminSupplierId || supplierId;
    const actualResellerId = adminResellerId || resellerId;

    switch (tab) {
      case 'supplier':
        if (!supplierCatalogItemsFetching && supplierCatalogId) {
          listSupplierCatalogItems(actualSupplierId, supplierCatalogId, defaultFilters);
        }
        break;
      case 'pending':
        if (resellerCatalogId) {
          listPendingProducts(actualResellerId, resellerCatalogId, defaultFilters);
        }
        break;
      case 'connected':
        if (resellerCatalogId) {
          listResellerCatalogItems(actualResellerId, resellerCatalogId, defaultFilters);
        }
        break;
      default:
    }
  }

  changeTab(tab) {
    this.setState({
      tab,
      offset: 0,
      filter: '',
    }, this.loadProductsForTab);
  }

  clearFilter() {
    this.setState({ filter: '' });
  }

  changePage(direction) {
    const {
      offset,
      limit,
    } = this.state;

    this.setState({
      offset: parseInt(offset, 10) + (parseInt(limit, 10) * direction),
    }, this.loadProductsForTab);
  }

  filterProducts() {
    const {
      pendingProducts,
      resellerCatalogItems,
      supplierCatalogItems,
    } = this.props;

    const {
      filter,
      tab,
    } = this.state;

    switch (tab) {
      case 'pending':
        return pendingProducts
          .filter(p => p.name.toLowerCase().includes(filter.toLowerCase()))
          .sort((a, b) => a.supplierName.localeCompare(b.supplierName))
          .sort((a, b) => a.name.localeCompare(b.name));
      case 'connected':
        return resellerCatalogItems
          .filter(p => p.name.toLowerCase().includes(filter.toLowerCase()))
          .map(p => ({
            ...p,
            productName: getProductName(p),
            retailPrice: p.retailPrice || 0,
          }));
      case 'supplier':
        return supplierCatalogItems
          .filter(p => p.name.toLowerCase().includes(filter.toLowerCase()))
          .map(p => ({
            ...p,
            name: p.name || p.tourName,
            retailPrice: p.retailPrice || 0,
          }));
      default:
    }

    return [];
  }

  renderProducts() {
    const {
      resellerId,
      adminResellerId,
      supplierId,
      adminSupplierId,
      resellerCatalogId,
      supplierCatalogId,
    } = this.props;

    const { tab, filter } = this.state;

    return (
      <div style={{ backgroundColor: '#f5f5f5' }}>
        <TextField
          hintText="Filter Products"
          defaultValue={filter}
          style={styles.filterProducts}
          className="search-text"
          onChange={this.handleFilterChange}
        />

        {tab === 'supplier' && (
          <RaisedButton
            onClick={addProduct}
            style={styles.addProductButton}
            label="Add New Product"
            primary
          />
        )}

        <ProductsTable
          type={tab}
          products={this.filterProducts()}
          resellerId={adminResellerId || resellerId}
          resellerCatalogId={resellerCatalogId}
          supplierId={adminSupplierId || supplierId}
          supplierCatalogId={supplierCatalogId}
          refreshData={this.loadProductsForTab}
        />
      </div>
    );
  }

  renderTabs() {
    const {
      productsFetching,
      supplierCatalogItemsFetching,
      resellerCatalogItemsFetching,
      role,
    } = this.props;

    const { tab } = this.state;

    const fetching = productsFetching ||
      supplierCatalogItemsFetching ||
      resellerCatalogItemsFetching;

    const tabs = [
      <Tab
        label="Connected Products"
        value="connected"
        key="connected"
      >
        {this.renderProducts()}
      </Tab>,
      <Tab
        label="Available Products"
        value="pending"
        key="pending"
      >
        {this.renderProducts()}
      </Tab>,
    ];

    if (isInternalStaff(role)) {
      tabs.unshift(
        <Tab
          label="Supplier Products"
          value="supplier"
          key="supplier"
        >
          {this.renderProducts()}
        </Tab>,
      );
    }

    if (isSupplierStaff(role)) {
      return this.renderProducts();
    }

    if (isInternalStaff(role) || isResellerStaff(role) || isPassIssuerStaff(role)) {
      return (
        <Tabs value={tab} onChange={this.changeTab}>
          {!fetching && tabs}
        </Tabs>
      );
    }

    return (
      <div>No products to show</div>
    );
  }

  renderPaginationControls() {
    const {
      productsFetching,
      supplierCatalogItems,
      resellerCatalogItems,
      pendingProducts,
    } = this.props;

    const {
      tab,
      offset,
      limit,
      filter,
    } = this.state;

    const allProducts = {
      supplier: supplierCatalogItems,
      connected: resellerCatalogItems,
      pending: pendingProducts,
    };

    const products = allProducts[tab];

    if (filter) {
      return (
        <div className="pagination-controls">
          <RaisedButton label="Clear Search" onClick={this.clearFilter} />
        </div>
      );
    }

    return (
      <div className="pagination-controls">
        {offset > 0 && (
          <RaisedButton
            label="Previous Page"
            onClick={() => this.changePage(-1)}
          />
        )}

        <div style={{ display: 'inline-block', marginRight: '1em' }} />

        {!productsFetching && products.length === limit && (
          <RaisedButton label="Next Page" onClick={() => this.changePage(1)} />
        )}
      </div>
    );
  }

  render() {
    return (
      <div className="reseller-view" >
        <Card>
          <div className="reseller-view-section reseller-products">
            {this.renderTabs()}
          </div>
        </Card>

        {this.renderPaginationControls()}
      </div>
    );
  }
}

Products.propTypes = {
  role: PropTypes.string.isRequired,
  productsFetching: PropTypes.bool.isRequired,
  supplierCatalogItemsFetching: PropTypes.bool.isRequired,
  resellerCatalogItemsFetching: PropTypes.bool.isRequired,
  supplierCatalogItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  resellerCatalogItems: PropTypes.arrayOf(PropTypes.object).isRequired,
  pendingProducts: PropTypes.arrayOf(PropTypes.object).isRequired,
  adminSupplierId: PropTypes.string,
  adminResellerId: PropTypes.string,
  supplierId: PropTypes.string,
  resellerId: PropTypes.string,
  supplierCatalogId: PropTypes.string,
  resellerCatalogId: PropTypes.string,
  listPendingProducts: PropTypes.func.isRequired,
  listSupplierCatalogItems: PropTypes.func.isRequired,
  listResellerCatalogItems: PropTypes.func.isRequired,
};

Products.defaultProps = {
  adminSupplierId: null,
  adminResellerId: null,
  adminPassIssuerId: null,
  supplierCatalogId: null,
  resellerCatalogId: null,
  supplierId: null,
  resellerId: null,
};

const mapStateToProps = (state) => {
  const {
    app,
    suppliers,
    resellers,
    products,
    reportFilters,
  } = state;

  return {
    supplierCatalogItems: suppliers.listCatalogItems.data ?
      suppliers.listCatalogItems.data.catalogItems :
      [],
    resellerCatalogItems: resellers.listCatalogItems.data ?
      resellers.listCatalogItems.data.catalogItems :
      [],
    pendingProducts: products.listPendingProducts.data ?
      products.listPendingProducts.data.products :
      [],
    productsFetching: products.getSupplierProducts.fetching ||
      products.listProducts.fetching ||
      resellers.listCatalogItems.fetching ||
      products.listPendingProducts.fetching,
    supplierCatalogItemsFetching: suppliers.listCatalogItems.fetching,
    resellerCatalogItemsFetching: resellers.listCatalogItems.fetching,
    supplierId: app.user.supplierId,
    resellerId: app.user.resellerId,
    role: app.role,
    adminSupplierId: reportFilters.adminSupplierId !== -1 ?
      reportFilters.adminSupplierId :
      null,
    adminResellerId: reportFilters.adminResellerId !== -1 ?
      reportFilters.adminResellerId :
      null,
    adminPassIssuerId: reportFilters.adminPassIssuerId !== -1 ?
      reportFilters.adminPassIssuerId :
      null,
    supplierCatalogId: reportFilters.supplierCatalogId !== -1 ?
      reportFilters.supplierCatalogId :
      null,
    resellerCatalogId: reportFilters.resellerCatalogId !== -1 ?
      reportFilters.resellerCatalogId :
      null,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  listPendingProducts: productsActions.listPendingProducts,
  listSupplierCatalogItems: suppliersActions.listSupplierCatalogItems,
  listResellerCatalogItems: resellerActions.listResellerCatalogItems,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Products);
