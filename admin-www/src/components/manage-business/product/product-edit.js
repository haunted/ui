import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Card } from 'material-ui/Card';
import CircularProgress from 'material-ui/CircularProgress';
import * as supplierActions from '../../../ax-redux/actions/suppliers';
import SupplerProductForm from '../../forms/SupplerProductForm';
import * as orgSettingsActions from '../../../ax-redux/actions/org-settings';


class ProductEdit extends Component {
  componentWillMount() {
    const {
      orgCode,
      editMode,
      params,
      getSupplierCatalogItem,
      readSettings,
    } = this.props;

    readSettings(orgCode, 'TYPE_SUPPLIER');

    if (editMode) {
      getSupplierCatalogItem(params.supplierId, params.catalogId, params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      orgCode,
      readSettings,
    } = this.props;

    if (orgCode !== nextProps.orgCode) {
      readSettings(nextProps.orgCode, 'TYPE_SUPPLIER');
    }
  }

  render() {
    const {
      editMode,
      product,
      productFetching,
      travelerTypesFetching,
    } = this.props;

    const fetching = productFetching || travelerTypesFetching || (editMode && !product);

    const formProps = {
      editMode,
    };

    if (!productFetching && editMode && product) {
      formProps.initialValues = {
        name: product.name,
        status: product.status,
        typeOf: product.typeOf,
        options: product.options,
      };
    }

    return (
      <div className="product-edit" >
        <Card className="inner-edit">
          {fetching ? (
            <div className="loading-circle">
              <CircularProgress color={'#27367a'} size={96} />
            </div>
          ) : (
            <SupplerProductForm {...formProps} />
          )}
        </Card>
      </div>
    );
  }
}

ProductEdit.propTypes = {
  orgCode: PropTypes.string,
  editMode: PropTypes.bool.isRequired,
  productFetching: PropTypes.bool.isRequired,
  travelerTypesFetching: PropTypes.bool.isRequired,
  product: PropTypes.shape({}),
  params: PropTypes.shape({}).isRequired,
  getSupplierCatalogItem: PropTypes.func.isRequired,
  readSettings: PropTypes.func.isRequired,
};

ProductEdit.defaultProps = {
  orgCode: '',
  product: null,
};

const mapStateToProps = (state) => {
  const {
    app,
    routing,
    suppliers,
    reportFilters,
    orgSettings,
  } = state;

  return {
    editMode: !routing.locationBeforeTransitions.pathname.includes('/add'),
    productFetching: suppliers.getCatalogItem.fetching,
    product: suppliers.getCatalogItem.data ? suppliers.getCatalogItem.data.item : null,
    orgCode: app.user ? app.user.account.orgCode : '',
    role: app.role,
    supplierId: app.user.supplierId,
    productSaving: suppliers.updateCatalogItem.fetching || suppliers.createCatalogItem.fetching,
    supplierCatalogId: reportFilters.supplierCatalogId,
    adminSupplierId: reportFilters.adminSupplierId,
    travelerTypesFetching: orgSettings.readOrgSettings.fetching,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  getSupplierCatalogItem: supplierActions.getSupplierCatalogItem,
  readSettings: orgSettingsActions.readSettings,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProductEdit);
