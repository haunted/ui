import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import moment from 'moment-timezone'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import {isInternalStaff} from '../../../util'
import CircularProgress from 'material-ui/CircularProgress'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import RadioButton from 'material-ui/RadioButton'
import Checkbox from 'material-ui/Checkbox'
import AddOptionModal from '../product/add-option-modal'
import AgeBandsModal from '../product/age-bands-modal'
import OperationHoursModal from '../product/operation-hours-modal'
import {formatCurrency, capitalize} from '../../../util'

const styles = {
  formFooter: {
    width: '100%',
    height: '2em'
  },
  submitButton: {
    marginRight: '1em',
    float: 'right'
  },
  overflow: {
    overflow: 'visible'
  },
  hoursOfOperation: {

  }
}

class ProductDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
        errorCode: "",
        errorName: "",
        errorPrice: "",
        code: "",
        options: [],
        name: "",
        price: "",
        distributed: false,
        active: false,
        typeOf: 'single',
        openingHours: [],
        closedDates: [],
        defaultOpeningHours: [],
        defaultClosedDates: [],
        travelerTypes: [],
        defaultAgeBands: [
          { type: "Adult", from: 0, to: 100 }
        ],
        overrideDefaultHours: false,
        overrideSystemAgeBands: false
    }
  }
  componentDidMount () {
    this.props.getSupplierCatalogItem(this.props.params.supplierId, this.props.params.catalogId, this.props.params.id)
    this.props.listPriceListsByCatalogItem(this.props.params.supplierId, this.props.params.catalogId, this.props.params.id)
    //this.props.listSupplierCatalogs(this.props.params.supplierId)
  }
  componentWillUpdate(nextProps, nextState) {
    // map fields to view
    if (this.props.catalogItemFetching == true && nextProps.catalogItemFetching == false) {
      let nextProduct = nextProps.catalogItem
      this.setState({
        id: nextProduct.id,
        code: nextProduct.code,
        name: nextProduct.name,
        status: nextProduct.status,
        catalogId: nextProduct.catalogId,
        options: nextProduct.options,
        typeOf: nextProduct.typeOf
      })
    }
    /*/ Clear Errors /*/
    if (this.state.errorCode && this.state.code != nextState.code) {
      this.setState({ errorCode: false })
    }
    if (this.state.errorName && this.state.name != nextState.name) {
      this.setState({ errorName: false })
    }
    if (this.state.errorPrice && this.state.price != nextState.price) {
      this.setState({ errorPrice: false })
    }
    console.log(nextState.defaultAgeBands)
  }
  isInternalStaff () {
    let role = this.props.role
    return isInternalStaff(role)
  }
  setProductType (type) {
    this.setState({
      typeOf: type
    })
  }
  cancel () {
    browserHistory.goBack()
  }
  formatTime(time) {
    let date = new Date()
    date.setHours(0)
    date.setMinutes(0)
    date.setSeconds(0)
    let dateTime = new Date(date.getTime() + time)
    return moment(dateTime).tz(TZ).format("hh:mm A")
  }
  renderHoursOfOperation (openingHours, closedDates) {
    let days = ['Mon', 'Tues', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    return (
      <section style={styles.hoursOfOperation}>
        {
          openingHours.map((hours, i) => {
            let dayLabel = '',
                range = [],
                ranges = []
            hours.days.map((day, i) =>{
              if (day == true) {
                range.push(i)
              } else {
                if (range.length > 0) {
                  ranges.push(range)
                  range = []
                }
              }
            })
            if (range.length > 0) {
              ranges.push(range)
            }
            ranges.map((range, i)=>{
              if (range.length != 1) {
                 dayLabel += days[range[0]]+'-'+days[range[range.length-1]]+' '
              } else {
                dayLabel += days[range[0]]+' '
              }
            })
            return (
              <div key={i}>
                {`${dayLabel} ${this.formatTime(hours.from)} - ${this.formatTime(hours.to)}`}
              </div>
            )
          })
        }
        <section>
          {
            closedDates.map((date, i) => {
              return (
                <span key={i}>{`Closed ${moment(date).tz(TZ).format("DD-MMM-YYYY")} `}</span>
              )
            })
          }
        </section>
      </section>
    )
  }
  toggleDefaultHours (v) {
    this.setState({
      overrideDefaultHours: v
    })
    if (!v) {
      this.setState({
        openingHours: [],
        closedDates: [],
        defaultOpeningHours: [],
        defaultClosedDates: []
      })
    }
  }
  toggleDefaultAgeBands (v) {
    this.setState({overrideSystemAgeBands: v })
    if (!v) {
      this.setState({
        travelerTypes: [],
        defaultAgeBands: [
          { type: "Adult", from: 0, to: 100 }
        ]
      })
    }
  }
  renderAgeBands (bands) {
    return bands.map((band, b)=>{
      return (
        <div key={b}>{`${band.type} ${band.from}YR - ${band.to}YR`}</div>
      )
    })
  }
  renderTable (mode, fetching) {
    if (fetching) {
        return (
          <div className="loading-circle">
            <CircularProgress color={"#27367a"} size={96} />
          </div>
        )
      }
      return (
          <Table className="products-table">
            <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn></TableHeaderColumn>
                    <TableHeaderColumn></TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow selectable={false} >
                  <TableRowColumn>
                      Name
                  </TableRowColumn>
                  <TableRowColumn style={styles.overflow}>
                            { this.state.name }
                  </TableRowColumn>
              </TableRow>,
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>
                            Type
                  </TableRowColumn>
                  <TableRowColumn style={styles.overflow}>
                          { `${capitalize(this.state.typeOf)} Product` }
                  </TableRowColumn>
              </TableRow>
              {/* <TableRow selectable={false} >
                  <TableRowColumn>
                    Hours of Operation
                  </TableRowColumn>
                  <TableRowColumn>
                            {
                              this.state.overrideDefaultHours ?
                              this.renderHoursOfOperation(this.state.openingHours, this.state.closedDates) :
                              this.renderHoursOfOperation(this.state.defaultOpeningHours, [])
                            }
                    </TableRowColumn>
              </TableRow>, */}
              <TableRow selectable={false} >
                  <TableRowColumn>
                    Traveler Types
                  </TableRowColumn>
                  <TableRowColumn>
                  {
                    this.state.overrideSystemAgeBands ?
                    this.renderAgeBands(this.state.travelerTypes) :
                    this.renderAgeBands(this.state.defaultAgeBands)
                  }
                  </TableRowColumn>
                </TableRow>
            </TableBody>
         </Table>
      )
  }
  renderProductOptions (data) {
      return (
          <Table className="products-table">
            <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>Code</TableHeaderColumn>
                    <TableHeaderColumn>Name</TableHeaderColumn>
                    <TableHeaderColumn>Price</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {
                !!data && data.map((option, o)=> (
                  <TableRow key={o} selectable={false} >
                      <TableRowColumn>
                        {option.code}
                      </TableRowColumn>
                      <TableRowColumn>
                        {option.title}
                      </TableRowColumn>
                      <TableRowColumn>
                        {/* {formatCurrency(option.currency, option.retailPrice)} */}
                      </TableRowColumn>
                  </TableRow>
                ))
              }
            </TableBody>
         </Table>
      )
  }
  renderProductResellers (data) {
    return (
      <Table className="products-table">
        <TableHeader  adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
            <TableRow>
                <TableHeaderColumn>Reseller Name</TableHeaderColumn>
                <TableHeaderColumn></TableHeaderColumn>
            </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            data.map((reseller, o)=> (
              <TableRow key={o} selectable={false} >
                  <TableRowColumn>
                    {reseller.name}
                  </TableRowColumn>
                  <TableRowColumn>
                    {/* <FlatButton onClick={e => { this.editReseller(o) }} // disabled for now
                                label="Edit"
                    /> */}
                  </TableRowColumn>
              </TableRow>
            ))
          }
        </TableBody>
     </Table>
    )
  }
  renderPriceLists (data) {
    return (
      <Table className="products-table">
        <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
            <TableRow>
                <TableHeaderColumn>Name</TableHeaderColumn>
                <TableHeaderColumn>Commission</TableHeaderColumn>
            </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            !!data && data.map((priceList, o)=> (
              <TableRow key={o} selectable={false} >
                  <TableRowColumn>
                    {priceList.name}
                  </TableRowColumn>
                  <TableRowColumn>
                    {priceList.commission}
                  </TableRowColumn>
              </TableRow>
            ))
          }
        </TableBody>
     </Table>
    )
  }
  render() {
      let fetching = this.props.resellerFetching ||
                     this.props.catalogItemFetching
    return (
      <div className="product-edit" >
        <Card className="inner-edit">
                <h2 style={{color:"#434343"}}>{ this.props.catalogItem != false ? this.props.catalogItem.name : "Product Details"}</h2>
                { this.renderTable(this.props.mode, fetching) }
                <h3>Product Options</h3>
                { this.props.catalogItem != false ? this.renderProductOptions(this.state.options) : "" }
                <h3>Price Lists</h3>
                { this.renderPriceLists(this.props.priceLists) }
                <div style={styles.formFooter}>
                  <RaisedButton onClick={e => { this.cancel() }}
                                className="view-report"
                                label="Back"
                  />
                </div>
        </Card>
      </div>
    )
  }
}

import { connect } from 'react-redux'
import { getReseller } from '../../../ax-redux/actions/resellers'
import {
  readProduct,
  updateProduct,
  createProduct
} from '../../../ax-redux/actions/products'
import {
  listPriceLists,
  listPriceListsByCatalogItem
} from '../../../ax-redux/actions/price-lists'
import {
  createSupplierCatalogItem,
  getSupplierCatalogItem
} from '../../../ax-redux/actions/suppliers'
export default connect(
  state => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      mode: (state.routing.locationBeforeTransitions.pathname.search("/add") > -1 ? "add" : "edit"),
      role: state.app.role,
      reseller: state.resellers.get.data,
      catalogItem: !!state.suppliers.getCatalogItem.data ? state.suppliers.getCatalogItem.data.item : false,
      catalogItemFetching: state.suppliers.getCatalogItem.fetching,
      resellerFetching: state.resellers.get.fetching,
      priceLists: state.priceLists.listPriceListsByCatalogItem.data != false ? state.priceLists.listPriceListsByCatalogItem.data.priceLists : false,
      priceListsFetching: state.priceLists.listPriceListsByCatalogItem.fetching,
      supplierId: state.app.user.supplierId,
      adminSupplierId: state.reportFilters.adminSupplierId
    }
  },
  dispatch => {
    return {
      readProduct: (id) => {
          dispatch(readProduct(id))
      },
      getSupplierCatalogItem: (supplierId, catalogId, id) => {
          dispatch(getSupplierCatalogItem(supplierId, catalogId, id))
      },
      listPriceLists: (supplierId) => {
          dispatch(listPriceLists(supplierId))
      },
      listPriceListsByCatalogItem: (supplierId, catalogId, id) => {
          dispatch(listPriceListsByCatalogItem(supplierId, catalogId, id))
      }
    }
  }
)(ProductDetail)
