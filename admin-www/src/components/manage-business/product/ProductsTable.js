import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import DeleteResellerCatalogItemButton from '../../delete-reseller-catalog-item-button';
import {
  formatCurrency,
  enDash,
} from '../../../util';

const styles = {
  productName: {
    cursor: 'pointer',
    width: '320px',
  },
  priceCol: {
    width: '96px',
  },
  checkboxCol: {
    width: '48px',
  },
  editProduct: {
    width: '134px',
    marginRight: '1em',
    overflow: 'visible',
  },
};

class ProductsTable extends Component {
  constructor(props) {
    super(props);

    this.viewSupplierProduct = this.viewSupplierProduct.bind(this);
    this.editSupplierProduct = this.editSupplierProduct.bind(this);
    this.editResellerProduct = this.editResellerProduct.bind(this);
  }

  setUpResellerProduct(id, optCode) {
    const { resellerCatalogId } = this.props;

    browserHistory.push(`/app/manage-business/products/reseller/setup/${resellerCatalogId}/${id}/${optCode}`);
  }

  viewSupplierProduct(id) {
    const {
      supplierId,
      supplierCatalogId,
    } = this.props;

    browserHistory.push(`/app/manage-business/product/detail/${supplierId}/${supplierCatalogId}/${id}`);
  }

  editSupplierProduct(id) {
    const {
      supplierId,
      supplierCatalogId,
    } = this.props;

    browserHistory.push(`/app/manage-business/products/edit/${supplierId}/${supplierCatalogId}/${id}`);
  }

  editResellerProduct(id) {
    const { resellerCatalogId } = this.props;

    browserHistory.push(`/app/manage-business/products/reseller/edit/${resellerCatalogId}/${id}`);
  }

  renderSupplierProducts() {
    const { products } = this.props;

    return (
      <Table className="products-table">
        <TableHeader
          adjustForCheckbox={false}
          displayRowCheckbox={false}
          enableSelectAll={false}
          displaySelectAll={false}
        >
          <TableRow>
            <TableHeaderColumn>Product Code</TableHeaderColumn>
            <TableHeaderColumn style={styles.productName}>Product Name</TableHeaderColumn>
            <TableHeaderColumn>Product Options</TableHeaderColumn>
            <TableHeaderColumn style={styles.priceCol}>Retail Price</TableHeaderColumn>
            <TableHeaderColumn style={styles.checkboxCol}>Distributed</TableHeaderColumn>
            <TableHeaderColumn style={styles.checkboxCol}>Active</TableHeaderColumn>
            <TableHeaderColumn style={styles.editProduct}>Actions</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {products.map(p => (
            <TableRow key={p.id} selectable={false} >
              <TableRowColumn>
                {p.code}
              </TableRowColumn>
              <TableRowColumn style={styles.productName}>
                <FlatButton
                  onClick={() => this.viewSupplierProduct(p.id)}
                  className="view-report"
                  label={p.name}
                />
              </TableRowColumn>
              <TableRowColumn>
                <span>
                  {p.options && p.options.length > 0 ? `Yes (See ${p.options.length} Options)` : 'No'}
                </span>
              </TableRowColumn>
              <TableRowColumn style={styles.priceCol}>
                {formatCurrency('USD', p.retailPrice)}
              </TableRowColumn>
              <TableRowColumn style={styles.checkboxCol}>
                <input
                  type="checkbox"
                  checked={p.distributed}
                  disabled
                />
              </TableRowColumn>
              <TableRowColumn style={styles.checkboxCol}>
                <input
                  type="checkbox"
                  checked={p.active}
                  disabled
                />
              </TableRowColumn>
              <TableRowColumn style={styles.editProduct}>
                <RaisedButton
                  onClick={() => { this.editSupplierProduct(p.id); }}
                  className="view-report secondary-button"
                  label="Edit Product"
                  secondary
                />
              </TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    );
  }

  renderConnectedProducts() {
    const {
      products,
      resellerCatalogId,
      resellerId,
      refreshData,
    } = this.props;

    return (
      <Table className="products-table">
        <TableHeader
          adjustForCheckbox={false}
          displayRowCheckbox={false}
          enableSelectAll={false}
          displaySelectAll={false}
        >
          <TableRow>
            <TableHeaderColumn>Your Product Code</TableHeaderColumn>
            <TableHeaderColumn style={styles.productName}>Product Name</TableHeaderColumn>
            <TableHeaderColumn>Product Code</TableHeaderColumn>
            <TableHeaderColumn>Product Option</TableHeaderColumn>
            <TableHeaderColumn>Product Option Code</TableHeaderColumn>
            <TableHeaderColumn style={styles.editProduct} />
            <TableHeaderColumn style={styles.editProduct} />
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {products.map(p => (
            <TableRow key={p.id} selectable={false} >
              <TableRowColumn>
                {p.map ? enDash(p.map.from.productCode) : ''}
              </TableRowColumn>
              <TableRowColumn style={styles.productName}>
                {enDash(p.productName)}
              </TableRowColumn>
              <TableRowColumn>
                {p.map ? enDash(p.map.to.productCode) : ''}
              </TableRowColumn>
              <TableRowColumn>
                {p.map && p.map.to.productOptionTitle ? enDash(p.map.to.productOptionTitle) : ''}
              </TableRowColumn>
              <TableRowColumn>
                {p.map ? enDash(p.map.to.productOptionCode) : ''}
              </TableRowColumn>
              <TableRowColumn style={styles.editProduct}>
                <DeleteResellerCatalogItemButton
                  resellerId={resellerId}
                  callback={refreshData}
                  catalogId={resellerCatalogId}
                  version={p.version}
                  id={p.id}
                />
              </TableRowColumn>
              <TableRowColumn style={styles.editProduct}>
                <RaisedButton
                  onClick={() => { this.editResellerProduct(p.id); }}
                  className="view-report secondary-button"
                  label="Edit Product"
                  secondary
                />
              </TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    );
  }

  renderPendingProducts() {
    const { products } = this.props;

    return (
      <Table className="products-table">
        <TableHeader
          adjustForCheckbox={false}
          displayRowCheckbox={false}
          enableSelectAll={false}
          displaySelectAll={false}
        >
          <TableRow>
            <TableHeaderColumn>Supplier</TableHeaderColumn>
            <TableHeaderColumn>Product Name</TableHeaderColumn>
            <TableHeaderColumn>Product Code</TableHeaderColumn>
            <TableHeaderColumn>Product Option</TableHeaderColumn>
            <TableHeaderColumn>Product Option Code</TableHeaderColumn>
            <TableHeaderColumn style={styles.editProduct}/>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {products.map(p => (
            p.options.length ? p.options.map((o, i) => (
              <TableRow key={`${p.id}-${i}`} selectable={false}>
                <TableRowColumn title={enDash(p.supplierName)}>
                  {enDash(p.supplierName)}
                </TableRowColumn>
                <TableRowColumn>
                  {enDash(p.priceListName)}
                </TableRowColumn>
                <TableRowColumn title={p.name}>
                  {p.name}
                </TableRowColumn>
                <TableRowColumn>
                  {p.code}
                </TableRowColumn>
                <TableRowColumn>
                  {o.title}
                </TableRowColumn>
                <TableRowColumn>
                  {o.code}
                </TableRowColumn>
                <TableRowColumn style={styles.editProduct}>
                  <RaisedButton
                    onClick={() => this.setUpResellerProduct(p.id, o.code)}
                    className="view-report secondary-button"
                    label="Setup Product"
                    secondary
                  />
                </TableRowColumn>
              </TableRow>
            )) : (
              <TableRow key={p.id} selectable={false}>
                <TableRowColumn title={enDash(p.supplierName)}>
                  {enDash(p.supplierName)}
                </TableRowColumn>
                <TableRowColumn title={p.name}>
                  {p.name}
                </TableRowColumn>
                <TableRowColumn>
                  {p.code}
                </TableRowColumn>
                <TableRowColumn>
                  {enDash('')}
                </TableRowColumn>
                <TableRowColumn>
                  {enDash('')}
                </TableRowColumn>
                <TableRowColumn style={styles.editProduct}>
                  <RaisedButton
                    onClick={() => this.setUpResellerProduct(p.id, '')}
                    className="view-report secondary-button"
                    label="Setup Product"
                    secondary
                  />
                </TableRowColumn>
              </TableRow>
            )
          ))}
        </TableBody>
      </Table>
    );
  }

  render() {
    const { type } = this.props;

    if (type === 'supplier') {
      return this.renderSupplierProducts();
    }

    if (type === 'connected') {
      return this.renderConnectedProducts();
    }

    if (type === 'pending') {
      return this.renderPendingProducts();
    }

    return (
      <div>No products to show</div>
    );
  }
}

ProductsTable.propTypes = {
  type: PropTypes.string.isRequired,
  products: PropTypes.arrayOf(PropTypes.object).isRequired,
  resellerCatalogId: PropTypes.string,
  supplierCatalogId: PropTypes.string,
  resellerId: PropTypes.string,
  supplierId: PropTypes.string,
  refreshData: PropTypes.func.isRequired,
};

ProductsTable.defaultProps = {
  resellerCatalogId: null,
  supplierCatalogId: null,
  resellerId: null,
  supplierId: null,
};

export default ProductsTable;
