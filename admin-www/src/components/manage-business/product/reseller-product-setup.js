import React, { Component, PropTypes } from 'react';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  reduxForm,
  formValueSelector,
  Field,
  FieldArray,
} from 'redux-form';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import { Card } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import ContentRemoveCircleOutline from 'material-ui/svg-icons/content/remove-circle-outline';
import ContentAddOutline from 'material-ui/svg-icons/content/add-circle-outline';
import NavigationArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import ProductStatusPopover from '../../product-status-popover';
import {
  TextField,
  Toggle,
} from '../../formElements';
import * as resellerActions from '../../../ax-redux/actions/resellers';

const styles = {
  tableRow: {
    height: 80,
  },
  formFooter: {
    width: '100%',
    height: '2em',
  },
  submitButton: {
    marginRight: '1em',
  },
  overflow: {
    overflow: 'visible',
  },
  hoursOfOperation: {

  },
  error: {
    marginLeft: '1em',
    color: 'red',
  },
  aliasesLabel: {
    verticalAlign: 'top',
    paddingTop: '32px',
  },
  colTwo: {
    width: '272px',
    overflow: 'visible',
  },
  mappingArrowColumn: {
    width: '100px',
  },
};

class ResellerProductSetup extends Component {
  constructor(props) {
    super(props);

    this.renderMappingArrow = this.renderMappingArrow.bind(this);
    this.setDefaultMapping = this.setDefaultMapping.bind(this);
    this.renderAliases = this.renderAliases.bind(this);
    this.updateProduct = this.updateProduct.bind(this);
  }

  setDefaultMapping(mappingField) {
    const {
      change,
      product,
      option,
    } = this.props;

    switch (mappingField) {
      case 'productName':
        change('productName', product.name);
        break;
      case 'productCode':
        change('productCode', product.code);
        break;
      case 'productOptionCode':
        change('productOptionCode', (option && option.code) || '');
        break;
      default:
    }
  }

  updateProduct(values) {
    const {
      product,
      catalog,
      option,
      adminResellerId,
      resellerId,
      createResellerCatalogItem,
      optionMapping,
    } = this.props;

    const {
      productName,
      aliases,
      productCode,
      productOptionCode,
      status,
    } = values;

    const data = {
      catalogId: catalog.id,
      name: product.name,
      code: productCode.trim(),
      map: {
        from: {
          productCode: productCode.trim(),
          productOptionCode: productOptionCode.trim(),
          productNames: [productName.trim(), ...aliases.map(a => a.trim())],
        },
        to: {
          catalogAddress: {
            catalogId: product.catalogId,
            itemId: product.id,
            optionId: optionMapping && option ? option.id : '',
          },
          priceListId: product.priceListId,
          productCode: product.code,
          productOptionCode: optionMapping && option ? option.code : '',
        },
      },
      typeOf: 'TYPE_MAPPING',
      status,
    };

    createResellerCatalogItem(adminResellerId || resellerId, catalog.id, data);
  }

  renderMappingArrow(mappingField) {
    return (
      <IconButton
        iconStyle={styles.mediumIcon}
        style={styles.medium}
        onTouchTap={() => this.setDefaultMapping(mappingField)}
      >
        <NavigationArrowBack />
      </IconButton>
    );
  }

  renderAliases({ fields }) {
    return (
      <div>
        {fields.map((alias, index) => (
          <div style={styles.alias} key={alias}>
            <Field
              name={alias}
              label="Product Alias"
              component={TextField}
            />

            <IconButton
              onClick={() => fields.remove(index)}
              iconStyle={styles.mediumIcon}
              style={styles.medium}
            >
              <ContentRemoveCircleOutline />
            </IconButton>
          </div>
        ))}

        <IconButton
          iconStyle={styles.mediumIcon}
          style={styles.medium}
          onTouchTap={() => fields.push('')}
        >
          <ContentAddOutline />
        </IconButton>
      </div>
    );
  }

  renderTable() {
    const {
      product,
      option,
      optionMapping,
      change,
    } = this.props;

    return (
      <Table className="products-table" selectable={false}>
        <TableHeader
          adjustForCheckbox={false}
          displayRowCheckbox={false}
          enableSelectAll={false}
          displaySelectAll={false}
        >
          <TableRow>
            <TableHeaderColumn />
            <TableHeaderColumn style={styles.colTwo} >Yours</TableHeaderColumn>
            <TableHeaderColumn style={styles.mappingArrowColumn} />
            <TableHeaderColumn>{product.supplierName}</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          <TableRow style={styles.tableRow}>
            <TableRowColumn>
              Product Name
            </TableRowColumn>
            <TableRowColumn style={styles.colTwo}>
              <Field
                name="productName"
                label="Product Name"
                component={TextField}
              />
            </TableRowColumn>
            <TableRowColumn style={styles.mappingArrowColumn} >
              {this.renderMappingArrow('productName')}
            </TableRowColumn>
            <TableRowColumn style={styles.overflow}>
              {product.name}
            </TableRowColumn>
          </TableRow>

          <TableRow style={styles.tableRow}>
            <TableRowColumn style={styles.aliasesLabel}>
              Aliases
            </TableRowColumn>
            <TableRowColumn style={styles.colTwo}>
              <FieldArray name="aliases" component={this.renderAliases} />
            </TableRowColumn>
            <TableRowColumn style={styles.mappingArrowColumn} />
            <TableRowColumn />
          </TableRow>

          <TableRow style={styles.tableRow}>
            <TableRowColumn>
              Product Code
            </TableRowColumn>
            <TableRowColumn style={styles.colTwo}>
              <Field
                name="productCode"
                label="Product Code"
                component={TextField}
              />
            </TableRowColumn>
            <TableRowColumn style={styles.mappingArrowColumn} >
              {this.renderMappingArrow('productCode')}
            </TableRowColumn>
            <TableRowColumn style={styles.overflow}>
              {product.code}
            </TableRowColumn>
          </TableRow>

          <TableRow style={styles.tableRow}>
            <TableRowColumn>
              Map to option
            </TableRowColumn>
            <TableRowColumn style={styles.colTwo}>
              <Field
                name="optionMapping"
                component={Toggle}
                onChange={(e, value) => !value && change('productOptionCode', '')}
              />
            </TableRowColumn>
            <TableRowColumn style={styles.mappingArrowColumn} />
            <TableRowColumn />
          </TableRow>

          <TableRow style={styles.tableRow}>
            <TableRowColumn>
              Product Option Code
            </TableRowColumn>
            <TableRowColumn style={styles.colTwo}>
              <Field
                name="productOptionCode"
                label="Product Option Code"
                component={TextField}
                disabled={!optionMapping}
              />
            </TableRowColumn>
            <TableRowColumn style={styles.mappingArrowColumn}>
              {optionMapping && option && this.renderMappingArrow('productOptionCode')}
            </TableRowColumn>
            <TableRowColumn>
              {optionMapping && option && option.code}
            </TableRowColumn>
          </TableRow>

          <TableRow style={styles.tableRow}>
            <TableRowColumn>
              Status
            </TableRowColumn>
            <TableRowColumn>
              <Field
                name="status"
                component={({ input }) => (
                  <ProductStatusPopover
                    onStatus={value => input.onChange(value)}
                    status={input.value}
                  />
                )}
              />
            </TableRowColumn>
            <TableRowColumn />
            <TableRowColumn />
          </TableRow>

          <TableRow style={styles.tableRow}>
            <TableRowColumn />
            <TableRowColumn style={styles.mappingArrowColumn} />
            <TableRowColumn />
            <TableRowColumn style={styles.colTwo}>
              <RaisedButton
                onClick={() => browserHistory.goBack()}
                className="view-report"
                style={styles.submitButton}
                label="Cancel"
              />
              <RaisedButton
                className="view-report"
                style={styles.submitButton}
                label="Save Changes"
                primary
                type="submit"
              />
            </TableRowColumn>
          </TableRow>
        </TableBody>
      </Table>
    );
  }

  render() {
    const {
      product,
      handleSubmit,
    } = this.props;

    if (!product) {
      return (
        <div>
          No product selected
        </div>
      );
    }

    return (
      <form className="product-edit" onSubmit={handleSubmit(this.updateProduct)}>
        <Card className="inner-edit">
          <h2 style={{ color: '#434343' }}>
            Setup {product.name || 'Product'} ({product.supplierName})
          </h2>
          {this.renderTable()}
          <div style={styles.formFooter} />
        </Card>
      </form>
    );
  }
}

ResellerProductSetup.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    supplierName: PropTypes.string,
    code: PropTypes.string,
  }),
  option: PropTypes.shape({
    code: PropTypes.string,
  }),
  optionMapping: PropTypes.bool.isRequired,
  change: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  catalog: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  resellerId: PropTypes.string.isRequired,
  adminResellerId: PropTypes.string.isRequired,
  createResellerCatalogItem: PropTypes.func.isRequired,
};

ResellerProductSetup.defaultProps = {
  product: null,
  option: null,
};

const mapStateToProps = (state, props) => {
  const {
    id: productId,
    optionCode,
  } = props.params;

  const products = state.products.listPendingProducts.data &&
    state.products.listPendingProducts.data.products;
  const product = products && products.find(p => p.id === productId);
  const option = product && product.options.find(o => o.code === optionCode);
  const formSelector = formValueSelector('resellerProductSetup');
  const optionMapping = formSelector(state, 'optionMapping');
  const resellerCatalogId = state.reportFilters.resellerCatalogId;
  const resellerCatalogs = state.resellers.listCatalogs.data &&
    state.resellers.listCatalogs.data.catalogs;
  const catalog = resellerCatalogs && resellerCatalogs.find(c => c.id === resellerCatalogId);
  const adminResellerId = state.reportFilters.adminResellerId !== -1 ?
    state.reportFilters.adminResellerId :
    '';
  const resellerId = state.app.user.resellerId;

  return {
    product,
    option,
    optionMapping,
    catalog,
    resellerId,
    adminResellerId,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  createResellerCatalogItem: resellerActions.createResellerCatalogItem,
}, dispatch);


/*
 * Redux wrapper
 */
const ResellerProductSetupConnected = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResellerProductSetup);


/*
* Form validation
*/
const validate = (values) => {
  const errors = {};
  const aliasesErrors = [];

  if (!values.productName.trim()) {
    errors.productName = 'Product Name is required';
  }

  if (!values.productCode.trim()) {
    errors.productCode = 'Product Code is required';
  }

  values.aliases.forEach((alias, index) => {
    if (!alias || !alias.trim()) {
      aliasesErrors[index] = 'Alias must contain at least one character';
    }
  });

  if (aliasesErrors.length) {
    errors.aliases = aliasesErrors;
  }

  if (values.optionMapping && !values.productOptionCode.trim()) {
    errors.productOptionCode = 'Product Option Code is required';
  }

  return errors;
};


/*
 * Form wrapper
 */
export default reduxForm({
  form: 'resellerProductSetup',
  validate,
  initialValues: {
    productName: '',
    aliases: [],
    productCode: '',
    optionMapping: false,
    status: 'STATUS_LIVE',
    productOptionCode: '',
  },
})(ResellerProductSetupConnected);
