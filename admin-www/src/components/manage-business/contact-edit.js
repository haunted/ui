import React, {PropTypes} from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import AccountsPopover from '../accounts-popover';

class ContactEdit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
        id: "",
        accountId: "",
        title: "",
        surname: "",
        givenName: "",
        phone: "",
        email: "",
        orgCode: this.props.orgCode,
        invalidEmail: false,
        invalidPhone: false
    }
  }

  componentWillMount () {
      if (this.props.mode == "edit") {
          if (!this.props.fetching) {
              this.props.readContact(this.props.params.id);
          }
      }
  }

  componentWillUpdate(nextProps, nextState) { // map fields to view

    /* Clear Errors */
    if (this.state.invalidEmail !== false && this.state.email != nextState.email) {
      this.setState({ invalidEmail: false })
    }
    if (this.state.invalidPhone !== false && this.state.phone != nextState.phone) {
      this.setState({ invalidPhone: false })
    }

  }


  shouldComponentUpdate(nextProps, nextState) {
    var key = null;
    if (this.props.mode == "edit") {
        //alert(JSON.stringify({user:this.props.user, nextValue: nextProps.user}));
        if (!this.props.user  && !!nextProps.user) {
            this.setState(nextProps.user);
            if (!this.state.orgCode) {
                this.setState({
                    "orgCode": ""
                })
            }
        }
    }
    return true;
  }

  save () {
      let error = "",
          emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i,
          phoneRegex = /^(\+?[01])?[-.\s]?\(?[1-9]\d{2}\)?[-.\s]?\d{3}[-.\s]?\d{4}/;

      if (!emailRegex.test(this.state.email)) {
          error = "You must enter a valid email address.";
          this.setState({ invalidEmail: error });
      }
      if (!phoneRegex.test(this.state.phone)) {
          error = "You must enter a valid phone number."
          this.setState({ invalidPhone: error });
      }

      if (error != "") {
          return;
      }
      if (this.props.mode == "edit") {
          this.props.updateContact(this.props.params.id, this.state);
      } else {
         this.props.createContact(this.state);
      }
  }

  deleteAccount (id) {
      if (confirm("Confirm deleting account?")) {
          this.props.deleteAccount(id);
      }
  }

  setSupplier(supplier) {
    console.log(supplier)
    this.setState({ orgCode: supplier.supplier.id })
  }

  renderForm (data, mode, fetchingGroups) {
      let user = data;
      if (mode == "add") {
          user = this.state;
      }
      return (
          <Table selectable={false} className="edit-table">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow displayRowCheckbox={false} selectable={false}>
                    <TableHeaderColumn>Property</TableHeaderColumn>
                    <TableHeaderColumn>Value</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                    <TableRow selectable={false} style={{display: "none"}}>
                        <TableRowColumn key={"1.1"}>ID</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField defaultValue={user.id} hintText={"ID"}
                                    key={"id"}
                                    onChange={e=>{ this.setState({"id": e.target.value}) }}
                            />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false}>
                        <TableRowColumn key={"1.1"}>Title</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField defaultValue={user.title} hintText={"Title"}
                                    key={"title"}
                                    onChange={e=>{ this.setState({"title": e.target.value}) }}
                            />
                        </TableRowColumn>
                    </TableRow>

                    <TableRow selectable={false}>
                        <TableRowColumn key={"1.1"}>First Name</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField defaultValue={user.givenName} hintText={"First Name"}
                                    key={"givenName"}
                                    onChange={e=>{ this.setState({"givenName": e.target.value}) }}
                            />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false}>
                        <TableRowColumn key={"1.1"}>Last Name</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField defaultValue={user.surname} hintText={"Last Name"}
                                    key={"ID"}
                                    onChange={e=>{ this.setState({"surname": e.target.value}) }}
                            />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false}>
                        <TableRowColumn key={"1.1"}>Phone</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField defaultValue={user.phone} hintText={"Phone"}
                                    key={"phone"}
                                    errorText={this.state.invalidPhone}
                                    onChange={e=>{ this.setState({"phone": e.target.value}) }}
                            />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false}>
                        <TableRowColumn key={"1.1"}>Email</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField defaultValue={user.email} hintText={"Email"}
                                    key={"email"}
                                    errorText={this.state.invalidEmail}
                                    onChange={e=>{ this.setState({"email": e.target.value}) }}
                            />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false}>
                        <TableRowColumn key={"1.1"}>User Account</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                                <AccountsPopover value={this.state.accountId} orgCode={this.props.orgCode} onAccount={account=> {
                                    this.setState({accountId: account})
                                }} />
                        </TableRowColumn>
                    </TableRow>
            </TableBody>
          </Table>
      )
  }

  render() {
      let fetching = this.props.fetching ||
                     this.props.saving ||
                     this.props.creating ||
                     this.props.deleteFetching ||
                     this.props.addUserFetching ||
                     this.props.removeUserFetching;
    return (
        <div className="user-edit">
            <Card className="inner-edit">
                <h2 >{(this.props.mode=="edit"?"Edit":"Add")+" "}Contact</h2>
                {
                    !fetching ? this.renderForm(this.props.user, this.props.mode, false) :
                    <div className="loading-circle">
                      <CircularProgress color={"#27367a"} size={96} />
                    </div>
                }
                { !fetching ? <RaisedButton className="save-button" label="Save" onClick={e => {this.save()}} /> : ""}
                {
                    !fetching && this.props.mode == "edit" ?
                    <RaisedButton className="delete-button" label="Delete" onClick={e => {this.deleteAccount(this.props.user.id)}} />
                    : ""
                }
            </Card>
        </div>
    );
  }
}

ContactEdit.propTypes = {

};

import { connect } from 'react-redux';
import {
    createContact,
    readContact,
    updateContact,
    disableContract
} from '../../ax-redux/actions/contacts';

export default connect(
  state => {
    return {
        contact: state.contacts.readContact.data,
        error: state.contacts.updateContact.error,
        fetching: state.contacts.readContact.fetching,
        saving: state.contacts.updateContact.fetching,
        creating: state.contacts.createContact.fetching,
        deleteFetching: state.contacts.disableContract.fetching,
        orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
        mode: (state.routing.locationBeforeTransitions.pathname.search("/add") > -1 ? "add" : "edit")
    }
  },
  dispatch => {
    return {
        readContact: (id) => {
            dispatch(readContact(id))
        },
        createContact: (data) => {
            let contact = {
                title: data.title,
                accountId: data.accountId,
                givenName: data.givenName,
                surname: data.surname,
                email: data.email,
                phone: data.phone,
                orgCode: data.orgCode
            }
            dispatch(createContact(contact))
        },
        disableContract: (id) => {
                dispatch(disableContract(id));
        },
        updateContact: (id, data) => {
            let contact = {
                id: data.id,
                accountId: data.accountId,
                title: data.title,
                givenName: data.givenName,
                surname: data.surname,
                email: data.email,
                phone: data.phone,
                orgCode: data.orgCode
            }
            dispatch(updateContact(id, contact))
        }
    }
  }
)(ContactEdit)
