import React, { Component, PropTypes } from 'react'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import { browserHistory } from 'react-router'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Snackbar from 'material-ui/Snackbar'

class ViewOrgDataButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      showError: false
    }
  }
  componentWillUpdate(nextProps, nextState) {

  }
  handleErrRequestClose () {
      this.setState({
        showError: false,
      });
  }
  submit () {
    let path = "/app/manage-business/" + this.props.model,
        orgType = this.props.orgType
    if (orgType == 'supplier') {
      this.props.setAdminSupplierId(this.props.id)
    } else if (orgType == 'reseller') {
      this.props.setAdminResellerId(this.props.id)
    } else if (orgType == 'pass-user') {
      this.props.setadminPassIssuerId(this.props.id)
    }
    this.props.spoofUserOrg(this.props.orgCode)
    browserHistory.push(path)
  }
  render() {
      let model = this.props.model,
          label = ''
      if (model == "products") {
        label = 'View Products'
      } else if (model == "contracts") {
        label = 'View Contracts'
      } else if (model == "price-lists"){
        label = 'View Price Lists'
      }
      return (
          <RaisedButton onTouchTap={e => { this.submit() } }
                        style={(this.props.style  || {marginLeft: "1em"})}
                        label={label}
          />
      )
  }
}

ViewOrgDataButton.propTypes = {
  id: PropTypes.string,
  orgType: PropTypes.string,
  orgCode: PropTypes.string,
  model: PropTypes.string
}

import { connect } from 'react-redux'
import {
  setAdminSupplierId,
  setAdminResellerId,
  setadminPassIssuerId
} from '../../ax-redux/actions/report-filters'
import {
  spoofUserOrg
} from '../../ax-redux/actions/app'
export default connect(
  (state, ownProps) => {
    return {

    }
  },
  dispatch => {
    return {
      setAdminSupplierId: (id) => {
          dispatch(setAdminSupplierId(id))
      },
      setAdminResellerId: (id) => {
          dispatch(setAdminResellerId(id))
      },
      setadminPassIssuerId: (id) => {
          dispatch(setadminPassIssuerId(id))
      },
      spoofUserOrg: (code) => {
          dispatch(spoofUserOrg(code))
      }
    }
  }
)(ViewOrgDataButton)
