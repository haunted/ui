import React, { Component, PropTypes } from 'react'

class AutoComplete extends Component {
  constructor(props) {
    super(props)
    this.state = {
        visible: false
    }
  }

  hide () {
      this.setState({
          visible: false
      });
  }

  componentWillUpdate (nextProps, nextState) {
      if (nextProps.searchText != this.props.searchText) {
          this.setState({
              visible: true
          })
      }
  }

    render () {
        let searchText = this.props.searchText,
            values = this.props.values;
        return (
            <div className="auto-complete" style={{marginLeft: (this.props.left), display: (this.state.visible ? "block" : "none")}} >
                {values.map((value, i) => {
                    let option = "";
                    if (value.toLowerCase().search(searchText.toLowerCase()) > -1) {
                        option = (
                            <div key={i} className="option" onClick={ (e) => {
                                this.hide();
                                this.props.onComplete(value)
                            } }>
                                {value}
                            </div>
                        );
                    }
                    return option;
                })}
            </div>
        )
    }

}

AutoComplete.propTypes = {
  //visible: React.PropTypes.bool.isRequired,
  searchText: React.PropTypes.string.isRequired,
  values: React.PropTypes.arrayOf(React.PropTypes.string),
  onComplete: PropTypes.func.isRequired,
  left: React.PropTypes.string
}

export default AutoComplete
