import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Dialog from 'material-ui/Dialog';
import ContractForm from '../../forms/ContractForm';
import * as resellerActions from '../../../ax-redux/actions/resellers';

const styles = {
  dialog: {
    overflow: 'auto',
  },
  dialogContent: {
    marginBottom: 48,
  },
};


class AddContractModal extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
  }

  componentWillMount() {
    const {
      listResellers,
      listResellerCatalogs,
      selectedReseller,
    } = this.props;

    listResellers();

    if (selectedReseller) {
      listResellerCatalogs(selectedReseller.id);
    }
  }

  handleSubmit(formValues) {
    const {
      onSave,
      resellers,
      closeModal,
    } = this.props;

    const {
      hasDateRange,
      ...data
    } = formValues;

    const reseller = resellers.find(r => r.name === data.resellerName);

    data.resellerCode = reseller.code;

    if (!hasDateRange) {
      data.validFrom = '';
      data.validUntil = '';
    }

    onSave(data);
    closeModal();
  }

  handleCancel() {
    this.props.closeModal();
  }

  render() {
    const {
      dialogTitle,
      open,
      initialValues,
    } = this.props;

    const formProps = {
      onSubmit: this.handleSubmit,
      onCancel: this.handleCancel,
    };

    if (initialValues) {
      formProps.initialValues = initialValues;
    }

    return (
      <Dialog
        title={dialogTitle}
        modal
        open={open}
        onRequestClose={this.handleCancel}
        autoDetectWindowHeight={false}
        style={styles.dialog}
        contentStyle={styles.dialogContent}
      >
        <ContractForm {...formProps} />
      </Dialog>
    );
  }
}

AddContractModal.propTypes = {
  open: PropTypes.bool.isRequired,
  dialogTitle: PropTypes.string,
  initialValues: PropTypes.shape({}),
  resellers: PropTypes.arrayOf(PropTypes.object).isRequired,
  selectedReseller: PropTypes.shape({
    id: PropTypes.string,
  }),
  closeModal: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  listResellers: PropTypes.func.isRequired,
  listResellerCatalogs: PropTypes.func.isRequired,
};

AddContractModal.defaultProps = {
  dialogTitle: 'Add New Contract',
  selectedReseller: null,
  initialValues: null,
};

const mapStateToProps = (state, props) => {
  const resellers = state.resellers.list.data ? state.resellers.list.data.resellers : [];

  const { initialValues = {} } = props;

  return {
    resellers,
    selectedReseller: resellers.find(r => r.code === initialValues.resellerCode),
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  ...resellerActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddContractModal);
