import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import CircularProgress from 'material-ui/CircularProgress'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import ResellersAutocomplete from '../../reports/resellers-autocomplete'
import ProductsAutocomplete from '../../reports/products-autocomplete'
import AddContractModal from '../supplier/add-contract-modal'
import Checkbox from 'material-ui/Checkbox'
import RadioButton from 'material-ui/RadioButton'
import IconButton from 'material-ui/IconButton'
import moment from 'moment-timezone'
import ContentRemoveCircleOutline from 'material-ui/svg-icons/content/remove-circle-outline'
import ContentAddOutline from 'material-ui/svg-icons/content/add-circle-outline'
import {formatCurrency} from '../../../util'

const styles = {
  labelColumn: {
    width: '200px'
  },
  overflow: {
    overflow: 'visible'
  },
  error: {
    marginLeft: '1em',
    color: 'red'
  },
  optionCodeColumn: {
    width: '180px'
  }
}

class ProductResellers extends Component {
  constructor(props) {
    super(props)
    this.state = {
      customizeContract: false,
      showAliases: true,
      formModified: false,
      errorMessage: '',
      errorUniqueName: '',
      errorUniqueOptions: '',
      errorReseller: '',
      errorProductName: '',
      errorProductCode: '',
      customize: false, // contract
      reseller: '',
      productName: '',
      aliases: [],
      resellerProductCode: '',
      contract: {
        validFrom: new Date(),
        commission: 20,
        priceOn: 'sale'
      },
      customContract: false,
      options: [ // load these from endpoint, once available
        {
          code: "TST",
          name: "Adult",
          currency: "USD",
          retailPrice: 100.00,
          distribution: "public"
        }, {
          code: "TST2",
          name: "Adult 2",
          currency: "USD",
          retailPrice: 12.99,
          distribution: "public"
        }
      ],
      resellerOptionCodes: [
        "",
        ""
      ],
      selectedOption: 0,
      updatedResellerOptionCode: 0
    }
  }
  componentDidMount () {
    this.props.readProduct(this.props.params.id);
    const { route } = this.props
    const { router } = this.context
    router.setRouteLeaveHook(route, v=> this.routerWillLeave(v))
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextState.reseller != this.state.reseller ||
        nextState.productName != this.state.productName ||
        nextState.resellerProductCode != this.state.resellerProductCode ||
        nextState.aliases.length != this.state.aliases.length ||
        nextState.options.length != this.state.options.length ||
        nextState.updatedResellerOptionCode != this.state.updatedResellerOptionCode
    ) {
        this.clearErrors(nextState)
    }
    if (this.props.productFetching &&
        nextProps.productFetching == false &&
        nextProps.product != false
    ) {
        let data = nextProps.product,
            resellerOptionCodes = []
        data.options.map(o => {
          resellerOptionCodes.push("")
        })
        this.setState({
          reseller: data.reseller,
          productName: data.productName,
          options: data.options,
          resellerOptionCodes
        })
    }
    if (this.props.productResellers.fetching && nextProps.productResellers.fetching == false) {
      this.setState({
        formModified: false
      })
    }
    // load default contract from endpoint here
  }
  routerWillLeave() {
    if (this.state.formModified) {
      return "Your changes haven't been saved yet.\nAre you sure you want to leave?"
    }
  }
  clearErrors(data) {
    let errorUniqueName = this.state.errorUniqueName,
        errorUniqueOptions = this.state.errorUniqueOptions,
        errorReseller = this.state.errorReseller,
        errorProductName = this.state.errorProductName,
        errorProductCode = this.state.errorProductCode
    if (this.checkNameIsUnique(this.state.productName, this.state.resellerProductCode, this.state.aliases)) {
      errorUniqueName = ''
    }
    if (this.checkOptionCodesAreUnique(this.state.resellerOptionCodes)) {
      errorUniqueOptions = ''
    }
    if (this.state.reseller != '') {
      errorReseller = ''
    }
    if (this.state.productName != '') {
      errorProductName = ''
    }
    if (this.state.resellerProductCode != '') {
      errorProductCode = ''
    }
    this.setState({
      errorUniqueName,
      errorUniqueOptions,
      errorReseller,
      errorProductName,
      errorProductCode
    })
  }
  validate(data) {
    let errorUniqueName = this.state.errorUniqueName,
        errorUniqueOptions = this.state.errorUniqueOptions,
        errorReseller = this.state.errorReseller,
        errorProductName = this.state.errorProductName,
        errorProductCode = this.state.errorProductCode,
        valid = true
    if (!this.checkNameIsUnique(this.state.productName, this.state.resellerProductCode, this.state.aliases)) {
      errorUniqueName = 'Combination of product name, code and aliases must be unique.'
      valid = false
    }
    if (!this.checkOptionCodesAreUnique(this.state.resellerOptionCodes)) {
      errorUniqueOptions = 'Option codes for this product and reseller must be unique.'
      valid = false
    }
    if (this.state.resellerOptionCodes[this.state.selectedOption] == '') {
      errorUniqueOptions = 'Option code is required.'
      valid = false
    }
    if (this.state.reseller == '') {
      errorReseller = 'Reseller is required.'
      valid = false
    }
    if (this.state.productName == '') {
      errorProductName = 'Reseller product name is required.'
      valid = false
    }
    if (this.state.resellerProductCode == '') {
      errorProductCode = 'Reseller product code is required.'
      valid = false
    }
    this.setState({
      errorUniqueName,
      errorUniqueOptions,
      errorReseller,
      errorProductName,
      errorProductCode
    })
    return valid
  }
  checkNameIsUnique(name, code, aliases) {
    return true
  }
  checkOptionCodesAreUnique(options) {
    let valid = true
    options.map((opt, i) => {
      options.map((optB, b) => {
        if (i != b) {
          if (opt == optB) {
            valid = false
          }
        }
      })
    })
    return valid
  }
  updateProductResellers () {
     if (this.validate(this.state)) {
       let data = {
         reseller: this.state.reseller,
         productName: this.state.productName,
         aliases: this.state.aliases,
         resellerProductCode: this.state.resellerProductCode,
         resellerOptionCode: this.state.resellerOptionCodes[this.state.selectedOption],
         contract: this.state.customizeContract ? this.state.customContract : this.state.contract,
         option: this.state.options[this.state.selectedOption]
       }
       //this.toggleForm()
       // call save endpoint
     }
  }
  setReseller (reseller) {
    this.setState({
       reseller: reseller.code,
       formModified: true
    })
  }
  setProduct (product) {
    console.log(product)
    this.setState({
      productName: product.name
    })
  }
  selectProductOption (index) {
    this.setState({
      selectedOption: index
    })
  }
  setResellerOptionCode (index, value) {
    let options = this.state.resellerOptionCodes
    options[index] = value
    this.setState({
      resellerOptionCodes: options,
      updatedResellerOptionCode: this.state.updatedResellerOptionCode + 1
    })
  }
  toggleCustomContract () {
    this.setState({
      customizeContract: ! this.state.customizeContract
    })
  }
  editContract (v) {
    this.setState({
      customContract: v,
      formModified: true
    })
  }
  setAlias (a, i) {
    let aliases = this.state.aliases
    aliases[i] = a
    this.setState({
      aliases,
      formModified: true
    })
  }
  addAlias () {
    let aliases = this.state.aliases
    aliases.push(" ")
    this.setState({
      aliases,
      formModified: true
    })
  }
  removeAlias (i) {
    let aliases = this.state.aliases
    aliases.splice(i, 1)
    this.setState({
      aliases,
      formModified: true,
      showAliases: false
    })
    setTimeout(()=>{
      this.setState({
        showAliases: true
      })
    }, 50)
  }
  showNetPrice (price) {
    if (this.state.contract != undefined) {
      let commission = this.state.contract.commission
      price -= (price * (commission/100.0))
    }
    return price
  }
  renderAliases (aliases) {
    if (!aliases) {
      return ''
    }
    return aliases.map((v,k) => (
      <div style={styles.alias} key={k}>
        <TextField hintText={"Product Alias"}
                defaultValue={v}
                key={k}
                onBlur={e=>{ this.setAlias(e.target.value, k) }}
        />
        <IconButton
          iconStyle={styles.mediumIcon}
          style={styles.medium}
          onClick={ e=> this.removeAlias(k)}
         >
         <ContentRemoveCircleOutline />
       </IconButton>
      </div>
    ))
  }
  renderContract (data) {
    if (data == false) {
      return ''
    }
    return (
      <div>
        <div>Valid From: <span>{moment(data.validFrom).tz(TZ).format("DD-MMM-YYYY")}</span></div>
        <div>Commission: <span>{data.commission}%</span></div>
        <div>Priced At: <span>{data.priceOn}</span></div>
      </div>
    )
  }
  renderProductOptions (rows) {
    if (!rows) {
        return "";
    }
    return rows.map((v, k) => (
        <TableRow key={k} selectable={false}>
          <TableRowColumn>{v.code}</TableRowColumn>
          <TableRowColumn>{v.name}</TableRowColumn>
          <TableRowColumn>{v.currency}</TableRowColumn>
          <TableRowColumn>{ formatCurrency(v.currency, v.retailPrice) }</TableRowColumn>
          <TableRowColumn>{v.distribution}</TableRowColumn>
          <TableRowColumn>
            <RadioButton checked={ this.state.selectedOption == k }
                         disabled={ this.state.resellerOptionCodes[k] == ''}
                         onClick={e=> this.selectProductOption(k) }
                         label=""
            />
          </TableRowColumn>
          <TableRowColumn style={styles.optionCodeColumn}>
            <TextField defaultValue={this.state.resellerOptionCodes[k]}
                       hintText="Reseller Option Code"
                       onChange={e=>{ this.setResellerOptionCode(k, e.target.value) }}
                       errorText={this.state.errorUniqueOptions != '' ? '_' : ''} // error won't fit here, but underline the field red anyway
                       key="ResellerOptionCode"
            />
          </TableRowColumn>
          <TableRowColumn>{ formatCurrency(v.currency, this.showNetPrice(v.retailPrice)) }</TableRowColumn>
        </TableRow>
    ))
  }

  render() {
      let fetching = this.props.resellersFetching ||
                     this.props.productFetching,
                     resellerOptionCodeError = this.state.errorProductCode != '' ? this.state.errorProductCode : this.state.errorUniqueName
    return (
      <div className="product-edit" >
        <Card className="inner-edit">
                <h2 style={{color:"#434343"}}>
                  { this.props.product != false ? this.props.product.name : 'Product Name Here' }
                </h2>
                { this.state.errorMessage != '' ? <h2 style={{color:'red'}}>{this.state.errorMessage}</h2> : '' }
                <Table style={styles.form}>
                  <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                      <TableRow style={{display: 'none'}}>
                          <TableHeaderColumn style={styles.labelColumn}></TableHeaderColumn>
                          <TableHeaderColumn></TableHeaderColumn>
                      </TableRow>
                  </TableHeader>
                  <TableBody displayRowCheckbox={false}>
                    <TableRow selectable={false} >
                          <TableRowColumn style={styles.labelColumn}>
                              Reseller
                          </TableRowColumn>
                          <TableRowColumn style={styles.overflow}>
                            <ResellersAutocomplete onReseller={ r=> this.setReseller(r) } />
                            <span style={styles.error}>{ this.state.errorReseller }</span>
                          </TableRowColumn>
                      </TableRow>
                      <TableRow selectable={false} >
                          <TableRowColumn>
                              Reseller Product Name
                          </TableRowColumn>
                          <TableRowColumn>
                            <ProductsAutocomplete onProduct={ r=> this.setProduct(r) }
                                                   supplierCode={ this.props.orgCode }
                            />
                            <span style={styles.error}>
                              { this.state.errorProductName != '' ? this.state.errorProductName : this.state.errorUniqueName }
                            </span>
                          </TableRowColumn>
                      </TableRow>
                      <TableRow selectable={false} >
                          <TableRowColumn style={styles.labelColumn}>
                              Aliases
                          </TableRowColumn>
                          <TableRowColumn>
                              { this.state.showAliases ? this.renderAliases(this.state.aliases) : '' }
                              <IconButton
                                iconStyle={styles.mediumIcon}
                                style={styles.medium}
                                onTouchTap={ e=> this.addAlias()}
                               >
                               <ContentAddOutline />
                             </IconButton>
                          </TableRowColumn>
                        </TableRow>
                        <TableRow selectable={false} >
                            <TableRowColumn style={styles.labelColumn}>
                                Reseller Product Code
                            </TableRowColumn>
                              <TableRowColumn>
                                <TextField defaultValue={this.state.resellerProductCode}
                                           hintText="Reseller Product Code"
                                           onChange={e=>{ this.setState({resellerProductCode: e.target.value}) }}
                                           errorText={resellerOptionCodeError}
                                           style={{marginBottom: '1em'}}
                                           key="Title"
                                />
                              </TableRowColumn>
                          </TableRow>
                          <TableRow selectable={false} >
                              <TableRowColumn style={styles.labelColumn}>
                                  Contract Details
                              </TableRowColumn>
                              <TableRowColumn>
                                <Checkbox style={styles.checkbox}
                                          label="Customize"
                                          checked={this.state.customizeContract}
                                          onCheck={(e, v)=> this.toggleCustomContract(v)}
                                />
                                { this.state.customizeContract ? this.renderContract(this.state.customContract) : ''}
                                { this.state.customizeContract ?
                                  <AddContractModal resellers={this.props.resellers}
                                                    onSave={ v=> this.editContract(v) }
                                                    label="Manage Contract"
                                  /> : this.renderContract(this.state.contract)
                                }

                              </TableRowColumn>
                          </TableRow>
                  </TableBody>
               </Table>
                <section>
                    <Table className="resellers index" >
                      <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                        <TableRow>
                          <TableHeaderColumn>Code</TableHeaderColumn>
                          <TableHeaderColumn>Name</TableHeaderColumn>
                          <TableHeaderColumn>Currency</TableHeaderColumn>
                          <TableHeaderColumn>Retail Price</TableHeaderColumn>
                          <TableHeaderColumn>Distribution</TableHeaderColumn>
                          <TableHeaderColumn>Enabled for Contract</TableHeaderColumn>
                          <TableHeaderColumn style={styles.optionCodeColumn}>Reseller Option Code</TableHeaderColumn>
                          <TableHeaderColumn>Net Price</TableHeaderColumn>
                        </TableRow>
                      </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        {!fetching ? this.renderProductOptions(this.state.options) : ""}
                      </TableBody>
                    </Table>
                    <span style={styles.error}>{ this.state.errorUniqueOptions }</span>
                </section>
                <RaisedButton className="view-report" label="Save Changes" onClick={e => { this.updateProductResellers() }} />
        </Card>
      </div>
    )
  }
}

ProductResellers.contextTypes = {
  router: React.PropTypes.object.isRequired
}

import { connect } from 'react-redux'
import { listResellers } from '../../../ax-redux/actions/resellers'
import { readProduct, updateProduct } from '../../../ax-redux/actions/products'

export default connect(
  state => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      resellers: state.resellers.list.data,
      product: state.products.readProduct.data,
      resellersFetching: state.resellers.get.fetching,
      productFetching: state.products.readProduct.fetching,
      //productResellers: state.products.addResellers
      productResellers: {
        fetching: false
      }
    }
  },
  dispatch => {
    return {
      readProduct: (id) => {
          dispatch(readProduct(id))
      },
      updateProduct: (id, data) => {
          dispatch(updateProduct(id, data))
      }
    }
  }
)(ProductResellers)
