import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import CircularProgress from 'material-ui/CircularProgress';
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import RaisedButton from 'material-ui/RaisedButton';
import { browserHistory } from 'react-router'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';

const tabStyle = {
    color: "black"
};

class ResellerDetail extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
      this.props.getReseller(this.props.params.id)
  }

  componentWillUpdate (nextProps, nextState) {
    if (this.props.resellerFetching && nextProps.resellerFetching == false &&
        nextProps.reseller != false) {
          nextProps.listProducts({resellerCode: nextProps.reseller.code})
    }
  }

  addProduct () {
      browserHistory.push("/app/manage-business/edit-product/add");
  }

  editProduct (id) {
      browserHistory.push("/app/manage-business/edit-product/"+id)
  }

  deleteProduct (id) {
      if (confirm("Really delete this product?")) {
          this.props.deleteProduct(id);
      }
  }

  renderContact (data) {
      let reseller = data.reseller ? data.reseller.reseller : false;
      return (
          <Table className="reseller-contact-table">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>Reseller</TableHeaderColumn>
                    <TableHeaderColumn>Contact Email</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                {!!reseller ? (
                <TableRow key={"1"} selectable={false} >
                    <TableRowColumn key={"1.2"}>
                        {reseller.name}
                    </TableRowColumn>
                    <TableRowColumn key={"1.2"}>
                        {reseller.email || "–" }
                    </TableRowColumn>
                </TableRow>
            ): "" }
            </TableBody>
         </Table>
      )
  }

  renderProducts (products) {
      return (
          <Table className="products-table">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>Supplier Product ID</TableHeaderColumn>
                    <TableHeaderColumn>Reseller Product ID</TableHeaderColumn>
                    <TableHeaderColumn>Tour Name</TableHeaderColumn>
                    <TableHeaderColumn>Tour Price</TableHeaderColumn>
                    <TableHeaderColumn>Status</TableHeaderColumn>
                    <TableHeaderColumn>Actions</TableHeaderColumn>
                </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
                { !!products && !!products.length && products.map((product, p)=> (
                        <TableRow key={p} selectable={false} >
                            <TableRowColumn key={"1.1"}>
                                {product.supplierProductId}
                            </TableRowColumn>
                            <TableRowColumn key={"1.2"}>
                                {product.resellerProductId}
                            </TableRowColumn>
                            <TableRowColumn key={"1.3"}>
                                {product.tourName}
                            </TableRowColumn>
                            <TableRowColumn key={"1.4"}>
                                {product.tourPrice}
                            </TableRowColumn>
                            <TableRowColumn key={"1.6"}>
                                {product.status}
                            </TableRowColumn>
                            <TableRowColumn key={"1.5"}>
                            <RaisedButton className="view-report" label="Edit" style={{marginRight: "1em"}}
                                          onClick={e => { this.editProduct(product.id); }} />
                            <RaisedButton className="view-report" label="Delete"
                                            onClick={e => { this.deleteProduct(product.id); }} />
                            </TableRowColumn>
                        </TableRow>
                ))}
            </TableBody>
         </Table>
      )
  }

  render() {
      let fetching = this.props.resellerFetching;
    return (
      <div className="reseller-view" >
        <Card className="inner-edit">
            <div className="reseller-view-section reseller-contact">
                <h2 style={{color:"#434343"}}>Reseller Contact</h2>
                { this.renderContact(this.props.reseller, fetching) }
            </div>
            <div className="reseller-view-section reseller-products">
                <h2 style={{color:"#434343"}}>Reseller Products</h2>
                { this.renderProducts(this.props.products) }
            </div>
            <FloatingActionButton
                className="action-button"
                onTouchTap={e=>{ this.addProduct(); }}
            >
                <ContentAdd />
            </FloatingActionButton>
        </Card>
      </div>
    )
  }
}

import { connect } from 'react-redux'
import { getReseller } from '../../../ax-redux/actions/resellers'
import { listProducts } from '../../../ax-redux/actions/products'

export default connect(
  state => {
    return {
      products: state.products.listProducts.data,
      reseller: state.resellers.get.data,
      resellerFetching: state.resellers.get.fetching,
      productsFetching: state.products.listProducts.fetching
    }
  },
  dispatch => {
    return {
      getReseller: (id) => {
          dispatch(getReseller(id))
      },
      listProducts: (params) => {
         dispatch(listProducts(params))
      }
    }
  }
)(ResellerDetail)
