import React, { Component, PropTypes } from 'react'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import SupplierOverview from './overview.js'

class SupplierManagement extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="supplier-management-index">
            {!!this.props.children ? this.props.children : <SupplierOverview /> }
            </div>
        )
    }
}

import { connect } from 'react-redux'
export default connect(
    state => {
        return {
            role: state.app.role,
            app: state.app,
        }
    },
    dispatch => {
        return {

        }
    }
)(SupplierManagement)
