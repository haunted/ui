/* globals TZ */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import { Card } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import moment from 'moment-timezone';
import { decodeUnit } from '../../../util';
import * as supplierActions from '../../../ax-redux/actions/suppliers';
import * as resellerActions from '../../../ax-redux/actions/resellers';
import * as contractActions from '../../../ax-redux/actions/contracts';
import * as priceListActions from '../../../ax-redux/actions/price-lists';
import AddContractModal from './add-contract-modal';


const styles = {
  tableCell: {
    whiteSpace: 'normal',
    wordWrap: 'wrap',
    padding: 8,
  },
  gridHeader: {
    width: '100%',
    height: '60px',
  },
  editProduct: {
    width: '110px',
    overflow: 'visible',
  },
  editReseller: {
    width: '130px',
    overflow: 'visible',
  },
  checkboxCol: {
    width: '48px',
  },
  priceCol: {
    width: '96px',
  },
  productName: {
    cursor: 'pointer',
  },
  addContractButton: {
    float: 'right',
  },
  addButton: {
    float: 'right',
    marginTop: '1em',
    marginRight: '1em',
  },
  title: {
    color: 'rgb(67, 67, 67)',
    display: 'inline-block',
    marginLeft: '1em',
    marginRight: '1em',
  },
};


class Contracts extends Component {
  constructor(props) {
    super(props);

    this.addContract = this.addContract.bind(this);
    this.editContract = this.editContract.bind(this);
    this.getListName = this.getListName.bind(this);
    this.closeModal = this.closeModal.bind(this);

    this.state = {
      addContractModalOpen: false,
      editableContractData: null,
    };
  }

  componentWillMount() {
    const {
      adminSupplierId,
      supplierId,
      listContracts,
      listPriceLists,
    } = this.props;

    listContracts(adminSupplierId || supplierId);
    listPriceLists(adminSupplierId || supplierId);
  }

  componentWillUpdate(nextProps) {
    const {
      supplierId,
      adminSupplierId,
      supplierFetching,
      supplier,
    } = this.props;

    const adminSupplierIdChanged = nextProps.adminSupplierId &&
      adminSupplierId !== nextProps.adminSupplierId;

    if (adminSupplierId !== nextProps.adminSupplierId || supplierId !== nextProps.supplierId) {
      this.props.listContracts(nextProps.adminSupplierId || nextProps.supplierId);
      this.props.listPriceLists(nextProps.adminSupplierId || nextProps.supplierId);
    }

    if ((!supplier || adminSupplierIdChanged) && !supplierFetching) {
      this.props.getSupplier(nextProps.adminSupplierId || nextProps.supplierId);
    }
  }

  getListName(id) {
    const { priceListsById } = this.props;

    return priceListsById[id] ? priceListsById[id].name : '–';
  }

  getContractData(contractId) {
    const { contracts } = this.props;
    const contractData = contracts.find(c => c.id === contractId);

    return {
      hasDateRange: !!contractData.validFrom && !!contractData.validUntil,
      ...contractData,
    };
  }

  closeModal() {
    this.setState({
      addContractModalOpen: false,
      editableContractData: null,
    });
  }

  addContract(data) {
    const {
      orgCode: supplierCode,
      supplierCatalogId,
      adminSupplierId,
      createContract,
      supplierId,
    } = this.props;

    const contractData = {
      ...data,
      supplierCode,
      supplierCatalogId,
      createdOn: new Date().toISOString(),
    };

    createContract(adminSupplierId || supplierId, contractData);
  }

  editContract(data) {
    const {
      orgCode: supplierCode,
      adminSupplierId,
      supplierId,
      updateContract,
    } = this.props;

    const contractData = {
      ...data,
      supplierCode,
    };

    updateContract(adminSupplierId || supplierId, contractData.id, contractData);
  }

  renderContracts() {
    const {
      supplierCatalogId,
      contracts,
    } = this.props;

    const contractsData = contracts.filter(c => (
      supplierCatalogId === -1 || supplierCatalogId === c.supplierCatalogId
    )).map(c => ({
      ...c,
      reseller: c.resellerName ? c.resellerName : c.resellerCode,
      validFrom: moment(c.validFrom).tz(TZ).format('DD-MMM-YYYY'),
      validUntil: moment(c.validUntil).tz(TZ).format('DD-MMM-YYYY'),
      priceOn: c.terms.priceOn,
      priceList: this.getListName(c.priceListId),
    }));

    return (
      <Table
        className="products-table"
        fixedHeader={false}
        style={{ tableLayout: 'auto' }}
      >
        <TableHeader
          adjustForCheckbox={false}
          displayRowCheckbox={false}
          enableSelectAll={false}
          displaySelectAll={false}
        >
          <TableRow>
            <TableHeaderColumn>
              <div className="inner">Name</div>
            </TableHeaderColumn>
            <TableHeaderColumn>
              <div className="inner">Reseller</div>
            </TableHeaderColumn>
            <TableHeaderColumn>
              <div className="inner">Valid From</div>
            </TableHeaderColumn>
            <TableHeaderColumn>
              <div className="inner">Valid Until</div>
            </TableHeaderColumn>
            <TableHeaderColumn>
              <div className="inner">External Agreement</div>
            </TableHeaderColumn>
            <TableHeaderColumn>
              <div className="inner">Price List</div>
            </TableHeaderColumn>
            <TableHeaderColumn>
              <div className="inner">Price At</div>
            </TableHeaderColumn>
            <TableHeaderColumn />
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {contractsData.map(c => (
            <TableRow key={c.id} selectable={false} >
              <TableRowColumn style={styles.tableCell}>
                {c.name}
              </TableRowColumn>
              <TableRowColumn style={styles.tableCell}>
                {c.reseller}
              </TableRowColumn>
              <TableRowColumn style={styles.tableCell}>
                {c.validFrom !== 'Invalid date' ? c.validFrom : '–'}
              </TableRowColumn>
              <TableRowColumn style={styles.tableCell}>
                {c.validUntil !== 'Invalid date' ? c.validUntil : '–'}
              </TableRowColumn>
              <TableRowColumn style={styles.tableCell}>
                {c.externalAgreement}
              </TableRowColumn>
              <TableRowColumn style={styles.tableCell}>
                {c.priceList}
              </TableRowColumn>
              <TableRowColumn style={styles.tableCell}>
                {decodeUnit(c.priceOn)}
              </TableRowColumn>
              <TableRowColumn style={styles.tableCell}>
                <FlatButton
                  label="Edit"
                  onTouchTap={() => this.setState({
                    editableContractData: this.getContractData(c.id),
                  })}
                />
              </TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    );
  }

  render() {
    const {
      addContractModalOpen,
      editableContractData,
    } = this.state;

    return (
      <div className="reseller-view" >
        <h2 style={styles.title}>Contracts</h2>
        <div style={{ float: 'right', marginTop: '1em' }}>
          <RaisedButton
            onTouchTap={() => this.setState({
              addContractModalOpen: true,
              editableContractData: null,
            })}
            label="Add new contract"
            primary
          />
        </div>

        <Card className="inner-edit">
          <div className="reseller-view-section reseller-products">
            {this.renderContracts()}
          </div>
        </Card>

        {(addContractModalOpen) && (
          <AddContractModal
            onSave={this.addContract}
            closeModal={this.closeModal}
            open
          />
        )}

        {(editableContractData) && (
          <AddContractModal
            dialogTitle="Edit Contract"
            onSave={this.editContract}
            closeModal={this.closeModal}
            initialValues={editableContractData}
            open
          />
        )}
      </div>
    );
  }
}

Contracts.propTypes = {
  adminSupplierId: PropTypes.string,
  supplierCatalogId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  supplierId: PropTypes.string.isRequired,
  supplier: PropTypes.shape({
    name: PropTypes.string,
  }),
  supplierFetching: PropTypes.bool.isRequired,
  contracts: PropTypes.arrayOf(PropTypes.object).isRequired,
  orgCode: PropTypes.string.isRequired,
  createContract: PropTypes.func.isRequired,
  updateContract: PropTypes.func.isRequired,
  getSupplier: PropTypes.func.isRequired,
  listPriceLists: PropTypes.func.isRequired,
  listContracts: PropTypes.func.isRequired,
  priceListsById: PropTypes.shape({}).isRequired,
  style: PropTypes.object, // eslint-disable-line
};

Contracts.defaultProps = {
  adminSupplierId: null,
  supplier: null,
  style: {},
};

const mapStateToProps = state => ({
  contracts: state.contracts.listContracts.data ?
    state.contracts.listContracts.data.contracts :
    [],
  priceListsById: state.priceLists.priceListsById,
  orgCode: state.app.user ? state.app.user.account.orgCode : '',
  supplierId: state.app.user.supplierId,
  supplier: state.suppliers.get.data ? state.suppliers.get.data.supplier.supplier : null,
  supplierFetching: state.suppliers.get.fetching,
  adminSupplierId: state.reportFilters.adminSupplierId !== -1 ?
    state.reportFilters.adminSupplierId :
    null,
  supplierCatalogId: state.reportFilters.supplierCatalogId,
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(resellerActions, dispatch),
  ...bindActionCreators(supplierActions, dispatch),
  ...bindActionCreators(contractActions, dispatch),
  ...bindActionCreators(priceListActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Contracts);
