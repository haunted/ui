import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import TextField from 'material-ui/TextField'
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import CircularProgress from 'material-ui/CircularProgress'
import { browserHistory } from 'react-router'
import OrgTelephone from '../org-telephone'
import LocationEditor from '../redeam/location-editor'

const tabStyle = {
    color: "black"
};

class SupplierOverview extends Component {
  constructor(props) {
    super(props)
    this.state = { // add reseller organization fields here
        address1: "",
        address2: "",
        location: {
            city: "",
            state: "",
            country: ""
        },
        city: "",
        state: "",
        country: "",
        telephones: [{
            value: "",
            type: "Office"
        }],
        contacts: [],
        resellers: [],
        username: "",
        validationMessage: ""
    }
  }

  componentDidMount() {
      this.props.listContacts({
        orgCode: this.props.orgCode
      });
      this.props.listResellers()
      if (this.props.adminSupplierId != -1) {
        this.props.getSupplier(this.props.adminSupplierId)
        //this.props.getTablets(this.props.adminSupplierId)
      } else {
        this.props.getSupplier(this.props.supplierId)
        //this.props.getTablets(this.props.supplierId)
      }
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.fetchingResellers && !nextProps.fetchingResellers && nextProps.resellers.length > 0) {
        let resellers = []
        nextProps.resellers.map(reseller => {
            if (reseller.supplierId == this.props.orgCode) {
                resellers.push(reseller)
            }
        });
        if (resellers.length > 0) {
            this.setState({
                resellers: resellers
            })
        }
    }
  }

  addTelephone () {
      let telephones = this.state.telephones;
      telephones.push({
          number: "",
          type: "office"
      })
      this.setState({
          telephones:telephones
      })
  }

  updateTelephoneNumber (number, newNumber, newType) {
      let telephones = this.state.telephones;
      telephones.map(phone => {
        if (phone.number == number) {
            phone.number = newNumber;
            phone.type = newType;
        }
      })
      this.setState({
          telephones:telephones
      })
  }

  removeTelephone (number) {
      let telephones = this.state.telephones;
      telephones.map(phone => {
        if (phone.number == number) {
            phone.number = newNumber;
            phone.type = newType;
        }
      })
      this.setState({
          telephones:telephones
      })
  }

  updateProfile () {
      this.props.updateProfile(this.props.orgCode,
          {
              address: this.state.address,
              city: this.state.city,
              state: this.state.state,
              country: this.state.country,
              contacts: this.state.contacts
          }
      )
  }

  addAccount () {
      browserHistory.push("/app/manage-business/contacts/add");
  }

  editContact (id) {
      browserHistory.push("/app/manage-business/contacts/"+id);
  }

  resellerDetail (keys) {
      if (keys.length == 0)
        return
      let reseller = this.props.resellers[keys[0]];
      browserHistory.push("/app/manage-business/reseller-detail/"+reseller.id);
  }

  disableContract (id) {
      if (confirm("Are you sure you want to remove this contact?")) {
          this.props.disableContract(id);
      }
  }

  renderResellers (rows) {
      if (!rows) {
          return "";
      }
      return rows.map((v, k) => (
          <TableRow key={k} onClick={ ()=> { this.resellerDetail(v.id) }}>
            <TableRowColumn key={k+".1"}>{v.name}</TableRowColumn>
            <TableRowColumn key={k+".2"}>{v.location}</TableRowColumn>
            <TableRowColumn key={k+".3"}>{v.num_connected_redeam_suppliers}</TableRowColumn>
            <TableRowColumn key={k+".4"}>{v.num_products}</TableRowColumn>
            <TableRowColumn key={k+".5"}>{v.total_lifetime_ticket_volume}</TableRowColumn>
          </TableRow>
      ))
  }

  renderContacts (rows) {
      if (!!!rows || !!rows && !!!rows.length) {
          return "";
      }
      return rows.map((contact, k) => (
        <TableRow key={k}>
          <TableRowColumn key={k+".1"}>{contact.title}</TableRowColumn>
          <TableRowColumn key={k+".2"}>{contact.givenName}</TableRowColumn>
          <TableRowColumn key={k+".3"}>{contact.surname}</TableRowColumn>
          <TableRowColumn key={k+".5"}>{contact.email}</TableRowColumn>
          <TableRowColumn key={k+".6"}>{contact.phone}</TableRowColumn>
          <TableRowColumn key={k+".7"}>
            <RaisedButton className="save-button" label="Edit" onClick={ e=> { this.editContact(contact.id); } } />
            <RaisedButton className="save-button" label="Delete" onClick={e => { this.disableContract(contact.id); }} />
          </TableRowColumn>
        </TableRow>
    ))
  }

  renderTablets (rows) {
      if (!rows) {
          return "";
      }
      return rows.map((account, k) => (
        <TableRow key={k}>
             <TableRowColumn key={k+".1"}>{account.deviceId}</TableRowColumn>
             <TableRowColumn key={k+".2"}>{account.IMEI}</TableRowColumn>
             <TableRowColumn key={k+".3"}>{account.location}</TableRowColumn>
             <TableRowColumn key={k+".4"}>{account.active}</TableRowColumn>
        </TableRow>
    ))
  }

  render() {
      let fetching = this.props.fetchingAccounts ||
                     this.props.fetchingResellers ||
                     this.props.updatingProfile;
    return (
        <div className="supplier-management">
            <Card className="inner-user-edit">
                <h1>{ this.props.orgCode || "Organization Profile"}</h1>
                <div className="org-name">{this.props.name}</div>
                <div className="org-head-office">{this.props.headOffice}</div>
                <section className="profile-section org-address">
                    <h2>Address</h2>
                    <TextField defaultValue="" hintText={"address line 1"}
                            key={"address1"} style={{paddingLeft: "0.5em", display:"block"}}
                            onBlur={e=>{ this.setState({address1: e.target.value }); }}
                    />
                    <TextField defaultValue="" hintText={"address line 2"}
                            key={"address2"} style={{paddingLeft: "0.5em", marginBottom: "0.5em"}}
                            onBlur={e=>{ this.setState({address2: e.target.value }); }}
                    />
                    <LocationEditor
                      value={this.state.location}
                      onChange={location => this.setState({location: location, validationMessage: ""})}
                      onError={error => {this.setState({validationMessage: error})} }
                    />
                </section>
                <section className="profile-section org-telephones">
                    <h2>Telephones</h2>
                    <div className="org-telephones">
                    {
                        this.state.telephones.map(phone => (
                            <OrgTelephone number={phone.number}
                                          type={phone.type}
                                          onTelephone={ (number, type) => {
                                              this.updateTelephoneNumber(phone.number, number, type)
                                          } }
                            />
                        ))
                    }
                    </div>
                    <RaisedButton className="add-contact-button" label="Add Telephone"
                                  style={{marginTop:"0.5em"}} onClick={e=>{ this.addTelephone(); }}/>
                </section>
                <section className="profile-section save-changes">
                  <RaisedButton className="view-report" label="Save Changes"
                                onClick={e => { this.updateProfile(); }} />
                </section>
                <section className="profile-section org-contacts">
                    <h2>Supplier Contacts</h2>
                    <Table className="users">
                      <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                        <TableRow>
                          <TableHeaderColumn>Title</TableHeaderColumn>
                          <TableHeaderColumn>First Name</TableHeaderColumn>
                          <TableHeaderColumn>Last Name</TableHeaderColumn>
                          <TableHeaderColumn>Email</TableHeaderColumn>
                          <TableHeaderColumn>Phone</TableHeaderColumn>
                          <TableHeaderColumn>Action</TableHeaderColumn>
                        </TableRow>
                      </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        { this.renderContacts(this.props.contacts) }
                      </TableBody>
                    </Table>
                    {this.props.fetchingAccounts ?
                        <div className="">
                          <CircularProgress color={"#27367a"} size={96} />
                        </div>
                    : (
                        <RaisedButton className="add-contact-button" label="Add Contact"
                                      style={{marginTop:"0.5em"}} onClick={e=>{ this.addAccount(); }}/>
                    ) }
                </section>
                <section className="profile-section org-tablet-inventory">
                    <h2>Tablet Inventory</h2>
                            <Table className="users">
                              <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                                <TableRow>
                                  <TableHeaderColumn>Device ID</TableHeaderColumn>
                                  <TableHeaderColumn>IMEI</TableHeaderColumn>
                                  <TableHeaderColumn>Location</TableHeaderColumn>
                                  <TableHeaderColumn>Active</TableHeaderColumn>
                                </TableRow>
                              </TableHeader>
                              <TableBody displayRowCheckbox={false}>
                                {!this.props.fetchingTablets ? this.renderTablets(this.props.tablets) : ""}
                              </TableBody>
                            </Table>
                            {(this.props.fetchingTablets ? (
                                <div className="loading-circle">
                                  <CircularProgress color={"#27367a"} size={96} />
                                </div>
                            ) : "")}
                </section>
                <section className="profile-section org-api-keys">
                    <h2>API Keys</h2>
                    <pre>{this.props.apiKeys}</pre>
                </section>
                <section className="profile-section connected-resellers">
                    <h2>Connected Resellers</h2>
                    <Table className="resellers index" onRowSelection={keys => this.resellerDetail(keys)} >
                      <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                        <TableRow>
                          <TableHeaderColumn>Name</TableHeaderColumn>
                          <TableHeaderColumn>Location</TableHeaderColumn>
                          <TableHeaderColumn>Connected Redeam Suppliers</TableHeaderColumn>
                          <TableHeaderColumn># of Products</TableHeaderColumn>
                          <TableHeaderColumn>Total Lifetime Ticket Volume</TableHeaderColumn>
                        </TableRow>
                      </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        {!this.props.fetchingResellers ? this.renderResellers(this.props.resellers) : ""}
                      </TableBody>
                    </Table>
                </section>
            </Card>
        </div>
    )
  }
}

import { connect } from 'react-redux'
import { listResellers } from '../../../ax-redux/actions/resellers'
import { getSupplier, updateSupplier } from '../../../ax-redux/actions/suppliers'
import { getTablets } from '../../../ax-redux/actions/tablets'
import {
  disableContract
}  from '../../../ax-redux/actions/contacts'
import {
  listAccounts
} from '../../../ax-redux/actions/accounts'
import {
  listGroups,
  listAccountsByGroup
}  from '../../../ax-redux/actions/groups'

export default connect(
  state => {
    return {
        contacts: state.accounts.list.data,
        groups: state.groups.list.data,
        fetchingAccounts: state.accounts.list.fetching,
        fetchingTablets: state.tablets.getTablets.fetching,
        fetchingResellers: state.resellers.list.fetching,
        updatingProfile: state.suppliers.update.fetching,
        resellers: state.resellers.list.data.resellers,
        supplier: state.suppliers.get.data,
        orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
        tablets: state.tablets.getTablets.data,
        apiKeys: "",
        adminSupplierId: state.reportFilters.adminSupplierId,
        supplierId: state.app.user.supplierId
        // auth: state.auth
    }
  },
  dispatch => {
    return {
        listResellers: (params) => {
          dispatch(listResellers(params))
        },
        listContacts: (params) => {
          dispatch(listAccounts(params))
        },
        disableContract: (id) => {
            dispatch(disableContract(id))
        },
        listGroups: (params) => {
            dispatch(listGroups(params))
        },
        updateProfile: (id, params) => {
            dispatch(updateSupplier(id, params))
        },
        getSupplier: (params) => {
            dispatch(getSupplier(params))
        },
        getTablets: (params) => {
            dispatch(getTablets(params))
        }
    }
  }
)(SupplierOverview)
