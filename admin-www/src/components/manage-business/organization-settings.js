import React, { Component, PropTypes } from 'react'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import RaisedButton from 'material-ui/RaisedButton'
import AgeBandsModal from './product/age-bands-modal'
import OperationHoursModal from './product/operation-hours-modal'
import TimezonesPopover from '../timezones-popover'
import { getOrgType, serializeTravelerTypes} from '../../util'
import Snackbar from 'material-ui/Snackbar'

const styles = {
  form: {

  },
  heading: {
    marginTop: '0.6em',
    marginLeft: '0.75em',
    color: 'rgb(67, 67, 67)'
  },
  subHeading: {
    marginLeft: '1em',
    marginBottom: 0,
    color: 'rgb(67, 67, 67)'
  },
  formOptions: {
    marginTop: "3em",
    marginLeft: "1em",
    marginBottom: "1em"
  },
  formOption: {
    marginTop: '1em',
    marginRight: '1em'
  },
  timezones: {
    marginTop: '1em',
    marginLeft: '1em'
  }
}

class OrganizationSettings extends Component {

    constructor(props) {
        super(props)
    }

    componentWillMount () {

      this.setState({
        formModified: false,
        snackbarText: "",
        showSnackbar: false,
        defaultTypesLoaded: false,
        refreshingTravelerTypes: false,
        openingHours: [],
        closedDates: [],
        timezone: 'America/New_York',
        orgCode: '',
        orgType: '',
        travelerTypes: [
          {type: 'Adult', from: 18, to: 100},
          {type: 'Child', from: 2, to: 7},
          {type: 'Youth', from: 8, to: 13},
          {type: 'Infant', from: 0, to: 1},
          {type: 'Military', from: 0, to: 100},
          {type: 'Student', from: 0, to: 100}
        ]
      })
      const { route } = this.props
      const { router } = this.context
      router.setRouteLeaveHook(route, v=> this.routerWillLeave(v))
      this.props.readSettings(this.props.orgCode, getOrgType(this.props.role))

    }

    componentWillUpdate ( nextProps, nextState ) {

      let orgSettingsLoaded = nextProps.orgSettings != false,
          orgSettingsJustLoaded = this.props.orgSettingsFetching && nextProps.orgSettingsFetching == false

      if ( (orgSettingsJustLoaded && orgSettingsLoaded) || (!nextState.defaultTypesLoaded && orgSettingsLoaded) ) {

        let data = nextProps.orgSettings
        this.setState({
          travelerTypes: [],
          defaultTypesLoaded: true,
          refreshingTravelerTypes: true
        })
        this.setState({
          openingHours: !!data.operationHours.openingHours ? data.operationHours.openingHours : [],
          closedDates: !!data.operationHours.closedDates ? data.operationHours.closedDates : [],
          timezone: data.operationHours.timezone,
          travelerTypes: data.travelerTypes,
          orgCode: data.orgCode,
          orgType: data.orgType,
          defaultTypesLoaded: true
        }, () => {

          setTimeout( () => {

            this.setState({
              refreshingTravelerTypes: false
            })

          }, 250)

        })

      }

      if (this.props.orgSettingsError == false && nextProps.orgSettingsError != false) {

        this.setState({
          showSnackbar: true,
          snackbarText: nextProps.orgSettingsError.data.Error
        })

      }

      if (this.props.orgSettingsSaving != false && nextProps.orgSettingsSaving == false && nextProps.orgSettingsError == false) {

        this.setState({
          formModified: false,
          showSnackbar: true,
          snackbarText: "Settings Updated"
        })

      }

    }

    updateTravelerTypes (data) {
      console.log('updateTravelerTypes', data)
      this.setState({
        travelerTypes: data,
        formModified: true
      })
    }
    updateOperationHours (data) {
      console.log('updateOperationHours', data)
      this.setState({
        openingHours: data.openingHours,
        closedDates: data.closedDates,
        formModified: true
      })
    }
    updateTimezone (data) {
      this.setState({
        timezone: data,
        formModified: true
      })
    }

    save () {
      let travelerTypes = serializeTravelerTypes(this.state.travelerTypes, true),
          data = {
            orgType: getOrgType(this.props.role),
            orgCode: this.props.orgCode,
            operationHours: {
              openingHours: this.state.openingHours,
              closedDates: this.state.closedDates,
              timezone: this.state.timezone,
            },
          travelerTypes
        }
      console.log("saving organization settings", data)
      // call save organization settings endpoint
      this.props.updateSettings(data)
    }

    routerWillLeave() {
        if (this.state.formModified) {
          return "Your changes haven't been saved yet.\nAre you sure you want to leave?"
        }
    }

    handleRequestClose () {
        this.setState({
          showSnackbar: false,
        })
    }

    render() {
        return (
          <div style={styles.form}>
            <h1 style={styles.heading}>Settings</h1>
            <Tabs>
              <Tab label="Hours of Operation" >
                <div>
                  <h2 style={styles.subHeading}>Timezone</h2>
                  <TimezonesPopover
                    onChange={ v=> this.updateTimezone(v) }
                    value={ this.state.timezone }
                    style={ styles.timezones }
                  />
                  {/* <OperationHoursModal onSave={ data=> this.updateOperationHours(data) }
                    openingHours={this.state.openingHours}
                    closedDates={this.state.closedDates}
                    modal={false}
                  /> */}
                </div>
              </Tab>
              <Tab label="Traveler Types" >
                <div>
                  <AgeBandsModal travelerTypes={this.state.travelerTypes}
                                 refreshing={ this.state.refreshingTravelerTypes }
                                 onSave={data => this.updateTravelerTypes(data) }
                                 modal={false}
                  />
                </div>
              </Tab>
            </Tabs>
            <div style={styles.formOptions}>
              <RaisedButton
                    onTouchTap={e => { this.save() } }
                    style={styles.formOption}
                    label="Save"
               />
            </div>
            <Snackbar message={this.state.snackbarText}
                      onRequestClose={(e)=>this.handleRequestClose()}
                      open={this.state.showSnackbar}
                      autoHideDuration={4000}
            />
          </div>
        )
    }
}

OrganizationSettings.contextTypes = {
  router: React.PropTypes.object.isRequired
}

import { connect } from 'react-redux'
import {
  readSettings,
  updateSettings
} from '../../ax-redux/actions/org-settings'

export default connect(
    state => {
        return {
            orgSettings: state.orgSettings.readOrgSettings.data,
            orgSettingsFetching: state.orgSettings.readOrgSettings.fetching,
            orgSettingsSaving: state.orgSettings.updateOrgSettings.fetching,
            orgSettingsError: state.orgSettings.updateOrgSettings.error,
            role: state.app.role,
            orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
            app: state.app,
        }
    },
    dispatch => {
        return {
          readSettings: (orgCode, orgType) => {
            dispatch(readSettings(orgCode, orgType))
          },
          updateSettings: (data) => {
            dispatch(updateSettings(data))
          }
        }
    }
)(OrganizationSettings)
