import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'
import TextField from 'material-ui/TextField';

export default class OrgTelephone extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      number: "",
      type: "office",
      label: "Office"
    }
  }

  componentWillMount() {

  }

  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
    console.log(event.currentTarget);
  }

  handleRequestClose() {
    this.setState({
      open: false,
    })
  }

  onMenuItem(e, item, i) {
    this.props.onTelephone(this.state.number, item.props.value)
    let values = [
        {type: "office", label: "Office"},
        {type: "accounting", label: "Accounting"},
        {type: "general manager", label: "General Manager"},
    ];
    this.setState({
      open: false,
      type: values[i].type,
      label: values[i].label
    })
  }
  onNumberUpdate (number) {
      this.props.onTelephone(number, this.state.type)
      this.setState({"number": number})
  }
  getPhoneType () {
      console.log("get phone type", (this.state.type != "" ?
             this.state.type :
             (this.props.type != null && this.props.type != "" ? this.props.type : "Office")))
      return this.state.type != "" ?
             this.state.type :
             (this.props.type != null && this.props.type != "" ? this.props.type : "Office")
  }
  render() {
    return (
    <div className="org-telephone">
        <TextField defaultValue={this.props.number} hintText={"Telephone Number"}
                   key={"Telephone Number"}
                   errorText={this.state.errorTourName}
                   onBlur={e=>{ this.onNumberUpdate(e.target.value)}}
        />
      <div className="telephone-popover">
            <RaisedButton
              onTouchTap={e => this.handleTouchTap(e)}
              label={this.state.label}
            />
            <Popover
              open={this.state.open}
              anchorEl={this.state.anchorEl}
              anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
              targetOrigin={{horizontal: 'left', vertical: 'top'}}
              onRequestClose={() => this.handleRequestClose()}
            >
              <Menu onItemTouchTap={(e, item, i) => this.onMenuItem(e, item, i)}>
                <MenuItem primaryText="Office" value="office" />
                <MenuItem primaryText="Accounting" value="accounting" />
                <MenuItem primaryText="General Manager" value="general manager" />
              </Menu>
            </Popover>
        </div>
      </div>
    );
  }
}
OrgTelephone.propTypes = {
  onTelephone: PropTypes.func.isRequired,
  number: PropTypes.string,
  type: PropTypes.string
}
