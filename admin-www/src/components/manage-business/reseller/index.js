import React, { Component, PropTypes } from 'react'
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import ResellerOverview from './overview.js'

class ResellerManagement extends Component {

    constructor(props) {

        super(props)

    }

    render() {

        return (

            <div className="reseller-management-index">
                { !!this.props.children ? this.props.children : <ResellerOverview /> }
            </div>
        )

    }

}

import { connect } from 'react-redux'
export default connect(
    state => {
        return {
            role: state.app.role,
            app: state.app,
        }
    },
    dispatch => {
        return {

        }
    }
)(ResellerManagement)
