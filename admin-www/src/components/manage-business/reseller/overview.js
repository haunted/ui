import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {Tabs, Tab} from 'material-ui/Tabs'
import Slider from 'material-ui/Slider'
import TextField from 'material-ui/TextField'
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton'
import ContentAdd from 'material-ui/svg-icons/content/add'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import CircularProgress from 'material-ui/CircularProgress'
import { browserHistory } from 'react-router'
import OrgTelephone from '../org-telephone'
import LocationEditor from '../redeam/location-editor'

const tabStyle = {
    color: "black"
};

let styles = {
    
}

class ResellerOverview extends Component {

  constructor(props) {

    super(props)
    this.state = { // add reseller organization fields here
        address1: "",
        address2: "",
        location: {
            city: "",
            state: "",
            country: ""
        },
        city: "",
        state: "",
        country: "",
        telephones: [{
            value: "",
            type: "Office"
        }],
        contacts: [],
        suppliers: [],
        username: "",
        validationMessage: ""
    }

  }

    componentDidMount() {
    
        // if ( this.props.orgCode != "ACTOUREX" && this.props.orgCode != "REDEAM") {

        //     this.props.listAccounts()

        // }
      
        this.props.getReseller(this.props.resellerId);
        
        if (this.props.adminSupplierId != -1) {
            
            this.props.getReseller(this.props.adminSupplierId)
        
        } else {
        
            this.props.getReseller(this.props.resellerId)
        
        }

  }

  componentWillUpdate( nextProps, nextState ) {

    if (this.props.fetchingAccounts && !nextProps.fetchingAccounts && nextProps.accounts.length > 0) {
        let contacts = [];
        nextProps.accounts.map(account => {
            if (account.supplierId == this.props.supplierId) {
                contacts.push(account);
            }
        });
        if (contacts.length > 0) {
            this.setState({
                contacts: contacts
            })
        }
    }
    if (this.props.fetchingSuppliers && !nextProps.fetchingSuppliers && nextProps.suppliers.length > 0) {
        let suppliers = [];
        nextProps.suppliers.map(reseller => {
            if (reseller.supplierId == this.props.supplierId) {
                suppliers.push(reseller);
            }
        });
        if (suppliers.length > 0) {
            this.setState({
                suppliers: suppliers
            })
        }
    }

  }

  addTelephone () {

      let telephones = this.state.telephones

      telephones.push({
          number: "",
          type: "office"
      })

      this.setState({
          telephones:telephones
      })

  }

  updateTelephoneNumber ( number, newNumber, newType ) {

      let telephones = this.state.telephones

      telephones.map(phone => {

        if (phone.number == number) {

            phone.number = newNumber
            phone.type = newType

        }

      })

      this.setState({
          telephones:telephones
      })

  }

  removeTelephone ( number ) {

      let telephones = this.state.telephones

      telephones.map(phone => {

        if (phone.number == number) {

            phone.number = newNumber
            phone.type = newType

        }

      })

      this.setState({
          telephones:telephones
      })

  }

  updateProfile () {

      this.props.updateProfile(this.props.supplierId, {
              address: this.state.address,
              city: this.state.city,
              state: this.state.state,
              country: this.state.country,
              phone1: this.state.phone1,
              phone2: this.state.phone2,
              contacts: this.state.contacts
      })

  }

  addAccount () {

      browserHistory.push("/app/manage-business/contacts/add")

  }

  editAccount ( id ) {

      browserHistory.push("/app/manage-business/contacts/"+id)

  }

  resellerDetail ( keys ) {

      if (keys.length == 0)
        return
      let reseller = this.props.suppliers[keys[0]];
      browserHistory.push("/app/manage-business/reseller-detail/"+reseller.id)

  }

  deleteAccount (id) {

      let contacts = this.state.contacts,
          index = -1

      if (confirm("Are you sure you want to remove this contact?")) {
          this.props.deleteAccount(id)
      }

  }

  renderContacts (rows) {

    if (!!!rows || rows.length === 0) {

        return (

            <TableRow>
                <TableRowColumn>No Data</TableRowColumn>
                <TableRowColumn></TableRowColumn>
                <TableRowColumn></TableRowColumn>
                <TableRowColumn></TableRowColumn>
                <TableRowColumn></TableRowColumn>
                <TableRowColumn></TableRowColumn>
                <TableRowColumn>
                   
                </TableRowColumn>
            </TableRow>

        )

    } else {

        return rows.map(( account, k ) => (

            <TableRow key={k}>
            <TableRowColumn key={k+".1"}>{account.username}</TableRowColumn>
            <TableRowColumn key={k+".2"}>{account.givenName}</TableRowColumn>
            <TableRowColumn key={k+".3"}>{account.surname}</TableRowColumn>
            <TableRowColumn key={k+".4"}>{account.roles}</TableRowColumn>
            <TableRowColumn key={k+".5"}>{account.email}</TableRowColumn>
            <TableRowColumn key={k+".6"}>{account.phone}</TableRowColumn>
            <TableRowColumn key={k+".7"}>
                <RaisedButton className="save-button" label="Edit" onClick={ e=> { this.editAccount(account.id); } } />
                <RaisedButton className="save-button" label="Delete" onClick={e => { this.deleteAccount(account.id); }} />
            </TableRowColumn>
            </TableRow>

        ))

    }
      
  }

  render() {
      let fetching = this.props.fetchingAccounts ||
                     this.props.fetchingResellers ||
                     this.props.updatingProfile;
    return (

        <div className="supplier-management user-edit">
            <Card className="inner-user-edit">
                <h1>{ this.props.orgCode || "Organization Profile" }</h1>
                <div className="org-name">{this.props.name}</div>
                <div className="org-head-office">{this.props.headOffice}</div>
                <section className="profile-section org-address">
                    <h2>Address</h2>
                    <TextField defaultValue="" hintText={"address"}
                            key={"adress"} style={{paddingLeft: "0.5em"}}
                            onChange={e=>{ this.setState({adress: e.target.value }); }}
                    />
                </section>
                <section className="profile-section org-telephones">
                    <h2>Telephones</h2>
                    <div className="org-telephones">
                    {
                        this.state.telephones.map(phone => (
                            <OrgTelephone number={phone.number}
                                          type={phone.type}
                                          onTelephone={ (number, type) => {
                                              this.updateTelephoneNumber(phone.number, number, type)
                                          } }
                            />
                        ))
                    }
                    </div>
                </section>
                <section className="profile-section org-contacts">
                    <h2>Contacts</h2>
                    <Table className="users">
                      <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                        <TableRow>
                          <TableHeaderColumn>Username</TableHeaderColumn>
                          <TableHeaderColumn>First Name</TableHeaderColumn>
                          <TableHeaderColumn>Last Name</TableHeaderColumn>
                          <TableHeaderColumn>Roles</TableHeaderColumn>
                          <TableHeaderColumn>Email</TableHeaderColumn>
                          <TableHeaderColumn>Phone</TableHeaderColumn>
                          <TableHeaderColumn>Action</TableHeaderColumn>
                        </TableRow>
                      </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        { this.renderContacts(this.state.contacts) }
                      </TableBody>
                    </Table>
                </section>
                <section className="profile-section org-api-keys">
                    <h2>API Keys</h2>
                   <Table>
                       <TableRow>
                            <TableRowColumn>No Data</TableRowColumn>
                            <TableRowColumn></TableRowColumn>
                            <TableRowColumn></TableRowColumn>
                            <TableRowColumn></TableRowColumn>
                            <TableRowColumn></TableRowColumn>
                            <TableRowColumn></TableRowColumn>
                            <TableRowColumn>
                            
                            </TableRowColumn>
                        </TableRow>
                   </Table>
                </section>
                <section className="profile-section connected-suppliers">
                    <span style={ styles.noData }>No Data</span>
                </section>
                <RaisedButton className="view-report" label="Save Changes"
                              onClick={e => { this.updateProfile(); }} />
            </Card>
        </div>

    )
  }
}

import { connect } from 'react-redux'
import { listSuppliers } from '../../../ax-redux/actions/suppliers'
import { getReseller, updateReseller } from '../../../ax-redux/actions/resellers'
import { getTablets } from '../../../ax-redux/actions/tablets'
import {
  listAccounts,
  deleteAccount,
  listAccountPermissions,
}  from '../../../ax-redux/actions/accounts'
import {
  listGroups,
  listAccountsByGroup
}  from '../../../ax-redux/actions/groups'

export default connect(
  state => {
    return {
        accounts: state.accounts.list.data,
        groups: state.groups.list.data,
        fetchingAccounts: state.accounts.list.fetching,
        fetchingTablets: state.tablets.getTablets.fetching,
        fetchingSuppliers: state.suppliers.list.fetching,
        updatingProfile: state.suppliers.update.fetching,
        suppliers: state.suppliers.list.data.suppliers,
        supplier: state.suppliers.get.data,
        tablets: state.tablets.getTablets.data,
        apiKeys: "",
        orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
        adminSupplierId: state.reportFilters.adminSupplierId,
        resellerId: state.app.user.resellerId
        // auth: state.auth
    }
  },
  dispatch => {
    return {
        listSuppliers: (params) => {
          dispatch(listSuppliers(params))
        },
        listAccounts: (params) => {
          dispatch(listAccounts(params))
        },
        listAccountsByGroup: (params) => {
          dispatch(listAccountsByGroup(params))
        },
        listAccountPermissions: id => {
          dispatch(listAccountPermissions(id))
        },
        deleteAccount: (id) => {
            dispatch(deleteAccount(id))
        },
        listGroups: (params) => {
            dispatch(listGroups(params))
        },
        updateProfile: (id, params) => {
            dispatch(updateReseller(id, params))
        },
        getReseller: (params) => {
            dispatch(getReseller(params))
        },
        getTablets: (params) => {
            dispatch(getTablets(params))
        }
    }
  }
)(ResellerOverview)
