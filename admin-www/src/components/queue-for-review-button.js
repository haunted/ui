import React, { Component, PropTypes } from 'react'
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import RejectionReasonDropdown from './rejection-reason-dropdown';

class QueueForReviewButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      activated: false,
      comments: ""
    }
  }

  toggleForm () {
    this.setState({
        activated: !this.state.activated
    })
  }

  submit () {
      this.props.rejectVoucher(this.props.voucherId, {
              reason: this.state.comments
      })
      this.toggleForm();
      !! this.props.callback && this.props.callback();
  }

  cancel () {
      this.toggleForm()
  }

  onCommentsChange (e) {
      this.setState({
          comments: e.target.value
      })
  }

  render() {
      if (this.state.activated) {
          return (
              <div className="ticket-lightbox queue-for-review-button">
                  <div className="inner-lightbox inner">
                  <span>Enter Reason (optional):</span>
                  <TextField id="rejection-reason" onChange={ e=> this.onCommentsChange(e) } />
                  <RejectionReasonDropdown callback={ e=> {
                    this.setState({comments: e})
                  } } />
                  <RaisedButton
                        onTouchTap={e => { this.submit() } }
                        className="no-print form-button"
                        label="Submit"
                   />
                   <RaisedButton
                         onTouchTap={e => { this.cancel() } }
                         className="no-print form-button"
                         label="Cancel"
                    />
                </div>
            </div>
          )
      } else {
          return (
              <RaisedButton
                    onTouchTap={e => { this.toggleForm() } }
                    className={"no-print toggle-queue-for-review-button " + (this.props.className || "")}
                    secondary={this.props.secondary}
                    style={(this.props.style  || {})}
                    label={this.props.label || "Queue For Review"}
               />
          )
      }
  }
}

QueueForReviewButton.propTypes = {
  voucherId: PropTypes.string.isRequired
}

import { connect } from 'react-redux'

import {
    rejectVoucher,
    correctVoucher
} from '../ax-redux/actions/vouchers'

export default connect(
  (state, ownProps) => {
    return {

    }
  },
  dispatch => {
    return {
        rejectVoucher: (id, data) => {
            dispatch(rejectVoucher(id, data))
        }
    }
  }
)(QueueForReviewButton)
