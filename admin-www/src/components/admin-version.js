import React, { Component, PropTypes } from 'react'

const styles = {
}

class AdminVersion extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    // right now just assume everything is in USD
    return (
        <div className="admin-version">
            <div className="admin">
                <div className="title">
                    admin
                </div>
                <div className="value">
                    {this.props.admin}
                </div>
            </div>
            {
                this.props.core != "" ? (
                    <div className="core">
                        <div className="title">
                            core
                        </div>
                        <div className="value">
                            {this.props.core}
                        </div>
                    </div>
                ) : ""
            }
        </div>
    )
  }
}

AdminVersion.propTypes = {
    admin: PropTypes.string.isRequired,
    core: PropTypes.string,

}

AdminVersion.defaultProps = {
    admin: "",
    core: ""
}

export default AdminVersion
