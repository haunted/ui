import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Slider from 'material-ui/Slider'

let styles = {
  slider: (type, state) => {
    let to = state.to,
        from = state.from
    if (type == 'from') {
        return {
          width: (from+1)+'%',
          display: 'inline-block'
        }
    } else {
        return {
          width: (100-from-1)+'%',
          display: 'inline-block'
        }
    }
  },
}

export default class AgeRangeSlider extends Component {
  constructor(props) {
    super(props)
    this.defaultValues = {
      from: 0,
      to: 100
    }
    this.state = Object.assign({}, this.defaultValues)
  }
  componentWillMount () {
    this.setState({
      from: this.props.from,
      to: this.props.to
    })
  }
  componentWillUpdate(nextProps, nextState) {

  }

  setAge (how, value) {
    let data = {
      to: this.state.to,
      from: this.state.from
    }
    if (how == 'to') {
      data.to = value
    } else {
      data.from = 100-value
    }
    //console.log("how", how, value)
    this.setState(data)
    this.props.onSave(data)
  }
  render() {
    return (
      <div style={this.props.style || {}}>
        <Slider
          onChange={(e,v) => { this.setAge('from', v) }}
          defaultValue={this.state.from}
          style={styles.slider('from', this.state)}
          axis="x-reverse"
          min={0}
          max={this.state.from+1}
          step={1}
        />
        <Slider
          onChange={(e,v) => { this.setAge('to', v)}}
          defaultValue={this.state.to}
          style={styles.slider('to', this.state)}
          min={this.state.from-1}
          max={100}
          step={1}
        />
      </div>
    )
  }
}

AgeRangeSlider.propTypes = {
  style: PropTypes.object,
  onSave: PropTypes.func.isRequired,
  from: PropTypes.int,
  to: PropTypes.int
}
