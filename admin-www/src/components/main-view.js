import React, {Component} from 'react';

class MainView extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render () {
        let sideMenuOpen = this.props.app.sideMenuOpen,
            adminMenuOpen = this.props.dashboard.adminMenuOpen,
            printMode = this.props.printMode,
            page = this.props.page;

        return (
            <div className={`main-view ${(sideMenuOpen || adminMenuOpen) && !printMode ? (" main-view-with-side"+
                                         (adminMenuOpen && page.search("dashboard") > -1 ? " admin-menu-open" : "")) : ""}`}
            >
                 {this.props.children}
            </div>
        )
    }
}

import { connect } from 'react-redux'

export default connect(
  state => {
    return {
      app: state.app,
      printMode: state.app.printMode,
      dashboard: state.dashboard,
      page: state.routing.locationBeforeTransitions.pathname
    }
  },
  dispatch => {
    return {

    }
  }
)(MainView)
