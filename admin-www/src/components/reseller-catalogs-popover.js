import React from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import { listResellerCatalogs } from '../ax-redux/actions/resellers';

const styles = {
  orgContainer: {
    marginLeft: '1em',
    display: 'inline-block',
  },
  selected: {
    borderLeft: '6px solid #27367a',
  },
};


class ResellerCatalogsPopover extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      resellerOpen: false,
      resellerCatalogId: -1,
      resellerCatalogName: '',
      lookedUpName: false,
    };
  }

  componentWillMount() {
    if (this.props.resellerId != -1) {
      this.props.listResellerCatalogs(this.props.resellerId);
    }
  }

  componentWillUpdate(nextProps) {
    if (nextProps.resellerId !== '' && this.props.resellerId !== nextProps.resellerId) {
      this.props.listResellerCatalogs(nextProps.resellerId);
    }

    const resellerCatalogsJustLoaded = this.props.resellerCatalogsFetching &&
      !nextProps.resellerCatalogsFetching &&
      !!nextProps.resellerCatalogs;

    const alreadyHasResellerCatalogs = !resellerCatalogsJustLoaded && !!nextProps.resellerCatalogs;
    const { lookedUpName } = this.state;

    if (resellerCatalogsJustLoaded || alreadyHasResellerCatalogs && !lookedUpName) {
      if (nextProps.resellerCatalogs.length > 0) {
        this.setState({
          resellerCatalogName: nextProps.resellerCatalogs[0].name,
        });

        this.selectCatalog(nextProps.resellerCatalogs[0]);
      }

      this.setState({
        lookedUpName: true,
      });
    }
  }

  handleTouchTap(event, which) {
    event.preventDefault();
    this.setState({
      resellerOpen: which === 'reseller',
      anchorEl: event.currentTarget,
    });
  }

  handleRequestClose() {
    this.setState({
      supplierOpen: false,
      resellerOpen: false,
      passIssuerOpen: false,
    });
  }

  selectCatalog(catalog) {
    this.setState({
      resellerOpen: false,
      resellerCatalogId: catalog.id,
      resellerCatalogName: `${catalog.name} (${catalog.typeOf === 'TYPE_PASS' ? 'Pass' : 'Reseller'})`,
    });

    this.props.onResellerCatalog(catalog);
  }

  render() {
    const { resellerCatalogs } = this.props;
    const {
      resellerCatalogId,
      resellerCatalogName,
    } = this.state;

    const catalogs = resellerCatalogs ?
      resellerCatalogs.filter(c => ['TYPE_RESELLER', 'TYPE_PASS'].includes(c.typeOf))
      : [];

    return (
      <div>
        {resellerCatalogs ? (
          <span style={styles.orgContainer}>
            <RaisedButton
              onTouchTap={(e) => { this.handleTouchTap(e, 'reseller'); }}
              label={resellerCatalogName || 'Reseller Catalog'}
              labelStyle={{ textTransform: 'none' }}
            />
            <Popover
              onRequestClose={(e) => { this.handleRequestClose(e); }}
              anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
              targetOrigin={{ horizontal: 'left', vertical: 'top' }}
              anchorEl={this.state.anchorEl}
              open={this.state.resellerOpen}
            >
              <Menu>
                {catalogs.map(c => (
                  <MenuItem
                    primaryText={`${c.name} (${c.typeOf === 'TYPE_PASS' ? 'Pass' : 'Reseller'})`}
                    onClick={() => this.selectCatalog(c)}
                    style={resellerCatalogId === c.id ? styles.selected : {}}
                    key={c.id}
                  />
                ))}
              </Menu>
            </Popover>
          </span>
        ) : ''}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  let filters = state.reportFilters,
    suppliers = state.suppliers,
    resellers = state.resellers;

  return {
    role: state.app.initialRole,
    adminResellerId: filters.adminResellerId,
    resellerCatalogs: resellers.listCatalogs.data ? resellers.listCatalogs.data.catalogs : false,
    resellerCatalogsFetching: resellers.listCatalogs.fetching,
  };
};
const mapDispatchToProps = dispatch => ({
  setSupplierCatalogId: (v) => {
    dispatch(setSupplierCatalogId(v));
  },
  setResellerCatalogId: (v) => {
    dispatch(setResellerCatalogId(v));
  },
  setPassIssuerCatalogId: (v) => {
    dispatch(setPassIssuerCatalogId(v));
  },
  listResellerCatalogs: (resellerId) => {
    dispatch(listResellerCatalogs(resellerId));
  },
  listSupplierCatalogs: (supplierId) => {
    dispatch(listSupplierCatalogs(supplierId));
  },
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResellerCatalogsPopover);
