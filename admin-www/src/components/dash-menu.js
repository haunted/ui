import React from 'react';
import { browserHistory } from 'react-router'
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import ActionHome from 'material-ui/svg-icons/action/home';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';

const DashMenu = ({title}) => (
  <AppBar
    title={title}
	className="dash-menu"
    iconElementLeft={<div></div>}

  />
);

export default DashMenu;
