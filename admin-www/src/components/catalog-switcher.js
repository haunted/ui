import React from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import { isInternalStaff } from '../util'

let styles = {
    orgContainer: {
        marginLeft: "1em",
        display: "inline-block"
    },
    selected: {
        borderLeft: '6px solid #27367a'
    }
}

class CatalogSwitcher extends React.Component {

  constructor(props) {

    super(props);
    this.state = {
        supplierOpen: false,
        resellerOpen: false,
        passIssuerOpen: false,
        supplierLabel: "",
        resellerLabel: "",
        passIssuerLabel: ""
    }

  }

  componentWillMount() {

    if (this.props.supplierId != "")

        this.props.listSupplierCatalogs(this.props.supplierId)

    if (this.props.resellerId != -1)

        this.props.listResellerCatalogs(this.props.resellerId)

    if (this.props.passIssuerId != -1)

        this.props.listPassIssuerCatalogs(this.props.passIssuerId)

  }

  componentWillReceiveProps ( nextProps ) {

    if ( nextProps.supplierId != "" && this.props.supplierId != nextProps.supplierId )

        this.props.listSupplierCatalogs(nextProps.supplierId)

     if ( nextProps.resellerId != "" && this.props.resellerId != nextProps.resellerId )

        this.props.listResellerCatalogs(nextProps.resellerId)

    if ( nextProps.passIssuerId != "" && this.props.passIssuerId != nextProps.passIssuerId )

        this.props.listPassIssuerCatalogs(nextProps.passIssuerId)

  }

  componentWillUpdate ( nextProps, nextState ) {


  }

  createHandleRequestClose ( type ) {

    return ( event ) => {

        this.setState({
            supplierOpen: false,
            resellerOpen: false,
            passIssuerOpen: false
        })

    }

  }

  createHandleTouchTap ( type ) {
  
    return ( event ) => {
    
        event.preventDefault();
        this.setState({
            supplierOpen: type == 'supplier',
            resellerOpen: type == 'reseller',
            passIssuerOpen: type == 'pass-issuer',
            anchorEl: event.currentTarget
        })

    }

  }

  label( defaultValue, type ) {

    let label = ""
    
    switch ( type ) {

        case "supplier":
            label =  !! this.state.supplierLabel ? this.state.supplierLabel : defaultValue 
        break
        case "reseller":
            label = !! this.state.resellerLabel ? this.state.resellerLabel : defaultValue 
        break
        case "pass-issuer":
            label = !! this.state.passIssuerLabel ? this.state.passIssuerLabel : defaultValue 
        break
    }
    
    return label

  }

  switchCatalog (type, id, name) {

    let data = Object.assign({}, this.state, {
       supplierOpen: false,
       resellerOpen: false,
       passIssuerOpen: false
    })

      switch ( type ) {

          case "supplier":
            data.supplierLabel = name
            this.props.setSupplierCatalogId(id)
          break
          case "reseller":
            data.resellerLabel = name
            this.props.setResellerCatalogId(id)
          break
          case "pass-issuer":
            data.passIssuerLabel = name
            this.props.setPassIssuerCatalogId(id)
          break

      }

    this.setState( data )

  }

  render() {

    let supplierId = this.props.supplierId,
        resellerId = this.props.resellerId,
        passIssuerId = this.props.passIssuerId,
        supplierCatalogs = this.props.supplierCatalogs,
        resellerCatalogs = this.props.resellerCatalogs,
        passIssuerCatalogs = this.props.passIssuerCatalogs,
        supplierCatalogId = this.props.supplierCatalogId,
        resellerCatalogId = this.props.resellerCatalogId

    return (
      <div className="role-switcher">
        {
            !!supplierId && supplierId != -1 && supplierCatalogs ? (
                <span style={styles.orgContainer}>
                    <RaisedButton onTouchTap={ this.createHandleTouchTap( 'supplier' ) }
                                  label={ this.label("Default Catalog", "supplier" ) }
                                  primary={true}
                    />
                    <Popover onRequestClose={ this.createHandleRequestClose( 'supplier' ) }
                             anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                             targetOrigin={{horizontal: 'left', vertical: 'top'}}
                             anchorEl={this.state.anchorEl}
                             open={this.state.supplierOpen}
                    >
                        <Menu>
                        {
                            !!! this.props.supplierCatalogs || this.props.supplierCatalogs && this.props.supplierCatalogs.length == 0 ? (
                                <MenuItem primaryText={"Supplier Catalog"} 
                                          onClick={()=>{this.switchCatalog( "supplier", "", "Supplier Catalog" )}}
                                          style={styles.selected}
                                          key={0}
                                />
                            ) :
                            this.props.supplierCatalogs.filter((catalog)=> { return catalog.typeOf == 'TYPE_SUPPLIER' }).map((catalog, index) => {
                                return (
                                    <MenuItem primaryText={catalog.name} 
                                              onClick={()=>{this.switchCatalog( "supplier", catalog.id, catalog.name )}}
                                              style={supplierCatalogId == catalog.id ? styles.selected : {}} 
                                              key={index}
                                    />
                                )
                            })
                        }
                        </Menu>
                    </Popover>
                </span>
            ): ""
        }
        {
            !!resellerId && resellerId != -1 && resellerCatalogs ? (
                <span style={styles.orgContainer}>
                    <RaisedButton onTouchTap={ this.createHandleTouchTap( 'reseller' ) }
                                  label={ this.label( "Default Catalog", "reseller" ) }
                    />
                    <Popover onRequestClose={ this.createHandleRequestClose( 'reseller' ) }
                             anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                             targetOrigin={{horizontal: 'left', vertical: 'top'}}
                             anchorEl={this.state.anchorEl}
                             open={this.state.resellerOpen}
                    >
                        <Menu>
                        {
                            !!! this.props.resellerCatalogs || this.props.resellerCatalogs && this.props.resellerCatalogs.length == 0 ? (
                                <MenuItem primaryText={"Reseller Catalog"} 
                                          onClick={()=>{this.switchCatalog( "reseller", "", "Reseller Catalog" )}}
                                          style={styles.selected}
                                          key={0}
                                />
                            ) :
                            this.props.resellerCatalogs.filter((catalog)=> { return catalog.typeOf == 'TYPE_RESELLER' }).map((catalog, index) => {
                                return (
                                    <MenuItem primaryText={catalog.name}
                                              onClick={()=>{this.switchCatalog( "reseller", catalog.id, catalog.name )}}
                                              style={resellerCatalogId == catalog.id ? styles.selected : {}} 
                                              key={index}
                                    />
                                )
                            })
                        }
                        </Menu>
                    </Popover>
                </span>
            ) : ""
        }
        {
            !!passIssuerId && passIssuerId != -1 && passIssuerCatalogs ? (
                <span style={styles.orgContainer}>
                    <RaisedButton onTouchTap={ this.createHandleTouchTap( 'pass-issuer' ) }
                                  label={ this.label( "Default Catalog", "pass-issuer" ) }
                    />
                    <Popover onRequestClose={ this.createHandleRequestClose( 'pass-issuer' ) }
                             anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                             targetOrigin={{horizontal: 'left', vertical: 'top'}}
                             anchorEl={this.state.anchorEl}
                             open={this.state.passIssuerOpen}
                    >
                        <Menu>
                        {
                            !!! this.props.passIssuerCatalogs || this.props.passIssuerCatalogs && this.props.passIssuerCatalogs.length == 0 ? (
                                <MenuItem primaryText={"Pass-Issuer Catalog"} 
                                          onClick={()=>{this.switchCatalog( "pass-issuer", "", "Pass Issuer Catalog" )}}
                                          style={styles.selected}
                                          key={0}
                                />
                            ) :
                            this.props.passIssuerCatalogs.filter((catalog)=> { return catalog.typeOf == 'TYPE_PASS_ISSUER' }).map((catalog, index) => {
                                return (
                                    <MenuItem primaryText={catalog.name}
                                              onClick={()=>{this.switchCatalog( "pass-issuer", catalog.id, catalog.name )}}
                                              style={resellerCatalogId == catalog.id ? styles.selected : {}} 
                                              key={index}
                                    />
                                )   
                            })
                        }
                        </Menu>
                    </Popover>
                </span>
            ) : ""
        }
      </div>
    )
  }
}

import { connect } from 'react-redux'
import { 
    setSupplierCatalogId,
    setResellerCatalogId,
    setPassIssuerCatalogId
} from '../ax-redux/actions/report-filters'
import {
    listSupplierCatalogs
} from '../ax-redux/actions/suppliers'
import {
    listResellerCatalogs
} from '../ax-redux/actions/resellers'
import { 
    listPassIssuerCatalogs
} from '../ax-redux/actions/pass-issuers'

const mapStateToProps = (state) => {

  const filters = state.reportFilters,
        suppliers = state.suppliers,
        resellers = state.resellers,
        passIssuers = state.passIssuers

  return {
      role: state.app.initialRole,
      passIssuerCatalogId: filters.passIssuerCatalogId,
      resellerCatalogId: filters.resellerCatalogId,
      supplierCatalogId: filters.supplierCatalogId,
      supplierCatalogs: !!suppliers.listCatalogs.data ? suppliers.listCatalogs.data.catalogs : false,
      resellerCatalogs: !!resellers.listCatalogs.data ? resellers.listCatalogs.data.catalogs : false,
      passIssuerCatalogs: !!passIssuers.listCatalogs.data ? passIssuers.listCatalogs.data : false,
      supplierCatalogsFetching: suppliers.listCatalogs.fetching,
      resellerCatalogsFetching: resellers.listCatalogs.fetching,
      passIssuerCatalogsFetching: passIssuers.listCatalogs.fetching
    }
}
const mapDispatchToProps = dispatch => {
  return {
    setSupplierCatalogId: v => {
        dispatch(setSupplierCatalogId(v))
    },
    setResellerCatalogId: v => {
        dispatch(setResellerCatalogId(v))
    },
    setPassIssuerCatalogId: v => {
        dispatch(setPassIssuerCatalogId(v))
    },
    listSupplierCatalogs: supplierId => {
        dispatch( listSupplierCatalogs(supplierId) )
    },
    listResellerCatalogs: resellerId => {
        dispatch( listResellerCatalogs(resellerId) )
    },
    listPassIssuerCatalogs: passIssuerId => {
        dispatch( listPassIssuerCatalogs( passIssuerId ) )
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CatalogSwitcher)
