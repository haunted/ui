import React, { Component, PropTypes } from 'react'
import Drawer from 'material-ui/Drawer'
import {List, ListItem} from 'material-ui/List'
import Subheader from 'material-ui/Subheader'
import { browserHistory } from 'react-router'
import moment from 'moment-timezone'
import { featureEnabled } from '../util'


class SideMenu extends Component {
  constructor(props) {
    super(props)
  }

  navigate ( url ) {

    // // TODO: figure out if this code is useless
    // let filters = this.props.filters,
    //     supplierCode = filters.supplierCode,
    //     resellerCode = filters.resellerCode,
    //     supplierCodes = !!filters.supplierCodes ? `/${filters.supplierCodes}` : '',
    //     supplier = supplierCode != '' ? `/${supplierCode}` : '',
    //     reseller = resellerCode != '' ? `/${resellerCode}` : '',
    //     start = moment(filters.startDate).tz(TZ).unix(),
    //     end = moment(filters.endDate).tz(TZ).unix(),
    //     queryParams = `?start=${start}&end=${end}`,
    //     params = ''

    // if ( url.indexOf("/redeemed") > -1 || url.indexOf("/exceptions") > -1 || 
    //      url.indexOf("/arrivals") > -1 || url.indexOf("/shifts") > -1 || url.indexOf("/rejections") > -1 )
    //   reseller = ''

    // if ( url.indexOf( "dollar-volume" ) > -1 || url.indexOf("ticket-volume") > -1 || url.indexOf("reseller-volume") > -1 ) {

    //   queryParams = ''
    //   reseller = ''

    // }

    // if ( url.indexOf( "reseller-invoice" ) > -1 ) {

    //   params = `${reseller}${supplier}${queryParams}`

    // } else if ( url.indexOf( "pass-redemptions" ) > -1 ) {

    //   params = `${supplierCodes}${queryParams}` // change this when filtering by reseller is in spec*

    // } else {

    //   params = `${supplier}${reseller}${queryParams}`

    // }

    // if ( url.indexOf( "reports" ) < 0 )
    //   params = ''

    browserHistory.push( url )

  }

  render() {

    let menuItems = this.props.app.sideMenuItems,
        menuClass =("new-side-menu  no-print "+(menuItems.length > 0 ? "" : "closed"));

    return (
      <Drawer className={menuClass} open={this.props.app.sideMenuOpen}
              style={{display: (this.props.printMode ? "none" : "inherit")}}>
        <List className="inner-side-menu">
          <Subheader>{this.props.app.sideMenuTitle}</Subheader>
          {
            menuItems
            .filter( v => !!!v.feature || featureEnabled(v.feature) )
            .map( ( v, k ) => (
              <ListItem
                key={k}
                className={v.url == this.props.page ? "selected" : ""}
                onTouchTap={() => this.navigate(v.url)}>
                {v.title}
              </ListItem>
            ))
          }
        </List>
      </Drawer>
    )

  }

}

import { connect } from 'react-redux'

export default connect(
  state => {
    return {
      page: state.routing.locationBeforeTransitions.pathname,
      app: state.app,
      printMode: state.app.printMode,
      filters: state.reportFilters
    }
  },
  dispatch => {
    return {}
  }
)(SideMenu)
