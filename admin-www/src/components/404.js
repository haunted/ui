import React, { Component, PropTypes } from 'react'
import FlatButton from 'material-ui/FlatButton';

import { browserHistory } from 'react-router'

const styles = {
  container: {
    display: 'flex',
    height: '100vh',
    justifyContent: 'center',
  },
  inner: {
    alignSelf: 'center',
  },
  errorStyle: {

  }
}

class PageNotFound extends Component {
  constructor(props) {
    super(props)
  }
  goHome() {
    browserHistory.push("/app/dashboard")
  }
  render() {
    return (
      <div style={styles.container}>
        <div style={styles.inner}>
          <h3>Oops! This page you are looking for is not here!</h3>
          <p>It may have been moved, or perhaps there is a typo in your address bar!</p>
          <FlatButton
            label="Back to Redeam App"
            onClick={e => this.goHome()} />
        </div>
      </div>
    )
  }
}


export default PageNotFound
