import React, { Component, PropTypes } from 'react'
import CircularProgress from 'material-ui/CircularProgress';
import FlatButton from 'material-ui/FlatButton';


const styles = {
  container: {
    display: 'flex',
    height: '100vh',
    justifyContent: 'center',
  },
  inner: {
    alignSelf: 'center',
  },
  errorStyle: {

  }
}

class ErrorScreen extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div style={styles.container}>
        <div style={styles.inner}>
          <p>{this.props.error != null ? "Error: "+this.props.error.toString() : "An Error Occurred"}</p>
          <FlatButton label="Go Back" onClick={e => this.props.goBack()} />
        </div>
      </div>
    )
  }
}

ErrorScreen.propTypes = {
  error: PropTypes.string.isRequired,
  goBack: PropTypes.func.isRequired,
}

export default ErrorScreen
