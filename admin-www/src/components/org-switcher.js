import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AutoComplete from 'material-ui/AutoComplete';
import {
  isResellerStaff,
  isSupplierStaff,
  isPassIssuerStaff,
} from '../util';
import * as appActions from '../ax-redux/actions/app';
import * as suppliersActions from '../ax-redux/actions/suppliers';
import * as resellersActions from '../ax-redux/actions/resellers';
import * as passIssuersActions from '../ax-redux/actions/pass-issuers';

const styles = {
  orgSwitcher: {
    right: '230px',
    height: '37px',
  },
  label: {
    padding: '0.25em',
    fontsize: '1em',
    color: 'white',
    paddingTop: '0.5em',
    marginTop: '0.5em',
  },
};

const dataSourceConfig = {
  text: 'text',
  value: 'value',
};

const placeholders = {
  supplier: 'Supplier Code',
  reseller: 'Reseller Code',
  passIssuer: 'Pass Issuer Code',
};

const getMode = (role) => {
  if (isSupplierStaff(role)) {
    return 'supplier';
  } else if (isResellerStaff(role)) {
    return 'reseller';
  } else if (isPassIssuerStaff(role)) {
    return 'passIssuer';
  }

  // not sure whether we need this default
  return 'supplier';
};

class OrgSwitcher extends Component {
  constructor(props) {
    super(props);

    const mode = getMode(props.role);

    this.onNewRequest = this.onNewRequest.bind(this);

    this.state = {
      open: false,
      label: '',
      mode,
      supplierCode: '',
      resellerCode: '',
      passIssuerCode: '',
      byName: [],
    };
  }

  componentWillMount() {
    const { role } = this.props;
    const mode = getMode(role);

    this.props.listResellers();
    this.props.listSuppliers();
    this.props.listPassIssuers();

    if (this.props[`${mode}s`]) {
      this.generateAutocomplete(mode, this.props[`${mode}s`]);
    }
  }

  componentWillReceiveProps(nextProps) {
    const roleChanged = nextProps.role !== this.props.role;

    let mode = this.state.mode;

    if (roleChanged) {
      mode = getMode(nextProps.role);
    }

    this.setState({ mode });

    if ((roleChanged || !this.props[`${mode}s`]) && nextProps[`${mode}s`]) {
      this.generateAutocomplete(mode, nextProps[`${mode}s`]);
    }
  }

  onNewRequest(chosenRequest) {
    const { mode } = this.state;

    let code = '';

    if (typeof chosenRequest === 'string') {
      code = chosenRequest;
    } else {
      code = chosenRequest.value;

      this.props.onOrgChange({
        code: chosenRequest.value,
        name: chosenRequest.text,
      });
    }

    this.setState({
      [`${mode}Code`]: code,
    });

    this.props.spoofUserOrg(code);
  }

  generateAutocomplete(type, records) {
    const byName = [];
    const uniqueCodes = [];

    this.setState({
      mode: type,
    });

    records.forEach((s) => {
      const record = s[type] ? s[type] : s;

      let code = '';

      // if there's a space, it's not a code
      if (record.code.includes(' ')) {
        return;
      }

      if (!uniqueCodes[record.value]) {
        uniqueCodes[record.value] = record.code;
      } else {
        code = ` (${record.code})`;
      }

      byName.push({
        text: record.name + code,
        value: record.code,
      });
    });

    byName.sort((a, b) => {
      const last = a.text.toLowerCase();
      const next = b.text.toLowerCase();

      if (last < next) {
        return -1;
      } else if (last > next) {
        return 1;
      }

      return 0;
    });

    this.setState({
      byName,
    });
  }

  label(defaultValue) {
    return this.state.label || defaultValue;
  }

  handleRequestClose() {
    this.setState({
      open: false,
    });
  }

  selectOrg(org) {
    this.setState({
      label: org,
    });

    this.props.spoofUserOrg(org);
    this.handleRequestClose();
  }

  render() {
    const { mode } = this.state;
    const sortedSource = this.state.byName;

    return (
      <div style={styles.orgSwitcher} className="role-switcher">
        {this.props.fetching ? (
          <span style={styles.label}>LOADING ORGS</span>
        ) : (
          <AutoComplete
            hintText={placeholders[mode]}
            className="OrgSwitcher-AutoComplete"
            style={{ width: '360px' }}
            listStyle={{ width: '360px' }}
            menuProps={{ maxHeight: 600 }}
            searchText={this.state[`${mode}Code`]}
            filter={AutoComplete.fuzzyFilter}
            key="supplier-name-field"
            id="supplier-name-field"
            dataSource={sortedSource}
            dataSourceConfig={dataSourceConfig}
            onNewRequest={this.onNewRequest}
            openOnFocus
            fullWidth
          />
        )}
      </div>
    );
  }
}

OrgSwitcher.propTypes = {
  role: PropTypes.string.isRequired,
  fetching: PropTypes.bool.isRequired,
  spoofUserOrg: PropTypes.func.isRequired,
  listSuppliers: PropTypes.func.isRequired,
  listResellers: PropTypes.func.isRequired,
  listPassIssuers: PropTypes.func.isRequired,
  onOrgChange: PropTypes.func,
};

OrgSwitcher.defaultProps = {
  onOrgChange: () => {},
};

const mapStateToProps = (state) => {
  const {
    app,
    suppliers,
    resellers,
    passIssuers,
  } = state;

  return {
    role: app.role,
    orgCode: app.user ? app.user.account.orgCode : '',
    suppliers: suppliers.list.data ? suppliers.list.data.suppliers : false,
    resellers: resellers.list.data ? resellers.list.data.resellers : false,
    passIssuers: passIssuers.list.data.length ? passIssuers.list.data : false,
    fetching: suppliers.list.fetching || resellers.list.fetching || passIssuers.list.fetching,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  ...appActions,
  ...suppliersActions,
  ...resellersActions,
  ...passIssuersActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(OrgSwitcher);
