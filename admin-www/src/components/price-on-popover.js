import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import {
  decodeUnit
} from '../util'

export default class PriceOnPopover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      label: '',
      options: [
        {name: "Sale", value: 'PRICE_ON_SALE'},
        {name: "Redemption", value: 'PRICE_ON_REDEMPTION'}
      ]
    }
  }
  componentDidMount () {
    let label = 'Select Option'
    console.log("LABEL ****** ", this.props.value)
    if (typeof this.props.value == 'string') {
      if (this.props.value.indexOf('PRICE_ON') > -1) {
        label = decodeUnit(this.props.value)
      } 
    } 
    this.setState({
      label
    })
  }
  handleTouchTap (event) {
    event.preventDefault();
    this.setState({
        open: true,
        anchorEl: event.currentTarget
    })
  }
  handleRequestClose (evt) {
    this.setState({
       open: false
    })
  }
  onMenuItem(option) {
    this.props.onChange(option.value)
    this.setState({
      open: false,
      label: option.name
    })
  }
  render() {
    return (
      <div className="roles-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={e => this.handleRequestClose(e)}
        >
          <Menu>
            {
                this.state.options.map((option, i) => {
                    return (
                        <MenuItem onClick={()=>{this.onMenuItem(option)}}
                                  primaryText={option.name}
                                  key={i}
                         />
                    )
                })
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}

PriceOnPopover.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired
}
