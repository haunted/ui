import React, { Component, PropTypes } from 'react'
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import RejectionReasonDropdown from './rejection-reason-dropdown';

class VoidButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      activated: false,
      comments: ""
    }
  }

  toggleForm () {
    this.setState({
        activated: !this.state.activated
    })
  }

  submit () {
    if (!!this.props.voucherId) {
        // if we have a voucher id, then we will call void voucher. 
        // The void voucher API endpoint will also clear the associated redemption
        // .... perhaps later we will only use the void redemption call, which
        //   could figure out which voucher was associated and void it as well...
        this.props.voidVoucher(this.props.voucherId)        
    } else if (!!this.props.redemptionId) {
        // if we don't have a voucher id, then we should try and void the redemption id
        this.props.voidRedemption(this.props.redemptionId)
    } else {
        console.error("nothing to void")
    }

    this.toggleForm();
    !! this.props.callback && this.props.callback();
  }

  cancel () {
      this.toggleForm()
  }

  onCommentsChange (e) {
      this.setState({
          comments: e.target.value
      })
  }

  render() {
      if (this.state.activated) {
          return (
              <div className="ticket-lightbox delete-voucher-button">
                  <div className="inner-lightbox inner">
                  <p>
                    WARNING: You are about to delete this voucher from your reports.
                    It will not be included in any redemption, shift or invoice reports and
                    will therefore not count towards any of your daily totals. Are you sure?
                  </p>
                  <RaisedButton
                        onTouchTap={e => { this.submit() } }
                        className="no-print form-button delete-button"
                        label="Yes"
                   />
                   Delete Voucher
                   <RaisedButton
                         onTouchTap={e => { this.cancel() } }
                         className="no-print form-button"
                         label="No"
                    />
                    Keep Voucher
                </div>
            </div>
          )
      } else {
          return (
              <RaisedButton
                    onTouchTap={e => { this.toggleForm() } }
                    className={"no-print " + (this.props.className || "")}
                    secondary={this.props.secondary}
                    style={(this.props.style  || {marginLeft: "1em"})}
                    label={this.props.label || "Delete Voucher"}
               />
          )
      }
  }
}

import { connect } from 'react-redux'

import {
    voidVoucher,    
} from '../ax-redux/actions/vouchers'

import {
    voidRedemption,    
} from '../ax-redux/actions/redemptions'

export default connect(
  (state, ownProps) => {
    return {

    }
  },
  dispatch => {
    return {
        voidVoucher: (id) => {
            dispatch(voidVoucher(id))
        },
        voidRedemption: (id) => {
            dispatch(voidRedemption(id))
        },
    }
  }
)(VoidButton)
