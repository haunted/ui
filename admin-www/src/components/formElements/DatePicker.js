import React, { PropTypes } from 'react';
import DatePickerMUI from 'material-ui/DatePicker';
import moment from 'moment-timezone';

// workaround to make DatePicker support timezones
// and display the correct date
const adjustTimezone = (date) => {
  const utcDate = moment(date);
  const localDate = utcDate.clone();

  localDate.tz(TZ);
  localDate.add(utcDate.utcOffset() - localDate.utcOffset(), 'minutes');

  return localDate.toISOString();
};

function DatePicker(props) {
  const {
    input: {
      value,
      onChange,
      ...input,
    },
    meta: {
      touched,
      error,
    },
    ...custom
  } = props;

  return (
    <DatePickerMUI
      value={value ? new Date(value) : null}
      onChange={(e, date) => onChange(adjustTimezone(date))}
      formatDate={date => moment(date).format('DD-MMM-YYYY')}
      errorText={touched && error}
      autoOk
      {...input}
      {...custom}
    />
  );
}

DatePicker.propTypes = {
  input: PropTypes.shape({}).isRequired,
  meta: PropTypes.shape({}).isRequired,
};

export default DatePicker;
