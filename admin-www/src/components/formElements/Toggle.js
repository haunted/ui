import React, { PropTypes } from 'react';
import ToggleMUI from 'material-ui/Toggle';

function Toggle(props) {
  const {
    label,
    input: {
      onChange,
      value,
      ...input
    },
    meta,
    ...custom
  } = props;

  return (
    <ToggleMUI
      label={label}
      toggled={value || false}
      onToggle={(e, checked) => onChange(checked)}
      {...input}
      {...custom}
    />
  );
}

Toggle.propTypes = {
  input: PropTypes.shape({}).isRequired,
  label: PropTypes.string,
  meta: PropTypes.shape({}).isRequired,
};

Toggle.defaultProps = {
  label: '',
};

export default Toggle;
