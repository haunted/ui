import React, { PropTypes } from 'react';
import AutoCompleteMUI from 'material-ui/AutoComplete';


function AutoComplete(props) {
  const {
    input: {
      name,
      value,
      onChange,
      onBlur,
    },
    meta: {
      touched,
      error,
    },
    ...custom
  } = props;

  return (
    <AutoCompleteMUI
      name={name}
      errorText={touched && error}
      searchText={value}
      onUpdateInput={onChange}
      onBlur={onBlur}
      filter={AutoCompleteMUI.fuzzyFilter}
      menuProps={{ maxHeight: 200 }}
      openOnFocus
      {...custom}
    />
  );
}

AutoComplete.propTypes = {
  input: PropTypes.shape({}).isRequired,
  meta: PropTypes.shape({}).isRequired,
  dataSource: PropTypes.arrayOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ])),
};

AutoComplete.defaultProps = {
  dataSource: [],
};

export default AutoComplete;
