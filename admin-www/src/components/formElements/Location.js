import React, { Component, PropTypes } from 'react';
import { Field } from 'redux-form';
import AutoCompleteMUI from 'material-ui/AutoComplete';
import { TextField, AutoComplete } from '../formElements';
import countries from '../../resources/countries.json';
import states from '../../resources/states.json';


class Location extends Component {
  constructor(props) {
    super(props);

    this.handleCountryUpdate = this.handleCountryUpdate.bind(this);
    this.isUSA = this.isUSA.bind(this);
  }

  handleCountryUpdate(e, value) {
    const {
      input: { name },
      change,
    } = this.props;

    if (this.isUSA() && !this.isUSA(value)) {
      change(`${name}.state`, '');
    }
  }

  isUSA(nextCountry) {
    const {
      input: { value },
    } = this.props;

    const country = nextCountry || nextCountry === '' ? nextCountry : value.country;

    return country.toLowerCase().includes('usa');
  }

  render() {
    const {
      input: { name },
      meta,
      change,
      ...custom,
    } = this.props;

    return (
      <div>
        <Field
          name={`${name}.country`}
          component={AutoComplete}
          floatingLabelText="Country"
          floatingLabelFixed
          dataSource={countries}
          filter={AutoCompleteMUI.caseInsensitiveFilter}
          onChange={this.handleCountryUpdate}
          {...custom}
        />

        <Field
          name={`${name}.state`}
          component={AutoComplete}
          floatingLabelText="State"
          floatingLabelFixed
          dataSource={this.isUSA() ? states : []}
          filter={AutoCompleteMUI.caseInsensitiveFilter}
          {...custom}
        />

        <Field
          floatingLabelText="City"
          floatingLabelFixed
          name={`${name}.city`}
          component={TextField}
          {...custom}
        />
      </div>
    );
  }
}

Location.propTypes = {
  change: PropTypes.func.isRequired,
  input: PropTypes.object.isRequired, // eslint-disable-line
};

Location.defaultProps = {
  value: {
    country: '',
    state: '',
    city: '',
  },
};

export default Location;
