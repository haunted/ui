import React, { PropTypes } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

function DropDown(props) {
  const {
    label,
    options,
    input: {
      onChange,
      ...input
    },
    meta: {
      touched,
      error,
    },
    ...custom
  } = props;

  return (
    <SelectField
      hintText={label}
      onChange={(e, key, value) => onChange(value)}
      errorText={touched && error}
      {...input}
      {...custom}
    >
      {options.map(option => (
        <MenuItem
          key={option.value}
          value={option.value}
          primaryText={option.label}
        />
      ))}
    </SelectField>
  );
}

DropDown.propTypes = {
  input: PropTypes.shape({}).isRequired,
  meta: PropTypes.shape({}).isRequired,
  label: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.isRequired,
  })).isRequired,
};

DropDown.defaultProps = {
  label: '',
};

export default DropDown;
