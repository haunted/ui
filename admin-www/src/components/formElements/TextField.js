import React, { PropTypes } from 'react';
import TextFieldMUI from 'material-ui/TextField';


function TextField(props) {
  const {
    input,
    label,
    meta: {
      touched,
      error,
    },
    ...custom
  } = props;

  return (
    <TextFieldMUI
      hintText={label}
      errorText={touched && error}
      {...input}
      {...custom}
    />
  );
}

TextField.propTypes = {
  input: PropTypes.shape({}).isRequired,
  label: PropTypes.string,
  meta: PropTypes.shape({}).isRequired,
};

TextField.defaultProps = {
  label: '',
};

export default TextField;
