import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

class AccountsPopover extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      label: !!this.props.value ? this.props.value : "Select User Account"
    };
  }

  componentWillMount() {
      this.props.listAccounts({
          orgCode: this.props.orgCode
      })
  }

  handleTouchTap (event) {
    // This prevents ghost click.
    event.preventDefault();
    this.setState({
        open: true,
        anchorEl: event.currentTarget
    });

  }

  handleRequestClose (evt) {
    this.setState({
       open: false
    });
  }

  onAccount(item, label) {
    this.props.onAccount(item)
    this.setState({
      open: false,
      label: label
    })
  }

  render() {

    return (
      <div className="accounts-popover">
        <RaisedButton
          onTouchTap={(event) => { this.handleTouchTap(event); }}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={(event) => { this.handleRequestClose(event); }}
        >
          <Menu>
            <MenuItem key="none" onClick={()=> { this.onAccount("", "No User") } } primaryText="No User" />
            {
                !!this.props.accounts && this.props.accounts.map(account => {
                        return (
                            <MenuItem key={account.id} onClick={()=>{this.onAccount(account.id, account.username)}} primaryText={account.username} />
                        )
                })
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}

AccountsPopover.propTypes = {
  onAccount: PropTypes.func.isRequired,
  orgCode: PropTypes.string
}

import { connect } from 'react-redux'

import { listAccounts } from '../ax-redux/actions/accounts'
export default connect(
  state => {
    return {
      accounts: state.accounts.list.data,
      fetching: state.accounts.list.fetching,
    }
  },
  dispatch => {
    return {
      listAccounts: (params) => {
          dispatch(listAccounts(params))
      },
    }
  }
)(AccountsPopover)
