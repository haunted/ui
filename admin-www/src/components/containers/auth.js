import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'

import LoadingScreen from '../loading-screen'
import ErrorScreen from '../error-screen'


class Auth extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    let queryParams = this.props.queryParams;

    this.props.onAuthorizationComplete(() => {
      console.log("on authorization complete, starting up")
      requestAnimationFrame(() => this.props.startup())
    })

    this.props.onAuthorizationFailure(err => {

      console.log("on authorization failure")
      requestAnimationFrame(() => {
        if (this.protectedPage()) {
          this.props.setRedirectAfterLogin(this.props.page+queryParams)
        }
        browserHistory.push("/")
        this.props.logout()
      })
    })

    this.props.onStartupComplete(() => {
      console.log("on startup complete")
      requestAnimationFrame(() => this.redirect())
    })

    if (this.protectedPage()) {
      this.props.setRedirectAfterLogin(this.props.page+queryParams)
    } else {
      this.props.setRedirectAfterLogin("app/dashboard")
    }
    browserHistory.push("/")
    this.props.loadAuthorization()
  }

  protectedPage() {
    return this.props.page != "/" && this.props.page != "login"
  }

  isStartingUp() {
    return this.props.startupFetching
  }

  redirect() {
    browserHistory.push(this.props.app.redirectAfterLogin)
    this.props.setRedirectAfterLogin("app/dashboard")
  }

  componentWillReceiveProps(nextProps) {
    //console.dir(nextProps)
  }
  componentDidUpdate(prevProps, prevState) {
  }

  render() {
    if (this.isStartingUp()) {
      return <LoadingScreen />
    }
    if (this.props.auth.loggedIn) { // prevent auth failed / Error is not defined
        if (this.props.startupError) {
          return <ErrorScreen error={this.props.startupError} goBack={this.props.startupFailBack} />
        }
    }

    return (
      <div>
        {
          this.props.children
        }
      </div>
    )
  }
}


import {
  loadAuthorization,
  logout,
  onAuthorizationComplete,
  onAuthorizationFailure,
} from '../../ax-redux/actions/auth'
import {
  startup,
  startupFailBack,
  setRedirectAfterLogin,
  onStartupComplete
} from '../../ax-redux/actions/app'
import { connect } from 'react-redux'

export default connect(
  state => ({
    app: state.app,
    auth: state.auth,
    page: state.routing.locationBeforeTransitions.pathname,
    queryParams: state.routing.locationBeforeTransitions.search,
    startupFetching: state.app.startupFetching,
    startupError: state.app.startupError,
  }),
  dispatch => ({
    startup: () => {
      dispatch(startup())
    },
    startupFailBack: () => {
      dispatch(startupFailBack())
    },
    loadAuthorization: () => {
      dispatch(loadAuthorization())
    },
    logout: () => {
      dispatch(logout())
    },
    setRedirectAfterLogin: (path) => {
      console.log("setting redirect after login:", path)
      dispatch(setRedirectAfterLogin(path))
    },
    onAuthorizationComplete: fn => dispatch(onAuthorizationComplete(fn)),
    onAuthorizationFailure: fn => dispatch(onAuthorizationFailure(fn)),
    onStartupComplete: fn => dispatch(onStartupComplete(fn)),
  })
)(Auth);
