import React, { Component, PropTypes } from 'react'

import {
  cyan700,
  grey100, grey400, grey500,
  pinkA200,
  white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';

import {fade} from 'material-ui/utils/colorManipulator';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


const muiTheme = getMuiTheme({
  palette: {
    primary1Color: "#27367a",
    primary2Color: cyan700,
    primary3Color: grey400,
    accent1Color: "#d50000",
    accent2Color: grey100,
    accent3Color: grey500,
    textColor: darkBlack,
    alternateTextColor: white,
    canvasColor: white,
    borderColor: grey100,
    disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: "#27367a",
    clockCircleColor: fade(darkBlack, 0.07),
    shadowColor: fullBlack,
  },
  appBar: {
    height: 48,
  }
});

class Theme extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        {this.props.children}
      </MuiThemeProvider>
    )
  }
}

export default Theme
