import React, {Component} from 'react'
import is from 'is_js'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { loginFetch } from '../ax-redux/actions/auth'
import TextField from 'material-ui/TextField'
import Card, {CardTitle, CardText, CardActions} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import LocalizedStrings from 'react-localization';
import LocaleSwitcher from './locale-switcher'
import { featureEnabled } from '../util'

let strings = new LocalizedStrings({
 en:{
   login:"Login",
   organization: "Organization",
   username: "Username",
   password: "Password",
   back: "Back",
   login: "Login",
   usernameError: "Invalid Username",
   requiredError: "This field is required",
   loginError: "Invalid Username or Password",
   networkError: "Network Error"
 },
 it: {
   login:"[!!! £ôϱïñ ℓ !!!]",
   organization: "[!!! Óřϱáñïƺáƭïôñ ℓôř !!!]",
   username: "[!!! Ûƨèřñá₥è ℓ !!!]",
   password: "[!!! Þáƨƨωôřδ ℓ !!!]",
   back: "[!!! ßáçƙ  !!!]",
   login: "[!!! £ôϱïñ ℓ !!!]",
   usernameError: "[!!! ÌñƲáℓïδ Ûƨèřñá₥è ℓôřè !!!]",
   requiredError: "[!!! Tλïƨ ƒïèℓδ ïƨ řè9úïřèδ ℓôřè₥  !!!]",
   loginError: "[!!! ÌñƲáℓïδ Ûƨèřñá₥è ôř Þáƨƨωôřδ ℓôřè₥ ï !!!]",
   networkError: "[!!! Nèƭωôřƙ Éřřôř ℓôř !!!]"
 }
});

const validatePasswordLength = false
const loginCardStyle = {
  margin: "auto",
  maxWidth: 300,
}
const loginPaperStyle = {
  padding: "1em",
}
const styles = {
  error: {
    color: "rgba(180, 90, 90, 0.8)",
  },
  container: {
    textAlign: 'center',
    paddingTop: 200,
  },
  button: {
    margin: 12
  }
};

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loginUsername: "",
      loginPassword: "",
      loginOrg: "",
      requestError: false,
      loginErrorUsernameInvalid: false,
      loginErrorUsernameRequired: false,
      loginErrorOrgRequired: false,
      loginErrorPasswordShort: false,
      startupSequence: false,
    }
  }
  inputChanged(update) {
    if (update.loginOrg) {
      this.setState({
        loginErrorOrgRequired: false,
      })
    }
    if (update.loginUsername) {
      this.setState({
        loginErrorUsernameRequired: false,
        loginErrorUsernameInvalid: false,
      })
    }
    if (update.loginPassword) {
      this.setState({
        loginErrorPasswordShort: false,
      })
    }

    this.setState({requestError: false})
    this.setState(update)
  }
  componentWillMount() {
    strings.setLanguage(this.props.locale)
    if (this.props.loggedIn) {
      browserHistory.push("/")
    }
  }
  componentWillUpdate(nextProps, nextState) {
    let err = nextProps.auth.loginError
    if (this.props.auth.loginError === false) {
      if (err) {
        if (err.status === 401) {
          this.setState({requestError: strings.loginError})
        } else if (err.message == "Network Error") {
          this.setState({requestError: strings.networkError})
        }
      }
    }
  }
  onLogin() {
      var errors = false
      if (validatePasswordLength && this.state.loginPassword.length < 7) {
          this.setState({ loginErrorPasswordShort: true })
          errors = true
      }
      if (this.state.loginUsername == "") {
          this.setState({ loginErrorUsernameRequired: true })
          errors = true
      }
      if (errors) {
        return
      }
      this.props.dispatchLogin(this.state.loginOrg, this.state.loginUsername, this.state.loginPassword)
  }
  handleKeyPress (evt) {
      let key = evt.keyCode || evt.which;
      if (key == 13) {
          this.onLogin();
      }
  }
  render() {
    return (
      <div style={styles.container}>
        <div className="landing-logo"></div>
        {/*<Paper style={loginPaperStyle} zDepth={2}>*/}
          <Card style={loginCardStyle} >
            <CardTitle>{strings.login}</CardTitle>
            <form name="redeam-admin-login" autoComplete="off" noValidate>
              <TextField
                className="login-input"
                id="redeam-admin-login-org"
                name="redeam-admin-login-org"
                autoComplete="off"
                type="text"
                floatingLabelText={strings.organization}
                value={this.state.loginOrg}
                onKeyPress={(evt)=>{ this.handleKeyPress(evt); }}
                onChange={e => this.inputChanged({loginOrg: e.target.value})}
                errorText={ this.state.loginErrorOrgRequired ? strings.requiredError : "" } />

              <TextField
                className="login-input"
                id="redeam-admin-login-email"
                name="redeam-admin-login-email"
                autoComplete="off"
                type="text"
                floatingLabelText={strings.username}
                value={this.state.loginUsername}
                onKeyPress={(evt)=>{ this.handleKeyPress(evt); }}
                onChange={e => this.inputChanged({loginUsername: e.target.value})}
                errorText={
                  this.state.loginErrorUsernameRequired ? strings.requiredError :
                  this.state.loginErrorUsernameInvalid ? strings.usernameError : ""}
              />
              <TextField
                className="login-input"
                id="redeam-admin-login-password"
                name="redeam-admin-login-password"
                autoComplete="off"
                type="password"
                floatingLabelText={strings.password}
                value={this.state.loginPassword}
                onKeyPress={(evt)=>{ this.handleKeyPress(evt); }}
                onChange={e => this.inputChanged({loginPassword: e.target.value})}
                errorText={this.props.loginError != false ? " " : ""}
              />
              {
                this.state.requestError !== false ?
                  <CardText style={styles.error}>{this.state.requestError}</CardText>
                  : ""
              }
              <CardActions>
                <RaisedButton label={strings.back} onTouchTap={e => browserHistory.push("/")} />
                <RaisedButton className="login-button" label={strings.login} primary={true} onTouchTap={e => this.onLogin()} />
              </CardActions>
            </form>
          </Card>
          { featureEnabled("locale-switcher") ? (
            <LocaleSwitcher onsubmit={v => strings.setLanguage(v) } />
          ) : ""}
        {/*</Paper>*/}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loggedIn: state.auth.loggedIn,
    auth: state.auth,
    loginError: state.auth.loginError,
    locale: state.app.locale
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    dispatchLogin: (org, username, password) => {
      dispatch(loginFetch(org, username, password))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
