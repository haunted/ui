import React, { Component, PropTypes } from 'react'
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import RejectionReasonDropdown from './rejection-reason-dropdown';

class DeleteResellerCatalogItemButton extends Component {

    constructor(props) {

        super(props);

        this.state = {
        activated: false,
        comments: ""
        }
    }

    toggleForm () {
        this.setState({
            activated: !this.state.activated
        })
    }

    submit () {

        console.warn("Submit ", this.props.resellerId, this.props.catalogId, this.props.id, this.props.version )

        if (!!this.props.resellerId && this.props.catalogId && this.props.id && this.props.version ) {
    
            this.props.disableResellerCatalogItem( this.props.resellerId, this.props.catalogId, this.props.id, this.props.version )
            !! this.props.callback && this.props.callback();
            this.toggleForm();

        }
    
    }

    cancel () {

      this.toggleForm()
  
    }

    onCommentsChange (e) {
  
        this.setState({
            comments: e.target.value
        })
  
    }

  render() {
      if (this.state.activated) {
          return (
              <div className="ticket-lightbox delete-voucher-button">
                  <div className="inner-lightbox inner">
                  <p>
                    WARNING: This product will no longer be connected. Confirm deletion?
                  </p>
                  <RaisedButton
                        onTouchTap={e => { this.submit() } }
                        className="no-print form-button delete-button"
                        label="Yes"
                   />
                   Delete Product
                   <RaisedButton
                         onTouchTap={e => { this.cancel() } }
                         className="no-print form-button"
                         label="No"
                    />
                    Keep Product
                </div>
            </div>
          )
      } else {
          return (
              <RaisedButton
                    onTouchTap={e => { this.toggleForm() } }
                    className={"no-print " + (this.props.className || "")}
                    secondary={this.props.secondary}
                    style={(this.props.style  || {marginLeft: "1em"})}
                    label={this.props.label || "Delete Product"}
               />
          )
      }
  }
}

import { connect } from 'react-redux'

import {
    disableResellerCatalogItem,    
} from '../ax-redux/actions/resellers'

export default connect(
  (state, ownProps) => {
    return {

    }
  },
  dispatch => {
    return {
        disableResellerCatalogItem: ( resellerId, catalogId, id, version ) => {
            dispatch( disableResellerCatalogItem( resellerId, catalogId, id, version ) )
        }
    }
  }
)(DeleteResellerCatalogItemButton)
