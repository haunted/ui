import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Snackbar from 'material-ui/Snackbar';
import { NotificationContainer } from 'react-notifications';
import * as notificationActions from '../ax-redux/actions/notifications';

import Theme from './containers/theme';
import Auth from './containers/auth';

function Root(props) {
  const {
    children,
    notifications,
    hideNotification,
  } = props;

  return (
    <Theme>
      <Auth>
        {children}

        <Snackbar
          message={notifications.message}
          onRequestClose={hideNotification}
          open={notifications.open}
          autoHideDuration={notifications.duration}
        />

        <NotificationContainer />
      </Auth>
    </Theme>
  );
}

Root.propTypes = {
  children: PropTypes.node.isRequired,
  notifications: PropTypes.shape({}).isRequired,
  hideNotification: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  notifications: state.notifications,
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(notificationActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Root);
