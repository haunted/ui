import React, {Component} from 'react'
import is from 'is_js'
import { browserHistory } from 'react-router'
import { registerFetch } from '../ax-redux/actions/auth'

import TextField from 'material-ui/TextField'
import Card, {CardTitle, CardText, CardActions} from 'material-ui/Card'
import RaisedButton from 'material-ui/FlatButton';
import Paper from 'material-ui/Paper';

const loginCardStyle = {
  margin: "auto",
  maxWidth: 300,
}
const loginPaperStyle = {
  padding: "1em",
}
const styles = {
  error: {
    color: "rgba(180, 90, 90, 0.8)",
  },
  container: {
    textAlign: 'center',
    paddingTop: 200,
  },
  button: {
    margin: 12
  }
};
class Register extends Component {
  constructor(props) {
    super(props)
    this.state = {
      registerEmail: "",
      registerPassword: "",
      registerPassword2: "",
      registerErrorPasswordMismatch: false,
      registerErrorPasswordShort: false,
      registerErrorEmailRequired: false,
      registerErrorEmailInvalid: false,
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.token) {
      browserHistory.push("/dashboard")
    }
  }
  inputChanged(update) {
    if (update.registerEmail) {
      this.setState({
        registerErrorEmailRequired: false,
        registerErrorEmailInvalid: false,
      })
    }
    if (update.registerPassword || update.registerPassword2) {
      this.setState({
        registerErrorPasswordMismatch: false,
        registerErrorPasswordShort: false,
      })
    }
    this.setState(update)
  }
  onRegister() {
    var errors = false
    if (this.state.registerPassword.length < 7) {
      this.setState({ registerErrorPasswordShort: true })
      errors = true
    }
    if (this.state.registerPassword != this.state.registerPassword2) {
      this.setState({ registerErrorPasswordMismatch: true })
      errors = true
    }
    if (this.state.registerEmail == "") {
      this.setState({ registerErrorEmailRequired: true })
      errors = true
    } else if (!is.email(this.state.registerEmail)) {
      this.setState({ registerErrorEmailInvalid: true })
      errors = true
    }
    if (errors) {
      return
    }
    this.props.dispatchRegister(this.state.registerEmail, this.state.registerPassword)
  }
  render() {
    return (
      <div style={styles.container}>
        <h1>Redeam</h1>
        <h2>Access your Dashboard</h2>
        <Card style={loginCardStyle}>
          <Paper style={loginPaperStyle} zDepth={2}>
            <CardTitle>Register</CardTitle>
            <TextField
              hintText="Email"
              floatingLabelText="Email"
              value={this.state.registerEmail}
              onChange={e => this.inputChanged({registerEmail: e.target.value})}
              errorText={
                this.state.registerErrorEmailRequired ? "This field is required" :
                this.state.registerErrorEmailInvalid ? "Invalid email address" : ""}
              />
            <TextField
              type="password"
              hintText="Password"
              errorText={
                this.state.registerErrorPasswordShort ? "Password less than 7 characters" : ""}
              floatingLabelText="Password"
              value={this.state.registerPassword}
              onChange={e => this.inputChanged({registerPassword: e.target.value})}
              />
            <TextField
              type="password"
              hintText="Confirm Password"
              errorText={
                this.state.registerErrorPasswordMismatch ? "The two passwords do not match" : ""}
              floatingLabelText="Confirm Password"
              value={this.state.registerPassword2}
              onChange={e => this.inputChanged({registerPassword2: e.target.value})}
              />
              {
                this.props.auth.registerError ?
                  <CardText style={styles.error}>{this.props.auth.registerError}</CardText>
                  : ""
              }
            <CardActions>
              <RaisedButton label="Back" onClick={e => browserHistory.push("/")}/>
              <RaisedButton primary={true} label="Register" onClick={e => this.onRegister()}/>
            </CardActions>
          </Paper>
        </Card>
      </div>
    )
  }
}
import { connect } from 'react-redux'
const mapStateToProps = (state) => {
  return {
    auth: state.auth
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    dispatchRegister: (email, password) => {
      dispatch(registerFetch(email, password))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register)
