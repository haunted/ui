import React from 'react';
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';
import AdminVersion from './admin-version';
import { dashboardToggleAdminMenu } from '../ax-redux/actions/dashboard';
import { isInternalStaff } from '../util'

class AdminMenu extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        docked: false
    }
  }

  componentWillMount () {
    if (!this.props.open && this.props.page.indexOf("dashboard") > -1) {
      this.props.handleToggle();
    }
  }

  componentWillUpdate(nextProps) {
      if (this.props.page != nextProps.page) {
          let docked = false;
          if (this.isSelected(nextProps.page, "/app/dashboard")) {
              docked = true;
          }
          this.setState({
              docked: docked
          })

      }
  }

  onTouchTapItem(e, url) {
    e.preventDefault()
    browserHistory.push(url)
    this.props.handleToggle()
  }

  isSelected (current, item) {
      return ("/"+current).search(item) > -1;
  }
  classSelected (current, item) {
      return this.isSelected(current, item) ? "selected" : "";
  }

  render() {

    let page = this.props.page,
        open = this.props.open,
        nav = [
            { route: "/app/dashboard", title: "Dashboard" }, 
            { route: "/app/manage-business", title: "Manage Business" },
            { route: "/app/reports", title: "Reports"}
        ]

    if (this.props.isAdmin) {

        nav = [ { route: "/app/redeam", title: "Redeam" } ].concat(nav)

    }

    return (

        <Drawer className={ "admin-menu  no-print " + (open ? " open" : "") + (this.isSelected(page, "/app/dashboard") ? " dashboard" : "") }
                docked={ this.state.docked }
                open={ open }
                width={320}
        >
            <div style={{display: (this.props.open && !this.state.docked) ? "" : "none"}}
                 onClick={e => this.props.handleToggle() }
                 className="close-lightbox"
            >
            </div>
            <MenuItem onClick={e => this.onTouchTapItem(e, '/app')}>
                <h1 style={{fontSize: "24px"}}>Redeam Admin</h1>
            </MenuItem>
            {nav.map((item, i)=>{
                return (
                    <MenuItem key={i} className={this.classSelected(page, item.route)}
                              onTouchTap={e => this.onTouchTapItem(e, item.route)} >
                        {item.title}
                    </MenuItem>
                );
            })}
            <MenuItem className="admin-version-menu-item">
                <AdminVersion admin={typeof window.GATEWAY_GIT_VERSION !== 'undefined' ? window.GATEWAY_GIT_VERSION : "-" }  />
            </MenuItem>
        </Drawer>

    )

  }

}

const mapStateToProps = (state) => {
  return {
    open: state.dashboard.adminMenuOpen,
    page: state.routing.locationBeforeTransitions.pathname,
    isAdmin: isInternalStaff(state.app.role) 
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
      handleToggle: () => {
          dispatch(dashboardToggleAdminMenu());
      }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminMenu);
