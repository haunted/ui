import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

export default class DaysOpen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      days: [false, false, false, false, false, false]
    }
  }
  componentDidMount () {
    this.setState({
      days: this.props.days
    })
  }
  toggleDay (i) {
    let days = this.state.days
    days[i] =! days[i]
    this.setState({ days })
    this.props.onChange(days)
  }
  colorButton (i) {
    let color = '#ffffff'
    if (this.state.days[i]) {
      color = 'rgba(39, 54, 122, 0.42)'
    }
    return color
  }
  render() {
      let days = [
        "Mon",
        "Tues",
        "Wed",
        "Thu",
        "Fri",
        "Sat",
        "Sun"
      ]

    return (
      <div>
            {
                this.state.days.map((day, i) => {
                    return (
                      <RaisedButton
                        onTouchTap={e => this.toggleDay(i)}
                        style={{marginRight: '0.2em'}}
                        backgroundColor={this.colorButton(i)}
                        label={days[i]}
                        key={i}
                      />
                    )
                })
            }
      </div>
    )
  }
}

DaysOpen.propTypes = {
  onChange: PropTypes.func.isRequired,
  days: PropTypes.array.isRequired
}
