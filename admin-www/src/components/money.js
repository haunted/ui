import React, { Component, PropTypes } from 'react'
import {formatCurrency} from '../util'
const styles = {
}

class Money extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    // right now just assume everything is in USD
    return (
      <span>{formatCurrency("USD", this.props.value)}</span>
    )
  }
}

Money.propTypes = {
}

export default Money
