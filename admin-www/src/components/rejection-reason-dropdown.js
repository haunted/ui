import React from 'react';
import DropDownMenu from 'material-ui/DropDownMenu'
import MenuItem from 'material-ui/MenuItem'

const styles = {
  customWidth: {
    width: 200,
  },
};

export default class RejectionReasonDropdown extends React.Component {

  constructor(props) {
    super(props);
    this.state = {value: ""};
  }

  handleChange (event, index, value) {
    this.setState({value});
    !!this.props.callback && this.props.callback(value);
  }

  render() {
    return (
      <div className="rejection-reason-dropdown">
        <DropDownMenu value={this.state.value} onChange={(event, index, value)=> this.handleChange(event, index, value)}>
          <MenuItem value={""} primaryText="Select Reason" />
          <MenuItem value={"not a voucher"} primaryText="Not a voucher" />
          <MenuItem value={"unreadable"} primaryText="Unreadable" />
        </DropDownMenu>
      </div>
    );
  }
}
