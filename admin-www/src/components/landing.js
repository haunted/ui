import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import { browserHistory } from 'react-router'
import {
  grey900
} from 'material-ui/styles/colors';

const styles = {
  header: {
    color: grey900,
    marginBottom: "1em",
  }
}

class Landing extends Component {
  constructor(props) {
    super(props)
  }
  componentDidMount() {
  }
  onLogin(e) {
    if (this.props.auth.authData.accessToken) {
      browserHistory.push(this.props.app.redirectAfterLogin)
    } else {
      browserHistory.push("login")
    }
  }
  render() {
    return (
      <div className="landing">
        <div className="landing-logo"></div>
        <RaisedButton
          className="landing-enter-button"
          label={this.props.auth.authData.accessToken ? "Open" : "Login"}
          onClick={e => this.onLogin(e)}
        />
      </div>
    )
  }
}
import { connect } from 'react-redux'

export default connect(
  state => {
    return {
      app: state.app,
      auth: state.auth,
    }
  },
  dispatch => {
    return {
    }
  }
)(Landing)
