/* globals TZ */

import React, { Component, PropTypes } from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import moment from 'moment';


export default class RedemptionsTable extends Component {
  renderRows() {
    const { redemptions } = this.props;

    return redemptions.map(r => (
      <TableRow key={r.redemptionId}>
        <TableRowColumn>
          {r.timestamp ? moment(r.timestamp).tz(TZ).locale('en').format('DD-MMM-YYYY HH:mm A') : ''}
        </TableRowColumn>
        <TableRowColumn>
          {r.source.operatorId}
        </TableRowColumn>
        <TableRowColumn>
          {r.supplier.code}
        </TableRowColumn>
      </TableRow>
    ));
  }

  render() {
    return (
      <Table selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn>Redemption Time</TableHeaderColumn>
            <TableHeaderColumn>Operator ID</TableHeaderColumn>
            <TableHeaderColumn>Supplier Code</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {this.renderRows()}
        </TableBody>
      </Table>
    );
  }
}

RedemptionsTable.propTypes = {
  redemptions: PropTypes.arrayOf(PropTypes.object),
};

RedemptionsTable.defaultProps = {
  redemptions: [],
};
