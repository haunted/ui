import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import Toggle from 'material-ui/Toggle';
import { browserHistory } from 'react-router'
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete';
import ActionGrade from 'material-ui/svg-icons/action/grade'
import AlertError from 'material-ui/svg-icons/alert/error'
import ExportButton from './export-button'
import PrintButton from './print-button'
import LightBox from './lightbox'
import moment from 'moment-timezone'
import getInclusiveRange from './get-inclusive-range'
import validateDateRange from './validate-date-range'
import {formatStatusEnum, formatUnknown} from '../../util'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'
import TableSettings from '../table-settings'
import Display from '../display'
import SuppliersPopover from './suppliers-popover'
import ResellersAutocomplete from './resellers-autocomplete'

import { DomainTime } from '../datepicker'
import { DateRangePicker, DateRangeTitle, Pagination } from './components/filters'


const dataSourceConfig = {
    text: 'text',
    value: 'value',
  },
  styles = {
    icon: {
      marginRight: "0.5em",
      opacity: 0.8
    },
    actions: {
      width: '210px'
    }
  }
class Vouchers extends Component {
  constructor(props) {
    super(props)

    props.contextFilters(props.params, props.location)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.version != prevProps.version) {
      this.fetch()
    }
  }

  fetch() {
    let { startDate, endDate, resellerCode, supplierCode, limit, offset, sort } = this.props

    browserHistory.push(
      `/app/reports/vouchers${
        !! supplierCode ? `/${supplierCode}` : ""
      }${
        !! resellerCode ? `/${resellerCode}` : ""
      }?start=${
        startDate.getUnixTime()
      }&end=${
        endDate.getUnixTime()  
      }&offset=${
        offset
      }&limit=${
        limit
      }&sort=${
        sort
      }`
    );

    this.props.getVouchers({
      start: startDate.getUnixTime() * 1000, // a wild endpoint has appeared! (surprise nanoseconds attack!)
      end: endDate.getUnixTime() * 1000,     // Wild endpoint deals 1000 damage to brain! Brain is knocked unconcious.
      limit,
      offset,
      sort,
      resellerCode,
      supplierCode,
    })    
  }
      
  export(format) {
    let { startDate, endDate, resellerCode, supplierCode } = this.props

    this.props.export({
      start: startDate.getUnixTime() * 1000,
      end: endDate.getUnixTime() * 1000,
      resellerCode,
      supplierCode,
    }, format)
  }

  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }
  
  parseStatus (code) {
      if (typeof code != 'undefined') {
          return formatStatusEnum(code)
      } else {
          return ""
      }
  }
  onRowTap (e, v) {
      e.preventDefault();
      let start = this.props.startDate,
          end = this.props.endDate,
          sort = this.props.sort,
          offset = this.props.offset,
          limit = this.props.limit,
          supplierCode = this.props.supplierCode,
          resellerCode = this.props.resellerCode;

      if (e.target.tagName != "DIV" && e.target.tagName != "SPAN" && e.target.tagname != "BUTTON") {
          browserHistory.push(`/app/reports/vouchers/${supplierCode}${(resellerCode != "" ? ("/"+resellerCode) : "")}/detail/${v.id}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}&offset=${offset}&limit=${limit}&sort=${sort}`)
      }
  }
  reject (voucherId) {
    let supplierCode = this.props.supplierCode,
        start = new Date(this.props.location.query.start * 1000),
        end = new Date(this.props.location.query.end * 1000);

       this.props.rejectVoucher(voucherId);
       setTimeout(()=>{
         browserHistory.push(`/app/reports/voucher-exceptions/${supplierCode}/edit/${voucherId}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}`);
       }, 500)
  }

  render() {
      let summaryMode = (this.props.children == null || this.props.children.length < 1),
          processed = 0,
          unprocessed = 0,
          exceptions = 0,
          corrected = 0


      this.props.vouchersData && this.props.vouchersData.vouchers && this.props.vouchersData.vouchers.map((v,i) => {
        switch (v.status) {
          case "STATUS_EXCEPTION":
          case "STATUS_FLAGGED":
          case "STATUS_REJECTED":
          case "STATUS_REDEMPTION_VOIDED":
            exceptions ++;
          break;
          case "STATUS_NEW":
          case "STATUS_PENDING":
          case "STATUS_UNKNOWN":
            unprocessed ++;
          break;
          case "STATUS_TICKETED":
            processed ++;
          break;
          case "STATUS_CORRECTED":
            corrected ++;
        }
      });

    return (
      <div className="all-vouchers">
        <div className="exceptions-header" style={{display: (summaryMode ? "inherit" : "none")}}>
            <div className="exceptions-header-left" style={{marginLeft: "1em"}}>
                <h2 style={{color:"#434343"}}>
                    All Vouchers
                    <DateRangeTitle />
                </h2>
            </div>
            <div className="exceptions-header-right" style={{marginRight: "1em"}}>
                <div className="no-print detail-view-controls" style={{display: this.props.printMode ? "none" : "inherit"}}>
                    { ! this.props.isSupplierStaff ? (
                      <SuppliersPopover defaultLabel={this.props.supplierCode}
                                        onSupplier={supplier => this.props.setSupplierCode(supplier.supplier.code)} />
                    ) : "" }
                    { ! this.props.isResellerStaff ? (
                      <ResellersAutocomplete 
                        onReseller={r => this.props.setResellerCode(r.code)}
                        supplierId={this.props.isSupplierStaff && this.props.adminSupplierId != -1 ? this.props.adminSupplierId : ""}
                        selectedResellerCode={this.props.resellerCode}
                      />                      
                    ) : ""}
                  <DateRangePicker />
                  <ExportButton
                    onExport={format => this.export(format)}
                  />
              </div>
              <PrintButton
                 style={{marginLeft: "1em"}}
                 onPrint={data=> { this.print() }} printMode={this.props.printMode}
              />
            </div>
      </div>
      <div className="report-details">
          {this.props.children}
      </div>
         {
            this.props.children == null || this.props.children.length < 1  ? (
               this.props.fetchingVouchers
               ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
               (
                 <section className="inner-report">
                   <Table className="detail-tabulation index">
                       <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                          <TableRow>
                            <TableHeaderColumn >Status</TableHeaderColumn>
                            <TableHeaderColumn >Count</TableHeaderColumn>
                          </TableRow>
                        </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                      <TableRow>
                        <TableRowColumn >Processed</TableRowColumn>
                        <TableRowColumn >{processed}</TableRowColumn>
                      </TableRow>
                      <TableRow>
                        <TableRowColumn >Unprocessed</TableRowColumn>
                        <TableRowColumn >{unprocessed}</TableRowColumn>
                      </TableRow>
                      <TableRow>
                        <TableRowColumn >Exceptions</TableRowColumn>
                        <TableRowColumn >{exceptions}</TableRowColumn>
                      </TableRow>
                      <TableRow>
                        <TableRowColumn >Corrected</TableRowColumn>
                        <TableRowColumn >{corrected}</TableRowColumn>
                      </TableRow>
                      </TableBody>
                    </Table>
                     <Table className="detail-tabulation index">
                         <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                            <TableRow>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("status") }>Status</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                <div className="inner" onClick={ e => this.props.setSort("date") }>Date</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("supplier") }>Supplier</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("reseller") }>Reseller</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("product") }>Product Name</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("productCode") }>Product Code</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("adults") }>Adults</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("children") }>Children</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("infants") }>Infants</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("seniors") }>Seniors</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                  <div className="inner" onClick={ e => this.props.setSort("students") }>Students</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn style={styles.actions} className="no-print-table-cell">Actions</TableHeaderColumn>
                            </TableRow>
                          </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        {
                          this.props.vouchersData && this.props.vouchersData.vouchers && this.props.vouchersData.vouchers.map((v,i) =>
                              {
                                  let hasCloudFactory = !!v.cloudFactory && !!v.cloudFactory.output,
                                      noImages = !!! v.images || (!!v.images && v.images.length === 0),
                                      rowColor = "transparent",
                                      statusStyle = {
                                        position: "relative"
                                      },
                                      icon = "",
                                      status = this.parseStatus(v.status),                                      
                                      supplierCode = !!v.supplierCode && v.supplierCode != "" ? formatUnknown(v.supplierCode) : "–",
                                      resellerCode = hasCloudFactory ? formatUnknown(v.cloudFactory.output.resellerCode) : "–",
                                      productName = hasCloudFactory ? formatUnknown(v.cloudFactory.output.productName) : "–",
                                      productCode = hasCloudFactory ? formatUnknown(v.cloudFactory.output.productCode) : "–"

                                  if (v.status == "STATUS_EXCEPTION") {
                                    rowColor = "rgba(255, 0, 0, 0.5)";
                                    statusStyle.bottom = "5px";
                                    icon = <AlertError key="icon" style={styles.icon} />
                                  } else if (v.status == "STATUS_NEW") {
                                    rowColor = "rgba(0, 255, 0, 0.25)";
                                    statusStyle.bottom = "5px";
                                    icon = <ActionGrade key="icon" style={styles.icon} />
                                  }

                                  return (
                                    <TableRow key={i} onTouchTap={e => { this.onRowTap(e, v) }} style={{ backgroundColor: rowColor }} >
                                      <TableRowColumn title={status}>
                                        {[icon, <span key="status" style={statusStyle} >{status}</span>]}
                                      </TableRowColumn>
                                      <TableRowColumn>
                                        <DomainTime ts={v.created} />
                                      </TableRowColumn>
                                      <TableRowColumn title={supplierCode}>
                                        {supplierCode}
                                      </TableRowColumn>
                                      <TableRowColumn title={resellerCode}>
                                        {resellerCode}
                                      </TableRowColumn>
                                      <TableRowColumn title={productName}>
                                        {productName}
                                      </TableRowColumn>
                                      <TableRowColumn title={productCode}>
                                        {productCode}
                                      </TableRowColumn>
                                      <TableRowColumn>
                                        {hasCloudFactory ? v.cloudFactory.output.numberOfAdults : "–"}
                                      </TableRowColumn>
                                      <TableRowColumn>
                                        {hasCloudFactory ? v.cloudFactory.output.numberOfChildren : "–"}
                                      </TableRowColumn>
                                      <TableRowColumn>
                                        {hasCloudFactory ? v.cloudFactory.output.numberOfInfants : "–"}
                                      </TableRowColumn>
                                      <TableRowColumn>
                                        {hasCloudFactory ? v.cloudFactory.output.numberOfSeniors : "–"}
                                      </TableRowColumn>
                                      <TableRowColumn>
                                        {hasCloudFactory ? v.cloudFactory.output.numberOfStudents : "–"}
                                      </TableRowColumn>
                                      <TableRowColumn style={styles.actions} className="no-print-table-cell">
                                        <LightBox voucherId={v.id} disabled={noImages} title={noImages ? "Scanned" : "View Image"} />
                                        {!noImages ? <RaisedButton className="save-button" label="Reject" style={{ marginLeft: "1em" }} onClick={e => this.reject(v.id)} /> : ""}
                                      </TableRowColumn>
                                    </TableRow>
                                  )
                              }
                          )
                        }
                      </TableBody>
                    </Table>
                    <Pagination />
                </section>
              )
          ) : ""
        }
      </div>
    )
  }
}


import { connect } from 'react-redux'
import { togglePrintMode } from '../../ax-redux/actions/app'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../ax-redux/actions/report-filters'

import {
    rejectVoucher,
    getVouchers,
    exportVouchersCSV,
    exportVouchersXLSX
} from '../../ax-redux/actions/vouchers'

export default connect(
  state => {
    let role = state.app.role !== false ? state.app.role : ""
    return ({
        vouchersData: state.vouchers.getVouchers.data,
        fetchingVouchers: state.vouchers.getVouchers.fetching,
        printMode: state.app.printMode,
        role: state.app.role,
        isSupplierStaff: role.indexOf("SUPPLIER") > -1,
        isSupplierBusinessStaff: role.indexOf("SUPPLIER_BUSINESS_STAFF") > -1,
        isResellerStaff: role.indexOf("RESELLER") > -1,
        isInternalStaff: (role.indexOf('ACTOUREX') > -1 || role.indexOf("REDEAM") > -1),
      ...state.reportFilters,
    })
  },
  dispatch => ({
    getVouchers: (data) => {
        dispatch(getVouchers(data))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportVouchersCSV(opts))
          return
        case "XLSX":
          dispatch(exportVouchersXLSX(opts))
          return
      }
    },
    rejectVoucher: (id, data) => {
      dispatch(rejectVoucher(id, data))
    },
    ...bindActionCreators(filterActions, dispatch),
  })
)(Vouchers);
