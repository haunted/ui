import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import ExportButton from './export-button'
import PrintButton from './print-button'
import { browserHistory } from 'react-router'
import SupplierMultiSelect from './supplier-multi-select'
import PassIssuersAutoComplete from './pass-issuers-autocomplete'
import { DateStringTitle } from '../datepicker'

import RedeamTable from '../standard/table'
import { DateRangePicker, DateRangeTitle,  Pagination } from './components/filters'

import {
  isSupplierStaff,
  formatCurrency,
  formatPrice,
  enDash
} from '../../util'

let self = null,
    styles = {
        operatorId: { marginLeft: '1em', maxWidth: '96px' },
        wide: { width: '300px' },
        narrow: { width: '80px' }
    }

class PassRedemptionsTable extends Component {
    constructor( props ) {
        super(props)

        props.contextFilters(props.params, props.location)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.version != prevProps.version) {
          this.fetch()
        }
      }
      
    fetch() {
        let { startDate, endDate, supplierCodes, limit, offset, sort } = this.props

        const url = `/app/reports/pass-redemptions/${
            supplierCodes.join(',')
        }?start=${
          startDate.getUnixTime()
        }&end=${
          endDate.getUnixTime()  
        }&&offset=${offset}&limit=${limit}&sort=${sort}`
    
        browserHistory.replace(url);
    
        this.props.fetch({
          start: startDate.getUnixTime(),
          end: endDate.getUnixTime(),
          limit,
          offset,
          suppliers: supplierCodes.join(','),
        })

    }

    export( format ) {
        let { startDate, endDate, supplierCodes } = this.props
        this.props.export({
            start: startDate.getUnixTime() * 1000,
            end: endDate.getUnixTime() * 1000,
            suppliers: supplierCodes.join(','),
        }, format)
    }

    print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500)
      }
      this.props.togglePrintMode()
    }

    rowClicked( event, redemption ) {
        if ( redemption.passId != "" ) {
            browserHistory.push(`/app/reports/pass/${redemption.passId}?supplierCode=${redemption.supplierCode}&back=pass-redemptions`);
        }
    }

    render () {
        let summaryMode = (this.props.children == null || this.props.children.length < 1)
        const { supplierCodes } = this.props
        return (
            <div className="redeemed">
                <div className="redeemed-header" style={{display: (summaryMode ? "inherit" : "none")}}>
                    <div className="redeemed-header-left">
                        <div>
                            <div>
                                <h2 style={{color:"#434343"}}>
                                    Pass Redemptions
                                    <DateRangeTitle />
                                </h2>
                            </div>
                            { supplierCodes.length > 0 ?
                                <div style={{ marginBottom: '1em'}}> 
                                    <b>Suppliers: </b> { supplierCodes.join(', ') }
                                </div> : null }
                            
                        </div>
                    </div>
                <div className="redeemed-header-right">
                    <div className={"no-print report-controls"+ (this.props.printMode?" print-mode" : "")}>
                        <DateRangePicker />
                        {/* {
                            false && this.props.role == ACTOUREX_TECHNICAL_STAFF ? // will need new endpoints for this
                            <PassIssuersAutoComplete onSupplier={supplier => this.setPassIssuer(supplier)}
                                                     defaultLabel={this.state.supplierCode}
                            /> : ""
                        } */}
                        <SupplierMultiSelect
                            onSuppliers={codes => this.props.setSupplierCodes(codes)}
                            selectedSuppliers={supplierCodes}
                        />
                        <ExportButton onExport={format => this.export(format)}
                                      style={{marginLeft: "1em"}} 
                        />
                    </div>
                    <PrintButton onPrint={data=> { this.print() }} printMode={this.props.printMode}
                                 style={{marginLeft: "1em"}}
                    />
                </div>
            </div>
            <div className="report-details">
                {this.props.children}
            </div>
            { 
                this.props.fetching ? (
                    <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={2} />
                ) : summaryMode ? (
                    <section style={{ width: '100%' }} className="inner-report">
                        <RedeamTable
                            onSort={(name) => this.props.setSort(name)}
                            onRowClick={(e, v) => this.rowClicked(e, v)}
                            columns={[
                                { title: 'Date', sort: "date" },
                                { title: 'Pass ID', sort: "reference" },
                                { title: 'Supplier', sort: "supplier" },
                                { title: 'Supplier Tour', sort: "supplierTourCode" },
                                { title: '# of Travelers', sort: "travelers" },
                                { title: 'Net', sort: "cost" },
                            ]}
                            data={this.props.redeemed ? this.props.redeemed : []}
                            rows={[
                                v => <DateStringTitle value={v.date} />,
                                v => v.passId,
                                v => enDash( v.supplierName ),
                                v => enDash( v.supplierTour ),
                                v => v.adults + v.youths + v.children + v.infants + v.seniors + v.military + v.student + v.unknown,
                                v => formatPrice( v.netPrice ),
                            ]}
                        />
                        <Pagination />
                    </section>
        ) : "" }
        </div>
        )
    }
}

// redemptionType determines the label for the redemptionType column
function redemptionType(row) {
  if (!!row.manuallyCreated) {
    return "Manually Added"
  }

  return row.scanned ? "Barcode" : "Voucher Photo"
}

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as filterActions from '../../ax-redux/actions/report-filters'

import { 
    getPassRedemptions,
    exportPassRedemptionsXLSX,
    exportPassRedemptionsCSV
} from '../../ax-redux/actions/passes'

import { setPageTitle, togglePrintMode } from '../../ax-redux/actions/app'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'

export default connect(
  state => ({
        redeemed: state.passes.getRedemptions.data ? state.passes.getRedemptions.data.details : false,
        fetching: state.passes.getRedemptions.fetching,
        // role: state.app.role,
        printMode: state.app.printMode,
        ...state.reportFilters
    }),
  dispatch => ({
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    fetch: (opts) => {
      dispatch(getPassRedemptions(opts))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportPassRedemptionsCSV(opts))
          return
        case "XLSX":
          dispatch(exportPassRedemptionsXLSX(opts))
          return
      }
    },
    ...bindActionCreators(filterActions, dispatch),
  })
)(PassRedemptionsTable);
