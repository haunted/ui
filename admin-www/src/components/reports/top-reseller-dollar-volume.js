import moment from 'moment-timezone'
import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import DatePicker from 'material-ui/DatePicker'
import Toggle from 'material-ui/Toggle';
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from './export-button'
import PrintButton from './print-button'
import SuppliersPopover from './suppliers-popover'
import ResellersPopover from './resellers-popover'
import { formatCurrency } from '../../util'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'

import { DateRangePicker, DateRangeTitle } from './components/filters'

class TopResellerDollarVolume extends Component {
  constructor(props) {
    super(props)
    props.contextFilters(props.params, props.location)
  }

  componentDidUpdate(prevProps, prevState) {
      if (this.props.version != prevProps.version) {
          this.fetch()
      }
  }
  fetch() {
    browserHistory.push(`/app/reports/top-reseller-dollar-volume${
      (this.props.supplierCode != "" ? ("/"+this.props.supplierCode):"")
    }?start=${
      moment(this.props.startDate).tz(TZ).unix()
    }&end=${
      moment(this.props.endDate).tz(TZ).unix()
    }&sort=${
      this.props.sort
    }`);
    this.props.getResellerDollarVolume({
      start: this.props.startDate,
      end: this.props.endDate,
      supplierId: this.props.supplierCode,
    });
  }
  export(format) {
    this.props.export({
      start: this.props.startDate,
      end: this.props.endDate,
      supplierId: this.props.supplierCode,
      endpoint: "/api/finance/supplier-top-reseller-dollar-volume"
    }, format)
  }

  viewDetails (e, reseller, start, end) {
      e.preventDefault();
      let supplierCode = this.props.supplierCode,
          sort = this.props.sort,
          detailPath = `${(supplierCode !="" ? "/"+supplierCode :"")}${(reseller != "" ? "/"+reseller : "")}`;
      if (reseller != "") {
          browserHistory.push("/app/reports/redeemed"+detailPath);
      }
  }
  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }
  render() {
      let fetching = this.props.reportFetching,
          reportData = this.props.reportData,
          summaryMode = (this.props.children == null || this.props.children.length < 1)

    return (
      <div className="top-resellers">
        <div className="exceptions-header" style={{display: (!summaryMode ? "none" : "inherit")}}>
          <div className="exceptions-header-left">
            <h2 style={{color:"#434343"}}>
                Top Reseller Dollar Volume
                <DateRangeTitle />
            </h2>
          </div>
          <div className="exceptions-header-right" >
          <div className={"no-print report-controls"+ (this.props.printMode?" print-mode" : "")}>
              <DateRangePicker />
             {
               this.props.role == 'ACTOUREX_TECHNICAL_STAFF' ?
               <SuppliersPopover defaultLabel={this.props.supplierCode}
                                 onSupplier={supplier => this.props.setSupplierCode(supplier.supplier.code)} /> : []
             }
             <ExportButton
                style={{marginLeft: "1em"}}
               onExport={format => this.export(format)}
             />
            </div>
            <PrintButton
               style={{marginLeft: "1em"}}
               onPrint={data=> { this.print() }} printMode={this.props.printMode}
            />
          </div>
      </div>
      <div className="report-details">
          {this.props.children}
      </div>
         {
           fetching ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
           (
             summaryMode ?
               <Table className="index" selectable={false} key="2">
                 <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                   <TableRow>
                      <TableHeaderColumn key="2" className="sortable">
                       <div className="inner" onClick={e => this.props.setSort("resellerName")}>Reseller Name</div>
                      </TableHeaderColumn>
                      <TableHeaderColumn key="3" className="sortable">
                       <div className="inner" onClick={e => this.props.setSort("totalValue")}>Combined Dollar Value</div>
                      </TableHeaderColumn>
                      <TableHeaderColumn key="4" className="sortable">
                       <div className="inner" onClick={e => this.props.setSort("averageValue")}>Average Dollar Value</div>
                      </TableHeaderColumn>
                   </TableRow>
                 </TableHeader>
                 <TableBody displayRowCheckbox={false}>
                   {
                     !!reportData ? reportData.map((v, i) => (
                       <TableRow key={i} onTouchTap={e => this.viewDetails(e, v.resellerCode, this.props.startDate, this.props.endDate)}>
                        <TableRowColumn key={i + ":2"}>
                           {!!v.resellerName && v.resellerName != "" ? v.resellerName : "Unknown Reseller"}
                         </TableRowColumn>
                        <TableRowColumn key={i + ":3"}>
                           {formatCurrency("USD", v.totalValue)}
                         </TableRowColumn>
                        <TableRowColumn key={i + ":4"}>
                           {formatCurrency("USD", v.averageValue)}
                         </TableRowColumn>
                       </TableRow>
                     )) : "No results to display."
                   }
                 </TableBody>
               </Table>
            : ""
          )
        }
      </div>
    )
  }
}


import { connect } from 'react-redux'
import { setPageTitle, togglePrintMode} from '../../ax-redux/actions/app'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../ax-redux/actions/report-filters'
import {
    getResellerDollarVolume,
    exportCSV,
    exportXLSX
} from '../../ax-redux/actions/finances'

export default connect(
  state => ({
      reportData: state.finances.getResellerDollarVolume.data ? state.finances.getResellerDollarVolume.data.records : [],
      reportFetching: state.finances.getResellerDollarVolume.fetching,
      role: state.app.role,
      printMode: state.app.printMode,
      ...state.reportFilters
  }),
  dispatch => ({
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    getResellerDollarVolume: params => {
      dispatch(getResellerDollarVolume(params))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportCSV(opts))
          return
        case "XLSX":
          dispatch(exportXLSX(opts))
          return
      }
    },
    ...bindActionCreators(filterActions, dispatch),
  })
)(TopResellerDollarVolume);
