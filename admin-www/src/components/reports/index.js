import React, { Component, PropTypes } from 'react'
import {
    ACTOUREX_CUSTOMER_SUPPORT_STAFF,
    ACTOUREX_BUSINESS_STAFF,
    ACTOUREX_TECHNICAL_STAFF,
    SUPPLIER_REDEMPTION_STAFF,
    SUPPLIER_REDEMPTION_SUPERVISOR,
    SUPPLIER_BUSINESS_STAFF,
    RESELLER_BUSINESS_STAFF,
    PASS_ISSUER_BUSINESS_STAFF
} from '../../ax-redux/constants'
import { browserHistory } from 'react-router'

class ReportsContainer extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.props.setPageTitle("Reports")
    this.props.setSideMenuTitle("Reports")
  }

  componentWillUpdate(nextProps, nextState) {
      if (this.props.role != nextProps.role) {
        this.setItemsFor(nextProps.role);
      }
  }

  componentDidMount () {
    this.setItemsFor(this.props.role);
  }

  loadDefaultReport (report) {
      if (this.props.subsection == "") { // report index page
          browserHistory.push(report);
      }
  }

  setItemsFor(role) {

    switch (role) {
      case ACTOUREX_CUSTOMER_SUPPORT_STAFF:
        this.props.setSideMenuItems([])
      break
      case ACTOUREX_BUSINESS_STAFF:
          this.props.setSideMenuItems([
            { title: 'Reseller Invoices', url: '/app/reports/supplier-reseller-invoice'},
            { title: 'Reseller Ticket Volume', url: '/app/reports/top-reseller-ticket-volume'},
            { title: 'Reseller Dollar Volume', url: '/app/reports/top-reseller-dollar-volume'},
            { title: 'All Vouchers', url:'/app/reports/vouchers' },
            { title: 'Voucher Exceptions', url:'/app/reports/voucher-exceptions'},
            { title: 'Expected Arrivals', url:'/app/reports/arrivals'},
            // #419 admin: This report shows cancellations, and is not currently useful
            // { title: 'Exceptions', url:'/app/reports/exceptions'},
            { title: 'Redeemed', url:'/app/reports/redeemed'},
            { title: 'Pass Redemptions', url:'/app/reports/pass-redemptions'},
            { title: 'Shift Summary', url: '/app/reports/shifts'},
          ])
      break
      case ACTOUREX_TECHNICAL_STAFF:
          this.props.setSideMenuItems([
            { title: 'Reseller Invoices', url: '/app/reports/supplier-reseller-invoice'},
            { title: 'Reseller Ticket Volume', url: '/app/reports/top-reseller-ticket-volume'},
            { title: 'Reseller Dollar Volume', url: '/app/reports/top-reseller-dollar-volume'},
            { title: 'All Vouchers', url:'/app/reports/vouchers' },
            { title: 'Voucher Exceptions', url:'/app/reports/voucher-exceptions'},
            { title: 'Expected Arrivals', url:'/app/reports/arrivals'},
            // #419 admin: This report shows cancellations, and is not currently useful
            // { title: 'Exceptions', url:'/app/reports/exceptions'},
            { title: 'Redeemed', url:'/app/reports/redeemed'},
            { title: 'Pass Redemptions', url:'/app/reports/pass-redemptions'},
            { title: 'Shift Summary', url: '/app/reports/shifts'},
          ])
      break
      case SUPPLIER_REDEMPTION_STAFF:
        // this.props.setSideMenuItems([
        //     { title: 'Shift Report', url: '/app/reports/shift'}
        // ])
      break
      case SUPPLIER_REDEMPTION_SUPERVISOR:
        this.props.setSideMenuItems([
            { title: 'Shift Summary', url: '/app/reports/shifts'},
        ])
        this.loadDefaultReport('/app/reports/shifts');
      break
      case SUPPLIER_BUSINESS_STAFF:
        this.props.setSideMenuItems([
          { title: 'Reseller Invoices', url: '/app/reports/supplier-reseller-invoice'},
          { title: 'Reseller Ticket Volume', url: '/app/reports/top-reseller-ticket-volume'},
          { title: 'Reseller Dollar Volume', url: '/app/reports/top-reseller-dollar-volume'},
          { title: 'All Vouchers', url:'/app/reports/vouchers' },
          { title: 'Voucher Exceptions', url:'/app/reports/voucher-exceptions'},
          { title: 'Expected Arrivals', url:'/app/reports/arrivals'},
          // #419 admin: This report shows cancellations, and is not currently useful
          // { title: 'Exceptions', url:'/app/reports/exceptions'},
          { title: 'Redeemed', url:'/app/reports/redeemed'},
          { title: 'Shift Summary', url: '/app/reports/shifts'},
          ]);
        this.loadDefaultReport('/app/reports/supplier-reseller-invoice');
        break
      case RESELLER_BUSINESS_STAFF:
        this.props.setSideMenuItems([
          { title: 'Redeemed', url:'/app/reports/reseller-redemptions'},
        ])
        this.loadDefaultReport('/app/reports/reseller-redemptions');
      break
      case PASS_ISSUER_BUSINESS_STAFF:
        this.props.setSideMenuItems([
          { title: 'Pass Redemptions', url:'/app/reports/pass-redemptions'},
        ])
      break
    }
  }

  render() {
    return ( 
      <div>
        {this.props.children}
      </div>
    )
  }
}
import { connect } from 'react-redux'
import { setPageTitle, setSideMenuItems, setSideMenuTitle } from '../../ax-redux/actions/app'

export default connect(
  state => {
    return {
      app: state.app,
      role: state.app.role,
      subsection: state.routing.locationBeforeTransitions.pathname.split("reports").pop(),
    }
  },
  dispatch => {
    return {
      setPageTitle: items => {
        dispatch(setPageTitle(items))
      },
      setSideMenuItems: items => {
        dispatch(setSideMenuItems(items))
      },
      setSideMenuTitle: items => {
        dispatch(setSideMenuTitle(items))
      },
    }
  }
)(ReportsContainer)
