import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete'
import TextField from 'material-ui/TextField'

const dataSourceConfig = {
    text: 'text',
    value: 'value',
};

class ProductsAutocomplete extends Component {

  constructor(props) {
    super(props)

    this.state = {
      open: false,
      label: "Products",
      lookedUpName: false,
      generated: false,
      productName: "",
      productCode: "",
      productId: "",
      productsByName: [],
      productsByCode: [],
      supplierCatalogId: -1,
      resellerCatalogId: -1
    }
  }

  generateProductAutoComplete (products, priceListItems) {

      let byName = [],
          byCode = []

      products.map((product) => {
          let optionsAdded = 0

        if ( !!this.props.catalogMode && !!priceListItems ) {
            priceListItems.map(item=>{
              let itemAddr = item.item.catalogAddress
              if (product.id == itemAddr.itemId) {
                product.options.map(opt => {
                  if (opt.id == itemAddr.optionId) {
                    optionsAdded ++
                  }
                })
              }
            }) 
        }

      if ( !!!this.props.catalogMode || !!product && !!product.options && (optionsAdded < product.options.length) ) {
         
            byCode.push({
              text: product.code,
              value: product
            })
            byName.push({
                text: product.name,
                value: product
            })
         }
      })
      this.setState({
          productsByName: byName,
          productsByCode: byCode,
          generated: true
      })

  }

  fetch(supplierCode) {
    this.props.getSupplierProducts(supplierCode)
  }

  componentWillMount() {

    if ( !!this.props.catalogMode) {
      

    } else {

      if ( (this.props.products == false && !this.props.fetchingProducts) ) {

          this.fetch(this.props.supplierCode)

      } else if (!this.props.fetchingProducts) {

          this.generateProductAutoComplete(this.props.products)

      }

    }

    let hasProducts = this.props.products != false && this.props.products.length != 0,
        noCompletionValues = !! this.state.productsByName && this.state.productsByName.length == 0

    if ( noCompletionValues && hasProducts) {

        this.generateProductAutoComplete(this.props.products); console.log("%%%%%%% generate autocomplete")

    }

  }

  componentWillUpdate( nextProps, nextState ) {

    if ( this.props.supplierCode != nextProps.supplierCode ) {

        this.fetch(nextProps.supplierCode)

    }

    if ( !!this.props.selectDefault && this.props.products.length == 0 ) {

        if ( nextProps.products.length > 0 ) {

              this.props.onProduct(nextProps.products[0])
              this.setState({
                  label: this.label(nextProps.products[0]),
              })

        }

    }

    if ( (nextState.lookedUpName === false && nextProps.products.length != 0 && nextProps.selectedProductCode != null && nextProps.selectedProductCode != "") ) {

        console.log("selected product code ", nextProps.selectedProductCode); console.log("looking up product name from code... ");
        
        nextProps.products.map(( v, i ) => {

          if ( v.code == nextProps.selectedProductCode ) {

            this.setState({
              lookedUpName: true,
              label: this.label(v),
              productName: this.label(v)
            })

          }

        })

    }

    if ( nextState.productName != "" && this.state.productName != nextState.productName ) {

          this.setState({
            lookedUpName: true,
            label: nextState.productName
          })

    }

    let hasProducts = nextProps.products != false && nextProps.products.length != 0,
        noCompletionValues = !! nextState.productsByName && nextState.productsByName.length == 0

    if ( ((nextProps.fetchingProducts == false && this.props.fetchingProducts == true) || (noCompletionValues && nextState.generated == false)) && hasProducts ) {
      
      this.generateProductAutoComplete(nextProps.products, nextProps.priceListItems); console.log("%%%%%%% 1 generate autocomplete")

    }

    if ( this.props.priceListItems == false && !!nextProps.priceListItems && hasProducts && nextState.generated == false ) {

      this.generateProductAutoComplete(nextProps.products, nextProps.priceListItems); console.log("%%%%%%% 2 generate autocomplete")

    }

    if ( (this.props.supplierCatalogId == -1 && nextProps.supplierCatalogId != -1) || (nextProps.supplierCatalogId != -1 && nextProps.supplierCatalogItems == false) ) {

       this.props.listSupplierCatalogItems(nextProps.supplierId, nextProps.supplierCatalogId)

    }

  }

  label(v) {
    if (v.name != "") {
      return v.name
    }
    return v.code
  }

  render() {

    return (
      <div className="reseller-autocomplete-container">
            {
                this.props.fetchingProducts || this.props.fetching || this.props.products.length == 0 ?
                  <TextField value={"Loading.."}
                             className="login-input"
                             autoComplete="off"
                             disabled={true}
                             id={"loading"}
                             type="text"
                     /> : (
                    <AutoComplete
                        hintText="Product Name"
                        searchText={this.state.productName}
                        filter={AutoComplete.fuzzyFilter}
                        key="reseller-name-field" id="reseller-name-field"
                        openOnFocus={true}
                        fullWidth={true}
                        menuProps={{ maxHeight: 600 }}
                        dataSource={this.state.productsByName}
                        dataSourceConfig={dataSourceConfig}
                        onNewRequest={(chosenRequest, index) => {
                          let productName = "";
                          if (typeof chosenRequest == "string") {
                            productName = chosenRequest
                          } else {
                            productName = chosenRequest.text
                            this.setState({
                              productCode: chosenRequest.value.code
                            })
                            console.log("autocompleting product.. ", chosenRequest.value)
                            if (this.props.catalogMode) {
                              this.props.onProduct(chosenRequest.value)
                              productName = chosenRequest.value.name
                            } else {
                              this.props.onProduct({
                                code: chosenRequest.value,
                                name: chosenRequest.text
                              })    
                            }
                            
                          }
                          this.setState({
                            productName: productName
                          })
                        }}
                        onUpdateInput={(searchText, dataSource) => {
                          //console.log("on update input", searchText, dataSource);
                        }}
                        onBlur={e => {
                          this.setState({
                            "productName": e.target.value
                          })
                        }}
                    />
                  )
            }
      </div>
    )
  }

}

ProductsAutocomplete.propTypes = {
  onProduct: PropTypes.func.isRequired,
  supplierCode: PropTypes.string,
  selectDefault: PropTypes.bool,
  selectedProductCode: PropTypes.string,
  supplierId: PropTypes.string,
  catalogMode: PropTypes.bool,
  priceListItems: PropTypes.array
}

import { connect } from 'react-redux'
import { getSupplierProducts } from '../../ax-redux/actions/products'
import {
  listSupplierCatalogs,
  listSupplierCatalogItems
} from '../../ax-redux/actions/suppliers'
import {
  listResellerCatalogs,
  listResellerCatalogItems
} from '../../ax-redux/actions/resellers'

export default connect(
  (state, ownProps) => {
    let supplierCatalogItems = !!state.suppliers.listCatalogItems.data ? state.suppliers.listCatalogItems.data.catalogItems : false,
        supplierProducts = !!state.products.getSupplierProducts.data ? state.products.getSupplierProducts.data.products : false
        //supplierResellerCatalogItems = !!state.suppliers.listResellerCatalogItems.data ? state.suppliers.listResellerCatalogItems.data.catalogItems : false

    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      supplierCatalogId: state.reportFilters.supplierCatalogId,
      resellerCatalogId: state.reportFilters.resellerCatalogId,
      supplierCatalogs: !!state.suppliers.listCatalogs.data ? state.suppliers.listCatalogs.data.catalogs : false,
      resellerCatalogs: !!state.resellers.listCatalogs.data ? state.resellers.listCatalogs.data.catalogs : false,
      resellerCatalogItems: !!state.resellers.listCatalogItems.data ? state.resellers.listCatalogItems.data.catalogItems : false,
      supplierCatalogsFetching: state.suppliers.listCatalogs.fetching,
      resellerCatalogsFetching: state.resellers.listCatalogs.fetching,
     // supplierResellerCatalogItemsFetching: state.suppliers.listResellerCatalogItems.fetching,
      supplierCatalogItems: supplierCatalogItems,
      //supplierResellerCatalogItems: supplierResellerCatalogItems,
      products: !!ownProps.catalogMode ? supplierCatalogItems : supplierProducts,
      fetchingProducts: state.suppliers.listCatalogItems.fetching ||
                        state.products.listProducts.fetching
    }
  },
  dispatch => {
    return {
      getSupplierProducts: (supplierCode) => {
          dispatch(getSupplierProducts(supplierCode))
      },
      listSupplierCatalogs: (supplierId) => {
          dispatch(listSupplierCatalogs(supplierId))
      },
      listSupplierCatalogItems: (supplierId, catalogId) => {
          dispatch(listSupplierCatalogItems(supplierId, catalogId))
      },
      listResellerCatalogs: (resellerId) => {
        dispatch(listResellerCatalogs(resellerId))
      },
      listResellerCatalogItems: (resellerId, catalogId) => {
          dispatch(listResellerCatalogItems(resellerId, catalogId))
      }
    }
  }
)(ProductsAutocomplete)
