import React from 'react';
import Card from 'material-ui/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';
import CardText from 'material-ui/Card/CardText';
import CircularProgress from 'material-ui/CircularProgress';
import { browserHistory } from 'react-router'

let onClick = (e, link) => {
    if (link != "") {
        browserHistory.push(link);
    }
}

const figureStyle = (figure) => {
    return (figure >= 99 ? {
        fontSize: (figure > 99999999 ? "0.6em" : (figure > 999999 ? "0.7em" : "0.75em")),
        width: "92%",
        paddingTop: "30px"
    } : {
        padding: ""
    });
}

const renderFigure = (figure, unit) => {
    let figureClass = "figure"
    if (!figure || figure.length < 1) {
        unit = "";
    }
    if (typeof figure == 'object') {
        if (typeof figure.length == 'undefined') {
            return (
                <div className={figureClass}>
                    {Object.keys(figure).map((key) => {
                        return <div className="card-value" key={key}>
                            <span className="multiValue">{figure[key]}</span>
                            <span className="multiLabel">{key}</span>
                        </div>;
                    })}
                </div>
            )
        } else {
            return (
                <div className={figureClass}>
                    {figure.map((f, i) => {
                        return <div className="card-value" key={i}>
                            <span className="multiValue">{f.value}</span>
                            <span className="multiLabel">{f.name}</span>
                        </div>;
                    })}
                </div>
            )
        }
    } else {
        if (isNaN(figure)) {
            figureClass = "text figure";
        }
        return (
            <div className={figureClass}>
                { !!unit ?
                    <span className="unit">{unit}</span>
                    : ""
                }
                { figure }
            </div>
        )
    }
}

const Tile = ({title, figure, subtitle, fetching, unit, link}) => (
    <Card className="tile report-tile" >
      <div className="title">{title}</div>
      <CardText className={"inner" + (!!link ? " link" : "")}
                title={(!!link ? "View Details" : "")}
                style={figureStyle(figure)} onClick={ e => onClick(e, !!link ? link : "") }>
          {!!fetching ?
             <CircularProgress className="tile-loading" color={"#f1f1f1"} size={96} /> :
             renderFigure(figure, unit)
          }
      </CardText>
      <div className="subtitle">{subtitle}</div>
    </Card>
);

export default Tile;
