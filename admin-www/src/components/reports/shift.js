import React, { Component, PropTypes } from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import CircularProgress from 'material-ui/CircularProgress';
import ExportButton from './export-button';
import PrintButton from './print-button';
import LightBox from './lightbox';
import VoidButton from '../delete-voucher-button';
import MergeVoucherButton from '../merge-voucher-button';
import moment from 'moment-timezone';
import RaisedButton from 'material-ui/RaisedButton';
import { browserHistory } from 'react-router';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import AlertError from 'material-ui/svg-icons/alert/error';
import {
  enDash,
  isInternalStaff,
  isSupplierBusinessStaff,
} from '../../util';
import DateRangePicker, { DateStringTitle, TimestampTitle } from '../datepicker';

const styles = {
  tableRow: (v) => {
    const style = {};
    if (isException(v.voucherStatus)) {
      style.background = 'rgba(255, 0, 0, 0.5)';
    }
    if (isNew(v.voucherStatus)) {
      style.background = 'rgba(0, 255, 0, 0.25)';
    }
    if (v.ticketId != null && v.ticketId != '') {
      style.cursor = 'initial';
    }
    return style;
  },
  icon: v => ({
    opacity: 0.8,
    position: 'relative',
    top: '4px',
    left: '6px',
    marginRight: '8px',
  }),
  addVoucherButton: {
    minWidth: '190px',
  },
  addVoucherButtonContainer: {
    display: 'inline-block',
    margin: '1em',
    marginLeft: '3em',
    width: '100%',
  },
};

let isException = status => status == 'Exception' || status == 'Rejected',
  isNew = status => status == 'New';

class Shift extends Component {
  constructor(props) {
    super(props);

    props.contextFilters(props.params, props.location);
  }


  componentDidUpdate(prevProps, prevState) {
    const summaryMode = (this.props.children == null || this.props.children.length < 1);
    if (this.props.version != prevProps.version) {
      if (summaryMode) {
        this.fetch();
      }
    }
  }

  fetch() {
    const { operatorId, supplierCode, sort } = this.props;
    const { date } = this.props.params;
    const url = `/app/reports/shift/${
      operatorId
    }/${
      date
    }?sort=${
      sort
    }`;
    browserHistory.replace(url);


    this.props.fetchReport({
      operatorId,
      supplierId: supplierCode,
      date: moment(date).unix() * 1000,
      sort,
    });
    this.props.setReturnToShiftURL(url);
  }

  export(format) {
    const { operatorId, supplierCode, sort } = this.props;
    const { date } = this.props.params;

    this.props.export({
      date: moment(date).unix(),
      supplierId: supplierCode,
      operatorId,
      endpoint: '/api/reports/shift',
    }, format);
  }

  refreshReport() {
    setTimeout(() => this.fetch(), 500);
  }

  rowClicked(e, id, data) {
    e.preventDefault();
    const { operatorId, supplierCode } = this.props;
    const { date } = this.props.params;

    if (e.target.tagName == 'TD' || e.target.tagName == 'TR') {
      if (data.passId) {
        browserHistory.push(`/app/reports/pass/${data.passId}?supplierCode=${supplierCode}&back=shift`);
      } else if (id != null && id != '') {
        browserHistory.push(`/app/reports/shift/${operatorId}/${date}/ticket/${id}`);
      }
    }
  }
  reject(voucherId) {
    const { supplierCode } = this.props;

    this.props.rejectVoucher(voucherId);
    setTimeout(() => {
      browserHistory.push(`/app/reports/voucher-exceptions/${supplierCode}/edit/${voucherId}?back=shift`);
    }, 500);
  }
  print() {
    if (!this.props.printMode) {
      setTimeout(() => {
        window.print();
      }, 500);
    }
    this.props.togglePrintMode();
  }
  returnToSummary() {
    browserHistory.push(this.props.returnToShiftSummaryURL);
  }

  addVoucher() {
    const { operatorId, supplierCode } = this.props;
    const { date } = this.props.params;

    browserHistory.push(`/app/reports/redeemed/add-voucher/${supplierCode}/${operatorId}/${date}`);
  }

  renderExceptions(exceptions) {
    if (!!exceptions && exceptions > 0) {
      return (
        <Table className="number-of-exceptions" selectable={false}>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn key="1">Number of Exceptions</TableHeaderColumn>
              <TableHeaderColumn key="2">{ exceptions }</TableHeaderColumn>
            </TableRow>
          </TableHeader>
        </Table>
      );
    }
  }

  renderSummary(fetching, report) {
    return (
      <Table className="index summary" selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn>Start</TableHeaderColumn>
            <TableHeaderColumn>End</TableHeaderColumn>
          </TableRow>
          <TableRow>
            <TableRowColumn>
              {!fetching && !!report ? <TimestampTitle ts={report.summary.start} format="DD-MMM-YYYY HH:mm A" /> : ''}</TableRowColumn>
            <TableRowColumn>
              {!fetching && !!report ? <TimestampTitle ts={report.summary.end} format="DD-MMM-YYYY HH:mm A" /> : ''}</TableRowColumn>
          </TableRow>
          <TableRow>
            <TableHeaderColumn key="1">Reseller</TableHeaderColumn>
            <TableHeaderColumn key="2">Vouchers</TableHeaderColumn>
            <TableHeaderColumn key="3">Tickets</TableHeaderColumn>
            <TableHeaderColumn key="4">Adult</TableHeaderColumn>
            <TableHeaderColumn key="5">Child</TableHeaderColumn>
            <TableHeaderColumn key="6">Other</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            !fetching && !!report && report.summary.ticketsByReseller.map((v, i) => (
              <TableRow key={i}>
                <TableRowColumn key={`${i}:1`}>{v.reseller}</TableRowColumn>
                <TableRowColumn key={`${i}:2`}>{v.vouchers}</TableRowColumn>
                <TableRowColumn key={`${i}:3`}>{v.tickets}</TableRowColumn>
                <TableRowColumn key={`${i}:4`}>{v.adults}</TableRowColumn>
                <TableRowColumn key={`${i}:5`}>{v.children}</TableRowColumn>
                <TableRowColumn key={`${i}:6`}>{v.other}</TableRowColumn>
              </TableRow>
            ))
          }
          {
            !fetching && !!report && !!report.summary && (report.summary.unprocessedVouchers > 0) && (
              <TableRow key="u_v">
                <TableRowColumn>Unprocessed Vouchers</TableRowColumn>
                <TableRowColumn>{report.summary.unprocessedVouchers}</TableRowColumn>
                <TableRowColumn />
                <TableRowColumn />
                <TableRowColumn />
                <TableRowColumn />
              </TableRow>
            )
          }
          {
            !fetching && !!report && !!report.summary && (report.summary.failedVouchers > 0) && (
              <TableRow key="f_v">
                <TableRowColumn>Voucher Exceptions</TableRowColumn>
                <TableRowColumn>{report.summary.failedVouchers}</TableRowColumn>
                <TableRowColumn />
                <TableRowColumn />
                <TableRowColumn />
                <TableRowColumn />
              </TableRow>
            )
          }
          {
            !fetching && !!report && !!report.summary && (
              <TableRow key="total">
                <TableRowColumn>TOTAL</TableRowColumn>
                <TableRowColumn>{report.summary.total}</TableRowColumn>
                <TableRowColumn>{report.summary.totalTickets}</TableRowColumn>
                <TableRowColumn>{report.summary.totalAdults}</TableRowColumn>
                <TableRowColumn>{report.summary.totalChildren}</TableRowColumn>
                <TableRowColumn>{report.summary.totalOther}</TableRowColumn>
              </TableRow>
            )
          }
        </TableBody>
      </Table>
    );
  }

  renderDetails(fetching, report) {
    return (
      <Table className="index detail" selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn className="sortable" style={{ width: '10%' }}>
              <div className="inner" onClick={e => this.sortBy('voucherStatus')}>Status</div>
            </TableHeaderColumn>
            <TableHeaderColumn className="sortable" style={{ width: '15%' }}>
              <div className="inner" onClick={e => this.sortBy('referenceNumber')}>Reference Number</div>
            </TableHeaderColumn>
            <TableHeaderColumn className="sortable" style={{ width: '15%' }}>
              <div className="inner" onClick={e => this.sortBy('resellerName')}>Reseller Name</div>
            </TableHeaderColumn>
            <TableHeaderColumn className="sortable" style={{ width: '20%' }}>
              <div className="inner" onClick={e => this.sortBy('tourName')}>Tour Name</div>
            </TableHeaderColumn>
            <TableHeaderColumn className="sortable" style={{ width: '10%' }}>
              <div className="inner" onClick={e => this.sortBy('timeRedeemed')}>Time Redeemed</div>
            </TableHeaderColumn>
            <TableHeaderColumn style={{ paddingLeft: '1em', width: '30%' }}>
                  Actions
            </TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            !fetching && !!report && report.detail.map((v, i) => {
              let noImages = v.manuallyCreated === true || (!v.manuallyCreated && (!v.voucherId || (!!v.voucherId && v.voucherId == ''))),
                status = v.voucherStatusCode || '',
                canHideRow = (status == 'STATUS_VOIDED' || status == 'STATUS_MERGED'),
                canNotReject = (status == 'STATUS_UNPROCESSED' || (status == 'STATUS_TICKETED' && noImages)),
                canDelete = status != 'STATUS_OPEN' && status != 'STATUS_TICKETED' && status != 'STATUS_CORRECTED' && status != 'STATUS_VOIDED',
                canMerge = status == 'STATUS_DUPLICATE' || status == 'STATUS_REDEMPTION_FAILED' || status == 'STATUS_REJECTED';

              if (canHideRow) return '';

              return (
                <TableRow key={i} style={styles.tableRow(v)} onTouchTap={(e) => { this.rowClicked(e, v.ticketId, v); }}>
                  <TableRowColumn style={{ paddingRight: '0', width: '10%' }}>
                    {[isException(v.voucherStatus) ? <AlertError style={styles.icon()} key="1" /> : '',
                      isNew(v.voucherStatus) ? <ActionGrade style={styles.icon()} key="2" /> : '',
                      v.voucherStatus,
                    ]}</TableRowColumn>
                  <TableRowColumn key={`${i}1`} style={{ paddingRight: '0', width: '15%' }}>{enDash(v.referenceNumber)}</TableRowColumn>
                  <TableRowColumn key={`${i}3`} style={{ paddingRight: '0', width: '15%' }}>
                    {enDash(v.resellerName)}
                  </TableRowColumn>
                  <TableRowColumn key={`${i}4`} style={{ paddingRight: '0', width: '20%' }}>
                    {enDash(v.tourName)}
                  </TableRowColumn>
                  <TableRowColumn key={`${i}5`} style={{ paddingRight: '0', width: '10%' }}>
                    <DateStringTitle value={v.timeRedeemed} format="HH:mm A" />
                  </TableRowColumn>
                  <TableRowColumn key={`${i}5`} style={{ paddingLeft: '1em', paddingRight: '0', width: '30%' }}>
                    <LightBox voucherId={v.voucherId} disabled={noImages} title={v.manuallyCreated === true ? 'Manually Added' : (noImages ? 'Scanned' : 'View')} />
                    { v.voucherId ? <RaisedButton
                      className="save-button"
                      label={status == 'STATUS_REJECTED' || status == 'STATUS_EXCEPTION' ? 'Correct' : 'Reject'}
                      style={{ marginLeft: '1em', marginRight: '0em' }}
                      onClick={e => this.reject(v.voucherId)}
                      disabled={canNotReject}
                    /> : '' }
                    { !!v.voucherId && canMerge ? (
                      <MergeVoucherButton
                        voucherId={v.voucherId}
                        label="Merge"
                        callback={() => this.refreshReport()}
                      />
                    ) : ''}
                    { !!v.voucherId && canDelete ? (
                      <VoidButton
                        voucherId={v.voucherId}
                        redemptionId={v.redemptionId}
                        label="Delete"
                        callback={() => this.refreshReport()}
                      />
                    ) : ''}
                  </TableRowColumn>
                </TableRow>
              );
            })
          }
        </TableBody>
      </Table>
    );
  }
  render() {
    let report = this.props.report,
      fetching = this.props.fetchingReport,
      totalExceptions = !!report && report.summary.exceptions ? report.summary.exceptions : '–',
      operatorId = this.props.operatorId,
      operatorName = report.detail && report.detail.length > 0 ? ` ${report.detail[0].operatorName}` : '',
      summaryMode = (this.props.children == null || this.props.children.length < 1);
    return (
      <div className="shifts">
        <div className="exceptions-header" style={{ display: (summaryMode ? 'inherit' : 'none') }}>
          <div className="exceptions-header-left">
            <h2 style={{ color: '#434343' }}>
                Shift Report
              <span className="operator-id">{ ` ${operatorId}${operatorName} ` }</span>
              <span className="title-date">
                  ( <DateStringTitle value={this.props.params.date} /> )
              </span>
            </h2>
          </div>
          <div className="exceptions-header-right">
            <h3 style={{ marginRight: '1em' }}>
                Total Vouchers: {!fetching && !!report && report.summary.total ? report.summary.total : 'TBD'}
            </h3>
            <div className={`no-print report-controls${this.props.printMode ? ' print-mode' : ''}`}>
              <RaisedButton
                onTouchTap={(e) => { this.returnToSummary(); }}
                className="no-print"
                style={{ minWidth: '190px', float: 'left', marginLeft: '1em' }}
                label="Return to Summary"
              />
              <ExportButton
                style={{ marginLeft: '1em', marginRight: '1em' }}
                onExport={format => this.export(format)}
              />
            </div>
            <PrintButton
              className="no-print"
              onPrint={(data) => { this.print(); }}
              printMode={this.props.printMode}
            />
          </div>
        </div>
        { summaryMode && this.renderExceptions(totalExceptions) }
        { summaryMode && this.renderSummary(fetching, report) }
        { summaryMode && (isInternalStaff(this.props.role) || isSupplierBusinessStaff(this.props.role)) ? (
          <div style={styles.addVoucherButtonContainer}>
            <RaisedButton
              onTouchTap={(e) => { this.addVoucher(); }}
              style={styles.addVoucherButton}
              className="no-print"
              label="Add Voucher"
            />
          </div>
        ) : ''
        }
        { summaryMode && !this.props.printMode && this.renderDetails(fetching, report) }
        { fetching ? <CircularProgress className="exceptions-circular-progress" color={'#27367a'} size={96} /> : ''}
        <div className="report-details">
          {this.props.children}
        </div>
      </div>
    );
  }
}


import { connect } from 'react-redux';
import { setPageTitle, togglePrintMode } from '../../ax-redux/actions/app';

import { bindActionCreators } from 'redux';
import * as filterActions from '../../ax-redux/actions/report-filters';

import { getShiftReport } from '../../ax-redux/actions/shifts';
import { setReturnToShiftURL } from '../../ax-redux/actions/navigation';
import {
  exportCSV,
  exportXLSX,
} from '../../ax-redux/actions/finances';
import {
  rejectVoucher,
} from '../../ax-redux/actions/vouchers';

export default connect(
  state => ({
    report: state.shifts.getShiftReport.data,
    fetchingReport: state.shifts.getShiftReport.fetching,
    role: state.app.role,
    printMode: state.app.printMode,
    isInternalStaff: isInternalStaff(state.app.role),
    returnToShiftSummaryURL: state.navigation.returnToShiftSummaryURL,
    ...state.reportFilters,
  }),
  dispatch => ({
    fetchReport: (params) => {
      dispatch(getShiftReport(params));
    },
    setReturnToShiftURL: (params) => {
      dispatch(setReturnToShiftURL(params));
    },
    setPageTitle: (title) => {
      dispatch(setPageTitle(title));
    },
    togglePrintMode: (opts) => {
      dispatch(togglePrintMode(opts));
    },
    export: (opts, format) => {
      switch (format) {
        case 'CSV':
          dispatch(exportCSV(opts));
          return;
        case 'XLSX':
          dispatch(exportXLSX(opts));
      }
    },
    rejectVoucher: (id, data) => {
      dispatch(rejectVoucher(id, data));
    },
    ...bindActionCreators(filterActions, dispatch),
  }),
)(Shift);
