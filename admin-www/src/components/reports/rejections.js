import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import Toggle from 'material-ui/Toggle';
import DatePicker from 'material-ui/DatePicker';
import { browserHistory } from 'react-router'
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from './export-button'
import PrintButton from './print-button'
import LightBox from './lightbox'
import SuppliersPopover from './suppliers-popover'
import moment from 'moment-timezone'
import ResellersPopover from './resellers-popover'
import getInclusiveRange from './get-inclusive-range'
import validateDateRange from './validate-date-range'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'

let self = null;

class SupplierVoucherExceptions extends Component {
  constructor(props) {
    super(props)
    let start = new Date(),
        end = new Date(),
        supplierCode = this.props.supplierCode,
        resellerCode = "", //this.props.filters.resellerCode,
        dateRange = [],
        sort = "",
        limit = 100,
        offset = 0,
        query = this.props.location.query,
        params = this.props.params

    start.setDate(start.getDate() - 30)
    if (query && query.start) {
        start = new Date(query.start*1000)
        end = new Date(query.end*1000)
    } else {
      start = this.props.filters.startDate
      end = this.props.filters.endDate
    }
    if (query && query.offset) {
      offset = query.offset
    }
    if (query && query.sort) {
        sort = query.sort
    }
    if (params && params.supplierCode) {
      supplierCode = params.supplierCode
    } else {
      supplierCode = this.props.filters.supplierCode
    }
    if (params && params.resellerCode) {
      resellerCode = params.resellerCode
    }
    dateRange = getInclusiveRange(start, end);
    this.state = {
        start: new Date(dateRange[0]),
        end: new Date(dateRange[1]),
        supplierCode: supplierCode,
        resellerCode: resellerCode,
        limit: limit,
        offset: offset,
        sort
    }
    self = this;
  }

  componentDidMount() {
    if (!!!this.props.children || this.props.children.length == 0) {
        let range = getInclusiveRange(this.state.start, this.state.end);
        this.props.getVoucherExceptions({
          start: range[0],
          end: range[1],
          limit: this.state.limit,
          offset: this.state.offset,
          resellerCode: this.state.resellerCode,
          supplierCode: this.state.supplierCode,
          sort: this.state.sort
        })
    } else {
        console.log("letting detail view fetch on its own..")
    }
  }
  componentWillUpdate(nextProps, nextState) {
    if (this.state.start != nextState.start ||
        this.state.end != nextState.end ||
        this.state.supplierCode != nextState.supplierCode ||
        this.state.resellerCode != nextState.resellerCode ||
        this.state.resellerName != nextState.resellerName
      ) {
        browserHistory.push(`/app/reports/rejections${(nextState.supplierCode !="" ?("/"+nextState.supplierCode):"")}${(nextState.resellerCode !="" ?("/"+nextState.resellerCode):"")}?start=${moment(nextState.start).tz(TZ).unix()}&end=${moment(nextState.end).tz(TZ).unix()}&offset=${nextState.offset}&limit=${nextState.limit}&sort=${nextState.sort}`);
    }
    if (this.props.pathname != nextProps.pathname ||
        this.props.queryParams != nextProps.queryParams) {
        if (!!!nextProps.children || nextProps.children.length == 0) { // if summary mode / not detail view
            let range = getInclusiveRange(nextState.start, nextState.end),
                requestParams = {
                start: range[0],
                end: range[1],
                limit: nextState.limit,
                offset: nextState.offset,
                resellerCode: nextState.resellerCode,
                supplierCode: nextState.supplierCode,
                sort: nextState.sort
              };
            if (nextProps.location.query) {
              if (nextProps.location.query.offset) {
                requestParams.offset = nextProps.location.query.offset;
              }
              if (nextProps.location.query.limit) {
                requestParams.limit = nextProps.location.query.limit;
              }
              if (nextProps.location.query.sort) {
                  requestParams.sort = nextProps.location.query.sort;
              }
            }
            this.props.getVoucherExceptions(requestParams)
        }
    }
  }
  setStartDate(date) {
      this.setState({
        start: date
      })
      this.props.setStartDate(date)
  }
  setEndDate(date) {
      this.setState({
        end: date
      })
      this.props.setEndDate(date)
  }
  setSupplier(supplier) {
      this.setState({ supplierCode: supplier.supplier.code })
      this.props.setSupplierCode(supplier.supplier.code)
  }
  setReseller(reseller) {
      this.setState({ resellerCode: reseller.code })
      this.props.setResellerCode(reseller.code)
  }
  nextPage (direction) {
    let offset = this.state.offset,
        limit = this.state.limit,
        sort = this.state.sort,
        supplierCode = this.state.supplierCode,
        resellerCode = this.state.resellerCode,
        start = this.state.start,
        end = this.state.end;

    if (direction > 0 || offset > 0) {
      offset = (parseInt(offset)+(parseInt(limit) * direction));
    }
    this.setState({offset: offset});
    browserHistory.push(`/app/reports/rejections${(supplierCode !="" ?("/"+supplierCode):"")}${(resellerCode !="" ?("/"+resellerCode):"")}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}&offset=${offset}&limit=${limit}&sort=${sort}`);
  }
  export(format) {
    this.props.export({
      start: this.state.start,
      end: this.state.end,
      resellerCode: this.state.resellerCode,
      supplierCode: this.state.supplierCode
    }, format)
  }
  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }

  nameFilter (name) {
      let output = "";
      output = name.replace('given:"', '');
      output = output.replace('" family:"', ' ');
      output = output.replace('"', '');
      return output;
  }
  shouldDisableStartDate (day) {
    return  !validateDateRange(day.getTime(), self.state.end);
  }
  shouldDisableEndDate (day) {
     return  !validateDateRange(self.state.start, day.getTime());
  }
  parseStatus (code) {
      if (typeof code != 'undefined') {
          return code.replace("STATUS_", "")
      } else {
          return ""
      }
  }
  onRowTap (e, v) {
      e.preventDefault();
      // disabled for now..
  }
  sortBy(column) {
    let start = this.state.start,
        end = this.state.end,
        sort = column,
        offset = this.state.offset,
        limit = this.state.limit,
        supplierCode = this.state.supplierCode,
        resellerCode = this.state.resellerCode;

    if (sort == this.state.sort) {
        if (sort.indexOf("-") > -1) {
          sort = sort.replace("-", "");
        } else {
          sort = "-"+sort;
        }
    }
    this.setState({
        sort: sort
    });
    browserHistory.push(`/app/reports/rejections${(supplierCode !="" ?("/"+supplierCode):"")}${(resellerCode !="" ?("/"+resellerCode):"")}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}&offset=${offset}&limit=${limit}&sort=${sort}`);
  }

  render() {
      let startDate = moment(this.state.start).tz(TZ),
            endDate = moment(this.state.end).tz(TZ),
            dateString = startDate.format("DD-MMM-YYYY"),
            dateEndString = endDate.format("DD-MMM-YYYY"),
            summaryMode = (this.props.children == null || this.props.children.length < 1)

      if (endDate.diff(startDate) > 86400000) { // more than one day
          dateString = `( ${dateString} - ${dateEndString} )`;
      }

    return (
      <div className="all-vouchers">
        <div className="exceptions-header" style={{display: (summaryMode ? "inherit" : "none")}}>
            <div className="exceptions-header-left" style={{marginLeft: "1em"}}>
                <h2 style={{color:"#434343"}}>
                    Rejections
                    <span className="title-date">
                        { dateString }
                    </span>
                </h2>
            </div>
            <div className="exceptions-header-right" style={{marginRight: "1em"}}>
                <div className="no-print detail-view-controls" style={{display: this.props.printMode ? "none" : "inherit"}}>
                    {
                      this.props.role == ACTOUREX_TECHNICAL_STAFF ? (
                        <SuppliersPopover defaultLabel={this.state.supplierCode} onSupplier={supplier => this.setSupplier(supplier)} />
                      ) : ""
                    }
                      <DatePicker
                        floatingLabelText="Start Date"
                        hintText="Date"
                        className="date-picker"
                        formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                        value={this.state.start}
                        shouldDisableDate={this.shouldDisableStartDate}
                        onChange={(ev, date) => this.setStartDate(date)}
                   />
                   <DatePicker
                        floatingLabelText="End Date"
                        style={{marginRight: "1em"}}
                        hintText="Date"
                        className="date-picker"
                        formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                        value={this.state.end}
                        shouldDisableDate={this.shouldDisableEndDate}
                        onChange={(ev, date) => this.setEndDate(date)}
                  />
                  {/* <ExportButton
                    onExport={format => this.export(format)}
                  /> */}
              </div>
              <PrintButton
                 style={{marginLeft: "1em"}}
                 onPrint={data=> { this.print() }} printMode={this.props.printMode}
              />
            </div>
      </div>
      <div className="report-details">
          {this.props.children}
      </div>
         {
            this.props.children == null || this.props.children.length < 1  ? (
               this.props.fetchingVouchers
               ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
               (
                 <section className="inner-report">
                     <Table className="detail-tabulation index">
                         <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                            <TableRow>
                              <TableHeaderColumn className="sortable">
                                <div className="inner" onClick={ e => this.sortBy("operatorId") }>Operator</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                <div className="inner" onClick={ e => this.sortBy("rejectionReason") }>Rejection Reason</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">
                                <div className="inner" onClick={ e => this.sortBy("created") }>Time Created</div>
                              </TableHeaderColumn>
                              <TableHeaderColumn className="sortable">Image</TableHeaderColumn>
                            </TableRow>
                          </TableHeader>
                      <TableBody displayRowCheckbox={false}>
                        {
                          this.props.data && this.props.data.exceptions && this.props.data.exceptions.map((v,i) => {
                            return (
                                <TableRow key={i} onTouchTap={e => { this.onRowTap(e, v) }}>
                                  <TableRowColumn>{`${v.operatorName} (${v.operatorId})`}</TableRowColumn>
                                  <TableRowColumn>{v.rejectionReason}</TableRowColumn>
                                  <TableRowColumn>{!!v.created ? moment.unix(v.created).tz(TZ).format("DD-MMM-YYYY") : "–"}</TableRowColumn>
                                  <TableRowColumn>
                    								<LightBox voucherId={v.voucherId} disabled={false} title="View Image" />
                  								</TableRowColumn>
                  							</TableRow>
                              )
                            }
                          )
                        }
                      </TableBody>
                    </Table>
                    <div className="pagination-controls">
                      {this.state.offset > 0 ?
                        <RaisedButton label="Previous Page" onClick={(e)=>{
                          this.nextPage(-1);
                        }}/> : ""
                      }
                      <div style={{display: "inline-block", marginRight: "1em"}}></div>
                        <RaisedButton label="Next Page" onClick={(e)=>{
                          this.nextPage(1);
                        }}/>
                    </div>
                </section>
              )
          ) : ""
        }
      </div>
    )
  }
}


import { connect } from 'react-redux'
import {
    setPageTitle,
    togglePrintMode
} from '../../ax-redux/actions/app'
import {
  setSupplierCode,
  setResellerCode,
  setStartDate,
  setEndDate
} from '../../ax-redux/actions/report-filters'
import {
    getVoucherImage,
} from '../../ax-redux/actions/vouchers'
import {
  fetch,
  exportCSV,
  exportXLSX
} from '../../ax-redux/actions/voucher-exceptions'
export default connect(
  state => ({
        role: state.app.role,
        resellers: state.resellers.list,
        supplierCode: state.app.user.account.orgCode,
        data: state.voucherExceptions.data,
        fetchingVouchers: state.voucherExceptions.fetching,
        printMode: state.app.printMode,
        voucherImage: state.vouchers.getVoucherImage.data,
        pathname: state.routing.locationBeforeTransitions.pathname,
        queryParams: state.routing.locationBeforeTransitions.search,
        filters: state.reportFilters
    }),
  dispatch => ({
    getVoucherExceptions: (data) => {
        dispatch(fetch(data))
    },
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportCSV(opts))
          return
        case "XLSX":
          dispatch(exportXLSX(opts))
          return
      }
    },
    getVoucherImage: (id) => {
        dispatch(getVoucherImage(id))
    },
    setSupplierCode: data => {
      dispatch(setSupplierCode(data))
    },
    setResellerCode: data => {
      dispatch(setResellerCode(data))
    },
    setStartDate: data => {
      dispatch(setStartDate(data))
    },
    setEndDate: data => {
      dispatch(setEndDate(data))
    }
  })
)(SupplierVoucherExceptions);
