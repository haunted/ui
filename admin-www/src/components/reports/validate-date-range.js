let validateDateRange = (startVal, endVal) => {
    let start = new Date(startVal),
        end = new Date(endVal),
        valid = true;

    if (start.getTime() > end.getTime()) {
        valid = false;
    }
    return valid;
}

export default validateDateRange
