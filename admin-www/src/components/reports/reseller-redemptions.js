import React, { Component, PropTypes } from 'react'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import ExportButton from './export-button'
import PrintButton from './print-button'
import { browserHistory } from 'react-router'
import SupplierMultiSelect from './supplier-multi-select'
import PassIssuersAutoComplete from './pass-issuers-autocomplete'
import { TimestampTitle } from '../datepicker'

import RedeamTable from '../standard/table'
import Progress from '../standard/progress'
import { DateRangePicker, DateRangeTitle, Pagination } from './components/filters'

import {
    isSupplierStaff,
    formatCurrency,
    formatPrice,
    enDash
} from '../../util'

let self = null,
    styles = {
        operatorId: { marginLeft: '1em', maxWidth: '96px' },
        wide: { width: '300px' },
        narrow: { width: '80px' }
    }

class ResellerRedemptions extends Component {
    constructor(props) {
        super(props)

        props.contextFilters(props.params, props.location)
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.version != prevProps.version) {
            this.fetch()
        }
    }

    fetch() {
        let { startDate, endDate, supplierCodes, limit, offset, sort, orgCode } = this.props

        const url = `/app/reports/reseller-redemptions/${
            supplierCodes.join(',')
            }?start=${
            startDate.getUnixTime()
            }&end=${
            endDate.getUnixTime()
            }&offset=${offset}&limit=${limit}&sort=${sort}`

        browserHistory.replace(url);

        this.props.fetch({
            start: startDate.getUnixTime() * 1000,
            end: endDate.getUnixTime() * 1000,
            limit,
            offset,
            supplierCodes: supplierCodes.join(','),
            resellerCode: orgCode,
        })

        this.props.setReturnToResellerRedemptionsURL(url)
    }

    export(format) {
        let { startDate, endDate, supplierCodes, orgCode } = this.props
        this.props.export({
            start: startDate.getUnixTime() * 1000,
            end: endDate.getUnixTime() * 1000,
            supplierCodes: supplierCodes.join(','),
            resellerCode: orgCode,
        }, format)
    }

    print() {
        if (!this.props.printMode) {
            setTimeout(() => {
                window.print()
            }, 500)
        }
        this.props.togglePrintMode()
    }

    rowClicked(event, redemption) {
        if (redemption.passId != "") {
            browserHistory.push(`/app/reports/pass/${redemption.passId}?back=reseller-redemptions`);
        } else if (redemption.ticketId != "") {
            browserHistory.push(`/app/reports/redemption/${redemption.ticketId}?back=reseller-redemptions`);
        }
    }

    render() {
        let summaryMode = (this.props.children == null || this.props.children.length < 1)
        const { supplierCodes, fetching } = this.props
        return (
            <div className="redeemed">
                <div className="redeemed-header" style={{ display: (summaryMode ? "inherit" : "none") }}>
                    <div className="redeemed-header-left">
                        <div>
                            <div>
                                <h2 style={{ color: "#434343" }}>
                                    Redemptions
                                    <DateRangeTitle />
                                </h2>
                            </div>
                            {supplierCodes.length > 0 ?
                                <div style={{ marginBottom: '1em' }}>
                                    <b>Suppliers: </b> {supplierCodes.join(', ')}
                                </div> : null}

                        </div>
                    </div>
                    <div className="redeemed-header-right">
                        <div className={"no-print report-controls" + (this.props.printMode ? " print-mode" : "")}>
                            <DateRangePicker />
                            <SupplierMultiSelect
                                onSuppliers={codes => this.props.setSupplierCodes(codes)}
                                selectedSuppliers={supplierCodes}
                            />
                            <ExportButton onExport={format => this.export(format)}
                                style={{ marginLeft: "1em" }}
                            />
                        </div>
                        <PrintButton onPrint={data => { this.print() }} printMode={this.props.printMode}
                            style={{ marginLeft: "1em" }}
                        />
                    </div>
                </div>
                <div className="report-details">
                    {this.props.children}
                </div>
                <Progress fetching={fetching}>
                    { summaryMode ? (
                        <section style={{ width: '100%' }} className="inner-report">
                            <RedeamTable
                                onSort={(name) => this.props.setSort(name)}
                                onRowClick={(e, v) => this.rowClicked(e, v)}
                                columns={[
                                    { title: 'Supplier', sort: 'supplier' },
                                    { title: 'Reseller Product Name', sort: '' },
                                    { title: 'Reseller Product Code', sort: '' },
                                    { title: 'Redeemed At', sort: '' },
                                    { title: '# of Travellers', sort: 'travelers' },
                                    { title: 'Net Price', sort: 'cost' },
                                ]}
                                data={this.props.redeemed.data ? this.props.redeemed.data : []}
                                rows={[
                                    v => enDash(v.supplier.name),
                                    v => v.tour,
                                    v => !!v.items ? (!!v.items[0] ? v.items[0].resellerProductCode : "") : "",
                                    v => <TimestampTitle ts={v.redemptionTime} />,
                                    v => v.travelers.length,
                                    v => formatPrice(v.net),
                                ]}
                            />
                            <Pagination />
                        </section>
                    ) : ""
                    }
                </Progress>
            </div>
        )
    }
}

// redemptionType determines the label for the redemptionType column
function redemptionType(row) {
    if (!!row.manuallyCreated) {
        return "Manually Added"
    }

    return row.scanned ? "Barcode" : "Voucher Photo"
}

import { connect } from 'react-redux'
import { setPageTitle, togglePrintMode } from '../../ax-redux/actions/app'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../ax-redux/actions/report-filters'
import { fetch, exportCSV, exportXLSX } from '../../ax-redux/actions/redeemed'

import { setReturnToResellerRedemptionsURL } from '../../ax-redux/actions/navigation'
export default connect(
    state => ({
        orgCode: state.app.user.account.orgCode,
        redeemed: state.redeemed,
        fetching: state.redeemed.fetching,
        role: state.app.role,
        adminSupplierId: state.reportFilters.adminSupplierId,
        printMode: state.app.printMode,
        ...state.reportFilters
    }),
    dispatch => ({
        setPageTitle: title => {
            dispatch(setPageTitle(title))
        },
        fetch: (opts) => {
            dispatch(fetch(opts))
        },
        togglePrintMode: (opts) => {
            dispatch(togglePrintMode(opts))
        },
        export: (opts, format) => {
            switch (format) {
                case "CSV":
                    dispatch(exportCSV(opts))
                    return
                case "XLSX":
                    dispatch(exportXLSX(opts))
                    return
            }
        },
        setReturnToResellerRedemptionsURL: url => dispatch(setReturnToResellerRedemptionsURL(url)),
        ...bindActionCreators(filterActions, dispatch),
    })
)(ResellerRedemptions);
