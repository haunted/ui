import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'

class ResellersPopover extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      label: (props.defaultLabel != null && props.defaultLabel != "" ? props.defaultLabel : "Reseller"),
      lookedUpName: false
    }
  }

  componentWillMount() {
    this.props.listResellers()
  }

  componentWillUpdate (nextProps, nextState) {
    let label = '';
    if (nextState.lookedUpName === false && nextProps.resellers.data !== false &&
        nextProps.defaultLabel != null && nextProps.defaultLabel != "")
    {
      nextProps.resellers.data.resellers.map((v, i) => {
        if (v.code == nextProps.defaultLabel) {
          label = v.name;
        }
      })
      if (label != '') {
        this.setState({
          lookedUpName: true,
          label: label
        })
      }
      this.setState({
        lookedUpName: true
      })
    }
  }

  handleTouchTap(event) {
    event.preventDefault()
    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
    console.log(event.currentTarget);
  }

  handleRequestClose() {
    this.setState({
      open: false,
    })
  }

  onMenuItem(e, item, i) {
    this.props.onReseller(item.props.value)
    this.setState({
      open: false,
      label: item.props.value.name
    })
  }

  render() {
    return (
      <div className="reseller-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={() => this.handleRequestClose()}
        >
          <Menu onItemTouchTap={(e, item, i) => this.onMenuItem(e, item, i)}>
            {
                this.props.resellers.fetching ?
                  <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={0.5} /> :
                  !!this.props.resellers.data && this.props.resellers.data.resellers.map(v => <MenuItem key={v.name} primaryText={v.name} value={v} />)
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}
ResellersPopover.propTypes = {
  onReseller: PropTypes.func.isRequired,
  defaultLabel: PropTypes.string
}

import { connect } from 'react-redux'
import { listResellers } from '../../ax-redux/actions/resellers'
export default connect(
  state => {
    return {
      resellers: state.resellers.list,
    }
  },
  dispatch => {
    return {
      listResellers: () => {
          dispatch(listResellers())
      }
    }
  }
)(ResellersPopover)
