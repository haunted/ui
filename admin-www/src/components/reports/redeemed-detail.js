import React, { Component, PropTypes } from 'react'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import DatePicker from 'material-ui/DatePicker'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import moment from 'moment-timezone'
import PrintButton from './print-button'
import LightBox from './lightbox'
import RedemptionsTable from './redemptions-table'
import QueueForReviewButton from '../queue-for-review-button'
import QRCodeButton from './qr-code-button'
import { browserHistory } from 'react-router'
import {
  formatBrokenPrice,
  formatCurrency,
  formatEnum,
  formatStatusEnum,
  decodeUnit,
  capitalize,
  isInternalStaff
} from '../../util'
import { internalHighlight } from '../../styles.js'
class RedeemedDetail extends Component {
  constructor(props) {
    super(props)

    this.state = {
      lightboxMode: false
    }
  }

  componentWillMount() {
    this.props.fetchDetails(this.props.params.id)
    //this.toggleLightbox();
  }

  componentWillReceiveProps ( nextProps ) {

    let ticketDetails = nextProps.redeemed.detailData ? nextProps.redeemed.detailData.ticket : false,
        noImages = !!!ticketDetails || !!!ticketDetails.voucherId;

    if ( this.props.redeemed.detailFetching && !nextProps.redeemed.detailFetching && !!nextProps.redeemed.detailData ) {

      if ( noImages == false && ticketDetails ) {

        this.props.getVoucherImage( ticketDetails.voucherId )

      }

    }

  }

  componentWillUpdate( nextProps, nextState ) {

  }

  print () {

    if ( !this.props.printMode ) {

      setTimeout(() => {
        window.print()
      }, 500);
    }
    
    this.props.togglePrintMode()

  }

  returnToSummary() {
    let supplierCode = this.props.params.supplierCode,
        resellerCode = !!this.props.location.query.resellerCode  ? "/"+this.props.location.query.resellerCode : "",
        operatorId = this.state.operatorId,
        start = this.props.location.query.start,
        limit = this.props.location.query.limit || 100,
        offset = this.props.location.query.offset || 0,
        end = this.props.location.query.end,
        sort = this.props.location.query.sort,
        path = this.props.pathname

    if (path.indexOf('reseller-invoice') > -1) {
      browserHistory.push(`/app/reports/supplier-reseller-invoice/${resellerCode}?start=${start}&end=${end}&offset=${offset}&limit=${limit}&sort=${sort}&supplierCode=${supplierCode}`)
    } else {      
      switch (this.props.location.query.back) {
        case "redeemed":
            browserHistory.push(this.props.returnToRedemptionsURL)
            break
        case "reseller-redemptions":
            browserHistory.push(this.props.returnToResellerRedemptionsURL)
            break
    }
    }
  }
  filterBasicDetails(i, column, columnName, data, values, meta) {
    let displayValue = data,
      displayColumn = columnName,
      internal = false

    if (data == "") {
      return "";
    }
    switch (column) {
      case "totalRetailPrice":
      case "totalNetPrice":
        if (data == undefined || data == null) {
          return ""
        }
        displayValue = formatBrokenPrice(data);
        break;
      case "id":
        displayColumn = 'Redeam Ticket ID'
        internal = true
        if (!this.props.isInternalStaff) {
          return "";
        }
        break;
      case "version":
        return "";
      case "travelDate":
        if (data == " ") {
          displayValue = "–";
        } else {
          displayValue = moment(data).tz(TZ).locale('en').format("DD-MMM-YYYY")
        }
        displayColumn = "Redeemed At";
        break;
      case "status":
        displayValue = formatStatusEnum(data)
        break;
      case "supplierCode":
        displayColumn = 'Supplier Code'
        break
      case "source":

        if ( meta.manuallyCreated ) {

          displayValue = "Manually Created"

        } else {

          displayValue = formatEnum(data)

        }

      break;
      case "resellerCode":
        return "";
      case "resellerName":
        displayColumn = 'Reseller Name';
        if (data != meta.brandName) {
          displayValue = `${meta.brandName} (${data})`
        }
        break
      case "supplierRef":
        displayColumn = 'Supplier Reference Number'
        if (data != null && data != "") {
          if (this.props.isInternalStaff) {
            displayValue = <QRCodeButton lightboxID={columnName} title={data} text={data} />
          }
        } else {
          return ""
        }
        break
      case "resellerRef":
        displayColumn = 'Reseller Reference Number'
        if (data != null && data != "") {
          if (this.props.isInternalStaff) {
            displayValue = <QRCodeButton lightboxID={columnName} title={data} text={data} />
          }
        } else {
          return ""
        }
        break
      case "redemption":
      case "redemptions":
      case "travelers":
      case "travelerProducts":
      case "products":
        return "";
      case "code":
      case "comment":
      case "cancelReason":
        break;
      case "emailId":
        internal = true
        if (!this.props.isInternalStaff) {
          return "";
        }
        break;
      case "voucherId":
        displayColumn = 'Redeam Voucher ID'
        internal = true
        if (!this.props.isInternalStaff) {
          return "";
        }
        break;
      default:
        break;
    }
    return (
      <TableRow key={i} style={internal ? internalHighlight : {}}>
        <TableRowColumn key={column + ":1"}>{capitalize(displayColumn)}</TableRowColumn>,
             <TableRowColumn key={column + ":2"}>
          {displayValue}
        </TableRowColumn>
      </TableRow>
    )
  }

  displayForAdmin(component) {
    if (this.props.isInternalStaff) {
      return component;
    } else {
      return "";
    }
  }
  renderTravelers(data, products, travelerProducts) {
    return (
      <Table selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn>Name</TableHeaderColumn>
            <TableHeaderColumn>Age Band</TableHeaderColumn>
            <TableHeaderColumn>Gender</TableHeaderColumn>
            <TableHeaderColumn>Is Lead</TableHeaderColumn>
            {this.displayForAdmin(
              <TableHeaderColumn style={internalHighlight}>Ref</TableHeaderColumn>
            )}
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            data.map((v, i) => {
              return [<TableRow key={"row:" + i}>
                <TableRowColumn>
                  {v.name != null ? (v.name.given + " " + v.name.family) : " "}
                </TableRowColumn>
                <TableRowColumn>
                  {!!v.travelerType && formatEnum(v.travelerType)}
                </TableRowColumn>
                <TableRowColumn>
                  {!!v.gender && formatEnum(v.gender)}
                </TableRowColumn>
                <TableRowColumn>
                  {v.isLead != null ? (v.isLead ? "Yes" : "No") : " "}
                </TableRowColumn>
                {this.displayForAdmin(
                  <TableRowColumn style={internalHighlight}>
                    {v.ref != null ? v.ref : ""}
                  </TableRowColumn>
                )}
              </TableRow>,
              this.renderTravelerProducts(v.ref, products, travelerProducts, true)]
            })
          }
        </TableBody>
      </Table>
    )
  }
  getProductData(products, ref) {
    let output = {
      title: "",
      supplierCode: ""
    };
    products.map((product) => {
      if (product.ref == ref) {
        output.title = product.title;
        output.supplierCode = product.supplierCode;
      }
    })
    return output;
  }
  renderTravelerProducts(travelerRef, products, data, header) {
    return typeof data == 'object' ? [
      !!header ? (
        <TableRow className="table-sub-header">
          <TableRowColumn>Retail Price</TableRowColumn>
          <TableRowColumn>Product Name</TableRowColumn>
          <TableRowColumn>Supplier Code</TableRowColumn>
        </TableRow>
      ) : "",
      data.map((v, i) => {
        if (v.travelerRef != null && v.travelerRef == travelerRef) {
          let product = this.getProductData(products, v.productRef);
          return (
            <TableRow key={"row:" + i}>
              <TableRowColumn key={":2"}>
                {v.retailPrice != null && formatBrokenPrice(v.retailPrice)}
              </TableRowColumn>
              <TableRowColumn key={":2"}>
                {product.title}
              </TableRowColumn>
              <TableRowColumn key={":2"}>
                {product.supplierCode}
              </TableRowColumn>
            </TableRow>
          )
        }
      })
    ] : "";
  }

  renderItems(data) {
    return (
      <Table selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableRowColumn>Name</TableRowColumn>
            <TableRowColumn>Net Price</TableRowColumn>
            <TableRowColumn>Retail Price</TableRowColumn>
            <TableRowColumn>Supplier Product Code</TableRowColumn>
            <TableRowColumn>Traveler Type</TableRowColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {
            data.map((item, index) => {
              return (
                <TableRow key={index}>
                  <TableRowColumn>
                    {item.name}
                  </TableRowColumn>
                  <TableRowColumn>
                    {item.net ? formatCurrency(item.net.currencyCode, item.net.amount) : "–"}
                  </TableRowColumn>
                  <TableRowColumn>
                    {item.retail ? formatCurrency(item.retail.currencyCode, item.retail.amount) : "–"}
                  </TableRowColumn>
                  <TableRowColumn>
                    {item.supplierProductCode}
                  </TableRowColumn>
                  <TableRowColumn>
                    {item.travelerType}
                  </TableRowColumn>
                </TableRow>
              )
            })
          }
        </TableBody>
      </Table>
    )
  }

  render() {

    let redeemed = this.props.redeemed,
        meta = {},
        details = {},
        manuallyCreated = false,
        productData = [],
        travelerData = [],
        travelerProductData = [],
        itemData = []

    if (!redeemed.detailFetching && !!redeemed.detailData) {

      meta = redeemed.detailData
      details = meta.ticket
      manuallyCreated = meta.manuallyCreated
      productData = details.products
      travelerData = details.travelers
      itemData = meta.redemption.items
      travelerProductData = details.travelerProducts
      // the ticket resellerName is actually the brand,
      // and we will replace it with meta.resellerName
      // also, we want the order to be ResellerName, BrandName
      delete details.resellerName

      if (!!meta.resellerName) {

        details.resellerName = meta.resellerName

      }
      
    }

    return (
      <div className="redeemed">
        <div className="redeemed-header">
          <div className="redeemed-header-left">
            <h2 style={{ color: "#434343" }}>Redeemed Details</h2>
          </div>
          <div className="redeemed-header-right">
            <div style={{ display: (this.props.printMode ? "none" : "inherit") }} className="detail-view-controls">
              <RaisedButton
                onTouchTap={e => { this.returnToSummary() }}
                className="no-print return-to-summary"
                label="Return to Summary"
              />
              {this.props.isInternalStaff && details && details.voucherId != null ?
                <QueueForReviewButton voucherId={details.voucherId} /> : ""
              }
            </div>
            <PrintButton
              className="no-print"
              style={{ marginLeft: "1em" }}
              onPrint={data => { this.print() }} printMode={this.props.printMode}
            />
          </div>
        </div>
        {this.props.redeemed.detailFetching ?
          <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={96} />
          : this.props.redeemed.detailError ? this.props.redeemed.detailError
            : <div>
              <Table selectable={false}>
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                  <TableRow>
                    <TableHeaderColumn>Item</TableHeaderColumn>
                    <TableHeaderColumn>Value</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                  {
                    Object.keys(details).map((v, i) => {
                      return this.filterBasicDetails(i, v, v, details[v], details, meta)
                    })
                  }
                  <TableRow>
                    <TableRowColumn key={":1"}>Voucher Image</TableRowColumn>
                    <TableRowColumn key={":2"}>
                      <LightBox
                        voucherId={details ? details.voucherId : false }
                        disabled={manuallyCreated || !!!details || !!!details.voucherId }
                        title={manuallyCreated ? "Manually Created" : !!!details.voucherId ? "Scanned" : "View Image"} />
                    </TableRowColumn>
                  </TableRow>
                </TableBody>
              </Table>
              <div className="redeemed-header">
                <div className="redeemed-header-left subsection">
                  <h3>Details</h3>
                </div>
              </div>
              {
                (!this.props.redeemed.detailFetching && !!details.redemptions) ?
                  <RedemptionsTable redemptions={[meta.redemption]} /> : ""
              }
              <div className="redeemed-header">
                <div className="redeemed-header-left subsection">
                  <h3>Travelers</h3>
                </div>
              </div>
              {!this.props.redeemed.detailFetching && this.renderTravelers(travelerData, productData, travelerProductData)}

              <div className="redeemed-header">
                <div className="redeemed-header-left subsection">
                  <h3>Items</h3>
                </div>
              </div>
              {!this.props.redeemed.detailFetching && this.renderItems(itemData)}
            </div>
        }
      </div>
    )
  }
}

import { connect } from 'react-redux'
import {
  fetchDetails,
} from '../../ax-redux/actions/redeemed'
import {
  setPageTitle,
  togglePrintMode
} from '../../ax-redux/actions/app'
import {
  getVoucherImage
} from '../../ax-redux/actions/vouchers'

export default connect(
  state => ({
    redeemed: state.redeemed,
    isInternalStaff: isInternalStaff(state.app.role),
    printMode: state.app.printMode,
    voucherImage: state.vouchers.getVoucherImage.data,
    pathname: state.routing.locationBeforeTransitions.pathname,
    returnToRedemptionsURL: state.navigation.returnToRedemptionsURL,
    returnToResellerRedemptionsURL: state.navigation.returnToResellerRedemptionsURL,
  }),
  dispatch => ({
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    fetchDetails: id => {
      dispatch(fetchDetails(id))
    },
    togglePrintMode: opts => {
      dispatch(togglePrintMode(opts))
    },
    getVoucherImage: (id) => {
      dispatch(getVoucherImage(id))
    }
  })
)(RedeemedDetail);
