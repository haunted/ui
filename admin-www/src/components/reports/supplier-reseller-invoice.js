import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import Toggle from 'material-ui/Toggle'
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from './export-button'
import PrintButton from './print-button'
import moment from 'moment-timezone'
import InvoicesPopover from './invoices-popover'
import InvoicesAutocomplete from './invoices-autocomplete'
import SuppliersPopover from './suppliers-popover'
import { DateStringTitle } from '../datepicker'
import { DateRangePicker, DateRangeTitle, Pagination } from './components/filters'

import { ACTOUREX_TECHNICAL_STAFF, ACTOUREX_BUSINESS_STAFF } from '../../ax-redux/constants'
import { formatCurrency } from "../../util"


class SupplierResellerInvoice extends Component {
  constructor(props) {
    super(props)

    props.contextFilters(props.params, props.location)
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.version != prevProps.version) {
      this.fetch()
    }
  }

  fetch() {
    let {startDate, endDate, resellerCode, supplierCode, sort} = this.props
    browserHistory.push(`/app/reports/supplier-reseller-invoice${
      (resellerCode != "" ? ("/" + resellerCode) : "")
      }?start=${startDate.getUnixTime()
      }&end=${endDate.getUnixTime()
      }&sort=${sort
      }${
        this.props.isTechStaff ? `&supplierCode=${supplierCode}` : ""
      }`);

    this.props.getResellerInvoices({
      start: startDate,
      end: endDate,
      resellerCode,
      supplierCode,
      sort,
    });
  }
  export(format) {
    let {startDate, endDate, resellerCode, supplierCode, sort} = this.props
    this.props.export({
      start: startDate,
      end: endDate,
      resellerCode: resellerCode,
      supplierCode: supplierCode,
      endpoint: "/api/finance/supplier-reseller-invoice"
    }, format)
  }
  print() {
    if (!this.props.printMode) {
      setTimeout(() => {
        window.print()
      }, 500);
    }
    this.props.togglePrintMode();
  }
  nameFilter(name) {
    let output = "";
    output = name.replace('given:"', '');
    output = output.replace('" family:"', ' ');
    output = output.replace('"', '');
    return output;
  }

  rowClicked(e, id, data) {
    e.preventDefault()
    let start = this.props.startDate,
      end = this.props.endDate,
      supplierCode = this.props.supplierCode,
      resellerCode = this.props.resellerCode,
      sort = this.props.sort,
      path = `/app/reports/`
    if (data.passId != "") {
      path += `pass/${data.passId}?supplierCode=${supplierCode}&resellerCode=${resellerCode}&start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}&sort=${sort}&back=invoices`
    } else {
      path += `supplier-reseller-invoice/details${(supplierCode != "" ? ("/" + supplierCode) : "")}/id/${id}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}&sort=${sort}`
    }
    browserHistory.push(path)
  }

  render() {
    let summary = this.props.reportData ? this.props.reportData.summary : false

    return (
      <div className="detailed-reseller">
        <div className="exceptions-header">
          <div className="exceptions-header-left">
            <h2 style={{ color: "#434343" }}>
              Reseller Invoices
              <DateRangeTitle />
            </h2>
          </div>
          <div className="exceptions-header-right">
            <div className={"no-print report-controls" + (this.props.printMode ? " print-mode" : "")}>
              {this.props.isTechStaff ?
                <SuppliersPopover defaultLabel={this.props.supplierCode}
                  onSupplier={supplier => this.props.setSupplierCode(supplier.supplier.code)}
                /> : ""}
              <InvoicesAutocomplete supplierCode={this.props.supplierCode}
                onInvoice={invoice => this.props.setResellerCode(invoice.resellerCode)}
                selectDefault={false}
                selectedResellerCode={this.props.resellerCode || ""}
              />
              <DateRangePicker />
              <ExportButton
                onExport={format => this.export(format)}
              />
            </div>
            <PrintButton
              style={{ marginLeft: "1em" }}
              onPrint={data => { this.print() }} printMode={this.props.printMode}
            />
          </div>
        </div>
        {
          this.props.fetchingReport && summary !== false
            ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
            (
              <section className="inner-report">
                <Table className="summary-view index" selectable={false}>
                  <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                      <TableHeaderColumn key="1">Number of Redemptions</TableHeaderColumn>
                      <TableHeaderColumn key="2">Total Sales (Net)</TableHeaderColumn>
                      <TableHeaderColumn key="2">Total Sales (Gross)</TableHeaderColumn>
                      <TableHeaderColumn key="2">Total Due</TableHeaderColumn>
                    </TableRow>
                  </TableHeader>
                  <TableBody displayRowCheckbox={false}>
                    {this.props.reportData ?
                      <TableRow key={"1"}>
                        <TableRowColumn key="1">{summary.totalRedemptions}</TableRowColumn>
                        <TableRowColumn key="2">{formatCurrency(summary.totalNetCurrency, summary.totalNetValue)}</TableRowColumn>
                        <TableRowColumn key="3">{formatCurrency(summary.totalRetailCurrency, summary.totalRetailValue)}</TableRowColumn>
                        <TableRowColumn key="2">{formatCurrency(summary.totalDueCurrency, summary.totalDue)}</TableRowColumn>
                      </TableRow> : ""
                    }
                  </TableBody>
                </Table>
                <Table className="detail-tabulation index" selectable={false}>
                  <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>
                      <TableHeaderColumn className="medium-column-print-only">
                        <div className="inner" onClick={e => this.props.setSort("dateRedeemed")}>Date Redeemed</div>
                      </TableHeaderColumn>
                      <TableHeaderColumn>
                        <div className="inner" style={{ paddingLeft: "0.5em" }} onClick={e => this.props.setSort("resellerName")}>Reseller Name</div>
                      </TableHeaderColumn>
                      <TableHeaderColumn>
                        <div className="inner" onClick={e => this.props.setSort("reference")}>Reseller Reference</div>
                      </TableHeaderColumn>
                      <TableHeaderColumn>
                        <div className="inner" style={{ paddingLeft: "0.5em" }} onClick={e => this.props.setSort("customerName")}>Lead Traveler Name</div>
                      </TableHeaderColumn>
                      <TableHeaderColumn>
                        <div className="inner" onClick={e => this.props.setSort("tourName")}>Product Name</div>
                      </TableHeaderColumn>
                      <TableHeaderColumn className="narrow-column-print-only sortable">
                        <div className="inner" onClick={e => this.props.setSort("totalRetailValue")}>Retail</div>
                      </TableHeaderColumn>
                      <TableHeaderColumn className="narrow-column-print-only sortable">
                        <div className="inner" onClick={e => this.props.setSort("totalNetValue")}>Net</div>
                      </TableHeaderColumn>
                    </TableRow>
                  </TableHeader>
                  <TableBody displayRowCheckbox={false}>
                    {
                      this.props.reportData && this.props.reportData.details && this.props.reportData.details.map((v, i) => {

                        let resellerRef = v.resellerRef != null ? v.resellerRef : "",
                          customerName = this.nameFilter(v.customerName),
                          tourName = v.tourName ? v.tourName : "",
                          cost = formatCurrency(v.totalNetCurrency != "" ? v.totalNetCurrency : "USD", v.totalNetValue)

                        return (
                          <TableRow key={i} onTouchTap={e => this.rowClicked(e, v.ticketId, v)} style={{ cursor: "pointer" }}>
                            <TableRowColumn>
                              <DateStringTitle value={v.dateRedeemed} />
                            </TableRowColumn>
                            <TableRowColumn title={v.resellerName}>
                              {v.resellerName}
                            </TableRowColumn>
                            <TableRowColumn title={resellerRef}>
                              {resellerRef}
                            </TableRowColumn>
                            <TableRowColumn title={customerName}>
                              {customerName}
                            </TableRowColumn>
                            <TableRowColumn title={tourName}>
                              {tourName}
                            </TableRowColumn>
                            <TableRowColumn title={v.totalRetailValue}>
                              {formatCurrency("USD", v.totalRetailValue)}
                            </TableRowColumn>
                            <TableRowColumn title={v.totalNetValue}>
                              {formatCurrency("USD", v.totalNetValue)}
                            </TableRowColumn>
                          </TableRow>
                        )

                      })
                    }
                  </TableBody>
                </Table>
              </section>
            )
        }
      </div>
    )
  }
}
import { connect } from 'react-redux'
import { setPageTitle, togglePrintMode} from '../../ax-redux/actions/app'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../ax-redux/actions/report-filters'
import { getResellerInvoices, exportCSV, exportXLSX} from '../../ax-redux/actions/finances'

export default connect(
  state => ({
    reportData: state.finances.getResellerInvoices.data,
    fetchingReport: state.finances.getResellerInvoices.fetching,
    printMode: state.app.printMode,
    isTechStaff: state.app.role == ACTOUREX_TECHNICAL_STAFF || state.app.role == ACTOUREX_BUSINESS_STAFF,
    ...state.reportFilters
  }),
  dispatch => ({
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    getResellerInvoices: (data) => {
      dispatch(getResellerInvoices(data))
    },
    togglePrintMode: (opts) => {
      dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportCSV(opts))
          return
        case "XLSX":
          dispatch(exportXLSX(opts))
          return
      }
    },
    ...bindActionCreators(filterActions, dispatch),
  })
)(SupplierResellerInvoice);
