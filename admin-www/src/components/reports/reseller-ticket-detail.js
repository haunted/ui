import moment from 'moment-timezone'
import React, { Component, PropTypes } from 'react'
import { browserHistory } from 'react-router';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import RaisedButton from 'material-ui/RaisedButton'
import Toggle from 'material-ui/Toggle';
import Paper from 'material-ui/Paper'
import DatePicker from 'material-ui/DatePicker';
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from './export-button'
import PrintButton from './print-button'
import SuppliersPopover from './suppliers-popover'
import ResellersPopover from './resellers-popover'
import getInclusiveRange from './get-inclusive-range'
import validateDateRange from './validate-date-range'
import formatColumn from './format-column'
import { capitalize, formatPrice } from '../../util.js'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'

let self = null;

class ResellerTicketDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      start: new Date(this.props.location.query.start * 1000),
      end: new Date(this.props.location.query.end * 1000),
      orgCode: this.props.params.supplierCode,
      sort: ""
    }
    self = this;
  }

  componentDidMount() {
    this.props.getResellerTicketDetails({
        start: new Date(this.props.location.query.start * 1000),
        end: new Date(this.props.location.query.end * 1000),
        supplierCode: this.state.orgCode,
        resellerCode: this.props.params.resellerCode,
        sort: this.state.sort
    })
  }
  componentWillUpdate(nextProps, nextState) {
    if (this.state.start != nextState.start ||
       this.state.end != nextState.end ||
       this.state.sort != nextState.sort ||
       this.state.orgCode != nextState.orgCode) {
         browserHistory.push(`/app/reports/top-reseller-ticket-volume${(nextProps.params.supplierCode !="" ?("/"+nextProps.params.supplierCode):"")}${(nextProps.params.resellerCode !="" ?("/"+nextProps.params.resellerCode):"")}?start=${moment(nextState.start).tz(TZ).unix()}&end=${moment(nextState.end).tz(TZ).unix()}&sort=${nextState.sort}`);
     }
     if (this.props.pathname != nextProps.pathname ||
         this.props.queryParams != nextProps.queryParams) {
           let requestParams = {
             start: new Date(this.props.location.query.start * 1000),
             end: new Date(this.props.location.query.end * 1000),
             supplierCode: nextState.supplierCode,
             resellerCode: this.props.params.resellerCode
           };
           if (nextProps.location.query.sort) {
               requestParams.sort = nextProps.location.query.sort;
           }
           this.props.getResellerTicketDetails(requestParams);
     }
  }

  setSupplier(supplier) {
    console.log(supplier)
    this.setState({ orgCode: supplier.supplier.code })
  }

  export(format) {
    this.props.export({
      start: this.state.start,
      end: this.state.end,
      supplierId: this.state.orgCode,
      endpoint: "/api/finance/supplier-top-reseller-ticket-volume"
    }, format)
  }
  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }
  rowClicked(event, id) {
    // This prevents ghost click.
    event.preventDefault()
    console.log("pathname", this.props.pathname);
    browserHistory.push(this.props.pathname+"/ticket/"+id+this.props.queryParams);
  }
  shouldDisableStartDate (day) {
    return  !validateDateRange(day.getTime(), self.state.end);
  }
  shouldDisableEndDate (day) {
     return  !validateDateRange(self.state.start, day.getTime());
  }
  filterHeadings (headings) {
      let out = [];
      headings.map(heading => {
        if (heading != "travelTime" && heading != "bookingId" && heading != "option" &&
            heading != "id" && heading != "voucherId" && heading != "language" && heading != "location" &&
            heading != "operatorId" && heading != "supplierTourCode" && heading != "redemptionTime" && heading != "mobile" ) {
          out.push(heading);
        }
      })
      return out;
  }
  filterColumns (columns) {
      let out = [];
      columns.map(heading => {
        if (heading != "travelTime" && heading != "supplierTourCode" && heading != "redemptionTime" && heading != "mobile" ) {
          out.push(heading);
        }
      })
      return out;
  }
  backToSummary () {
      let path = this.props.pathname,
          summaryRoute = path,
          supplierCode = this.props.params.supplierCode,
          start = this.props.location.query.start,
          end = this.props.location.query.end,
          sort = this.props.location.query.sort;

      if (path.indexOf("dollar-volume") > -1) {
          summaryRoute = "/app/reports/top-reseller-dollar-volume";
      } else if (path.indexOf("ticket-volume") > -1) {
          summaryRoute = "/app/reports/top-reseller-ticket-volume";
      } else {
          summaryRoute = "/app/reports/top-reseller-volume";
      }
      browserHistory.push(`${summaryRoute}/${supplierCode}?start=${start}&end=${end}&sort=${sort}`);
  }
  sortBy(column) {
    let start = this.state.start,
        end = this.state.end,
        sort = column,
        supplierCode = this.props.params.supplierCode,
        resellerCode = this.props.params.resellerCode;
    console.log(supplierCode, resellerCode);
    if (sort == this.state.sort) {
        if (sort.indexOf("-") > -1) {
          sort = sort.replace("-", "");
        } else {
          sort = "-"+sort;
        }
    }
    this.setState({
        sort: sort
    });
    browserHistory.push(`/app/reports/top-reseller-ticket-volume${(supplierCode !="" ?("/"+supplierCode):"")}${(resellerCode !="" ? ("/"+resellerCode):"")}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}&sort=${sort}`);
  }

  render() {
      let fetching = this.props.reportFetching,
          reportData = this.props.reportData,
          startDate = moment(this.props.location.query.start * 1000),
          endDate = moment(this.props.location.query.end * 1000),
          dateString = startDate.format("DD-MMM-YYYY"),
          dateEndString = endDate.format("DD-MMM-YYYY"),
          summaryMode = (this.props.children == null || this.props.children.length < 1);

        if (endDate.diff(startDate) > 86400000 ) { // more than one day
            dateString = `( ${dateString} - ${dateEndString} )`;
        }

    return (
      <div className="top-resellers">
        <div className="exceptions-header" style={{display: (!summaryMode ? "none" : "inherit")}}>
          <div className="exceptions-header-left">
            <h2 style={{color:"#434343"}}>
                Reseller Ticket Details
                <span className="title-date">
                    { dateString }
                </span>
            </h2>
          </div>
          <div className="exceptions-header-right">
          <div className="no-print" style={{display: this.props.printMode ? "none" : "inherit"}}>
              <RaisedButton onTouchTap={e => { this.backToSummary() } }
                            className="no-print"
                            style={{minWidth: "190px"}}
                            label="Return to Summary"
              />
             <ExportButton
                style={{marginLeft: "1em"}}
               onExport={format => this.export(format)}
             />
            </div>
            <PrintButton
               style={{marginLeft: "1em"}}
               onPrint={data=> { this.print() }} printMode={this.props.printMode}
            />
          </div>
      </div>
      <div className="report-details">
          {this.props.children}
      </div>
         {
           fetching ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
           (
               summaryMode ? (
               <Table className="index" >
                 <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                   <TableRow>
                     {!!this.props.reportData && this.props.reportData.length > 0 && this.filterHeadings(Object.keys(this.props.reportData[0])).map((col) =>
                         <TableHeaderColumn key={col} className="sortable">
                           <div className="inner" onClick={ e => this.sortBy(col) }>{capitalize(col)}</div>
                         </TableHeaderColumn>
                     )}
                   </TableRow>
                 </TableHeader>
                 <TableBody displayRowCheckbox={false}>
                   {
                     !!this.props.reportData && this.props.reportData.map((v,i) => (
                     <TableRow key={i} >
                       {
                           this.filterHeadings(Object.keys(v)).map((col) => (
                               <TableRowColumn key={i+":"+col} className={"formatColumn(col)"} onTouchTap={e => this.rowClicked(e, v.id)}>
                                   {
                                      (col == "cost") ? (
                                          formatPrice(v[col])
                                      ) : (
                                          (col == "travelers") ? (
                                              v[col].length
                                          ) : (
                                              v[col]
                                          )
                                      )
                                    }
                               </TableRowColumn>
                           ))
                       }
                     </TableRow>
                    ))
                  }
                </TableBody>
              </Table>
            ) : ""
          )
        }
      </div>
    )
  }
}

import { connect } from 'react-redux'
import {
    setPageTitle,
    togglePrintMode
} from '../../ax-redux/actions/app'
import {
    getResellerTicketDetails,
    exportCSV,
    exportXLSX
} from '../../ax-redux/actions/finances'

export default connect(
  state => ({
      reportData: state.finances.getResellerTicketDetails.data &&
                  state.finances.getResellerTicketDetails.data.tickets ? state.finances.getResellerTicketDetails.data.tickets : [],
      reportFetching: state.finances.getResellerTicketDetails.fetching,
      pathname: state.routing.locationBeforeTransitions.pathname,
      queryParams: state.routing.locationBeforeTransitions.search,
      role: state.app.role,
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      printMode: state.app.printMode
  }),
  dispatch => ({
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    getResellerTicketDetails: params => {
      dispatch(getResellerTicketDetails(params))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportCSV(opts))
          return
        case "XLSX":
          dispatch(exportXLSX(opts))
          return
      }
    }
  })
)(ResellerTicketDetail);
