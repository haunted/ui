/* globals window */

import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import qs from 'querystring';
import CircularProgress from 'material-ui/CircularProgress';
import RaisedButton from 'material-ui/RaisedButton';
import PrintButton from './print-button';
import QueueForReviewButton from '../queue-for-review-button';
import TicketTable from './components/TicketTable';
import TravelersTable from './components/TravelersTable';
import { isInternalStaff } from '../../util';
import * as arrivalsActions from '../../ax-redux/actions/arrivals';
import * as appActions from '../../ax-redux/actions/app';


class ArrivalsDetail extends Component {
  constructor(props) {
    super(props);

    this.print = this.print.bind(this);
    this.returnToSummary = this.returnToSummary.bind(this);
  }

  componentWillMount() {
    const {
      fetchDetails,
      params,
    } = this.props;

    // TODO handle undefined ids
    fetchDetails(params.id);
  }

  print() {
    const {
      printMode,
      togglePrintMode,
    } = this.props;

    if (!printMode) {
      setTimeout(() => {
        window.print();
      }, 500);
    }

    togglePrintMode();
  }

  returnToSummary() {
    const {
      pathname,
      params: {
        supplierCode,
        resellerCode,
      },
      location: {
        query: {
          start,
          end,
          sort,
        },
      },
    } = this.props;

    let summaryRoute = pathname;

    const queryString = qs.stringify({ start, end, sort });

    if (pathname.indexOf('arrivals') > -1) {
      browserHistory.push(`/app/reports/arrivals/${supplierCode}?${queryString}`);
    } else {
      if (pathname.indexOf('dollar-volume') > -1) {
        summaryRoute = '/app/reports/top-reseller-dollar-volume';
      } else if (pathname.indexOf('ticket-volume') > -1) {
        summaryRoute = '/app/reports/top-reseller-ticket-volume';
      } else {
        summaryRoute = '/app/reports/top-reseller-volume';
      }
      browserHistory.push(`${summaryRoute}/${supplierCode}/${resellerCode}?${queryString}`);
    }
  }

  renderHeaderNavigation() {
    const {
      printMode,
      isInternal,
      arrivals,
    } = this.props;

    const { ticket } = arrivals.detailData;

    return (
      <div className="exceptions-header">
        <div className="exceptions-header-left">
          <h2 style={{ color: '#434343' }}>Arrival Details</h2>
        </div>
        <div className="exceptions-header-right">
          <div
            style={{ display: (printMode ? 'none' : 'block') }}
            className="detail-view-controls"
          >
            <RaisedButton
              onTouchTap={this.returnToSummary}
              className="no-print return-to-summary"
              label="Return to Summary"
            />

            {isInternal && ticket.voucherId && (
              <QueueForReviewButton voucherId={ticket.voucherId} />
            )}
          </div>
          <PrintButton
            className="no-print"
            style={{ marginLeft: '1em' }}
            onPrint={this.print}
            printMode={printMode}
          />
        </div>
      </div>
    );
  }

  render() {
    const { arrivals, isInternal } = this.props;

    if (arrivals.detailFetching) {
      return (
        <div className="exceptions">
          <CircularProgress className="exceptions-circular-progress" color="#27367a" size={96} />
        </div>
      );
    }

    return (
      <div className="exceptions">
        {arrivals.detailData ? (
          <div>
            {this.renderHeaderNavigation()}
            <TicketTable arrivals={arrivals} isInternal={isInternal} />
            <TravelersTable arrivals={arrivals} isInternal={isInternal} />
          </div>
        ) : (
          <div>No data loaded</div>
        )}
      </div>
    );
  }
}

ArrivalsDetail.propTypes = {
  fetchDetails: PropTypes.func.isRequired,
  togglePrintMode: PropTypes.func.isRequired,
  isInternal: PropTypes.bool.isRequired,
  printMode: PropTypes.bool.isRequired,
  arrivals: PropTypes.shape({}).isRequired,
  params: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  location: PropTypes.shape({
    query: PropTypes.object,
  }).isRequired,
  pathname: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => {
  const { arrivals } = state;

  // the ticket resellerName is actually the brand,
  // and we will replace it with meta.resellerName
  if (arrivals.detailData) {
    arrivals.detailData.ticket.resellerName = arrivals.detailData.resellerName;
  }

  return {
    arrivals,
    isInternal: isInternalStaff(state.app.role),
    printMode: state.app.printMode,
    pathname: state.routing.locationBeforeTransitions.pathname,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({
  ...arrivalsActions,
  ...appActions,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ArrivalsDetail);
