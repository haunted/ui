import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import {Card, CardHeader, CardText} from 'material-ui/Card'
import DatePicker from 'material-ui/DatePicker';
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from './export-button'
import PrintButton from './print-button'
import TourFilters from './tour-filters'
import moment from 'moment-timezone'
import SuppliersPopover from './suppliers-popover'
import { browserHistory } from 'react-router'

import {DateRangeTitle, TZDatePicker, TimestampTitle} from '../datepicker'


class ArrivalsTable extends Component {
  constructor(props) {
    super(props)
    
    props.setHiddenTours(this.queryTours())
    props.contextFilters(props.params, props.location)    
  }

  componentDidUpdate(prevProps, prevState) {
      if (this.props.version != prevProps.version) {
        this.fetch()
      } else if (prevProps.hiddenTours.length != this.props.hiddenTours.length) {
        this.pushURL()
      }
  }

  queryTours () {
    let { products = "" } = this.props.location.query
    
    return decodeURIComponent(products).split(",")
  }

  pushURL() {
    let productFilter = "",
      hiddenTours = this.props.hiddenTours      
    if (hiddenTours.length > 0) {
      productFilter = `&products=${encodeURIComponent(hiddenTours.join(","))}`;
    }
    let { supplierCode, startOfDay, sort } = this.props
    browserHistory.push(`/app/reports/arrivals${(supplierCode != "" ? ( "/" + supplierCode) : "")
    }?day=${
      startOfDay.getUnixTime()
    }&sort=${
      sort
    }${productFilter}`);
  }

  fetch() {
    this.pushURL()
    this.props.fetch({
      start: this.props.startOfDay,
      end: this.props.endOfDay,
      supplierCode: this.props.supplierCode,
      sort: this.props.sort,
    })
  }

  tourState(tour) {    
    return !!! this.props.hiddenToursDict[tour]
  }

  handleTourFilterChange (activeTours) {
    let hiddenTours = {}
    
    // dictionary of all tours
    this.props.tours.map(v => {      
      hiddenTours[v] = true
    })
    
    // delete selected tours
    activeTours.map(v => {
      delete hiddenTours[v] 
    })

    // set hidden tours
    this.props.setHiddenTours(Object.keys(hiddenTours))
  }

  export(format) {
    this.props.export({
      start: this.props.startOfDay,
      end: this.props.endOfDay,
      supplierCode: this.props.supplierCode,
    }, format)
  }
  
  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }

  rowClicked(event, id, arrival) {
    event.preventDefault() // This prevents ghost click.
    let supplierCode = this.props.supplierCode,
        start = this.props.startOfDay,
        sort = this.props.sort

    if (arrival.passId != '') {
      browserHistory.push(`/app/reports/pass/${arrival.passId}?supplierCode=${supplierCode}&start=${moment(start).tz(TZ).unix()}&sort=${sort}&back=arrivals`);
    } else if (id != '') {
      browserHistory.push(`/app/reports/arrivals${(supplierCode !="" ? ("/"+supplierCode):"")}/${id}?start=${moment(start).tz(TZ).unix()}&sort=${sort}`);
    }
  }

  renderArrivalsTables() {
    let tours = [...this.props.tours]
     tours.sort()
      return tours.map((tour, t) => {
        if (this.tourState(tour) === false) {
          return ""
        }
        let dataByTour = this.props.dataByTour[tour],
            tickets = dataByTour.length,
            hasLanguage = false,
            tourName = ''

        dataByTour.map(t => {
          if (t.tour != '') {
            tourName = t.tour
          }
          if (t.language != '') {
            hasLanguage = true
          }
        })
        return (
          <Card key={t}>
            <CardHeader
              title={`${tourName != '' ? tourName : tour} ${tour == 'No Tour Code' ? '(No Supplier Tour Code)' : ''}`}
              subtitle={`${tickets} Ticket${tickets > 1 ? 's' : ''}`}
              actAsExpander={true}
              showExpandableButton={true}
            />
            <CardText expandable={true}>
              <Table className="index" selectable={false}>
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                  <TableRow>
                    <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={ e => this.props.setSort("name") }>Lead Traveler Name</div>
                    </TableHeaderColumn>
                    <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={ e => this.props.setSort("mobile") }>Lead Traveler Telephone</div>
                    </TableHeaderColumn>
                    <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={ e => this.props.setSort("tour") }>Product Name</div>
                    </TableHeaderColumn>
                    <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={ e => this.props.setSort("travelers") }># of Travelers</div>
                    </TableHeaderColumn>
                    <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={ e => this.props.setSort("date") }>Date</div>
                    </TableHeaderColumn>
                { hasLanguage ? (
                      <TableHeaderColumn className="sortable">
                          <div className="inner" onClick={ e => this.props.setSort("language") }>Language</div>
                        </TableHeaderColumn>
                     
                      ) : ''
                    }
                  </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                  {
                    dataByTour.map((v,i) => (
                      <TableRow key={i} onTouchTap={e => this.rowClicked(e, v.ticketId, v)}>
                        <TableRowColumn title={v.name}>
                            {v.name}
                        </TableRowColumn>
                        <TableRowColumn title={v.mobile}>
                            {v.mobile}
                        </TableRowColumn>
                        <TableRowColumn title={v.tour}>
                            {v.tour}
                        </TableRowColumn>
                        <TableRowColumn>
                            {v.tickets}
                        </TableRowColumn>
                        <TableRowColumn>
                          <TimestampTitle ts={v.travelTime} />
                        </TableRowColumn>
                        { hasLanguage ? (
                          
                            <TableRowColumn>
                              {v.language}
                            </TableRowColumn>
                          
                          ) : ''
                        }
                      </TableRow>
                  ))
                }
              </TableBody>
            </Table>
          </CardText>
        </Card>
      )
    })
  }

  render() {
    let summaryMode = (this.props.children == null || this.props.children.length < 1)

    return (
      <div className="arrivals">
        <div className="arrivals-header" style={{display: (summaryMode ? "inherit" : "none")}}>
          <div className="arrivals-header-left">
            <h2 style={{color:"#434343"}}>
                Expected Arrivals
                <DateRangeTitle 
                  start={this.props.startOfDay} 
                  end={this.props.endOfDay}
                />
            </h2>
           </div>
           <div className="arrivals-header-right">
           <div className={"no-print report-controls"+ (this.props.printMode?" print-mode" : "")}>
               <TZDatePicker
                  value={this.props.startOfDay}
                  onChange={date => this.props.setDay(date)}
                />
                {
                  this.props.role == ACTOUREX_TECHNICAL_STAFF ?
                  <SuppliersPopover defaultLabel={this.props.supplierCode}
                                    onSupplier={supplier => this.props.setSupplierCode(supplier.supplier.code)} /> : []
                }

                 <TourFilters 
                   tours={this.props.tours}
                   tourState={tour => this.tourState(tour)}
                   onSetSelectedTours={(tours) => { this.handleTourFilterChange(tours); }} />
                 <ExportButton
                   onExport={format => this.export(format)}
                 />
             </div>
             <PrintButton
                style={{marginLeft: "1em"}}
                onPrint={data=> { this.print() }} printMode={this.props.printMode}
             />
           </div>
         </div>
         <div className="report-details">
             {!summaryMode ? this.props.children : ""}
         </div>
        { this.props.fetching ? <CircularProgress className="arrivals-circular-progress" color="#27367a" size={96} /> :
              summaryMode ?	(	
                <div className="arrivals-tables">		
                    { this.renderArrivalsTables() }		
                </div>		
              ) : "" 
        }
      </div>
    )
  }
}

import { connect } from 'react-redux'
import {
  fetch,
  exportCSV,
  exportXLSX,
  setHiddenTours
} from '../../ax-redux/actions/arrivals'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../ax-redux/actions/report-filters'

import {
    togglePrintMode
} from '../../ax-redux/actions/app'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'

export default connect(
  state => ({
      fetching: state.arrivals.fetching,
      dataByTour: state.arrivals.dataByTour,
      hiddenTours: state.arrivals.hiddenTours,
      hiddenToursDict: state.arrivals.hiddenToursDict,
      tours: state.arrivals.tours,
      role: state.app.role,      
      printMode: state.app.printMode,      
      ...state.reportFilters
  }),
  dispatch => ({
    fetch: (opts) => {
      dispatch(fetch(opts))
    },
    setHiddenTours: (tours) => {
        dispatch(setHiddenTours(tours))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportCSV(opts))
          return
        case "XLSX":
          dispatch(exportXLSX(opts))
          return
      }
    },
    ...bindActionCreators(filterActions, dispatch),
  })
)(ArrivalsTable);
