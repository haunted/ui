import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'

class SuppliersPopover extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      label: (props.defaultLabel != null && props.defaultLabel != "" ? props.defaultLabel : "Supplier"),
      lookedUpName: false
    }
  }

  componentWillMount() {
    if (!this.props.suppliers && !this.props.fetching) {
      this.props.listSuppliers()
    }
  }

  componentWillUpdate (nextProps, nextState) {
    let label = '';
    if (nextState.lookedUpName === false && nextProps.suppliers !== false &&
        nextProps.defaultLabel != null && nextProps.defaultLabel != "")
    {
      nextProps.suppliers.map((v, i) => {
        if (v.supplier.code == nextProps.defaultLabel) {
          label = v.supplier.name;
        }
      })
      if (label.length > 0) {
        this.setState({
          label
        })
      }
      this.setState({
        lookedUpName: true
      })
    }
  }

  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
    console.log(event.currentTarget);
  }

  handleRequestClose() {
    this.setState({
      open: false,
    })
  }

  onMenuItem(e, item, i) {
    this.props.onSupplier(item.props.value)
    this.setState({
      open: false,
      label: item.props.value.supplier.name,
    })
  }

  render() {
    return (
      <div className="supplier-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={() => this.handleRequestClose()}
        >
          <Menu onItemTouchTap={(e, item, i) => this.onMenuItem(e, item, i)}>
            {
                this.props.suppliers ?
                  this.props.suppliers.map((v, i) => <MenuItem key={i} primaryText={v.supplier.name} value={v} />) :
                  <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={32} />
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}
SuppliersPopover.propTypes = {
  onSupplier: PropTypes.func.isRequired,
  defaultLabel: PropTypes.string
}

import { connect } from 'react-redux'
import { listSuppliers } from '../../ax-redux/actions/suppliers'
export default connect(
  state => {
    return {
      suppliers: state.suppliers.list.data ? state.suppliers.list.data.suppliers : false,
      fetching: state.suppliers.list.fetching,
    }
  },
  dispatch => {
    return {
      listSuppliers: () => {
          dispatch(listSuppliers())
      }
    }
  }
)(SuppliersPopover)
