import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'

class SupplierMultiSelect extends Component {
  constructor(props) {
    super(props)

    this.state = {
      open: false,
    }
  }

  componentWillMount() {
    if (!this.props.suppliers && !this.props.fetching) {
      this.props.listSuppliers()
    }
  }

  handleTouchTap(e) {
    e.preventDefault()

    this.setState({
      open: true,
      anchorEl: e.currentTarget,
    })
  }

  onMenuItem(e, item, i) {
    const v = item.props.value
    const { selectedSuppliers } = this.props

    let selectedSupplierDict = arrayValuesToKeys(selectedSuppliers)
    if (!!selectedSupplierDict[v]) {
      delete selectedSupplierDict[v]
    } else {
      selectedSupplierDict[v] = true
    }

    this.props.onSuppliers(Object.keys(selectedSupplierDict))

    this.setState({
      open: false,
    })
  }

  render() {
    const { fetching, suppliers, selectedSuppliers } = this.props
    const label = selectedSuppliers.length == 0 ? 'Suppliers' : `Suppliers (${selectedSuppliers.length} selected)`

    let selectedSupplierDict = arrayValuesToKeys(selectedSuppliers)

    let selectedSupplierObjects = []
    let unselectedSupplierObjects = []

    if (!! suppliers && suppliers.length != 0) {
      suppliers.forEach(
        v => {
          if (selectedSupplierDict[v.supplier.code]) {
            selectedSupplierObjects.push(v)
          } else {
            unselectedSupplierObjects.push(v)
          }
        }
      ) 
    }

    return (
      <div className="supplier-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={() => this.setState({ open: false })}
          >
          <Menu multiple onItemTouchTap={(e, item, i) => this.onMenuItem(e, item, i)}>
            { 
              fetching ? 
                <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={32} />
                : 
                [
                  selectedSupplierObjects.map((v, i) =>
                    <MenuItem 
                      key={`s${i}`} 
                      primaryText={v.supplier.name} 
                      value={v.supplier.code} 
                      style={itemSelectedStyle}
                    />
                ),
                unselectedSupplierObjects.map((v, i) =>
                  <MenuItem 
                    key={`u${i}`} 
                    primaryText={v.supplier.name} 
                    value={v.supplier.code} 
                  />
                )
              ]
            }
          </Menu>
        </Popover>
      </div>
    )
  }
}

function arrayValuesToKeys(arr) {
  let o = {}
  for (let i = 0; i < arr.length; ++i) {
    o[arr[i]] = true
  }
  return o
}

const itemSelectedStyle = { borderLeft: '5px solid #27367a' }

SupplierMultiSelect.propTypes = {
  onSuppliers: PropTypes.func.isRequired,
  defaultLabel: PropTypes.string
}

import { connect } from 'react-redux'
import { listSuppliers } from '../../ax-redux/actions/suppliers'
export default connect(
  state => {
    return {
      suppliers: state.suppliers.list.data ? state.suppliers.list.data.suppliers : false,
      fetching: state.suppliers.list.fetching,
    }
  },
  dispatch => {
    return {
      listSuppliers: () => {
        dispatch(listSuppliers())
      }
    }
  }
)(SupplierMultiSelect)
