import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

class QRCodeButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      enabled: false,
      firstTime: true
    }
  }

  componentDidMount () {
    //   setTimeout(()=>{
    //
    // }, 250);
  }

  toggleLightbox () {
    this.setState({
        enabled: !this.state.enabled
    })
    if (this.state.firstTime) {
        let lightBoxID = "ticket-qrcode-"+(this.props.lightboxID != null ? this.props.lightboxID : this.props.title),
            qrcode = new QRCode(document.getElementById(lightBoxID), {
              text: this.props.text,
              width: 256,
              height: 256,
              colorDark : "#000000",
              colorLight : "#ffffff",
              correctLevel : QRCode.CorrectLevel.H
          });
    }
    this.setState({
        firstTime: false
    })
  }

  render() {
      let lightBoxID = "ticket-qrcode-"+(this.props.lightboxID != null ? this.props.lightboxID : this.props.title);
          return (
              <div className="qr-code-button">
                  <RaisedButton
                        onTouchTap={e => { this.toggleLightbox() } }
                        className="no-print"
                        label={this.props.title}
                        style={this.props.style}
                   />
                  <div className="ticket-lightbox"
                       title="Close"
                       style={{display: this.state.enabled ? "block" : "none"}}
                       onClick={(evt) => { this.toggleLightbox() }}
                  >
                      <div className="inner-lightbox" id={lightBoxID} >
                        <div className="close-lightbox"
                             title="Close"
                             onClick={(evt) => { this.toggleLightbox() }}>
                             x
                        </div>
                      </div>
                  </div>
              </div>
          )
  }
}

QRCodeButton.propTypes = {
  text: PropTypes.oneOfType([
    React.PropTypes.number,
    React.PropTypes.string
  ]).isRequired,
  title: PropTypes.oneOfType([
    React.PropTypes.number,
    React.PropTypes.string
  ]).isRequired,
  style: PropTypes.object
}

export default QRCodeButton
