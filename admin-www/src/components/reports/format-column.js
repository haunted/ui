let formatColumn = (c) => {
    return (
        (c == "tour"   || c == "tourName" ? " wide-tour-column" : "") +
        (c == "date" ? " medium-narrow-column" : "") +
        (c == "name" ? " medium-column" : "") +
        (c == "operatorId" ? " medium-column" : "") +
        (c == "source" || c == "option"   || c == "tickets"    || c == "mobile"
                       || c == "cost"     || c == "operatorId" || c == "resellerCode"
                       || c == "language" || c == "location" ? " narrow-column" : "")

    );
}

export default formatColumn;
