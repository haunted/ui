/* globals TZ */

import React, { Component, PropTypes } from 'react';
import moment from 'moment-timezone';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import QrCodeButton from '../components/QrCodeButton';
import LightBox from '../lightbox';
import {
  formatBrokenPrice,
  capitalize,
  formatEnum,
  formatStatusEnum,
} from '../../../util';
import { internalHighlight } from '../../../styles';


class TicketTable extends Component {
  constructor(props) {
    super(props);

    this.filterBasicDetails = this.filterBasicDetails.bind(this);
  }

  filterBasicDetails(columnName, value, details) {
    const { arrivals, isInternal } = this.props;
    const hasRedemption = !!arrivals.detailData.redemption;

    let displayValue = value;
    let displayColumn = columnName;
    let internal = false;

    if (!value) {
      return '';
    }

    switch (columnName) {
      case 'totalRetailPrice':
      case 'totalNetPrice':
        displayValue = value ? formatBrokenPrice(value) : '–';

        break;
      case 'id':
        internal = true;

        if (!isInternal) {
          return '';
        }

        break;
      case 'version':
        return '';
      case 'status':
        displayValue = formatStatusEnum(value);

        break;
      case 'source':
        if (details.manuallyCreated) {
          displayValue = 'Manually Created';
        } else {
          displayValue = formatEnum(value);
        }

        break;
      case 'travelDate':
        if (value === ' ') {
          displayValue = '–';
        } else {
          displayValue = moment(value).tz(TZ).locale('en').format('DD-MMM-YYYY');
        }

        if (hasRedemption) {
          displayColumn = 'Travel Date';
        } else if (moment(value).unix() > moment(new Date()).unix()) {
          displayColumn = 'Expected Arrival Date';
        } else {
          displayColumn = 'Redeemed At';
        }

        break;
      case 'supplierRef':
        displayColumn = 'Supplier Reference Number';

        if (value) {
          if (isInternal) {
            displayValue = (
              <QrCodeButton title={value} text={value} />
            );
          }
        } else {
          return '';
        }

        break;
      case 'resellerRef':
        displayColumn = 'Reseller Reference Number';

        if (value) {
          if (isInternal) {
            displayValue = (
              <QrCodeButton title={value} text={value} />
            );
          }
        } else {
          return '';
        }

        break;
      case 'resellerCode':
        return '';
      case 'resellerName':
        displayColumn = 'Reseller Name';

        if (value !== details.brandName) {
          displayValue = `${details.brandName} (${value})`;
        }

        break;
      case 'travelers':
      case 'travelerProducts':
      case 'products':
        return '';
      case 'redemption':
        return '';
      case 'redemptions':
        return '';
      case 'code':
      case 'comment':
      case 'cancelReason':
        break;
      case 'emailId':
        internal = true;

        if (!isInternal) {
          return '';
        }

        break;
      case 'voucherId':
        internal = true;

        if (!isInternal) {
          return '';
        }

        break;
      default:
        break;
    }

    return (
      <TableRow key={columnName} style={internal ? internalHighlight : {}}>
        <TableRowColumn>{capitalize(displayColumn)}</TableRowColumn>
        <TableRowColumn>{displayValue}</TableRowColumn>
      </TableRow>
    );
  }

  render() {
    const { arrivals } = this.props;

    const {
      ticket,
      ...details
    } = arrivals.detailData;

    const viewImageLabel = ticket.voucherId ? 'View Image' : 'Scanned';

    return (
      <Table selectable={false}>
        <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
          <TableRow>
            <TableHeaderColumn key="1">Item</TableHeaderColumn>
            <TableHeaderColumn key="2">Value</TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody displayRowCheckbox={false}>
          {Object.keys(ticket).map(key => this.filterBasicDetails(key, ticket[key], details))}

          {details.redemption && (
            <TableRow>
              <TableRowColumn>Voucher Image</TableRowColumn>
              <TableRowColumn>
                <LightBox
                  voucherId={ticket.voucherId}
                  disabled={!ticket.voucherId || details.manuallyCreated}
                  title={details.manuallyCreated ? 'Manually Created' : viewImageLabel}
                />
              </TableRowColumn>
            </TableRow>
          )}
        </TableBody>
      </Table>
    );
  }
}

TicketTable.propTypes = {
  arrivals: PropTypes.shape({}).isRequired,
  isInternal: PropTypes.bool.isRequired,
};

TicketTable.defaultProps = {};

export default TicketTable;
