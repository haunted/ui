import React, { Component, PropTypes } from 'react';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import { internalHighlight } from '../../../styles';
import {
  formatBrokenPrice,
  decodeUnit,
  formatEnum,
} from '../../../util';

class TravelersTable extends Component {
  renderTravelerProducts(travelerRef) {
    const { arrivals } = this.props;
    const {
      redemption,
      ticket: {
        products,
        travelerProducts,
      },
    } = arrivals.detailData;

    // TODO find out if this check is needed here
    if (typeof travelerProducts !== 'object') {
      return '';
    }

    const travelerProduct = travelerProducts.find(p => p.travelerRef === travelerRef);
    const product = redemption ?
      redemption.items.find(r => r.travelerRef === travelerRef) :
      products.find(p => p.ref === travelerProduct.productRef);

    return [
      <TableRow className="table-sub-header">
        <TableRowColumn>Retail Price</TableRowColumn>
        <TableRowColumn>Title</TableRowColumn>
        <TableRowColumn>{redemption ? 'Traveler Type' : 'Supplier Code'}</TableRowColumn>
      </TableRow>,
      product && (
        <TableRow key={`travelerProduct:${travelerProducts.ref}`}>
          <TableRowColumn>
            {travelerProduct.retailPrice && formatBrokenPrice(travelerProduct.retailPrice)}
          </TableRowColumn>
          <TableRowColumn>
            {redemption ? product.name : product.title}
          </TableRowColumn>
          <TableRowColumn>
            {redemption ? decodeUnit(product.travelerType) : product.supplierCode}
          </TableRowColumn>
        </TableRow>
      ),
    ];
  }

  render() {
    const {
      arrivals,
      isInternal,
    } = this.props;

    const {
      ticket: {
        travelers,
      },
    } = arrivals.detailData;

    return (
      <div>
        <div className="redeemed-header">
          <div className="redeemed-header-left">
            <h3>Travelers</h3>
          </div>
        </div>

        <Table selectable={false}>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow>
              <TableHeaderColumn>Name</TableHeaderColumn>
              <TableHeaderColumn>Age Band</TableHeaderColumn>
              <TableHeaderColumn>Gender</TableHeaderColumn>
              <TableHeaderColumn>Is Lead</TableHeaderColumn>
              {isInternal && (
                <TableHeaderColumn style={internalHighlight}>Ref</TableHeaderColumn>
              )}
              <TableHeaderColumn>Weight</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {travelers.map(t => [
              <TableRow key={`traveler:${t.ref}`}>
                <TableRowColumn>
                  {t.name ? (`${t.name.given} ${t.name.family}`) : ' '}
                </TableRowColumn>
                <TableRowColumn>
                  {t.travelerType && formatEnum(t.travelerType)}
                </TableRowColumn>
                <TableRowColumn>
                  {t.gender && formatEnum(t.gender)}
                </TableRowColumn>
                <TableRowColumn>
                  {t.isLead ? 'Yes' : 'No'}
                </TableRowColumn>
                {isInternal && (
                  <TableRowColumn style={internalHighlight}>{t.ref}</TableRowColumn>
                )}
                <TableRowColumn>
                  {`${t.weight ? t.weight.value : ''} ${t.weight ? formatEnum(t.weight.units) : ''}`}
                </TableRowColumn>
              </TableRow>,
              this.renderTravelerProducts(t.ref),
            ])}
          </TableBody>
        </Table>
      </div>
    );
  }
}

TravelersTable.propTypes = {
  arrivals: PropTypes.shape({}).isRequired,
  isInternal: PropTypes.bool.isRequired,
};

export default TravelersTable;
