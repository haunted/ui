import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import * as filterActions from '../../../ax-redux/actions/report-filters'
import { bindActionCreators } from 'redux'
import RaisedButton from 'material-ui/RaisedButton'

// connectFilters is used to connect any of these report-driving components
// to the report filters redux
const connectFilters = component => connect(
    state => ({
        ...state.reportFilters
    }),
    dispatch => ({
        ...bindActionCreators(filterActions, dispatch),
    })
)(component);

// Pagination buttons
const _Pagination = (props) => (
    <div className="pagination-controls">
        {
            props.offset > 0 ?
                <RaisedButton
                    label="Previous Page"
                    onClick={(e) => props.prevPage()}
                />
                : null
        }
        <div style={{ display: "inline-block", marginRight: "1em" }} />
        <RaisedButton
            label="Next Page"
            onClick={(e) => props.nextPage()}
        />
    </div>
)

export const Pagination = connectFilters(_Pagination)



// Date Range Picker
import __DateRangePicker, { DateRangeTitle as __DateRangeTitle } from '../../datepicker'
const _DateRangePicker = props =>
    <__DateRangePicker
        onStartDate={d => props.setStartDate(d)}
        onEndDate={d => props.setEndDate(d)}
        start={props.startDate}
        end={props.endDate}
    />
export const DateRangePicker = connectFilters(_DateRangePicker)

// Date Range Title
const _DateRangeTitle = props =>
    <__DateRangeTitle
        start={props.startDate}
        end={props.endDate}
    />
export const DateRangeTitle = connectFilters(_DateRangeTitle)

const _SupplierMultiSelect = props => {

}
export const SupplierMultiSelect = connectFilters(_SupplierMultiSelect)
