import React, { Component, PropTypes } from 'react'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'

import TextField from 'material-ui/TextField'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import ExportButton from './export-button'
import PrintButton from './print-button'
import { browserHistory } from 'react-router'

import SuppliersPopover from './suppliers-popover'
import ResellersAutocomplete from './resellers-autocomplete'
import  { TimestampTitle } from '../datepicker'
import { DateRangePicker, DateRangeTitle, Pagination } from './components/filters'

import {
  formatPrice,  
  isSupplierStaff
} from '../../util'

class RedeemedTable extends Component {
  constructor(props) {
    super(props)

    props.contextFilters(props.params, props.location)
  }
  
  componentDidUpdate(prevProps, prevState) {
    if (this.props.version != prevProps.version) {
      this.fetch()
    }
  }
  
  fetch() {
    let { startDate, endDate, resellerCode, supplierCode, operatorId, limit, offset, sort } = this.props

    const url = `/app/reports/redeemed${
      !! supplierCode ? `/${supplierCode}` : ""
    }${
      !! resellerCode ? `/${resellerCode}` : ""
    }?start=${
      startDate.getUnixTime()
    }&end=${
      endDate.getUnixTime()  
    }&operatorId=${
      operatorId
    }&offset=${offset}&limit=${limit}&sort=${sort}`

    browserHistory.replace(url);

    this.props.fetch({
      start: startDate,
      end: endDate,
      limit,
      offset,
      resellerCode,
      supplierCode,
      operatorId,
    })
    this.props.setReturnToRedemptionsURL(url)
  }

  export(format) {    
    let { startDate, endDate, resellerCode, supplierCode, operatorId } = this.props
    this.props.export({
        start: startDate,
        end: endDate,
        resellerCode,
        supplierCode,
        operatorId,
    }, format)
  }
  print() {
    if (!this.props.printMode) {
      setTimeout(() => {
        window.print()
      }, 500);
    }
    this.props.togglePrintMode();
  }
  rowClicked(event, redemption) {
    if (redemption.ticketId != "") {
      event.preventDefault() // This prevents ghost click.
      let supplierCode = this.props.supplierCode != "" ? ("/" + this.props.supplierCode) : "",
        resellerCode = this.props.resellerCode != "" ? ("/" + resellerCode) : "",
        offset = this.props.offset,
        limit = this.props.limit,
        start = this.props.startDate.getUnixTime(),
        operatorId = this.props.operatorId,
        end = this.props.endDate.getUnixTime(),
        sort = this.props.sort

      browserHistory.push(`/app/reports/redeemed${supplierCode}${resellerCode}/details/${redemption.ticketId}?back=redeemed`);
    } else if (redemption.passId != "") {
      let supplierCode = this.props.supplierCode,
        resellerCode = this.props.resellerCode,
        offset = this.props.offset,
        limit = this.props.limit,
        start = this.props.startDate.getUnixTime(),
        operatorId = this.props.operatorId,
        end = this.props.endDate.getUnixTime(),
        sort = this.props.sort

      browserHistory.push(`/app/reports/pass/${redemption.passId}?supplierCode=${supplierCode}&back=redeemed`);
    }
  }

  render() {
    let summaryMode = (this.props.children == null || this.props.children.length < 1)

    return (
      <div className="redeemed">
        <div className="redeemed-header" style={{ display: (summaryMode ? "inherit" : "none") }}>
          <div className="redeemed-header-left">
            <h2 style={{ color: "#434343" }}>
              Redeemed
              <DateRangeTitle />
            </h2>
          </div>
          <div className="redeemed-header-right">
            <div className={"no-print report-controls" + (this.props.printMode ? " print-mode" : "")}>
              <DateRangePicker />
              {
                this.props.role == ACTOUREX_TECHNICAL_STAFF ?
                  <SuppliersPopover defaultLabel={this.props.supplierCode}
                    onSupplier={supplier => this.props.setSupplierCode(supplier.supplier.code)} /> : []
              }
              <ResellersAutocomplete 
                onReseller={r => this.props.setResellerCode(r.code)}
                supplierId={isSupplierStaff(this.props.role) && this.props.adminSupplierId != -1 ? this.props.adminSupplierId : ""}                
                selectedResellerCode={this.props.resellerCode}
              />
              <TextField defaultValue={this.props.operatorId} hintText={"Operator ID"}
                value={this.props.operatorId}
                onChange={(e, val) => this.props.setOperatorId(val)}
                style={{ marginLeft: '1em' }}
                key="OperatorId"
              />
              <ExportButton
                style={{ marginLeft: "1em" }} onExport={format => this.export(format)}
              />
            </div>
            <PrintButton
              style={{ marginLeft: "1em" }}
              onPrint={data => { this.print() }} printMode={this.props.printMode}
            />
          </div>
        </div>
        <div className="report-details">
          {this.props.children}
        </div>
        {
          this.props.redeemed.fetching ? (<CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={96} />) :
            this.props.children == null || this.props.children.length < 1 ? (
              <section className="inner-report">
                <Table className="index" selectable={false}>
                  <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                    <TableRow>

                      <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={e => this.sortBy("operatorId")}>Redemption Agent</div>
                      </TableHeaderColumn>


                      <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={e => this.sortBy("date")}>Date</div>
                      </TableHeaderColumn>


                      <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={e => this.sortBy("source")}>Reseller Name</div>
                      </TableHeaderColumn>


                      <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={e => this.sortBy("reference")}>Reference</div>
                      </TableHeaderColumn>


                      <TableHeaderColumn className={"sortable"}>
                        <div className="inner" onClick={e => this.sortBy("travelers")}># of Travelers</div>
                      </TableHeaderColumn>


                      <TableHeaderColumn className={"sortable"}>
                        <div className="inner" onClick={e => this.sortBy("tour")}>Product Name</div>
                      </TableHeaderColumn>

                      <TableHeaderColumn className="print-only sortable">
                        <div className="inner" onClick={e => this.sortBy("supplierTourCode")}>Tour Code</div>
                      </TableHeaderColumn>

                      <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={e => this.sortBy("name")}>Lead Traveler Name</div>
                      </TableHeaderColumn>


                      <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={e => this.sortBy("retail")}>Retail</div>
                      </TableHeaderColumn>

                      <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={e => this.sortBy("net")}>Net</div>
                      </TableHeaderColumn>

                      <TableHeaderColumn className="sortable">
                        <div className="inner" onClick={e => this.sortBy("scanned")}>Redemption Type</div>
                      </TableHeaderColumn>

                    </TableRow>
                  </TableHeader>
                  <TableBody displayRowCheckbox={false}>
                    {
                      this.props.redeemed.data.map((v, i) => (
                        <TableRow key={i} onTouchTap={e => this.rowClicked(e, v)}>

                          <TableRowColumn title={`${v.operatorName} (${v.operatorId})`} className="small-text-print-only">
                            {`${v.operatorName} (${v.operatorId})`}
                          </TableRowColumn>


                          <TableRowColumn className="small-text-print-only">
                            <TimestampTitle ts={v.redemptionTime} /> 
                          </TableRowColumn>


                          <TableRowColumn title={v.source != "" ? v.source : v.voucherBrand}>
                            {v.source != "" ? v.source : v.voucherBrand}
                          </TableRowColumn>


                          <TableRowColumn title={v.bookingId != null ? v.bookingId : ""} className="small-text-print-only">
                            {v.bookingId != null ? v.bookingId : ""}
                          </TableRowColumn>


                          <TableRowColumn>
                            {v.tickets}
                          </TableRowColumn>


                          <TableRowColumn title={v.tour} className="small-text-print-only">
                            {v.tour}
                          </TableRowColumn>

                          <TableRowColumn className="print-only">
                            {v.supplierTourCode != null ? v.supplierTourCode : ""}
                          </TableRowColumn>

                          <TableRowColumn title={v.name} className="small-text-print-only">
                            {v.name}
                          </TableRowColumn>


                          <TableRowColumn className="small-text-print-only">
                            {formatPrice(v.retail)}
                          </TableRowColumn>

                          <TableRowColumn className="small-text-print-only">
                            {formatPrice(v.net)}
                          </TableRowColumn>

                          <TableRowColumn title={redemptionType(v)}>
                            {redemptionType(v)}
                          </TableRowColumn>

                        </TableRow>
                      ))
                    }
                  </TableBody>
                </Table>
                <Pagination />
              </section>
            ) : ""
        }
      </div>
    )
  }
}

// redemptionType determines the label for the redemptionType column
function redemptionType(row) {
  if (!!row.manuallyCreated) {
    return "Manually Added"
  }

  return row.scanned ? "Barcode" : "Voucher Photo"
}



import { connect } from 'react-redux'
import { setPageTitle, togglePrintMode} from '../../ax-redux/actions/app'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../ax-redux/actions/report-filters'
import { fetch, exportCSV, exportXLSX } from '../../ax-redux/actions/redeemed'

import { setReturnToRedemptionsURL } from '../../ax-redux/actions/navigation'
export default connect(
  state => ({
    redeemed: state.redeemed,
    role: state.app.role,
    adminSupplierId: state.reportFilters.adminSupplierId,    
    printMode: state.app.printMode,    
    ...state.reportFilters
  }),
  dispatch => ({
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    fetch: (opts) => {
      dispatch(fetch(opts))
    },
    togglePrintMode: (opts) => {
      dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportCSV(opts))
          return
        case "XLSX":
          dispatch(exportXLSX(opts))
          return
      }
    },
    setReturnToRedemptionsURL: url => dispatch(setReturnToRedemptionsURL(url)),
    ...bindActionCreators(filterActions, dispatch),
  })
)(RedeemedTable);
