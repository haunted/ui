import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import Toggle from 'material-ui/Toggle';
import DatePicker from 'material-ui/DatePicker';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from './export-button'
import PrintButton from './print-button'
import { browserHistory } from 'react-router';
import moment from 'moment-timezone'
import SuppliersPopover from './suppliers-popover'
import { DateStringTitle } from '../datepicker'

import { DateRangePicker, DateRangeTitle } from './components/filters'

let self = null;
const styles = {
  masterSummaryButton: {
    marginLeft: "1em",
    minWidth: '220px'
  }
}

class ShiftSummary extends Component {
  constructor(props) {
    super(props)

    props.contextFilters(props.params, props.location)
  }


  componentDidUpdate(prevProps, prevState) {
    if (this.props.version != prevProps.version) {
      this.fetch()
    }
  }

  fetch() {
    const { supplierCode, startDate, endDate, sort } = this.props
    const url = `/app/reports/shifts${
      (supplierCode !="" ?("/"+supplierCode):"")
    }?start=${
      startDate.getUnixTime()
    }&end=${
      endDate.getUnixTime()
    }&sort=${
      sort
    }`

    browserHistory.replace(url)
    
    this.props.getShiftReportSummary({
      supplierId: supplierCode,
      start: startDate.getUnixTime(),
      end: endDate.getUnixTime(),
      sort,
    })
    this.props.setReturnToShiftSummaryURL(url)
  }
  export(format) {    
    const { supplierCode, startDate, endDate } = this.props
    this.props.export({
      start: startDate,
      end: endDate,
      supplierId: supplierCode,
      endpoint: "/api/reports/shift-summary"
    }, format)
  }
  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }
  viewReport (operatorId, date) {
    const { supplierCode, startDate, endDate, sort } = this.props
      
    browserHistory.push(`/app/reports/shift/${operatorId}/${date}`)
  }
  
  toMasterShift () {
    const supplierCode = this.props.supplierCode != '' ? this.props.supplierCode+'/' : '',
        sort = this.props.sort,
        start = this.props.startDate.getUnixTime(),
        end = this.props.endDate.getUnixTime()
    browserHistory.push(`/app/reports/shifts/${supplierCode}master-summary?start=${
      start
    }&end=${
      end
    }&sort=${
      sort
    }`)
  }

  render() {
      let shifts = this.props.shifts,
          fetching = this.props.fetchingShifts,
          summaryMode = (this.props.children == null || this.props.children.length < 1)

    return (
      <div className="shift-summary">
        <div className="exceptions-header no-print" style={{display: (!summaryMode ? "none" : "inherit")}}>
          <div className="exceptions-header-left">
            <h2 style={{color:"#434343"}} className="no-print">
                Shift Summary Report
                <DateRangeTitle />
            </h2>
          </div>
        <div className="exceptions-header-right">
            <div className={"no-print report-controls"+ (this.props.printMode?" print-mode" : "")}>
              <DateRangePicker />
                {
                  this.props.role == ACTOUREX_TECHNICAL_STAFF ?
                  <SuppliersPopover defaultLabel={this.props.supplierCode}
                                    onSupplier={supplier => this.props.setSupplierCode(supplier.supplier.code)} /> : []
                }
              <RaisedButton
                label="View Master Summary"
                style={styles.masterSummaryButton}
                onTouchTap={e=> this.toMasterShift()}
              />
              <ExportButton
                style={{marginLeft: "1em"}}
                onExport={format => this.export(format)}
              />
          </div>
          <PrintButton
             style={{marginLeft: "1em", marginRight: "1em"}}
             onPrint={data=> { this.print() }} printMode={this.props.printMode}
          />
        </div>
      </div>
      <div className="report-details">
          {this.props.children}
      </div>
         {
           fetching ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
           (this.props.children == null || this.props.children.length < 1 ?
            <Table className="index" selectable={false} key="2">
               <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                 <TableRow>
                   <TableHeaderColumn className="sortable">
                     <div className="inner" onClick={e => this.sortBy("operatorId")}>Staff/Operator ID</div>
                   </TableHeaderColumn>
                   <TableHeaderColumn className="sortable">
                     <div className="inner" onClick={e => this.sortBy("operatorName")}>Operator Name</div>
                   </TableHeaderColumn>
                   <TableHeaderColumn className="sortable">
                     <div className="inner" onClick={e => this.sortBy("date")}>Date of Report</div>
                   </TableHeaderColumn>
                   <TableHeaderColumn>Details</TableHeaderColumn>
                 </TableRow>
               </TableHeader>
               <TableBody displayRowCheckbox={false}>
                 {!!shifts ? shifts.detail.map((shift, i) => (
                   <TableRow key={i}>
                     <TableRowColumn>{shift.operatorId}</TableRowColumn>
                     <TableRowColumn>{shift.operatorName}</TableRowColumn>
                     <TableRowColumn>
                       <DateStringTitle value={shift.date} />
                     </TableRowColumn>
                     <TableRowColumn>
                       <RaisedButton className="view-report no-print" label="View Shift"
                         onClick={e => { this.viewReport(shift.operatorId, shift.date); }} />
                     </TableRowColumn>
                   </TableRow>
                 )) : ""}
               </TableBody>
            </Table> 
          : "" )
        }
      </div>
    )
  }
}

import { connect } from 'react-redux'
import { setPageTitle, togglePrintMode} from '../../ax-redux/actions/app'

import { bindActionCreators } from 'redux'
import * as filterActions from '../../ax-redux/actions/report-filters'

import { getShiftReportSummary } from '../../ax-redux/actions/shifts'
import { setReturnToShiftSummaryURL } from '../../ax-redux/actions/navigation'

import {
    exportCSV,
    exportXLSX
} from '../../ax-redux/actions/finances'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'

export default connect(
  state => ({
      role: state.app.role,
      shifts: state.shifts.getShiftReportSummary.data,
      fetchingShifts: state.shifts.getShiftReportSummary.fetching,
      printMode: state.app.printMode,
      ...state.reportFilters,
  }),
  dispatch => ({
      getShiftReportSummary: (params) => {
          dispatch(getShiftReportSummary(params))
      },
      setReturnToShiftSummaryURL: (params) => {
          dispatch(setReturnToShiftSummaryURL(params))
      },
      setPageTitle: title => {
          dispatch(setPageTitle(title))
      },
      togglePrintMode: (opts) => {
          dispatch(togglePrintMode(opts))
      },
      export: (opts, format) => {
        switch (format) {
          case "CSV":
            dispatch(exportCSV(opts))
            return
          case "XLSX":
            dispatch(exportXLSX(opts))
            return
        }
      },
    ...bindActionCreators(filterActions, dispatch),
  })
)(ShiftSummary);
