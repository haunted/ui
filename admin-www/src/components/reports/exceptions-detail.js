import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import DatePicker from 'material-ui/DatePicker'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import PrintButton from './print-button'
import QueueForReviewButton from '../queue-for-review-button'
import DeleteVoucherButton from '../delete-voucher-button'
import { browserHistory } from 'react-router'
import moment from 'moment-timezone'
import {
  isInternalStaff,
  formatBrokenPrice,
  decodeUnit,
  capitalize,
  formatEnum,
  formatStatusEnum
} from '../../util'
import {internalHighlight} from '../../styles.js'

class ExceptionsDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      exceptionId: this.props.params.id
    }
  }
  componentWillMount() {
    this.props.fetchDetails(this.props.params.id)
    //this.toggleLightbox();
  }
  componentWillUpdate(nextProps, nextState) {
    // if (this.props.exceptions.detailFetching && !nextProps.exceptions.detailFetching && !!nextProps.exceptions.detailData) {
    //   this.props.getVoucherImage(nextProps.exceptions.detailData.voucherId)
    // }
  }

  print() {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }
  returnToSummary () {
      let supplierCode = this.props.params.supplierCode,
          start = this.props.location.query.start,
          end = this.props.location.query.end,
          sort = this.props.location.query.sort;

      browserHistory.push(`/app/reports/exceptions/${supplierCode}?start=${start}&end=${end}&sort=${sort}`);
  }
  filterBasicDetails (i, column, columnName, data, values, meta) {
      let displayValue = data,
          displayColumn = columnName,
          internal = false

    if (data == "") {
      return "";
    }
     switch (column) {
         case "totalRetailPrice":
         case "totalNetPrice":
            displayValue = formatBrokenPrice(data);
         break;
         case "id":
            internal = true
            if (!this.props.isInternalStaff) {
                return "";
            }
         break;
         case "version":
          return "";
         break;
         case "travelDate":
            if (data==" ") {
                displayValue = "–";
            } else {
                displayValue = moment(data).tz(TZ).locale('en').format("DD-MMM-YYYY")
            }
            displayColumn = "Redeemed At";
         break;
         case "supplierRef":
           displayColumn = 'Supplier Reference Number'
            if (data != null && data != "") {
             if (this.props.isInternalStaff) {
                 displayValue = <QRCodeButton lightboxID={columnName} title={data} text={data} />;
             }
           } else {
             return "";
           }
         break;
         case "resellerRef":
            displayColumn = 'Reseller Reference Number'
            if (data != null && data != "") {
              if (this.props.isInternalStaff) {
                displayValue = <QRCodeButton lightboxID={columnName} title={data} text={data} />;
              }
            } else {
              return "";
            }
         break;
         case "status":
           displayValue = formatStatusEnum(data)
         break;
         case "source":
            displayValue = formatEnum(data)
         break;
         case "resellerCode":
            return "";
         break;
         case "resellerName":
          displayColumn = 'Reseller Name';
          if (data != meta.brandName) {
              displayValue = `${meta.brandName} (${data})`
          }
         break;
         case "travelers":
         case "travelerProducts":
         case "products":
            return "";
         case "code":
         case "comment":
         case "cancelReason":
         break;
         case "emailId":
            internal = true
            if (!this.props.isInternalStaff) {
                return "";
            }
         break;
         case "voucherId":
            internal = true
            if (!this.props.isInternalStaff) {
                return "";
            }
         break;
         default:
         break;
     }
     return (
         <TableRow key={i} style={internal ? internalHighlight : {}}>
             <TableRowColumn key={column+":1"}>{capitalize(displayColumn)}</TableRowColumn>
             <TableRowColumn key={column+":2"}>
                { displayValue }
            </TableRowColumn>
         </TableRow>
     )
  }
  displayForAdmin(component) {
      if (this.props.isInternalStaff) {
          return component;
      } else {
          return "";
      }
  }
  renderTravelers (data, products, travelerProducts, header) {
      return (
          <Table selectable={false}>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
             <TableRow>
                 <TableHeaderColumn>Name</TableHeaderColumn>
                 <TableHeaderColumn>Age Band</TableHeaderColumn>
                 <TableHeaderColumn>Gender</TableHeaderColumn>
                 <TableHeaderColumn>Is Lead</TableHeaderColumn>
                 { this.displayForAdmin(
                   <TableHeaderColumn style={internalHighlight}>Ref</TableHeaderColumn>
                 )}
                 <TableHeaderColumn>Weight</TableHeaderColumn>
             </TableRow>
           </TableHeader>
           <TableBody displayRowCheckbox={false}>
              {
                data.map((v, i) => {
                    return [<TableRow key={"row:"+i}>
                        <TableRowColumn key={":2"}>
                           { v.name != null ? (v.name.given+" "+v.name.family) : " "}
                        </TableRowColumn>
                        <TableRowColumn key={":2"}>
                           { v.travelerType != null && formatEnum(v.travelerType) }
                        </TableRowColumn>
                        <TableRowColumn key={":2"}>
                           { v.gender != null && formatEnum(v.gender) }
                        </TableRowColumn>
                        <TableRowColumn key={":2"}>
                           { v.isLead != null ? v.isLead : " " }
                        </TableRowColumn>

                        { this.displayForAdmin(
                            <TableRowColumn key={":2"} style={internalHighlight}>
                            { v.ref != null ? v.ref : " " }
                           </TableRowColumn>
                        )}
                        <TableRowColumn key={":2"}>
                           { v.weight != null ? (v.weight.value + " " + formatEnum(v.weight.units)) : " " }
                        </TableRowColumn>
                      </TableRow>,
                      this.renderTravelerProducts(v.ref, products, travelerProducts, true)]
                  })
                }
            </TableBody>
          </Table>
      )
  }
  getProductData ( products, ref) {
    let output = {
      title: "",
      supplierCode: ""
    };
    products.map((product) => {
      if (product.ref == ref) {
        output.title = product.title;
        output.supplierCode = product.supplierCode;
      }
    })
    return output;
  }
  renderTravelerProducts (travelerRef, products, data, header) {
      return typeof data == 'object' ? [
            !!header ? (
              <TableRow className="table-sub-header">
                <TableRowColumn>Retail Price</TableRowColumn>
                <TableRowColumn>Title</TableRowColumn>
                <TableRowColumn>Supplier Code</TableRowColumn>
             </TableRow>
           ) : "",
            data.map((v, i) => {
              if (v.travelerRef != null && v.travelerRef == travelerRef) {
                let product = this.getProductData(products, v.productRef);
                return (
                  <TableRow key={"row:"+i}>
                      <TableRowColumn>
                        { v.retailPrice != null && formatBrokenPrice(v.retailPrice) }
                      </TableRowColumn>
                          <TableRowColumn>
                            { product.title }
                          </TableRowColumn>
                          <TableRowColumn>
                            { product.supplierCode }
                          </TableRowColumn>
                  </TableRow>
                )
              }
            })
        ] : "";
  }

  render() {
      let exceptions = this.props.exceptions,
          meta = {},
          details = {},
          productData = [],
          travelerData = [],
          travelerProductData = [];

        if (!exceptions.detailFetching && !!exceptions.detailData) {
          meta = exceptions.detailData
          details = meta.ticket
          productData = details.products
          travelerData = details.travelers
          travelerProductData = details.travelerProducts
          // the ticket resellerName is actually the brand,
          // and we will replace it with meta.resellerName
          // also, we want the order to be ResellerName, BrandName
          delete details.resellerName
          if (!!meta.resellerName) {
           details.resellerName = meta.resellerName
          }
        }

    return (
      <div className="exceptions">
        <div className="exceptions-header">
            <div className="exceptions-header-left">
                <h2 style={{color:"#434343"}}>Exception Details</h2>
            </div>
            <div className="exceptions-header-right">
                <div style={{ display: (this.props.printMode ? "none" : "inherit") }}  className="detail-view-controls">
                    <RaisedButton
                          onTouchTap={e => { this.returnToSummary() } }
                          className="no-print return-to-summary"
                          label="Return to Summary"
                    />
                    {this.props.isInternalStaff && details.voucherId != null ?
                        <QueueForReviewButton voucherId={details.voucherId} /> : ""
                    }
                    { details.voucherId && details.voucherId != "" ? (
                      <DeleteVoucherButton voucherId={details.voucherId} label="Delete"
                                           callback={ () => this.returnToSummary() }
                      />
                    ) : ""}
                </div>
                <PrintButton
                       className="no-print"
                       style={{marginLeft: "1em"}}
                       onPrint={data=> { this.print() }} printMode={this.props.printMode}
                />
            </div>
      </div>
         { this.props.exceptions.detailFetching
           ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
           (
             <Table selectable={false}>
               <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                  <TableRow>
                        <TableHeaderColumn key="1">Item</TableHeaderColumn>
                        <TableHeaderColumn key="2">Value</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                  {
                          Object.keys(details).map( (v, i) => {
                              return this.filterBasicDetails(i, v, v, details[v], details, meta)
                          })
                  }
                </TableBody>
              </Table>
           )
        }
        <div className="redeemed-header">
          <div className="redeemed-header-left subsection">
            <h3>Travelers</h3>
          </div>
        </div>
        {!this.props.exceptions.detailFetching && this.renderTravelers(travelerData, productData, travelerProductData) }
      </div>
    )
  }
}

import { connect } from 'react-redux'
import {
  fetchDetails,
} from '../../ax-redux/actions/ticket-exceptions'
import {
    setPageTitle,
    togglePrintMode
} from '../../ax-redux/actions/app'

export default connect(
  state => ({
      exceptions: state.exceptions,
      isInternalStaff: isInternalStaff(state.app.role),
      printMode: state.app.printMode
  }),
  dispatch => ({
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    fetchDetails: id => {
      dispatch(fetchDetails(id))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
  })
)(ExceptionsDetail);
