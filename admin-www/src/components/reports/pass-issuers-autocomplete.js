import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete';

const dataSourceConfig = {
    text: 'text',
    value: 'value',
};

class PassIssuersAutoComplete extends Component {

  constructor( props ) {

    super( props )

    this.state = {
      open: false,
      label: "Pass Issuers",
      lookedUpName: false,
      passIssuerName: "",
      passIssuerCode: "",
      passIssuersByName: [],
      passIssuersByCode: []
    }

  }

  generateCompletionValues (passIssuers) {

      let byName = [],
          byCode = [],
          uniqueCodes = [];

      passIssuers.map( s => {

          const passIssuer = s.passIssuer
          
          let code = ""
              
          if ( passIssuer.code.indexOf(" ") > -1 ) // if there's a space, it's not a code
            return
    
          if (uniqueCodes[passIssuer.name] == null) {

            uniqueCodes[passIssuer.name] = passIssuer.code

          } else {

            code = ` (${passIssuer.code})`

          }

          byName.push({
              text: passIssuer.name + code,
              value: passIssuer.code
          })
          byCode.push({
              text: passIssuer.code,
              value: passIssuer.name
          })

      })

      this.setState({
          passIssuersByName: byName,
          passIssuersByCode: byCode
      })

  }

  fetch () {

    this.props.listPassIssuers()

  }

  componentWillMount() {

    if ( this.props.passIssuers.length != 0 && this.props.passIssuersFetching == false ) {
      
      this.generateCompletionValues( this.props.passIssuers )

    } else {
      
      this.fetch()
    }
  }

  componentWillReceiveProps ( nextProps ) {

    if ( this.props.passIssuerCode != nextProps.passIssuerCode )

        this.fetch( nextProps.passIssuerCode )


    if ( !!this.props.selectDefault && this.props.passIssuers.length == 0 ) {

        if ( nextProps.passIssuers.length > 0 ) {

            this.props.onPassIssuer( nextProps.passIssuers[0] )
            this.setState({
                  label: this.label( nextProps.passIssuers[0] ),
            })

        }

    }

    if ( this.props.passIssuersFetching && !nextProps.passIssuersFetching && nextProps.passIssuers.length != 0 )

        this.generateCompletionValues( nextProps.passIssuers )

        
  }

  componentWillUpdate ( nextProps, nextState ) {

      
  }

  label( v ) {

    return v.name != "" ? v.name : v.code
    
  }

  render () {

    return (
      <div className="supplier-autocomplete-container" style={ this.props.buttonStyle != undefined ? this.props.buttonStyle : {} }>
            {
                this.props.passIssuersFetching && (this.props.passIssuers.length == 0) ?
                  <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={0.5} /> : (
                    <AutoComplete
                        hintText="Supplier Name"
                        searchText={this.state.passIssuerCode}
                        filter={AutoComplete.fuzzyFilter}
                        key="passIssuer-name-field" id="passIssuer-name-field"
                        openOnFocus={true}
                        fullWidth={true}
                        menuProps={{ maxHeight: 600 }}
                        dataSource={this.state.passIssuersByName}
                        dataSourceConfig={dataSourceConfig}
                        onNewRequest={(chosenRequest, index) => {
                          let passIssuerCode = "";
                          if (typeof chosenRequest == "string") {
                            passIssuerCode = chosenRequest;
                          } else {
                            passIssuerCode = chosenRequest.text;
                            this.setState({
                              passIssuerCode: chosenRequest.value.code
                            })
                            this.props.onPassIssuer({
                              code: chosenRequest.value,
                              name: chosenRequest.text
                            })
                          }
                          this.setState({
                            passIssuerCode: passIssuerCode
                          })
                        }}
                        onUpdateInput={(searchText, dataSource) => {
                        }}
                        onBlur={e => {
                          this.setState({
                            "passIssuerCode": e.target.value
                          })
                        }}
                    />
                  )
            }
      </div>
    )

  }

}

PassIssuersAutoComplete.propTypes = {
  onPassIssuer: PropTypes.func.isRequired,
  selectDefault: PropTypes.bool,
  selectedSupplierCode: PropTypes.string,
  buttonStyle: PropTypes.object
}

import { connect } from 'react-redux'

import { listPassIssuers } from '../../ax-redux/actions/pass-issuers'
export default connect(
  (state, ownProps) => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      passIssuers: state.passIssuers.list.data != false ? state.passIssuers.list.data.passIssuers: [],
      passIssuersFetching: state.passIssuers.list.fetching
    }
  },
  dispatch => {
    return {
      listPassIssuers: (data) => {
          dispatch(listPassIssuers(data))
      }
    }
  }
)(PassIssuersAutoComplete)
