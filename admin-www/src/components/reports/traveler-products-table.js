import React, { Component } from 'react'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import moment from 'moment'
import {
  formatBrokenPrice,
  decodeUnit,
  capitalize,
  formatEnum,
  formatStatusEnum,
  isInternalStaff
} from '../../util'
import {
  internalHighlight
} from '../../styles.js'

export default class TravelerProductsTable extends Component {

    constructor ( props ) {
        super( props )
    }

    displayForAdmin( role, component ) {

      return isInternalStaff( role ) ? component : ""

    }

    getProductData ( products, ref) {
      let output = {
        title: "",
        supplierCode: ""
      };
      products.map((product) => {
        if (product.ref == ref) {
          output.title = product.title;
          output.supplierCode = product.supplierCode;
        }
      })
      return output;
    }

    renderTravelerProducts (travelerRef, products, data, header) {
      return typeof data == 'object' ? [
            !!header ? (
               <TableRow className="table-sub-header">
                <TableRowColumn>Retail Price</TableRowColumn>
                <TableRowColumn>Title</TableRowColumn>
                <TableRowColumn>Supplier Code</TableRowColumn>
               </TableRow>) : "",
            data.map((v, i) => {
              if (v.travelerRef != null && v.travelerRef == travelerRef) {
                let product = this.getProductData(products, v.productRef);
                return (
                  <TableRow key={"row:"+i}>
                      <TableRowColumn key={":2"}>
                        { v.retailPrice != null && formatBrokenPrice(v.retailPrice) }
                      </TableRowColumn>
                          <TableRowColumn key={":2"}>
                            { product.title }
                          </TableRowColumn>
                          <TableRowColumn key={":2"}>
                            { product.supplierCode }
                          </TableRowColumn>
                  </TableRow>
                )
              }
            })
        ] : ""
  }

  renderTravelers (data, products, travelerProducts, role) {

      return (
          <Table selectable={false}>
           <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
             <TableRow>
                 <TableHeaderColumn>Name</TableHeaderColumn>
                 <TableHeaderColumn>Age Band</TableHeaderColumn>
                 <TableHeaderColumn>Gender</TableHeaderColumn>
                 <TableHeaderColumn>Is Lead</TableHeaderColumn>
                 { this.displayForAdmin( role,
                   <TableHeaderColumn style={internalHighlight}>Ref</TableHeaderColumn>
                 )}
                 <TableHeaderColumn>Weight</TableHeaderColumn>
             </TableRow>
           </TableHeader>
           <TableBody displayRowCheckbox={false}>
              {
                data.map((v, i) => {
                    return [<TableRow key={"row:"+i}>
                        <TableRowColumn>
                           { v.name != null ? (v.name.given+" "+v.name.family) : " " }
                        </TableRowColumn>
                        <TableRowColumn>
                           { v.travelerType != null && formatEnum(v.travelerType) }
                        </TableRowColumn>
                        <TableRowColumn>
                           { v.gender != null && formatEnum(v.gender) }
                        </TableRowColumn>
                        <TableRowColumn>
                           { v.isLead != null ? (v.isLead ? "Yes" : "No") : " " }
                        </TableRowColumn>
                        { this.displayForAdmin( role,
                            <TableRowColumn style={internalHighlight}>{v.ref}</TableRowColumn>
                        )}
                        <TableRowColumn>
                           { (v.weight != null ? v.weight.value : "") + " "+ (v.weight != null ? formatEnum(v.weight.units) : "") }
                        </TableRowColumn>
                    </TableRow>,
                    this.renderTravelerProducts(v.ref, products, travelerProducts, true)]
                })
              }
            </TableBody>
          </Table>
      )

  }

  render() {
    if ( !!!this.props.travelers || !!!this.props.products || !!!this.props.travelerProducts || !!!this.props.role )
        return ""
    return this.renderTravelers( this.props.travelers, this.props.products, this.props.travelerProducts, this.props.role )
  }

}

TravelerProductsTable.propTypes = {
  products: React.PropTypes.array.isRequired,
  travelers: React.PropTypes.array.isRequired,
  travelerProducts: React.PropTypes.array.isRequired,
  role: React.PropTypes.string.isRequired
}