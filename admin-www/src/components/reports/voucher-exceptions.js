import React, { Component, PropTypes } from 'react';
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import { browserHistory } from 'react-router';
import moment from 'moment-timezone';
import CircularProgress from 'material-ui/CircularProgress';
import ActionGrade from 'material-ui/svg-icons/action/grade';
import AlertError from 'material-ui/svg-icons/alert/error';
import ExportButton from './export-button';
import PrintButton from './print-button';
import LightBox from './lightbox';
import SuppliersPopover from './suppliers-popover';
import ResellersAutocomplete from './resellers-autocomplete';

import { DomainTime } from '../datepicker';
import { formatStatusEnum, formatUnknown } from '../../util';
import { DateRangePicker, DateRangeTitle, Pagination } from './components/filters';


const styles = {
  icon: {
    marginRight: '0.5em',
    opacity: 0.8,
  },
  actions: {
    width: '120px',
  },
};


class VoucherExceptions extends Component {
  constructor(props) {
    super(props);

    props.contextFilters(props.params, props.location);
  }

  componentDidUpdate(prevProps, prevState) {
    const summaryMode = (this.props.children == null || this.props.children.length < 1);
    if (summaryMode) {
      if (this.props.version != prevProps.version) {
        this.fetch();
      }
    }
  }

  fetch() {
    const { startDate, endDate, resellerCode, supplierCode, limit, offset, sort } = this.props;

    browserHistory.replace(
      `/app/reports/voucher-exceptions${
        supplierCode ? `/${supplierCode}` : ''
      }${
        resellerCode ? `/${resellerCode}` : ''
      }?start=${
        startDate.getUnixTime()
      }&end=${
        endDate.getUnixTime()
      }&offset=${
        offset
      }&limit=${
        limit
      }&sort=${
        sort
      }`,
    );

    this.props.getVoucherExceptions({
      start: startDate.getUnixTime() * 1000, // a wild endpoint has appeared! (surprise nanoseconds attack!)
      end: endDate.getUnixTime() * 1000, // Wild endpoint deals 1000 damage to brain! Brain is knocked unconcious.
      limit,
      offset,
      sort,
      resellerCode,
      supplierCode,
    });
  }

  export(format) {
    const { startDate, endDate, resellerCode, supplierCode } = this.props;

    this.props.export({
      start: startDate.getUnixTime() * 1000,
      end: endDate.getUnixTime() * 1000,
      resellerCode,
      supplierCode,
    }, format);
  }
  print() {
    if (!this.props.printMode) {
      setTimeout(() => {
        window.print();
      }, 500);
    }
    this.props.togglePrintMode();
  }

  parseStatus(code) {
    if (typeof code !== 'undefined') {
      return formatStatusEnum(code);
    }
    return '';
  }
  onRowTap(e, v) {
    e.preventDefault();
    let start = this.props.startDate,
      end = this.props.endDate,
      sort = this.props.sort,
      offset = this.props.offset,
      limit = this.props.limit,
      supplierCode = this.props.supplierCode,
      resellerCode = this.props.resellerCode;
    if (this.props.isInternalStaff || this.props.isSupplierBusinessStaff) {
      if (e.target.tagName != 'DIV' && e.target.tagName != 'SPAN' && e.target.tagname != 'BUTTON') {
        browserHistory.push(`/app/reports/voucher-exceptions/${supplierCode}${(resellerCode != '' ? (`/${resellerCode}`) : '')}/edit/${v.id}?start=${
          moment(start).tz(TZ).unix()
        }&end=${
          moment(end).tz(TZ).unix()
        }&offset=${
          offset
        }&limit=${
          limit
        }&sort=${
          sort
        }`);
      }
    }
  }

  render() {
    let summaryMode = (this.props.children == null || this.props.children.length < 1),
      internalStaff = this.props.isInternalStaff,
      canUseEditView = internalStaff || this.props.isSupplierBusinessStaff,
      allowedColumns = ['status', 'date', 'reseller', 'product', 'productCode', 'operatorId', 'rejectionReason'];

    if (internalStaff) {
      allowedColumns.splice(2, 0, 'supplier');
    }

    return (
      <div className="all-vouchers">
        <div className="exceptions-header" style={{ display: (summaryMode ? 'inherit' : 'none') }}>
          <div className="exceptions-header-left" style={{ marginLeft: '1em' }}>
            <h2 style={{ color: '#434343' }}>
                    Voucher Exceptions
              <DateRangeTitle />
            </h2>
          </div>
          <div className="exceptions-header-right" style={{ marginRight: '1em' }}>
            <div className="no-print detail-view-controls" style={{ display: this.props.printMode ? 'none' : 'inherit' }}>
              { !this.props.isSupplierStaff ? (
                <SuppliersPopover
                  defaultLabel={this.props.supplierCode}
                  onSupplier={supplier => this.props.setSupplierCode(supplier.supplier.code)}
                />
              ) : '' }
              { !this.props.isResellerStaff ? (
                <ResellersAutocomplete
                  onReseller={r => this.props.setResellerCode(r.code)}
                  supplierId={this.props.isSupplierStaff && this.props.adminSupplierId != -1 ? this.props.adminSupplierId : ''}
                  selectedResellerCode={this.props.resellerCode}
                />
              ) : ''}
              <DateRangePicker />
              <ExportButton
                onExport={format => this.export(format)}
              />
            </div>
            <PrintButton
              style={{ marginLeft: '1em' }}
              onPrint={(data) => { this.print(); }}
              printMode={this.props.printMode}
            />
          </div>
        </div>
        <div className="report-details">
          {this.props.children}
        </div>
        {
          this.props.children == null || this.props.children.length < 1 ? (
            this.props.fetchingVouchers
              ? <CircularProgress className="exceptions-circular-progress" color={'#27367a'} size={96} /> :
              (
                <section className="inner-report">
                  <Table className={`detail-tabulation ${canUseEditView ? 'index' : ''}`}>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                      <TableRow>
                        <TableHeaderColumn className="sortable">
                          <div className="inner" onClick={e => this.props.setSort('status')}>Status</div>
                        </TableHeaderColumn>
                        <TableHeaderColumn className="sortable">
                          <div className="inner" onClick={e => this.props.setSort('date')}>Date</div>
                        </TableHeaderColumn>
                        {internalStaff ? (
                          <TableHeaderColumn className="sortable">
                            <div className="inner" onClick={e => this.props.setSort('supplier')}>Supplier</div>
                          </TableHeaderColumn>
                        ) : null}
                        <TableHeaderColumn className="sortable">
                          <div className="inner" onClick={e => this.props.setSort('reseller')}>Reseller</div>
                        </TableHeaderColumn>
                        <TableHeaderColumn className="sortable">
                          <div className="inner" onClick={e => this.props.setSort('product')}>Product Name</div>
                        </TableHeaderColumn>
                        <TableHeaderColumn className="sortable">
                          <div className="inner" onClick={e => this.props.setSort('productCode')}>Product Code</div>
                        </TableHeaderColumn>
                        <TableHeaderColumn className="sortable">
                          <div className="inner" onClick={e => this.props.setSort('operatorId')}>Operator</div>
                        </TableHeaderColumn>
                        <TableHeaderColumn className="sortable">
                          <div className="inner" onClick={e => this.props.setSort('rejectionReason')}>Exception Reason</div>
                        </TableHeaderColumn>
                        <TableHeaderColumn style={styles.actions} className="sortable">Details</TableHeaderColumn>
                      </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                      {
                        this.props.vouchersData && this.props.vouchersData.vouchers && this.props.vouchersData.vouchers.map((v, i) => {
                          let hasCloudFactory = !!v.cloudFactory && !!v.cloudFactory.output,
                            noImages = !v.images || (!!v.images && v.images.length === 0),
                            rowColor = 'transparent',
                            statusStyle = {
                              position: 'relative',
                            },
                            icon = '',
                            status = this.parseStatus(v.status),
                            supplierCode = !!v.supplierCode && v.supplierCode != '' ? formatUnknown(v.supplierCode) : '–',
                            resellerCode = hasCloudFactory ? formatUnknown(v.cloudFactory.output.resellerCode) : '–',
                            productName = hasCloudFactory ? formatUnknown(v.cloudFactory.output.productName) : '–',
                            productCode = hasCloudFactory ? formatUnknown(v.cloudFactory.output.productCode) : '–';

                          if (v.status == 'STATUS_EXCEPTION') {
                            rowColor = 'rgba(255, 0, 0, 0.5)';
                            statusStyle.bottom = '5px';
                            icon = <AlertError style={styles.icon} key="0" />;
                          } else if (v.status == 'STATUS_NEW') {
                            rowColor = 'rgba(0, 255, 0, 0.25)';
                            statusStyle.bottom = '5px';
                            icon = <ActionGrade style={styles.icon} key="0" />;
                          }

                          return (
                            <TableRow key={i} onTouchTap={(e) => { this.onRowTap(e, v); }} style={{ backgroundColor: rowColor }}>
                              <TableRowColumn title={status}>
                                {[icon, <span key="1" style={statusStyle} >{status}</span>]}
                              </TableRowColumn>
                              <TableRowColumn>
                                <DomainTime ts={v.created} />
                              </TableRowColumn>
                              {internalStaff ? (
                                <TableRowColumn title={supplierCode}>
                                  {supplierCode}
                                </TableRowColumn>
                              ) : null}
                              <TableRowColumn title={resellerCode}>
                                {resellerCode}
                              </TableRowColumn>
                              <TableRowColumn title={productName}>
                                {productName}
                              </TableRowColumn>
                              <TableRowColumn title={productCode}>
                                {productCode}
                              </TableRowColumn>
                              <TableRowColumn>
                                {v.operatorId != '' ? v.operatorId : '–'}
                              </TableRowColumn>
                              <TableRowColumn>
                                {v.rejectionReason != '' ? v.rejectionReason : '–'}
                              </TableRowColumn>
                              <TableRowColumn style={styles.actions} className="no-print-table-cell">
                                <LightBox voucherId={v.id} disabled={noImages} title={noImages ? 'Scanned' : 'View Image'} />
                              </TableRowColumn>
                            </TableRow>
                          );
                        },
                        )
                      }
                    </TableBody>
                  </Table>
                  <Pagination />
                </section>
              )
          ) : ''
        }
      </div>
    );
  }
}


import { connect } from 'react-redux';
import { setPageTitle, togglePrintMode } from '../../ax-redux/actions/app';

import { bindActionCreators } from 'redux';
import * as filterActions from '../../ax-redux/actions/report-filters';

import {
  getVoucherExceptions,
  exportVoucherExceptionsCSV,
  exportVoucherExceptionsXLSX,
} from '../../ax-redux/actions/vouchers';


export default connect(
  (state) => {
    const role = state.app.role !== false ? state.app.role : '';
    return ({
      vouchersData: state.vouchers.getVoucherExceptions.data,
      fetchingVouchers: state.vouchers.getVoucherExceptions.fetching,
      printMode: state.app.printMode,
      role: state.app.role,
      isSupplierStaff: role.indexOf('SUPPLIER') > -1,
      isSupplierBusinessStaff: role.indexOf('SUPPLIER_BUSINESS_STAFF') > -1,
      isResellerStaff: role.indexOf('RESELLER') > -1,
      isInternalStaff: (role.indexOf('ACTOUREX') > -1 || role.indexOf('REDEAM') > -1),
      ...state.reportFilters,
    });
  },
  dispatch => ({
    getVoucherExceptions: (data) => {
      dispatch(getVoucherExceptions(data));
    },
    setPageTitle: (title) => {
      dispatch(setPageTitle(title));
    },
    togglePrintMode: (opts) => {
      dispatch(togglePrintMode(opts));
    },
    export: (opts, format) => {
      switch (format) {
        case 'CSV':
          dispatch(exportVoucherExceptionsCSV(opts));
          return;
        case 'XLSX':
          dispatch(exportVoucherExceptionsXLSX(opts));
      }
    },
    ...bindActionCreators(filterActions, dispatch),
  }),
)(VoucherExceptions);
