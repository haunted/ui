import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete';

const dataSourceConfig = {
    text: 'text',
    value: 'value',
};

class ResellersAutocomplete extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      label: "Resellers",
      lookedUpName: false,
      resellerName: "",
      resellerCode: "",
      resellersByName: [],
      resellersByCode: []
    }
  }

  generateResellerAutocomplete (resellers) {
      let byName = [],
          byCode = [],
          uniqueCodes = [];

      resellers.map((reseller) => {
          let code = "";
          if (reseller.code.indexOf(" ") > -1) { // if there's a space, it's not a code
            return;
          }
          if (uniqueCodes[reseller.name] == null) {
            uniqueCodes[reseller.name] = reseller.code;
          } else {
            code = ` (${reseller.code})`;
          }
          byName.push({
              text: reseller.name + code,
              value: reseller
          });
          byCode.push({
              text: reseller.code,
              value: reseller
          });
      })
      this.setState({
          resellersByName: byName,
          resellersByCode: byCode
      })
  }

  fetch() {

    if ( this.props.supplierId ) {

      this.props.listSupplierResellers( this.props.supplierId )

    } else {

      this.props.listResellers()

    }

  }

  componentWillMount() {

    if ( !!this.props.resellers && this.props.resellers.length != 0 && this.props.resellersFetching == false ) { console.log("generating reseller autocomplete ")
      
      this.generateResellerAutocomplete( this.props.resellers )

    } else {
      
      this.fetch(); console.log("fetching resellers")

    }
    
  }

  componentWillReceiveProps( nextProps ) {

    if ( this.props.supplierId != nextProps.supplierId && !!nextProps.supplierId ) {
        this.fetch( nextProps.supplierId )
    }

    if (!!this.props.selectDefault && this.props.resellers.length == 0) {
        if (nextProps.resellers.length > 0) {
              this.props.onReseller(nextProps.resellers[0])
              this.setState({
                  label: this.label(nextProps.resellers[0]),
              })
        }
    }

    if (this.props.resellersFetching && !nextProps.resellersFetching && !!nextProps.resellers && nextProps.resellers.length != 0) {
        this.generateResellerAutocomplete(nextProps.resellers)
    }

  }

  componentWillUpdate( nextProps, nextState ) {

      if ((nextState.lookedUpName === false && !!nextProps.resellers && nextProps.resellers.length != 0 &&
          nextProps.selectedResellerCode != null && nextProps.selectedResellerCode != "")) {
        
        nextProps.resellers.map((v, i) => {
          if (v.code == nextProps.selectedResellerCode) {
            console.log("found reseller", v)
            this.setState({
              lookedUpName: true,
              label: this.label(v),
              resellerName: this.label(v)
            })
          }
        })
      }
      if (nextState.resellerName != "" && this.state.resellerName != nextState.resellerName) {
          this.setState({
            lookedUpName: true,
            label: nextState.resellerName
          })
      }
      
  }

  label(v) {
    if (v.name != "") {
      return v.name
    }
    return v.code
  }

  render() {
    return (
      <div className="reseller-autocomplete-container">
            {
                this.props.resellersFetching && (this.props.resellers.length == 0) ?
                  <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={0.5} /> : (
                    <AutoComplete
                        hintText="Reseller Name"
                        searchText={this.state.resellerName}
                        filter={AutoComplete.fuzzyFilter}
                        key="reseller-name-field" id="reseller-name-field"
                        openOnFocus={true}
                        fullWidth={true}
                        menuProps={{ maxHeight: 600 }}
                        dataSource={this.state.resellersByName}
                        dataSourceConfig={dataSourceConfig}
                        onNewRequest={(chosenRequest, index) => {
                          let resellerName = ""
                          if (typeof chosenRequest == "string") {
                            resellerName = chosenRequest
                          } else {
                            resellerName = chosenRequest.text
                            this.setState({
                              resellerCode: chosenRequest.value.code
                            })
                            console.log("selecting reseller ID", chosenRequest.value.id)
                            this.props.onReseller({
                              code: chosenRequest.value.code,
                              name: chosenRequest.text,
                              id: chosenRequest.value.id
                            })
                          }
                          this.setState({
                            resellerName: resellerName
                          })
                        }}
                        onUpdateInput={ ( searchText, dataSource ) => {

                          let resellerCode = this.state.resellerCode

                          if ( searchText == "" ) {

                              resellerCode = ""
                              this.props.onReseller({
                                  code: "",
                                  name: "",
                                  id: ""
                              })

                          }

                        }}
                        onBlur={e => {
                          this.setState({
                            "resellerName": e.target.value
                          })
                        }}
                    />
                  )
            }
      </div>
    )
  }
}

ResellersAutocomplete.propTypes = {
  onReseller: PropTypes.func.isRequired,
  selectDefault: PropTypes.bool,
  selectedResellerCode: PropTypes.string
}

import { connect } from 'react-redux'

import { 
  listResellers,
  listSupplierResellers 
} from '../../ax-redux/actions/resellers'

export default connect(
  ( state, ownProps ) => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      resellers: !!ownProps.supplierId ? (state.resellers.listBySupplier.data != false ? state.resellers.listBySupplier.data.resellers : []) : 
                                          state.resellers.list.data != false ? state.resellers.list.data.resellers: [],
      resellersFetching: !!ownProps.supplierId ? state.resellers.listBySupplier.fetching : state.resellers.list.fetching
    }
  },
  dispatch => {
    return {
      listResellers: ( data ) => {
          dispatch( listResellers( data ) )
      },
      listSupplierResellers: ( supplierCode ) => {
        dispatch( listSupplierResellers( supplierCode ) )
      }
    }
  }
)(ResellersAutocomplete)
