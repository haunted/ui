/* globals window */

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CircularProgress from 'material-ui/CircularProgress';
import RaisedButton from 'material-ui/RaisedButton';
import { browserHistory } from 'react-router';
import PrintButton from './print-button';
import VoidButton from '../delete-voucher-button';
import RedemptionsTable from './redemptions-table';
import TicketTable from './components/TicketTable';
import TravelersTable from './components/TravelersTable';
import { isInternalStaff } from '../../util';
import * as arrivalsActions from '../../ax-redux/actions/arrivals';
import * as appActions from '../../ax-redux/actions/app';


class ShiftTicket extends Component {
  constructor(props) {
    super(props);

    this.print = this.print.bind(this);
  }

  componentWillMount() {
    const {
      fetchDetails,
      params,
    } = this.props;

    // TODO handle undefined ids
    fetchDetails(params.id);
  }

  print() {
    const {
      printMode,
      togglePrintMode,
    } = this.props;

    if (!printMode) {
      setTimeout(() => {
        window.print();
      }, 500);
    }

    togglePrintMode();
  }

  renderHeaderNavigation() {
    const {
      printMode,
      returnToShiftURL,
      arrivals,
    } = this.props;

    const {
      ticket,
      ...details
    } = arrivals.detailData;

    // "r:" is for fake redemptions. A lot of old tickets will show this button and do nothing if
    // we don't stop it here
    const canDelete = (ticket.voucherId || details.redemption) && !details.redemption.id.startsWith('r:');

    return (
      <div className="exceptions-header">
        <div className="exceptions-header-left">
          <h2 style={{ color: '#434343' }}>Ticket Details</h2>
        </div>
        <div className="exceptions-header-right">
          <div style={{ display: (printMode ? 'none' : 'inline-block') }}>
            <RaisedButton
              onTouchTap={() => browserHistory.push(returnToShiftURL)}
              className="no-print"
              label="Return to Summary"
            />

            {canDelete && (
              <VoidButton
                voucherId={ticket.voucherId}
                redemptionId={details.redemption ? details.redemption.id : ''}
                label="Delete"
                callback={this.returnToSummary}
              />
            )}
          </div>

          <div style={{ display: 'inline-block' }}>
            <PrintButton
              className="no-print"
              style={{ marginLeft: '1em' }}
              onPrint={this.print}
              printMode={this.props.printMode}
            />
          </div>
        </div>
      </div>
    );
  }

  render() {
    const {
      arrivals,
      isInternal,
    } = this.props;

    const {
      ticket,
      ...details
    } = arrivals.detailData;

    if (arrivals.detailFetching) {
      return (
        <div className="exceptions">
          <CircularProgress className="exceptions-circular-progress" color={'#27367a'} size={96} />
        </div>
      );
    }

    return (
      <div className="exceptions">
        {arrivals.detailData ? (
          <div>
            {this.renderHeaderNavigation()}
            <TicketTable arrivals={arrivals} isInternal={isInternal} />
            <TravelersTable arrivals={arrivals} isInternal={isInternal} />

            <div>
              <div className="redeemed-header">
                <div className="redeemed-header-left">
                  <h3>Redemptions</h3>
                </div>
              </div>
              {ticket.redemptions && (
                <RedemptionsTable redemptions={[details.redemption]} />
              )}
            </div>
          </div>
        ) : (
          <div>No data loaded</div>
        )}
      </div>
    );
  }
}

ShiftTicket.propTypes = {
  printMode: PropTypes.bool.isRequired,
  isInternal: PropTypes.bool.isRequired,
  returnToShiftURL: PropTypes.string.isRequired,
  arrivals: PropTypes.shape({}).isRequired,
  params: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  fetchDetails: PropTypes.func.isRequired,
  togglePrintMode: PropTypes.func.isRequired,
};

ShiftTicket.defaultProps = {};

const mapStateToProps = state => ({
  arrivals: state.arrivals,
  isInternal: isInternalStaff(state.app.role),
  printMode: state.app.printMode,
  returnToShiftURL: state.navigation.returnToShiftURL,
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators(arrivalsActions, dispatch),
  ...bindActionCreators(appActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ShiftTicket);
