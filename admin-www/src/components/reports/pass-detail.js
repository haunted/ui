import React, { Component, PropTypes } from 'react'
import { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import DatePicker from 'material-ui/DatePicker'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton'
import PrintButton from './print-button'
import QueueForReviewButton from '../queue-for-review-button'
import QRCodeButton from './qr-code-button'
import { browserHistory } from 'react-router'
import moment from 'moment-timezone'
import {
    formatBrokenPrice,
    decodeUnit,
    capitalize,
    formatEnum,
    formatCurrency,
    formatStatusEnum,
    isInternalStaff
} from '../../util'
import { internalHighlight } from '../../styles.js'

class PassDetail extends Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.fetchPass(this.props.params.id, this.props.location.query.supplierCode)
    }

    print() {
        if (!this.props.printMode) {
            setTimeout(() => {
                window.print()
            }, 500);
        }
        this.props.togglePrintMode();
    }
    returnToSummary() {
        let path = this.props.pathname,
            summaryRoute = path,
            supplierCode = this.props.location.query.supplierCode,
            supplierCodes = this.props.location.query.supplierCodes,
            resellerCode = this.props.location.query.resellerCode,
            operatorId = this.props.location.query.operatorId,
            date = this.props.location.query.date,
            start = this.props.location.query.start,
            end = this.props.location.query.end,
            sort = this.props.location.query.sort

        switch (this.props.location.query.back) {
            case "redeemed":
                browserHistory.push(this.props.returnToRedemptionsURL)
                break
            case "reseller-redemptions":
                browserHistory.push(this.props.returnToResellerRedemptionsURL)
                break
            case "invoices":
                browserHistory.push(`/app/reports/supplier-reseller-invoice/${resellerCode}/${supplierCode}?start=${start}&end=${end}&sort=${sort}`);
                break
            case "shift":   // shifts/INTREPID/details/ccarvajal/2017-04-09T00:00:00-04:00
                browserHistory.push(this.props.returnToShiftURL)
                break
            case "master-shift":
                browserHistory.push(`/app/reports/shifts/${supplierCode}/master-summary?start=${start}&end=${end}&sort=${sort}`)
                break
            case "arrivals":
                browserHistory.push(`/app/reports/arrivals/${supplierCode}?start=${start}&end=${end}&sort=${sort}`)
                break
            case "pass-redemptions":
                browserHistory.push(`/app/reports/pass-redemptions`)
                break
        }
    }
    getProductData(products, ref) {
        let output = {
            title: "",
            supplierCode: ""
        };
        products.map((product) => {
            if (product.ref == ref) {
                output.title = product.title;
                output.supplierCode = product.supplierCode;
            }
        })
        return output;
    }
    displayForAdmin(component) {
        if (this.props.isInternalStaff) {
            return component;
        } else {
            return "";
        }
    }
    renderTravelers(data) {
        return data.map((v, i) => {
            return (
                <TableRow key={"row:" + i}>
                    <TableRowColumn>
                        {v.fullName}
                    </TableRowColumn>
                    <TableRowColumn>
                        {v.isLead != null ? (v.isLead ? "Yes" : "No") : " "}
                    </TableRowColumn>
                    <TableRowColumn>
                        {v.ageBand != null && formatEnum(v.ageBand)}
                    </TableRowColumn>
                    <TableRowColumn>
                        {v.gender != null && formatEnum(v.gender)}
                    </TableRowColumn>
                    <TableRowColumn style={isInternalStaff(this.props.role) ? internalHighlight : {}}>
                        {this.displayForAdmin(v.ref)}
                    </TableRowColumn>
                </TableRow>
            )
        })
    }
    renderSources(data) {
        return data.map((source, index) => {
            return (
                <TableRow key={index}>
                    <TableRowColumn>
                        {formatEnum(source.typeOf)}
                    </TableRowColumn>
                    <TableRowColumn>
                        {source.supplierCode}
                    </TableRowColumn>
                    <TableRowColumn>
                        {this.displayForAdmin(source.operatorId)}
                    </TableRowColumn>
                    <TableRowColumn style={isInternalStaff(this.props.role) ? internalHighlight : {}}>
                        {this.displayForAdmin(source.deviceId)}
                    </TableRowColumn>
                    <TableRowColumn>

                    </TableRowColumn>
                </TableRow>
            )
        })
    }
    renderItems(data) {
        return data.map((item, index) => {
            return (
                <TableRow key={index}>
                    <TableRowColumn>
                        {item.name}
                    </TableRowColumn>
                    <TableRowColumn>
                        {formatCurrency(item.net.currencyCode, item.net.amount)}
                    </TableRowColumn>
                    <TableRowColumn>
                        {formatCurrency(item.retail.currencyCode, item.retail.amount)}
                    </TableRowColumn>
                    <TableRowColumn>
                        {item.supplierProductCode}
                    </TableRowColumn>
                    <TableRowColumn>
                        {item.travelerType}
                    </TableRowColumn>
                </TableRow>
            )
        })
    }
    renderRedemptions(data) {
        return typeof data == 'object' ?
            data.map((v, i) => {
                return (
                    <Table key={"redemption:" + i} style={{ marginBottom: '1em' }}>
                        <TableBody displayRowCheckbox={false}>
                            {this.displayForAdmin(
                                <TableRow>
                                    <TableRowColumn style={internalHighlight}>
                                        ID
                                </TableRowColumn>
                                    <TableRowColumn style={internalHighlight}>
                                        {v.id}
                                    </TableRowColumn>
                                    <TableRowColumn></TableRowColumn>
                                    <TableRowColumn></TableRowColumn>
                                    <TableRowColumn></TableRowColumn>
                                </TableRow>
                            )}
                            <TableRow>
                                <TableRowColumn>
                                    Status
                            </TableRowColumn>
                                <TableRowColumn>
                                    {formatStatusEnum(v.status)}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Source Type
                            </TableRowColumn>
                                <TableRowColumn>
                                    {v.source.typeOf}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Source Supplier Code
                            </TableRowColumn>
                                <TableRowColumn>
                                    {v.source.supplierCode}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Source Operator Id
                            </TableRowColumn>
                                <TableRowColumn>
                                    {v.source.operatorId}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Supplier Name
                            </TableRowColumn>
                                <TableRowColumn>
                                    {v.supplier.name}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Supplier Code
                            </TableRowColumn>
                                <TableRowColumn>
                                    {v.supplier.code}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Reseller Name
                            </TableRowColumn>
                                <TableRowColumn>
                                    {v.reseller.name}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Reseller Code
                            </TableRowColumn>
                                <TableRowColumn>
                                    {v.reseller.code}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            {this.displayForAdmin(
                                <TableRow>
                                    <TableRowColumn>
                                        Reseller Ref
                                </TableRowColumn>
                                    <TableRowColumn>
                                        {v.resellerRef}
                                    </TableRowColumn>
                                    <TableRowColumn></TableRowColumn>
                                    <TableRowColumn></TableRowColumn>
                                    <TableRowColumn></TableRowColumn>
                                </TableRow>
                            )}
                            <TableRow>
                                <TableRowColumn>
                                    Net Price
                            </TableRowColumn>
                                <TableRowColumn>
                                    {formatCurrency(v.net.currencyCode, v.net.amount)}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Retail Price
                            </TableRowColumn>
                                <TableRowColumn>
                                    {formatCurrency(v.retail.currencyCode, v.retail.amount)}
                                </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Sources
                            </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>Type</TableRowColumn>
                                <TableRowColumn>Supplier</TableRowColumn>
                                <TableRowColumn>Operator Id</TableRowColumn>
                                <TableRowColumn style={isInternalStaff(this.props.role) ? internalHighlight : {}}>
                                    {this.displayForAdmin("Device Id")}
                                </TableRowColumn>
                            </TableRow>
                            {this.renderSources(v.sources)}
                            <TableRow>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Items
                            </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>Name</TableRowColumn>
                                <TableRowColumn>Net Price</TableRowColumn>
                                <TableRowColumn>Retail Price</TableRowColumn>
                                <TableRowColumn>Supplier Product Code</TableRowColumn>
                                <TableRowColumn>Traveler Type</TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            {this.renderItems(v.items)}
                            <TableRow>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>
                                    Travelers
                            </TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                                <TableRowColumn></TableRowColumn>
                            </TableRow>
                            <TableRow>
                                <TableRowColumn>Name</TableRowColumn>
                                <TableRowColumn>Is Lead</TableRowColumn>
                                <TableRowColumn>Age Band</TableRowColumn>
                                <TableRowColumn>Gender</TableRowColumn>
                                <TableRowColumn style={isInternalStaff(this.props.role) ? internalHighlight : {}}>
                                    {this.displayForAdmin("Version")}
                                </TableRowColumn>
                            </TableRow>
                            {this.renderTravelers(v.travelers)}
                        </TableBody>
                    </Table>
                )
            }) :
            ""
    }

    render() {

        let passDetails = this.props.passDetails,
            details = !!passDetails ? passDetails.pass : false,
            redemptions = !!passDetails ? passDetails.redemptions : false,
            productData = [],
            travelerData = [],
            travelerProductData = [];

        console.log("Pass details details", details)

        if (!this.props.passFetching && !!details) {
            travelerData = details.travelers
            travelerProductData = details.travelerProducts || []
        }

        return (
            <div className="exceptions">
                <div className="exceptions-header">
                    <div className="exceptions-header-left">
                        <h2 style={{ color: "#434343" }}>Pass Details</h2>
                    </div>
                    <div className="exceptions-header-right">
                        <div style={{ display: (this.props.printMode ? "none" : "inherit") }} className="detail-view-controls">
                            <RaisedButton
                                onTouchTap={e => { this.returnToSummary() }}
                                className="no-print return-to-summary"
                                label="Return to Summary"
                            />
                        </div>
                        <PrintButton
                            className="no-print"
                            style={{ marginLeft: "1em" }}
                            onPrint={data => { this.print() }} printMode={this.props.printMode}
                        />
                    </div>
                </div>
                {this.props.passFetching || !!!details
                    ? <CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} /> :
                    (
                        <Table selectable={false}>
                            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                <TableRow>
                                    <TableHeaderColumn key="1">Item</TableHeaderColumn>
                                    <TableHeaderColumn key="2">Value</TableHeaderColumn>
                                </TableRow>
                            </TableHeader>
                            <TableBody displayRowCheckbox={false}>
                                {
                                    isInternalStaff(this.props.role) ? (
                                        <TableRow>
                                            <TableRowColumn>Id</TableRowColumn>
                                            <TableRowColumn>
                                                {details.id}
                                            </TableRowColumn>
                                        </TableRow>
                                    ) : ''
                                }
                                <TableRow>
                                    <TableRowColumn>
                                        Status
                                    </TableRowColumn>
                                    <TableRowColumn>
                                        {formatStatusEnum(details.status)}
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn>
                                        Single Use
                                    </TableRowColumn>
                                    <TableRowColumn>
                                        {details.singleUse ? "Yes" : "No"}
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn>Issuer</TableRowColumn>
                                    <TableRowColumn>
                                        {details.issuer.name != "" ? details.issuer.name : details.issuer.code}
                                    </TableRowColumn>
                                </TableRow>
                                <TableRow>
                                    <TableRowColumn>Reseller</TableRowColumn>
                                    <TableRowColumn>
                                        {details.reseller.name != "" ? details.reseller.name : details.reseller.code}
                                    </TableRowColumn>
                                </TableRow>
                            </TableBody>
                        </Table>
                    )}
                <div className="redeemed-header">
                    <div className="redeemed-header-left subsection">
                        <h3>Travelers</h3>
                    </div>
                </div>
                <Table selectable={false}>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                            <TableHeaderColumn>Name</TableHeaderColumn>
                            <TableHeaderColumn>Is Lead</TableHeaderColumn>
                            <TableHeaderColumn>Age Band</TableHeaderColumn>
                            <TableHeaderColumn>Gender</TableHeaderColumn>
                            <TableHeaderColumn style={isInternalStaff(this.props.role) ? internalHighlight : {}}>
                                {this.displayForAdmin("Version")}
                            </TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {!this.props.passFetching && !!details && this.renderTravelers(details.travelers)}
                    </TableBody>
                </Table>
                <div className="redeemed-header">
                    <div className="redeemed-header-left subsection">
                        <h3>Redemptions</h3>
                    </div>
                </div>
                {!this.props.passFetching && redemptions != false && this.renderRedemptions(redemptions)}
            </div>
        )
    }
}
import { connect } from 'react-redux'
import {
    fetchPass,
} from '../../ax-redux/actions/passes'
import {
    setPageTitle,
    togglePrintMode
} from '../../ax-redux/actions/app'

export default connect(
    state => ({
        role: state.app.role,
        passDetails: state.passes.get.data,
        passFetching: state.passes.get.fetching,
        isInternalStaff: isInternalStaff(state.app.role),
        printMode: state.app.printMode,
        pathname: state.routing.locationBeforeTransitions.pathname,
        returnToShiftURL: state.navigation.returnToShiftURL,
        returnToRedemptionsURL: state.navigation.returnToRedemptionsURL,
        returnToResellerRedemptionsURL: state.navigation.returnToResellerRedemptionsURL,
    }),
    dispatch => ({
        setPageTitle: title => {
            dispatch(setPageTitle(title))
        },
        fetchPass: (id, supplierCode) => {
            dispatch(fetchPass(id, supplierCode))
        },
        togglePrintMode: (opts) => {
            dispatch(togglePrintMode(opts))
        },
    })
)(PassDetail);
