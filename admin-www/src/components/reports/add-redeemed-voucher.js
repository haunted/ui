import React, {PropTypes, Component} from 'react'
import { browserHistory } from 'react-router'
import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField'
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete'
import moment from 'moment-timezone'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {
  cyan700,
} from 'material-ui/styles/colors'
import TimePicker from 'material-ui/TimePicker'
import DatePicker from 'material-ui/DatePicker'
import {isSupplierStaff, isResellerStaff, isInternalStaff} from '../../util'
import getInclusiveRange from './get-inclusive-range'
import validateDateRange from './validate-date-range'
const dataSourceConfig = {
  text: 'text',
  value: 'value',
}
const styles = {
  showOverflow: {
    overflow: 'visible'
  },
  headerControls : {
    display: "inline-block",
    width: "100%"
  },
  returnToSummaryButton: {
    minWidth: "190px",
    float: "right",
    marginTop: "1em",
    marginRight: "1em"
  },
  submitButton: {

  }
}
class AddRedeemedVoucher extends Component {
  constructor(props) {
    super(props)

    this.state = {
      images: [],
      version: 0,
      voucherId: "",
      deviceId: "",
      ticketId: "",
      payableBy: "",
      date: new Date(),
      operatorId: this.props.params.operator,
      time: new Date(),
      fetching: false,
      created: null,
      status: "STATUS_TICKETED",
      supplierReferenceNumber: "",
      resellerReferenceNumber: "",
      reservationNumber: "",
      supplierCode: "",
      resellerCode: "",
      resellerId: "",
      productName: "",
      productCode: "",
      productId: "",
      cloudFactory: null,
      cloudVision: {
        imageId: "",
        status: "STATUS_PROCESSING",
        text: ""
      },
      totalPrice: "",
      totalPriceCurrency: "",
      travelerFirstName: "",
      travelerLastName: "",
      rejectReason: "",
      validationMessage: "",
      resellersByName: [],
      resellersByCode: [],
      productsByName: [],
      productsByCode: [],
      operatorsById: [],
      dateError: "",
      adultsError: "",
      childrenError: "",
      studentsError: "",
      infantsError: "",
      seniorsError: ""
    }
    this.resellerCodeField = null
    this.resellerNameField = null
    this.productCodeField = null
    this.productNameField = null
  }

  generateResellerAutoComplete ( resellers ) {
      let byName = [],
          byCode = []

      resellers.map( reseller => {
          byCode.push({
              text: reseller.code,
              value: reseller
          });
          byName.push({
              text: reseller.name,
              value: reseller
          });
          if (!!reseller.aliases) {
            reseller.aliases.forEach(alias=>{
              byName.push({
                  text: alias,
                  value: reseller
              });
            })
          }
      })
      this.setState({
          resellersByName: byName,
          resellersByCode: byCode
      })
  }
  generateProductAutoComplete ( products ) {
      let byName = [],
          byCode = [],
          hasProducts = !! products && typeof products.map == 'function'

      hasProducts && products.map( product => {
          byCode.push({
              text: product.code,
              value: product
          });
          byName.push({
              text: !!product.map ? product.map.from.productNames[0] : product.code + " (no name)",
              value: product
          });
      })
      this.setState({
          productsByName: byName,
          productsByCode: byCode
      })
  }

  componentWillMount() {

    let supplierCode = "",
        resellerCode = ""

    if ( this.props.resellers == false && !this.props.fetchingResellers ) {

        if ( this.isSupplierStaff() ) {

            this.props.listSupplierResellers( this.props.adminSupplierId != -1 ? this.props.adminSupplierId : this.props.supplierId )

        }   else {

            this.props.listResellers()

        }

    } else if (!this.props.fetchingResellers) {
        this.generateResellerAutoComplete(this.props.resellers)
    }
    if (!!this.props.params) {
        if (!!this.props.params.supplierCode) {
            supplierCode = this.props.params.supplierCode
        }
        if (!!this.props.params.resellerCode) {
            resellerCode = this.props.params.resellerCode
        }
    }
    if (this.isSupplierStaff()) {
      supplierCode = this.props.orgCode
    } else if (this.isResellerStaff()) {
      resellerCode = this.props.orgCode
    }
    this.setState({
      supplierCode,
      resellerCode
    })

    if ((this.props.products == false && !this.props.fetchingProducts) ||
        this.isSupplierStaff() == false && this.state.supplierCode != this.props.params.supplierCode) {
        if (resellerCode != "") {            
            this.props.listSupplierResellerCatalogItems(supplierCode, resellerCode)
        }
    } else if (!this.props.fetchingProducts) {
        this.generateProductAutoComplete(this.props.products);
    }
    if (!this.props.fetchingAccounts) {
      if (this.isInternalStaff()) {
        this.props.listAccounts()
      } else {
        this.props.listAccounts({orgCode: this.props.orgCode})
      }
    }

  }

  componentWillUpdate(nextProps, nextState) { // map fields to view
    if (this.props.fetchingVoucher && nextProps.fetchingVoucher == false && nextProps.voucher != false) {
        let meta = nextProps.voucher,
            voucher = meta.voucher,
            hasCFOutput = (voucher.cloudFactory != null && voucher.cloudFactory.output != null),
            output = hasCFOutput ? voucher.cloudFactory.output : {};

                  this.setState({
                    voucher: voucher,
                    version: voucher.version,
                    images: voucher.images,
                    created: voucher.created,
                    status: voucher.status,
                    cloudFactoryStatus: voucher.cloudFactory ? voucher.cloudFactory.status : "",
                    bookingReferenceNumber: hasCFOutput ? output.bookingReferenceNumber : "",
                    numberOfAdults: hasCFOutput ? output.numberOfAdults : "",
                    numberOfChildren: hasCFOutput ? output.numberOfChildren : "",
                    numberOfInfants: hasCFOutput ? output.numberOfInfants : "",
                    numberOfSeniors: hasCFOutput ? output.numberOfSeniors : "",
                    numberOfStudents: hasCFOutput ? output.numberOfStudents : "",
                    product: hasCFOutput ? output.product : "",
                    productCode: hasCFOutput ? output.productCode : "",
                    productId: hasCFOutput ? output.productId : "",
                    productName: hasCFOutput ? output.productName : "",
                    resellerCode: hasCFOutput ? output.resellerCode : "",
                    resellerId: hasCFOutput ? output.resellerId : "",
                    reseller: hasCFOutput ? output.reseller : "",
                    resellerName: hasCFOutput ? output.resellerName : "",
                    reservationNumber: hasCFOutput ? output.reservationNumber : "",
                    supplierConfirmationNumber: hasCFOutput ? output.supplierConfirmationNumber : "",
                    totalPrice: hasCFOutput ? output.totalPrice : "",
                    totalPriceCurrency: hasCFOutput ? output.totalPriceCurrency : "",
                    travelerFirstName: hasCFOutput ? output.travelerFirstName : "",
                    travelerLastName: hasCFOutput ? output.travelerLastName : "",
                    supplierCode: voucher.supplierCode != null ? voucher.supplierCode : "",
                    cloudFactory: voucher.cloudFactory || "",
                    cloudVision: voucher.cloudVision != null ? voucher.cloudVision : {},
                    rejectReason: voucher.rejectReason != null ? voucher.rejectReason : "",
                    operatorId: voucher.operatorId != null ? voucher.operatorId : ""
                  })

      }
      if (this.props.resellers == false && nextProps.resellers != false) {
          this.generateResellerAutoComplete(nextProps.resellers)
      }
      if (this.props.fetchingProducts == true && nextProps.fetchingProducts === false){
          this.generateProductAutoComplete(nextProps.products)
      }
      if (nextState.resellerCode != "") {
        if (this.props.params.supplierCode != nextProps.params.supplierCode ||
          this.state.resellerCode != nextState.resellerCode) {
            if (this.isSupplierStaff()) {
                //if (nextProps.params.supplierCode != nextProps.orgCode) {
                this.props.listSupplierResellerCatalogItems(nextProps.orgCode, nextState.resellerCode)
                //}
            } else {
                this.props.listSupplierResellerCatalogItems(nextProps.params.supplierCode, nextState.resellerCode)
                this.setState({
                    supplierCode: nextProps.params.supplierCode
                })
            }
        }
      }
      
      if (nextProps.resellers != false) {
        if ((this.state.resellerName != nextState.resellerName) ||
            (nextState.resellerName != "" && nextState.resellerName != "UNKNOWN" && (nextState.resellerId == "" || nextState.resellerId == "UNKNOWN"))) {
            nextProps.resellers.map((reseller) => {
                if (reseller.name == nextState.resellerName) {
                    this.setState({
                        resellerCode: reseller.code,
                        resellerId: reseller.id
                    })
                    if (!!this.resellerCodeField) {
                        this.resellerCodeField.value = reseller.name
                    }
                }
            })
        }
      }
      if (!!nextProps.products) {
        if ((this.state.productName != nextState.productName) ||
            (nextState.productName != "" && nextState.productName != "UNKNOWN" && (nextState.productId == "" || nextState.productId == "UNKNOWN"))){
            nextProps.products.map((product) => {
                if (!!product.map && product.map.from.productNames[0] == nextState.productName) {
                    this.setState({
                        productCode: product.code,
                        productId: product.id
                    })
                    if (!!this.productCodeField) {
                        this.productCodeField.value = product.code
                    }
                }
            })
        }
        if ((this.state.productCode != nextState.productCode) ||
            (nextState.productCode != "" && nextState.productCode != "UNKNOWN" && (nextState.productId == "" || nextState.productId == "UNKNOWN"))) {
          nextProps.products.map((product) => {
              if (product.code == nextState.productCode) {
                  this.setState({
                      productId: product.id
                  })
                  if (!!product.map && product.map.from.productNames[0] != '') {
                    this.setState({
                        product: product.map.from.productNames[0],
                        productName: product.map.from.productNames[0]
                    })
                  }
                  if (!!this.productNameField) {
                    this.productNameField.value = product.name
                  }
              }
          })
        }
      }

      if (this.props.createResponse == false && nextProps.createResponse != false) {
          let voucher = nextProps.createResponse.voucher,
              supplierCode = this.state.supplierCode;
          this.setState({
            voucher: voucher
          })
          // browserHistory.push(`/app/reports/voucher-exceptions/${supplierCode}?start=${this.props.location.query.start}&end=${this.props.location.query.end}`)
          this.returnToSummary()
      }
      // Clear Errors
      if (nextState.numberOfAdults != this.state.numberOfAdults &&
         (!isNaN(nextState.numberOfAdults) && parseInt(nextState.numberOfAdults) % 1 == 0)){
           this.setState({adultsError: ""});
      }
      if (nextState.numberOfChildren != this.state.numberOfChildren &&
         (!isNaN(nextState.numberOfChildren) && parseInt(nextState.numberOfChildren) % 1 == 0)){
           this.setState({childrenError: ""});
      }
      if (nextState.numberOfInfants != this.state.numberOfInfants &&
         (!isNaN(nextState.numberOfInfants) && parseInt(nextState.numberOfInfants) % 1 == 0)) {
           this.setState({infantsError: ""});
      }
      if (nextState.numberOfSeniors != this.state.numberOfSeniors &&
         (!isNaN(nextState.numberOfSeniors) && parseInt(nextState.numberOfSeniors) % 1 == 0)) {
           this.setState({seniorsError: ""});
      }
      if (nextState.numberOfStudents != this.state.numberOfStudents &&
         (!isNaN(nextState.numberOfStudents) && parseInt(nextState.numberOfStudents) % 1 == 0)) {
           this.setState({studentsError: ""});
      }
  }

  save() {
      this.state.date.setHours(0)
      this.state.date.setMinutes(0)
      this.state.date.setSeconds(1)
      
    let createdTime = new Date(this.state.time), 
        createdDate = this.state.date, 
        created = new Date(createdDate.getTime() + createdTime.getMilliseconds() + (createdTime.getMinutes() * 60000)  + (createdTime.getHours() * 3600000)),
        seconds = Math.floor(created.getTime() / 1000),
        nanos = created.getMilliseconds() * 1000000,
        data = {
            status: this.state.status,
            version: this.state.version,
            created: {
              seconds: seconds,
              nanos,
            },
            id: this.state.voucherId,
            deviceId: null,
            ticketId: this.state.ticketId,
            payableBy: this.state.payableBy,
            images: this.state.images,
            operatorId: this.state.operatorId,
            supplierCode: this.state.supplierCode,
            resellerCode: this.state.resellerCode,
            rejectReason: this.state.rejectReason,
            // cloudVision: this.state.cloudVision,
            cloudFactory: {
                // id: "",
                // lineId: "",
                // imageId: "",
                // input: {
                //     voucherId: this.state.voucherId
                // },
                status:"STATUS_COMPLETED",
                output: {
                    bookingReferenceNumber: this.state.resellerReferenceNumber,
                    numberOfAdults: this.state.numberOfAdults,
                    numberOfChildren: this.state.numberOfChildren,
                    numberOfInfants: this.state.numberOfInfants,
                    numberOfSeniors: this.state.numberOfSeniors,
                    numberOfStudents: this.state.numberOfStudents,
                    product: this.state.product,
                    productCode: this.state.productCode,
                    productId: this.state.productId,
                    productName: this.state.productName,
                    resellerCode: this.state.resellerCode,
                    resellerId: this.state.resellerId,
                    resellerName: this.state.resellerName,
                    reservationNumber: this.state.reservationNumber,
                    supplierConfirmationNumber: this.state.supplierReferenceNumber,
                    totalPrice: this.state.totalPrice,
                    totalPriceCurrency: this.state.totalPriceCurrency,
                    travelerFirstName: this.state.travelerFirstName,
                    travelerLastName: this.state.travelerLastName
                }
            }
          }
    // validation
    if (created.getTime() > Date.now()) {
        this.setState({dateError: "Created time can not be in the future."})
        return;
    }
    if (this.state.numberOfAdults != "" && (isNaN(this.state.numberOfAdults) || parseFloat(this.state.numberOfAdults) % 1 != 0)) {
      this.setState({adultsError: "Adults must be whole a number."});
      return;
    }
    if (this.state.numberOfChildren != "" && (isNaN(this.state.numberOfChildren) || parseFloat(this.state.numberOfChildren) % 1 != 0)) {
      this.setState({childrenError: "Children must be whole a number."});
      return;
    }
    if (this.state.numberOfInfants != "" && (isNaN(this.state.numberOfInfants) || parseFloat(this.state.numberOfInfants) % 1 != 0)) {
      this.setState({infantsError: "Infants must be whole a number."});
      return;
    }
    if (this.state.numberOfSeniors != "" && (isNaN(this.state.numberOfSeniors) || parseFloat(this.state.numberOfSeniors) % 1 != 0)) {
      this.setState({seniorsError: "Seniors must be whole a number."});
      return;
    }
    if (this.state.numberOfStudents != "" && (isNaN(this.state.numberOfStudents) || parseFloat(this.state.numberOfStudents) % 1 != 0)) {
      this.setState({studentsError: "Students must be whole a number."});
      return;
    }
    console.log("data", data)
    this.props.createRedeemedVoucher(data)
  }
  isSupplierStaff () {
    return isSupplierStaff(this.props.role)
  }
  isResellerStaff () {
    return isResellerStaff(this.props.role)
  }
  isInternalStaff () {
    return isInternalStaff(this.props.role)
  }
  returnToSummary () {
    browserHistory.push(this.props.returnToShiftURL)
  }
  shouldDisableDate (day) {
    let time = new Date(this.state.time)
    time = new Date(day.getTime() + time.getMilliseconds() + (time.getMinutes() * 60000)  + (time.getHours() * 3600000))
    return  time.getTime() > Date.now()
  }
  handleRequestClose (evt) {
    this.setState({
       popoverOpen: false
    });
  }
  onAccount(item, label) {
    this.setState({
      operatorId: label,
      popoverOpen: false
    })
  }
  handleAccountTouchTap (event) {
    event.preventDefault()
    this.setState({
        popoverOpen: true,
        anchorEl: event.currentTarget
    })
  }
  renderForm () {
      return (
          <Table className="edit-table edit-form">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
              <TableRow>
                  <TableHeaderColumn>Field</TableHeaderColumn>
                  <TableHeaderColumn>Value</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow selectable={false}>
                  <TableRowColumn>Date</TableRowColumn>
                  <TableRowColumn style={styles.showOverflow}>
                  <DatePicker
                       hintText="Enter Date"
                       className="form-date-picker"
                       formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                       shouldDisableDate={(day) => { return this.shouldDisableDate(day) }}
                       value={this.state.date}
                       onChange={(ev, date) => { this.setState({ date: date}) } }
                 />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                  <TableRowColumn>Time</TableRowColumn>
                  <TableRowColumn style={styles.showOverflow}>
                    <TimePicker
                      hintText="Enter Time"
                      errorText={this.state.dateError}
                      className="form-time-picker"
                      dialogStyle={{color: 'black'}}
                      onChange={(ev, date) => { this.setState({ time: date}) } }
                    />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                  <TableRowColumn>Status</TableRowColumn>
                  <TableRowColumn>
                  {this.state.status}
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                  <TableRowColumn>Supplier Code</TableRowColumn>
                  <TableRowColumn>
                  { this.state.supplierCode }
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                  <TableRowColumn>Operator ID</TableRowColumn>
                  <TableRowColumn>
                    <RaisedButton
                      onTouchTap={(event) => { this.handleAccountTouchTap(event); }}
                      label={this.state.operatorId}
                    />
                    <Popover
                      open={this.state.popoverOpen}
                      anchorEl={this.state.anchorEl}
                      anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                      targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
                      onRequestClose={(event) => { this.handleRequestClose(event); }}
                    >
                      <Menu style={{maxHeight:"66vh"}}>
                        {
                            !!this.props.accounts && this.props.accounts.map(account => {
                                    return (
                                        <MenuItem key={account.id} onClick={()=>{this.onAccount(account.id, account.username)}}
                                                  primaryText={account.username} />
                                    )
                            })
                        }
                      </Menu>
                    </Popover>
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Reseller Name</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                  <AutoComplete
                     searchText={this.state.resellerName}
                     filter={AutoComplete.fuzzyFilter}
                     key="reseller-name-field" id="reseller-name-field"
                     openOnFocus={true}
                     fullWidth={true}
                     dataSource={this.state.resellersByName}
                     dataSourceConfig={dataSourceConfig}
                     onNewRequest={(chosenRequest, index) => {
                        console.log("onNewRequest", chosenRequest);
                        let resellerName = "";
                        if (typeof chosenRequest == "string") {
                          resellerName = chosenRequest;
                        } else {
                          resellerName = chosenRequest.value.name;
                        }
                        this.setState({
                            resellerName: resellerName
                        })
                     }}
                     onUpdateInput={(searchText, dataSource) => {}}
                     onBlur={e => {
                         this.setState({
                             "resellerName": e.target.value
                         })
                     }}
                   />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Reseller Code</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                  <AutoComplete
                     searchText={this.state.resellerCode}
                     filter={AutoComplete.fuzzyFilter}
                     key="reseller-code-field" id="reseller-code-field"
                     ref={r=> { this.resellerCodeField = r }}
                     openOnFocus={true}
                     fullWidth={true}
                     dataSource={this.state.resellersByCode}
                     dataSourceConfig={dataSourceConfig}
                     onNewRequest={(chosenRequest, index) => {
                         this.setState({
                            resellerCode: chosenRequest.value.code
                         })
                     }}
                     onUpdateInput={(searchText, dataSource) => {}}
                     onBlur={e => {
                         this.setState({
                             "resellerCode": e.target.value
                         })
                     }}
                   />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Product Name</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                  <AutoComplete
                     searchText={this.state.productName}
                     filter={AutoComplete.fuzzyFilter}
                     key="product-name-field" id="product-name-field"
                     ref={r=> { this.productNameField = r }}
                     openOnFocus={true}
                     fullWidth={true}
                     dataSource={this.state.productsByName}
                     dataSourceConfig={dataSourceConfig}
                     onNewRequest={(chosenRequest, index) => {
                         if (!!chosenRequest.value.map && chosenRequest.value.map[0] != "") {
                            this.setState({
                                productName: chosenRequest.value.map.from.productNames[0],
                                product: chosenRequest.value.map.from.productNames[0]
                            })
                         }
                     }}
                     onUpdateInput={(searchText, dataSource) => {}}
                     onBlur={e => {
                         console.log("Blur !!!!!   !!! ", e.target.value)
                         if (e.target.value != "") {
                            this.setState({
                             productName: e.target.value,
                             product: e.target.value
                            })
                         }
                     }}
                   />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Product Code</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                  <AutoComplete
                     searchText={this.state.productCode}
                     filter={AutoComplete.fuzzyFilter}
                     key="product-code-field" id="product-code-field"
                     ref={r=> { this.productCodeField = r }}
                     openOnFocus={true}
                     fullWidth={true}
                     dataSource={this.state.productsByCode}
                     dataSourceConfig={dataSourceConfig}
                     onNewRequest={(chosenRequest, index) => {
                         this.setState({
                            productCode: chosenRequest.value.code
                         })
                     }}
                     onUpdateInput={(searchText, dataSource) => {}}
                     onBlur={e => {
                         this.setState({
                             "productCode": e.target.value
                         })
                     }}
                   />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Adults</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                    <TextField
                      defaultValue={this.state.numberOfAdults}
                      key="numberOfAdults-textfield" id="numberOfAdults-textfield"
                      errorText={this.state.adultsError}
                      onBlur={e => {
                          this.setState({
                              "numberOfAdults": e.target.value
                          })
                      }}
                    />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Children</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                      <TextField
                        defaultValue={this.state.numberOfChildren}
                        key="numberOfChildren-textfield" id="numberOfChildren-textfield"
                        errorText={this.state.childrenError}
                        onBlur={e => {
                            this.setState({
                                "numberOfChildren": e.target.value
                            })
                        }}
                      />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Infants</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                    <TextField
                      defaultValue={this.state.numberOfInfants}
                      key="numberOfInfants-textfield" id="numberOfInfants-textfield"
                      errorText={this.state.infantsError}
                      onBlur={e => {
                          this.setState({
                              "numberOfInfants": e.target.value
                          })
                      }}
                    />
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Seniors</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                      <TextField
                        defaultValue={this.state.numberOfSeniors}
                        key="numberOfSeniors-textfield" id="numberOfSeniors-textfield"
                        errorText={this.state.seniorsError}
                        onBlur={e => {
                            this.setState({
                                "numberOfSeniors": e.target.value
                            })
                        }}
                      />
              </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Students</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                      <TextField
                        defaultValue={this.state.numberOfStudents}
                        key="numberOfStudents-textfield" id="numberOfStudents-textfield"
                        errorText={this.state.studentsError}
                        onBlur={e => {
                            this.setState({
                                "numberOfStudents": e.target.value
                            })
                        }}
                      />
              </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Supplier Reference Number</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                      <TextField
                        defaultValue={this.state.supplierReferenceNumber}
                        key="supplierReferenceNumber-textfield" id="supplierReferenceNumber-textfield"
                        onBlur={e => {
                            this.setState({
                                "supplierReferenceNumber": e.target.value
                            })
                        }}
                      />
              </TableRowColumn>
              </TableRow>
              <TableRow selectable={false} >
                  <TableRowColumn key={"1.1"}>Reseller Reference Number</TableRowColumn>
                  <TableRowColumn key={"1.2"} style={styles.showOverflow}>
                      <TextField
                        defaultValue={this.state.resellerReferenceNumber}
                        key="resellerReferenceNumber-textfield" id="resellerReferenceNumber-textfield"
                        onBlur={e => {
                            this.setState({
                                "resellerReferenceNumber": e.target.value
                            })
                        }}
                      />
              </TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
      )
  }

  renderError (err) {
        let msg = ""
        if (err !== false) {
          if (typeof err == "string") {
            msg = err
          } else {
            msg = err.data.Error
          }
        }
        return (
          <span className="correction-error">{msg}</span>
        )
  }

  render() {
      let createError = this.props.createError,
          images = this.props.images,
          fetching = this.props.fetching
                  || this.props.savingVoucher;
    return (
        <div className="supplier-edit voucher-edit add-redeemed-voucher">
            <div style={styles.headerControls}>
              <h2>Add Redeemed Voucher</h2>
              <span className="correct-button" style={styles.submitButton}>
                  <RaisedButton className="save-button" label="Submit" primary={true}
                                style={{marginLeft: "1em"}} onClick={e => this.save()} />
              </span>
              {this.renderError(createError)}
              <RaisedButton onTouchTap={e => { this.returnToSummary() } }
                            className="no-print"
                            style={styles.returnToSummaryButton}
                            label="Return to Summary"
              />
            </div>
            <section className="image">
            {
              images.map((image, i) => {
              return image == '' ? '' : (
                <a key={i} href={image} title="Open In New Tab" target="_blank" className="voucher-detail-image-link">
                    <img src={image} title="Open In New Tab" className="voucher-image" style={{cursor: "pointer"}} />
                </a>
              )})
            }
            </section>
            <section className="tables" style={{marginBottom: '6em'}}>
                { !fetching ? this.renderForm() :
                    <div className="loading-circle">
                    <CircularProgress color={"#27367a"} size={96} />
                    </div>
                }
            </section>
      </div>
    );
  }
}

AddRedeemedVoucher.propTypes = {

};

import { connect } from 'react-redux';
import {
    correctVoucher,
    getVoucher,
    getVoucherImage,
    createRedeemedVoucher
} from '../../ax-redux/actions/vouchers'
import {
    listResellers,
    listSupplierResellers
} from '../../ax-redux/actions/resellers'
import {
    listSupplierResellerCatalogItems
} from '../../ax-redux/actions/suppliers'
import {
  listAccounts
} from '../../ax-redux/actions/accounts'

export default connect(
  (state, ownProps) => {
    return {
        role: state.app.role,
        supplierId: !!state.app.user ? state.app.user.supplierId : "",
        adminSupplierId: state.reportFilters.adminSupplierId,
        orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
        voucher: state.vouchers.getVoucher.data,
        savingVoucher: state.vouchers.correctVoucher.fetching,
        createResponse: state.vouchers.createRedeemedVoucher.data,
        createError: state.vouchers.createRedeemedVoucher.error,
        fetching: state.vouchers.getVoucher.fetching || state.vouchers.rejectVoucher.fetching,
        fetchingVoucher: state.vouchers.getVoucher.fetching,
        images: [""],
        resellers: isSupplierStaff(state.app.role) ? (!!state.resellers.listBySupplier.data ? state.resellers.listBySupplier.data.resellers : false) :
                                                     (!!state.resellers.list.data ? state.resellers.list.data.resellers : false),
        fetchingResellers: isSupplierStaff(state.app.role) ? state.resellers.listBySupplier.fetching  : state.resellers.list.fetching,
        products: !!state.suppliers.listResellerCatalogItems.data && !!state.suppliers.listResellerCatalogItems.data.catalogItems ? state.suppliers.listResellerCatalogItems.data.catalogItems : false,
        fetchingProducts: state.suppliers.listResellerCatalogItems.fetching,
        accounts: state.accounts.list.data,
        fetchingAccounts: state.accounts.list.fetching,
        returnToShiftURL: state.navigation.returnToShiftURL,
    }
  },
  dispatch => {
    return {
      getVoucher: (id) => {
        dispatch(getVoucher(id))
      },
      correctVoucher: (id, data) => {
        dispatch(correctVoucher(id, data))
      },
      getVoucherImage: (id) => {
          dispatch(getVoucherImage(id))
      },
      listResellers: () => {
          dispatch(listResellers())
      },
      listSupplierResellers: ( supplierCode ) => {

        dispatch( listSupplierResellers( supplierCode ) )

      },
      listSupplierResellerCatalogItems: (supplierCode, resellerCode, params) => {
          dispatch(listSupplierResellerCatalogItems(supplierCode, resellerCode, params))
      },
      listAccounts: (params) => {
        dispatch(listAccounts(params))
      },
      createRedeemedVoucher: (data) => {
        dispatch(createRedeemedVoucher(data))
      }
    }
  }
)(AddRedeemedVoucher)
