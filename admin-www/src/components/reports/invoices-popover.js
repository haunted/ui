import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'

class InvoicesPopover extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      label: "Invoices",
      lookedUpName: false
    }
  }

  fetch(supplierCode) {
    this.props.listResellerInvoices({ supplierCode })
  }

  componentWillMount() {
    this.fetch(this.props.supplierCode ? this.props.supplierCode : this.props.orgCode)
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.supplierCode != nextProps.supplierCode) {
        this.fetch(nextProps.supplierCode)
    }
    if (!!this.props.selectDefault && (this.props.invoices.data == false) && nextProps.invoices != false) {

        if (nextProps.invoices.data  != null) {

            if (nextProps.invoices.data.invoices  != null) {

                    this.props.onInvoice(nextProps.invoices.data.invoices[0])
                    this.setState({
                        label: this.label(nextProps.invoices.data.invoices[0]),
                    })
                }
            }
      }
      if (nextState.lookedUpName === false && nextProps.invoices.data !== false &&
          nextProps.defaultLabel != null && nextProps.defaultLabel != "")
      {
        nextProps.invoices.data.invoices.map((v, i) => {
          if (v.resellerCode == nextProps.defaultLabel) {
            this.setState({
              lookedUpName: true,
              label: this.label(v)
            })
          }
        })
      }
  }

  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
    console.log(event.currentTarget);
  }

  handleRequestClose() {
    this.setState({
      open: false,
    })
  }
  label(v) {
    if (v.resellerName != "") {
      return v.resellerName
    }
    return v.resellerCode
  }
  onMenuItem(e, item, i) {
    this.props.onInvoice(item.props.value)
    this.setState({
      open: false,
      label: this.label(item.props.value),
    })
  }

  render() {
    return (
      <div className="reseller-popover">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.state.label}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={() => this.handleRequestClose()}
        >
          <Menu onItemTouchTap={(e, item, i) => this.onMenuItem(e, item, i)}>
            {
                this.props.invoices.fetching ?
                  <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={0.5} /> :
                  !!this.props.invoices.data && !!this.props.invoices.data.invoices &&
                  this.props.invoices.data.invoices.map(v => <MenuItem key={v.resellerName} primaryText={this.label(v)} value={v} />)
            }
          </Menu>
        </Popover>
      </div>
    );
  }
}
InvoicesPopover.propTypes = {
  onInvoice: PropTypes.func.isRequired,
  selectDefault: PropTypes.bool,
  defaultLabel: PropTypes.string
}

import { connect } from 'react-redux'

import { listResellerInvoices } from '../../ax-redux/actions/finances'
export default connect(
  state => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      invoices: state.finances.listResellerInvoices,
    }
  },
  dispatch => {
    return {
      listResellerInvoices: (data) => {
          dispatch(listResellerInvoices(data))
      }
    }
  }
)(InvoicesPopover)
