import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete';

const dataSourceConfig = {
    text: 'text',
    value: 'value',
};

class InvoicesAutocomplete extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      label: "Invoices",
      lookedUpName: false,
      resellerName: "",
      resellerCode: "",
      invoicesByName: [],
      invoicesByCode: []
    }
  }

  generateInvoiceAutoComplete (invoices) {
      let byName = [],
          byCode = [],
          uniqueCodes = [];

      invoices.map((invoice) => {
          let code = "";
          if (invoice.resellerCode.indexOf(" ") > -1) { // if there's a space, it's not a code
            return;
          }
          if (uniqueCodes[invoice.resellerName] == null) {
            uniqueCodes[invoice.resellerName] = invoice.resellerCode;
          } else {
            code = ` (${invoice.resellerCode})`;
          }
          byName.push({
              text: invoice.resellerName + code,
              value: invoice.resellerCode
          });
          byCode.push({
              text: invoice.resellerCode,
              value: invoice.resellerName
          });
      })
      this.setState({
          invoicesByName: byName,
          invoicesByCode: byCode
      })
  }

  fetch(supplierCode) {
    this.props.listResellerInvoices({ supplierCode })
  }

  componentWillMount() {

    if (this.props.invoices.data != false && this.props.invoices.data.invoices != false) {
      console.log("generating invoices autocomplete ")
      this.generateInvoiceAutoComplete(this.props.invoices.data.invoices)
    } else {
      console.log("fetching invoices")
      this.fetch(this.props.supplierCode ? this.props.supplierCode : this.props.orgCode)
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.supplierCode != nextProps.supplierCode) {
        this.fetch(nextProps.supplierCode)
    }
    if (!!this.props.selectDefault && (this.props.invoices.data == false) && nextProps.invoices != false) {
        if (nextProps.invoices.data  != null) {
            if (nextProps.invoices.data.invoices  != null) {
                    this.props.onInvoice(nextProps.invoices.data.invoices[0])
                    this.setState({
                        label: this.label(nextProps.invoices.data.invoices[0]),
                    })
                }
            }
      }
      if ((nextState.lookedUpName === false && nextProps.invoices.data !== false &&
          nextProps.selectedResellerCode != null && nextProps.selectedResellerCode != ""))
      {
        console.log("selected reseller code ", nextProps.selectedResellerCode);
        console.log("looking up reseller name from code... ");
        nextProps.invoices.data.invoices.map((v, i) => {
          if (v.resellerCode == nextProps.selectedResellerCode) {
            this.setState({
              lookedUpName: true,
              label: this.label(v),
              resellerName: this.label(v)
            })
          }
        })
      }
      if (nextState.resellerName != "" && this.state.resellerName != nextState.resellerName) {
            this.setState({
              lookedUpName: true,
              label: nextState.resellerName
            })
      }
      if (this.props.invoices.fetching && !nextProps.invoices.fetching && nextProps.invoices.data != false) {
        this.generateInvoiceAutoComplete(nextProps.invoices.data.invoices)
      }
  }

  label(v) {
    if (v.resellerName != "") {
      return v.resellerName
    }
    return v.resellerCode
  }

  render() {
    return (
      <div className="reseller-autocomplete-container">
            {
                this.props.invoices.fetching && (!!!this.props.invoices.data || !!!this.props.invoices.data.invoices) ?
                  <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={0.5} /> : (
                    <AutoComplete
                        hintText="Reseller Name"
                        searchText={this.state.resellerName}
                        filter={AutoComplete.fuzzyFilter}
                        key="reseller-name-field" id="reseller-name-field"
                        openOnFocus={true}
                        fullWidth={true}
                        menuProps={{ maxHeight: 600 }}
                        dataSource={this.state.invoicesByName}
                        dataSourceConfig={dataSourceConfig}
                        onNewRequest={(chosenRequest, index) => {
                          let resellerName = "";
                          if (typeof chosenRequest == "string") {
                            resellerName = chosenRequest;
                          } else {
                            resellerName = chosenRequest.text;
                            this.setState({
                              resellerCode: chosenRequest.value.code
                            })
                            console.log("autocompleting invoice.. ", chosenRequest.value)
                            this.props.onInvoice({
                              resellerCode: chosenRequest.value,
                              resellerName: chosenRequest.text
                            })
                          }
                          this.setState({
                            resellerName: resellerName
                          })
                        }}
                        onUpdateInput={(searchText, dataSource) => {
                         
                          if ( searchText == "" ) {

                              this.props.onInvoice({
                                  resellerCode: "",
                                  resellerName: ""
                              })

                          }

                        }}
                        onBlur={e => {
                          this.setState({
                            "resellerName": e.target.value
                          })
                        }}
                    />
                  )
            }
      </div>
    )
  }
}

// this.props.invoices.data.invoices.map(v => <MenuItem key={v.resellerName} primaryText={this.label(v)} value={v} />

InvoicesAutocomplete.propTypes = {
  onInvoice: PropTypes.func.isRequired,
  selectDefault: PropTypes.bool,
  selectedResellerCode: PropTypes.string
}

import { connect } from 'react-redux'

import { listResellerInvoices } from '../../ax-redux/actions/finances'
export default connect(
  state => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      invoices: state.finances.listResellerInvoices,
    }
  },
  dispatch => {
    return {
      listResellerInvoices: (data) => {
          dispatch(listResellerInvoices(data))
      }
    }
  }
)(InvoicesAutocomplete)
