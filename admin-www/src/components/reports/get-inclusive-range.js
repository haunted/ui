import moment from 'moment-timezone'
// import moment from 'moment'

let getInclusiveRange = (startVal, endVal) => {

    let start = moment.tz(startVal, TZ).toDate(),
        end = moment.tz(endVal, TZ).toDate(),
        offset = - moment().utcOffset(),
        estOffset = (offset-240),
        offsetHours = Math.floor(estOffset / 60),
        offsetMinutes = estOffset % 60

    start.setHours(0);
    start.setMinutes(0);
    start.setSeconds(0);
    start = start.getTime();
    end.setHours(23-offsetHours);
    end.setMinutes(59-offsetMinutes);
    end.setSeconds(59);
    end = end.getTime();

    return [start, end];
}

export default getInclusiveRange
