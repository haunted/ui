import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

class PrintButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
    }
  }

  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
    console.log(event.currentTarget);
  }

  handleRequestClose() {
    this.setState({
      open: false,
    })
  }

  onMenuItem(e, item, i) {
    this.props.onExport(item.props.value)
    this.setState({
      open: false,
    })
  }

  render() {
      let buttonStyle = (this.props.style !=null ? this.props.style : {});
    return (
      <div style={buttonStyle} className="no-print print-button-container">
        <RaisedButton
          onTouchTap={e => this.props.onPrint() }
          label={this.props.printMode ? "Exit Print Preview" : "Print"}
        />
      </div>
    );
  }
}
PrintButton.propTypes = {
  onPrint: PropTypes.func.isRequired,
  printMode: PropTypes.bool.isRequired,
  style: PropTypes.object
}

export default PrintButton
