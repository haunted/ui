import React, { Component, PropTypes } from 'react'
import Toggle from 'material-ui/Toggle';
import Popover from 'material-ui/Popover';
import RaisedButton from 'material-ui/RaisedButton';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

const styles = {
  block: {
    maxWidth: 250,
    padding: "1em",
  },
  toggle: {
    marginBottom: "1em",
  },
}

export default class TourFilters extends Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
    };
  }

  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  }

  handleRequestClose() {
    this.setState({
      open: false,
    });
  }

  onToggle(tour) {
    let selectedTours = []

    this.props.tours.map(v => {
      if (v == tour) {
        if (!this.props.tourState(v)) {
          selectedTours.push(v)
        }  
      } else if (this.props.tourState(v)) {
        selectedTours.push(v)
      }
    })

    this.props.onSetSelectedTours(selectedTours)
  }

  render() {    
    return (
      <div className="tour-filters">
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label="Filter Products"
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={() => this.handleRequestClose()}
        >
          <div style={styles.block}>
          {
            this.props.tours.map((tour, i) => {
                return (
                  <Toggle
                    key={i}
                    label={tour}
                    toggled={this.props.tourState(tour)}
                    onToggle={() => this.onToggle(tour)}
                    style={styles.toggle}
                  />
                )
            })
          }
          </div>
        </Popover>
      </div>
    );
  }
}