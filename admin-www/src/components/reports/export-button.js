import React, { Component, PropTypes } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';


class ExportButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
    }
  }

  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault()

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    })
    console.log(event.currentTarget);
  }

  handleRequestClose() {
    this.setState({
      open: false,
    })
  }

  onMenuItem(e, item, i) {
    this.props.onExport(item.props.value)
    this.setState({
      open: false,
    })
  }

  render() {
      let buttonStyle = (this.props.style !=null ? this.props.style : {});
    return (
      <div style={buttonStyle} className={"export-button " + (this.props.buttonClass || "")}>
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label="Export"
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={() => this.handleRequestClose()}
        >
          <Menu onItemTouchTap={(e, item, i) => this.onMenuItem(e, item, i)}>
            <MenuItem primaryText="CSV" value="CSV" />
            <MenuItem primaryText="Excel" value="XLSX" />
          </Menu>
        </Popover>
      </div>
    );
  }
}
ExportButton.propTypes = {
  onExport: PropTypes.func.isRequired,
  style: PropTypes.object,
  buttonClass: PropTypes.string
}

export default ExportButton
