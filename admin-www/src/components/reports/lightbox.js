import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'

class LightBox extends Component {

  constructor(props) {
    super(props);

    this.state = {
      enabled: false,
      initialLoad: false,
      paginationOpen: false,
      anchorEl: null,
      currentImage: 0
    }
  }

  toggleLightbox () {
    this.setState({
        enabled: !this.state.enabled
    })
  }

  render() {

      let currentImage = this.state.currentImage,
          fetching = this.props.fetching;

      if ( this.state.enabled ) {

          return (
              <div className="ticket-lightbox"
                   title="Close"
                   onClick={ e => {
                       
                       let elementClass = e.target.getAttribute("class"),
                           tagname = e.target.tagName.toLowerCase();

                       if ( tagname != "span" &&
                            elementClass != "inner-lightbox" &&
                            elementClass != "lightbox-pagination-button" ) {

                           this.toggleLightbox()
                       
                       }
              }}>
                  <div title="Open In New Tab" className="inner-lightbox" style={{backgroundImage: "url("+this.props.images[ currentImage ]+")"}}
                       onClick={ e => {

                          let elementClass = e.target.getAttribute("class"),
                              tagname = e.target.tagName.toLowerCase();

                           if ( tagname != "span" &&
                                elementClass != "close-lightbox" &&
                                elementClass != "lightbox-pagination-button") {

                               let imageTab = window.open( this.props.images[ currentImage ], '_blank' )

                               if ( imageTab ) {

                                   imageTab.focus();

                               } else {

                                   alert("Popups must be enabled for this site.")

                               }
                           }

                      }
                  }>
                  {fetching ?(
                    <CircularProgress className="lightbox-circular-progress" color={"#27367a"} size={96} />
                  ): ""}
                    <div className="close-lightbox"
                         title="Close"
                         onClick={(evt) => { this.toggleLightbox() }}>x</div>
                  { !fetching && this.props.images.length > 1 && this.renderPagination() }
                  </div>
              </div>
          )
      } else {
          return (
              <RaisedButton
                    onTouchTap={e => {
                        this.props.getVoucherImage(this.props.voucherId);
                        this.toggleLightbox();
                    } }
                    className="no-print"
                    label={this.props.title}
                    disabled={this.props.disabled}
               />
          )
      }
  }

  handleTouchTapPages (event) {
    event.preventDefault();
    this.setState({
        paginationOpen: true,
        anchorEl: event.currentTarget
    });
  }
  handleRequestClosePages (evt) {
    this.setState({
       paginationOpen: false
    });
  }

  renderPagination () {
    let pages = this.props.images.length;
    return (
      <div className="lightbox-pagination">
        {[<RaisedButton
          onTouchTap={ e=>this.handleTouchTapPages(e) }
          label={`Page ${(this.state.currentImage+1)} Of ${pages}`}
          className="lightbox-pagination-button"
        />,
        <Popover
          open={this.state.paginationOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={ e=>this.handleRequestClosePages(e) }
        >
          <Menu>
            {
                this.props.images.map((image, p) => {
                        return (
                            <MenuItem onClick={()=>{ this.setState({currentImage: p})}}
                                      key={p} primaryText={"Page "+(p+1)}
                            />
                        )
                })
            }
          </Menu>
        </Popover>]}
      </div>
    )
  }
}

import { connect } from 'react-redux'
import {
    getVoucherImage
} from '../../ax-redux/actions/vouchers'

export default connect(
  (state, ownProps) => ({
        voucherId: ownProps.voucherId,
        disabled: ownProps.disabled,
        title: ownProps.title,
        style: ownProps.style,
        fetching: state.vouchers.getVoucherImage.fetching,
        images: !!state.vouchers.getVoucherImage.data ? (
          !!state.vouchers.getVoucherImage.data.urls ? state.vouchers.getVoucherImage.data.urls  :
          [state.vouchers.getVoucherImage.data.url]
        ) : [""]
    }),
  dispatch => ({
    getVoucherImage: (id) => {
        dispatch(getVoucherImage(id))
    }
  })
)(LightBox);
