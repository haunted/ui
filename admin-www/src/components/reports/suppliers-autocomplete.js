import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete';

const dataSourceConfig = {
    text: 'text',
    value: 'value',
};

class SuppliersAutoComplete extends Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      label: "Suppliers",
      lookedUpName: false,
      supplierName: "",
      supplierCode: "",
      suppliersByName: [],
      suppliersByCode: []
    }
  }

  generateSupplierAutocomplete ( suppliers ) {

      let byName = [],
          byCode = [],
          uniqueCodes = [];

      suppliers.map( s => {

          let code = "",
              supplier = s.supplier

          if ( supplier.code.indexOf(" ") > -1 )  // if there's a space, it's not a code
            
            return

          if ( uniqueCodes[ supplier.name ] == null ) {

            uniqueCodes[ supplier.name ] = supplier.code

          } else {

            code = ` (${supplier.code})`

          }

          byName.push({
              text: supplier.name + code,
              value: supplier.code
          })
          byCode.push({
              text: supplier.code,
              value: supplier.name
          })

      })

      this.setState({
          suppliersByName: byName,
          suppliersByCode: byCode
      })

  }

  fetch ( ) {

    this.props.listSuppliers()

  }

  componentWillMount ( ) {

      if ( this.props.suppliers.length != 0 && this.props.suppliersFetching == false ) {
      
        this.generateSupplierAutocomplete( this.props.suppliers )

      } else {
   
        this.fetch()

      }
  }

  componentWillReceiveProps ( nextProps ) {

    if ( this.props.supplierCode != nextProps.supplierCode )

        this.fetch( nextProps.supplierCode )


    if ( !!this.props.selectDefault && this.props.suppliers.length == 0 && nextProps.suppliers.length > 0 )

        this.props.onSupplier( nextProps.suppliers[ 0 ] )
        this.setState({
           label: this.label( nextProps.suppliers[ 0 ] ),
        })


    if ( this.props.suppliersFetching && !nextProps.suppliersFetching && nextProps.suppliers.length != 0 )
      
      this.generateSupplierAutocomplete( nextProps.suppliers )


  }

  componentWillUpdate ( nextProps, nextState ) {

    if ( nextState.lookedUpName === false && nextProps.suppliers.length != 0 && nextProps.selectedSupplierCode != null && nextProps.selectedSupplierCode != "" )
        
      nextProps.suppliers.map( ( v, i ) => {

        if ( v.supplierCode == nextProps.selectedSupplierCode )

          this.setState({
            lookedUpName: true,
            label: this.label(v),
            supplierCode: this.label(v)
          })
            
      })


    if ( nextState.supplierCode != "" && this.state.supplierCode != nextState.supplierCode )

      this.setState({
        lookedUpName: true,
        label: nextState.supplierCode
      })

      
  }

  label( v ) {
    if (v.name != "") {
      return v.name
    }
    return v.code
  }

  render() {
    return (
      <div className="supplier-autocomplete-container" style={ this.props.buttonStyle != undefined ? this.props.buttonStyle : {} }>
            {
                this.props.suppliersFetching && (this.props.suppliers.length == 0) ?
                  <CircularProgress className="redeemed-circular-progress" color={"#27367a"} size={0.5} /> : (
                    <AutoComplete
                        hintText="Supplier Name"
                        searchText={this.state.supplierCode}
                        filter={AutoComplete.fuzzyFilter}
                        key="supplier-name-field" id="supplier-name-field"
                        openOnFocus={true}
                        fullWidth={true}
                        menuProps={{ maxHeight: 600 }}
                        dataSource={this.state.suppliersByName}
                        dataSourceConfig={dataSourceConfig}
                        onNewRequest={(chosenRequest, index) => {
                          let supplierCode = "";
                          if (typeof chosenRequest == "string") {
                            supplierCode = chosenRequest;
                          } else {
                            supplierCode = chosenRequest.text;
                            this.setState({
                              supplierCode: chosenRequest.value.code
                            })
                            this.props.onSupplier({
                              code: chosenRequest.value,
                              name: chosenRequest.text
                            })
                          }
                          this.setState({
                            supplierCode: supplierCode
                          })
                        }}
                        onUpdateInput={(searchText, dataSource) => {
                        }}
                        onBlur={e => {
                          this.setState({
                            "supplierCode": e.target.value
                          })
                        }}
                    />
                  )
            }
      </div>
    )
  }
}

SuppliersAutoComplete.propTypes = {
  onSupplier: PropTypes.func.isRequired,
  selectDefault: PropTypes.bool,
  selectedSupplierCode: PropTypes.string,
  buttonStyle: PropTypes.object
}

import { connect } from 'react-redux'

import { listSuppliers } from '../../ax-redux/actions/suppliers'
export default connect(
  (state, ownProps) => {
    return {
      orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
      suppliers: state.suppliers.list.data != false ? state.suppliers.list.data.suppliers: [],
      suppliersFetching: state.suppliers.list.fetching,
      buttonStyle: ownProps.buttonStyle
    }
  },
  dispatch => {
    return {
      listSuppliers: (data) => {
          dispatch(listSuppliers(data))
      }
    }
  }
)(SuppliersAutoComplete)
