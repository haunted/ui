import React, { Component, PropTypes } from 'react'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table'
import Paper from 'material-ui/Paper'
import Toggle from 'material-ui/Toggle';
import DatePicker from 'material-ui/DatePicker';
import CircularProgress from 'material-ui/CircularProgress'
import ExportButton from './export-button'
import PrintButton from './print-button'
import { browserHistory } from 'react-router'
import moment from 'moment-timezone'
import SuppliersPopover from './suppliers-popover'
import getInclusiveRange from './get-inclusive-range'
import validateDateRange from './validate-date-range'
import formatColumn from './format-column'
import { formatPrice } from '../../util'

let self = null;

class ExceptionsTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
        start: new Date(),
        end: new Date(),
        supplierCode: "",
        sort: "",
    }
    self = this;
  }
  componentDidMount() {
    let start = new Date(),
        end = new Date(),
        sort = "",
        supplierCode = this.props.orgCode,
        dateRange = [],
        query = this.props.location.query;

    if (this.props.params && this.props.params.supplierCode) {
      supplierCode = this.props.params.supplierCode
      this.props.setSupplierCode(supplierCode)
    } else {
      supplierCode = this.props.filters.supplierCode
    }
    if (query && query.start) {
      start = new Date(this.props.location.query.start*1000);
      end = new Date(this.props.location.query.end*1000);
    } else {
      start = this.props.filters.startDate
      end = this.props.filters.endDate
    }
    if (query && query.sort) {
      sort = this.props.location.query.sort;
    }

    dateRange = getInclusiveRange(start, end);
    this.setState({
        start: new Date(dateRange[0]),
        end: new Date(dateRange[1]),
        supplierCode,
        sort
    })
    this.props.fetch({
        start: dateRange[0],
        end: dateRange[1],
        supplierCode,
        sort
    })
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.start != nextState.start ||
        this.state.end != nextState.end ||
        this.state.supplierCode != nextState.supplierCode)
    {
        browserHistory.push(`/app/reports/exceptions${(nextState.supplierCode !="" ?("/"+nextState.supplierCode):"")}?start=${moment(nextState.start).tz(TZ).unix()}&end=${moment(nextState.end).tz(TZ).unix()}&sort=${nextState.sort}`);
    }
    if (this.props.pathname != nextProps.pathname ||
        this.props.queryParams != nextProps.queryParams) {
          let range = getInclusiveRange(nextState.start, nextState.end),
              requestParams = {
                start: range[0],
                end: range[1],
                supplierCode: nextState.supplierCode,
          };
          if (nextProps.location.query.sort) {
              requestParams.sort = nextProps.location.query.sort;
          }
          this.props.fetch(requestParams);
    }
  }
  setStartDate(date) {
      this.setState({
        start: date
      })
      this.props.setStartDate(date)
  }
  setEndDate(date) {
      this.setState({
        end: date
      })
      this.props.setStartDate(date)
  }
  setSupplier(supplier) {
    console.log(supplier)
    this.setState({ supplierCode: supplier.supplier.code })
    this.props.setSupplierCode(supplier.supplier.code)
  }
  export(format) {
    let range = getInclusiveRange(this.state.start, this.state.end);
    this.props.export({
      start: range[0],
      end: range[1],
      supplierCode: this.state.supplierCode,
    }, format)
  }
  print () {
      if (!this.props.printMode) {
          setTimeout(()=>{
              window.print()
          }, 500);
      }
      this.props.togglePrintMode();
  }
  rowClicked(event, id) {
    event.preventDefault() // This prevents ghost click.
    let supplierCode = this.state.supplierCode,
        start = this.state.start,
        end = this.state.end,
        sort = this.state.sort;

    browserHistory.push(`/app/reports/exceptions${supplierCode!=""?"/"+supplierCode:""}/${id}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}&sort=${sort}`);
  }
  shouldDisableStartDate (day) {
    return  !validateDateRange(day.getTime(), self.state.end);
  }
  shouldDisableEndDate (day) {
     return  !validateDateRange(self.state.start, day.getTime());
  }
  sortBy(column) {
    let supplierCode = this.state.supplierCode,
        start = this.state.start,
        end = this.state.end,
        sort = column;

    if (sort == this.state.sort) {
        if (sort.indexOf("-") > -1) {
          sort = sort.replace("-", "");
        } else {
          sort = "-"+sort;
        }
    }
    this.setState({
        sort: sort
    });
    browserHistory.push(`/app/reports/exceptions${(supplierCode!=""?("/"+supplierCode):"")}?start=${moment(start).tz(TZ).unix()}&end=${moment(end).tz(TZ).unix()}&sort=${sort}`);
  }

  render() {

      let startDate = moment(this.state.start),
            endDate = moment(this.state.end),
            dateString = startDate.format("DD-MMM-YYYY"),
            dateEndString = endDate.format("DD-MMM-YYYY"),
            summaryMode = (this.props.children == null || this.props.children.length < 1)

      if ( endDate.diff( startDate ) > 86400000 ) { // more than one day
          dateString = `( ${dateString} - ${dateEndString} )`;
      }

    return (
      <div className="exceptions">
        <div className="exceptions-header" style={{display: (summaryMode ? "inherit" : "none")}}>
          <div className="exceptions-header-left">
            <h2 style={{color:"#434343"}}>
                Exceptions
                <span className="title-date">
                    { dateString }
                </span>
            </h2>
          </div>
          <div className="exceptions-header-right">
          <div className={"no-print report-controls"+ (this.props.printMode?" print-mode" : "")}>
              <DatePicker
                  floatingLabelText="Start Date"
                  hintText="Date"
                  className="date-picker"
                  formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                  value={this.state.start}
                  shouldDisableDate={this.shouldDisableStartDate}
                  onChange={(ev, date) => this.setStartDate(date)}
              />
              <DatePicker
                  floatingLabelText="End Date"
                  hintText="Date"
                  className="date-picker"
                  formatDate={(date)=> moment(date).tz(TZ).format("DD-MMM-YYYY")}
                  value={this.state.end}
                  shouldDisableDate={this.shouldDisableEndDate}
                  onChange={(ev, date) => this.setEndDate(date)}
              />
              {
                this.props.role == ACTOUREX_TECHNICAL_STAFF ?
                <SuppliersPopover defaultLabel={this.state.supplierCode}
                                  onSupplier={supplier => this.setSupplier(supplier)} /> : []
              }
              <ExportButton
                style={{marginLeft: "1em"}} onExport={format => this.export(format)}
              />
          </div>
          <PrintButton
             style={{marginLeft: "1em"}}
             onPrint={data=> { this.print() }} printMode={this.props.printMode}
          />
          </div>
      </div>
      <div className="report-details">
          {this.props.children}
      </div>
      <TableSettings callback={ c => { this.setState({cols: c}) } }
                     columns={["name", "mobile", "tour", "option", "travelers", "date", "language", "source"]}
      />
         {
           this.props.exceptions.fetching ? (<CircularProgress className="exceptions-circular-progress" color={"#27367a"} size={96} />) :
             this.props.children == null || this.props.children.length < 1 ? (
             <Table className="index" selectable={false}>
              <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                <TableRow>
                  <TableHeaderColumn className="sortable">
                    <div className="inner" onClick={ e => this.sortBy("name") }>Name</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner" onClick={ e => this.sortBy("mobile") }>Mobile</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner" onClick={ e => this.sortBy("tour") }>Tour</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner" onClick={ e => this.sortBy("option") }>Option</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner" onClick={ e => this.sortBy("travelers") }>Travelers</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner" onClick={ e => this.sortBy("date") }>Date</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner" onClick={ e => this.sortBy("language") }>Language</div>
                  </TableHeaderColumn>
                  <TableHeaderColumn className="sortable">
                    <div className="inner" onClick={ e => this.sortBy("source") }>Source</div>
                  </TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                {
                  this.props.exceptions.data.map((v,i) => (
                    <TableRow key={i} onTouchTap={e => this.rowClicked(e, v.id)}>
                      <TableRowColumn>
                          {v.name}
                      </TableRowColumn>
                      <TableRowColumn>
                          {v.mobile}
                      </TableRowColumn>
                      <TableRowColumn>
                          {v.tour}
                      </TableRowColumn>
                      <TableRowColumn>
                          {v.option}
                      </TableRowColumn>
                      <TableRowColumn>
                          {v.tickets}
                      </TableRowColumn>
                      <TableRowColumn>
                          {v.travelTime != "" ? moment.unix(parseInt(v.travelTime)).tz(TZ).format('DD-MMM-YYYY') : "—"}
                      </TableRowColumn>
                      <TableRowColumn>
                          {v.language}
                      </TableRowColumn>
                      <TableRowColumn>
                          {v.source}
                      </TableRowColumn>
                    </TableRow>
                  ))
                }
              </TableBody>
            </Table>
            ) : ""
        }
      </div>
    )
  }

}

import { connect } from 'react-redux'
import {
  fetch,
  exportCSV,
  exportXLSX,
} from '../../ax-redux/actions/ticket-exceptions'
import {
  setSupplierCode,
  setStartDate,
  setEndDate
} from '../../ax-redux/actions/report-filters'
import {
    setPageTitle,
    togglePrintMode
} from '../../ax-redux/actions/app'
import { ACTOUREX_TECHNICAL_STAFF } from '../../ax-redux/constants'

export default connect(
  state => ({
    exceptions: state.exceptions,
    role: state.app.role,
    orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
    printMode: state.app.printMode,
    pathname: state.routing.locationBeforeTransitions.pathname,
    queryParams: state.routing.locationBeforeTransitions.search,
    filters: state.reportFilters
  }),
  dispatch => ({
    setPageTitle: title => {
      dispatch(setPageTitle(title))
    },
    fetch: (opts) => {
      dispatch(fetch(opts))
    },
    togglePrintMode: (opts) => {
        dispatch(togglePrintMode(opts))
    },
    export: (opts, format) => {
      switch (format) {
        case "CSV":
          dispatch(exportCSV(opts))
          return
        case "XLSX":
          dispatch(exportXLSX(opts))
          return
      }
    },
    setSupplierCode: data => {
      dispatch(setSupplierCode(data))
    },
    setStartDate: data => {
      dispatch(setStartDate(data))
    },
    setEndDate: data => {
      dispatch(setEndDate(data))
    }
  })
)(ExceptionsTable);
