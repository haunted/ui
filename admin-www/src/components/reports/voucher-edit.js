import React, {PropTypes, Component} from 'react'
import { browserHistory } from 'react-router'
import Paper from 'material-ui/Paper'
import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete';
import moment from 'moment-timezone'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import {
  cyan700,
} from 'material-ui/styles/colors';
import getInclusiveRange from './get-inclusive-range'
import {
    formatStatusEnum,
    isSupplierStaff
} from '../../util'
const dataSourceConfig = {
  text: 'text',
  value: 'value',
}
const styles = {
  headerControls : {
    display: "inline-block",
    width: "100%"
  },
  returnToSummaryButton: {
    minWidth: "190px",
    float: "right",
    marginTop: "1em",
    marginRight: "1em"
  }
}

class VoucherEdit extends Component {
  constructor(props) {
    super(props)
    let start = new Date()
    start.setDate(start.getDate() - 30);
    this.state = {
      start: start,
      end: new Date(),
      fetching: false,
      created: null,
      status: "",
      supplierCode: "",
      resellerCode: "",
      resellerId: "",
      productName: "",
      productCode: "",
      productId: "",
      cloudFactory: null,
      validationMessage: "",
      resellersByName: [],
      resellersByCode: [],
      productsByName: [],
      productsByCode: [],
      adultsError: "",
      childrenError: "",
      studentsError: "",
      infantsError: "",
      seniorsError: ""
    }
    this.resellerCodeField = null
    this.resellerNameField = null
    this.productCodeField = null
    this.productNameField = null
  }

  generateResellerAutoComplete (resellers) {

      let byName = [],
          byCode = [];
      resellers.map((reseller) =>{
          byCode.push({
              text: reseller.code,
              value: reseller
          });
          byName.push({
              text: reseller.name,
              value: reseller
          });
          if (!!reseller.aliases) {
            reseller.aliases.forEach(alias=>{
              byName.push({
                  text: alias,
                  value: reseller
              });
            })
          }
      })
      this.setState({
          resellersByName: byName,
          resellersByCode: byCode
      })
  }

  generateProductAutoComplete (products) {

      let byName = [],
          byCode = []

      !! products && typeof products.map == 'function' &&
      products.map((product) =>{
          byCode.push({
              text: product.code,
              value: product
          });
          byName.push({
              text: !!product.map ? product.map.from.productNames[0] : product.code + " (no name)",
              value: product
          });
      })
      this.setState({
          productsByName: byName,
          productsByCode: byCode
      })
  }

  componentWillMount() {

    let supplierCode = "",
        resellerCode = ""

    if (this.props.resellers == false && !this.props.fetchingResellers) {

        this.fetchResellers()

    } else if (!this.props.fetchingResellers) {

        this.generateResellerAutoComplete(this.props.resellers)

    }

    if (!!this.props.params) {
        if (!!this.props.params.supplierCode) {
            supplierCode = this.props.params.supplierCode
        }
        if (!!this.props.params.resellerCode) {
            resellerCode = this.props.params.resellerCode;
        }
    }

    if ( this.isSupplierStaff() ) {

      supplierCode = this.props.orgCode;

    } else if ( this.isResellerStaff() ) {

      resellerCode = this.props.orgCode;
      
    }

    this.setState({
      supplierCode,
      resellerCode
    })

    if ((this.props.products == false && !this.props.fetchingProducts) ||
        this.isSupplierStaff() == false && this.state.supplierCode != this.props.params.supplierCode) {
        //this.props.getSupplierProducts(supplierCode);
        //this.props.listSupplierResellerCatalogItems(supplierCode, this.state.resellerCode)
    } else if (!this.props.fetchingProducts) {
        this.generateProductAutoComplete(this.props.products);
    }

    this.props.getVoucher(this.props.params.id)
    this.props.getVoucherImage(this.props.params.id)

  }

  fetchResellers () {

    if ( this.isSupplierStaff() ) {

        this.props.listSupplierResellers( this.props.adminSupplierId != -1 ? this.props.adminSupplierId : this.props.supplierId )

    } else {

        this.props.listResellers()

    }

  }

  componentWillReceiveProps( nextProps ) {

    let hasSupplierId = !!nextProps.supplierId,
        hasAdminSupplierId = !!nextProps.adminSupplierId

    if (this.props.fetchingVoucher && nextProps.fetchingVoucher == false && nextProps.voucher != false) {

        let meta = nextProps.voucher,
            voucher = meta.voucher,
            hasCFOutput = (voucher.cloudFactory != null && voucher.cloudFactory.output != null),
            output = hasCFOutput ? voucher.cloudFactory.output : {};

                  this.setState({
                    voucher: voucher,
                    version: voucher.version,
                    images: voucher.images,
                    created: voucher.created,
                    status: voucher.status,
                    cloudFactoryStatus: voucher.cloudFactory ? voucher.cloudFactory.status : "",
                    bookingReferenceNumber: hasCFOutput ? output.bookingReferenceNumber : "",
                    numberOfAdults: hasCFOutput ? output.numberOfAdults : "",
                    numberOfChildren: hasCFOutput ? output.numberOfChildren : "",
                    numberOfInfants: hasCFOutput ? output.numberOfInfants : "",
                    numberOfSeniors: hasCFOutput ? output.numberOfSeniors : "",
                    numberOfStudents: hasCFOutput ? output.numberOfStudents : "",
                    product: hasCFOutput ? output.product : "",
                    productCode: hasCFOutput ? output.productCode : "",
                    productId: hasCFOutput ? output.productId : "",
                    productName: hasCFOutput ? output.productName : "",
                    resellerCode: hasCFOutput ? output.resellerCode : "",
                    resellerId: hasCFOutput ? output.resellerId : "",
                    reseller: hasCFOutput ? output.reseller : "",
                    resellerName: hasCFOutput ? output.resellerName : "",
                    reservationNumber: hasCFOutput ? output.reservationNumber : "",
                    supplierConfirmationNumber: hasCFOutput ? output.supplierConfirmationNumber : "",
                    totalPrice: hasCFOutput ? output.totalPrice : "",
                    totalPriceCurrency: hasCFOutput ? output.totalPriceCurrency : "",
                    travelerFirstName: hasCFOutput ? output.travelerFirstName : "",
                    travelerLastName: hasCFOutput ? output.travelerLastName : "",
                    supplierCode: voucher.supplierCode != null ? voucher.supplierCode : "",
                    cloudFactory: voucher.cloudFactory || "",
                    cloudVision: voucher.cloudVision != null ? voucher.cloudVision : {},
                    rejectReason: voucher.rejectReason != null ? voucher.rejectReason : "",
                    operatorId: voucher.operatorId != null ? voucher.operatorId : ""
                  })
                  this.props.listSupplierResellerCatalogItems(nextProps.params.supplierCode, hasCFOutput ? output.resellerCode : "")
      }

      if ( this.props.adminSupplierId != nextProps.adminSupplierId && hasAdminSupplierId || ( hasSupplierId && this.props.supplierId != nextProps.supplierId) ) {

        this.fetchResellers()

      }

      if (this.props.resellers == false && nextProps.resellers != false) {
          this.generateResellerAutoComplete(nextProps.resellers);
      }

      if (this.props.fetchingProducts == true && nextProps.fetchingProducts === false){
          
          this.generateProductAutoComplete(nextProps.products)
          
      }

      if (this.props.correctionResponse == false && nextProps.correctionResponse != false) {
          let voucher = nextProps.correctionResponse.voucher,
              supplierCode = this.state.supplierCode;
          this.setState({
            voucher: voucher
          })
          this.returnToSummary()
      }

  }

  componentWillUpdate(nextProps, nextState) { // map fields to view

      if (this.props.params.supplierCode != nextProps.params.supplierCode || nextState.resellerCode != this.state.resellerCode) {
              console.log("resellerCode", nextState.resellerCode)
        if (nextState.resellerCode != "") {
            if (this.isSupplierStaff()) {
                if (nextProps.params.supplierCode != nextProps.orgCode || 
                    nextState.resellerCode != this.state.resellerCode) {
                    //   this.props.getSupplierProducts(nextProps.orgCode);
                    this.props.listSupplierResellerCatalogItems(nextProps.orgCode, nextState.resellerCode)
                }
            } else {
                //this.props.getSupplierProducts(nextProps.params.supplierCode);
                this.props.listSupplierResellerCatalogItems(nextProps.params.supplierCode, nextState.resellerCode)
                this.setState({
                    supplierCode: nextProps.params.supplierCode
                })
            }
        }
      }
      if (nextProps.resellers != false) {
        if ((this.state.resellerName != nextState.resellerName) ||
            (nextState.resellerName != "" && nextState.resellerName != "UNKNOWN" && (nextState.resellerId == "" || nextState.resellerId == "UNKNOWN"))) {
            nextProps.resellers.map((reseller) => {
                if (reseller.name == nextState.resellerName) {
                    this.setState({
                        resellerCode: reseller.code,
                        resellerId: reseller.id
                    })
                    if (!!this.resellerCodeField) {
                        this.resellerCodeField.value = reseller.name
                    }
                }
            })
        }
      }
      if (nextProps.products != false) {
        if ((this.state.productName != nextState.productName) ||
            (nextState.productName != "" && nextState.productName != "UNKNOWN" && (nextState.productId == "" || nextState.productId == "UNKNOWN"))){
            nextProps.products.map((product) => {
                if (!!product.map && product.map.from.productNames[0] == nextState.productName) {
                    this.setState({
                        productCode: product.code,
                        productId: product.id
                    })
                    if (!!this.productCodeField) {
                        this.productCodeField.value = product.code
                    }
                }
            })
        }
        if ((this.state.productCode != nextState.productCode) ||
            (nextState.productCode != "" && nextState.productCode != "UNKNOWN" && (nextState.productId == "" || nextState.productId == "UNKNOWN"))) {
          nextProps.products.map((product) => {
              if (product.code == nextState.productCode) {
                  let resellerProductName = !!product.map ? product.map.from.productNames[0] : ""

                  this.setState({
                      productName: resellerProductName,
                      productId: product.id
                  })
                  if (!!this.productNameField) {
                    this.productNameField.value = resellerProductName
                  }
              }
          })
        }
      }

      // Clear Errors
      if (nextState.numberOfAdults != this.state.numberOfAdults &&
         (!isNaN(nextState.numberOfAdults) && parseInt(nextState.numberOfAdults) % 1 == 0)){
           this.setState({adultsError: ""});
      }
      if (nextState.numberOfChildren != this.state.numberOfChildren &&
         (!isNaN(nextState.numberOfChildren) && parseInt(nextState.numberOfChildren) % 1 == 0)){
           this.setState({childrenError: ""});
      }
      if (nextState.numberOfInfants != this.state.numberOfInfants &&
         (!isNaN(nextState.numberOfInfants) && parseInt(nextState.numberOfInfants) % 1 == 0)) {
           this.setState({infantsError: ""});
      }
      if (nextState.numberOfSeniors != this.state.numberOfSeniors &&
         (!isNaN(nextState.numberOfSeniors) && parseInt(nextState.numberOfSeniors) % 1 == 0)) {
           this.setState({seniorsError: ""});
      }
      if (nextState.numberOfStudents != this.state.numberOfStudents &&
         (!isNaN(nextState.numberOfStudents) && parseInt(nextState.numberOfStudents) % 1 == 0)) {
           this.setState({studentsError: ""});
      }
  }

  save() {

    let input = this.state.cloudFactory.input || {
          id: "",
          imageUrl: "",
          supplierCode: this.state.supplierCode,
          voucherId: this.state.voucher.id
        },
        data = {
          status: this.state.status,
          version: this.state.version,
          created: this.state.created,
          id: this.state.voucher.id,
          deviceId: this.state.voucher.deviceId,
          ticketId: this.state.voucher.ticketId,
          payableBy: this.state.voucher.payableBy,
          images: this.state.images,
          operatorId: this.state.operatorId,
          supplierCode: this.state.supplierCode,
          resellerCode: this.state.resellerCode,
          rejectReason: this.state.rejectReason,
          cloudVision: this.state.cloudVision,
          cloudFactory: {
              id: this.state.cloudFactory.id,
              lineId: this.state.cloudFactory.lineId,
              imageId: this.state.cloudFactory.imageId,
              input: input,
              status: this.state.cloudFactoryStatus,
              output: {
                  bookingReferenceNumber: this.state.bookingReferenceNumber,
                  numberOfAdults: this.state.numberOfAdults,
                  numberOfChildren: this.state.numberOfChildren,
                  numberOfInfants: this.state.numberOfInfants,
                  numberOfSeniors: this.state.numberOfSeniors,
                  numberOfStudents: this.state.numberOfStudents,
                  product: this.state.product,
                  productCode: this.state.productCode,
                  productId: this.state.productId,
                  productName: this.state.productName,
                  resellerCode: this.state.resellerCode,
                  resellerId: this.state.resellerId,
                  resellerName: this.state.resellerName,
                  reservationNumber: this.state.reservationNumber,
                  supplierConfirmationNumber: this.state.supplierConfirmationNumber,
                  totalPrice: this.state.totalPrice,
                  totalPriceCurrency: this.state.totalPriceCurrency,
                  travelerFirstName: this.state.travelerFirstName,
                  travelerLastName: this.state.travelerLastName
              }
        }
    };

    // validation
    if (this.state.numberOfAdults != "" && (isNaN(this.state.numberOfAdults) || parseFloat(this.state.numberOfAdults) % 1 != 0)) {
      this.setState({adultsError: "Adults must be whole a number."});
      return;
    }
    if (this.state.numberOfChildren != "" && (isNaN(this.state.numberOfChildren) || parseFloat(this.state.numberOfChildren) % 1 != 0)) {
      this.setState({childrenError: "Children must be whole a number."});
      return;
    }
    if (this.state.numberOfInfants != "" && (isNaN(this.state.numberOfInfants) || parseFloat(this.state.numberOfInfants) % 1 != 0)) {
      this.setState({infantsError: "Infants must be whole a number."});
      return;
    }
    if (this.state.numberOfSeniors != "" && (isNaN(this.state.numberOfSeniors) || parseFloat(this.state.numberOfSeniors) % 1 != 0)) {
      this.setState({seniorsError: "Seniors must be whole a number."});
      return;
    }
    if (this.state.numberOfStudents != "" && (isNaN(this.state.numberOfStudents) || parseFloat(this.state.numberOfStudents) % 1 != 0)) {
      this.setState({studentsError: "Students must be whole a number."});
      return;
    }

    this.props.correctVoucher(this.props.params.id, data)
  }

  isSupplierStaff () {
    return this.props.role !== false && this.props.role.indexOf("SUPPLIER") > -1;
  }

  isResellerStaff () {
    return this.props.role !== false && this.props.role.indexOf("RESELLER") > -1;
  }

  returnToSummary () {

      let supplierCode = this.props.params.supplierCode,
          start = this.props.location.query.start,
          end = this.props.location.query.end,
          sort = this.props.location.query.sort

    if ( !!this.props.location.query.back ) {
        switch ( this.props.location.query.back ) {
            case "shift":
                browserHistory.push(this.props.returnToShiftURL)
            break
        }
    } else {
        browserHistory.push(`/app/reports/voucher-exceptions/${supplierCode}?start=${start}&end=${end}&sort=${sort}`)
    }
    this.props.refreshReport()
  }

  renderBasics () {

      return (
          <Table className="edit-table edit-form">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
              <TableRow>
                  <TableHeaderColumn>Property</TableHeaderColumn>
                  <TableHeaderColumn>Value</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow selectable={false}>
                  <TableRowColumn>Date</TableRowColumn>
                  <TableRowColumn>
                  { !!this.state.created && this.state.created.seconds != "" ? moment(this.state.created.seconds * 1000).tz(TZ).format("DD-MMM-YYYY") : "–" }
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                  <TableRowColumn>Status</TableRowColumn>
                  <TableRowColumn>
                  {formatStatusEnum(this.state.status)}
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                  <TableRowColumn>Supplier Code</TableRowColumn>
                  <TableRowColumn>
                  { this.state.supplierCode }
                  </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                      <TableRowColumn>Operator ID</TableRowColumn>
                      <TableRowColumn>
                        { this.state.operatorId }
                      </TableRowColumn>
              </TableRow>
              <TableRow selectable={false}>
                      <TableRowColumn>Timestamp</TableRowColumn>
                      <TableRowColumn>
                        { !!this.state.created && this.state.created.seconds != "" ? moment(this.state.created.seconds * 1000).tz(TZ).format("HH:MM A") : "–" }
                      </TableRowColumn>
              </TableRow>
            </TableBody>
          </Table>
      )
  }

  renderCloudVision () {

      return (
          <Table className="edit-table edit-form">
            <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
              <TableRow>
                  <TableHeaderColumn>Property</TableHeaderColumn>
                  <TableHeaderColumn>Value</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              <TableRow key={"1"} selectable={false}>
                  <TableRowColumn key={"1.1"}>Image ID</TableRowColumn>
                  <TableRowColumn key={"1.2"}>
                  {  !!this.state.cloudVision && !!this.state.cloudVision.imageId ? this.state.cloudVision.imageId : "–"  }
                  </TableRowColumn>
              </TableRow>

                  <TableRow selectable={false}>
                      <TableRowColumn key={"1.3"}>Status</TableRowColumn>
                      <TableRowColumn key={"1.2"}>
                        { !!this.state.cloudVision && !!this.state.cloudVision.status ? this.state.cloudVision.status : "–" }
                      </TableRowColumn>
                  </TableRow>
            </TableBody>
          </Table>
      )
  }

  renderCloudFactory () {

      if (!!!this.state.cloudFactory) { // not loaded yet / switching records
        return "";
      } else {
        return (
            <Table className="edit-table edit-form">
              <TableHeader adjustForCheckbox={false} displayRowCheckbox={false} enableSelectAll={false} displaySelectAll={false}>
                <TableRow>
                    <TableHeaderColumn>Property</TableHeaderColumn>
                    <TableHeaderColumn>Value</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false} className="voucher-table-show-overflow">
                  <TableRow selectable={false} >
                      <TableRowColumn key={"1.1"}>Transaction ID</TableRowColumn>
                      <TableRowColumn key={"1.2"}>
                      {!!this.state.cloudFactory && this.state.cloudFactory.id != "" ? this.state.cloudFactory.id : "–"}
                      </TableRowColumn>
                  </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Status</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                        {formatStatusEnum(this.state.cloudFactoryStatus)}
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Reseller Name</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                        <AutoComplete
                           searchText={this.state.resellerName}
                           filter={AutoComplete.fuzzyFilter}
                           key="reseller-name-field" id="reseller-name-field"
                           ref={r=> { this.resellerNameField = r }}
                           openOnFocus={true}
                           fullWidth={true}
                           dataSource={this.state.resellersByName}
                           dataSourceConfig={dataSourceConfig}
                           onNewRequest={(chosenRequest, index) => {
                              console.log("onNewRequest", chosenRequest);
                              let resellerName = "";
                              if (typeof chosenRequest == "string") {
                                resellerName = chosenRequest;
                              } else {
                                resellerName = chosenRequest.value.name;
                              }
                              this.setState({
                                  resellerName: resellerName
                              })
                           }}
                           onUpdateInput={(searchText, dataSource) => {}}
                           onBlur={e => {
                               this.setState({
                                   "resellerName": e.target.value
                               })
                           }}
                         />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Reseller Code</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                        <AutoComplete
                           searchText={this.state.resellerCode}
                           filter={AutoComplete.fuzzyFilter}
                           key="reseller-code-field" id="reseller-code-field"
                           ref={r=> { this.resellerCodeField = r }}
                           openOnFocus={true}
                           fullWidth={true}
                           dataSource={this.state.resellersByCode}
                           dataSourceConfig={dataSourceConfig}
                           onNewRequest={(chosenRequest, index) => {
                               this.setState({
                                  resellerCode: chosenRequest.value.code
                               })
                           }}
                           onUpdateInput={(searchText, dataSource) => {}}
                           onBlur={e => {
                               this.setState({
                                   "resellerCode": e.target.value
                               })
                           }}
                         />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Product Name</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                        <AutoComplete
                           searchText={this.state.productName}
                           filter={AutoComplete.fuzzyFilter}
                           key="product-name-field" id="product-name-field"
                           ref={r=> { this.productNameField = r }}
                           openOnFocus={true}
                           fullWidth={true}
                           dataSource={this.state.productsByName}
                           dataSourceConfig={dataSourceConfig}
                           onNewRequest={(chosenRequest, index) => {
                               let value = chosenRequest.value,
                                   productName = !!value.map ? value.map.from.productNames[0] : 'No Name'

                               this.setState({
                                  productName,
                                  product: productName
                               })
                           }}
                           onUpdateInput={(searchText, dataSource) => {}}
                           
                         />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Product Code</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                        <AutoComplete
                           searchText={this.state.productCode}
                           filter={AutoComplete.fuzzyFilter}
                           key="product-code-field" id="product-code-field"
                           ref={r=> { this.productCodeField = r }}
                           openOnFocus={true}
                           fullWidth={true}
                           dataSource={this.state.productsByCode}
                           dataSourceConfig={dataSourceConfig}
                           onNewRequest={(chosenRequest, index) => {
                               let value = chosenRequest.value,
                                   productName = !!value.map ? value.map.from.productNames[0] : 'No Name'

                               this.setState({
                                  productCode: value.code,
                                  product: productName
                               })
                           }}
                           onUpdateInput={(searchText, dataSource) => {}}
                           onBlur={e => {
                               let value = e.target.value

                               this.setState({
                                   productCode: value.code,
                                   product: !!value.map ? value.map.from.productNames[0] : 'No Name'
                               })
                           }}
                         />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Adults</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                          <TextField
                            defaultValue={this.state.numberOfAdults}
                            key="numberOfAdults-textfield" id="numberOfAdults-textfield"
                            errorText={this.state.adultsError}
                            onBlur={e => {
                                this.setState({
                                    "numberOfAdults": e.target.value
                                })
                            }}
                          />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Children</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              defaultValue={this.state.numberOfChildren}
                              key="numberOfChildren-textfield" id="numberOfChildren-textfield"
                              errorText={this.state.childrenError}
                              onBlur={e => {
                                  this.setState({
                                      "numberOfChildren": e.target.value
                                  })
                              }}
                            />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Infants</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                          <TextField
                            defaultValue={this.state.numberOfInfants}
                            key="numberOfInfants-textfield" id="numberOfInfants-textfield"
                            errorText={this.state.infantsError}
                            onBlur={e => {
                                this.setState({
                                    "numberOfInfants": e.target.value
                                })
                            }}
                          />
                        </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Seniors</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              defaultValue={this.state.numberOfSeniors}
                              key="numberOfSeniors-textfield" id="numberOfSeniors-textfield"
                              errorText={this.state.seniorsError}
                              onBlur={e => {
                                  this.setState({
                                      "numberOfSeniors": e.target.value
                                  })
                              }}
                            />
                    </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Students</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              defaultValue={this.state.numberOfStudents}
                              key="numberOfStudents-textfield" id="numberOfStudents-textfield"
                              errorText={this.state.studentsError}
                              onBlur={e => {
                                  this.setState({
                                      "numberOfStudents": e.target.value
                                  })
                              }}
                            />
                    </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Reservation Number</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              defaultValue={this.state.reservationNumber}
                              key="reservationNumber-textfield" id="reservationNumber-textfield"
                              onBlur={e => {
                                  this.setState({
                                      "reservationNumber": e.target.value
                                  })
                              }}
                            />
                    </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Booking Reference Number</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              defaultValue={this.state.bookingReferenceNumber}
                              key="bookingReferenceNumber-textfield" id="bookingReferenceNumber-textfield"
                              onBlur={e => {
                                  this.setState({
                                      "bookingReferenceNumber": e.target.value
                                  })
                              }}
                            />
                    </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Supplier Confirmation Number</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              defaultValue={this.state.supplierConfirmationNumber}
                              key="supplierConfirmationNumber-textfield" id="supplierConfirmationNumber-textfield"
                              onBlur={e => {
                                  this.setState({
                                      "supplierConfirmationNumber": e.target.value
                                  })
                              }}
                            />
                    </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Total Price</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              defaultValue={this.state.totalPrice}
                              key="totalPrice-textfield" id="totalPrice-textfield"
                              onBlur={e => {
                                  this.setState({
                                      "totalPrice": e.target.value
                                  })
                              }}
                            />
                    </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Total Price Currency</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                            <TextField
                              defaultValue={this.state.totalPriceCurrency}
                              key="totalPriceCurrency-textfield" id="totalPriceCurrency-textfield"
                              onBlur={e => {
                                  this.setState({
                                      "totalPriceCurrency": e.target.value
                                  })
                              }}
                            />
                    </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Traveler First Name</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                          <TextField
                              defaultValue={this.state.travelerFirstName}
                              key="travelerFirstName-textfield" id="travelerFirstName-textfield"
                              onBlur={e => {
                                  this.setState({
                                      "travelerFirstName": e.target.value
                                  })
                              }}
                          />
                    </TableRowColumn>
                    </TableRow>
                    <TableRow selectable={false} >
                        <TableRowColumn key={"1.1"}>Traveler Last name</TableRowColumn>
                        <TableRowColumn key={"1.2"}>
                          <TextField
                              defaultValue={this.state.travelerLastName}
                              key="travelerLastName-textfield" id="travelerLastName-textfield"
                              onBlur={e => {
                                  this.setState({
                                      "travelerLastName": e.target.value
                                  })
                              }}
                          />
                    </TableRowColumn>
                  </TableRow>
              </TableBody>
            </Table>
        )
      }
  }

  renderError (err) {

        let msg = "";
        if (err !== false) {
          if (typeof err == "string") {
            msg = err;
          } else {
            msg = err.message;
          }
        }
        return (
          <span className="correction-error">{msg}</span>
        )

  }

  render() {

      let correctionError = this.props.correctionError,
          images = this.props.images,
          fetching = this.props.fetching
                  || this.props.savingVoucher;
    return (
        <div className="supplier-edit voucher-edit">
            <div style={styles.headerControls}>
              <h2>Exception Detail</h2>
              <RaisedButton onTouchTap={e => { this.returnToSummary() } }
                            className="no-print"
                            style={styles.returnToSummaryButton}
                            label="Return to Summary"
              />
            </div>
            <section className="image">
            {
              images.map((image, i) => (
                <a key={i} href={image} title="Open In New Tab" target="_blank" className="voucher-detail-image-link">
                    <img src={image} title="Open In New Tab" className="voucher-image" style={{cursor: "pointer"}} />
                </a>
              ))
            }
            </section>
            <section className="tables">
                { !fetching ? this.renderBasics() :
                    <div className="loading-circle">
                    <CircularProgress color={"#27367a"} size={96} />
                    </div>
                }
                <h2>CloudFactory Output
                    <span className="correct-button">
                        <RaisedButton className="save-button" label="Correct" primary={true}
                                      style={{marginLeft: "1em"}} onClick={e => this.save()} />
                    </span>
                    {this.renderError(this.props.correctionError)}
                </h2>
                { !fetching ? this.renderCloudFactory() : ""}
                {/*<h2>CloudVision Output</h2>*/}
                {/*{ !fetching ? this.renderCloudVision() :
                    <div className="loading-circle">
                    <CircularProgress color={"#27367a"} size={96} />
                    </div>
                }*/}
                {/*<div className="cloud-vision-text">
                {!!this.state.cloudVision ? this.state.cloudVision.text : ""}
                </div>*/}
            </section>
      </div>
    );

  }

}

VoucherEdit.propTypes = {

};

import { connect } from 'react-redux';
import {
    correctVoucher,
    getVoucher,
    getVoucherImage
} from '../../ax-redux/actions/vouchers'
import {
    listSupplierResellerCatalogItems
} from '../../ax-redux/actions/suppliers'
import {
    listResellers,
  listSupplierResellers,
} from '../../ax-redux/actions/resellers'
import { refresh } from '../../ax-redux/actions/report-filters'
// import {
//   getSupplierProducts
// } from '../../ax-redux/actions/products'

export default connect(
  (state, ownProps) => {
      let supplierResellerCatalogItems = !!state.suppliers.listResellerCatalogItems.data ? state.suppliers.listResellerCatalogItems.data.catalogItems : false

    return {
        role: state.app.role,
        orgCode: !!state.app.user ? state.app.user.account.orgCode : "",
        voucher: state.vouchers.getVoucher.data,
        savingVoucher: state.vouchers.correctVoucher.fetching,
        correctionResponse: state.vouchers.correctVoucher.data,
        correctionError: state.vouchers.correctVoucher.error,
        fetching: state.vouchers.getVoucher.fetching || state.vouchers.rejectVoucher.fetching,
        fetchingVoucher: state.vouchers.getVoucher.fetching,
        images: !!state.vouchers.getVoucherImage.data ? (
            !!state.vouchers.getVoucherImage.data.urls ? state.vouchers.getVoucherImage.data.urls  :
            [state.vouchers.getVoucherImage.data.url]
        ) : [""],
        resellers: isSupplierStaff(state.app.role) ? (!!state.resellers.listBySupplier.data ? state.resellers.listBySupplier.data.resellers : false) :
                                                     (!!state.resellers.list.data ? state.resellers.list.data.resellers : false),
        fetchingResellers: isSupplierStaff(state.app.role) ? state.resellers.listBySupplier.fetching  : state.resellers.list.fetching,
        products: !!state.suppliers.listResellerCatalogItems.data ? state.suppliers.listResellerCatalogItems.data.catalogItems : false,
        fetchingProducts: state.suppliers.listResellerCatalogItems.fetching,
        returnToShiftURL: state.navigation.returnToShiftURL,
    }
  },
  dispatch => {
    return {
      getVoucher: (id) => {
        dispatch(getVoucher(id))
      },
      correctVoucher: (id, data) => {
        dispatch(correctVoucher(id, data))
      },
      getVoucherImage: (id) => {
          dispatch(getVoucherImage(id))
      },
      listResellers: () => {
          dispatch(listResellers())
      },
      listSupplierResellers: ( supplierCode ) => {

        dispatch( listSupplierResellers( supplierCode ) )

      },
      listSupplierResellerCatalogItems: (supplierCode, resellerCode, params) => {
          dispatch(listSupplierResellerCatalogItems(supplierCode, resellerCode, params))
      },
      refreshReport: () => dispatch(refresh()),
    //   getSupplierProducts: (supplierCode) => {
    //       dispatch(getSupplierProducts(supplierCode))
    //   }
    }
  }
)(VoucherEdit)
