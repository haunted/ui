import React, { Component, PropTypes } from 'react'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Snackbar from 'material-ui/Snackbar'

class MergeVoucherButton extends Component {

  constructor(props) {
    super(props);

    this.state = {
      activated: false,
      comments: "",
      showError: false
    }
  }
  componentWillUpdate(nextProps, nextState) {
    if (this.props.error == false && nextProps.error != false) {
      this.setState({
        showError: true
      })
    }
  }
  toggleForm () {
    this.setState({
        activated: !this.state.activated
    })
  }
  handleErrRequestClose () {
      this.setState({
        showError: false,
      });
  }
  submit () {
    this.props.mergeVoucher(this.props.voucherId)
    this.toggleForm()
    setTimeout(()=>{
      if (this.state.showError == false) {
        !! this.props.callback && this.props.callback()
      }
    }, 500)
  }

  cancel () {
      this.toggleForm()
  }

  renderSnackBar () {
    return (
      <Snackbar
        open={this.state.showError}
        message={this.props.error !== false ? this.props.error.data.Error : "Error Occurred"}
        autoHideDuration={4000}
        onRequestClose={(e)=>this.handleErrRequestClose()}
      />
    )
  }

  render() {
        if (this.state.activated) {
            return (
                <div className="ticket-lightbox merge-voucher-button">
                    <div className="inner-lightbox inner">
                    <p>
                      WARNING: You are about to merge this voucher.
                      It will be combined with a matching, redeemed voucher. Are you sure?
                    </p>
                    <RaisedButton
                          onTouchTap={e => { this.submit() } }
                          className="no-print form-button merge-button"
                          label="Yes"
                     />
                     Merge Voucher
                     <RaisedButton
                           onTouchTap={e => { this.cancel() } }
                           className="no-print form-button"
                           label="No"
                      />
                      Cancel
                  </div>
                  { this.renderSnackBar() }
              </div>
            )
        } else {
            return (
              <span style={{display: 'inline-block'}}>
                  <RaisedButton
                        onTouchTap={e => { this.toggleForm() } }
                        className={"no-print " + (this.props.className || "")}
                        secondary={this.props.secondary}
                        style={(this.props.style  || {marginLeft: "1em"})}
                        label={this.props.label || "Merge Voucher"}
                   />
                   { this.renderSnackBar() }
              </span>
            )
        }
    }
}

MergeVoucherButton.propTypes = {
  voucherId: PropTypes.string.isRequired
}

import { connect } from 'react-redux'

import {
    mergeVoucher
} from '../ax-redux/actions/vouchers'

export default connect(
  (state, ownProps) => {
    return {
      error: state.vouchers.mergeVoucher.error
    }
  },
  dispatch => {
    return {
        mergeVoucher: (id) => {
            dispatch(mergeVoucher(id))
        }
    }
  }
)(MergeVoucherButton)
