import React, {Component} from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { create } from '../../ax-redux/actions/news'

import TextField from 'material-ui/TextField'
import Card, {CardTitle, CardText, CardActions} from 'material-ui/Card'
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';


const loginCardStyle = {
  margin: "auto",
  maxWidth: 300,
}
const loginPaperStyle = {
  padding: "1em",
}
const styles = {
  error: {
    color: "rgba(180, 90, 90, 0.8)",
  },
  container: {
    textAlign: 'center',
  },
  button: {
    margin: 12
  }
};
class CreateNews extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: "",
      body: "",
    }
  }
  onCreateNews() {
    this.props.onCreateNews({
      title: this.state.title,
      body: this.state.body
    })
  }
  render() {
    return (
      <div style={styles.container}>
        <Card style={loginCardStyle}>
          <Paper style={loginPaperStyle} zDepth={2}>
              <CardTitle>Create News</CardTitle>
              <TextField
                hintText="Title"
                floatingLabelText="Title"
                value={this.state.title}
                onChange={e => this.setState({ title: e.target.value })}
              />
              <TextField
                hintText="Body"
                floatingLabelText="Body"
                value={this.state.body}
                onChange={e => this.setState({ body: e.target.value })}
              />

            <CardActions>
              <RaisedButton label="Back" onClick={e => browserHistory.push("/")}/>
              <RaisedButton primary={true} label="Create" onClick={e => this.onCreateNews()}/>
            </CardActions>
          </Paper>
        </Card>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    news: state.news
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onCreateNews: (news) => {
      dispatch(create(news))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateNews);
