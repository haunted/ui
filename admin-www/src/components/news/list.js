import React, {Component} from 'react'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { fetch } from '../../ax-redux/actions/news'


import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
const loginCardStyle = {
  margin: "auto",
  maxWidth: 300,
}
const loginPaperStyle = {
  padding: "1em",
}
const styles = {
  error: {
    color: "rgba(180, 90, 90, 0.8)",
  },
  container: {
    textAlign: 'center',
  },
  button: {
    margin: 12
  }
};

class ListNews extends Component {
  constructor(props) {
    super(props)
    this.props.dispatch(fetch())
  }
  render() {
    return (
      <div style={styles.container}>
        <Table>
          <TableHeader>
            <TableRow>
              <TableHeaderColumn>ID</TableHeaderColumn>
              <TableHeaderColumn>Title</TableHeaderColumn>
              <TableHeaderColumn>Body</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody>
            {
              this.props.news.data.map(v => (
                <TableRow>
                  <TableRowColumn>{v.news.id}</TableRowColumn>
                  <TableRowColumn>{v.news.title}</TableRowColumn>
                  <TableRowColumn>{v.news.body}</TableRowColumn>
                </TableRow>
              ))
            }
          </TableBody>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    news: state.news
  }
}

export default connect(mapStateToProps)(ListNews);
