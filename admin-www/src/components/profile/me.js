import React, { Component, PropTypes } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Card from 'material-ui/Card'
import Paper from 'material-ui/Paper'
import CircularProgress from 'material-ui/CircularProgress';

const styles = {
  paper: {
    padding: 10,
  }
}

class Me extends Component {
  constructor(p) {
    super(p)
  }
  componentDidMount() {
    this.props.fetch()
  }
  render() {
    return (
      <Paper style={styles.paper} zDepth={2}>
        <RaisedButton label="Fetch Profile" onClick={e => this.props.fetch()} />
        <pre>
          {
            this.props.auth.meFetching
              ? <CircularProgress />
              : JSON.stringify(this.props.auth, false, 2)
          }
        </pre>
      </Paper>

    )
  }
}

import { connect } from 'react-redux'
import { meFetch } from '../../ax-redux/actions/auth'

export default connect(
  state => ({ auth: state.auth }),
  dispatch => ({
    fetch: () => dispatch(meFetch()),
  })
)(Me);
