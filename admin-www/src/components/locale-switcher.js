import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import LocalizedStrings from 'react-localization';

let strings = new LocalizedStrings({
 en:{
   chooseLanguage:"Choose Language"
 },
 it: {
   chooseLanguage:"[!!! Çλôôƨè £áñϱúáϱè ℓôřè !!!]"
 }
});

class LocaleSwitcher extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleTouchTap (event) {
    // This prevents ghost click.
    event.preventDefault();
    this.setState({
        open: true,
        anchorEl: event.currentTarget
    });
  }

  handleRequestClose (evt) {
    this.setState({
       open: false
    });
  }

  handleMenuItem (locale) {
    this.setState({
       open: false
    });
    this.props.onsubmit(locale)
    this.props.changeLocale(locale)
  }

  render() {
    return (
      <div className="locale-switcher">
        <span className="locale-switcher-label">
          { strings.chooseLanguage }
        </span>
        <RaisedButton
          onTouchTap={e => this.handleTouchTap(e)}
          label={this.props.locale}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}
        >
          <Menu>
            <MenuItem onClick={()=>{ this.handleMenuItem("en") }} primaryText="English" />
            <MenuItem onClick={()=>{ this.handleMenuItem("it") }}  primaryText="Italian" />
          </Menu>
        </Popover>
      </div>
    );
  }
}

import { connect } from 'react-redux'
import { changeLocale } from '../ax-redux/actions/app'

const mapStateToProps = (state) => {
  return {
      locale: state.app.locale
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
      changeLocale: (locale) => {
          dispatch(changeLocale(locale));
      }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocaleSwitcher);
