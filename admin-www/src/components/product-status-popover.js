import React, { PropTypes } from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import { formatEnum } from '../util';


const statuses = [
  { value: 'STATUS_DRAFT', name: 'Draft' },
  { value: 'STATUS_TEST_MODE', name: 'Test Mode' },
  { value: 'STATUS_LIVE', name: 'Live' },
  { value: 'STATUS_DISABLED', name: 'Disabled' },
  { value: 'STATUS_DELETED', name: 'Deleted' },
];


export default class ProductStatusPopover extends React.Component {
  constructor(props) {
    super(props);

    this.handleTouchTap = this.handleTouchTap.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.onMenuItem = this.onMenuItem.bind(this);

    this.state = {
      open: false,
    };
  }

  onMenuItem(value, name) {
    this.props.onStatus(value, name);

    this.setState({
      open: false,
    });
  }

  handleTouchTap(event) {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  }

  handleRequestClose() {
    this.setState({
      open: false,
    });
  }

  render() {
    const {
      status,
    } = this.props;

    return (
      <div className="roles-popover">
        <RaisedButton
          onTouchTap={this.handleTouchTap}
          label={status ? formatEnum(status) : 'Choose status'}
        />
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}
        >
          <Menu>
            {statuses.map(s => (
              <MenuItem
                key={s.value}
                onClick={() => this.onMenuItem(s.value, s.name)}
                primaryText={s.name}
              />
            ))}
          </Menu>
        </Popover>
      </div>
    );
  }
}

ProductStatusPopover.propTypes = {
  onStatus: PropTypes.func.isRequired,
  status: PropTypes.string,
};

ProductStatusPopover.defaultProps = {
  status: null,
};
