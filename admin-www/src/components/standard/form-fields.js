import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import React, {Component} from 'react'

export const validateFeeStructureForSubmit = feeStructure => {
    let error = ""
    let value = feeStructure
    if (!!!feeStructure) {
        value = {}
        return {value, error}    
    }
    if (typeof feeStructure.percentage === 'string') {
        feeStructure.percentage = parseFloat(feeStructure.percentage)
        if (isNaN(feeStructure.percentage)) {
            feeStructure.percentage = 0
        }
    }
    if (typeof feeStructure.min === 'string') {
        feeStructure.min = parseFloat(feeStructure.min)
        if (isNaN(feeStructure.min)) {
            feeStructure.min = 0
        }
    }
    if (typeof feeStructure.cap === 'string') {
        feeStructure.cap = parseFloat(feeStructure.cap)
        if (isNaN(feeStructure.cap)) {
            feeStructure.cap = 0
        }
    }
    if (feeStructure.percentage > 100) {
        error = "that doesn't seem like a good business plan"        
    }
    if (feeStructure.min > feeStructure.cap) {
        error = "min is greater than the cap"
    }

    return {value, error}
}
export const FeeStructure = ({ value={}, error="", onChange, ...other }) => (
    <div style={{display:'flex'}} {...other}>
        <TextField 
            onChange={(e, v) => onChange(Object.assign(value, { percentage: isNaN(parseFloat(v)) ? v : parseFloat(v) }))}
            type="number"
            min={0}
            max={100}
            floatingLabelText="Percentage" key="percentage"
            floatingLabelFixed={true}
            value={value.percentage}
            className="label"
            errorText={error}
        />

        <TextField 
            onChange={(e, v) => onChange(Object.assign(value, { min: isNaN(parseFloat(v)) ? v : parseFloat(v) }))}
            type="number"
            min={0}
            floatingLabelText="Min" key="min"
            floatingLabelFixed={true}
            value={value.min}
            className="label"
        />

        <TextField 
            onChange={(e, v) => onChange(Object.assign(value, { cap: isNaN(parseFloat(v)) ? v : parseFloat(v) }))}
            type="number"
            min={0}
            floatingLabelText="Cap" key="cap"
            floatingLabelFixed={true}
            value={value.cap}
            className="label"
        />
    </div>
)

const labelStyle = {
    lineHeight: '22px',
    fontWeight: 'bolder',
    margin: '1em 0 0',
    color: 'rgba(0, 0, 0, 0.8)',
    display: 'block',
}
export const Label = ({children, style, ...other}) => (
    <label style={{...labelStyle, ...style}} {...other}>{children}</label>
)

export const RepeatedField = ({ 
    value = [],
    onChange = v=>{},
    createField = () => {},
    defaultValue = "",
    label = "",
}) => {
    return <div style={{ 
            display: 'flex',
        }}>
        
        <div>
            { !!label ? <Label style={{ margin: '1em 0 0.5em' }}>{label}</Label> : null }
            {
                value.map((v,k) => (
                    <div key={"a"+k} style={{ display: 'flex' }}>
                        {
                            createField(v, val => {
                                value[k] = val
                                onChange(value)
                            })
                        }
                        <RaisedButton
                            label="Remove"
                            style={{ alignSelf: 'flex-end' }}
                            onClick={e => {
                                let newVal = value
                                newVal.splice(k, 1)
                                onChange(newVal)
                            }}
                        />
                    </div>
                ))
            }
            <div style={{ alignSelf: "flex-end" }}>
                <RaisedButton
                    label="Add"
                    onClick={e => {
                        let newVal = value
                        if (!!!newVal) {
                            newVal = []
                        }
                        newVal.push(defaultValue)
                        onChange(newVal)
                    }}>
                </RaisedButton>
            </div>
        </div>
    </div>
}