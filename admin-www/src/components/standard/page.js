import React, { Component, PropTypes } from 'react'
import Paper from 'material-ui/Paper';
import Progress from './progress'

let styles = {
    title: {
        color: 'rgb(67, 67, 67)',
        display: 'inline-block',
        marginLeft: '1em',
        marginRight: '1em'
    },
}


// Page takes care of a management page UI that has has a title, fetches data, and has displayable children
// after fetchage completes
const Page = ({ title, fetching, error, children }) => (
    <div className="user-management">
        <h2 style={styles.title}>{title}</h2>

        <Paper className="group-management">
            <Progress fetching={fetching}>
                { !! error ? 
                <PageError>Network Error</PageError>
                : children }
            </Progress>
        </Paper>
    </div>
)

export default Page


const PageError = ({children}) => (
    <div style={{ padding: '10vh 0', margin: 'auto', textAlign: 'center', color: '#ed5a5a', fontSize: '120%' }}>
        {children}
    </div>
)

export const PageForm = ({style={}, children, error, ...other}) => (
    <form 
        style={{
            padding: '3em',
            ...style,
        }}
        {...other}
    >
        {error ? <PageError>{error}</PageError> : null}
        {children}
    </form>
)