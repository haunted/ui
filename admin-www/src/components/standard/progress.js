import React, { Component, PropTypes } from 'react'
import CircularProgress from 'material-ui/CircularProgress'

const Progress = ({fetching, children}) => (
    fetching ? <div style={{ padding: '10vh 0', margin: 'auto', textAlign: 'center' }}>
        <CircularProgress color={"#27367a"} size={96} />
    </div> : 
    <div>{children}</div>
)

export default Progress