import React, { Component } from 'react';
import IconButton from 'material-ui/IconButton';
import FormatAlignLeft from 'material-ui/svg-icons/editor/format-align-left';
import Visibility from 'material-ui/svg-icons/action/visibility';
import { copyTextToClipboard } from '../../util';

const NOOP = () => {};

const tableIconColor = 'rgba(0, 0, 0, 0.570588)';

const tableStyles = {
  backgroundColor: 'rgb(255, 255, 255)',
  width: '100%',
  borderCollapse: 'collapse',
  borderSpacing: '0px',
  // tableLayout: "fixed",
  fontFamily: 'Roboto, sans-serif',
};

const thStyles = {
  height: '56px',
  fontWeight: 'normal',
  fontSize: '12px',
  paddingLeft: '24px',
  paddingRight: '24px',
  textAlign: 'left',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis',
  color: 'rgb(158, 158, 158)',
  position: 'relative',
  backgroundColor: 'inherit',
};

const trStyles = {
  borderBottom: '1px solid rgb(245, 245, 245)',
  color: 'rgba(0, 0, 0, 0.870588)',
  height: '48px',
};

const tdStyles = {
  paddingLeft: '24px',
  paddingRight: '24px',
  height: '48px',
  textAlign: 'left',
  fontSize: '13px',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis',
  backgroundColor: 'inherit',

  // styles to make tooltip show not behind table... thanks again MUI...
  // overflow: "hidden",
  position: 'relative',
  overflow: 'visible',
};

const actionStyle = {
  padding: '1em',
  display: 'flex',
  flexFlow: 'column-reverse',
};

// wrapper div styles to make tooltip show not behind table... thanks again MUI...
const tooltipCellStyles = {
  position: 'absolute',
  zIndex: 1000,
  top: 0,
};

const noteTooltipStyles = {
  // maxWidth: "600px",
  width: 400,
  whiteSpace: 'pre-line',
  textAlign: 'left',
};

// RedeamTable is a badass component that makes a badass table from it's props
export default class RedeamTable extends Component {
  render() {
    const {
      onSort = NOOP,
      onRowClick = NOOP,
      columns = [],
      data = [],
      rows = [],
      actions = null,
    } = this.props;

    // onRowSelection={keys => onRowClick(data[keys[0]])}>
    // {/*...headerColStyle({ width }),*/}
    // {/*...tdStyles,*/}
    return (
      <div style={{ position: 'relative', overflow: 'auto' }}>
        <table style={tableStyles}>
          <thead>
            <tr style={trStyles}>
              {columns.map(({ title, sort, width = 0 }, k) => (
                <th
                  key={k}
                  className={sort ? 'sortable' : ''}
                  style={{
                    ...thStyles,
                  }}
                  onClick={e => onSort(sort)}
                >
                  {title}
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {
              data.length == 0
                ?
                <tr style={trStyles} className="table-error">
                  <td style={tdStyles} colSpan={rows.length}>No Records To Display</td>
                </tr>
                :
                data.map((data, key) => (
                  <tr
                    style={trStyles}
                    className={onRowClick === NOOP ? '' : 'redeam-tr-hoverable'}
                    key={key}
                    onClick={e => onRowClick(e, data)}
                  >
                    {
                      rows.map((rowFn, k) =>
                        (<td
                          style={tdStyles}
                          key={k}
                        >
                          {rowFn(data)}
                        </td>),
                      )
                    }
                  </tr>
                ))
            }
          </tbody>
          <tfoot>
            {
              actions != null ?
                <tr style={trStyles}>
                  <td style={tdStyles} colSpan={rows.length}>
                    <div style={actionStyle}>
                      {actions}
                    </div>
                  </td>
                </tr>
                : null
            }
          </tfoot>
        </table>
      </div>
    );
  }
}


export const NotesCell = ({ show, value = [], ...other }) => (show ? (
  <div style={{
    ...tooltipCellStyles,
    textAlign: 'left',
  }}
  >
    <IconButton
      tooltip={<div style={noteTooltipStyles}>{value.map(
        (v, k) => <div key={k}>- {v.message}</div>,
      )}</div>}
      {...other}
    >
      <FormatAlignLeft color={tableIconColor} />
    </IconButton>
  </div>
) : null);


export class UUIDCell extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }
  render() {
    const { value, ...other } = this.props;
    return (
      <div style={{
        ...tooltipCellStyles,
      }}
      >
        <IconButton
          onClick={e => copyTextToClipboard(value)}
          tooltip={`Copy ${value}`}
          {...other}
        >
          <Visibility color={tableIconColor} />
        </IconButton>
      </div>
    );
  }
}
