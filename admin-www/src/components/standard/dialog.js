
import React, { Component, PropTypes } from 'react'
import Dialog from 'material-ui/Dialog';
import Progress from './progress'


const FetchingDialog = ({ fetching, title, children, ...other }) => (
    fetching ?
        <Dialog {...other} >
            <Progress fetching={true} />
        </Dialog>
        :
        <Dialog title={title} {...other}>
            {children}
        </Dialog>
            
)

export default FetchingDialog