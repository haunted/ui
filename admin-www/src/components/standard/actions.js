import React, {Component} from 'react'
import RaisedButton from 'material-ui/RaisedButton'

const actionStyle = {
    padding: '1em',
}
export const ActionFooter = ({ children, right=true, style, ...other }) => (
    <div style={{
        ...actionStyle,
        textAlign: right ? 'left' : 'right',    
        ...style,
    }} {...other}>
        {children}
    </div>
)

// ActionButton is a nice padded button that fits snugly in a table cell
// multiple can fit snugly together too. yay. button intimacy!
export const ActionButton = ({ style, ...props }) =>
    <RaisedButton
        style={Object.assign({ margin: '0 0.5em' }, style)}
        {...props}
    />
