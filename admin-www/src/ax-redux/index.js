/* globals window */

import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import reduxThunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { routerReducer } from 'react-router-redux';
import { reducer as form } from 'redux-form';

import accounts from './reducers/accounts';
import app, { appDefault } from './reducers/app';
import arrivals from './reducers/arrivals';
import auth from './reducers/auth';
import contacts from './reducers/contacts';
import contracts from './reducers/contracts';
import dashboard from './reducers/dashboard';
import devices from './reducers/devices';
import exceptions from './reducers/exceptions';
import failedOrders from './reducers/failed-orders';
import finances from './reducers/finances';
import groups from './reducers/groups';
import navigation from './reducers/navigation';
import news from './reducers/news';
import orgSettings from './reducers/org-settings';
import passes from './reducers/passes';
import passIssuers from './reducers/pass-issuers';
import priceLists from './reducers/price-lists';
import products from './reducers/products';
import redeemed from './reducers/redeemed';
import redemptions from './reducers/redemptions';
import reportFilters from './reducers/report-filters';
import resellers from './reducers/resellers';
import search from './reducers/search';
import shifts from './reducers/shifts';
import suppliers from './reducers/suppliers';
import tablets from './reducers/tablets';
import voucherExceptions from './reducers/voucher-exceptions';
import vouchers from './reducers/vouchers';
import notifications from './reducers/notifications';

import { LOGOUT } from './constants';

const initialState = {};

const logger = createLogger({
  level: 'info',
  collapsed: true,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const hasLogRocket = false; // window.LogRocket !== undefined


const appReducer = combineReducers({
  routing: routerReducer,
  form,
  accounts,
  app,
  arrivals,
  auth,
  contacts,
  contracts,
  dashboard,
  devices,
  exceptions,
  failedOrders,
  finances,
  groups,
  navigation,
  news,
  orgSettings,
  passes,
  passIssuers,
  priceLists,
  products,
  redeemed,
  redemptions,
  reportFilters,
  resellers,
  search,
  shifts,
  suppliers,
  tablets,
  voucherExceptions,
  vouchers,
  notifications,
});

// reset redux state on logout: https://stackoverflow.com/a/37338532
const rootReducer = (state, action) => {
  if (action.type === LOGOUT) {
    // reset org timezone setting
    window.TZ = 'America/New_York';

    // things not to clear when logged out
    const { routing, auth, app } = state;

    const appState = Object.assign({}, appDefault, {
      startupCompleteCallback: app.startupCompleteCallback,
      redirectAfterLogin: app.redirectAfterLogin,
    });

    state = {
      // can never delete routing state
      routing,
      // don't erase our callbacks in auth
      auth,
      // don't erase our callbacks in app
      app: appState,
    };
  }
  return appReducer(state, action);
};

export default createStore(
  rootReducer,
  initialState,
  hasLogRocket ? composeEnhancers(
    applyMiddleware(
      reduxThunk,
      logger, // redux devtools alone won't show errors
    ),
    LogRocket.reduxEnhancer(),
  ) : composeEnhancers(applyMiddleware(
    reduxThunk,
    logger,
  )),
);
