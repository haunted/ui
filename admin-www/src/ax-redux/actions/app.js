import client from '../../ax-client';
import {
  refreshToken,
  logout,
} from './auth';

import {
  SET_PAGE_TITLE,
  SET_SIDE_MENU_ITEMS,
  SET_SIDE_MENU_TITLE,
  STARTUP,
  ME_SPOOF_USER_ROLE,
  ME_SPOOF_USER_ORG,
  APP_CHANGE_LOCALE,
  STARTUP_DONE,
  STARTUP_FAIL,
  STARTUP_FAIL_BACK,
  ACTOUREX_CUSTOMER_SUPPORT_STAFF,
  ACTOUREX_BUSINESS_STAFF,
  ACTOUREX_TECHNICAL_STAFF,
  SUPPLIER_REDEMPTION_STAFF,
  SUPPLIER_REDEMPTION_SUPERVISOR,
  SUPPLIER_BUSINESS_STAFF,
  PASS_ISSUER_BUSINESS_STAFF,
  SET_REDIRECT_AFTER_LOGIN,
  RESELLER_BUSINESS_STAFF,
  TOGGLE_PRINT_MODE,
  ON_STARTUP_COMPLETE,
  FIRE_STARTUP_COMPLETE,
  TOKEN_REFRESH_FETCH,
  TOKEN_REFRESH_DONE,
  TOKEN_REFRESH_FAIL,
  FILTER_SET_SUPPLIER_CODE,
  FILTER_SET_RESELLER_CODE,
  FILTER_SET_PASS_ISSUER_CODE,
  FILTER_SET_RESELLER_ID,
  FILTER_SET_SUPPLIER_ID,
  FILTER_SET_PASS_ISSUER_ID,
  FILTER_SET_DEFAULTS_IN_TZ,
} from '../constants';

import { readSettings } from './org-settings';

const roleGroups = {
  [ACTOUREX_CUSTOMER_SUPPORT_STAFF]: true,
  [ACTOUREX_BUSINESS_STAFF]: true,
  [ACTOUREX_TECHNICAL_STAFF]: true,
  [SUPPLIER_REDEMPTION_STAFF]: true,
  [SUPPLIER_REDEMPTION_SUPERVISOR]: true,
  [SUPPLIER_BUSINESS_STAFF]: true,
  [RESELLER_BUSINESS_STAFF]: true,
  [PASS_ISSUER_BUSINESS_STAFF]: true,
};

export const onStartupComplete = fn => ({
  type: ON_STARTUP_COMPLETE,
  fn,
});

export const setPageTitle = pageTitle => ({
  type: SET_PAGE_TITLE,
  pageTitle,
});

export const setSideMenuItems = items => ({
  type: SET_SIDE_MENU_ITEMS,
  items,
});

export const setSideMenuTitle = title => ({
  type: SET_SIDE_MENU_TITLE,
  title,
});

export const togglePrintMode = params => ({
  type: TOGGLE_PRINT_MODE,
});

// startup should occur after we are authorized
// the auth container should not allow any other components to mount
// while starting up, because user/group information is still missing
export const startup = () => (dispatch, getState) => {
  dispatch({ type: STARTUP });
  const state = getState();
  const token = `${state.auth.authData.refreshTokenType} ${state.auth.authData.refreshToken}`;
  dispatch({ type: TOKEN_REFRESH_FETCH });
  client.auth.unsetAuthorization();

  client.auth.refreshToken(token).then((res) => {
    client.auth.setAuthorization(res.data);
    dispatch({ type: TOKEN_REFRESH_DONE, data: res.data });

    client.auth.me().then((meRes) => {
      client.accounts.listAccountGroups(meRes.data.account.id).then((groupsRes) => {
        let role = false,
          orgCode = '',
          orgType = '';

        if (groupsRes.data != null && groupsRes.data.groups != null) {
          groupsRes.data.groups.map(v => role = roleGroups[v.name] ? v.name : role);
        }

        if (!role) {
          return dispatch({
            type: STARTUP_FAIL,
            error: 'This account has has not yet been configured for use of this control panel! Please contact Redeam Support.',
          });
        }

        if ((role == SUPPLIER_REDEMPTION_STAFF ||
                 role == SUPPLIER_REDEMPTION_SUPERVISOR ||
                 role == SUPPLIER_BUSINESS_STAFF) &&
                 !meRes.data.account.orgCode) {
          return dispatch({
            type: STARTUP_FAIL,
            error: 'This account is misconfigured! Please contact Redeam Support.',
          });
        }

        setTimeout(() => {
          dispatch(refreshToken());
        }, 3000);

        dispatch({
          type: STARTUP_DONE,
          me: meRes.data,
          role,
        });

        dispatch({
          type: FILTER_SET_SUPPLIER_CODE,
          data: meRes.data.account.orgCode,
        });

        console.log('LOGGED IN: meRes.data, meRes.data.account', meRes.data, meRes.data.account);

        if (meRes.data.supplierId) {
          orgType = 'TYPE_SUPPLIER';
        } else if (meRes.data.resellerId) {
          orgType = 'TYPE_RESELLER';
        } else if (meRes.data.passIssuerId) {
          orgType = 'TYPE_PASS_ISSUER';
        }

        if (orgType) {
          dispatch(readSettings(meRes.data.account.orgCode, orgType));
        } else {
          console.log(`No orgType for orgCode: ${meRes.data.account.orgCode}`);
          dispatch({ type: FILTER_SET_DEFAULTS_IN_TZ });
        }


        dispatch({
          type: FIRE_STARTUP_COMPLETE,
        });
      },
      err => dispatch({
        type: STARTUP_FAIL,
        error: err,
      }),

      );
    },
    err => dispatch({
      type: STARTUP_FAIL,
      error: err,
    }),

    );
  })
    .catch((err) => {
      dispatch({ type: STARTUP_FAIL, error: err });
      dispatch({ type: TOKEN_REFRESH_FAIL, error: err });
      dispatch(logout());
    });
};

export const spoofUserRole = role => (dispatch, getState) => {
  let state = getState(),
    supplierCode = state.reportFilters.supplierCode,
    orgCode = supplierCode;

  if (orgCode == '') {
    orgCode = state.reportFilters.resellerCode;
  }

  if (orgCode != '') {
    dispatch({
      type: ME_SPOOF_USER_ORG,
      orgCode,
    });
  }

  dispatch({
    type: ME_SPOOF_USER_ROLE,
    role,
  });
};

export const spoofUserOrg = orgCode => (dispatch, getState) => {
  const state = getState();

  let adminResellerId = -1,
    adminSupplierId = -1,
    adminPassIssuerId = -1;

  !!state.suppliers.list.data && !!state.suppliers.list.data && state.suppliers.list.data.suppliers.map((supplier) => {
    if (supplier.supplier.code == orgCode) { adminSupplierId = supplier.supplier.id; }
  });

  !!state.resellers.list.data && !!state.resellers.list.data.resellers && state.resellers.list.data.resellers.map((reseller) => {
    if (reseller.code == orgCode) { adminResellerId = reseller.id; }
  });

  if (state.app.role.indexOf('SUPPLIER') > -1) {
    console.log('IMPERSONATE SUPPLIER');

    if (adminSupplierId != -1) {
      dispatch({
        type: FILTER_SET_SUPPLIER_ID,
        data: adminSupplierId,
      });
    }

    dispatch({
      type: FILTER_SET_SUPPLIER_CODE,
      data: orgCode,
    });
    dispatch(readSettings(orgCode, 'TYPE_SUPPLIER'));
  } else if (state.app.role.indexOf('RESELLER') > -1) {
    console.log('IMPERSONATE RESELLER');

    if (adminResellerId != -1) {
      dispatch({
        type: FILTER_SET_RESELLER_ID,
        data: adminResellerId,
      });
    }

    dispatch({
      type: FILTER_SET_RESELLER_CODE,
      data: orgCode,
    });
    dispatch(readSettings(orgCode, 'TYPE_RESELLER'));
  } else if (state.app.role.indexOf('PASS') > -1) {
    console.log('IMPERSONATE PASS ISSUER');

    if (adminPassIssuerId != -1) {
      dispatch({
        type: FILTER_SET_PASS_ISSUER_ID,
        data: adminPassIssuerId,
      });
    }

    dispatch({
      type: FILTER_SET_PASS_ISSUER_CODE,
      data: orgCode,
    });
    dispatch(readSettings(orgCode, 'TYPE_RESELLER'));
  }

  dispatch({
    type: ME_SPOOF_USER_ORG,
    orgCode,
  });
};

export const changeLocale = locale => ({
  type: APP_CHANGE_LOCALE,
  locale,
});

export const startupFailBack = () => ({
  type: STARTUP_FAIL_BACK,
});


export const setRedirectAfterLogin = path => ({
  type: SET_REDIRECT_AFTER_LOGIN,
  path,
});
