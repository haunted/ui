import client from '../../ax-client'

import {
  REGISTER_FETCH,
  REGISTER_DONE,
  REGISTER_FAIL,
  LOGIN_FETCH,
  LOGIN_DONE,
  LOGIN_FAIL,
  LOGOUT,
  LOAD_AUTHORIZATION,
  LOAD_AUTHORIZATION_FAIL,
  TOKEN_REFRESH_FETCH,
  TOKEN_REFRESH_DONE,
  TOKEN_REFRESH_FAIL,
  ON_AUTHORIZATION_COMPLETE,
  FIRE_AUTHORIZATION_COMPLETE,
  ON_AUTHORIZATION_FAILURE,
  FIRE_AUTHORIZATION_FAILURE,
} from '../constants'

import { onFailAuthorzation } from '../../ax-client/auth'

export const onAuthorizationComplete = fn => ({
  type: ON_AUTHORIZATION_COMPLETE,
  fn,
})


export const onAuthorizationFailure = fn => {
  return dispatch => {
    // hook into ax-client
    onFailAuthorzation(err => dispatch({
      type: FIRE_AUTHORIZATION_FAILURE,
      err,
    }))
    dispatch({
      type: ON_AUTHORIZATION_FAILURE,
      fn,
    })
  }
}

export const registerFetch = (email, password) => {
  return dispatch => {
    dispatch({ type: REGISTER_FETCH })
    client.auth.register({email, password})
      .then(res => { dispatch({ type: REGISTER_DONE, data: res.data }) })
      .catch(err => { dispatch({ type: REGISTER_FAIL, error: err }) })
  }
}

export const loginFetch = (org, email, password) => {
  return dispatch => {
    dispatch({ type: LOGIN_FETCH })
    client.auth.login({org, email, password})
      .then(res => {
        client.auth.setAuthorization(`${res.data.accessTokenType} ${res.data.accessToken}`)
        client.auth.setAuthorization(res.data)
        dispatch({ type: LOGIN_DONE, data: res.data })
        dispatch({ type: FIRE_AUTHORIZATION_COMPLETE })
      })
      .catch(err => {
        dispatch({ type: LOGIN_FAIL, error: err })
      })
  }
}

export const refreshToken = () => {
    return (dispatch, getState) => {
        const state = getState()
        const token = state.auth.authData.refreshTokenType +" "+ state.auth.authData.refreshToken;
        dispatch({type: TOKEN_REFRESH_FETCH})
        client.auth.unsetAuthorization()
        client.auth.refreshToken(token)
        .then(res => {
            if (APP_JS_HASH != "" && res.data.appJsHash != APP_JS_HASH) {
              if (confirm("A new version of the app is available. Reload now?")) {
                location.reload()
              }
            }
            client.auth.setAuthorization(res.data)
            dispatch({ type: TOKEN_REFRESH_DONE, data: res.data })
            setTimeout(()=>{ // keep the session going
                dispatch(refreshToken())
            }, (state.auth.authData.accessTokenExpiresIn * 1000) -4000)
        })
        .catch(err => {
            dispatch({ type: TOKEN_REFRESH_FAIL, error: err })
            dispatch(logout());
        })
    }
}


export const loadAuthorization = () => {
  return dispatch => {
    let data = client.auth.loadAuthorization()
    if (!data) {
      dispatch({ type: LOAD_AUTHORIZATION_FAIL })
      dispatch({ type: FIRE_AUTHORIZATION_FAILURE, err: "no saved authorization" })
      return
    }
    dispatch({ type: LOAD_AUTHORIZATION, data })
    dispatch({ type: FIRE_AUTHORIZATION_COMPLETE })
  }
}

export const logout = () => {
  return dispatch => {
    client.auth.logout()
    dispatch({ type: LOGOUT })
    
    // hide logrocket
    let iframes = document.getElementsByTagName("iframe")
    if (iframes.length > 0) {
      iframes[0].setAttribute("class", "")
    }
  }
}
