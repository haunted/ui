import client from '../../ax-client'
import { browserHistory } from 'react-router'

import {
    LIST_DEVICES_FETCH,
    LIST_DEVICES_DONE,
    LIST_DEVICES_FAIL,
    GET_DEVICES_FETCH,
    GET_DEVICES_DONE,
    GET_DEVICES_FAIL,
    UPDATE_DEVICE,
    UPDATE_DEVICE_DONE,
    UPDATE_DEVICE_FAIL,
    CREATE_DEVICE,
    CREATE_DEVICE_DONE,
    CREATE_DEVICE_FAIL,
    DEACTIVATE_DEVICE,
    DEACTIVATE_DEVICE_DONE,
    DEACTIVATE_DEVICE_FAIL,
    LIST_UNPROVISIONED_DEVICES_FETCH,
    LIST_UNPROVISIONED_DEVICES_DONE,
    LIST_UNPROVISIONED_DEVICES_FAIL,
    PROVISION_DEVICE,
    PROVISION_DEVICE_DONE,
    PROVISION_DEVICE_FAIL,
    DEPROVISION_DEVICE,
    DEPROVISION_DEVICE_DONE,
    DEPROVISION_DEVICE_FAIL,
    GET_NEXT_DEVICE_ASSET_ID,
    GET_NEXT_DEVICE_ASSET_ID_DONE,
    GET_NEXT_DEVICE_ASSET_ID_FAIL,
    DEVICE_APPEND_NOTE,
    DEVICE_APPEND_NOTE_DONE,
    DEVICE_APPEND_NOTE_FAIL,
    DEVICE_DELETE_NOTE,
    DEVICE_DELETE_NOTE_DONE,
    DEVICE_DELETE_NOTE_FAIL,
} from '../constants'

export const listDevices = (params) => {
    return dispatch => {
        dispatch({
            type: LIST_DEVICES_FETCH,
            params,
        })
        client.devices.listDevices(params)
            .then(res => dispatch({ type: LIST_DEVICES_DONE, data: res.data }))
            .catch(err => dispatch({ type: LIST_DEVICES_FAIL, error: err }))
    }
}

export const listUnprovisionedDevices = () => {
    return dispatch => {
        const params = {
            sort: "assetID",
            statuses: 'STATUS_NEW,STATUS_DEPROVISIONED',
        }
        dispatch({
            type: LIST_UNPROVISIONED_DEVICES_FETCH,
            params,
        })
        client.devices.listDevices(params)
            .then(res => dispatch({ type: LIST_UNPROVISIONED_DEVICES_DONE, data: res.data }))
            .catch(err => dispatch({ type: LIST_UNPROVISIONED_DEVICES_FAIL, error: err }))
    }
}


export const getDevice = (id) => {
    return dispatch => {
        dispatch({
            type: GET_DEVICES_FETCH,
        })
        client.devices.getDevice(id)
            .then(res => dispatch({ type: GET_DEVICES_DONE, data: res.data }))
            .catch(err => dispatch({ type: GET_DEVICES_FAIL, error: err }))
    }
}

export const openDeviceModal = (id) => ({
    type: OPEN_DEVICE_MODAL,
    id,
})

export const closeDeviceModal = () => ({
    type: CLOSE_DEVICE_MODAL,
})

export const updateDevice = (data) => {
    return dispatch => {
        dispatch({
            type: UPDATE_DEVICE,
            data,
        })
        client.devices.updateDevice(data)
            .then(res => {
                dispatch({ type: UPDATE_DEVICE_DONE, data: res.data })
            })
            .catch(err => dispatch({ type: UPDATE_DEVICE_FAIL, error: err }))
    }
}

export const createDevice = (data) => {
    return dispatch => {
        dispatch({
            type: CREATE_DEVICE,
            data,
        })
        client.devices.createDevice(data)
            .then(res => {
                dispatch({ type: CREATE_DEVICE_DONE, data: res.data })
            })
            .catch(err => dispatch({ type: CREATE_DEVICE_FAIL, error: err }))
    }
}

export const deactivateDevice = (id, v, reason) => {
    return dispatch => {
        dispatch({
            type: DEACTIVATE_DEVICE,
        })
        client.devices.deactivateDevice(id, v, reason)
            .then(res => {
                dispatch({ type: DEACTIVATE_DEVICE_DONE, data: res.data })
            })
            .catch(err => dispatch({ type: DEACTIVATE_DEVICE_FAIL, error: err }))
    }
}

export const provisionDevice = (id, v, supplierCode) => {
    return dispatch => {
        dispatch({
            type: PROVISION_DEVICE,
        })
        client.devices.provisionDevice(id, v, supplierCode)
            .then(res => {
                dispatch({ type: PROVISION_DEVICE_DONE, data: res.data })
            })
            .catch(err => dispatch({ type: PROVISION_DEVICE_FAIL, error: err }))
    }
}

export const deprovisionDevice = (id, v) => {
    return dispatch => {
        dispatch({
            type: DEPROVISION_DEVICE,
        })
        client.devices.deprovisionDevice(id, v)
            .then(res => {
                dispatch({ type: DEPROVISION_DEVICE_DONE, data: res.data })
            })
            .catch(err => dispatch({ type: DEPROVISION_DEVICE_FAIL, error: err }))
    }
}
export const getNextAssetId = () => {
    return dispatch => {
        dispatch({
            type: GET_NEXT_DEVICE_ASSET_ID,
        })
        client.devices.getNextAssetId()
            .then(res => dispatch({ type: GET_NEXT_DEVICE_ASSET_ID_DONE, data: res.data }))
            .catch(err => dispatch({ type: GET_NEXT_DEVICE_ASSET_ID_FAIL, error: err }))
    }
}

export const appendNote = (id, v, message) => {
    return dispatch => {
        dispatch({ type: DEVICE_APPEND_NOTE })
        client.devices.appendNote(id, v, message)
            .then(res => {
                dispatch({ type: DEVICE_APPEND_NOTE_DONE, data: res.data })
                browserHistory.push(`app/redeam/devices/${id}/${parseInt(v)+1}/notes`)
            })
            .catch(err => dispatch({ type: DEVICE_APPEND_NOTE_FAIL, error: err }))
    }
}

export const deleteNote = (id, v, message) => {
    return dispatch => {
        dispatch({ type: DEVICE_DELETE_NOTE })
        client.devices.deleteNote(id, v, message)
            .then(res => {
                dispatch({ type: DEVICE_DELETE_NOTE_DONE, data: res.data })
                browserHistory.push(`app/redeam/devices/${id}/${parseInt(v)+1}/notes`)
            })
            .catch(err => dispatch({ type: DEVICE_DELETE_NOTE_FAIL, error: err }))
    }
}