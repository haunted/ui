import client from '../../ax-client'
import {formatCurrency} from '../../util'

import {
    PRICE_LIST_FETCH,
    PRICE_LIST_FETCH_DONE,
    PRICE_LIST_FETCH_FAIL,
    PRICE_LISTS_FETCH,
    PRICE_LISTS_FETCH_DONE,
    PRICE_LISTS_FETCH_FAIL,
    PRICE_LIST_CREATE_FETCH,
    PRICE_LIST_CREATE_DONE,
    PRICE_LIST_CREATE_FAIL,
    PRICE_LIST_UPDATE_FETCH,
    PRICE_LIST_UPDATE_DONE,
    PRICE_LIST_UPDATE_FAIL,
    PRICE_LIST_DISABLE_FETCH,
    PRICE_LIST_DISABLE_DONE,
    PRICE_LIST_DISABLE_FAIL,
    PRICE_LIST_ITEMS_FETCH,
    PRICE_LIST_ITEMS_FETCH_FAIL,
    PRICE_LIST_ITEMS_FETCH_DONE,
    PRICE_LIST_ITEM_FETCH,
    PRICE_LIST_ITEM_FETCH_DONE,
    PRICE_LIST_ITEM_FETCH_FAIL,
    PRICE_LIST_ITEM_CREATE_FETCH,
    PRICE_LIST_ITEM_CREATE_DONE,
    PRICE_LIST_ITEM_CREATE_FAIL,
    PRICE_LIST_ITEM_UPDATE_FETCH,
    PRICE_LIST_ITEM_UPDATE_DONE,
    PRICE_LIST_ITEM_UPDATE_FAIL,
    PRICE_LIST_ITEM_DISABLE_FETCH,
    PRICE_LIST_ITEM_DISABLE_DONE,
    PRICE_LIST_ITEM_DISABLE_FAIL,
    CATALOG_ITEM_PRICE_LISTS_FETCH,
    CATALOG_ITEM_PRICE_LISTS_FETCH_DONE,
    CATALOG_ITEM_PRICE_LISTS_FETCH_FAIL,
    PRICE_LISTS_FILTER_UPDATE,
    PRICE_LIST_ITEMS_FILTER_UPDATE
} from '../constants'
import { browserHistory } from 'react-router'

/** PriceLists **/
export const listPriceLists = (supplierId) => {
  return dispatch => {
    dispatch({ type: PRICE_LISTS_FETCH })
    client.suppliers.listPriceLists(supplierId)
      .then(res => dispatch({ type: PRICE_LISTS_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LISTS_FETCH_FAIL, error: err }))
  }
}
export const getPriceList = (supplierId, id) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_FETCH })
    client.suppliers.getPriceList(supplierId, id)
      .then(res => dispatch({ type: PRICE_LIST_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_FETCH_FAIL, error: err }))
  }
}
export const createPriceList = (supplierId, data) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_CREATE_FETCH })
    client.suppliers.createPriceList(supplierId, data)
      .then(res => dispatch({ type: PRICE_LIST_CREATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_CREATE_FAIL, error: err }))
  }
}
export const updatePriceList = (supplierId, id, data) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_UPDATE_FETCH })
    client.suppliers.updatePriceList(supplierId, id, data)
      .then(res => dispatch({ type: PRICE_LIST_UPDATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_UPDATE_FAIL, error: err }))
  }
}
export const disablePriceList = (supplierId, id, version) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_DISABLE_FETCH })
    client.suppliers.disablePriceList(supplierId, id, version)
      .then(res => dispatch({ type: PRICE_LIST_DISABLE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_DISABLE_FAIL, error: err }))
  }
}

/** PriceListItems **/
export const listPriceListItems = (supplierId, priceListId) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_ITEMS_FETCH })
    client.suppliers.listPriceListItems(supplierId, priceListId)
      .then(res => dispatch({ type: PRICE_LIST_ITEMS_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_ITEMS_FETCH_FAIL, error: err }))
  }
}
export const getPriceListItem = (supplierId, priceListId, id) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_ITEM_FETCH })
    client.suppliers.getPriceListItem(supplierId, priceListId, id)
      .then(res => dispatch({ type: PRICE_LIST_ITEM_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_ITEM_FETCH_FAIL, error: err }))
  }
}
export const createPriceListItem = (supplierId, priceListId, data) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_ITEM_CREATE_FETCH })
    client.suppliers.createPriceListItem(supplierId, priceListId, data)
      .then(res => dispatch({ type: PRICE_LIST_ITEM_CREATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_ITEM_CREATE_FAIL, error: err }))
  }
}
export const updatePriceListItem = (supplierId, priceListId, id, data) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_ITEM_UPDATE_FETCH })
    client.suppliers.updatePriceListItem(supplierId, priceListId, id, data)
      .then(res => dispatch({ type: PRICE_LIST_ITEM_UPDATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_ITEM_UPDATE_FAIL, error: err }))
  }
}
export const disablePriceListItem = (supplierId, priceListId, id, version) => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_ITEM_DISABLE_FETCH })
    client.suppliers.disablePriceListItem(supplierId, priceListId, id, version)
      .then(res => dispatch({ type: PRICE_LIST_ITEM_DISABLE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRICE_LIST_ITEM_DISABLE_FAIL, error: err }))
  }
}
export const listPriceListsByCatalogItem = (supplierId, catalogId, id) => {
  return dispatch => {
    dispatch({ type: CATALOG_ITEM_PRICE_LISTS_FETCH })
    client.suppliers.listPriceListsByCatalogItem(supplierId, catalogId, id)
      .then(res => dispatch({ type: CATALOG_ITEM_PRICE_LISTS_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: CATALOG_ITEM_PRICE_LISTS_FETCH_FAIL, error: err }))
  }
}

export const priceListItemsFilterUpdate = () => {
  return dispatch => {
    dispatch({ type: PRICE_LIST_ITEMS_FILTER_UPDATE })
  }
}
export const priceListsFilterUpdate = () => {
  return dispatch => {
    dispatch({ type: PRICE_LISTS_FILTER_UPDATE })
  }
}