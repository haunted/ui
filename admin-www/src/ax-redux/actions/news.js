import client from '../../ax-client'
import {
  NEWS_FETCH,
  NEWS_FETCH_FAIL,
  NEWS_FETCH_DONE,
  NEWS_CREATE,
  NEWS_CREATE_FAIL,
  NEWS_CREATE_DONE,
} from '../constants'

export const fetch = function() {
  return (dispatch, getState) => {
    dispatch({ type: NEWS_FETCH })
    client.news.getNews()
      .then(res => {
        dispatch(fetchDone(res.data))
      })
      .catch(err => dispatch(fetchFail(err)))
  }
}

export const fetchFail = (err) => ({
  type: NEWS_FETCH_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const fetchDone = (data) => ({
  type: NEWS_FETCH_DONE,
  data: data,
  receivedAt: Date.now(),
})

export const create = function(news) {
  return (dispatch, getState) => {
    dispatch({ type: NEWS_CREATE })
    client.news.createNews(news)
      .then(res => {
        dispatch(createDone(res.data))
      })
      .catch(err => dispatch(createFail(err)))
  }
}

export const createFail = (err) => ({
  type: NEWS_CREATE_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const createDone = (data) => ({
  type: NEWS_CREATE_DONE,
  data: data,
  receivedAt: Date.now(),
})
