import client from '../../ax-client'
import {
    REDEMPTION_VOID_FETCH,
    REDEMPTION_VOID_DONE,
    REDEMPTION_VOID_FAIL,
} from '../constants'

import { browserHistory } from 'react-router';

export const voidRedemption = (id, data) => {
  return dispatch => {
    dispatch({ type: REDEMPTION_VOID_FETCH })
    client.redemptions.voidRedemption(id, data)
      .then(res => dispatch({ type: REDEMPTION_VOID_DONE, data: res.data }))
      .catch(err => dispatch({ type: REDEMPTION_VOID_FAIL, error: err }))
  }
}