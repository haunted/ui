import { browserHistory } from 'react-router'
import client from '../../ax-client'
import {
  LIST_PASS_ISSUERS_FETCH,
  LIST_PASS_ISSUERS_DONE,
  LIST_PASS_ISSUERS_FAIL,
  GET_PASS_ISSUER_FETCH,
  GET_PASS_ISSUER_DONE,
  GET_PASS_ISSUER_FAIL,
  CREATE_PASS_ISSUER_FETCH,
  CREATE_PASS_ISSUER_DONE,
  CREATE_PASS_ISSUER_FAIL,
  UPDATE_PASS_ISSUER_FETCH,
  UPDATE_PASS_ISSUER_DONE,
  UPDATE_PASS_ISSUER_FAIL,
  DISABLE_PASS_ISSUER_FETCH,
  DISABLE_PASS_ISSUER_DONE,
  DISABLE_PASS_ISSUER_FAIL,
  PASS_ISSUER_CATALOG_CREATE_FETCH,
  PASS_ISSUER_CATALOG_CREATE_DONE,
  PASS_ISSUER_CATALOG_CREATE_FAIL,
  PASS_ISSUER_CATALOG_UPDATE_FETCH,
  PASS_ISSUER_CATALOG_UPDATE_DONE,
  PASS_ISSUER_CATALOG_UPDATE_FAIL,
  PASS_ISSUER_CATALOGS_LIST_FETCH,
  PASS_ISSUER_CATALOGS_LIST_DONE,
  PASS_ISSUER_CATALOGS_LIST_FAIL,
  PASS_ISSUER_CATALOG_FETCH,
  PASS_ISSUER_CATALOG_FETCH_DONE,
  PASS_ISSUER_CATALOG_FETCH_FAIL,
  PASS_ISSUER_CATALOG_DISABLE_FETCH,
  PASS_ISSUER_CATALOG_DISABLE_DONE,
  PASS_ISSUER_CATALOG_DISABLE_FAIL,
  FILTER_SET_PASS_ISSUER_CATALOG_ID
} from '../constants'

export const listPassIssuers = (params={}) => {
  return dispatch => {
    dispatch({ type: LIST_PASS_ISSUERS_FETCH, params: params })
    client.passIssuers.listPassIssuers(params)
      .then(res => dispatch({ type: LIST_PASS_ISSUERS_DONE, data: res.data }))
      .catch(err => dispatch({ type: LIST_PASS_ISSUERS_FAIL, error: err }))
  }
}

export const getPassIssuer = id => {

  return dispatch => {

    dispatch({ type: GET_PASS_ISSUER_FETCH, id })
    client.passIssuers.getPassIssuer(id)
      .then(res => dispatch({ type: GET_PASS_ISSUER_DONE, data: res.data }))
      .catch(err => dispatch({ type: GET_PASS_ISSUER_FAIL, error: err }))

  }

}

export const createPassIssuer = data => {

  return dispatch => {

    dispatch({ type: CREATE_PASS_ISSUER_FETCH })
    client.passIssuers.createPassIssuer(data)
      .then(res => {
          dispatch({ type: CREATE_PASS_ISSUER_DONE, data: res.data })
          browserHistory.push("/app/redeam/pass-issuers");
      })
      .catch(err => dispatch({ type: CREATE_PASS_ISSUER_FAIL, error: err }))

  }

}

export const updatePassIssuer = (id, data) => {

  return dispatch => {

    dispatch({ type: UPDATE_PASS_ISSUER_FETCH })
    client.passIssuers.updatePassIssuer(id, data)
      .then(res => {
          dispatch({ type: UPDATE_PASS_ISSUER_DONE, data: res.data })
          browserHistory.push("/app/redeam/pass-issuers");
      })
      .catch(err => dispatch({ type: UPDATE_PASS_ISSUER_FAIL, error: err }))

  }

}

export const disablePassIssuer = (id, version) => {

  return (dispatch, getState) => {

    dispatch({ type: DISABLE_PASS_ISSUER_FETCH })
    client.passIssuers.disablePassIssuer(id, version)
      .then(res => {
          dispatch({ type: DISABLE_PASS_ISSUER_DONE, data: res.data })
          browserHistory.push("/app/redeam/pass-issuers")
      })
      .catch(err => dispatch({ type: DISABLE_PASS_ISSUER_FAIL, error: err }))

  }

}

export const listPassIssuerCatalogs = (passIssuerId) => {

  if ( !!!passIssuerId || passIssuerId == -1 )

    return { type: PASS_ISSUER_CATALOGS_LIST_FAIL, error: { message: "passIssuerId missing"} }

  return (dispatch, getState) => {

    dispatch({ type: PASS_ISSUER_CATALOGS_LIST_FETCH, params: passIssuerId })
    client.passIssuers.listPassIssuerCatalogs(passIssuerId)
      .then(res => {

        dispatch({ type: PASS_ISSUER_CATALOGS_LIST_DONE, data: res.data })
        _lookUpDefaultPassIssuerCatalog( dispatch, res.data.catalogs, false )

      })
      .catch(err => {
        dispatch({ type: PASS_ISSUER_CATALOGS_LIST_FAIL, error: err })
      })

  }

}

let _lookUpDefaultPassIssuerCatalog = ( dispatch, passIssuerCatalogs, noDefault ) => {

    let foundDefault = false

    if ( !!!passIssuerCatalogs || typeof passIssuerCatalogs.map != 'function' )

        return

    passIssuerCatalogs.map((catalog, index) => {

        if ( (catalog.typeOf == 'TYPE_PASS_ISSUER' && ( catalog.name == "Default" || noDefault )) || passIssuerCatalogs.length == 1 ) {

            if ( catalog.name == "Default" )

                foundDefault = true

            dispatch({
              type: FILTER_SET_PASS_ISSUER_CATALOG_ID,
              data: catalog.id
            })
        }

    })
        
    if ( foundDefault == false && !!!noDefault )

        _lookUpDefaultPassIssuerCatalog( dispatch, passIssuerCatalogs, true )
        
}

export const getPassIssuerCatalog = (passIssuerId, id) => {

  return dispatch => {

    dispatch({ type: PASS_ISSUER_CATALOG_FETCH })
    client.passIssuers.getPassIssuerCatalog(passIssuerId, id)
      .then(res => dispatch({ type: PASS_ISSUER_CATALOG_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PASS_ISSUER_CATALOG_FETCH_FAIL, error: err }))

  }

}

export const createPassIssuerCatalog = (passIssuerId, data) => {

  return dispatch => {

    dispatch({ type: PASS_ISSUER_CATALOG_CREATE_FETCH })
    client.passIssuers.createPassIssuerCatalog(passIssuerId, data)
      .then(res => {
          dispatch({ type: PASS_ISSUER_CATALOG_CREATE_DONE, data: res.data })
      })
      .catch(err => dispatch({ type: PASS_ISSUER_CATALOG_CREATE_FAIL, error: err }))

  }

}

export const updatePassIssuerCatalog = (passIssuerId, id, data) => {

  return dispatch => {

    dispatch({ type: PASS_ISSUER_CATALOG_UPDATE_FETCH })
    client.passIssuers.updatePassIssuerCatalog(passIssuerId, id, data)
      .then(res => {
          dispatch({ type: PASS_ISSUER_CATALOG_UPDATE_DONE, data: res.data })
      })
      .catch(err => dispatch({ type: PASS_ISSUER_CATALOG_UPDATE_FAIL, error: err }))

  }

}

export const disablePassIssuerCatalog = (passIssuerId, id, version) => {

  return dispatch => {

    dispatch({ type: PASS_ISSUER_CATALOG_DISABLE_FETCH })
    client.passIssuers.disablePassIssuerCatalog(passIssuerId, id, version)
      .then(res => {
          dispatch({ type: PASS_ISSUER_CATALOG_DISABLE_DONE, data: res.data })
      })
      .catch(err => dispatch({ type: PASS_ISSUER_CATALOG_DISABLE_FAIL, error: err }))

  }

}
