import client from '../../ax-client'
import {
  NAV_SET_RETURN_TO_REDEMPTIONS_URL,
  NAV_SET_RETURN_TO_RESELLER_REDEMPTIONS_URL,
  NAV_SET_RETURN_TO_SHIFT_SUMMARY_URL,
  NAV_SET_RETURN_TO_SHIFT_URL,
  NAV_SET_RETURN_TO_FAILED_ORDERS_URL,
  NAV_SET_RETURN_TO_DEVICES_URL,
} from '../constants'

export const setReturnToRedemptionsURL = url => ({
  type: NAV_SET_RETURN_TO_REDEMPTIONS_URL,
  data: url,
})
export const setReturnToResellerRedemptionsURL = url => ({
  type: NAV_SET_RETURN_TO_RESELLER_REDEMPTIONS_URL,
  data: url,
})

export const setReturnToShiftSummaryURL = url => ({
  type: NAV_SET_RETURN_TO_SHIFT_SUMMARY_URL,
  data: url,
})

export const setReturnToShiftURL = url => ({
  type: NAV_SET_RETURN_TO_SHIFT_URL,
  data: url,
})

export const setReturnToFailedOrdersURL = url => ({
  type: NAV_SET_RETURN_TO_FAILED_ORDERS_URL,
  data: url,
})

export const setReturnToDevicesURL = url => ({
  type: NAV_SET_RETURN_TO_DEVICES_URL,
  data: url,
})
