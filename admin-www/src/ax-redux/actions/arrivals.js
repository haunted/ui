import client from '../../ax-client'
import {
  STATUS_OPEN,
  STATUS_CANCELLED,
  STATUS_REDEEMED
} from '../../ax-client/tickets'

import {
  ARRIVALS_FETCH,
  ARRIVALS_FETCH_FAIL,
  ARRIVALS_FETCH_DONE,
  ARRIVALS_SET_HIDDEN_TOURS,
  ACTOUREX_TECHNICAL_STAFF,
} from '../constants'

// Arrivals

export const setHiddenTours = hiddenTours => ({
  type: ARRIVALS_SET_HIDDEN_TOURS,
  hiddenTours,
})

export const fetch = function(params = {}) {
  return (dispatch, getState) => {
    let state = getState()

    dispatch({ type: ARRIVALS_FETCH })
    params.status = STATUS_OPEN
    client.tickets.tickets(params)
      .then(res => {
        dispatch(fetchDone(res.data.tickets ? res.data.tickets : []))
      })
      .catch(err => dispatch(fetchFail(err)))
  }
}

export const fetchFail = (err) => ({
  type: ARRIVALS_FETCH_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const fetchDone = (tickets) => ({
  type: ARRIVALS_FETCH_DONE,
  data: tickets,
  receivedAt: Date.now(),
})

function exportParams(getState, params) {
  let state = getState()
  params.status = STATUS_OPEN
  return params
}

export const exportCSV = (params = {}) => {
  return (dispatch, getState) => {
    client.tickets.ticketsOpenCSV(exportParams(getState, params))
  }
}

export const exportXLSX = (params = {}) => {
  return (dispatch, getState) => {
    client.tickets.ticketsOpenXLSX(exportParams(getState, params))
  }
}

import {
  ARRIVALS_FETCH_DETAILS,
  ARRIVALS_FETCH_DETAILS_FAIL,
  ARRIVALS_FETCH_DETAILS_DONE
} from '../constants'

export const fetchDetails = function(id) {
  return (dispatch, getState) => {
    let state = getState();
    let params = {};
    dispatch({ type: ARRIVALS_FETCH_DETAILS })

    if (state.app.role != ACTOUREX_TECHNICAL_STAFF && state.app.user.account.orgCode != "") {
      params.supplierCode = getState().app.user.account.orgCode
    }

    client.tickets.ticketById(id, params)
      .then(res => dispatch(fetchDetailsDone(res.data)))
      .catch(err => dispatch(fetchDetailsFail(err)))
  }
}

export const fetchDetailsFail = (err) => ({
  type: ARRIVALS_FETCH_DETAILS_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const fetchDetailsDone = (deets) => ({
  type: ARRIVALS_FETCH_DETAILS_DONE,
  data: deets.result,
  receivedAt: Date.now(),
})
