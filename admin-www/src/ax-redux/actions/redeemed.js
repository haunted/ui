import client from '../../ax-client'
import {
  STATUS_OPEN,
  STATUS_CANCELLED,
  STATUS_REDEEMED
} from '../../ax-client/tickets'

import {
  REDEEMED_FETCH,
  REDEEMED_FETCH_FAIL,
  REDEEMED_FETCH_DONE,
  REDEEMED_SET_START_DATE,
  REDEEMED_SET_END_DATE,
  ACTOUREX_TECHNICAL_STAFF,
  RESELLER_BUSINESS_STAFF,
} from '../constants'

export const setStartDate = (date) => ({
  type: REDEEMED_SET_START_DATE,
  date
})
export const setEndDate = (date) => ({
  type: REDEEMED_SET_END_DATE,
  date
})

export const fetch = function(params = {}) {
  return (dispatch, getState) => {
    let state = getState();
    dispatch({ type: REDEEMED_FETCH })
    if (state.app.role != ACTOUREX_TECHNICAL_STAFF && state.app.user.account.orgCode != "") {
      if (state.app.role == RESELLER_BUSINESS_STAFF) {
        // params.resellerCode = getState().app.user.account.orgCode        
      } else {
        params.supplierCode = getState().app.user.account.orgCode
      }
    }
    params.status = STATUS_REDEEMED
    client.redemptions.redemptions(params)
      .then(res => dispatch(fetchDone(res.data.tickets ? res.data.tickets : [])))
      .catch(err => dispatch(fetchFail(err)))
  }
}

export const fetchFail = (err) => ({
  type: REDEEMED_FETCH_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const fetchDone = (tickets) => ({
  type: REDEEMED_FETCH_DONE,
  data: tickets,
  receivedAt: Date.now(),
})

function exportParams(getState, params) {
  let state = getState()
  if (state.app.role != ACTOUREX_TECHNICAL_STAFF && state.app.role != RESELLER_BUSINESS_STAFF && state.app.user.account.orgCode != "") {
    params.supplierCode = state.app.user.account.orgCode
  }
  params.status = STATUS_REDEEMED
  return params
}

export const exportCSV = (params = {}) => {
  return (dispatch, getState) => {
    client.redemptions.redemptionsOpenCSV(exportParams(getState, params))
  }
}

export const exportXLSX = (params = {}) => {
  return (dispatch, getState) => {
    client.redemptions.redemptionsOpenXLSX(exportParams(getState, params))
  }
}


import {
  REDEEMED_FETCH_DETAILS,
  REDEEMED_FETCH_DETAILS_FAIL,
  REDEEMED_FETCH_DETAILS_DONE
} from '../constants'

export const fetchDetails = function(id) {
  return (dispatch, getState) => {
    let state = getState();
    let params = {};
    dispatch({ type: REDEEMED_FETCH_DETAILS })

    if (state.app.role != ACTOUREX_TECHNICAL_STAFF && state.app.user.account.orgCode != "") {
      params.supplierCode = state.app.user.account.orgCode
    }

    client.tickets.ticketById(id, params)
      .then(res => dispatch(fetchDetailsDone(res.data)))
      .catch(err => dispatch(fetchDetailsFail(err)))
  }
}

export const fetchDetailsFail = (err) => ({
  type: REDEEMED_FETCH_DETAILS_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const fetchDetailsDone = (deets) => ({
  type: REDEEMED_FETCH_DETAILS_DONE,
  data: deets.result,
  receivedAt: Date.now(),
})
