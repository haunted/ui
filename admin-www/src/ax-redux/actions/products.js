import client from '../../ax-client'
import {formatCurrency} from '../../util'

import {
    PRODUCT_FETCH,
    PRODUCT_FETCH_DONE,
    PRODUCT_FETCH_FAIL,
    PRODUCTS_FETCH,
    PRODUCTS_FETCH_DONE,
    PRODUCTS_FETCH_FAIL,
    PRODUCT_CREATE_FETCH,
    PRODUCT_CREATE_DONE,
    PRODUCT_CREATE_FAIL,
    PRODUCT_UPDATE_FETCH,
    PRODUCT_UPDATE_DONE,
    PRODUCT_UPDATE_FAIL,
    PRODUCT_DELETE_FETCH,
    PRODUCT_DELETE_DONE,
    PRODUCT_DELETE_FAIL,
    SUPPLIER_PRODUCTS_FETCH,
    SUPPLIER_PRODUCTS_FETCH_DONE,
    SUPPLIER_PRODUCTS_FETCH_FAIL,
    PASS_PRODUCT_GET_FETCH,
    PASS_PRODUCT_GET_DONE,
    PASS_PRODUCT_GET_FAIL,
    PASS_PRODUCTS_LIST_FETCH,
    PASS_PRODUCTS_LIST_FETCH_DONE,
    PASS_PRODUCTS_LIST_FETCH_FAIL,
    PASS_PRODUCT_CREATE_FETCH,
    PASS_PRODUCT_CREATE_DONE,
    PASS_PRODUCT_CREATE_FAIL,
    PASS_PRODUCT_UPDATE_FETCH,
    PASS_PRODUCT_UPDATE_DONE,
    PASS_PRODUCT_UPDATE_FAIL,
    CONNECTED_PRODUCTS_LIST_FETCH,
    CONNECTED_PRODUCTS_LIST_DONE,
    CONNECTED_PRODUCTS_LIST_FAIL,
    PENDING_PRODUCTS_LIST_FETCH,
    PENDING_PRODUCTS_LIST_DONE,
    PENDING_PRODUCTS_LIST_FAIL,
    CONNECTED_PRODUCT_FETCH,
    CONNECTED_PRODUCT_FETCH_DONE,
    CONNECTED_PRODUCT_FETCH_FAIL,
    PENDING_PRODUCT_FETCH,
    PENDING_PRODUCT_FETCH_DONE,
    PENDING_PRODUCT_FETCH_FAIL,
    CONNECTED_PRODUCT_EDIT_FETCH,
    CONNECTED_PRODUCT_EDIT_DONE,
    CONNECTED_PRODUCT_EDIT_FAIL,
    PENDING_PRODUCT_SETUP_FETCH,
    PENDING_PRODUCT_SETUP_DONE,
    PENDING_PRODUCT_SETUP_FAIL

} from '../constants'
import { browserHistory } from 'react-router';

export const listProducts = (params) => {
  return dispatch => {
    dispatch({ type: PRODUCTS_FETCH })
    client.products.listProducts(params)
      .then(res => dispatch({ type: PRODUCTS_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRODUCTS_FETCH_FAIL, error: err }))
  }
}
export const getSupplierProducts = (supplierCode, params) => {
  return dispatch => {
    dispatch({ type: SUPPLIER_PRODUCTS_FETCH })
    client.products.getSupplierProducts(supplierCode, params)
      .then(res => dispatch({ type: SUPPLIER_PRODUCTS_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: SUPPLIER_PRODUCTS_FETCH_FAIL, error: err }))
  }
}
export const readProduct = id => {
  return dispatch => {
    dispatch({ type: PRODUCT_FETCH })
    client.products.readProduct(id)
      .then(res => dispatch({ type: PRODUCT_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRODUCT_FETCH_FAIL, error: err }))
  }
}
export const createProduct = (supplierCode, params) => {
  return dispatch => {
    dispatch({ type: PRODUCT_CREATE_FETCH })
    client.products.createProduct(supplierCode, params)
      .then(res => dispatch({ type: PRODUCT_CREATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRODUCT_CREATE_FAIL, error: err }))
  }
}
export const updateProduct = (id, data) => {
  return dispatch => {
    dispatch({ type: PRODUCT_UPDATE_FETCH })
    client.products.updateProduct(id, data)
      .then(res => dispatch({ type: PRODUCT_UPDATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRODUCT_UPDATE_FAIL, error: err }))
  }
}
export const deleteProduct = (params) => {
  return dispatch => {
    dispatch({ type: PRODUCT_DELETE_FETCH })
    client.products.deleteProduct(params)
      .then(res => dispatch({ type: PRODUCT_DELETE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PRODUCT_DELETE_FAIL, error: err }))
  }
}
export const listPassProducts = (params) => {
  return dispatch => {
    dispatch({ type: PASS_PRODUCTS_LIST_FETCH })
    client.products.listPassProducts(params)
      .then(res => dispatch({ type: PASS_PRODUCTS_LIST_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PASS_PRODUCTS_LIST_FETCH_FAIL, error: err }))

  }
}
export const getPassProduct = id => {
  return dispatch => {
    dispatch({ type: PASS_PRODUCT_GET_FETCH })
    client.products.getPassProduct(id)
      .then(res => dispatch({ type: PASS_PRODUCT_GET_DONE, data: res.data }))
      .catch(err => dispatch({ type: PASS_PRODUCT_GET_FAIL, error: err }))
  }
}
export const createPassProduct = (params) => {
  return dispatch => {
    dispatch({ type: PASS_PRODUCT_CREATE_FETCH })
    client.products.createPassProduct(params)
      .then(res => dispatch({ type: PASS_PRODUCT_CREATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PASS_PRODUCT_CREATE_FAIL, error: err }))
  }
}
export const updatePassProduct = (id, data) => {
  return dispatch => {
    dispatch({ type: PASS_PRODUCT_UPDATE_FETCH })
    client.products.updatePassProduct(id, data)
      .then(res => dispatch({ type: PASS_PRODUCT_UPDATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: PASS_PRODUCT_UPDATE_FAIL, error: err }))
  }
}
export const listConnectedProducts = (resellerId, catalogId) => {
  return dispatch => {
    dispatch({ type: CONNECTED_PRODUCTS_LIST_FETCH })
    client.products.listConnectedProducts(resellerId, catalogId)
      .then(res => dispatch({ type: CONNECTED_PRODUCTS_LIST_DONE, data: res.data }))
      .catch(err => dispatch({ type: CONNECTED_PRODUCTS_LIST_FAIL, error: err }))
  }
}
export const listPendingProducts = (resellerId, catalogId, params) => {
  return dispatch => {
    dispatch({ type: PENDING_PRODUCTS_LIST_FETCH })
    client.products.listPendingProducts(resellerId, catalogId, params)
      .then(res => dispatch({ type: PENDING_PRODUCTS_LIST_DONE, data: res.data }))
      .catch(err => dispatch({ type: PENDING_PRODUCTS_LIST_FAIL, error: err }))
  }
}
export const getConnectedProduct = (resellerId, productId) => {
  return dispatch => {
    dispatch({ type: CONNECTED_PRODUCT_FETCH })
    client.products.getConnectedProduct(resellerId, productId)
      .then(res => dispatch({ type: CONNECTED_PRODUCT_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: CONNECTED_PRODUCT_FETCH_FAIL, error: err }))
  }
}
export const getPendingProduct = (resellerId, productId) => {
  return dispatch => {
    dispatch({ type: PENDING_PRODUCT_FETCH })
    client.products.getPendingProduct(resellerId, productId)
      .then(res => dispatch({ type: PENDING_PRODUCT_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: PENDING_PRODUCT_FETCH_FAIL, error: err }))
  }
}
export const editConnectedProduct = (catalogId, productId, data) => {
  return dispatch => {
    dispatch({ type: CONNECTED_PRODUCT_EDIT_FETCH })
    client.products.editConnectedProduct(catalogId, productId, data)
      .then(res => dispatch({ type: CONNECTED_PRODUCT_EDIT_DONE, data: res.data }))
      .catch(err => dispatch({ type: CONNECTED_PRODUCT_EDIT_FAIL, error: err }))
  }
}
export const setupPendingProduct = (catalogId, productId, data) => {
  return dispatch => {
    dispatch({ type: PENDING_PRODUCT_SETUP_FETCH })
    client.products.setupPendingProduct(catalogId, productId, data)
      .then(res => dispatch({ type: PENDING_PRODUCT_SETUP_DONE, data: res.data }))
      .catch(err => dispatch({ type: PENDING_PRODUCT_SETUP_FAIL, error: err }))
  }
}
