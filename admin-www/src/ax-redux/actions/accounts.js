import { browserHistory } from 'react-router';
import { NotificationManager } from 'react-notifications';
import client from '../../ax-client';
import {
  CREATE_ACCOUNT_FETCH,
  CREATE_ACCOUNT_DONE,
  CREATE_ACCOUNT_FAIL,
  LIST_ACCOUNTS_FETCH,
  LIST_ACCOUNTS_DONE,
  LIST_ACCOUNTS_FAIL,
  READ_ACCOUNT_FETCH,
  READ_ACCOUNT_DONE,
  READ_ACCOUNT_FAIL,
  UPDATE_ACCOUNT_FETCH,
  UPDATE_ACCOUNT_DONE,
  UPDATE_ACCOUNT_FAIL,
  DELETE_ACCOUNT_FETCH,
  DELETE_ACCOUNT_DONE,
  DELETE_ACCOUNT_FAIL,
  LIST_ACCOUNT_PERMISSIONS_FETCH,
  LIST_ACCOUNT_PERMISSIONS_DONE,
  LIST_ACCOUNT_PERMISSIONS_FAIL,
  UPDATE_ACCOUNT_PERMISSIONS_FETCH,
  UPDATE_ACCOUNT_PERMISSIONS_DONE,
  UPDATE_ACCOUNT_PERMISSIONS_FAIL,
  LIST_ACCOUNT_GROUPS_FETCH,
  LIST_ACCOUNT_GROUPS_DONE,
  LIST_ACCOUNT_GROUPS_FAIL,
  ACCOUNT_UPDATE_PASSWORD_FETCH,
  ACCOUNT_UPDATE_PASSWORD_DONE,
  ACCOUNT_UPDATE_PASSWORD_FAIL,
  ACCOUNT_SET_INITIAL_GROUPS,
  ADD_ACCOUNT_TO_GROUP_FETCH,
  ADD_ACCOUNT_TO_GROUP_DONE,
  ADD_ACCOUNT_TO_GROUP_FAIL,
} from '../constants';

export const createAccount = data => (dispatch, getState) => {
  dispatch({ type: CREATE_ACCOUNT_FETCH });
  client.accounts.createAccount(data)
    .then((res) => {
      dispatch({ type: CREATE_ACCOUNT_DONE, data: res.data });
      if (getState().accounts.create.initialGroups.length > 0) {
        dispatch({ type: ADD_ACCOUNT_TO_GROUP_FETCH });
        const initialGroups = getState().accounts.create.initialGroups;
        initialGroups.map((group) => {
          client.groups.addAccountToGroup(group.groupId, res.data.id)
            .then((res) => { dispatch({ type: ADD_ACCOUNT_TO_GROUP_DONE, data: res.data }); })
            .catch(err => dispatch({ type: ADD_ACCOUNT_TO_GROUP_FAIL, error: err }));
        });
      }
      browserHistory.push(`/app/redeam/users/${res.data.id}`);
      dispatch({ type: ACCOUNT_SET_INITIAL_GROUPS, groups: [] });
    })
    .catch(err => dispatch({ type: CREATE_ACCOUNT_FAIL, error: err }));
};

export const listAccounts = params => (dispatch) => {
  dispatch({ type: LIST_ACCOUNTS_FETCH });
  client.accounts.listAccounts(params)
    .then(res => dispatch({ type: LIST_ACCOUNTS_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_ACCOUNTS_FAIL, error: err }));
};

export const readAccount = id => (dispatch) => {
  dispatch({ type: READ_ACCOUNT_FETCH });
  client.accounts.readAccount(id)
    .then(res => dispatch({ type: READ_ACCOUNT_DONE, data: res.data }))
    .catch(err => dispatch({ type: READ_ACCOUNT_FAIL, error: err }));
};

export const updateAccount = (id, data) => (dispatch) => {
  dispatch({ type: UPDATE_ACCOUNT_FETCH });
  client.accounts.updateAccount(id, data)
    .then((res) => {
      dispatch({ type: UPDATE_ACCOUNT_DONE, data: res.data });
      NotificationManager.success('User successfully updated');
      browserHistory.push('/app/redeam/users');
    })
    .catch(err => dispatch({ type: UPDATE_ACCOUNT_FAIL, error: err }));
};
export const deleteAccount = id => (dispatch) => {
  dispatch({ type: DELETE_ACCOUNT_FETCH });
  client.accounts.deleteAccount(id)
    .then((res) => {
      dispatch({ type: DELETE_ACCOUNT_DONE, data: res.data });
      NotificationManager.success('User deleted successfully');
      browserHistory.push('/app/redeam/users');
    })
    .catch(err => dispatch({ type: DELETE_ACCOUNT_FAIL, error: err }));
};
export const listAccountPermissions = id => (dispatch) => {
  dispatch({ type: LIST_ACCOUNT_PERMISSIONS_FETCH });
  client.accounts.listAccountPermissions(id)
    .then(res => dispatch({ type: LIST_ACCOUNT_PERMISSIONS_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_ACCOUNT_PERMISSIONS_FAIL, error: err }));
};
export const updateAccountPermissions = (id, data) => (dispatch) => {
  dispatch({ type: UPDATE_ACCOUNT_PERMISSIONS_FETCH });
  client.accounts.updateAccountPermissions(id, data)
    .then(res => dispatch({ type: UPDATE_ACCOUNT_PERMISSIONS_DONE, data: res.data }))
    .catch(err => dispatch({ type: UPDATE_ACCOUNT_PERMISSIONS_FAIL, error: err }));
};
export const listAccountGroups = id => (dispatch) => {
  dispatch({ type: LIST_ACCOUNT_GROUPS_FETCH });
  client.accounts.listAccountGroups(id)
    .then(res => dispatch({ type: LIST_ACCOUNT_GROUPS_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_ACCOUNT_GROUPS_FAIL, error: err }));
};
export const updateAccountPassword = (id, data) => (dispatch) => {
  dispatch({ type: ACCOUNT_UPDATE_PASSWORD_FETCH });
  return client.accounts.updateAccountPassword(id, data)
    .then(res => dispatch({ type: ACCOUNT_UPDATE_PASSWORD_DONE, data: res.data }))
    .catch(err => dispatch({ type: ACCOUNT_UPDATE_PASSWORD_FAIL, error: err }));
};
export const setAccountInitialGroups = groups => (dispatch) => {
  dispatch({
    type: ACCOUNT_SET_INITIAL_GROUPS,
    groups,
  });
};

export const createAccountWithGroups = ({ roles, ...accountData }) => (dispatch) => {
  dispatch({ type: CREATE_ACCOUNT_FETCH });

  client.accounts.createAccount(accountData)
    .then((res) => {
      const userId = res.data.id;
      const addGroups = roles
        .filter(g => !!g)
        .map(g => client.groups.addAccountToGroup(g, userId));

      Promise.all(addGroups)
        .then((rolesData) => {
          rolesData.forEach(g => dispatch({ type: ADD_ACCOUNT_TO_GROUP_DONE, data: g.data }));
          dispatch({ type: CREATE_ACCOUNT_DONE, data: res.data });
          dispatch(readAccount(userId));
          dispatch(listAccountGroups(userId));

          NotificationManager.success('Account created successfully');
          browserHistory.push(`/app/redeam/users/${userId}`);
        })
        .catch(() => {
          NotificationManager.error('Error occurred. Roles have not been added to user.');
        });
    })
    .catch(() => {
      NotificationManager.error('Error occurred. User has not been created.');
    });
};
