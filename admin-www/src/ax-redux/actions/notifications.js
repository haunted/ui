export const NOTIFICATION_SHOW = 'NOTIFICATION_SHOW';
export const NOTIFICATION_HIDE = 'NOTIFICATION_HIDE';

export const showNotification = (message, duration = 3000) => ({
  type: NOTIFICATION_SHOW,
  message,
  duration,
});


export const hideNotification = () => ({
  type: NOTIFICATION_HIDE,
});
