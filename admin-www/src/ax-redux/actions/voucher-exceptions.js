// Voucher Exceptions Report Redux
// this is for the supplier version of the voucher-exceptions report.
// this is NOT for the voucher manage business interface.

import client  from '../../ax-client'

import {
  REPORTS_VOUCHER_EXCEPTIONS_FETCH,
  REPORTS_VOUCHER_EXCEPTIONS_FETCH_FAIL,
  REPORTS_VOUCHER_EXCEPTIONS_FETCH_DONE,
  REPORTS_VOUCHER_EXCEPTIONS_SET_START_DATE,
  REPORTS_VOUCHER_EXCEPTIONS_SET_END_DATE,
  ACTOUREX_TECHNICAL_STAFF
} from '../constants'

// Arrivals

export const setStartDate = (date) => ({
  type: REPORTS_VOUCHER_EXCEPTIONS_SET_START_DATE,
  date
})

export const setEndDate = (date) => ({
  type: REPORTS_VOUCHER_EXCEPTIONS_SET_END_DATE,
  date
})

export const fetch = function(params = {}) {
  return (dispatch, getState) => {
    let state = getState();
    dispatch({ type: REPORTS_VOUCHER_EXCEPTIONS_FETCH })
    if (state.app.role != ACTOUREX_TECHNICAL_STAFF && 
        !!state.app.user && state.app.user.account.orgCode != "") {
      params.supplierCode = getState().app.user.account.orgCode
    }
    client.exceptions.listExceptions(params)
      .then(res => dispatch(fetchDone(res.data)))
      .catch(err => dispatch(fetchFail(err)))
  }
}

export const fetchFail = (err) => ({
  type: REPORTS_VOUCHER_EXCEPTIONS_FETCH_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const fetchDone = (tickets) => ({
  type: REPORTS_VOUCHER_EXCEPTIONS_FETCH_DONE,
  data: tickets,
  receivedAt: Date.now(),
})

function exportParams(getState, params) {
  let state = getState()
  if (state.app.role != ACTOUREX_TECHNICAL_STAFF &&
     !!state.app.user && state.app.user.account.orgCode != ""
  ) {
    params.supplierCode = getState().app.user.account.orgCode
  }
  return params
}

export const exportCSV = (params = {}) => {
  return (dispatch, getState) => {
    client.exceptions.exceptionsOpenCSV(exportParams(getState, params))
  }
}

export const exportXLSX = (params = {}) => {
  return (dispatch, getState) => {
    client.exceptions.exceptionsOpenXLSX(exportParams(getState, params))
  }
}
