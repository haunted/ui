import client from '../../ax-client';
import { showNotification } from './notifications';
import {
  CONTRACT_CREATE_FETCH,
  CONTRACT_CREATE_DONE,
  CONTRACT_CREATE_FAIL,
  CONTRACTS_LIST_FETCH,
  CONTRACTS_LIST_DONE,
  CONTRACTS_LIST_FAIL,
  CONTRACT_READ_FETCH,
  CONTRACT_READ_DONE,
  CONTRACT_READ_FAIL,
  CONTRACT_UPDATE_FETCH,
  CONTRACT_UPDATE_DONE,
  CONTRACT_UPDATE_FAIL,
  CONTRACT_DISABLE_FETCH,
  CONTRACT_DISABLE_DONE,
  CONTRACT_DISABLE_FAIL,
} from '../constants';

export const listContracts = supplierId => (dispatch) => {
  dispatch({ type: CONTRACTS_LIST_FETCH });
  client.suppliers.listContracts(supplierId)
    .then(res => dispatch({ type: CONTRACTS_LIST_DONE, data: res.data }))
    .catch(err => dispatch({ type: CONTRACTS_LIST_FAIL, error: err }));
};

export const createContract = (supplierId, data) => (dispatch) => {
  dispatch({ type: CONTRACT_CREATE_FETCH });
  client.suppliers.createContract(supplierId, data)
    .then((res) => {
      dispatch({ type: CONTRACT_CREATE_DONE, data: res.data });
      dispatch(showNotification('Contract created successfully'));
      dispatch(listContracts(supplierId));
    })
    .catch((err) => {
      dispatch({ type: CONTRACT_CREATE_FAIL, error: err });
      dispatch(showNotification('Could not create contract'));
    });
};

export const readContract = (supplierId, id) => (dispatch) => {
  dispatch({ type: CONTRACT_READ_FETCH });
  client.suppliers.readContract(supplierId, id)
    .then(res => dispatch({ type: CONTRACT_READ_DONE, data: res.data }))
    .catch(err => dispatch({ type: CONTRACT_READ_FAIL, error: err }));
};

export const updateContract = (supplierId, id, data) => (dispatch) => {
  dispatch({ type: CONTRACT_UPDATE_FETCH });
  client.suppliers.updateContract(supplierId, id, data)
    .then((res) => {
      dispatch({ type: CONTRACT_UPDATE_DONE, data: res.data });
      dispatch(showNotification('Contract updated successfully'));
      dispatch(listContracts(supplierId));
    })
    .catch((err) => {
      dispatch({ type: CONTRACT_UPDATE_FAIL, error: err });
      dispatch(showNotification('Could not update contract'));
    });
};
export const disableContract = (supplierId, id) => (dispatch) => {
  dispatch({ type: CONTRACT_DISABLE_FETCH });
  client.suppliers.disableContract(supplierId, id)
    .then((res) => {
      dispatch({ type: CONTRACT_DISABLE_DONE, data: res.data });
    })
    .catch(err => dispatch({ type: CONTRACT_DISABLE_FAIL, error: err }));
};
