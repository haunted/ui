import client from '../../ax-client'
import {
    FAILED_ORDERS_FETCH,
    FAILED_ORDERS_FETCH_DONE ,
    FAILED_ORDERS_FETCH_FAIL,
    FAILED_ORDERS_DETAIL_FETCH,
    FAILED_ORDERS_DETAIL_FETCH_DONE ,
    FAILED_ORDERS_DETAIL_FETCH_FAIL,
    FAILED_ORDERS_REPROCESS_FETCH,
    FAILED_ORDERS_REPROCESS_DONE,
    FAILED_ORDERS_REPROCESS_FAIL
} from '../constants'

export const listFailedOrders = function ( params ) {
    return (dispatch, getState) => {
        dispatch({ type: FAILED_ORDERS_FETCH }) 
        client.failedOrders.listFailedOrders( params )
            .then(res => {
                dispatch( listFailedOrdersDone( res.data ) )
            })
            .catch(err => dispatch(listFailedOrdersFail(err)))
    }
}

export const listFailedOrdersDone = (data) => ({
    type: FAILED_ORDERS_FETCH_DONE,
    data: data,
})

export const listFailedOrdersFail = (err) => ({
    type: FAILED_ORDERS_FETCH_FAIL,
    error: err,
})

export const listFailedOrderDetails = function ( params ) {
    return (dispatch, getState) => {
        dispatch({ type: FAILED_ORDERS_DETAIL_FETCH }) 
        client.failedOrders.listFailedOrderDetails( params )
            .then(res => {
                dispatch( listFailedOrderDetailsDone( res.data ) )
            })
            .catch(err => dispatch(listFailedOrderDetailsFail(err)))
    }
}

export const listFailedOrderDetailsDone = (data) => ({
    type: FAILED_ORDERS_DETAIL_FETCH_DONE,
    data: data,
})

export const listFailedOrderDetailsFail = (err) => ({
    type: FAILED_ORDERS_DETAIL_FETCH_FAIL,
    error: err,
})


export const reprocessFailedOrders = function ( params ) {
    return (dispatch, getState) => {
        dispatch({ type: FAILED_ORDERS_REPROCESS_FETCH }) 
        client.failedOrders.reprocessFailedOrders( params )
            .then(res => {
                dispatch( reprocessFailedOrdersDone( res.data ) )
            })
            .catch(err => dispatch(reprocessFailedOrdersFail(err)))
    }
}

export const reprocessFailedOrdersDone = (data) => ({
    type: FAILED_ORDERS_REPROCESS_DONE,
    data: data,
})

export const reprocessFailedOrdersFail = (err) => ({
    type: FAILED_ORDERS_REPROCESS_FAIL,
    error: err,
})



