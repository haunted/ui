import { browserHistory } from 'react-router';
import client from '../../ax-client';
import {
  LIST_RESELLERS_FETCH,
  LIST_RESELLERS_DONE,
  LIST_RESELLERS_FAIL,
  LIST_SUPPLIER_RESELLERS_FETCH,
  LIST_SUPPLIER_RESELLERS_DONE,
  LIST_SUPPLIER_RESELLERS_FAIL,
  GET_RESELLER_FETCH,
  GET_RESELLER_DONE,
  GET_RESELLER_FAIL,
  CREATE_RESELLER_FETCH,
  CREATE_RESELLER_DONE,
  CREATE_RESELLER_FAIL,
  UPDATE_RESELLER_FETCH,
  UPDATE_RESELLER_DONE,
  UPDATE_RESELLER_FAIL,
  DISABLE_RESELLER_FETCH,
  DISABLE_RESELLER_DONE,
  DISABLE_RESELLER_FAIL,
  RESELLER_CATALOG_CREATE_FETCH,
  RESELLER_CATALOG_CREATE_DONE,
  RESELLER_CATALOG_CREATE_FAIL,
  RESELLER_CATALOG_UPDATE_FETCH,
  RESELLER_CATALOG_UPDATE_DONE,
  RESELLER_CATALOG_UPDATE_FAIL,
  RESELLER_CATALOGS_LIST_FETCH,
  RESELLER_CATALOGS_LIST_DONE,
  RESELLER_CATALOGS_LIST_FAIL,
  RESELLER_CATALOG_FETCH,
  RESELLER_CATALOG_FETCH_DONE,
  RESELLER_CATALOG_FETCH_FAIL,
  RESELLER_CATALOG_DISABLE_FETCH,
  RESELLER_CATALOG_DISABLE_DONE,
  RESELLER_CATALOG_DISABLE_FAIL,
  RESELLER_CATALOG_ITEM_CREATE_FETCH,
  RESELLER_CATALOG_ITEM_CREATE_DONE,
  RESELLER_CATALOG_ITEM_CREATE_FAIL,
  RESELLER_CATALOG_ITEM_UPDATE_FETCH,
  RESELLER_CATALOG_ITEM_UPDATE_DONE,
  RESELLER_CATALOG_ITEM_UPDATE_FAIL,
  RESELLER_CATALOG_ITEMS_LIST_FETCH,
  RESELLER_CATALOG_ITEMS_LIST_DONE,
  RESELLER_CATALOG_ITEMS_LIST_FAIL,
  RESELLER_CATALOG_ITEM_FETCH,
  RESELLER_CATALOG_ITEM_FETCH_DONE,
  RESELLER_CATALOG_ITEM_FETCH_FAIL,
  RESELLER_CATALOG_ITEM_DISABLE_FETCH,
  RESELLER_CATALOG_ITEM_DISABLE_DONE,
  RESELLER_CATALOG_ITEM_DISABLE_FAIL,
  FILTER_SET_RESELLER_CATALOG_ID,
} from '../constants';

const lookUpDefaultResellerCatalog = (dispatch, resellerCatalogs, noDefault) => {
  let foundDefault = false;

  if (!resellerCatalogs || typeof resellerCatalogs.map !== 'function') {
    return;
  }

  resellerCatalogs.forEach((catalog) => {
    if ((catalog.typeOf === 'TYPE_RESELLER' &&
        (catalog.name === 'Default' || noDefault)) ||
      resellerCatalogs.length === 1
    ) {
      if (catalog.name === 'Default') {
        foundDefault = true;
      }

      dispatch({
        type: FILTER_SET_RESELLER_CATALOG_ID,
        data: catalog.id,
      });
    }
  });

  if (foundDefault === false && !noDefault) {
    lookUpDefaultResellerCatalog(dispatch, resellerCatalogs, true);
  }
};

export const listResellers = params => (dispatch) => {
  dispatch({ type: LIST_RESELLERS_FETCH, params });
  client.resellers.listResellers(params)
    .then(res => dispatch({ type: LIST_RESELLERS_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_RESELLERS_FAIL, error: err }));
};

export const listSupplierResellers = (supplierId, params) => (dispatch) => {
  dispatch({ type: LIST_SUPPLIER_RESELLERS_FETCH, supplierId, params });
  client.resellers.listSupplierResellers(supplierId)
    .then(res => dispatch({ type: LIST_SUPPLIER_RESELLERS_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_SUPPLIER_RESELLERS_FAIL, error: err }));
};

export const getReseller = id => (dispatch) => {
  dispatch({ type: GET_RESELLER_FETCH, id });
  client.resellers.getReseller(id)
    .then(res => dispatch({ type: GET_RESELLER_DONE, data: res.data }))
    .catch(err => dispatch({ type: GET_RESELLER_FAIL, error: err }));
};

export const createReseller = data => (dispatch) => {
  dispatch({ type: CREATE_RESELLER_FETCH });
  client.resellers.createReseller(data)
    .then((res) => {
      dispatch({ type: CREATE_RESELLER_DONE, data: res.data });
      browserHistory.push(`/app/redeam/resellers/${res.data.reseller.reseller.id}`);
    })
    .catch(err => dispatch({ type: CREATE_RESELLER_FAIL, error: err }));
};

export const updateReseller = (id, data) => (dispatch) => {
  dispatch({ type: UPDATE_RESELLER_FETCH });
  client.resellers.updateReseller(id, data)
    .then((res) => {
      dispatch({ type: UPDATE_RESELLER_DONE, data: res.data });
      browserHistory.push('/app/redeam/resellers');
    })
    .catch(err => dispatch({ type: UPDATE_RESELLER_FAIL, error: err }));
};

export const disableReseller = (id, version) => (dispatch) => {
  dispatch({ type: DISABLE_RESELLER_FETCH });
  client.resellers.disableReseller(id, version)
    .then((res) => {
      dispatch({ type: DISABLE_RESELLER_DONE, data: res.data });
      browserHistory.push('/app/redeam/resellers');
    })
    .catch(err => dispatch({ type: DISABLE_RESELLER_FAIL, error: err }));
};

export const listResellerCatalogs = (resellerId) => {
  if (!resellerId || resellerId === -1) {
    return { type: RESELLER_CATALOGS_LIST_FAIL, error: { message: 'resellerId missing' } };
  }

  return (dispatch) => {
    dispatch({ type: RESELLER_CATALOGS_LIST_FETCH, params: resellerId });
    client.resellers.listResellerCatalogs(resellerId)
      .then((res) => {
        dispatch({ type: RESELLER_CATALOGS_LIST_DONE, data: res.data });
        lookUpDefaultResellerCatalog(dispatch, res.data.catalogs, false);
      })
      .catch((err) => {
        dispatch({ type: RESELLER_CATALOGS_LIST_FAIL, error: err });
      });
  };
};

export const getResellerCatalog = (resellerId, id) => (dispatch) => {
  dispatch({ type: RESELLER_CATALOG_FETCH });
  client.resellers.getResellerCatalog(resellerId, id)
    .then(res => dispatch({ type: RESELLER_CATALOG_FETCH_DONE, data: res.data }))
    .catch(err => dispatch({ type: RESELLER_CATALOG_FETCH_FAIL, error: err }));
};

export const createResellerCatalog = (resellerId, data) => (dispatch) => {
  dispatch({ type: RESELLER_CATALOG_CREATE_FETCH });
  client.resellers.createResellerCatalog(resellerId, data)
    .then((res) => {
      dispatch({ type: RESELLER_CATALOG_CREATE_DONE, data: res.data });
    })
    .catch(err => dispatch({ type: RESELLER_CATALOG_CREATE_FAIL, error: err }));
};

export const updateResellerCatalog = (resellerId, id, data) => (dispatch) => {
  dispatch({ type: RESELLER_CATALOG_UPDATE_FETCH });
  client.resellers.updateResellerCatalog(resellerId, id, data)
    .then((res) => {
      dispatch({ type: RESELLER_CATALOG_UPDATE_DONE, data: res.data });
    })
    .catch(err => dispatch({ type: RESELLER_CATALOG_UPDATE_FAIL, error: err }));
};

export const disableResellerCatalog = (resellerId, id, version) => (dispatch) => {
  dispatch({ type: RESELLER_CATALOG_DISABLE_FETCH });
  client.resellers.disableResellerCatalog(resellerId, id, version)
    .then((res) => {
      dispatch({ type: RESELLER_CATALOG_DISABLE_DONE, data: res.data });
    })
    .catch(err => dispatch({ type: RESELLER_CATALOG_DISABLE_FAIL, error: err }));
};

export const listResellerCatalogItems = (resellerId, catalogId, params) => (dispatch) => {
  dispatch({
    type: RESELLER_CATALOG_ITEMS_LIST_FETCH, resellerId, catalogId, params,
  });
  client.resellers.listResellerCatalogItems(resellerId, catalogId, params)
    .then(res => dispatch({ type: RESELLER_CATALOG_ITEMS_LIST_DONE, data: res.data }))
    .catch(err => dispatch({ type: RESELLER_CATALOG_ITEMS_LIST_FAIL, error: err }));
};

export const getResellerCatalogItem = (resellerId, catalogId, id) => (dispatch) => {
  dispatch({ type: RESELLER_CATALOG_ITEM_FETCH });
  client.resellers.getResellerCatalogItem(resellerId, catalogId, id)
    .then(res => dispatch({ type: RESELLER_CATALOG_ITEM_FETCH_DONE, data: res.data }))
    .catch(err => dispatch({ type: RESELLER_CATALOG_ITEM_FETCH_FAIL, error: err }));
};

export const createResellerCatalogItem = (resellerId, catalogId, data) => (dispatch) => {
  dispatch({ type: RESELLER_CATALOG_ITEM_CREATE_FETCH });
  client.resellers.createResellerCatalogItem(resellerId, catalogId, data)
    .then((res) => {
      dispatch({ type: RESELLER_CATALOG_ITEM_CREATE_DONE, data: res.data });
      browserHistory.push('/app/manage-business/products');
    })
    .catch(err => dispatch({ type: RESELLER_CATALOG_ITEM_CREATE_FAIL, error: err }));
};

export const updateResellerCatalogItem = (resellerId, catalogId, id, data) => (dispatch) => {
  dispatch({ type: RESELLER_CATALOG_ITEM_UPDATE_FETCH });
  client.resellers.updateResellerCatalogItem(resellerId, catalogId, id, data)
    .then((res) => {
      dispatch({ type: RESELLER_CATALOG_ITEM_UPDATE_DONE, data: res.data });
    })
    .catch(err => dispatch({ type: RESELLER_CATALOG_ITEM_UPDATE_FAIL, error: err }));
};

export const disableResellerCatalogItem = (resellerId, catalogId, id, version) => (dispatch) => {
  dispatch({ type: RESELLER_CATALOG_ITEM_DISABLE_FETCH });
  client.resellers.disableResellerCatalogItem(resellerId, catalogId, id, version)
    .then((res) => {
      dispatch({ type: RESELLER_CATALOG_ITEM_DISABLE_DONE, data: res.data });
      browserHistory.push('/app/manage-business/products');
    })
    .catch(err => dispatch({ type: RESELLER_CATALOG_ITEM_DISABLE_FAIL, error: err }));
};
