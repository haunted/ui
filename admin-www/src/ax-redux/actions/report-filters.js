import client from '../../ax-client'
import {
  SET_REPORT_FILTERS,
  FILTER_SET_SUPPLIER_CODE,
  FILTER_SET_SUPPLIER_CODES,
  FILTER_SET_RESELLER_CODE,
  FILTER_SET_PASS_ISSUER_CODE,
  FILTER_SET_SUPPLIER_ID,
  FILTER_SET_RESELLER_ID,
  FILTER_SET_PASS_ISSUER_ID,
  FILTER_SET_SUPPLIER_CATALOG_ID,
  FILTER_SET_RESELLER_CATALOG_ID,
  FILTER_SET_PASS_ISSUER_CATALOG_ID,
  FILTER_SET_START_DATE,
  FILTER_SET_END_DATE,
  FILTER_SET_DAY,
  FILTER_SET_OPERATOR_ID,
  FILTER_SET_SORT,
  FILTER_SET_OFFSET,
  FILTER_REFRESH,
} from '../constants'

export const setSupplierCode = data => ({
  type: FILTER_SET_SUPPLIER_CODE,
  data
})

export const setSupplierCodes = data => ({
  type: FILTER_SET_SUPPLIER_CODES,
  data
})

export const setResellerCode = data => ({
  type: FILTER_SET_RESELLER_CODE,
  data
})

export const setpassIssuerCode = data => ({
  type: FILTER_SET_PASS_ISSUER_CODE,
  data
})

export const setSupplierCatalogId = data => ({
  type: FILTER_SET_SUPPLIER_CATALOG_ID,
  data
})

export const setResellerCatalogId = data  => ({
  type: FILTER_SET_RESELLER_CATALOG_ID,
  data
})

export const setPassIssuerCatalogId = data => ({
  type: FILTER_SET_PASS_ISSUER_CATALOG_ID,
  data
})

export const setAdminSupplierId = data => ({
  type: FILTER_SET_SUPPLIER_ID,
  data
})

export const setAdminResellerId = data => ({
  type: FILTER_SET_RESELLER_ID,
  data
})

export const setadminPassIssuerId = data => ({
  type: FILTER_SET_PASS_ISSUER_ID,
  data
})

export const setStartDate = data => ({
  type: FILTER_SET_START_DATE,
  data
})

export const setEndDate = data => ({
  type: FILTER_SET_END_DATE,
  data
})

export const setDay = data => ({
  type: FILTER_SET_DAY,
  data
})

export const setOperatorId = data => ({
  type: FILTER_SET_OPERATOR_ID,
  data
})

export const setSort = data => ({
  type: FILTER_SET_SORT,
  data
})

export const nextPage = () => {
  return (dispatch, getState) => {
    let { offset = 0, limit = 100 } = getState().reportFilters
    offset += limit
    dispatch({
      type: FILTER_SET_OFFSET,
      data: offset,
    })
  }
}

export const prevPage = () => {
  return (dispatch, getState) => {
    let { offset = 0, limit = 100 } = getState().reportFilters
    offset = Math.max(0, offset - limit)
    dispatch({
      type: FILTER_SET_OFFSET,
      data: offset,
    })
  }  
}

export const refresh = () => ({
  type: FILTER_REFRESH,
})

export const setReportFilters = ({ 
  start,
  end,
  limit,
  offset,
  sort,
  supplierCode,
  supplierCodes,
  resellerCode,
  operatorId,
  pathname,
  startOfDay,
}) => ({
  type: SET_REPORT_FILTERS,
  start,
  end,
  startOfDay,
  limit,
  offset,
  sort,
  supplierCode,
  supplierCodes,
  resellerCode,
  operatorId,
  pathname,
})

// // not an action, but a helper to turn filters into
// // report initial state getter
// export const reportFilters = state => ({
//   start: state.reportFilters.startDate,
//   end: state.reportFilters.endDate,
//   supplierCode: state.reportFilters.supplierCode,
//   resellerCode: state.reportFilters.resellerCode,
//   limit: 100,
//   offset: 0,
//   sort: ""
// })



// contextFilters is used in the constructor of a report component
// this allows for common filters to be used in every report
// It determines the starting filters based on redux state, query params, and path params
// some filters to carry across reports when navigating, such as supplierCode and start-end dates
// other filters get reset every time, such as limit, offset, sort, operatorId
export const contextFilters = (params = {}, location) => {
  return (dispatch, getState) => {    
    let state = getState()
    let { query={}, pathname } = location    
    let filters = state.reportFilters
    let { startDate, endDate, startOfDay, endOfDay } = filters
    let limit = 100
    let offset = 0
    
    let {     
      sort = "",       
      operatorId = "",
      supplierCode = filters.supplierCode,
      supplierCodes = filters.supplierCodes,
      resellerCode = filters.resellerCode,
    } = query
    if (query.start) {
      startDate = new Date(query.start * 1000)
    }
    if (query.end) {
      endDate = new Date(query.end * 1000)
    }
    if (query.limit) {
      limit = parseInt(query.limit)
    }
    if (query.offset) {
      offset = parseInt(query.offset)
    }    
    if (query.day) {
      startOfDay = new Date(query.day * 1000)
    }

    // path params take precidence over query params, if available
    if (params.supplierCode) {
      supplierCode = params.supplierCode
    }
    if (params.supplierCodes) {
      supplierCodes = params.supplierCodes.split(',')
    }
    if (params.resellerCode) {
      resellerCode = params.resellerCode
    }
    if (params.operatorId) {
      operatorId = params.operatorId
    }

    
    // populate reseller/supplier filters from orgCode if empty (also.. no more 'actourex' being in there..)
    
    let role = state.app.role !== false ? state.app.role : ""
    if (!!state.app.user && role.indexOf("SUPPLIER") > -1) {
      supplierCode = state.app.user.account.orgCode        
    }
    if (!!state.app.user && role.indexOf("RESELLER") > -1) {
      resellerCode = state.app.user.account.orgCode        
    }
    
    if (pathname != filters.pathname
      || startDate != filters.startDate 
      || endDate != filters.endDate 
      || startOfDay != filters.startOfDay
      || limit != filters.limit 
      || offset != filters.offset
      || sort != filters.sort
      || supplierCode != filters.supplierCode
      || params.supplierCodes != filters.supplierCodes.join(',')
      || resellerCode != filters.resellerCode) {
        dispatch(setReportFilters({
          start: startDate, 
          end: endDate, 
          startOfDay, // arrivals to not override other date range filters when opened
          limit, 
          offset, 
          sort, 
          supplierCode, 
          supplierCodes, 
          resellerCode,
          operatorId,
          pathname,
        }))
    }
    // if (operatorId != "") {

    // }
  }
}
