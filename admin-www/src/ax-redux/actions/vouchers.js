import client from '../../ax-client'
import {
    VOUCHER_FETCH,
    VOUCHER_FETCH_DONE,
    VOUCHER_FETCH_FAIL,
    VOUCHERS_FETCH,
    VOUCHERS_FETCH_DONE,
    VOUCHERS_FETCH_FAIL,
    VOUCHER_EXCEPTIONS_FETCH,
    VOUCHER_EXCEPTIONS_FETCH_DONE,
    VOUCHER_EXCEPTIONS_FETCH_FAIL,
    VOUCHER_UPDATE_FETCH,
    VOUCHER_GET_IMAGE_FETCH,
    VOUCHER_GET_IMAGE_DONE,
    VOUCHER_GET_IMAGE_FAIL,
    VOUCHER_UPDATE_DONE,
    VOUCHER_UPDATE_FAIL,
    VOUCHER_REJECT_FETCH,
    VOUCHER_REJECT_DONE,
    VOUCHER_REJECT_FAIL,
    VOUCHER_CORRECT_FETCH,
    VOUCHER_CORRECT_DONE,
    VOUCHER_CORRECT_FAIL,
    VOUCHER_VOID_FETCH,
    VOUCHER_VOID_DONE,
    VOUCHER_VOID_FAIL,
    VOUCHER_CREATE_REDEEMED_FETCH,
    VOUCHER_CREATE_REDEEMED_DONE,
    VOUCHER_CREATE_REDEEMED_FAIL,
    VOUCHER_MERGE_FETCH,
    VOUCHER_MERGE_DONE,
    VOUCHER_MERGE_FAIL,
    ACTOUREX_TECHNICAL_STAFF,
    ACTOUREX_BUSINESS_STAFF
} from '../constants'

import { browserHistory } from 'react-router';

export const getVoucher = (id) => {
  return dispatch => {
    dispatch({ type: VOUCHER_FETCH })
    client.vouchers.getVoucher(id)
      .then(res => dispatch({ type: VOUCHER_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_FETCH_FAIL, error: err }))
  }
}

export const getVouchers = (params) => {
  return dispatch => {
    dispatch({ type: VOUCHERS_FETCH })
    client.vouchers.getVouchers(params)
      .then(res => dispatch({ type: VOUCHERS_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHERS_FETCH_FAIL, error: err }))
  }
}

export const getVoucherExceptions = (params) => {
  return dispatch => {
    dispatch({ type: VOUCHER_EXCEPTIONS_FETCH })
    client.vouchers.getVoucherExceptions(params)
      .then(res => dispatch({ type: VOUCHER_EXCEPTIONS_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_EXCEPTIONS_FETCH_FAIL, error: err }))

  }
}

export const getVoucherImage = (params) => {
  return dispatch => {
    dispatch({ type: VOUCHER_GET_IMAGE_FETCH })
    client.vouchers.getVoucherImage(params)
      .then(res => dispatch({ type: VOUCHER_GET_IMAGE_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_GET_IMAGE_FAIL, error: err }))

  }
}

export const updateVoucher = (id, params) => {
  return dispatch => {
    dispatch({ type: VOUCHER_UPDATE_FETCH })
    client.vouchers.updateVoucher(params)
      .then(res => dispatch({ type: VOUCHER_UPDATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_UPDATE_FAIL, error: err }))
  }
}

export const rejectVoucher = (id, data) => {
  return dispatch => {
    dispatch({ type: VOUCHER_REJECT_FETCH })
    client.vouchers.rejectVoucher(id, data)
      .then(res => dispatch({ type: VOUCHER_REJECT_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_REJECT_FAIL, error: err }))
  }
}

export const correctVoucher = (id, params) => {
  return dispatch => {
    dispatch({ type: VOUCHER_CORRECT_FETCH })
    client.vouchers.correctVoucher(id, params)
      .then(res => dispatch({ type: VOUCHER_CORRECT_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_CORRECT_FAIL, error: err.data ? err.data.Error : "Error Occurred" }))
  }
}

export const voidVoucher = (id, data) => {
  return dispatch => {
    dispatch({ type: VOUCHER_VOID_FETCH })
    client.vouchers.voidVoucher(id, data)
      .then(res => dispatch({ type: VOUCHER_VOID_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_VOID_FAIL, error: err }))
  }
}

export const createRedeemedVoucher = (data) => {
  return dispatch => {
    dispatch({ type: VOUCHER_CREATE_REDEEMED_FETCH })
    client.vouchers.createRedeemedVoucher(data)
      .then(res => dispatch({ type: VOUCHER_CREATE_REDEEMED_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_CREATE_REDEEMED_FAIL, error: err }))
  }
}

export const mergeVoucher = (id, data) => {
  return dispatch => {
    dispatch({ type: VOUCHER_MERGE_FETCH })
    client.vouchers.mergeVoucher(id, data)
      .then(res => dispatch({ type: VOUCHER_MERGE_DONE, data: res.data }))
      .catch(err => dispatch({ type: VOUCHER_MERGE_FAIL, error: err }))
  }
}

function exportParams(getState, params) {
  let state = getState()
  if (state.app.role != ACTOUREX_TECHNICAL_STAFF &&
      state.app.role != ACTOUREX_BUSINESS_STAFF && state.app.user.account.orgCode != ""
  ) {
    params.supplierCode = state.app.user.account.orgCode
  }
  return params
}

export const exportVouchersCSV = (params = {}) => {
  return (dispatch, getState) => {
    client.vouchers.vouchersCSV(exportParams(getState, params))
  }
}

export const exportVouchersXLSX = (params = {}) => {
  return (dispatch, getState) => {
    client.vouchers.vouchersXLSX(exportParams(getState, params))
  }
}
export const exportVoucherExceptionsCSV = (params = {}) => {
  return (dispatch, getState) => {
    client.vouchers.voucherExceptionsCSV(exportParams(getState, params))
  }
}

export const exportVoucherExceptionsXLSX = (params = {}) => {
  return (dispatch, getState) => {
    client.vouchers.voucherExceptionsXLSX(exportParams(getState, params))
  }
}
