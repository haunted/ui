import { NotificationManager } from 'react-notifications';
import client from '../../ax-client';
import {
  LIST_SUPPLIERS_FETCH,
  LIST_SUPPLIERS_DONE,
  LIST_SUPPLIERS_FAIL,
  GET_SUPPLIER_FETCH,
  GET_SUPPLIER_DONE,
  GET_SUPPLIER_FAIL,
  CREATE_SUPPLIER_FETCH,
  CREATE_SUPPLIER_DONE,
  CREATE_SUPPLIER_FAIL,
  UPDATE_SUPPLIER_FETCH,
  UPDATE_SUPPLIER_DONE,
  UPDATE_SUPPLIER_FAIL,
  DISABLE_SUPPLIER_FETCH,
  DISABLE_SUPPLIER_DONE,
  DISABLE_SUPPLIER_FAIL,
  SUPPLIER_CATALOG_CREATE_FETCH,
  SUPPLIER_CATALOG_CREATE_DONE,
  SUPPLIER_CATALOG_CREATE_FAIL,
  SUPPLIER_CATALOG_UPDATE_FETCH,
  SUPPLIER_CATALOG_UPDATE_DONE,
  SUPPLIER_CATALOG_UPDATE_FAIL,
  SUPPLIER_CATALOGS_LIST_FETCH,
  SUPPLIER_CATALOGS_LIST_DONE,
  SUPPLIER_CATALOGS_LIST_FAIL,
  SUPPLIER_CATALOG_FETCH,
  SUPPLIER_CATALOG_FETCH_DONE,
  SUPPLIER_CATALOG_FETCH_FAIL,
  SUPPLIER_CATALOG_DISABLE_FETCH,
  SUPPLIER_CATALOG_DISABLE_DONE,
  SUPPLIER_CATALOG_DISABLE_FAIL,
  SUPPLIER_CATALOG_ITEM_CREATE_FETCH,
  SUPPLIER_CATALOG_ITEM_CREATE_DONE,
  SUPPLIER_CATALOG_ITEM_CREATE_FAIL,
  SUPPLIER_CATALOG_ITEM_UPDATE_FETCH,
  SUPPLIER_CATALOG_ITEM_UPDATE_DONE,
  SUPPLIER_CATALOG_ITEM_UPDATE_FAIL,
  SUPPLIER_CATALOG_ITEMS_LIST_FETCH,
  SUPPLIER_CATALOG_ITEMS_LIST_DONE,
  SUPPLIER_CATALOG_ITEMS_LIST_FAIL,
  SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_FETCH,
  SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_DONE,
  SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_FAIL,
  SUPPLIER_CATALOG_ITEM_FETCH,
  SUPPLIER_CATALOG_ITEM_FETCH_DONE,
  SUPPLIER_CATALOG_ITEM_FETCH_FAIL,
  SUPPLIER_CATALOG_ITEM_DISABLE_FETCH,
  SUPPLIER_CATALOG_ITEM_DISABLE_DONE,
  SUPPLIER_CATALOG_ITEM_DISABLE_FAIL,
  FILTER_SET_SUPPLIER_CATALOG_ID,
} from '../constants';
import { browserHistory } from 'react-router';

export const listSuppliers = params => (dispatch) => {
  dispatch({ type: LIST_SUPPLIERS_FETCH, params });
  client.suppliers.listSuppliers(params)
    .then(res => dispatch({ type: LIST_SUPPLIERS_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_SUPPLIERS_FAIL, error: err }));
};
export const getSupplier = id => (dispatch) => {
  dispatch({ type: GET_SUPPLIER_FETCH, id });
  client.suppliers.getSupplier(id)
    .then(res => dispatch({ type: GET_SUPPLIER_DONE, data: res.data }))
    .catch(err => dispatch({ type: GET_SUPPLIER_FAIL, error: err }));
};
export const createSupplier = data => (dispatch) => {
  dispatch({ type: CREATE_SUPPLIER_FETCH });
  client.suppliers.createSupplier(data)
    .then((res) => {
      dispatch({ type: CREATE_SUPPLIER_DONE, data: res.data });
      NotificationManager.success('Supplier created successfully');
      browserHistory.push(`/app/redeam/suppliers/${res.data.supplier.supplier.id}`);
    })
    .catch((err) => {
      dispatch({ type: CREATE_SUPPLIER_FAIL, error: err });
      NotificationManager.error(err.data.Error);
    });
};
export const updateSupplier = (id, data) => (dispatch) => {
  dispatch({ type: UPDATE_SUPPLIER_FETCH, id });
  client.suppliers.updateSupplier(id, data)
    .then((res) => {
      dispatch({ type: UPDATE_SUPPLIER_DONE, data: res.data });
      NotificationManager.success('Supplier updated successfully');
      browserHistory.push('/app/redeam/suppliers');
    })
    .catch(err => dispatch({ type: UPDATE_SUPPLIER_FAIL, error: err }));
};
export const disableSupplier = (id, version) => (dispatch) => {
  dispatch({ type: DISABLE_SUPPLIER_FETCH });
  client.suppliers.disableSupplier(id, version)
    .then((res) => {
      dispatch({ type: DISABLE_SUPPLIER_DONE, data: res.data });
      browserHistory.push('/app/redeam/suppliers');
    })
    .catch(err => dispatch({ type: DISABLE_SUPPLIER_FAIL, error: err }));
};

export const listSupplierCatalogs = (supplierId) => {
  if (!supplierId || supplierId == -1) {
    return { type: SUPPLIER_CATALOGS_LIST_FAIL, error: { message: 'supplierId missing' } };
  }

  return (dispatch, getState) => {
    dispatch({ type: SUPPLIER_CATALOGS_LIST_FETCH, params: supplierId });
    client.suppliers.listSupplierCatalogs(supplierId)

      .then((res) => {
        dispatch({ type: SUPPLIER_CATALOGS_LIST_DONE, data: res.data });
        _lookUpDefaultSupplierCatalog(dispatch, res.data.catalogs, false);
      })
      .catch((err) => {
        let state = getState(),
          supplierCode = '';

        dispatch({ type: SUPPLIER_CATALOGS_LIST_FAIL, error: err });
      });
  };
};

let _lookUpDefaultSupplierCatalog = (dispatch, supplierCatalogs, noDefault) => {
  let foundDefault = false,
    foundSomething = false;

  if (!supplierCatalogs || typeof supplierCatalogs.map !== 'function') {
    return;
  }

  supplierCatalogs.map((catalog, index) => {
    if (!foundSomething && catalog.typeOf == 'TYPE_SUPPLIER') {
      if ((catalog.name == 'Default' && !noDefault) || !!noDefault || supplierCatalogs.length == 1) {
        if (catalog.name == 'Default') {
          foundDefault = true;
        }
        foundSomething = true;

        dispatch({
          type: FILTER_SET_SUPPLIER_CATALOG_ID,
          data: catalog.id,
        });
      }
    }
  });

  if (foundDefault == false && !noDefault) {
    _lookUpDefaultSupplierCatalog(dispatch, supplierCatalogs, true);
  }
};

export const getSupplierCatalog = (supplierId, id) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_FETCH });
  client.suppliers.getSupplierCatalog(supplierId, id)
    .then(res => dispatch({ type: SUPPLIER_CATALOG_FETCH_DONE, data: res.data }))
    .catch(err => dispatch({ type: SUPPLIER_CATALOG_FETCH_FAIL, error: err }));
};
export const createSupplierCatalog = (supplierId, data) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_CREATE_FETCH });
  client.suppliers.createSupplierCatalog(supplierId, data)
    .then((res) => {
      dispatch({ type: SUPPLIER_CATALOG_CREATE_DONE, data: res.data });
    })
    .catch(err => dispatch({ type: SUPPLIER_CATALOG_CREATE_FAIL, error: err }));
};
export const updateSupplierCatalog = (supplierId, id, data) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_UPDATE_FETCH });
  client.suppliers.updateSupplierCatalog(supplierId, id, data)
    .then((res) => {
      dispatch({ type: SUPPLIER_CATALOG_UPDATE_DONE, data: res.data });
      browserHistory.push('/app/redeam/suppliers');
    })
    .catch(err => dispatch({ type: SUPPLIER_CATALOG_UPDATE_FAIL, error: err }));
};
export const disableSupplierCatalog = (supplierId, id, version) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_DISABLE_FETCH });
  client.suppliers.disableSupplierCatalog(supplierId, id, version)
    .then((res) => {
      dispatch({ type: SUPPLIER_CATALOG_DISABLE_DONE, data: res.data });
      browserHistory.push('/app/redeam/suppliers');
    })
    .catch(err => dispatch({ type: SUPPLIER_CATALOG_DISABLE_FAIL, error: err }));
};

export const listSupplierCatalogItems = (supplierId, catalogId, params) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_ITEMS_LIST_FETCH, supplierId, params });
  client.suppliers.listSupplierCatalogItems(supplierId, catalogId, params)
    .then(res => dispatch({ type: SUPPLIER_CATALOG_ITEMS_LIST_DONE, data: res.data }))
    .catch(err => dispatch({ type: SUPPLIER_CATALOG_ITEMS_LIST_FAIL, error: err }));
};
export const listSupplierResellerCatalogItems = (supplierCode, resellerCode, params) => (dispatch) => {
  dispatch({ type: SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_FETCH, supplierCode, resellerCode, params });
  client.suppliers.listSupplierResellerCatalogItems(supplierCode, resellerCode, params)
    .then(res => dispatch({ type: SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_DONE, data: res.data }))
    .catch(err => dispatch({ type: SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_FAIL, error: err }));
};

export const getSupplierCatalogItem = (supplierId, catalogId, id) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_ITEM_FETCH });
  client.suppliers.getSupplierCatalogItem(supplierId, catalogId, id)
    .then(res => dispatch({ type: SUPPLIER_CATALOG_ITEM_FETCH_DONE, data: res.data }))
    .catch(err => dispatch({ type: SUPPLIER_CATALOG_ITEM_FETCH_FAIL, error: err }));
};
export const createSupplierCatalogItem = (supplierId, catalogId, data) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_ITEM_CREATE_FETCH });
  client.suppliers.createSupplierCatalogItem(supplierId, catalogId, data)
    .then((res) => {
      dispatch({ type: SUPPLIER_CATALOG_ITEM_CREATE_DONE, data: res.data });
      NotificationManager.success('Product successfully created');
      browserHistory.push('/app/manage-business/products');
    })
    .catch((err) => {
      dispatch({ type: SUPPLIER_CATALOG_ITEM_CREATE_FAIL, error: err });
      NotificationManager.error(err.data.Error);
    });
};
export const updateSupplierCatalogItem = (supplierId, catalogId, id, data) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_ITEM_UPDATE_FETCH });
  client.suppliers.updateSupplierCatalogItem(supplierId, catalogId, id, data)
    .then((res) => {
      dispatch({ type: SUPPLIER_CATALOG_ITEM_UPDATE_DONE, data: res.data });
      NotificationManager.success('Product successfully updated');
      browserHistory.push('/app/manage-business/products');
    })
    .catch((err) => {
      dispatch({ type: SUPPLIER_CATALOG_ITEM_UPDATE_FAIL, error: err });
      NotificationManager.error(err.data.Error);
    });
};
export const disableSupplierCatalogItem = (supplierId, catalogId, id, version) => (dispatch) => {
  dispatch({ type: SUPPLIER_CATALOG_ITEM_DISABLE_FETCH });
  client.suppliers.disableSupplierCatalogItem(supplierId, catalogId, id, version)
    .then((res) => {
      dispatch({ type: SUPPLIER_CATALOG_ITEM_DISABLE_DONE, data: res.data });
      browserHistory.push('/app/manage-business/products');
    })
    .catch(err => dispatch({ type: SUPPLIER_CATALOG_ITEM_DISABLE_FAIL, error: err }));
};
