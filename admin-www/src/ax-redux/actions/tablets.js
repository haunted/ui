import client from '../../ax-client'
import {
    TABLET_FETCH,
    TABLET_FETCH_DONE,
    TABLET_FETCH_FAIL,
    TABLETS_FETCH,
    TABLETS_FETCH_DONE,
    TABLETS_FETCH_FAIL,
    TABLET_CREATE_FETCH,
    TABLET_CREATE_DONE,
    TABLET_CREATE_FAIL,
    TABLET_UPDATE_FETCH,
    TABLET_UPDATE_DONE,
    TABLET_UPDATE_FAIL,
    TABLET_DELETE_FETCH,
    TABLET_DELETE_DONE,
    TABLET_DELETE_FAIL
} from '../constants'
import { browserHistory } from 'react-router';

export const getTablets = (params) => {
  return dispatch => {
    dispatch({ type: TABLETS_FETCH })
    client.tablets.getTablets(params)
      .then(res => dispatch({ type: TABLETS_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: TABLETS_FETCH_FAIL, error: err }))

  }
}
export const getTablet = id => {
  return dispatch => {
    dispatch({ type: TABLET_FETCH })
    client.tablets.getTablet(id)
      .then(res => dispatch({ type: TEBLET_FETCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: TABLET_FETCH_FAIL, error: err }))
  }
}
export const createTablet = (params) => {
  return dispatch => {
    dispatch({ type: TABLET_CREATE_FETCH })
    client.tablets.createTablet(params)
      .then(res => dispatch({ type: TABLET_CREATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: TABLET_CREATE_FAIL, error: err }))

  }
}
export const updateTablet = (params) => {
  return dispatch => {
    dispatch({ type: TABLET_UPDATE_FETCH })
    client.tablets.updateTablet(params)
      .then(res => dispatch({ type: TABLET_UPDATE_DONE, data: res.data }))
      .catch(err => dispatch({ type: TABLET_UPDATE_FAIL, error: err }))
  }
}
export const deleteTablet = (params) => {
  return dispatch => {
    dispatch({ type: TABLET_DELETE_FETCH })
    client.tablets.deleteTablet(params)
      .then(res => dispatch({ type: TABLET_DELETE_DONE, data: res.data }))
      .catch(err => dispatch({ type: TABLET_DELETE_FAIL, error: err }))
  }
}
