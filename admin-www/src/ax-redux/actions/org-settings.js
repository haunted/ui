import client from '../../ax-client'
import {
  ORG_SETTINGS_LIST_FETCH,
  ORG_SETTINGS_LIST_DONE,
  ORG_SETTINGS_LIST_FAIL,
  ORG_SETTINGS_READ_FETCH,
  ORG_SETTINGS_READ_DONE,
  ORG_SETTINGS_READ_FAIL,
  ORG_SETTINGS_UPDATE_FETCH,
  ORG_SETTINGS_UPDATE_DONE,
  ORG_SETTINGS_UPDATE_FAIL,
  ORG_SETTINGS_DELETE_FETCH,
  ORG_SETTINGS_DELETE_DONE,
  ORG_SETTINGS_DELETE_FAIL,
  FILTER_SET_DEFAULTS_IN_TZ,
} from '../constants'
import { browserHistory } from 'react-router';

export const listSettings = ( params ) => {

  return dispatch => {
    dispatch({ type: SETTINGSS_LIST_FETCH })
    client.orgSettings.listSettings(params)
      .then(res => dispatch({ type: SETTINGSS_LIST_DONE, data: res.data}))
      .catch(err => dispatch({ type: SETTINGSS_LIST_FAIL, error: err }))
  }

}

export const readSettings = ( orgCode, orgType ) => {

  return dispatch => {
    dispatch({ type: ORG_SETTINGS_READ_FETCH, orgCode, orgType })
    client.orgSettings.readSettings(orgCode, orgType)
      .then(res => {
        dispatch({ type: ORG_SETTINGS_READ_DONE, data: res.data })
        if (!!res.data.operationHours && !!res.data.operationHours.timezone) {
            window.TZ = res.data.operationHours.timezone        
        }
      })
      .catch(err => dispatch({ type: ORG_SETTINGS_READ_FAIL, error: err }))
      .then(() => dispatch({ type: FILTER_SET_DEFAULTS_IN_TZ }))
  }

}

export const updateSettings = ( data ) => {

  return dispatch => {
    dispatch({ type: ORG_SETTINGS_UPDATE_FETCH })
    client.orgSettings.updateSettings(data)
      .then(res => {
          dispatch({ type: ORG_SETTINGS_UPDATE_DONE, data: res.data })
          if (!!res.data.operationHours && !!res.data.operationHours.timezone) {
              window.TZ = res.data.operationHours.timezone        
          }
          //browserHistory.push("/app/manage-business");
      })
      .catch(err => dispatch({ type: ORG_SETTINGS_UPDATE_FAIL, error: err }))
      .then(() => dispatch({ type: FILTER_SET_DEFAULTS_IN_TZ }))
  }

}

export const deleteSettings = ( orgCode, orgType ) => {

  return dispatch => {
    dispatch({ type: ORG_SETTINGS_DELETE_FETCH })
    client.orgSettings.deleteSettings(orgCode, orgType)
      .then(res => {
          dispatch({ type: ORG_SETTINGS_DELETE_DONE, data: res.data })
          browserHistory.push("/app/manage-business");
      })
      .catch(err => dispatch({ type: CONTACT_DELETE_FAIL, error: err }))
  }

}
