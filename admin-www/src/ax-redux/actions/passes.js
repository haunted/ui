import client from '../../ax-client'

import {
    PASS_FETCH,
    PASS_FETCH_FAIL,
    PASS_FETCH_DONE,
    PASS_CREATE_FETCH,
    PASS_CREATE_FETCH_FAIL,
    PASS_CREATE_FETCH_DONE,
    PASS_REDEMPTIONS_FETCH,
    PASS_REDEMPTIONS_FETCH_FAIL,
    PASS_REDEMPTIONS_FETCH_DONE,
    ACTOUREX_TECHNICAL_STAFF,
    STATUS_REDEEMED
} from '../constants'

export const fetchPass = ( id, supplierCode ) => {

    return (dispatch, getState) => {

        const state = getState()

        if ( state.app.role != ACTOUREX_TECHNICAL_STAFF && state.app.user.account.orgCode != "" )
            supplierCode = state.app.user.account.orgCode

        dispatch({ type: PASS_FETCH }) 
        client.passes.getPass(id, supplierCode)
            .then(res => {
                dispatch(fetchPassDone(res.data))
            })
            .catch(err => dispatch(fetchPassFail(err)))
    }

}

export const fetchPassFail = err => ({
    type: PASS_FETCH_FAIL,
    error: err,
})

export const fetchPassDone = data => ({
    type: PASS_FETCH_DONE,
    data: data,
})

export const createPass = data => {

    return (dispatch, getState) => {

        dispatch({ type: PASS_CREATE_FETCH }) 
        client.passes.createPass(id, supplierCode)
            .then(res => {

                dispatch( createPassDone( res.data ) )

            })
            .catch(err => dispatch(createPassFail(err)))
    }

}

export const createPassDone = data => ({
    type: PASS_CREATE_FETCH_DONE,
    data: data,
})

export const createPassFail = err => ({
    type: PASS_CREATE_FETCH_FAIL,
    error: err,
})

export const getPassRedemptions = params => {

    return ( dispatch, getState ) => {

        const state = getState()

        dispatch({ type: PASS_REDEMPTIONS_FETCH }) 
        client.passes.getPassRedemptions( params )
            .then(res => {

                if ( typeof res.data == 'string' )
                    res.data = JSON.parse( res.data )

                dispatch( getPassRedemptionsDone( res.data  ) )
            
            })
            .catch(err => dispatch(getPassRedemptionsFail(err)))
    }

}

export const getPassRedemptionsDone = data => ({
    type: PASS_REDEMPTIONS_FETCH_DONE,
    data: data,
})

export const getPassRedemptionsFail = err => ({
    type: PASS_REDEMPTIONS_FETCH_FAIL,
    error: err,
})

function exportParams( getState, params ) {

  const state = getState()

  if ( state.app.role != ACTOUREX_TECHNICAL_STAFF && state.app.user.account.orgCode != "" )
    params.passIssuerCode = state.app.user.account.orgCode

  return params

}

export const exportPassRedemptionsCSV = (params = {}) => {
  return (dispatch, getState) => {
    client.passes.passRedemptionsCSV(exportParams(getState, params))
  }
}

export const exportPassRedemptionsXLSX = (params = {}) => {
  return (dispatch, getState) => {
    client.passes.passRedemptionsXLSX(exportParams(getState, params))
  }
}