import client from '../../ax-client'
import {
  MANAGEBIZ_FETCH,
  MANAGEBIZ_FETCH_FAIL,
  MANAGEBIZ_FETCH_DONE,
} from '../constants'

export const fetch = function(params = {}) {
  return (dispatch, getState) => {
    dispatch({ type: MANAGEBIZ_FETCH })
    // client.myBusiness(params)
    //   .then(res => {
    //     dispatch(manageBusinessFetchDone(res.data))
    //   })
    //   .catch(err => dispatch(manageBusinessFetchFail(err)))
  }
}

export const manageBusinessFetchFail = (err) => ({
  type: MANAGEBIZ_FETCH_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const manageBusinessFetchDone = (data) => ({
  type: MANAGEBIZ_FETCH_DONE,
  data: data,
  receivedAt: Date.now(),
})
