import client from '../../ax-client'
import {
    SEARCH_FETCH,
    SEARCH_DONE,
    SEARCH_FAIL

} from '../constants'
import { browserHistory } from 'react-router';

export const getResults = (params) => {
  return dispatch => {
    dispatch({ type: SEARCH_FETCH })
    client.search.getResults(params)
      .then(res => dispatch({ type: SEARCH_DONE, data: res.data }))
      .catch(err => dispatch({ type: SEARCH_FAIL, error: err }))

  }
}
