import client from '../../ax-client'
import {
  DASHBOARD_FETCH,
  DASHBOARD_FETCH_FAIL,
  DASHBOARD_FETCH_DONE,
  DASHBOARD_TOGGLE_MENU,
  DASHBOARD_TOGGLE_ADMIN_MENU
} from '../constants'

export const dashboardFetch = function(params = {}) {
  return (dispatch, getState) => {
    let role = params.role
    params.role = undefined //won't need that in the url
    var supplierId = getState().app.user.account.orgCode
    if (!!supplierId) {
      params.supplierId = supplierId
    }
    dispatch({ type: DASHBOARD_FETCH, role: role})
    client.dashboard.dashboard(params, role)
      .then(res => {
        dispatch(dashboardFetchDone(res.data))
      })
      .catch(err => dispatch(dashboardFetchFail(err)))
  }
}

export const dashboardFetchFail = (err) => ({
  type: DASHBOARD_FETCH_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const dashboardFetchDone = (data) => ({
  type: DASHBOARD_FETCH_DONE,
  data: data,
  receivedAt: Date.now(),
})

export const dashboardToggleMenu = (data) => ({
        type: DASHBOARD_TOGGLE_MENU
})

export const dashboardToggleAdminMenu = (data) => ({
        type: DASHBOARD_TOGGLE_ADMIN_MENU
})
