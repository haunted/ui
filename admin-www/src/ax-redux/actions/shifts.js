import client from '../../ax-client'
import {
  /* remove these three once the master shift summary endpoint is here */
  WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_FETCH,
  WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_DONE,
  WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_FAIL,
  GET_MASTER_SHIFT_REPORT_SUMMARY_FETCH,
  GET_MASTER_SHIFT_REPORT_SUMMARY_DONE,
  GET_MASTER_SHIFT_REPORT_SUMMARY_FAIL,
  GET_SHIFT_REPORT_SUMMARY_FETCH,
  GET_SHIFT_REPORT_SUMMARY_DONE,
  GET_SHIFT_REPORT_SUMMARY_FAIL,
  GET_SHIFT_REPORT_FETCH,
  GET_SHIFT_REPORT_DONE,
  GET_SHIFT_REPORT_FAIL
} from '../constants'
import { browserHistory } from 'react-router';

// this version will work once the master shift summary report endpoint is in place
// export const getMasterShiftSummaryReport = ({ supplierId, start, end, sort}) => {
//   return dispatch => {
//     dispatch({ type: GET_MASTER_SHIFT_REPORT_SUMMARY_FETCH })
//     client.shifts.getMasterShiftSummaryReport({ supplierId, start, end, sort})
//       .then(res => dispatch({ type: GET_MASTER_SHIFT_REPORT_SUMMARY_DONE, data: res.data }))
//       .catch(err => dispatch({ type: GET_MASTER_SHIFT_REPORT_SUMMARY_FAIL, error: err }))
//   }
// }

export const getMasterShiftSummaryReport = ({ supplierId, start, end, sort }) => {
  return (dispatch, getState) => {
    let state = getState()
   
    dispatch({ type: WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_FETCH })

    client.shifts.getShiftReport({      
      start: start / 1000,
      end: end / 1000,
      supplierId,
      sort
    }).then(res => {      
      dispatch({
        type: WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_DONE, 
        data: res.data
      })
      
    })
    .catch(err => dispatch({ type: WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_FAIL, error: err }))
  }
}

export const getShiftReportSummary = ({ supplierId, start, end, sort }) => {
  return dispatch => {
    dispatch({ type: GET_SHIFT_REPORT_SUMMARY_FETCH })
    client.shifts.getShiftReportSummary({ supplierId, start, end, sort })
      .then(res => dispatch({ type: GET_SHIFT_REPORT_SUMMARY_DONE, data: res.data }))
      .catch(err => dispatch({ type: GET_SHIFT_REPORT_SUMMARY_FAIL, error: err }))
  }
}

export const getShiftReport = params => {
  return dispatch => {
    dispatch({ type: GET_SHIFT_REPORT_FETCH })
    client.shifts.getShiftReport(params)
      .then(res => dispatch({ type: GET_SHIFT_REPORT_DONE, data: res.data }))
      .catch(err => dispatch({ type: GET_SHIFT_REPORT_FAIL, error: err }))
  }
}
