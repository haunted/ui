import client from '../../ax-client'
import {
    CONTACT_CREATE_FETCH,
    CONTACT_CREATE_DONE,
    CONTACT_CREATE_FAIL,
    CONTACTS_LIST_FETCH,
    CONTACTS_LIST_DONE,
    CONTACTS_LIST_FAIL,
    CONTACT_READ_FETCH,
    CONTACT_READ_DONE,
    CONTACT_READ_FAIL,
    CONTACT_UPDATE_FETCH,
    CONTACT_UPDATE_DONE,
    CONTACT_UPDATE_FAIL,
    CONTACT_DELETE_FETCH,
    CONTACT_DELETE_DONE,
    CONTACT_DELETE_FAIL
} from '../constants'
import { addContactToGroup } from './groups'
import { browserHistory } from 'react-router';

export const createContact = (supplierId, data) => {
  return (dispatch, getState) => {
    dispatch({ type: CONTACT_CREATE_FETCH })
    client.suppliers.createContact(supplierId, data)
      .then(res => {
          dispatch({ type: CONTACT_CREATE_DONE, data: res.data });
      })
      .catch(err => dispatch({ type: CONTACT_CREATE_FAIL, error: err }))
  }
}

export const listContacts = (supplierId) => {
  return dispatch => {
    dispatch({ type: CONTACTS_LIST_FETCH })
    client.suppliers.listContacts(supplierId)
      .then(res => dispatch({ type: CONTACTS_LIST_DONE, data: res.data}))
      .catch(err => dispatch({ type: CONTACTS_LIST_FAIL, error: err }))

  }
}

export const readContact = (supplierId, id) => {
  return dispatch => {
    dispatch({ type: CONTACT_READ_FETCH })
    client.suppliers.readContact(supplierId, id)
      .then(res => dispatch({ type: CONTACT_READ_DONE, data: res.data }))
      .catch(err => dispatch({ type: CONTACT_READ_FAIL, error: err }))

  }
}

export const updateContact = (supplierId, id, data) => {
  return dispatch => {
    dispatch({ type: CONTACT_UPDATE_FETCH })
    client.suppliers.updateContact(supplierId, id, data)
      .then(res => {
          dispatch({ type: CONTACT_UPDATE_DONE, data: res.data })
          browserHistory.push("/app/manage-business");
      })
      .catch(err => dispatch({ type: CONTACT_UPDATE_FAIL, error: err }))

  }
}
export const disableContract = (supplierId, id, version) => {
  return dispatch => {
    dispatch({ type: CONTACT_DELETE_FETCH })
    client.suppliers.disableContract(supplierId, id, version)
      .then(res => {
          dispatch({ type: CONTACT_DELETE_DONE, data: res.data })
          browserHistory.push("/app/manage-business");
      })
      .catch(err => dispatch({ type: CONTACT_DELETE_FAIL, error: err }))

  }
}
