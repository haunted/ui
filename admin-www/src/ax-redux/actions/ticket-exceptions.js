import client from '../../ax-client'
import {
  STATUS_OPEN,
  STATUS_CANCELLED,
  STATUS_REDEEMED
} from '../../ax-client/tickets'

import {
  EXCEPTIONS_FETCH,
  EXCEPTIONS_FETCH_FAIL,
  EXCEPTIONS_FETCH_DONE,
  EXCEPTIONS_SET_START_DATE,
  EXCEPTIONS_SET_END_DATE,
  ACTOUREX_TECHNICAL_STAFF
} from '../constants'

// Arrivals

export const setStartDate = (date) => ({
  type: EXCEPTIONS_SET_START_DATE,
  date
})

export const setEndDate = (date) => ({
  type: EXCEPTIONS_SET_END_DATE,
  date
})

export const fetch = function(params = {}) {
  return (dispatch, getState) => {
    let state = getState();
    dispatch({ type: EXCEPTIONS_FETCH })
    if (!!state.app.user.account) {
      if (state.app.role != ACTOUREX_TECHNICAL_STAFF &&
          !!state.app.user && state.app.user.account.orgCode != ""
      ) {
        params.supplierCode = getState().app.user.account.orgCode
      }
    }
    params.status = STATUS_CANCELLED
    client.tickets.tickets(params)
      .then(res => dispatch(fetchDone(res.data.tickets ? res.data.tickets : [])))
      .catch(err => dispatch(fetchFail(err)))
  }
}

export const fetchFail = (err) => ({
  type: EXCEPTIONS_FETCH_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const fetchDone = (tickets) => ({
  type: EXCEPTIONS_FETCH_DONE,
  data: tickets,
  receivedAt: Date.now(),
})

function exportParams(getState, params) {
  let state = getState()
  if (!!state.app.user) {
    if (state.app.role != ACTOUREX_TECHNICAL_STAFF && 
        !!state.app.user && state.app.user.account.orgCode != "") {
      params.supplierCode = state.app.user.account.orgCode
    }
  }
  params.status = STATUS_CANCELLED
  return params
}

export const exportCSV = (params = {}) => {
  return (dispatch, getState) => {
    client.tickets.ticketsOpenCSV(exportParams(getState, params))
  }
}

export const exportXLSX = (params = {}) => {
  return (dispatch, getState) => {
    client.tickets.ticketsOpenXLSX(exportParams(getState, params))
  }
}

import {
  EXCEPTIONS_FETCH_DETAILS,
  EXCEPTIONS_FETCH_DETAILS_FAIL,
  EXCEPTIONS_FETCH_DETAILS_DONE
} from '../constants'

export const fetchDetails = function(id) {
  return (dispatch, getState) => {
    let state = getState();
    let params = {};
    dispatch({ type: EXCEPTIONS_FETCH_DETAILS })

    if (!!state.app.user.account) {
      if (state.app.role != ACTOUREX_TECHNICAL_STAFF &&
         !!state.app.user && state.app.user.account.orgCode != "") {
        params.supplierCode = state.app.user.account.orgCode
      }
    }

    client.tickets.ticketById(id, params)
      .then(res => dispatch(fetchDetailsDone(res.data)))
      .catch(err => dispatch(fetchDetailsFail(err)))
  }
}

export const fetchDetailsFail = (err) => ({
  type: EXCEPTIONS_FETCH_DETAILS_FAIL,
  error: err,
  receivedAt: Date.now(),
})

export const fetchDetailsDone = (deets) => ({
  type: EXCEPTIONS_FETCH_DETAILS_DONE,
  data: deets.ticket,
  receivedAt: Date.now(),
})
