import client from '../../ax-client';
import {
  CREATE_GROUP_FETCH,
  CREATE_GROUP_DONE,
  CREATE_GROUP_FAIL,
  LIST_GROUPS_FETCH,
  LIST_GROUPS_DONE,
  LIST_GROUPS_FAIL,
  READ_GROUP_FETCH,
  READ_GROUP_DONE,
  READ_GROUP_FAIL,
  UPDATE_GROUP_FETCH,
  UPDATE_GROUP_DONE,
  UPDATE_GROUP_FAIL,
  DELETE_GROUP_FETCH,
  DELETE_GROUP_DONE,
  DELETE_GROUP_FAIL,
  LIST_ACCOUNTS_BY_GROUP_FETCH,
  LIST_ACCOUNTS_BY_GROUP_DONE,
  LIST_ACCOUNTS_BY_GROUP_FAIL,
  ADD_ACCOUNT_TO_GROUP_FETCH,
  ADD_ACCOUNT_TO_GROUP_DONE,
  ADD_ACCOUNT_TO_GROUP_FAIL,
  REMOVE_ACCOUNT_FROM_GROUP_FETCH,
  REMOVE_ACCOUNT_FROM_GROUP_DONE,
  REMOVE_ACCOUNT_FROM_GROUP_FAIL,
  LIST_GROUP_PERMISSIONS_FETCH,
  LIST_GROUP_PERMISSIONS_DONE,
  LIST_GROUP_PERMISSIONS_FAIL,
  UPDATE_GROUP_PERMISSIONS_FETCH,
  UPDATE_GROUP_PERMISSIONS_DONE,
  UPDATE_GROUP_PERMISSIONS_FAIL,
} from '../constants';
import { browserHistory } from 'react-router';

export const createGroup = data => (dispatch) => {
  dispatch({ type: CREATE_GROUP_FETCH });
  client.groups.createGroup(data)
    .then((res) => {
      dispatch({ type: CREATE_GROUP_DONE, data: res.data });
      browserHistory.push(`/app/manage-business/groups/${res.data.id}`);
    })
    .catch(err => dispatch({ type: CREATE_GROUP_FAIL, error: err }));
};
export const listGroups = params => (dispatch) => {
  dispatch({ type: LIST_GROUPS_FETCH });
  client.groups.listGroups(params)
    .then(res => dispatch({ type: LIST_GROUPS_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_GROUPS_FAIL, error: err }));
};
export const readGroup = id => (dispatch) => {
  dispatch({ type: READ_GROUP_FETCH });
  client.groups.readGroup(id)
    .then(res => dispatch({ type: READ_GROUP_DONE, data: res.data }))
    .catch(err => dispatch({ type: READ_GROUP_FAIL, error: err }));
};
export const updateGroup = (id, data) => (dispatch) => {
  dispatch({ type: UPDATE_GROUP_FETCH });
  client.groups.updateGroup(id, data)
    .then((res) => {
      dispatch({ type: UPDATE_GROUP_DONE, data: res.data });
      browserHistory.push('/app/manage-business/groups');
    })
    .catch(err => dispatch({ type: UPDATE_GROUP_FAIL, error: err }));
};
export const deleteGroup = id => (dispatch) => {
  dispatch({ type: DELETE_GROUP_FETCH });
  client.groups.deleteGroup(id)
    .then((res) => {
      dispatch({ type: DELETE_GROUP_DONE, data: res.data });
      browserHistory.push('/app/manage-business/groups');
    })
    .catch(err => dispatch({ type: DELETE_GROUP_FAIL, error: err }));
};

export const listAccountsByGroup = id => (dispatch) => {
  dispatch({ type: LIST_ACCOUNTS_BY_GROUP_FETCH });
  client.groups.listAccountsByGroup(id)
    .then(res => dispatch({ type: LIST_ACCOUNTS_BY_GROUP_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_ACCOUNTS_BY_GROUP_FAIL, error: err }));
};
export const addAccountToGroup = (id, accountId) => (dispatch) => {
  dispatch({ type: ADD_ACCOUNT_TO_GROUP_FETCH });
  return client.groups.addAccountToGroup(id, accountId)
    .then((res) => {
      dispatch({ type: ADD_ACCOUNT_TO_GROUP_DONE, data: res.data });
      // browserHistory.push("/app/manage-business/groups");
    })
    .catch(err => dispatch({ type: ADD_ACCOUNT_TO_GROUP_FAIL, error: err }));
};
export const removeAccountFromGroup = (id, accountId) => (dispatch) => {
  dispatch({ type: REMOVE_ACCOUNT_FROM_GROUP_FETCH });
  return client.groups.removeAccountFromGroup(id, accountId)
    .then((res) => {
      dispatch({ type: REMOVE_ACCOUNT_FROM_GROUP_DONE, data: res.data });
      // browserHistory.push("/app/manage-business/groups");
    })
    .catch(err => dispatch({ type: REMOVE_ACCOUNT_FROM_GROUP_FAIL, error: err }));
};
export const listGroupPermissions = id => (dispatch) => {
  dispatch({ type: LIST_GROUP_PERMISSIONS_FETCH });
  client.groups.listGroupPermissions(id)
    .then(res => dispatch({ type: LIST_GROUP_PERMISSIONS_DONE, data: res.data }))
    .catch(err => dispatch({ type: LIST_GROUP_PERMISSIONS_FAIL, error: err }));
};
export const updateGroupPermissions = (id, data) => (dispatch) => {
  dispatch({ type: UPDATE_GROUP_PERMISSIONS_FETCH });
  client.groups.updateGroupPermissions(id, data)
    .then((res) => {
      dispatch({ type: UPDATE_GROUP_PERMISSIONS_DONE, data: res.data });
      // browserHistory.push("/app/manage-business/groups");
    })
    .catch(err => dispatch({ type: UPDATE_GROUP_PERMISSIONS_FAIL, error: err }));
};
