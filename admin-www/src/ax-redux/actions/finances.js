import client from '../../ax-client'
import {
    LIST_RESELLER_INVOICES_FETCH,
    LIST_RESELLER_INVOICES_DONE,
    LIST_RESELLER_INVOICES_FAIL,
    GET_RESELLER_INVOICES_FETCH,
    GET_RESELLER_INVOICES_DONE,
    GET_RESELLER_INVOICES_FAIL,
    GET_RESELLER_TICKET_VOLUME_FETCH,
    GET_RESELLER_TICKET_VOLUME_DONE,
    GET_RESELLER_TICKET_VOLUME_FAIL,
    GET_RESELLER_DOLLAR_VOLUME_FETCH,
    GET_RESERLLER_DOLLAR_VOLUME_DONE,
    GET_RESELLER_DOLLAR_VOLUME_FAIL,
    GET_RESELLER_TICKET_DETAIL_FETCH,
    GET_RESELLER_TICKET_DETAIL_DONE,
    GET_RESELLER_TICKET_DETAIL_FAIL
} from '../constants'
import { browserHistory } from 'react-router';

export const listResellerInvoices = (data) => {
  return dispatch => {
    dispatch({ type: LIST_RESELLER_INVOICES_FETCH })
    client.finances.listResellerInvoices(data)
      .then(res => dispatch({ type: LIST_RESELLER_INVOICES_DONE, data: res.data }))
      .catch(err => dispatch({ type: LIST_RESELLER_INVOICES_FAIL, error: err }))
  }
}

export const getResellerInvoices = (data) => {
  return dispatch => {
    dispatch({ type: GET_RESELLER_INVOICES_FETCH })
    client.finances.getResellerInvoices(data)
      .then(res => dispatch({ type: GET_RESELLER_INVOICES_DONE, data: res.data }))
      .catch(err => dispatch({ type: GET_RESELLER_INVOICES_FAIL, error: err }))
  }
}

export const getResellerTicketVolume = (data) => {
  return dispatch => {
    dispatch({ type: GET_RESELLER_TICKET_VOLUME_FETCH })
    client.finances.getResellerTicketVolume(data)
      .then(res => dispatch({ type: GET_RESELLER_TICKET_VOLUME_DONE, data: res.data }))
      .catch(err => dispatch({ type: GET_RESELLER_TICKET_VOLUME_FAIL, error: err }))
  }
}

export const getResellerDollarVolume = (data) => {
  return dispatch => {
    dispatch({ type: GET_RESELLER_DOLLAR_VOLUME_FETCH })
    client.finances.getResellerDollarVolume(data)
      .then(res => dispatch({ type: GET_RESERLLER_DOLLAR_VOLUME_DONE, data: res.data }))
      .catch(err => dispatch({ type: GET_RESELLER_DOLLAR_VOLUME_FAIL, error: err }))
  }
}

export const getResellerTicketDetails = (data) => {
    data.status = "STATUS_REDEEMED"
    return dispatch => {
        dispatch({ type: GET_RESELLER_TICKET_DETAIL_FETCH })
        client.tickets.allTickets(data)
          .then(res => dispatch({ type: GET_RESELLER_TICKET_DETAIL_DONE, data: res.data }))
          .catch(err => dispatch({ type: GET_RESELLER_TICKET_DETAIL_FAIL, error: err }))
      }
}

export const exportCSV = (params = {}) => {
    return (dispatch, getState) => {
      let state = getState()
      params.offset = 0;
      params.limit = 10000;
      if (state.app.user.account.orgCode != "" && params.supplierCode == null && params.supplierId == null) {
        params.supplierId = state.app.user.account.orgCode
      }
      client.tickets.ticketsOpenCSV(params)
    }
}

export const exportXLSX = (params = {}) => {
    return (dispatch, getState) => {
      let state = getState()
      params.offset = 0;
      params.limit = 10000;
      if (state.app.user.account.orgCode != "" &&  params.supplierCode == null && params.supplierId == null) {
        params.supplierId = state.app.user.account.orgCode
      }
      client.tickets.ticketsOpenXLSX(params)
    }
}
