import {
  DASHBOARD_FETCH,
  DASHBOARD_FETCH_FAIL,
  DASHBOARD_FETCH_DONE,
  DASHBOARD_TOGGLE_MENU,
  DASHBOARD_TOGGLE_ADMIN_MENU
} from '../constants'

const dashboardDefault = {
  data: {},
  fetching: false,
  error: false,
  menuOpen: false,
  adminMenuOpen: false
}

function dashboard(state = dashboardDefault, action) {
  switch (action.type) {

    case DASHBOARD_FETCH:
      return Object.assign({}, state, {
         data: {},
         fetching: true,
         error: false,
      })
    case DASHBOARD_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
    case DASHBOARD_FETCH_DONE:
      return Object.assign({}, state, {
        data: action.data,
        fetching: false,
        error: false,
      })
     case DASHBOARD_TOGGLE_MENU:
     return Object.assign({}, state, {
       menuOpen: !state.menuOpen
     })
     case DASHBOARD_TOGGLE_ADMIN_MENU:
     return Object.assign({}, state, {
       adminMenuOpen: !state.adminMenuOpen
     })
  }
  return state
}
export default dashboard
