import {
  RESELLER_CATALOG_UPDATE_FETCH,
  RESELLER_CATALOG_UPDATE_DONE,
  RESELLER_CATALOG_UPDATE_FAIL
} from '../../constants'

const updateCatalogState = {
  fetching: false,
  error: false,
  data: false
}

function updateCatalog(state = updateCatalogState, action) {
  switch (action.type) {
    case RESELLER_CATALOG_UPDATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case RESELLER_CATALOG_UPDATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case RESELLER_CATALOG_UPDATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default updateCatalog
