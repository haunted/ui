import {
  GET_RESELLER_FETCH,
  GET_RESELLER_DONE,
  GET_RESELLER_FAIL
} from '../../constants'

const getState = {
  fetching: false,
  error: false,
  data: false
}

function get(state = getState, action) {
  switch (action.type) {
    case GET_RESELLER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case GET_RESELLER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case GET_RESELLER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default get
