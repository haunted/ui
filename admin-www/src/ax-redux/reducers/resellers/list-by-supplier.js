import {
  LIST_SUPPLIER_RESELLERS_FETCH,
  LIST_SUPPLIER_RESELLERS_DONE,
  LIST_SUPPLIER_RESELLERS_FAIL
} from '../../constants'

const listState = {
  fetching: false,
  error: false,
  data: false,
  resellersById: {}
}

function listBySupplier( state = listState, action ) {
  switch ( action.type ) {
    case LIST_SUPPLIER_RESELLERS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case LIST_SUPPLIER_RESELLERS_DONE:
      let resellersById = {}
      if ( !!action.data && !!action.data.resellers && !!action.data.resellers.length ) {
        action.data.resellers.forEach( v => {
         resellersById[v.id] = v
        })
      }
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
        resellersById
      })
    case LIST_SUPPLIER_RESELLERS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listBySupplier
