import {
  RESELLER_CATALOG_ITEM_CREATE_FETCH,
  RESELLER_CATALOG_ITEM_CREATE_DONE,
  RESELLER_CATALOG_ITEM_CREATE_FAIL
} from '../../constants'

const createCatalogItemState = {
  fetching: false,
  error: false,
  data: false
}

function createCatalogItem(state = createCatalogItemState, action) {
  switch (action.type) {
    case RESELLER_CATALOG_ITEM_CREATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case RESELLER_CATALOG_ITEM_CREATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case RESELLER_CATALOG_ITEM_CREATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default createCatalogItem
