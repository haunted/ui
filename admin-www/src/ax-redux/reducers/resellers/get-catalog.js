import {
  RESELLER_CATALOG_FETCH,
  RESELLER_CATALOG_FETCH_DONE,
  RESELLER_CATALOG_FETCH_FAIL
} from '../../constants'

const getCatalogState = {
  fetching: false,
  error: false,
  data: false
}

function getCatalog(state = getCatalogState, action) {
  switch (action.type) {
    case RESELLER_CATALOG_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case RESELLER_CATALOG_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case RESELLER_CATALOG_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default getCatalog
