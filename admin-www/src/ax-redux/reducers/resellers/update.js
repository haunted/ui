import {
  UPDATE_RESELLER_FETCH,
  UPDATE_RESELLER_DONE,
  UPDATE_RESELLER_FAIL
} from '../../constants'

const updateState = {
  fetching: false,
  error: false,
  data: false
}

function update(state = updateState, action) {
  switch (action.type) {
    case UPDATE_RESELLER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case UPDATE_RESELLER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case UPDATE_RESELLER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default update
