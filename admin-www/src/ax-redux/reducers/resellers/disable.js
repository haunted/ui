import {
  DISABLE_RESELLER_FETCH,
  DISABLE_RESELLER_DONE,
  DISABLE_RESELLER_FAIL
} from '../../constants'

const disableState = {
  fetching: false,
  error: false,
  data: false
}

function disable(state = disableState, action) {
  switch (action.type) {
    case DISABLE_RESELLER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case DISABLE_RESELLER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case DISABLE_RESELLER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default disable
