import {
  CREATE_RESELLER_FETCH,
  CREATE_RESELLER_DONE,
  CREATE_RESELLER_FAIL
} from '../../constants'

const createState = {
  fetching: false,
  error: false,
  data: false
}

function create(state = createState, action) {
  switch (action.type) {
    case CREATE_RESELLER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CREATE_RESELLER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CREATE_RESELLER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default create
