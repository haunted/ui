import {
  CREATE_PASS_ISSUER_FETCH,
  CREATE_PASS_ISSUER_DONE,
  CREATE_PASS_ISSUER_FAIL
} from '../../constants'

const createState = {
  fetching: false,
  error: false,
  data: false
}

function create(state = createState, action) {
  switch (action.type) {
    case CREATE_PASS_ISSUER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CREATE_PASS_ISSUER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CREATE_PASS_ISSUER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default create
