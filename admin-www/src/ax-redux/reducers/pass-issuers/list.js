import {
  LIST_PASS_ISSUERS_FETCH,
  LIST_PASS_ISSUERS_DONE,
  LIST_PASS_ISSUERS_FAIL
} from '../../constants'

const listState = {
  fetching: false,
  error: false,
  data: [],
  params: {
    sort: "",
  },
  passIssuersById: {}
}

function list(state = listState, action) {
  switch (action.type) {
    case LIST_PASS_ISSUERS_FETCH:
      return Object.assign({}, state, {
        params: action.params,
        fetching: true,
        error: false,
      })
    case LIST_PASS_ISSUERS_DONE:
      let passIssuersById = {}
      if (!!action.data && !!action.data.passIssuers && !!action.data.passIssuers.length) {
        action.data.passIssuers.forEach(v => {
         passIssuersById[v.id] = v
        })
      }
      return Object.assign({}, state, {
        fetching: false,
        data: action.data.passissuers,
        passIssuersById
      })
    case LIST_PASS_ISSUERS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default list
