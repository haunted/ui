import {
  DISABLE_PASS_ISSUER_FETCH,
  DISABLE_PASS_ISSUER_DONE,
  DISABLE_PASS_ISSUER_FAIL
} from '../../constants'

const disableState = {
  fetching: false,
  error: false,
  data: false
}

function disable(state = disableState, action) {
  switch (action.type) {
    case DISABLE_PASS_ISSUER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case DISABLE_PASS_ISSUER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case DISABLE_PASS_ISSUER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default disable
