import {
  PASS_ISSUER_CATALOG_DISABLE_FETCH,
  PASS_ISSUER_CATALOG_DISABLE_DONE,
  PASS_ISSUER_CATALOG_DISABLE_FAIL
} from '../../constants'

const disableCatalogState = {
  fetching: false,
  error: false,
  data: false
}

function disableCatalog(state = disableCatalogState, action) {
  switch (action.type) {
    case PASS_ISSUER_CATALOG_DISABLE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PASS_ISSUER_CATALOG_DISABLE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PASS_ISSUER_CATALOG_DISABLE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default disableCatalog
