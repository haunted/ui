import {
  PASS_ISSUER_CATALOGS_LIST_FETCH,
  PASS_ISSUER_CATALOGS_LIST_DONE,
  PASS_ISSUER_CATALOGS_LIST_FAIL
} from '../../constants'

const listCatalogsState = {
  fetching: false,
  error: false,
  data: false
}

function listCatalogs(state = listCatalogsState, action) {
  switch (action.type) {
    case PASS_ISSUER_CATALOGS_LIST_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PASS_ISSUER_CATALOGS_LIST_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PASS_ISSUER_CATALOGS_LIST_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listCatalogs
