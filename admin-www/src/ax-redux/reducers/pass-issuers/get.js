import {
  GET_PASS_ISSUER_FETCH,
  GET_PASS_ISSUER_DONE,
  GET_PASS_ISSUER_FAIL
} from '../../constants'

const getState = {
  fetching: false,
  error: false,
  data: false,
  version: 0,
}

function get(state = getState, action) {
  switch (action.type) {
    case GET_PASS_ISSUER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case GET_PASS_ISSUER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
        version: state.version + 1,
      })
    case GET_PASS_ISSUER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default get
