import {
  UPDATE_PASS_ISSUER_FETCH,
  UPDATE_PASS_ISSUER_DONE,
  UPDATE_PASS_ISSUER_FAIL
} from '../../constants'

const updateState = {
  fetching: false,
  error: false,
  data: false
}

function update(state = updateState, action) {
  switch (action.type) {
    case UPDATE_PASS_ISSUER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case UPDATE_PASS_ISSUER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case UPDATE_PASS_ISSUER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default update
