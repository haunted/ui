import {
  PASS_ISSUER_CATALOG_CREATE_FETCH,
  PASS_ISSUER_CATALOG_CREATE_DONE,
  PASS_ISSUER_CATALOG_CREATE_FAIL
} from '../../constants'

const createCatalogState = {
  fetching: false,
  error: false,
  data: false
}

function createCatalog(state = createCatalogState, action) {
  switch (action.type) {
    case PASS_ISSUER_CATALOG_CREATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PASS_ISSUER_CATALOG_CREATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PASS_ISSUER_CATALOG_CREATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default createCatalog
