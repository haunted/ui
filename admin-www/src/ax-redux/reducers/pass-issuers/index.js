import {combineReducers} from 'redux'

import create from './create'
import update from './update'
import list from './list'
import get from './get'
import disable from './disable'
import createCatalog from './create-catalog'
import updateCatalog from './update-catalog'
import listCatalogs from './list-catalogs'
import getCatalog from './get-catalog'
import disableCatalog from './disable-catalog'

export default combineReducers({
  create,
  update,
  list,
  get,
  disable,
  createCatalog,
  updateCatalog,
  listCatalogs,
  getCatalog,
  disableCatalog,
})
