import {
    ORG_SETTINGS_CREATE_FETCH,
    ORG_SETTINGS_CREATE_DONE,
    ORG_SETTINGS_CREATE_FAIL,
    ORG_SETTINGS_LIST_FETCH,
    ORG_SETTINGS_LIST_DONE,
    ORG_SETTINGS_LIST_FAIL,
    ORG_SETTINGS_READ_FETCH,
    ORG_SETTINGS_READ_DONE,
    ORG_SETTINGS_READ_FAIL,
    ORG_SETTINGS_UPDATE_FETCH,
    ORG_SETTINGS_UPDATE_DONE,
    ORG_SETTINGS_UPDATE_FAIL,
    ORG_SETTINGS_DELETE_FETCH,
    ORG_SETTINGS_DELETE_DONE,
    ORG_SETTINGS_DELETE_FAIL
} from '../constants'

const OrgSettingsState = {
    listOrgSettings: {
        fetching: false,
        error: false,
        data: false
    },
    readOrgSettings: {
        fetching: false,
        error: false,
        data: false
    },
    createOrgSettings: {
        fetching: false,
        error: false,
        data: false
    },
    updateOrgSettings: {
        fetching: false,
        error: false,
        data: false
    },
    deleteOrgSettings: {
        fetching: false,
        error: false,
        data: false
    }
}

function OrgSettings(state = OrgSettingsState, action) {
    switch (action.type) {
        case ORG_SETTINGS_READ_FETCH:
            return Object.assign({}, state, {
                readOrgSettings: {
                    fetching: true,
                    error: false,
                    data: false,
                }
            })
        case ORG_SETTINGS_READ_DONE:
            return Object.assign({}, state, {
                readOrgSettings: {
                    fetching: false,
                    error: false,
                    data: action.data,
                }
            })
        case ORG_SETTINGS_READ_FAIL:
            return Object.assign({}, state, {
                readOrgSettings: {
                    fetching: false,
                    error: action.error,
                    data: false
                }
            })
        case ORG_SETTINGS_LIST_FETCH:
            return Object.assign({}, state, {
                listOrgSettings: {
                    fetching: true,
                    error: false,
                    data: false,
                }
            })
        case ORG_SETTINGS_LIST_DONE:
            let resellersById = {}
            return Object.assign({}, state, {
                listOrgSettings: {
                    fetching: false,
                    error: false,
                    data: action.data,
                }
            })
        case ORG_SETTINGS_LIST_FAIL:
            return Object.assign({}, state, {
                listOrgSettings: {
                    fetching: false,
                    error: action.error,
                    data: false
                }
            })
        case ORG_SETTINGS_CREATE_FETCH:
            return Object.assign({}, state, {
                createOrgSettings: {
                    fetching: true,
                    error: false,
                    data: false,
                }
            })
        case ORG_SETTINGS_CREATE_DONE:
            return Object.assign({}, state, {
                createOrgSettings: {
                    fetching: false,
                    error: false,
                    data: action.data,
                }
            })
        case ORG_SETTINGS_CREATE_FAIL:
            return Object.assign({}, state, {
                createOrgSettings: {
                    fetching: false,
                    error: action.error,
                    data: false
                }
            })
        case ORG_SETTINGS_UPDATE_FETCH:
            return Object.assign({}, state, {
                updateOrgSettings: {
                    fetching: true,
                    error: false,
                    data: false,
                }
            })
        case ORG_SETTINGS_UPDATE_DONE:
            return Object.assign({}, state, {
                updateOrgSettings: {
                    fetching: false,
                    error: false,
                    data: action.data,
                },
                // also update the readOrgSettings data because it's used for other stuff
                readOrgSettings: {
                    fetching: false,
                    error: false,
                    data: action.data
                }
            })
        case ORG_SETTINGS_UPDATE_FAIL:
            return Object.assign({}, state, {
                updateOrgSettings: {
                    fetching: false,
                    error: action.error,
                    data: false
                }
            })
        case ORG_SETTINGS_DELETE_FETCH:
            return Object.assign({}, state, {
                deleteOrgSettings: {
                    fetching: true,
                    error: false,
                    data: false,
                }
            })
        case ORG_SETTINGS_DELETE_DONE:
            return Object.assign({}, state, {
                deleteOrgSettings: {
                    fetching: false,
                    error: false,
                    data: action.data,
                }
            })
        case ORG_SETTINGS_DELETE_FAIL:
            return Object.assign({}, state, {
                deleteOrgSettings: {
                    fetching: false,
                    error: action.error,
                    data: false
                }
            })
    }
    return state
}

export default OrgSettings
