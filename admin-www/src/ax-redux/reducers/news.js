import {
  NEWS_FETCH,
  NEWS_FETCH_FAIL,
  NEWS_FETCH_DONE,
  NEWS_CREATE,
  NEWS_CREATE_FAIL,
  NEWS_CREATE_DONE,
} from '../constants'


const newsDefault = {
  data: [],
  fetching: false,
  error: false,
}

function news(state = newsDefault, action) {
  switch (action.type) {

    case NEWS_FETCH:
      return Object.assign({}, state, {
         data: [],
         fetching: true,
         error: false,
      })
    case NEWS_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
    case NEWS_FETCH_DONE:
      return Object.assign({}, state, {
        data: action.data.news,
        fetching: false,
        error: false,
      })

    case NEWS_CREATE:
      return Object.assign({}, state, {
         created: false,
         creating: true,
         createError: false,
      })
    case NEWS_CREATE_FAIL:
      return Object.assign({}, state, {
        creating: false,
        createError: action.error,
      })
    case NEWS_CREATE_DONE:
      return Object.assign({}, state, {
        created: action.data,
        creating: false,
        createError: false,
      })
  }
  return state
}
export default news
