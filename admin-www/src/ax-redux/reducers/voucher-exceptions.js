import {
  REPORTS_VOUCHER_EXCEPTIONS_FETCH,
  REPORTS_VOUCHER_EXCEPTIONS_FETCH_FAIL,
  REPORTS_VOUCHER_EXCEPTIONS_FETCH_DONE,
  REPORTS_VOUCHER_EXCEPTIONS_SET_START_DATE,
  REPORTS_VOUCHER_EXCEPTIONS_SET_END_DATE,
} from '../constants'
import moment from 'moment-timezone'
import {formatCurrency} from '../../util'


const defaultState = {
  data: [],
  startDateFilter: new Date(),
  endDateFilter: new Date(),
  fetching: false,
  error: false,
  total: 0,
}

function voucherExceptionsReport(state = defaultState, action) {
  switch (action.type) {
    case REPORTS_VOUCHER_EXCEPTIONS_SET_START_DATE:
      return Object.assign({}, state, {
        startDateFilter: action.date,
      })
    case REPORTS_VOUCHER_EXCEPTIONS_SET_END_DATE:
      return Object.assign({}, state, {
        endDateFilter: action.date,
      })
    case REPORTS_VOUCHER_EXCEPTIONS_FETCH:
      return Object.assign({}, state, {  }, {
         data: [],
         fetching: true,
         error: false,
         total: 0,
      })
    case REPORTS_VOUCHER_EXCEPTIONS_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
    case REPORTS_VOUCHER_EXCEPTIONS_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        error: false,
        data: action.data,
        total: action.data.total ? action.data.total : action.data.length,
      })

  }
  return state
}
export default voucherExceptionsReport
