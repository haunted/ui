import {
    REDEMPTIONS_FETCH,
    REDEMPTIONS_FETCH_DONE,
    REDEMPTIONS_FETCH_FAIL,
} from '../constants'

const redemptionsState = {
    voidRedemption: {
        fetching: false,
        error: false,
        data: false
    },
}

function redemptions(state = redemptionsState, action) {
    switch (action.type) {
        case REDEMPTIONS_FETCH_DONE:
        return Object.assign({}, state, {
            voidRedemption: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case REDEMPTIONS_FETCH_FAIL:
        return Object.assign({}, state, {
            voidRedemption: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
    }
    return state
}

export default redemptions
