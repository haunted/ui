import {
  PASS_FETCH,
  PASS_FETCH_FAIL,
  PASS_FETCH_DONE,
  PASS_CREATE_FETCH,
  PASS_CREATE_FETCH_FAIL,
  PASS_CREATE_FETCH_DONE,
  PASS_REDEMPTIONS_FETCH,
  PASS_REDEMPTIONS_FETCH_FAIL,
  PASS_REDEMPTIONS_FETCH_DONE
} from '../constants'


const passDefault = {
  get: {
    data: false,
    fetching: false,
    error: false,
  },
  create: {
    data: false,
    fetching: false,
    error: false,
  },
  getRedemptions: {
    data: false,
    fetching: false,
    error: false,
  }
}

function pass( state = passDefault, action ) {

  switch (action.type) {

    case PASS_FETCH:
      return {
        ...state,
        get: {
          ...state.get,
          data: false,
          fetching: true,
          error: false
        }
      }
    case PASS_FETCH_FAIL:
      return {
        ...state,
        get: { 
          ...state.get,
            data: false,
            fetching: false,
            error: action.error
        }
      }
    case PASS_FETCH_DONE:
      return {
        ...state,
        get: { 
          ...state.get,
            data: action.data,
            fetching: false,
            error: false
        }
      }
    case PASS_CREATE_FETCH:
      return {
        ...state,
        create: { 
          ...state.get,
            data: false,
            fetching: true,
            error: false
        }
      }
    case PASS_CREATE_FETCH_FAIL:
      return {
        ...state,
        create: {
          ...state.get, 
            data: false,
            fetching: false,
            error: action.error
        }
      }
    case PASS_CREATE_FETCH_DONE:
      return {
        ...state,
        create: {
          ...state.get,
            data: action.data,
            fetching: false,
            error: false
        }
      }
    case PASS_REDEMPTIONS_FETCH:
      return {
        ...state,
         getRedemptions: {
           ...state.get,
            data: false,
            fetching: true,
            error: false
        }
      }
    case PASS_REDEMPTIONS_FETCH_FAIL:
      return {
        ...state,
        getRedemptions: {
            ...state.getRedemptions,
            data: false,
            fetching: false,
            error: action.error
        }
      }
    case PASS_REDEMPTIONS_FETCH_DONE:
      return {
        ...state,
        getRedemptions: {
            ...state.getRedemptions,
            data: action.data,
            fetching: false,
            error: false
        }
      }

  }

  return state

}

export default pass

