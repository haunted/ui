import {
    /* remove these three once the master shift summary endpoint is here */
    WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_FETCH,
    WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_DONE,
    WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_FAIL,
    GET_MASTER_SHIFT_REPORT_SUMMARY_FETCH,
    GET_MASTER_SHIFT_REPORT_SUMMARY_DONE,
    GET_MASTER_SHIFT_REPORT_SUMMARY_FAIL,
    GET_SHIFT_REPORT_SUMMARY_FETCH,
    GET_SHIFT_REPORT_SUMMARY_DONE,
    GET_SHIFT_REPORT_SUMMARY_FAIL,
    GET_SHIFT_REPORT_FETCH,
    GET_SHIFT_REPORT_DONE,
    GET_SHIFT_REPORT_FAIL
} from '../constants'

const shiftsState = {
  getMasterShiftSummaryReport: {
    fetching: false,
    error: false,
    data: false,
  },
  getShiftReportSummary: {
    fetching: false,
    error: false,
    data: false,
  },
  getShiftReport: {
    fetching: false,
    error: false,
    data: false,
  }
}

function shifts(state = shiftsState, action) {
  switch (action.type) {
    case WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_FETCH:
      return Object.assign({}, state, {
        getMasterShiftSummaryReport: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_DONE:
    return Object.assign({}, state, {
      getMasterShiftSummaryReport: {
        fetching: false,
        error: false,
        data: action.data,
      }
    })
    case WIP_GET_MASTER_SHIFT_REPORT_SUMMARY_FAIL:
      return Object.assign({}, state, {
        getMasterShiftSummaryReport: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
    case GET_MASTER_SHIFT_REPORT_SUMMARY_FETCH:
      return Object.assign({}, state, {
        getMasterShiftSummaryReport: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case GET_MASTER_SHIFT_REPORT_SUMMARY_DONE:
    return Object.assign({}, state, {
      getMasterShiftSummaryReport: {
        fetching: false,
        error: false,
        data: action.data,
      }
    })
    case GET_MASTER_SHIFT_REPORT_SUMMARY_FAIL:
      return Object.assign({}, state, {
        getMasterShiftSummaryReport: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
    case GET_SHIFT_REPORT_SUMMARY_FETCH:
      return Object.assign({}, state, {
        getShiftReportSummary: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case GET_SHIFT_REPORT_SUMMARY_DONE:
    return Object.assign({}, state, {
      getShiftReportSummary: {
        fetching: false,
        error: false,
        data: action.data,
      }
    })
    case GET_SHIFT_REPORT_SUMMARY_FAIL:
      return Object.assign({}, state, {
        getShiftReportSummary: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
      case GET_SHIFT_REPORT_FETCH:
        return Object.assign({}, state, {
          getShiftReport: {
            fetching: true,
            error: false,
            data: false,
          }
        })
      case GET_SHIFT_REPORT_DONE:
      return Object.assign({}, state, {
        getShiftReport: {
          fetching: false,
          error: false,
          data: action.data,
        }
      })
      case GET_SHIFT_REPORT_FAIL:
        return Object.assign({}, state, {
          getShiftReport: {
            fetching: false,
            error: action.error,
            data: false
          }
        })
  }
  return state
}

export default shifts
