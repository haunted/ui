import {
  REGISTER_FETCH,
  REGISTER_DONE,
  REGISTER_FAIL,
  LOGIN_FETCH,
  LOGIN_DONE,
  LOGIN_FAIL,
  LOGOUT,
  ACTOUREX_TECHNICAL_STAFF,
  LOAD_AUTHORIZATION,
  LOAD_AUTHORIZATION_FAIL,
  TOKEN_REFRESH_FETCH,
  TOKEN_REFRESH_DONE,
  TOKEN_REFRESH_FAIL,
  ON_AUTHORIZATION_COMPLETE,
  FIRE_AUTHORIZATION_COMPLETE,
  ON_AUTHORIZATION_FAILURE,
  FIRE_AUTHORIZATION_FAILURE,
} from '../constants'

const authState = {
  registerFetching: false,
  registerError: false,
  loginFetching: false,
  loginError: false,
  loggedIn: false,
  authData: {},
  refreshedTokenEver: false,
  authCompleteCallback: ()=>{},
  authFailureCallback: ()=>{},
}

function auth(state = authState, action) {
  switch (action.type) {
    case REGISTER_FETCH:
      return Object.assign({}, state, {
        registerFetching: true,
        registerError: false,
      })
    case REGISTER_DONE:
      return Object.assign({}, state, {
        registerFetching: false,
        loggedIn: true,
        user: action.data,
      })
    case REGISTER_FAIL:
      return Object.assign({}, state, {
        registerFetching: false,
        registerError: action.error,
      })
    case LOGIN_FETCH:
      return Object.assign({}, state, {
        loginFetching: true,
        loginError: false,
        authData: {},
        loggedIn: false,
      })
    case LOGOUT:
      return Object.assign({}, state, {
        refreshedTokenEver: false,
        loggedIn: false,
        authData: {},
      })
    case LOGIN_DONE:
      return Object.assign({}, state, {
        loginFetching: false,
        loggedIn: true,
        authData: action.data,
      })
    case LOGIN_FAIL:
      return Object.assign({}, state, {
        loginFetching: false,
        loginError: action.error,
      })
    case LOAD_AUTHORIZATION:
      return Object.assign({}, state, {
        loggedIn: true,
        authData: action.data,
        role: ACTOUREX_TECHNICAL_STAFF, // TODO: roles from api
      })
    case LOAD_AUTHORIZATION_FAIL:
      return Object.assign({}, state, {
        loggedIn: false,
        authData: {},
      })
    case TOKEN_REFRESH_FETCH:
      return Object.assign({}, state, {

      })
    case TOKEN_REFRESH_DONE:
      return Object.assign({}, state, {
        authData: action.data,
        refreshedTokenEver: true,
      })
    case TOKEN_REFRESH_FAIL:
      return Object.assign({}, state, {

      })
    case ON_AUTHORIZATION_COMPLETE:
      return Object.assign({}, state, {
        authCompleteCallback: action.fn,
      })
    case FIRE_AUTHORIZATION_COMPLETE:
      state.authCompleteCallback()
      return Object.assign({}, state, {

      })
    case ON_AUTHORIZATION_FAILURE:
      return Object.assign({}, state, {
        authFailureCallback: action.fn,
      })
    case FIRE_AUTHORIZATION_FAILURE:
      state.authFailureCallback(action.err)
      return Object.assign({}, state, {})
  }
  return state
}

export default auth
