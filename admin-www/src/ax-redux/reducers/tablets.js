import {
    TABLET_FETCH,
    TABLET_FETCH_DONE,
    TABLET_FETCH_FAIL,
    TABLETS_FETCH,
    TABLETS_FETCH_DONE,
    TABLETS_FETCH_FAIL,
    TABLET_CREATE_FETCH,
    TABLET_CREATE_DONE,
    TABLET_CREATE_FAIL,
    TABLET_UPDATE_FETCH,
    TABLET_UPDATE_DONE,
    TABLET_UPDATE_FAIL,
    TABLET_DELETE_FETCH,
    TABLET_DELETE_DONE,
    TABLET_DELETE_FAIL
} from '../constants'

const tabletsState = {
    getTablets: {
        fetching: false,
        error: false,
        data: false
    },
    getTablet: {
        fetching: false,
        error: false,
        data: false
    },
    createTablet: {
        fetching: false,
        error: false,
        data: false
    },
    updateTablet: {
        fetching: false,
        error: false,
        data: false
    },
    deleteTablet: {
        fetching: false,
        error: false,
        data: false
    },
}

function tablets(state = tabletsState, action) {
  switch (action.type) {
    case TABLET_FETCH:
      return Object.assign({}, state, {
        getTablet: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case TABLET_FETCH_DONE:
      return Object.assign({}, state, {
        getTablet: {
          fetching: false,
          error: false,
          data: action.data,
        }
      })
    case TABLET_FETCH_FAIL:
      return Object.assign({}, state, {
        getTablet: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
      case TABLETS_FETCH:
        return Object.assign({}, state, {
          getTablets: {
            fetching: true,
            error: false,
            data: false,
          }
        })
      case TABLETS_FETCH_DONE:
        let resellersById = {}
        return Object.assign({}, state, {
          getTablets: {
            fetching: false,
            error: false,
            data: action.data,
          }
        })
      case TABLETS_FETCH_FAIL:
        return Object.assign({}, state, {
          getTablets: {
            fetching: false,
            error: action.error,
            data: false
          }
        })
        case TABLET_CREATE_FETCH:
          return Object.assign({}, state, {
            createTablet: {
              fetching: true,
              error: false,
              data: false,
            }
          })
        case TABLET_CREATE_DONE:
          return Object.assign({}, state, {
            createTablet: {
              fetching: false,
              error: false,
              data: action.data,
            }
          })
        case TABLET_CREATE_FAIL:
          return Object.assign({}, state, {
            createTablet: {
              fetching: false,
              error: action.error,
              data: false
            }
          })
          case TABLET_UPDATE_FETCH:
            return Object.assign({}, state, {
              updateTablet: {
                fetching: true,
                error: false,
                data: false,
              }
            })
          case TABLET_UPDATE_DONE:
            return Object.assign({}, state, {
              updateTablet: {
                fetching: false,
                error: false,
                data: action.data,
              }
            })
          case TABLET_UPDATE_FAIL:
            return Object.assign({}, state, {
              updateTablet: {
                fetching: false,
                error: action.error,
                data: false
              }
            })
            case TABLET_DELETE_FETCH:
              return Object.assign({}, state, {
                deleteTablet: {
                  fetching: true,
                  error: false,
                  data: false,
                }
              })
            case TABLET_DELETE_DONE:
              return Object.assign({}, state, {
                deleteTablet: {
                  fetching: false,
                  error: false,
                  data: action.data,
                }
              })
            case TABLET_DELETE_FAIL:
              return Object.assign({}, state, {
                deleteTablet: {
                  fetching: false,
                  error: action.error,
                  data: false
                }
              })
  }
  return state
}

export default tablets
