import {
  ARRIVALS_FETCH,
  ARRIVALS_FETCH_FAIL,
  ARRIVALS_FETCH_DONE,
  ARRIVALS_SET_HIDDEN_TOURS,
  ARRIVALS_SET_DATE,
  ARRIVALS_FETCH_DETAILS,
  ARRIVALS_FETCH_DETAILS_FAIL,
  ARRIVALS_FETCH_DETAILS_DONE,
} from '../constants'
import moment from 'moment-timezone'
import {formatCurrency} from '../../util'

const arrivalsDefault = {
  data: [],
  columns: ["name", "mobile", "id", "tour", "option", "tickets", "date", "language", "location", "cost", "source"],
  tours: [],
  dataByTour: {},
  hiddenTours: [],
  hiddenToursDict: {},
  fetching: false,
  error: false,
  total: 0,
}

function arrivals(state = arrivalsDefault, action) {
  switch (action.type) {
    case ARRIVALS_SET_HIDDEN_TOURS:
      let hiddenToursDict = {}
      action.hiddenTours.forEach(v => hiddenToursDict[v] = true)
      return Object.assign({}, state, {
        hiddenToursDict,
        hiddenTours: action.hiddenTours
      })

    case ARRIVALS_FETCH:
      return Object.assign({}, state, {  }, {
         data: [],
         tours: [],
         dataByTour: {},
         fetching: true,
         error: false,
         total: 0,
      })
    case ARRIVALS_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
    case ARRIVALS_FETCH_DONE:
      let tours = [],
          dataByTour = {}

      action.data.forEach(v => {
        let supplierTourCode = v.supplierTourCode != '' ? v.supplierTourCode : 'No Tour Code'
        
        if (!dataByTour[supplierTourCode]) {          
          dataByTour[supplierTourCode] = []
          tours.push(supplierTourCode)
        }

        dataByTour[supplierTourCode].push(v)
      })

      return Object.assign({}, state, {
        fetching: false,
        error: false,
        data: action.data,        
        dataByTour,
        tours,
        total: action.data.total ? action.data.total : action.data.length,
      })

    case ARRIVALS_FETCH_DETAILS:
      return Object.assign({}, state, {
         detailData: false,
         detailFetching: true,
         detailError: false,
      })
    case ARRIVALS_FETCH_DETAILS_FAIL:
      return Object.assign({}, state, {
        detailData: false,
        detailFetching: false,
        detailError: action.error,
      })
    case ARRIVALS_FETCH_DETAILS_DONE:
      return Object.assign({}, state, {
        detailData: action.data,
        detailError: false,
        detailFetching: false,
      })
  }
  return state
}
export default arrivals
