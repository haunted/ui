import {
  LIST_ACCOUNT_GROUPS_FETCH,
  LIST_ACCOUNT_GROUPS_DONE,
  LIST_ACCOUNT_GROUPS_FAIL,
} from '../../constants'

const listGroupsState = {
  fetching: false,
  error: false,
  data: []
}

function listGroups(state = listGroupsState, action) {
  switch (action.type) {
    case LIST_ACCOUNT_GROUPS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
        data: []
      })
    case LIST_ACCOUNT_GROUPS_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data.groups,
      })
    case LIST_ACCOUNT_GROUPS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listGroups
