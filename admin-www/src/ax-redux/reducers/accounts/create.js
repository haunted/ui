import {
  CREATE_ACCOUNT_FETCH,
  CREATE_ACCOUNT_DONE,
  CREATE_ACCOUNT_FAIL,
  ACCOUNT_SET_INITIAL_GROUPS
} from '../../constants'

const createState = {
  fetching: false,
  error: false,
  data: {},
  initialGroups: []
}

function create(state = createState, action) {
  switch (action.type) {
    case CREATE_ACCOUNT_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CREATE_ACCOUNT_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CREATE_ACCOUNT_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
     case ACCOUNT_SET_INITIAL_GROUPS:
       return Object.assign({}, state, {
           initialGroups: action.groups
       })
  }
  return state
}

export default create
