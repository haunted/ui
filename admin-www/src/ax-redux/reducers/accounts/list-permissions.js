import {
  LIST_ACCOUNT_PERMISSIONS_FETCH,
  LIST_ACCOUNT_PERMISSIONS_DONE,
  LIST_ACCOUNT_PERMISSIONS_FAIL,
} from '../../constants'

const listPermissionsState = {
  fetching: false,
  error: false,
  data: {},
}

function listPermissions(state = listPermissionsState, action) {
  switch (action.type) {
    case LIST_ACCOUNT_PERMISSIONS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case LIST_ACCOUNT_PERMISSIONS_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case LIST_ACCOUNT_PERMISSIONS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listPermissions
