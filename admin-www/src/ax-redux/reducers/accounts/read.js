import {
  READ_ACCOUNTS_FETCH,
  READ_ACCOUNTS_DONE,
  READ_ACCOUNTS_FAIL,
  READ_ACCOUNT_FETCH,
  READ_ACCOUNT_DONE,
  READ_ACCOUNT_FAIL
} from '../../constants'

const readState = {
  fetching: false,
  error: false,
  data: {},
  current: false
}

function read(state = readState, action) {
  switch (action.type) {
    case READ_ACCOUNTS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case READ_ACCOUNTS_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case READ_ACCOUNTS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
      case READ_ACCOUNT_FETCH:
        return Object.assign({}, state, {
          current: false,
          fetching: true,
          error: false,
        })
      case READ_ACCOUNT_DONE:
        return Object.assign({}, state, {
          fetching: false,
          current: action.data,
        })
      case READ_ACCOUNT_FAIL:
        return Object.assign({}, state, {
          fetching: false,
          error: action.error,
        })
  }
  return state
}

export default read
