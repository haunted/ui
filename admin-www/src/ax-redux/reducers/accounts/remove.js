import {
  DELETE_ACCOUNT_FETCH,
  DELETE_ACCOUNT_DONE,
  DELETE_ACCOUNT_FAIL,
} from '../../constants'

const removeState = {
  fetching: false,
  error: false,
  data: {},
}

function remove(state = removeState, action) {
  switch (action.type) {
    case DELETE_ACCOUNT_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case DELETE_ACCOUNT_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case DELETE_ACCOUNT_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default remove
