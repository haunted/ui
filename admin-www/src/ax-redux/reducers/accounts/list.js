import {
  LIST_ACCOUNTS_FETCH,
  LIST_ACCOUNTS_DONE,
  LIST_ACCOUNTS_FAIL,
} from '../../constants'

const listState = {
  fetching: false,
  error: false,
  data: [],
  limit: 0,
  offset: 0
}

function list(state = listState, action) {
    let doneState = {};
  switch (action.type) {
    case LIST_ACCOUNTS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case LIST_ACCOUNTS_DONE:
        doneState = {
            fetching: false,
            error: false,
            offset: 0,
            limit: 0
        };
        // if (action.limit != 0) {
        //     doneState.limit = action.limit;
        // }
        // if (action.offset != 0) {
        //     doneState.offset = action.offset;
        //     doneState.data = state.data.concat(action.data.accounts);
        // } else {
            doneState.data = action.data.accounts;
        //}
      return Object.assign({}, state, doneState)
    case LIST_ACCOUNTS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default list
