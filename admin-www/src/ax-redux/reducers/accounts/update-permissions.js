import {
  UPDATE_ACCOUNT_PERMISSIONS_FETCH,
  UPDATE_ACCOUNT_PERMISSIONS_DONE,
  UPDATE_ACCOUNT_PERMISSIONS_FAIL,
} from '../../constants'

const updatePermissionsState = {
  fetching: false,
  error: false,
  data: {},
}

function updatePermissions(state = updatePermissionsState, action) {
  switch (action.type) {
    case UPDATE_ACCOUNT_PERMISSIONS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case UPDATE_ACCOUNT_PERMISSIONS_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case UPDATE_ACCOUNT_PERMISSIONS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default updatePermissions
