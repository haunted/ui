import {combineReducers} from 'redux'
import create from './create.js'
import remove from './remove.js'
import list from './list.js'
import listPermissions from './list-permissions.js'
import read from './read.js'
import update from './update.js'
import updatePermissions from './update-permissions.js'
import listGroups from './list-groups.js'

export default combineReducers({
  create,
  remove,
  list,
  listPermissions,
  listGroups,
  read,
  update,
  updatePermissions
})
