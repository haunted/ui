import {
  UPDATE_ACCOUNT_FETCH,
  UPDATE_ACCOUNT_DONE,
  UPDATE_ACCOUNT_FAIL,
  ACCOUNT_UPDATE_PASSWORD_FETCH,
  ACCOUNT_UPDATE_PASSWORD_DONE,
  ACCOUNT_UPDATE_PASSWORD_FAIL,
} from '../../constants';

const updateState = {
  updatingPassword: false,
  fetching: false,
  error: false,
  data: {},
};

function update(state = updateState, action) {
  switch (action.type) {
    case UPDATE_ACCOUNT_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      });
    case UPDATE_ACCOUNT_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      });
    case UPDATE_ACCOUNT_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      });

    case ACCOUNT_UPDATE_PASSWORD_FETCH:
      return {
        ...state,
        updatingPassword: true,
      };

		case ACCOUNT_UPDATE_PASSWORD_DONE:
			return {
				...state,
				updatingPassword: false,
			};

		case ACCOUNT_UPDATE_PASSWORD_FAIL:
			return {
				...state,
				updatingPassword: false,
			};

    default:
      return state;
  }
}

export default update;
