import { combineReducers } from 'redux'

import {
    LIST_DEVICES_FETCH,
    LIST_DEVICES_DONE,
    LIST_DEVICES_FAIL,
    GET_DEVICES_FETCH,
    GET_DEVICES_DONE,
    GET_DEVICES_FAIL,
    UPDATE_DEVICE,
    UPDATE_DEVICE_DONE,
    UPDATE_DEVICE_FAIL,
    CREATE_DEVICE,
    CREATE_DEVICE_DONE,
    CREATE_DEVICE_FAIL,
    DEACTIVATE_DEVICE,
    DEACTIVATE_DEVICE_DONE,
    DEACTIVATE_DEVICE_FAIL,
    LIST_UNPROVISIONED_DEVICES_FETCH,
    LIST_UNPROVISIONED_DEVICES_DONE,
    LIST_UNPROVISIONED_DEVICES_FAIL,
    PROVISION_DEVICE,
    PROVISION_DEVICE_DONE,
    PROVISION_DEVICE_FAIL,
    DEPROVISION_DEVICE,
    DEPROVISION_DEVICE_DONE,
    DEPROVISION_DEVICE_FAIL,
    GET_NEXT_DEVICE_ASSET_ID,
    GET_NEXT_DEVICE_ASSET_ID_DONE,
    GET_NEXT_DEVICE_ASSET_ID_FAIL,
    DEVICE_APPEND_NOTE,
    DEVICE_APPEND_NOTE_DONE,
    DEVICE_APPEND_NOTE_FAIL,
    DEVICE_DELETE_NOTE,
    DEVICE_DELETE_NOTE_DONE,
    DEVICE_DELETE_NOTE_FAIL,
} from '../constants'

const listState = {
    fetching: false,
    error: false,
    data: [],
    devicesById: {},
    params: {
        sort: "assetID",
        status: "",
        supplierCode: "",
    },
}


function list(state = listState, action) {
    switch (action.type) {
        
        case LIST_DEVICES_FETCH:
            return Object.assign({}, state, {
                params: action.params,
                fetching: true,
                error: false,
                data: [],
            })
        case LIST_DEVICES_DONE:
            const data = action.data.devices
            const devicesById = {}
            data.map(v => {                
              devicesById[v.id] = v
            })
            return Object.assign({}, state, {
                fetching: false,
                data,
                devicesById,
            })
        case LIST_DEVICES_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
            })
    }
    return state
}

const listUnprovisionedState = {
    fetching: false,
    error: false,
    data: [],
}


function listUnprovisioned(state = listUnprovisionedState, action) {
    switch (action.type) {
        case LIST_UNPROVISIONED_DEVICES_FETCH:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: [],
            })
        case LIST_UNPROVISIONED_DEVICES_DONE:
            const data = action.data.devices
            return Object.assign({}, state, {
                fetching: false,
                data,
            })
        case LIST_UNPROVISIONED_DEVICES_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
            })
    }
    return state
}



const getState = {
    fetching: false,
    error: false,
    data: false,
    version: 0,
}

function get(state = getState, action) {
    switch (action.type) {
        
        case GET_DEVICES_FETCH:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: false,
            })
        case GET_DEVICES_DONE:
            return Object.assign({}, state, {
                fetching: false,
                data: action.data,
                version: state.version + 1,                
            })
        case GET_DEVICES_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
                version: state.version + 1,
            })
    }
    return state
}

const updateState = {
    fetching: false,
    error: false,
    data: false,
    version: 0,
}

function update(state = updateState, action) {
    switch (action.type) {
        
        case UPDATE_DEVICE:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: false,
            })
        case UPDATE_DEVICE_DONE:
            return Object.assign({}, state, {
                fetching: false,
                data: action.data,
                version: state.version + 1,                
            })
        case UPDATE_DEVICE_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
                version: state.version + 1,
            })
    }
    return state
}

const createState = {
    fetching: false,
    error: false,
    data: false,
    version: 0,
}

function create(state = createState, action) {
    switch (action.type) {
        
        case CREATE_DEVICE:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: false,
            })
        case CREATE_DEVICE_DONE:
            return Object.assign({}, state, {
                fetching: false,
                data: action.data,
                version: state.version + 1,                
            })
        case CREATE_DEVICE_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
                version: state.version + 1,
            })
    }
    return state
}

const deactivateState = {
    fetching: false,
    error: false,
    data: false,
    version: 0,
}

function deactivate(state = deactivateState, action) {
    switch (action.type) {
        
        case DEACTIVATE_DEVICE:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: false,
            })
        case DEACTIVATE_DEVICE_DONE:
            return Object.assign({}, state, {
                fetching: false,
                data: action.data,
                version: state.version + 1,                
            })
        case DEACTIVATE_DEVICE_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
                version: state.version + 1,
            })
    }
    return state
}


const provisionState = {
    fetching: false,
    error: false,
    data: false,
    version: 0,
}

function provision(state = provisionState, action) {
    switch (action.type) {
        
        case PROVISION_DEVICE:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: false,
            })
        case PROVISION_DEVICE_DONE:
            return Object.assign({}, state, {
                fetching: false,
                data: action.data,
                version: state.version + 1,                
            })
        case PROVISION_DEVICE_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
                version: state.version + 1,
            })
    }
    return state
}

const deprovisionState = {
    fetching: false,
    error: false,
    data: false,
    version: 0,
}

function deprovision(state = deprovisionState, action) {
    switch (action.type) {
        
        case DEPROVISION_DEVICE:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: false,
            })
        case DEPROVISION_DEVICE_DONE:
            return Object.assign({}, state, {
                fetching: false,
                data: action.data,
                version: state.version + 1,                
            })
        case DEPROVISION_DEVICE_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
                version: state.version + 1,
            })
    }
    return state
}

const appendNoteState = {
    fetching: false,
    error: false,
    data: false,
    version: 0,
}
function appendNote(state = appendNoteState, action) {
    switch (action.type) {
        
        case DEVICE_APPEND_NOTE:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: false,
            })
        case DEVICE_APPEND_NOTE_DONE:
            return Object.assign({}, state, {
                fetching: false,
                data: action.data,
                version: state.version + 1,                
            })
        case DEVICE_APPEND_NOTE_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
                version: state.version + 1,
            })
    }
    return state
}

const deleteNoteState = {
    fetching: false,
    error: false,
    data: false,
    version: 0,
}
function deleteNote(state = deleteNoteState, action) {
    switch (action.type) {
        case DEVICE_DELETE_NOTE:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
            })
        case DEVICE_DELETE_NOTE_DONE:
            return Object.assign({}, state, {
                fetching: false,
                version: state.version + 1,                
            })
        case DEVICE_DELETE_NOTE_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
                version: state.version + 1,
            })
    }
    return state
}

const nextAssetIdState = {
    fetching: false,
    error: false,
    data: false,
}

function nextAssetId(state = nextAssetIdState, action) {
    switch (action.type) {
        case GET_NEXT_DEVICE_ASSET_ID:
            return Object.assign({}, state, {
                fetching: true,
                error: false,
                data: false,
            })
        case GET_NEXT_DEVICE_ASSET_ID_DONE:
            return Object.assign({}, state, {
                fetching: false,
                data: action.data,
            })
        case GET_NEXT_DEVICE_ASSET_ID_FAIL:
            return Object.assign({}, state, {
                fetching: false,
                error: action.error,
            })
    }
    return state
}


export default combineReducers({
    list,
    listUnprovisioned,
    get,
    update,
    create,
    deactivate,
    provision,
    deprovision,
    nextAssetId,
    appendNote,
    deleteNote,
})
