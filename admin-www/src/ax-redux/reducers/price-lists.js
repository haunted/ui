import {
  PRICE_LIST_FETCH,
  PRICE_LIST_FETCH_DONE,
  PRICE_LIST_FETCH_FAIL,
  PRICE_LISTS_FETCH,
  PRICE_LISTS_FETCH_DONE,
  PRICE_LISTS_FETCH_FAIL,
  PRICE_LIST_CREATE_FETCH,
  PRICE_LIST_CREATE_DONE,
  PRICE_LIST_CREATE_FAIL,
  PRICE_LIST_UPDATE_FETCH,
  PRICE_LIST_UPDATE_DONE,
  PRICE_LIST_UPDATE_FAIL,
  PRICE_LIST_DISABLE_FETCH,
  PRICE_LIST_DISABLE_DONE,
  PRICE_LIST_DISABLE_FAIL,
  PRICE_LIST_ITEMS_FETCH,
  PRICE_LIST_ITEMS_FETCH_FAIL,
  PRICE_LIST_ITEMS_FETCH_DONE,
  PRICE_LIST_ITEM_FETCH,
  PRICE_LIST_ITEM_FETCH_DONE,
  PRICE_LIST_ITEM_FETCH_FAIL,
  PRICE_LIST_ITEM_CREATE_FETCH,
  PRICE_LIST_ITEM_CREATE_DONE,
  PRICE_LIST_ITEM_CREATE_FAIL,
  PRICE_LIST_ITEM_UPDATE_FETCH,
  PRICE_LIST_ITEM_UPDATE_DONE,
  PRICE_LIST_ITEM_UPDATE_FAIL,
  PRICE_LIST_ITEM_DISABLE_FETCH,
  PRICE_LIST_ITEM_DISABLE_DONE,
  PRICE_LIST_ITEM_DISABLE_FAIL,
  CATALOG_ITEM_PRICE_LISTS_FETCH,
  CATALOG_ITEM_PRICE_LISTS_FETCH_DONE,
  CATALOG_ITEM_PRICE_LISTS_FETCH_FAIL,
  PRICE_LIST_ITEMS_FILTER_UPDATE,
  PRICE_LISTS_FILTER_UPDATE
} from '../constants'

const priceListsState = {
    priceListsById: {},
    listPriceLists: {
        fetching: false,
        error: false,
        data: false
    },
    listPriceListItems: {
        fetching: false,
        error: false,
        data: false
    },
    getPriceList: {
        fetching: false,
        error: false,
        data: false
    },
    getPriceListItem: {
        fetching: false,
        error: false,
        data: false
    },
    createPriceList: {
        fetching: false,
        error: false,
        data: false
    },
    createPriceListItem: {
        fetching: false,
        error: false,
        data: false
    },
    updatePriceList: {
        fetching: false,
        error: false,
        data: false
    },
    updatePriceListItem: {
        fetching: false,
        error: false,
        data: false
    },
    disablePriceList: {
        fetching: false,
        error: false,
        data: false
    },
    disablePriceListItem: {
        fetching: false,
        error: false,
        data: false
    },
    listPriceListsByCatalogItem: {
        fetching: false,
        error: false,
        data: false
    },
    listsFilterUpdate: {

    },
    listItemsFilterUpdate: {

    }
}

function priceLists(state = priceListsState, action) {
  let priceListsById = {},
      disabledId = -1

  switch (action.type) {
    case PRICE_LIST_FETCH:
      return Object.assign({}, state, {
        getPriceList: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case PRICE_LIST_FETCH_DONE:
      return Object.assign({}, state, {
        getPriceList: {
          fetching: false,
          error: false,
          data: action.data,
        }
      })
    case PRICE_LIST_FETCH_FAIL:
      return Object.assign({}, state, {
        getPriceList: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
     case PRICE_LIST_ITEM_FETCH:
        return Object.assign({}, state, {
          getPriceListItem: {
            fetching: true,
            error: false,
            data: false,
          }
        })
     case PRICE_LIST_ITEM_FETCH_DONE:
        return Object.assign({}, state, {
          getPriceListItem: {
            fetching: false,
            error: false,
            data: action.data,
          }
        })
     case PRICE_LIST_ITEM_FETCH_FAIL:
        return Object.assign({}, state, {
          getPriceListItem: {
            fetching: false,
            error: action.error,
            data: false
          }
        })
      case PRICE_LISTS_FETCH:
        return Object.assign({}, state, {
          listPriceLists: {
            fetching: true,
            error: false,
            data: false,
          }
        })
      case PRICE_LISTS_FETCH_DONE:
        if (!!action.data && !!action.data.priceLists) {
          action.data.priceLists.map((priceList, p) => {
            priceListsById[priceList.id] = priceList
          })
        }
        return Object.assign({}, state, {
          priceListsById,
          listPriceLists: {
            fetching: false,
            error: false,
            data: action.data,
          }
        })
      case PRICE_LISTS_FETCH_FAIL:
        return Object.assign({}, state, {
          listPriceLists: {
            fetching: false,
            error: action.error,
            data: false
          }
        })
        case CATALOG_ITEM_PRICE_LISTS_FETCH:
        return Object.assign({}, state, {
          listPriceListsByCatalogItem: {
            fetching: true,
            error: false,
            data: false,
          }
        })
      case CATALOG_ITEM_PRICE_LISTS_FETCH_DONE:
        if (!!action.data && !!action.data.priceLists) {
          action.data.priceLists.map((priceList, p) => {
            priceListsById[priceList.id] = priceList
          })
        }
        return Object.assign({}, state, {
          priceListsById,
          listPriceListsByCatalogItem: {
            fetching: false,
            error: false,
            data: action.data,
          }
        })
      case CATALOG_ITEM_PRICE_LISTS_FETCH_FAIL:
        return Object.assign({}, state, {
          listPriceListsByCatalogItem: {
            fetching: false,
            error: action.error,
            data: false
          }
        })
      case PRICE_LIST_ITEMS_FETCH:
        return Object.assign({}, state, {
          listPriceListItems: {
            fetching: true,
            error: false,
            data: false,
          }
        })
      case PRICE_LIST_ITEMS_FETCH_DONE:
        return Object.assign({}, state, {
          listPriceListItems: {
              fetching: false,
              error: false,
              data: action.data,
            }
          })
        case PRICE_LIST_ITEMS_FETCH_FAIL:
          return Object.assign({}, state, {
            listPriceListItems: {
              fetching: false,
              error: action.error,
              data: false
            }
          })
        case PRICE_LIST_CREATE_FETCH:
          return Object.assign({}, state, {
            createPriceList: {
              fetching: true,
              error: false,
              data: false,
            }
          })
        case PRICE_LIST_CREATE_DONE:
          return Object.assign({}, state, {
            createPriceList: {
              fetching: false,
              error: false,
              data: action.data,
            }
          })
        case PRICE_LIST_CREATE_FAIL:
          return Object.assign({}, state, {
            createPriceList: {
              fetching: false,
              error: action.error,
              data: false
            }
          })
        case PRICE_LIST_ITEM_CREATE_FETCH:
          return Object.assign({}, state, {
            createPriceListItem: {
              fetching: true,
              error: false,
              data: false,
            }
          })
        case PRICE_LIST_ITEM_CREATE_DONE:
          return Object.assign({}, state, {
            createPriceListItem: {
              fetching: false,
              error: false,
              data: action.data,
            }
          })
        case PRICE_LIST_ITEM_CREATE_FAIL:
          return Object.assign({}, state, {
            createPriceListItem: {
              fetching: false,
              error: action.error,
              data: false
            }
          })
          case PRICE_LIST_UPDATE_FETCH:
            return Object.assign({}, state, {
              updatePriceList: {
                fetching: true,
                error: false,
                data: false,
              }
            })
          case PRICE_LIST_UPDATE_DONE:
            return Object.assign({}, state, {
              updatePriceList: {
                fetching: false,
                error: false,
                data: action.data,
              }
            })
          case PRICE_LIST_UPDATE_FAIL:
            return Object.assign({}, state, {
              updatePriceList: {
                fetching: false,
                error: action.error,
                data: false
              }
            })
          case PRICE_LIST_ITEM_UPDATE_FETCH:
            return Object.assign({}, state, {
              updatePriceListItem: {
                fetching: true,
                error: false,
                data: false,
              }
            })
          case PRICE_LIST_ITEM_UPDATE_DONE:
            return Object.assign({}, state, {
              updatePriceListItem: {
                fetching: false,
                error: false,
                data: action.data,
              }
            })
          case PRICE_LIST_ITEM_UPDATE_FAIL:
            return Object.assign({}, state, {
              updatePriceListItem: {
                fetching: false,
                  error: action.error,
                  data: false
              }
            })
            case PRICE_LIST_DISABLE_FETCH:
              return Object.assign({}, state, {
                disablePriceList: {
                  fetching: true,
                  error: false,
                  data: false,
                }
              })
            case PRICE_LIST_DISABLE_DONE:
              return Object.assign({}, state, {
                disablePriceList: {
                  fetching: false,
                  error: false,
                  data: action.data,
                }
              })
            case PRICE_LIST_DISABLE_FAIL:
              return Object.assign({}, state, {
                disablePriceList: {
                  fetching: false,
                  error: action.error,
                  data: false
                }
              })
            case PRICE_LIST_ITEM_DISABLE_FETCH:
              return Object.assign({}, state, {
                disablePriceListItem: {
                  fetching: true,
                  error: false,
                  data: false,
                }
              })
            case PRICE_LIST_ITEM_DISABLE_DONE:
              return Object.assign({}, state, {
                disablePriceListItem: {
                  fetching: false,
                  error: false,
                  data: action.data,
                }
              })
            case PRICE_LIST_ITEM_DISABLE_FAIL:
              return Object.assign({}, state, {
                disablePriceListItem: {
                  fetching: false,
                  error: action.error,
                  data: false
                }
              })
          case PRICE_LIST_ITEMS_FILTER_UPDATE:
            disabledId = !!state.disablePriceListItem.data ? state.disablePriceListItem.data.priceListItem.priceListItem.id : -1
            return Object.assign({}, state, {
                listItemsFilterUpdate: {},
                listPriceListItems: Object.assign({}, state.listPriceListItems, {
                  data: {
                    items: state.listPriceListItems.data.items.filter((item) => {
                      return item.item.id != disabledId
                    })
                  }
                })
            })
          case PRICE_LISTS_FILTER_UPDATE:
            disabledId = !!state.disablePriceList.data ? state.disablePriceList.data.priceList.priceList.id : -1
            return Object.assign({}, state, {
              listsFilterUpdate: {},
              listPriceLists: Object.assign({}, state.listPriceLists, {
                data: {
                  priceLists: state.listPriceLists.data.priceLists.filter((item) => {
                    return item.id != disabledId
                  })
                }
              })
            })

  }
  return state
}

export default priceLists
