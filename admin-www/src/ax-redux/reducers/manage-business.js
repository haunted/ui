import {
  MANAGEBIZ_FETCH,
  MANAGEBIZ_FETCH_FAIL,
  MANAGEBIZ_FETCH_DONE,
} from '../constants'


const ManageBusinessDefault = {
  data: {},
  fetching: false,
  error: false,
}

function manageBusiness(state = ManageBusinessDefault, action) {
  switch (action.type) {

    case MANAGEBIZ_FETCH:
      return Object.assign({}, state, {
         data: {},
         fetching: true,
         error: false,
      })
    case MANAGEBIZ_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
    case MANAGEBIZ_FETCH_DONE:
      return Object.assign({}, state, {
        data: action.data,
        fetching: false,
        error: false,
      })
  }
  return state
}
export default manageBusiness
