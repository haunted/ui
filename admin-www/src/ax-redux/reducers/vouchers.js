import {
    VOUCHER_FETCH,
    VOUCHER_FETCH_DONE,
    VOUCHER_FETCH_FAIL,
    VOUCHERS_FETCH,
    VOUCHERS_FETCH_DONE,
    VOUCHERS_FETCH_FAIL,
    VOUCHER_EXCEPTIONS_FETCH,
    VOUCHER_EXCEPTIONS_FETCH_DONE,
    VOUCHER_EXCEPTIONS_FETCH_FAIL,
    VOUCHER_GET_IMAGE_FETCH,
    VOUCHER_GET_IMAGE_DONE,
    VOUCHER_GET_IMAGE_FAIL,
    VOUCHER_UPDATE_FETCH,
    VOUCHER_UPDATE_DONE,
    VOUCHER_UPDATE_FAIL,
    VOUCHER_REJECT_FETCH,
    VOUCHER_REJECT_DONE,
    VOUCHER_REJECT_FAIL,
    VOUCHER_CORRECT_FETCH,
    VOUCHER_CORRECT_DONE,
    VOUCHER_CORRECT_FAIL,
    VOUCHER_VOID_FETCH,
    VOUCHER_VOID_DONE,
    VOUCHER_VOID_FAIL,
    VOUCHER_CREATE_REDEEMED_FETCH,
    VOUCHER_CREATE_REDEEMED_DONE,
    VOUCHER_CREATE_REDEEMED_FAIL,
    VOUCHER_MERGE_FETCH,
    VOUCHER_MERGE_DONE,
    VOUCHER_MERGE_FAIL,
} from '../constants'

const vouchersState = {
    getVoucher: {
        fetching: false,
        error: false,
        data: false
    },
    getVouchers: {
        fetching: false,
        error: false,
        data: false
    },
    getVoucherExceptions: {
        fetching: false,
        error: false,
        data: false
    },
    getVoucherImage: {
        fetching: false,
        error: false,
        data: false
    },
    updateVoucher: {
        fetching: false,
        error: false,
        data: false
    },
    rejectVoucher: {
        fetching: false,
        error: false,
        data: false
    },
    correctVoucher: {
        fetching: false,
        error: false,
        data: false
    },
    voidVoucher: {
        fetching: false,
        error: false,
        data: false
    },
    createRedeemedVoucher: {
      fetching: false,
      error: false,
      data: false
    },
    mergeVoucher: {
        fetching: false,
        error: false,
        data: false
    },
}

function vouchers(state = vouchersState, action) {
    switch (action.type) {
        case VOUCHER_FETCH:
        return Object.assign({}, state, {
            getVoucher: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_FETCH_DONE:
        return Object.assign({}, state, {
            getVoucher: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_FETCH_FAIL:
        return Object.assign({}, state, {
            getVoucher: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHERS_FETCH:
        return Object.assign({}, state, {
            getVouchers: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHERS_FETCH_DONE:
        return Object.assign({}, state, {
            getVouchers: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHERS_FETCH_FAIL:
        return Object.assign({}, state, {
            getVouchers: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHER_EXCEPTIONS_FETCH:
        return Object.assign({}, state, {
            getVoucherExceptions: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_EXCEPTIONS_FETCH_DONE:
        return Object.assign({}, state, {
            getVoucherExceptions: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_EXCEPTIONS_FETCH_FAIL:
        return Object.assign({}, state, {
            getVoucherExceptions: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHER_GET_IMAGE_FETCH:
        return Object.assign({}, state, {
            getVoucherImage: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_GET_IMAGE_DONE:
        let resellersById = {}
        return Object.assign({}, state, {
            getVoucherImage: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_GET_IMAGE_FAIL:
        return Object.assign({}, state, {
            getVoucherImage: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHER_UPDATE_FETCH:
        return Object.assign({}, state, {
            updateVoucher: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_UPDATE_DONE:
        return Object.assign({}, state, {
            updateVoucher: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_UPDATE_FAIL:
        return Object.assign({}, state, {
            updateVoucher: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHER_REJECT_FETCH:
        return Object.assign({}, state, {
            rejectVoucher: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_REJECT_DONE:
        return Object.assign({}, state, {
            rejectVoucher: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_REJECT_FAIL:
        return Object.assign({}, state, {
            rejectVoucher: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHER_CORRECT_FETCH:
        return Object.assign({}, state, {
            correctVoucher: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_CORRECT_DONE:
        return Object.assign({}, state, {
            correctVoucher: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_CORRECT_FAIL:
        return Object.assign({}, state, {
            correctVoucher: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHER_VOID_FETCH:
        return Object.assign({}, state, {
            voidVoucher: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_VOID_DONE:
        return Object.assign({}, state, {
            voidVoucher: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_VOID_FAIL:
        return Object.assign({}, state, {
            voidVoucher: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHER_CREATE_REDEEMED_FETCH:
        return Object.assign({}, state, {
            createRedeemedVoucher: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_CREATE_REDEEMED_DONE:
        return Object.assign({}, state, {
            createRedeemedVoucher: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_CREATE_REDEEMED_FAIL:
        return Object.assign({}, state, {
            createRedeemedVoucher: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case VOUCHER_MERGE_FETCH:
        return Object.assign({}, state, {
            mergeVoucher: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case VOUCHER_MERGE_DONE:
        return Object.assign({}, state, {
            mergeVoucher: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case VOUCHER_MERGE_FAIL:
        return Object.assign({}, state, {
            mergeVoucher: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
    }
    return state
}

export default vouchers
