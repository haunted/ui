import {
  SET_REPORT_FILTERS,
  FILTER_SET_SUPPLIER_CODE,
  FILTER_SET_SUPPLIER_CODES,
  FILTER_SET_RESELLER_CODE,
  FILTER_SET_PASS_ISSUER_CODE,
  FILTER_SET_SUPPLIER_ID,
  FILTER_SET_RESELLER_ID,
  FILTER_SET_PASS_ISSUER_ID,
  FILTER_SET_SUPPLIER_CATALOG_ID,
  FILTER_SET_RESELLER_CATALOG_ID,
  FILTER_SET_PASS_ISSUER_CATALOG_ID,
  FILTER_SET_START_DATE,
  FILTER_SET_END_DATE,
  FILTER_SET_DAY,
  FILTER_SET_OPERATOR_ID,
  FILTER_SET_SORT,
  FILTER_SET_OFFSET,
  FILTER_SET_DEFAULTS_IN_TZ,
  FILTER_REFRESH,
} from '../constants'

import moment from 'moment'

const zeroDate = new Date(0)

const filterDefaults = {
  // basic filters
  tzReady: false,
  supplierCode: "",
  resellerCode: "",
  // these will be overridden by the timezone defaults
  startDate: zeroDate,
  endDate: zeroDate,
  startOfDay: zeroDate,
  endOfDay: zeroDate,
  limit: 100,
  offset: 0,
  sort: "",
  // change the version to signal the component to fetch
  version: 0,
  // pathname records the last pathname so we can know if we changed reports
  pathname: "",
  supplierCodes: [],
  passIssuerCode: "",
  resellerCatalogId: -1,
  supplierCatalogId: -1,
  passIssuerCatalogId: -1,
  adminSupplierId: -1,
  adminResellerId: -1,
  adminPassIssuerId: -1,
  supplierId: -1,
}

function assignVersion( state ) {
  if (!state.tzReady) {
    return state
  }
  // don't update version without an orgCode filter
  if (state.supplierCode == "" && state.resellerCode == "") {
    return state
  }
  // don't update version if we have a zeroDate (TZ settings not loaded yet)
  if (state.startDate == zeroDate) {
    return state
  }

  state.version++
  return state
}

function reportFilters( state = filterDefaults, action ) {

  switch (action.type) {
    case FILTER_SET_DEFAULTS_IN_TZ:
      let newState = Object.assign({}, state, {
        tzReady: true,
      })

      if (state.startDate == zeroDate) {
        newState.startOfDay = moment().tz(TZ).startOf('day').toDate()
        newState.endOfDay = moment(newState.startOfDay).tz(TZ).add(1, 'day').toDate()

        newState.startDate = moment(newState.startOfDay).tz(TZ).add(-1, 'day').toDate()
        newState.endDate = newState.endOfDay

      }
      return assignVersion(newState)

    case FILTER_SET_SUPPLIER_CODE:
      return assignVersion(Object.assign({}, state, {
         supplierCode: action.data,
      }))
    case FILTER_SET_SUPPLIER_CODES:
      return assignVersion(Object.assign({}, state, {
         supplierCodes: action.data,
      }))
    case FILTER_SET_RESELLER_CODE:
      return assignVersion(Object.assign({}, state, {
        resellerCode: action.data,
      }))
    case FILTER_SET_PASS_ISSUER_CODE:
      return assignVersion(Object.assign({}, state, {
        resellerCode: action.data,
      }))
    case FILTER_SET_SUPPLIER_ID:
      return assignVersion(Object.assign({}, state, {
        adminSupplierId: action.data,
      }))
    case FILTER_SET_RESELLER_ID:
      return assignVersion(Object.assign({}, state, {
        adminResellerId: action.data,
      }))
    case FILTER_SET_PASS_ISSUER_ID:
      return assignVersion(Object.assign({}, state, {
        adminPassIssuerId: action.data,
      }))
    case FILTER_SET_START_DATE:
      return assignVersion(Object.assign({}, state, {
        startDate: action.data,
      }))
    case FILTER_SET_END_DATE:
      return assignVersion(Object.assign({}, state, {
        endDate: action.data,
      }))
    case FILTER_SET_DAY:
      return assignVersion(Object.assign({}, state, {
        startOfDay: action.data,
        endOfDay: moment(action.data).tz(TZ).add(1, 'day').toDate(),
      }))
    case FILTER_SET_OPERATOR_ID:
      return assignVersion(Object.assign({}, state, {
        operatorId: action.data,
      }))
    case FILTER_SET_SORT:
        let sort = action.data
        if (sort == state.sort) {
            if (sort.indexOf("-") > -1) {
              sort = sort.replace("-", "");
            } else {
              sort = "-"+sort;
            }
        }
      return assignVersion(Object.assign({}, state, {
        sort,
      }))
    case FILTER_SET_OFFSET:
      return assignVersion(Object.assign({}, state, {
        offset: action.data,
      }))

    case FILTER_SET_SUPPLIER_CATALOG_ID:
      return assignVersion(Object.assign({}, state, {
        supplierCatalogId: action.data,
      }))
    case FILTER_SET_RESELLER_CATALOG_ID:
      return assignVersion(Object.assign({}, state, {
        resellerCatalogId: action.data,
      }))
    case FILTER_SET_PASS_ISSUER_CATALOG_ID:
      return assignVersion(Object.assign({}, state, {
        passIssuerCatalogId: action.data,
      }))
    case SET_REPORT_FILTERS:
      return assignVersion(Object.assign({}, state, {
        supplierCode: action.supplierCode,
        supplierCodes: action.supplierCodes,
        resellerCode: action.resellerCode,
        startDate: action.start,
        endDate: action.end,
        startOfDay: action.startOfDay,
        endOfDay: moment(action.startOfDay).tz(TZ).add(1, 'day').toDate(),
        limit: action.limit,
        offset: action.offset,
        sort: action.sort,
        operatorId: action.operatorId,
        pathname: action.pathname,
      }))
    case FILTER_REFRESH:
      return assignVersion(Object.assign({}, state, {}))
  }

  return state

}

export default reportFilters
