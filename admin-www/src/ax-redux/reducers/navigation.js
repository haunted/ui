import {
    /* remove these three once the master shift summary endpoint is here */
    NAV_SET_RETURN_TO_REDEMPTIONS_URL,
    NAV_SET_RETURN_TO_RESELLER_REDEMPTIONS_URL,
    NAV_SET_RETURN_TO_SHIFT_URL,
    NAV_SET_RETURN_TO_SHIFT_SUMMARY_URL,
    NAV_SET_RETURN_TO_FAILED_ORDERS_URL,
    NAV_SET_RETURN_TO_DEVICES_URL,
} from '../constants'

const navigationState = {
    // defaults to shift summary (but will go to shift, once we navigate through it)
    returnToRedemptionsURL: "/app/reports/redeemed",
    returnToResellerRedemptionsURL: "/app/reports/reseller-redemptions",
    returnToShiftURL: "/app/reports/shifts",
    returnToShiftSummaryURL: "/app/reports/shifts",
    returnToFailedOrdersURL: "/app/redeam/failed-orders",
    returnToDevicesURL: "/app/redeam/devices",
}

function navigation(state = navigationState, action) {
  switch (action.type) {
      case NAV_SET_RETURN_TO_REDEMPTIONS_URL:
      return Object.assign({}, state, {
          returnToRedemptionsURL: action.data,
      })
      case NAV_SET_RETURN_TO_RESELLER_REDEMPTIONS_URL:
      return Object.assign({}, state, {
        returnToResellerRedemptionsURL: action.data,
      })
    case NAV_SET_RETURN_TO_SHIFT_URL:
      return Object.assign({}, state, {
          returnToShiftURL: action.data,
      })
    case NAV_SET_RETURN_TO_SHIFT_SUMMARY_URL:
      return Object.assign({}, state, {
          returnToShiftSummaryURL: action.data,
      })
    case NAV_SET_RETURN_TO_FAILED_ORDERS_URL:
      return Object.assign({}, state, {
          returnToFailedOrdersURL: action.data,
      })
    case NAV_SET_RETURN_TO_DEVICES_URL:
      return Object.assign({}, state, {
          returnToDevicesURL: action.data,
      })
  }
  return state
}

export default navigation
