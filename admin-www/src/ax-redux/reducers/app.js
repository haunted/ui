import {
  SET_PAGE_TITLE,
  SET_SIDE_MENU_ITEMS,
  SET_SIDE_MENU_TITLE,
  LOGOUT,
  STARTUP,
  STARTUP_DONE,
  STARTUP_FAIL,
  STARTUP_FAIL_BACK,
  ME_SPOOF_USER_ROLE,
  ME_SPOOF_USER_ORG,
  APP_CHANGE_LOCALE,
  SET_REDIRECT_AFTER_LOGIN,
  TOGGLE_PRINT_MODE,
  ON_STARTUP_COMPLETE,
  FIRE_STARTUP_COMPLETE
} from '../constants'

var language = window.navigator.userLanguage || window.navigator.language;
if (language.indexOf("-") > -1) {
  language = language.split("-")[0]
}

export const appDefault = {
  tz: "America/New_York",
  pageTitle: "",
  sideMenuTitle: "",
  sideMenuItems: [],
  sideMenuOpen: false,
  startupFetching: false,
  startupError: false,
  printMode: false,
  user: {
    "account":{
      "id":"",
      "orgCode":"",
      "username":"",
      "password":"",
      "givenName":"",
      "surname":"",
      "email":"",
      "phone":""
    },
    "supplierId":"",
    "resellerId":"",
    "passIssuerId":""
  },
  role: false,
  initialRole: false,
  initialOrg: false,
  locale: language,
  redirectAfterLogin: "app/dashboard",
  startupCompleteCallback: ()=>{},
}
function app(state = appDefault, action) {
  switch (action.type) {
    case TOGGLE_PRINT_MODE:
      return Object.assign({}, state, {
        printMode: !state.printMode
      })

    case SET_PAGE_TITLE:
      return Object.assign({}, state, {
        pageTitle: action.pageTitle
      })

    case SET_SIDE_MENU_ITEMS:
      return Object.assign({}, state, {
        sideMenuItems: action.items,
        sideMenuOpen: action.items.length > 0,
      })

    case SET_SIDE_MENU_TITLE:
      return Object.assign({}, state, {
        sideMenuTitle: action.title
      })

    case ME_SPOOF_USER_ROLE:
      let initialRole = state.initialRole
      if (initialRole === false) {
        initialRole = state.role
      }
      return Object.assign({}, state, {
        role: action.role,
        initialRole
      })
    case ME_SPOOF_USER_ORG:
      let initialOrg = state.initialOrg
      if (initialOrg === false) {
        initialOrg = state.user.account.orgCode
      }
      return Object.assign({}, state, {
        user: Object.assign({}, state.user, {
          account: Object.assign({}, state.user.account, {
            orgCode: action.orgCode
          })
        }),
        initialOrg
      })
    case APP_CHANGE_LOCALE:
      return Object.assign({}, state, {
        locale: action.locale
      })
    case STARTUP_FAIL_BACK:
      return Object.assign({}, state, {
        startupError: false,
      })

    case STARTUP:
      return Object.assign({}, state, {
        startupFetching: true,
        startupError: false,
      })

    case STARTUP_DONE:
      let role = action.role,
          user = !!action.me.account ? action.me :
          {account: action.me, supplierId: "", resellerId: "", passIssuerId: ""}

      return Object.assign({}, state, {
        startupFetching: false,
        user,
        role,
      })

    case STARTUP_FAIL:
      return Object.assign({}, state, {
        startupFetching: false,
        startupError: action.error,
      })

    case LOGOUT:
      return Object.assign({}, state, {
        role: false,
        user: false,
      })

    case SET_REDIRECT_AFTER_LOGIN:
      return Object.assign({}, state, {
        redirectAfterLogin: action.path,
      })
    case ON_STARTUP_COMPLETE:
      return Object.assign({}, state, {
        startupCompleteCallback: action.fn,
      })
    case FIRE_STARTUP_COMPLETE:
      state.startupCompleteCallback()
      return Object.assign({}, state, { })

  }
  return state
}
export default app
