import {
  REDEEMED_FETCH,
  REDEEMED_FETCH_FAIL,
  REDEEMED_FETCH_DONE,
  REDEEMED_SET_START_DATE,
  REDEEMED_SET_END_DATE,
  REDEEMED_FETCH_DETAILS,
  REDEEMED_FETCH_DETAILS_FAIL,
  REDEEMED_FETCH_DETAILS_DONE,
} from '../constants'

const defaultState = {
  data: [],
  columns: ["name", "mobile", "id", "tour", "option", "tickets", "date", "language", "location", "cost", "source"],
  startDateFilter: new Date(),
  endDateFilter: new Date(),
  fetching: false,
  error: false,
  total: 0,
  detailData: false,
  detailFetching: false,
  detailError: false,
}

function redeemed(state = defaultState, action) {
  switch (action.type) {

    case REDEEMED_SET_START_DATE:
      return Object.assign({}, state, {
        startDateFilter: action.date,
      })

    case REDEEMED_SET_END_DATE:
      return Object.assign({}, state, {
        endDateFilter: action.date,
      })

    case REDEEMED_FETCH:
      return Object.assign({}, state, {
         data: [],
         fetching: true,
         error: false,
         total: 0,
      })
    case REDEEMED_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
    case REDEEMED_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        error: false,
        data: action.data,
        total: action.data.total ? action.data.total : action.data.length,
      })
    case REDEEMED_FETCH_DETAILS:
      return Object.assign({}, state, {
         detailData: false,
         detailFetching: true,
         detailError: false,
      })
    case REDEEMED_FETCH_DETAILS_FAIL:
      return Object.assign({}, state, {
        detailData: false,
        detailFetching: false,
        detailError: action.error,
      })
    case REDEEMED_FETCH_DETAILS_DONE:
      return Object.assign({}, state, {
        detailData: action.data,
        detailError: false,
        detailFetching: false,
      })
  }
  return state
}
export default redeemed
