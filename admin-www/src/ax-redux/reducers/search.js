import {
    SEARCH_FETCH,
    SEARCH_DONE,
    SEARCH_FAIL,

} from '../constants'

const searchState = {
    getResults: {
        fetching: false,
        error: false,
        data: false
    },
}

function search(state = searchState, action) {
  switch (action.type) {
    case SEARCH_FETCH:
      return Object.assign({}, state, {
        getResults: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case SEARCH_DONE:
      return Object.assign({}, state, {
        getResults: {
          fetching: false,
          error: false,
          data: action.data,
        }
      })
    case SEARCH_FAIL:
      return Object.assign({}, state, {
        getResults: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
  }
  return state
}

export default search
