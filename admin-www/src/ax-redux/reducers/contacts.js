import {
    CONTACT_CREATE_FETCH,
    CONTACT_CREATE_DONE,
    CONTACT_CREATE_FAIL,
    CONTACTS_LIST_FETCH,
    CONTACTS_LIST_DONE,
    CONTACTS_LIST_FAIL,
    CONTACT_READ_FETCH,
    CONTACT_READ_DONE,
    CONTACT_READ_FAIL,
    CONTACT_UPDATE_FETCH,
    CONTACT_UPDATE_DONE,
    CONTACT_UPDATE_FAIL,
    CONTACT_DELETE_FETCH,
    CONTACT_DELETE_DONE,
    CONTACT_DELETE_FAIL
} from '../constants'

const contactsState = {
    listContacts: {
        fetching: false,
        error: false,
        data: false
    },
    readContact: {
        fetching: false,
        error: false,
        data: false
    },
    createContact: {
        fetching: false,
        error: false,
        data: false
    },
    updateContact: {
        fetching: false,
        error: false,
        data: false
    },
    disableContract: {
        fetching: false,
        error: false,
        data: false
    }
}

function contacts(state = contactsState, action) {
  switch (action.type) {
    case CONTACT_READ_FETCH:
      return Object.assign({}, state, {
        readContact: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case CONTACT_READ_DONE:
      return Object.assign({}, state, {
        readContact: {
          fetching: false,
          error: false,
          data: action.data,
        }
      })
    case CONTACT_READ_FAIL:
      return Object.assign({}, state, {
        readContact: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
      case CONTACTS_LIST_FETCH:
        return Object.assign({}, state, {
          listContacts: {
            fetching: true,
            error: false,
            data: false,
          }
        })
      case CONTACTS_LIST_DONE:
        let resellersById = {}
        return Object.assign({}, state, {
          listContacts: {
            fetching: false,
            error: false,
            data: action.data,
          }
        })
      case CONTACTS_LIST_FAIL:
        return Object.assign({}, state, {
          listContacts: {
            fetching: false,
            error: action.error,
            data: false
          }
        })
        case CONTACT_CREATE_FETCH:
        return Object.assign({}, state, {
            createContact: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case CONTACT_CREATE_DONE:
        return Object.assign({}, state, {
            createContact: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case CONTACT_CREATE_FAIL:
        return Object.assign({}, state, {
            createContact: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case CONTACT_UPDATE_FETCH:
        return Object.assign({}, state, {
            updateContact: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case CONTACT_UPDATE_DONE:
        return Object.assign({}, state, {
            updateContact: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case CONTACT_UPDATE_FAIL:
        return Object.assign({}, state, {
            updateContact: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case CONTACT_DELETE_FETCH:
        return Object.assign({}, state, {
            disableContract: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case CONTACT_DELETE_DONE:
        return Object.assign({}, state, {
            disableContract: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case CONTACT_DELETE_FAIL:
        return Object.assign({}, state, {
            disableContract: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
  }
  return state
}

export default contacts
