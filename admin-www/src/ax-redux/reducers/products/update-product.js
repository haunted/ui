import {
  PRODUCT_UPDATE_FETCH,
  PRODUCT_UPDATE_DONE,
  PRODUCT_UPDATE_FAIL
} from '../../constants'

const updateProductState = {
  fetching: false,
  error: false,
  data: false
}

function updateProduct(state = updateProductState, action) {
  switch (action.type) {
    case PRODUCT_UPDATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PRODUCT_UPDATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PRODUCT_UPDATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default updateProduct
