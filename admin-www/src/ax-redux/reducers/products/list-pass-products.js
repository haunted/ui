import {
  PASS_PRODUCTS_LIST_FETCH,
  PASS_PRODUCTS_LIST_FETCH_DONE,
  PASS_PRODUCTS_LIST_FETCH_FAIL
} from '../../constants'

const listPassProductsState = {
  fetching: false,
  error: false,
  data: false
}

function listPassProducts(state = listPassProductsState, action) {
  switch (action.type) {
    case PASS_PRODUCTS_LIST_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PASS_PRODUCTS_LIST_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PASS_PRODUCTS_LIST_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listPassProducts
