import {
  PASS_PRODUCT_UPDATE_FETCH,
  PASS_PRODUCT_UPDATE_DONE,
  PASS_PRODUCT_UPDATE_FAIL
} from '../../constants'

const updatePassProductState = {
  fetching: false,
  error: false,
  data: false
}

function updatePassProduct(state = updatePassProductState, action) {
  switch (action.type) {
    case PASS_PRODUCT_UPDATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PASS_PRODUCT_UPDATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PASS_PRODUCT_UPDATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default updatePassProduct
