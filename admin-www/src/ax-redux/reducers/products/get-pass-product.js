import {
  PASS_PRODUCT_GET_FETCH,
  PASS_PRODUCT_GET_DONE,
  PASS_PRODUCT_GET_FAIL
} from '../../constants'

const getPassProductState = {
  fetching: false,
  error: false,
  data: false
}

function getPassProduct(state = getPassProductState, action) {
  switch (action.type) {
    case PASS_PRODUCT_GET_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PASS_PRODUCT_GET_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PASS_PRODUCT_GET_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default getPassProduct
