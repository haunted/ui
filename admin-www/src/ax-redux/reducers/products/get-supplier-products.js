import {
  SUPPLIER_PRODUCTS_FETCH,
  SUPPLIER_PRODUCTS_FETCH_DONE,
  SUPPLIER_PRODUCTS_FETCH_FAIL
} from '../../constants'

const getSupplierProductsState = {
  fetching: false,
  error: false,
  data: false
}

function getSupplierProducts(state = getSupplierProductsState, action) {
  switch (action.type) {
    case SUPPLIER_PRODUCTS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case SUPPLIER_PRODUCTS_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case SUPPLIER_PRODUCTS_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default getSupplierProducts
