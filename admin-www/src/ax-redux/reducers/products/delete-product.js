import {
  PRODUCT_DELETE_FETCH,
  PRODUCT_DELETE_DONE,
  PRODUCT_DELETE_FAIL
} from '../../constants'

const deleteProductState = {
  fetching: false,
  error: false,
  data: false
}

function deleteProduct(state = deleteProductState, action) {
  switch (action.type) {
    case PRODUCT_DELETE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PRODUCT_DELETE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PRODUCT_DELETE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default deleteProduct
