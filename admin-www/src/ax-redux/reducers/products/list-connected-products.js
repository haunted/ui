import {
  CONNECTED_PRODUCTS_LIST_FETCH,
  CONNECTED_PRODUCTS_LIST_DONE,
  CONNECTED_PRODUCTS_LIST_FAIL
} from '../../constants'

const listConnectedProductsState = {
  fetching: false,
  error: false,
  data: false
}

function listConnectedProducts(state = listConnectedProductsState, action) {
  switch (action.type) {
    case CONNECTED_PRODUCTS_LIST_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CONNECTED_PRODUCTS_LIST_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CONNECTED_PRODUCTS_LIST_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listConnectedProducts
