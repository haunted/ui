import {
  PENDING_PRODUCTS_LIST_FETCH,
  PENDING_PRODUCTS_LIST_DONE,
  PENDING_PRODUCTS_LIST_FAIL
} from '../../constants'

const listPendingProductsState = {
  fetching: false,
  error: false,
  data: false
}

function listPendingProducts(state = listPendingProductsState, action) {
  switch (action.type) {
    case PENDING_PRODUCTS_LIST_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PENDING_PRODUCTS_LIST_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PENDING_PRODUCTS_LIST_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listPendingProducts
