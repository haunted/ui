import {
  PRODUCTS_FETCH,
  PRODUCTS_FETCH_DONE,
  PRODUCTS_FETCH_FAIL
} from '../../constants'

const createPassProductState = {
  fetching: false,
  error: false,
  data: false
}

function createPassProduct(state = createPassProductState, action) {
  switch (action.type) {
    case PRODUCTS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PRODUCTS_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PRODUCTS_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default createPassProduct
