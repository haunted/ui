import {
  CONNECTED_PRODUCT_EDIT_FETCH,
  CONNECTED_PRODUCT_EDIT_DONE,
  CONNECTED_PRODUCT_EDIT_FAIL
} from '../../constants'

const editConnectedProductState = {
  fetching: false,
  error: false,
  data: false
}

function editConnectedProduct(state = editConnectedProductState, action) {
  switch (action.type) {
    case CONNECTED_PRODUCT_EDIT_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CONNECTED_PRODUCT_EDIT_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CONNECTED_PRODUCT_EDIT_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default editConnectedProduct
