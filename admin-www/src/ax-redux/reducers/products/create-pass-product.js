import {
  PASS_PRODUCT_CREATE_FETCH,
  PASS_PRODUCT_CREATE_DONE,
  PASS_PRODUCT_CREATE_FAIL
} from '../../constants'

const createPassProductState = {
  fetching: false,
  error: false,
  data: false
}

function createPassProduct(state = createPassProductState, action) {
  switch (action.type) {
    case PASS_PRODUCT_CREATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PASS_PRODUCT_CREATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PASS_PRODUCT_CREATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default createPassProduct
