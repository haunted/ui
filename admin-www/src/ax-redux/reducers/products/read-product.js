import {
  PRODUCT_FETCH,
  PRODUCT_FETCH_DONE,
  PRODUCT_FETCH_FAIL
} from '../../constants'

const readProductState = {
  fetching: false,
  error: false,
  data: false
}

function readProduct(state = readProductState, action) {
  switch (action.type) {
    case PRODUCT_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PRODUCT_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PRODUCT_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default readProduct
