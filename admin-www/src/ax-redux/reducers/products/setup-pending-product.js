import {
  PENDING_PRODUCT_SETUP_FETCH,
  PENDING_PRODUCT_SETUP_DONE,
  PENDING_PRODUCT_SETUP_FAIL
} from '../../constants'

const setupPendingProductState = {
  fetching: false,
  error: false,
  data: false
}

function setupPendingProduct(state = setupPendingProductState, action) {
  switch (action.type) {
    case PENDING_PRODUCT_SETUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PENDING_PRODUCT_SETUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PENDING_PRODUCT_SETUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default setupPendingProduct
