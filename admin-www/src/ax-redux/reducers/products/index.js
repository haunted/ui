import {combineReducers} from 'redux'
import listProducts from './list-products'
import listPassProducts from './list-pass-products'
import getSupplierProducts from './get-supplier-products'
import readProduct from './read-product'
import getPassProduct from './get-pass-product'
import createProduct from './create-product'
import createPassProduct from './create-pass-product'
import updateProduct from './update-product'
import updatePassProduct from './update-pass-product'
import deleteProduct from './delete-product'
import listConnectedProducts from './list-connected-products'
import listPendingProducts from './list-pending-products'
import getConnectedProduct from './get-connected-product'
import getPendingProduct from './get-pending-product'
import editConnectedProduct from './edit-connected-product'
import setupPendingProduct from './setup-pending-product'

export default combineReducers({
  listProducts,
  listPassProducts,
  getSupplierProducts,
  readProduct,
  getPassProduct,
  createProduct,
  createPassProduct,
  updateProduct,
  updatePassProduct,
  deleteProduct,
  listConnectedProducts,
  listPendingProducts,
  getConnectedProduct,
  getPendingProduct,
  editConnectedProduct,
  setupPendingProduct
})
