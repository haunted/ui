import {
  PENDING_PRODUCT_FETCH,
  PENDING_PRODUCT_FETCH_DONE,
  PENDING_PRODUCT_FETCH_FAIL
} from '../../constants'

const getPendingProductState = {
  fetching: false,
  error: false,
  data: false
}

function getPendingProduct(state = getPendingProductState, action) {
  switch (action.type) {
    case PENDING_PRODUCT_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case PENDING_PRODUCT_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case PENDING_PRODUCT_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default getPendingProduct
