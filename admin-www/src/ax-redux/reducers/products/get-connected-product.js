import {
  CONNECTED_PRODUCT_FETCH,
  CONNECTED_PRODUCT_FETCH_DONE,
  CONNECTED_PRODUCT_FETCH_FAIL
} from '../../constants'

const getConnectedProductState = {
  fetching: false,
  error: false,
  data: false
}

function getConnectedProduct(state = getConnectedProductState, action) {
  switch (action.type) {
    case CONNECTED_PRODUCT_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CONNECTED_PRODUCT_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CONNECTED_PRODUCT_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default getConnectedProduct
