import {
  EXCEPTIONS_FETCH,
  EXCEPTIONS_FETCH_FAIL,
  EXCEPTIONS_FETCH_DONE,
  EXCEPTIONS_SET_START_DATE,
  EXCEPTIONS_SET_END_DATE,
  EXCEPTIONS_FETCH_DETAILS,
  EXCEPTIONS_FETCH_DETAILS_FAIL,
  EXCEPTIONS_FETCH_DETAILS_DONE,
} from '../constants'
import moment from 'moment-timezone'
import {formatCurrency} from '../../util'


const defaultState = {
  data: [],
  columns: ["name", "mobile", "id", "tour", "option", "tickets", "date", "language", "location", "cost", "source"],
  startDateFilter: new Date(),
  endDateFilter: new Date(),
  fetching: false,
  error: false,
  total: 0,
}

function exceptions(state = defaultState, action) {
  switch (action.type) {
    case EXCEPTIONS_SET_START_DATE:
      return Object.assign({}, state, {
        startDateFilter: action.date,
      })
    case EXCEPTIONS_SET_END_DATE:
      return Object.assign({}, state, {
        endDateFilter: action.date,
      })
    case EXCEPTIONS_FETCH:
      return Object.assign({}, state, {  }, {
         data: [],
         fetching: true,
         error: false,
         total: 0,
      })
    case EXCEPTIONS_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
    case EXCEPTIONS_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        error: false,
        data: action.data,
        total: action.data.total ? action.data.total : action.data.length,
      })

    case EXCEPTIONS_FETCH_DETAILS:
      return Object.assign({}, state, {
         detailData: false,
         detailFetching: true,
         detailError: false,
      })
    case EXCEPTIONS_FETCH_DETAILS_FAIL:
      return Object.assign({}, state, {
        detailData: false,
        detailFetching: false,
        detailError: action.error,
      })
    case EXCEPTIONS_FETCH_DETAILS_DONE:
      return Object.assign({}, state, {
        detailData: action.data,
        detailError: false,
        detailFetching: false,
      })
  }
  return state
}
export default exceptions
