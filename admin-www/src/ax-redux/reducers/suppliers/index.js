import {combineReducers} from 'redux'

import create from './create'
import update from './update'
import list from './list'
import get from './get'
import disable from './disable'
import createCatalog from './create-catalog'
import updateCatalog from './update-catalog'
import listCatalogs from './list-catalogs'
import getCatalog from './get-catalog'
import disableCatalog from './disable-catalog'
import createCatalogItem from './create-catalog-item'
import updateCatalogItem from './update-catalog-item'
import listCatalogItems from './list-catalog-items'
import listResellerCatalogItems from './list-reseller-catalog-items'
import getCatalogItem from './get-catalog-item'
import disableCatalogItem from './disable-catalog-item'

export default combineReducers({
  updateCatalogItem,
  updateCatalog,
  update,
  listResellerCatalogItems,  
  listCatalogs,
  listCatalogItems,
  list,
  getCatalogItem,
  getCatalog,
  get,
  disableCatalogItem,
  disableCatalog,
  disable,
  createCatalogItem,
  createCatalog,
  create,

})
