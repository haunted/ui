import {
  LIST_SUPPLIERS_FETCH,
  LIST_SUPPLIERS_DONE,
  LIST_SUPPLIERS_FAIL
} from '../../constants'

const listState = {
  fetching: false,
  error: false,
  data: false,
  suppliersById: {},
  suppliersByCode: {},
}

function list(state = listState, action) {
  switch (action.type) {
    case LIST_SUPPLIERS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case LIST_SUPPLIERS_DONE:
      let suppliersById = {}
      let suppliersByCode = {}
      action.data.suppliers.forEach(v => { 
        suppliersById[v.supplier.id] = v 
        suppliersByCode[v.supplier.code] = v 
      })
      action.data.suppliers.sort((a, b) => {
        const c = a.supplier.name.localeCompare(b.supplier.name)
        if (c == 0) {
          return a.supplier.code.localeCompare(b.supplier.code)
        }
        return c
      })
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
        suppliersById,
        suppliersByCode,
      })
    case LIST_SUPPLIERS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default list
