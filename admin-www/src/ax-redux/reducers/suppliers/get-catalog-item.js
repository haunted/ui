import {
  SUPPLIER_CATALOG_ITEM_FETCH,
  SUPPLIER_CATALOG_ITEM_FETCH_DONE,
  SUPPLIER_CATALOG_ITEM_FETCH_FAIL
} from '../../constants'

const getCatalogItemState = {
  fetching: false,
  error: false,
  data: false
}

function getCatalogItem(state = getCatalogItemState, action) {
  switch (action.type) {
    case SUPPLIER_CATALOG_ITEM_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case SUPPLIER_CATALOG_ITEM_FETCH_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case SUPPLIER_CATALOG_ITEM_FETCH_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default getCatalogItem
