import {
  SUPPLIER_CATALOG_ITEM_UPDATE_FETCH,
  SUPPLIER_CATALOG_ITEM_UPDATE_DONE,
  SUPPLIER_CATALOG_ITEM_UPDATE_FAIL
} from '../../constants'

const updateCatalogItemState = {
  fetching: false,
  error: false,
  data: false
}

function updateCatalogItem(state = updateCatalogItemState, action) {
  switch (action.type) {
    case SUPPLIER_CATALOG_ITEM_UPDATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case SUPPLIER_CATALOG_ITEM_UPDATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case SUPPLIER_CATALOG_ITEM_UPDATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default updateCatalogItem
