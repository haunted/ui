import {
  SUPPLIER_CATALOGS_LIST_FETCH,
  SUPPLIER_CATALOGS_LIST_DONE,
  SUPPLIER_CATALOGS_LIST_FAIL
} from '../../constants'

const listCatalogsState = {
  fetching: false,
  error: false,
  data: false
}

function listCatalogs(state = listCatalogsState, action) {
  switch (action.type) {
    case SUPPLIER_CATALOGS_LIST_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case SUPPLIER_CATALOGS_LIST_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case SUPPLIER_CATALOGS_LIST_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listCatalogs
