import {
  SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_FETCH,
  SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_DONE,
  SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_FAIL
} from '../../constants'

const listCatalogItemsState = {
  fetching: false,
  error: false,
  data: false
}

function listResellerCatalogItems(state = listCatalogItemsState, action) {
  switch (action.type) {
    case SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case SUPPLIER_RESELLER_CATALOG_ITEMS_LIST_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listResellerCatalogItems
