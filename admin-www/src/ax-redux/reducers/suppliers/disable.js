import {
  DISABLE_SUPPLIER_FETCH,
  DISABLE_SUPPLIER_DONE,
  DISABLE_SUPPLIER_FAIL
} from '../../constants'

const disableState = {
  fetching: false,
  error: false,
  data: false
}

function disable(state = disableState, action) {
  switch (action.type) {
    case DISABLE_SUPPLIER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case DISABLE_SUPPLIER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case DISABLE_SUPPLIER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default disable
