import {
  GET_SUPPLIER_FETCH,
  GET_SUPPLIER_DONE,
  GET_SUPPLIER_FAIL
} from '../../constants'

const getState = {
  fetching: false,
  error: false,
  data: false
}

function get(state = getState, action) {
  switch (action.type) {
    case GET_SUPPLIER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case GET_SUPPLIER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case GET_SUPPLIER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default get
