import {
  UPDATE_SUPPLIER_FETCH,
  UPDATE_SUPPLIER_DONE,
  UPDATE_SUPPLIER_FAIL
} from '../../constants'

const updateState = {
  fetching: false,
  error: false,
  data: false
}

function update(state = updateState, action) {
  switch (action.type) {
    case UPDATE_SUPPLIER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case UPDATE_SUPPLIER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case UPDATE_SUPPLIER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default update
