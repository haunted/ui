import {
  SUPPLIER_CATALOG_CREATE_FETCH,
  SUPPLIER_CATALOG_CREATE_DONE,
  SUPPLIER_CATALOG_CREATE_FAIL
} from '../../constants'

const createCatalogState = {
  fetching: false,
  error: false,
  data: false
}

function createCatalog(state = createCatalogState, action) {
  switch (action.type) {
    case SUPPLIER_CATALOG_CREATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case SUPPLIER_CATALOG_CREATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case SUPPLIER_CATALOG_CREATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default createCatalog
