import {
  CREATE_SUPPLIER_FETCH,
  CREATE_SUPPLIER_DONE,
  CREATE_SUPPLIER_FAIL
} from '../../constants'

const createState = {
  fetching: false,
  error: false,
  data: false
}

function create(state = createState, action) {
  switch (action.type) {
    case CREATE_SUPPLIER_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CREATE_SUPPLIER_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CREATE_SUPPLIER_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default create
