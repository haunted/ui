import {
  SUPPLIER_CATALOG_ITEM_CREATE_FETCH,
  SUPPLIER_CATALOG_ITEM_CREATE_DONE,
  SUPPLIER_CATALOG_ITEM_CREATE_FAIL
} from '../../constants'

const createCatalogItemState = {
  fetching: false,
  error: false,
  data: false
}

function createCatalogItem(state = createCatalogItemState, action) {
  switch (action.type) {
    case SUPPLIER_CATALOG_ITEM_CREATE_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case SUPPLIER_CATALOG_ITEM_CREATE_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case SUPPLIER_CATALOG_ITEM_CREATE_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default createCatalogItem
