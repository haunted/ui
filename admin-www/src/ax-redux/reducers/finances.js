import {
    LIST_RESELLER_INVOICES_FETCH,
    LIST_RESELLER_INVOICES_DONE,
    LIST_RESELLER_INVOICES_FAIL,
    GET_RESELLER_INVOICES_FETCH,
    GET_RESELLER_INVOICES_DONE,
    GET_RESELLER_INVOICES_FAIL,
    GET_RESELLER_TICKET_VOLUME_FETCH,
    GET_RESELLER_TICKET_VOLUME_DONE,
    GET_RESELLER_TICKET_VOLUME_FAIL,
    GET_RESELLER_DOLLAR_VOLUME_FETCH,
    GET_RESERLLER_DOLLAR_VOLUME_DONE,
    GET_RESELLER_DOLLAR_VOLUME_FAIL,
    GET_RESELLER_TICKET_DETAIL_FETCH,
    GET_RESELLER_TICKET_DETAIL_DONE,
    GET_RESELLER_TICKET_DETAIL_FAIL
} from '../constants'

const financesState = {
  listResellerInvoices: {
    fetching: false,
    error: false,
    data: false,
  },
  getResellerInvoices: {
    fetching: false,
    error: false,
    data: false,
  },
  getResellerTicketVolume: {
    fetching: false,
    error: false,
    data: false,
  },
  getResellerDollarVolume: {
    fetching: false,
    error: false,
    data: false,
  },
  getResellerTicketDetails: {
      fetching: false,
      error: false,
      data: false,
  }
}

function finances(state = financesState, action) {
  switch (action.type) {
    case LIST_RESELLER_INVOICES_FETCH:
      return Object.assign({}, state, {
        listResellerInvoices: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case LIST_RESELLER_INVOICES_DONE:
    return Object.assign({}, state, {
      listResellerInvoices: {
        fetching: false,
        error: false,
        data: action.data,
      }
    })
    case LIST_RESELLER_INVOICES_FAIL:
      return Object.assign({}, state, {
        listResellerInvoices: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
      case GET_RESELLER_INVOICES_FETCH:
        return Object.assign({}, state, {
          getResellerInvoices: {
            fetching: true,
            error: false,
            data: false,
          }
        })
        case GET_RESELLER_INVOICES_DONE:
        return Object.assign({}, state, {
            getResellerInvoices: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case GET_RESELLER_INVOICES_FAIL:
        return Object.assign({}, state, {
            getResellerInvoices: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case GET_RESELLER_TICKET_VOLUME_FETCH:
        return Object.assign({}, state, {
            getResellerTicketVolume: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case GET_RESELLER_TICKET_VOLUME_DONE:
        return Object.assign({}, state, {
            getResellerTicketVolume: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case GET_RESELLER_TICKET_VOLUME_FAIL:
        return Object.assign({}, state, {
            getResellerTicketVolume: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case GET_RESELLER_DOLLAR_VOLUME_FETCH:
        return Object.assign({}, state, {
            getResellerDollarVolume: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case GET_RESERLLER_DOLLAR_VOLUME_DONE:
        return Object.assign({}, state, {
            getResellerDollarVolume: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case GET_RESELLER_DOLLAR_VOLUME_FAIL:
        return Object.assign({}, state, {
            getResellerDollarVolume: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case GET_RESELLER_TICKET_DETAIL_FETCH:
        return Object.assign({}, state, {
            getResellerTicketDetails: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case GET_RESELLER_TICKET_DETAIL_DONE:
        return Object.assign({}, state, {
            getResellerTicketDetails: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case GET_RESELLER_TICKET_DETAIL_FAIL:
        return Object.assign({}, state, {
            getResellerTicketDetails: {
                fetching: false,
                error: action.error,
                data: false
            }
        })



  }
  return state
}

export default finances
