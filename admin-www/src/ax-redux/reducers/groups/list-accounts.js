import {
  LIST_ACCOUNTS_BY_GROUP_FETCH,
  LIST_ACCOUNTS_BY_GROUP_DONE,
  LIST_ACCOUNTS_BY_GROUP_FAIL,
} from '../../constants'

const listAccountsState = {
  fetching: false,
  error: false,
  data: [],
}

function listAccounts(state = listAccountsState, action) {
  switch (action.type) {
    case LIST_ACCOUNTS_BY_GROUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case LIST_ACCOUNTS_BY_GROUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data.accounts,
      })
    case LIST_ACCOUNTS_BY_GROUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default listAccounts
