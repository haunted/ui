import {
  DELETE_GROUP_FETCH,
  DELETE_GROUP_DONE,
  DELETE_GROUP_FAIL,
} from '../../constants'

const removeState = {
  fetching: false,
  error: false,
  data: {},
}

function remove(state = removeState, action) {
  switch (action.type) {
    case DELETE_GROUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case DELETE_GROUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case DELETE_GROUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default remove
