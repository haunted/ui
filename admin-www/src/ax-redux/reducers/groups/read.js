import {
  READ_GROUP_FETCH,
  READ_GROUP_DONE,
  READ_GROUP_FAIL,
} from '../../constants'

const readState = {
  fetching: false,
  error: false,
  data: {},
  current: null
}

function read(state = readState, action) {
  switch (action.type) {
    case READ_GROUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case READ_GROUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        current: action.data,
      })
    case READ_GROUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default read
