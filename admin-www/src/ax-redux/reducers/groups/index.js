import {combineReducers} from 'redux'
import addAccount from './add-account.js'
import create from './create.js'
import remove from './remove.js'
import listAccounts from './list-accounts.js'
import list from './list.js'
import listPermissions from './list-permissions.js'
import read from './read.js'
import removeAccount from './remove-account.js'
import update from './update.js'
import updatePermissions from './update-permissions.js'

export default combineReducers({
  addAccount,
  create,
  remove,
  listAccounts,
  list,
  listPermissions,
  read,
  removeAccount,
  update,
  updatePermissions,
})
