import {
  ADD_ACCOUNT_TO_GROUP_FETCH,
  ADD_ACCOUNT_TO_GROUP_DONE,
  ADD_ACCOUNT_TO_GROUP_FAIL,
} from '../../constants'

const addAccountState = {
  fetching: false,
  error: false,
  data: {},
}

function addAccount(state = addAccountState, action) {
  switch (action.type) {
    case ADD_ACCOUNT_TO_GROUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case ADD_ACCOUNT_TO_GROUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case ADD_ACCOUNT_TO_GROUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default addAccount
