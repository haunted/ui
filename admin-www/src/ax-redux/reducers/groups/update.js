import {
  UPDATE_GROUP_FETCH,
  UPDATE_GROUP_DONE,
  UPDATE_GROUP_FAIL,
} from '../../constants'

const updateState = {
  fetching: false,
  error: false,
  data: {},
}

function update(state = updateState, action) {
  switch (action.type) {
    case UPDATE_GROUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case UPDATE_GROUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case UPDATE_GROUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default update
