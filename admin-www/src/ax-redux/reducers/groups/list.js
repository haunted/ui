import {
  LIST_GROUPS_FETCH,
  LIST_GROUPS_DONE,
  LIST_GROUPS_FAIL,
} from '../../constants'

const listState = {
  fetching: false,
  error: false,
  data: []
}

function list(state = listState, action) {
  switch (action.type) {
    case LIST_GROUPS_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case LIST_GROUPS_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data.groups,
      })
    case LIST_GROUPS_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default list
