import {
  CREATE_GROUP_FETCH,
  CREATE_GROUP_DONE,
  CREATE_GROUP_FAIL,
} from '../../constants'

const createState = {
  fetching: false,
  error: false,
  data: {},
}

function create(state = createState, action) {
  switch (action.type) {
    case CREATE_GROUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CREATE_GROUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CREATE_GROUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default create
