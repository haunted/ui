import {
  REMOVE_ACCOUNT_FROM_GROUP_FETCH,
  REMOVE_ACCOUNT_FROM_GROUP_DONE,
  REMOVE_ACCOUNT_FROM_GROUP_FAIL,
} from '../../constants'

const removeAccountState = {
  fetching: false,
  error: false,
  data: {},
}

function removeAccount(state = removeAccountState, action) {
  switch (action.type) {
    case REMOVE_ACCOUNT_FROM_GROUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case REMOVE_ACCOUNT_FROM_GROUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case REMOVE_ACCOUNT_FROM_GROUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default removeAccount
