import {
  NOTIFICATION_SHOW,
  NOTIFICATION_HIDE,
} from '../actions/notifications';

const initialState = {
  open: false,
  message: '',
  duration: 3000,
};

export default (state = initialState, { type, ...payload }) => {
  switch (type) {
    case NOTIFICATION_SHOW:
      return {
        ...state,
        ...payload,
        open: true,
      };

    case NOTIFICATION_HIDE:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};
