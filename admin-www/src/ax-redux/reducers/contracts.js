import {
    CONTRACT_CREATE_FETCH,
    CONTRACT_CREATE_DONE,
    CONTRACT_CREATE_FAIL,
    CONTRACTS_LIST_FETCH,
    CONTRACTS_LIST_DONE,
    CONTRACTS_LIST_FAIL,
    CONTRACT_READ_FETCH,
    CONTRACT_READ_DONE,
    CONTRACT_READ_FAIL,
    CONTRACT_UPDATE_FETCH,
    CONTRACT_UPDATE_DONE,
    CONTRACT_UPDATE_FAIL,
    CONTRACT_DISABLE_FETCH,
    CONTRACT_DISABLE_DONE,
    CONTRACT_DISABLE_FAIL
} from '../constants'

const ContractsState = {
    listContracts: {
        fetching: false,
        error: false,
        data: false
    },
    readContract: {
        fetching: false,
        error: false,
        data: false
    },
    createContract: {
        fetching: false,
        error: false,
        data: false
    },
    updateContract: {
        fetching: false,
        error: false,
        data: false
    },
    deleteContract: {
        fetching: false,
        error: false,
        data: false
    }
}

function Contracts(state = ContractsState, action) {
  switch (action.type) {
    case CONTRACT_READ_FETCH:
      return Object.assign({}, state, {
        readContract: {
          fetching: true,
          error: false,
          data: false,
        }
      })
    case CONTRACT_READ_DONE:
      return Object.assign({}, state, {
        readContract: {
          fetching: false,
          error: false,
          data: action.data,
        }
      })
    case CONTRACT_READ_FAIL:
      return Object.assign({}, state, {
        readContract: {
          fetching: false,
          error: action.error,
          data: false
        }
      })
      case CONTRACTS_LIST_FETCH:
        return Object.assign({}, state, {
          listContracts: {
            fetching: true,
            error: false,
            data: false,
          }
        })
      case CONTRACTS_LIST_DONE:
        let resellersById = {}
        return Object.assign({}, state, {
          listContracts: {
            fetching: false,
            error: false,
            data: action.data,
          }
        })
      case CONTRACTS_LIST_FAIL:
        return Object.assign({}, state, {
          listContracts: {
            fetching: false,
            error: action.error,
            data: false
          }
        })
        case CONTRACT_CREATE_FETCH:
        return Object.assign({}, state, {
            createContract: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case CONTRACT_CREATE_DONE:
        return Object.assign({}, state, {
            createContract: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case CONTRACT_CREATE_FAIL:
        return Object.assign({}, state, {
            createContract: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case CONTRACT_UPDATE_FETCH:
        return Object.assign({}, state, {
            updateContract: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case CONTRACT_UPDATE_DONE:
        return Object.assign({}, state, {
            updateContract: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case CONTRACT_UPDATE_FAIL:
        return Object.assign({}, state, {
            updateContract: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
        case CONTRACT_DISABLE_FETCH:
        return Object.assign({}, state, {
            deleteContract: {
                fetching: true,
                error: false,
                data: false,
            }
        })
        case CONTRACT_DISABLE_DONE:
        return Object.assign({}, state, {
            deleteContract: {
                fetching: false,
                error: false,
                data: action.data,
            }
        })
        case CONTRACT_DISABLE_FAIL:
        return Object.assign({}, state, {
            deleteContract: {
                fetching: false,
                error: action.error,
                data: false
            }
        })
  }
  return state
}

export default Contracts
