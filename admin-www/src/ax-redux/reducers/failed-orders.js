import {
    FAILED_ORDERS_FETCH,
    FAILED_ORDERS_FETCH_DONE ,
    FAILED_ORDERS_FETCH_FAIL,
    FAILED_ORDERS_DETAIL_FETCH,
    FAILED_ORDERS_DETAIL_FETCH_DONE ,
    FAILED_ORDERS_DETAIL_FETCH_FAIL,
    FAILED_ORDERS_REPROCESS_FETCH,
    FAILED_ORDERS_REPROCESS_DONE,
    FAILED_ORDERS_REPROCESS_FAIL
} from '../constants'


const failedOrdersDefault = {
    listFailedOrders: {
        data: false,
        fetching: false,
        error: false
    },
    listFailedOrdersDetail: {
        data: false,
        fetching: false,
        error: false
    },
    reprocessFailedOrders: {
        data: false,
        fetching: false,
        error: false
    }
}

function failedOrders ( state = failedOrdersDefault, action ) {
  switch (action.type) {

    case FAILED_ORDERS_FETCH:
      return Object.assign({}, state, {
         listFailedOrders: Object.assign({}, state.listFailedOrders, {
            data: false,
            fetching: true,
            error: false
         })
      })
    case FAILED_ORDERS_FETCH_DONE:
      return Object.assign({}, state, {
         listFailedOrders: Object.assign({}, state.listFailedOrders, {
            data: action.data,
            fetching: false,
            error: false
         })
      })
    case FAILED_ORDERS_FETCH_FAIL:
      return Object.assign({}, state, {
         listFailedOrders: Object.assign({}, state.listFailedOrders, {
            data: false,
            fetching: false,
            error: action.error
         })
      })
    case FAILED_ORDERS_DETAIL_FETCH:
      return Object.assign({}, state, {
         listFailedOrdersDetail: Object.assign({}, state.listFailedOrders, {
            data: false,
            fetching: true,
            error: false
         })
      })
    case FAILED_ORDERS_DETAIL_FETCH_DONE:
      return Object.assign({}, state, {
         listFailedOrdersDetail: Object.assign({}, state.listFailedOrders, {
            data: action.data,
            fetching: false,
            error: false
         })
      })
    case FAILED_ORDERS_DETAIL_FETCH_FAIL:
      return Object.assign({}, state, {
         listFailedOrdersDetail: Object.assign({}, state.listFailedOrders, {
            data: false,
            fetching: false,
            error: action.error
         })
      })
    case FAILED_ORDERS_REPROCESS_FETCH:
      return Object.assign({}, state, {
         reprocessFailedOrders: Object.assign({}, state.listFailedOrders, {
            data: false,
            fetching: true,
            error: false
         })
      })
    case FAILED_ORDERS_REPROCESS_DONE:
      return Object.assign({}, state, {
         reprocessFailedOrders: Object.assign({}, state.listFailedOrders, {
            data: action.data,
            fetching: false,
            error: false
         })
      })
    case FAILED_ORDERS_REPROCESS_FAIL:
      return Object.assign({}, state, {
         reprocessFailedOrders: Object.assign({}, state.listFailedOrders, {
            data: false,
            fetching: false,
            error: action.error
         })
      })
    
  }
  return state
}
export default failedOrders
