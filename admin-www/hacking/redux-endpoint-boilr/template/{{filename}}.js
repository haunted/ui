import {
  {{ACTION_NAME}}_FETCH,
  {{ACTION_NAME}}_DONE,
  {{ACTION_NAME}}_FAIL,
} from '../../constants'

const {{reducerName}}State = {
  fetching: false,
  error: false,
  data: {},
}

function {{reducerName}}(state = {{reducerName}}State, action) {
  switch (action.type) {
    case {{ACTION_NAME}}_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case {{ACTION_NAME}}_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case {{ACTION_NAME}}_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.error,
      })
  }
  return state
}

export default {{reducerName}}
