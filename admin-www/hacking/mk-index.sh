#!/bin/bash

TARGET_DIR="$1"

REDUCER_IMPORTS=""
REDUCER_COMBINE=""


echo "import {combineReducers} from 'redux'"

ls $TARGET_DIR -1 | while read x; do
  if [[ "$x" == "index.js" ]]; then
    continue;
  fi
  name=$(grep "export" $TARGET_DIR/$x | cut -d\  -f3)
  echo "import $name from './$x'"
done

echo -e "\nexport default combineReducers({"

ls $TARGET_DIR -1 | while read x; do
  if [[ "$x" == "index.js" ]]; then
    continue;
  fi
  name=$(grep "export" $TARGET_DIR/$x | cut -d\  -f3)
  echo "  $name",
done

echo "})"
