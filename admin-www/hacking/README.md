# hacking code-gen stuff


# redux-endpoint-boilr

This creates a fetching reducer. Use with boilr

```
cd redux-endpoint-boilr
boilr template save . reduxEndpoint
boilr template use reduxEndpoint ../../src/ax-redux/reducers/groups/

[dsont@dsont redux-endpoint-boilr]$ boilr template use reduxEndpoint /tmp/
[?] Please choose a value for "filename" [default: "create"]: create-group
[?] Please choose a value for "ACTION_NAME" [default: "CREATE_FOO"]: CREATE_GROUP
[?] Please choose a value for "reducerName" [default: "createFoo"]: createGroup
[✔] Created /tmp/boilr-use-template907105765/create-group.js
[✔] Successfully executed the project template reduxEndpoint in /tmp

[dsont@dsont redux-endpoint-boilr]$ cat /tmp/create-group.js
import {
  CREATE_GROUP_FETCH,
  CREATE_GROUP_DONE,
  CREATE_GROUP_FAIL,
} from '../../constants'

const createGroupState = {
  fetching: false,
  error: false,
  data: {},
}

function createGroup(state = createGroupState, action) {
  switch (action.type) {
    case CREATE_GROUP_FETCH:
      return Object.assign({}, state, {
        fetching: true,
        error: false,
      })
    case CREATE_GROUP_DONE:
      return Object.assign({}, state, {
        fetching: false,
        data: action.data,
      })
    case CREATE_GROUP_FAIL:
      return Object.assign({}, state, {
        fetching: false,
        error: action.data.error,
      })
  }
  return state
}

export default createGroup


```

### mk-index.sh

```bash
# this scans the directory and generates the combined reducer index.js file from the folder

[dsont@dsont hacking]$ ./mk-index.sh ../src/ax-redux/reducers/accounts/ ../src/ax-redux/reducers/accounts/index.js
import {combineReducers} from 'redux'
import create from './create.js'
import delete from './delete.js'
import list from './list.js'
import listPermissions from './list-permissions.js'
import read from './read.js'
import update from './update.js'
import updatePermissions from './update-permissions.js'

export default combineReducers({
  create,
  delete,
  list,
  listPermissions,
  read,
  update,
  updatePermissions,
})
[dsont@dsont hacking]$ ./mk-index.sh ../src/ax-redux/reducers/accounts/ > ../src/ax-redux/reducers/accounts/index.js
[dsont@dsont hacking]$ ./mk-index.sh ../src/ax-redux/reducers/groups/ > ../src/ax-redux/reducers/groups/index.js
[dsont@dsont hacking]$ ./mk-index.sh ../src/ax-redux/reducers/accounts/ > ../src/ax-redux/reducers/accounts/index.js
[dsont@dsont hacking]$ ./mk-index.sh ../src/ax-redux/reducers/groups/ > ../src/ax-redux/reducers/groups/index.js
[dsont@dsont hacking]$ ./mk-index.sh ../src/ax-redux/reducers/groups/ > ../src/ax-redux/reducers/groups/index.js
[dsont@dsont hacking]$ ./mk-index.sh ../src/ax-redux/reducers/accounts/ > ../src/ax-redux/reducers/accounts/index.js

```
