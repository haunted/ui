MK := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
PROJECT_ROOT := $(abspath $(MK)..)
export PROJECT_ROOT
OS=$(shell uname)

GO ?= $(shell which go)
DOCKER ?= $(shell which docker)
DOCKER_MACHINE ?= $(shell which docker-machine)
DOCKER_COMPOSE ?= $(shell which docker-compose)
PROTOC ?= $(shell which protoc)
GCLOUD ?= $(shell which gcloud)
JET ?= $(shell which jet)
TIME ?= $(shell which time)
YARN ?= $(shell which yarn)
REALPATH ?= $(shell which realpath)
STYLUS ?= $(shell which stylus)

GLIDE ?= $(GOPATH)/bin/glide
GINKGO ?= $(GOPATH)/bin/ginkgo
GODOTENV ?= $(GOPATH)/bin/godotenv
MIGRATE ?= $(GOPATH)/bin/migrate
STRINGER ?= $(GOPATH)/bin/stringer
BOILR ?= $(GOPATH)/bin/boilr

ifeq ($(OS),Darwin)
pre: go glide docker compose protoc machine machine-create ginkgo godotenv jet stringer realpath yarn stylus
else
pre: go glide docker compose protoc ginkgo godotenv jet stringer yarn stylus
endif

# Go Stuff #

# prerequisite: go must be installed
go: $(GO)
$(GO):
	$(error You must install Go v1.6 (or newer)! [https://golang.org/dl/])
	# brew install go

# prerequisite: glide must be installed
glide: $(GLIDE)
$(GLIDE):
	go get -u github.com/Masterminds/glide
	cd $GOPATH/src/github.com/masterminds/glide && git tag -l && git checkout tags/v0.12.2 && go install

# Docker Stuff #

# prerequisite: docker must be installed
docker: $(DOCKER)
$(DOCKER):
	$(error Go install Docker Toolbox now! [https://www.docker.com/docker-toolbox])

ifeq ($(OS),Darwin)
# prerequisite: docker-machine must be installed
machine: $(DOCKER_MACHINE)
$(DOCKER_MACHINE):
	$(error Go install Docker Toolbox now! [https://www.docker.com/docker-toolbox])
endif

# prerequisite: docker-compose must be installed
compose: $(DOCKER_COMPOSE)
$(DOCKER_COMPOSE):
	$(error Go install Docker Toolbox now! [https://www.docker.com/docker-toolbox])

# prerequisite: protoc must be installed
protoc: $(PROTOC)
$(PROTOC):
	$(error Go install protoc v3.0 (or newer) now! [https://github.com/google/protobuf/releases])

# prerequisite: protoc must be installed
gcloud: $(GCLOUD)
$(GCLOUD):
	$(error Go install Google Cloud SDK now! [https://cloud.google.com/sdk/downloads])

# prerequisite: ginkgo must be installed
ginkgo: $(GINKGO)
$(GINKGO):
	go get -u github.com/onsi/ginkgo

# prerequisite: godotenv must be installed
godotenv: $(GODOTENV)
$(GODOTENV):
	go get -u github.com/joho/godotenv/cmd/godotenv

# prerequisite: migrate must be installed
migrate: $(MIGRATE)
$(MIGRATE):
	go get -u github.com/mattes/migrate

# prerequisite: jet must be installed
jet: $(JET)
$(JET):
	$(error Go install Codeship Jet now! [https://codeship.com/documentation/docker/installation/])

# prerequesite stringer must be installed
stringer: $(STRINGER)
$(STRINGER):
	go get -u golang.org/x/tools/cmd/stringer

# prerequesite boilr must be installed
boilr: $(BOILR)
$(BOILR):
	go get -u github.com/tmrts/boilr
	boilr init

# Frontend Stuff #

# prerequisite: yarn must be installed
yarn: $(YARN)
$(YARN):
	$(error Go install yarn now! ([https://yarnpkg.com/en/docs/install]))

# prerequisite: realpath must be installed
realpath: $(REALPATH)
$(REALPATH):
	$(error Go install realpath now! [https://github.com/harto/realpath-osx])

# prerequisite: stylus must be installed
stylus: $(STYLUS)
$(STYLUS):
	$(error Go install stylus now! [https://www.npmjs.com/package/stylus])

.PHONY: pre go glide docker machine compose protoc ginkgo jet stringer boilr migrate realpath yarn stylus
