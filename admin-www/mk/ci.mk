ifdef CI
# CI should only be set for develop, staging, and production
DOCKER_REGISTRY_ORG = actourex-$(CI_BRANCH)

CI_BRANCH:=$(subst /,-,$(CI_BRANCH))
CI_COMMIT_TAG:=$(CI_NAME).$(CI_BRANCH).$(CI_COMMIT_ID)

# dockerize, tag and push when running CI
ci-dockerize: decode-dockercfg decode-codeship-json docker-login
	@echo "CI_NAME:       $(CI_NAME)"
	@echo "CI_BRANCH:     $(CI_BRANCH)"
	@echo "CI_COMMIT_ID:  $(CI_COMMIT_ID)"
	@echo "CI_COMMIT_TAG: $(CI_COMMIT_TAG)"
	make -C cmd dockerize
	make -C cmd docker-tag docker-push TAG=$(CI_COMMIT_TAG)

# only for use within codeship-steps.yml
decode-dockercfg:
	mkdir -p $(HOME)/.docker
	@echo -n $(DOCKERCFG_JSON) | base64 -d > $(HOME)/.docker/config.json
	ls -la $(HOME)/.docker/config.json
	# cat $(HOME)/.docker/config.json

decode-github-key:
	@echo "Decoding SSH key for GitHub..."
	@mkdir -p ~/.ssh
	@echo -n $(DEVOPS_ACTOUREX_GITHUB_SSH) | base64 -d > ~/.ssh/id_rsa
	chmod 600 $(HOME)/.ssh/id_rsa
	ls -la ~/.ssh
	/usr/local/bin/ssh-agent -s > ~/.ssh/agent.env
	eval `cat ~/.ssh/agent.env` && /usr/local/bin/ssh-add ~/.ssh/id_rsa && /usr/local/bin/ssh-add -l
	eval `cat ~/.ssh/agent.env` && /usr/local/bin/ssh-keyscan -t rsa github.com >> ~/.ssh/known_hosts
	cat ~/.ssh/known_hosts

# only for use within codeship-steps.yml
decode-codeship-json:
	mkdir -p $(HOME)/.docker
ifeq ($(CI_BRANCH),sandbox)
	@echo -n $(CODESHIP_ACTOUREX_SANDBOX_JSON) | base64 -d > $(HOME)/.docker/gcr-codeship.json
endif
ifeq ($(CI_BRANCH),develop)
	@echo -n $(CODESHIP_ACTOUREX_DEVELOP_JSON) | base64 -d > $(HOME)/.docker/gcr-codeship.json
endif
ifeq ($(CI_BRANCH),staging)
	@echo -n $(CODESHIP_ACTOUREX_STAGING_JSON) | base64 -d > $(HOME)/.docker/gcr-codeship.json
endif
ifeq ($(CI_BRANCH),production)
	@echo -n $(CODESHIP_ACTOUREX_PRODUCTION_JSON) | base64 -d > $(HOME)/.docker/gcr-codeship.json
endif
	ls -la $(HOME)/.docker/gcr-codeship.json

endif #CI

# encryption key for config.env.encrypted
codeship.aes:
	$(error You need a 'codeship.aes' file to encrypt/decrypt config.env...)

# encrypted version of config.env for Codeship Jet
config.env.encrypted: codeship.aes test-codeship.env
	$(JET) encrypt --key-path=codeship.aes test-codeship.env config.env.encrypted

## encrypt config.env for Codeship Jet
encrypt-config-env: config.env.encrypted

## run Codeship Jet locally
jet-steps: config.env.encrypted
	$(JET_CMD) steps --log-driver=logrus --no-cache --pull-if-present

## decrypt config.env.encrypted from Codeship Jet
# decrypt-config-env: codeship.aes
# 	$(JET) decrypt --key-path=codeship.aes config.env.encrypted config.env

.PHONY: ci-dockerize decode-dockercfg decode-gcr-codeship-json \
		jet-steps encrypt-config-env decrypt-config-env \
