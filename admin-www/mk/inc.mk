NUMCPUS ?= 11
GOLANG_VERSION ?= 1.7.1

DOCKER_MACHINE_NAME := actourex
DOCKER_MACHINE_CMD := docker-machine
DOCKER_MACHINE_DIR := ~/.docker/machine/machines/$(DOCKER_MACHINE_NAME)/

# Default to local build
CI_BRANCH ?= develop

OS=$(shell uname)

ifeq ($(OS),Linux)
USE_LOCAL_DOCKER := true
OPEN_CMD ?= xdg-open
endif

ifeq ($(OS),Darwin)
OPEN_CMD ?= open
endif

ifeq ($(USE_LOCAL_DOCKER),true)
DOCKER_MACHINE_ENV :=
endif
DOCKER_MACHINE_ENV ?= eval "$$($(DOCKER_MACHINE_CMD) env $(DOCKER_MACHINE_NAME))";
DOCKER_COMPOSE_CMD := $(DOCKER_MACHINE_ENV) $(DOCKER_COMPOSE)
DOCKER_CMD := $(DOCKER_MACHINE_ENV) $(DOCKER)

DOCKER_REGISTRY_HOST := us.gcr.io
DOCKER_REGISTRY_ORG ?= actourex-develop
ifdef CI
# CI should only be set for develop, staging, and production
DOCKER_REGISTRY_ORG = actourex-$(CI_BRANCH)
endif

CONTEXT = .
IMAGE = $(DOCKER_REGISTRY_HOST)/$(DOCKER_REGISTRY_ORG)/$(NAME)
TAG ?= latest

JET_CMD := $(DOCKER_MACHINE_ENV) $(JET)

ifndef CI
# SEE: http://jamesdolan.blogspot.com/2009/10/color-coding-makefile-output.html
NO_COLOR=\x1b[0m
OK_COLOR=\x1b[32;01m
ERROR_COLOR=\x1b[31;01m
WARN_COLOR=\x1b[33;01m
endif
OK_STRING=$(OK_COLOR)[OK]$(NO_COLOR)
ERROR_STRING=$(ERROR_COLOR)[ERRORS]$(NO_COLOR)
WARN_STRING=$(WARN_COLOR)[WARNINGS]$(NO_COLOR)
