DOCKER_MACHINE_CREATE_FLAGS ?= --driver=virtualbox \
	--virtualbox-disk-size=20000 \
	--virtualbox-cpu-count=4 \
	--virtualbox-memory=2048

## docker-machine create ...
machine-create: $(DOCKER_MACHINE_DIR)
$(DOCKER_MACHINE_DIR):
	$(DOCKER_MACHINE_CMD) create $(DOCKER_MACHINE_CREATE_FLAGS) $(DOCKER_MACHINE_NAME)

## docker-machine start ...
machine-start: machine-create
	-$(DOCKER_MACHINE_CMD) start $(DOCKER_MACHINE_NAME)

## docker-machine stop ...
machine-stop:
	-$(DOCKER_MACHINE_CMD) stop $(DOCKER_MACHINE_NAME)

## docker-machine rm ...
machine-rm:
	$(DOCKER_MACHINE_CMD) rm $(DOCKER_MACHINE_NAME)

MACHINE_IP = $$($(DOCKER_MACHINE_CMD) ip $(DOCKER_MACHINE_NAME))

## docker-machine ip ...
machine-ip:
	@echo
	@echo Docker Machine
	@echo ---------------------
	@echo "Name: $(DOCKER_MACHINE_NAME)"
	@echo "IP:   $$($(DOCKER_MACHINE_CMD) ip $(DOCKER_MACHINE_NAME))"


## docker-machine ssh ... -f -N -L ...:localhost:...
machine-port-forward:
	@echo "Exposing port $(PORT) from $(MACHINE_IP) on localhost which has these IPs:"
	@ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p'
	@echo
	$(DOCKER_MACHINE_CMD) ssh $(DOCKER_MACHINE_NAME) -f -N -L $(PORT):localhost:$(PORT)

## docker-compose build --no-cache
compose-build:
	@$(DOCKER_COMPOSE_CMD) build --no-cache $(SERVICE)

## docker-compose up --force-recreate -d
compose-up: docker-login
	@$(DOCKER_COMPOSE_CMD) up --remove-orphans --force-recreate -d

compose-up-jenkins: docker-login
	@$(DOCKER_COMPOSE_CMD) --file docker-compose-jenkins.yml up --force-recreate -d

## docker-compose ps
compose-ps:
	@$(DOCKER_COMPOSE_CMD) ps

## docker-compose stop
compose-stop:
	@$(DOCKER_COMPOSE_CMD) stop $(SERVICE)

compose-stop-jenkins:
	@$(DOCKER_COMPOSE_CMD) --file docker-compose-jenkins.yml stop $(SERVICE)

## docker-compose rm -f -v
compose-rm:
	@$(DOCKER_COMPOSE_CMD) rm -f -v $(SERVICE)

## docker-compose start
compose-start:
	@$(DOCKER_COMPOSE_CMD) start $(SERVICE)

## docker-compose restart
compose-restart:
	@$(DOCKER_COMPOSE_CMD) restart $(SERVICE)

## docker-compose logs
compose-logs:
	-@$(DOCKER_COMPOSE_CMD) logs -f $(SERVICE)

## docker version
docker-version:
	$(DOCKER_CMD) version

## docker images
docker-images:
	$(DOCKER_CMD) images

## docker-clean-containers docker-clean-images
docker-clean: docker-clean-containers docker-clean-images

## docker rm -v $(docker ps -a -q -f status=exited)
docker-clean-containers:
	@($(DOCKER_CMD) rm -v $$($(DOCKER_CMD) ps -a -q -f status=exited) ||:) &> /dev/null

## docker rmi -v $(docker images -f "dangling=true" -q)
docker-clean-images:
	@($(DOCKER_CMD) rmi $$($(DOCKER_CMD) images -f "dangling=true" -q) ||:) &> /dev/null

## docker login ...
docker-login:
ifdef CI
	$(DOCKER_CMD) login -e devops@actourex.com -u _json_key -p "`cat $(HOME)/.docker/gcr-codeship.json`" https://$(DOCKER_REGISTRY_HOST)
else
	$(DOCKER_CMD) login -e devops@actourex.com -u _json_key -p "`cat ./_ignore/codeship-actourex-$(CI_BRANCH).json`" https://$(DOCKER_REGISTRY_HOST)
endif
	@echo "Login complete."

## docker ps
docker-ps:
	@echo
	$(DOCKER_CMD) ps

## docker ps -a
docker-ps-a:
	@echo
	$(DOCKER_CMD) ps -a

## docker stats
docker-stats:
	@echo
	$(DOCKER_CMD) stats $(NAME)

CMD?=echo Usage: make docker-exec CMD="whatever you want"
docker-exec:
	@echo
	$(DOCKER_CMD) exec $(NAME) $(CMD)

## docker build ...
docker-build: .image
.image: build
	-$(DOCKER_CMD) rm --force $(NAME)
	-$(DOCKER_CMD) rmi --force $(IMAGE)
	sleep 1
	sync
	$(DOCKER_CMD) build --no-cache --rm --tag=$(IMAGE) $(CONTEXT)
	$(DOCKER_CMD) inspect -f '{{.Id}}' $(IMAGE) > .image

## docker tag ...:latest ...:$TAG (default 'latest')
docker-tag:
	$(DOCKER_CMD) tag $(IMAGE):latest $(IMAGE):$(TAG)

## docker push ...:$TAG (default 'latest')
docker-push:
ifdef CI
	$(DOCKER_CMD) push $(IMAGE):$(TAG) > /dev/null
else
ifneq ($(findstring gcr.io,$(IMAGE)),)
	$(GCLOUD) docker push $(IMAGE):$(TAG)
else
	$(DOCKER_CMD) push $(IMAGE):$(TAG)
endif
endif

netdata-run:
	@echo Starting netdata container...
	-@$(DOCKER_CMD) run --detach --cap-add SYS_PTRACE --name netdata -v /proc:/host/proc:ro -v /sys:/host/sys:ro -p 19999:19999 titpetric/netdata
	@sleep 1

ifeq ($(USE_LOCAL_DOCKER),true)
netdata-open:
	$(OPEN_CMD) http://localhost:19999
else
netdata-open:
	$(OPEN_CMD) http://$$($(DOCKER_MACHINE_CMD) ip $(DOCKER_MACHINE_NAME)):19999
endif

## start netdata container and open in browser
netdata: netdata-run netdata-open

## stop netdata container
netdata-stop:
	@echo Stopping netdata container...
	@-$(DOCKER_CMD) rm -f netdata

.PHONY: machine-ip machine-create machine-start machine-stop machine-rm \
				compose-build compose-up compose-ps compose-stop compose-rm compose-start compose-restart compose-logs \
				docker-images docker-clean docker-clean-containers docker-clean-images docker-login docker-ps docker-ps-a docker-stats docker-exec \
				docker-build tag-dev tag-prod push-dev push-prod netdata netdata-stop
