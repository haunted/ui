package main

import (
	"flag"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

var (
	apiAddr    = flag.String("gw-addr", "http://localhost:8003", "http address for admin-gw service (to proxy requests to)")
	listenPort = flag.String("port", ":3015", "port to listen for http requests on")
	wwwPath    = flag.String("www", "dist/", "path to built web application")
)

func main() {
	flag.Parse()
	log.Println("Starting App File Server:", *listenPort)

	mux := http.NewServeMux()
	mux.Handle("/", makeAppHandler(*wwwPath))

	log.Fatal(http.ListenAndServe(*listenPort, mux))
}

func makeAppHandler(pathstr string) http.HandlerFunc {
	log.Println("proxying to", *apiAddr)
	target, err := url.Parse(*apiAddr)
	if err != nil {
		panic(err)
	}
	log.Println("url", target)

	health, err := url.Parse(target.String() + "/admin/")
	if err != nil {
		panic(err)
	}
	go func() {
		for {
			resp, err := http.Get(health.String())
			if err != nil {
				log.Println("WARN: could not reach api server")
				time.Sleep(10 * time.Second)
			} else {
				resp.Body.Close()
				log.Println("API server alive")
				time.Sleep(60 * time.Second)
			}
		}
	}()

	targetQuery := target.RawQuery
	p := httputil.ReverseProxy{
		Director: func(req *http.Request) {
			req.URL.Scheme = target.Scheme
			req.URL.Host = target.Host
			req.URL.Path = singleJoiningSlash(target.Path, req.URL.Path)
			if targetQuery == "" || req.URL.RawQuery == "" {
				req.URL.RawQuery = targetQuery + req.URL.RawQuery
			} else {
				req.URL.RawQuery = targetQuery + "&" + req.URL.RawQuery
			}
			req.Host = target.Host
		},
	}

	fs := http.FileServer(http.Dir(pathstr))
	return func(w http.ResponseWriter, r *http.Request) {

		// if file found, serve file
		if _, err := os.Stat(path.Join(pathstr, r.URL.Path)); err == nil {
			fs.ServeHTTP(w, r)
			return
		}
		if strings.HasPrefix(r.URL.Path, "/api/") {
			p.ServeHTTP(w, r)
			return
		}
		// route to index.html for file not found
		http.ServeFile(w, r, path.Join(pathstr, "index.html"))
	}

}

func singleJoiningSlash(a, b string) string {
	aslash := strings.HasSuffix(a, "/")
	bslash := strings.HasPrefix(b, "/")
	switch {
	case aslash && bslash:
		return a + b[1:]
	case !aslash && !bslash:
		return a + "/" + b
	}
	return a + b
}
