FROM node

RUN npm install -g watchman myth browserify uglifyjs babelify

VOLUME /src
WORKDIR /src

RUN npm install

# ENV NODE_ENV="development"
ENV NODE_ENV="production"

CMD ["./build.sh"]
