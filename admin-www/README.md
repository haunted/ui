# admin
Admin UIs for the Redeam System


### Instructions

```shell

# clone me
git clone git@github.com:actourex/admin.git

# get deps
npm install
sudo npm install -g watchman myth browserify uglifyjs babelify

# watch / build the app
./build.sh

# build the dev-server
go build

# run dev-server / reverse proxy written in go
ngrok="http://localhost:60016" ./admin
ngrok="http://some-ngrok.com" ./admin

# link: http://localhost:3015/

# check the config
cat .env
```
